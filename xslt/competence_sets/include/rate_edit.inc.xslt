<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_rate_edit">
		<xsl:param name="draw_button" select="true()"/>
		<xsl:param name="legend" select="''"/>
		<xsl:param name="descr" select="''"/>
		<fieldset>
			<xsl:if test="$legend != ''">
				<legend class="toggle" jq-toggle="jq-toggle-tree">
					<xsl:value-of select="$legend"/>
				</legend>
			</xsl:if>
			<div id="jq-toggle-tree">
				<xsl:copy-of select="$descr"/>
				<xsl:call-template name="draw_tree_head"/>
				<div class="tree">
					<xsl:call-template name="draw_tree_competence_group">
						<xsl:with-param name="competence_is_clickable" select="false()"/>
						<xsl:with-param name="show_bars" select="false()"/>
					</xsl:call-template>
				</div>
			</div>
		</fieldset>
		<xsl:if test="$draw_button">
			<button class="button">Сохранить</button>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="tree_right_th" match="group">
		<th class="rate-select-">
			<div>
				<strong>Влияние&#160;на&#160;развитие</strong>
			</div>
		</th>
	</xsl:template>
	<xsl:variable name="rate_competences" select="/root/rate[1]/competence"/>
	<xsl:variable name="rate_options" select="/root/rate[1]/options/option"/>
	<xsl:template mode="tree_right_td" match="competence">
		<td class="rate-select-">
			<div>
				<select name="rates[{current()/@id}]">
					<xsl:variable select="current()" name="current_rate" />
					<xsl:for-each select="$rate_options">
						<option value="{@mark}">
							<xsl:if test="$rate_competences[@id = $current_rate/@id]/@mark = current()/@mark">
								<xsl:attribute name="selected">selected</xsl:attribute>
							</xsl:if>
							<xsl:if test="$pass_info/vars/var[@name = 'rates']/item[@index = $current_rate/@mark] = current()/@id">
								<xsl:attribute name="selected">selected</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="@mark_title"/>
						</option>
					</xsl:for-each>
				</select>
			</div>
		</td>
	</xsl:template>
</xsl:stylesheet>
