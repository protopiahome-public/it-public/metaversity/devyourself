<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_rate_show">
		<xsl:param name="show_used_competence_set" select="true()"/>
		<xsl:param name="no_descr" select="false()"/>
		<xsl:variable name="competence_set" select="/root/competence_set_short[@id = /root/rate[1]/@competence_set_id]"/>
		<xsl:variable name="rate" select="/root/rate[1]"/>
		<xsl:if test="descr/div/* and not($no_descr)">
			<div class="box">
				<xsl:copy-of select="descr/div"/>
			</div>
		</xsl:if>
		<xsl:if test="not($rate/competence)">
			<p>Пока нет ни одной компетенции.</p>
		</xsl:if>
		<xsl:if test="$rate/competence">
			<xsl:if test="$show_used_competence_set">
				<p>
					<xsl:text>Используется набор компетенций </xsl:text>
					<xsl:for-each select="$competence_set[1]">
						<a href="{$prefix}/sets/set-{@id}/">
							<xsl:value-of select="@title"/>
						</a>
					</xsl:for-each>
					<xsl:text>.</xsl:text>
				</p>
			</xsl:if>
			<table class="tbl">
				<tr>
					<th>
						<span>Компетенция</span>
					</th>
					<th class="center-">
						<span>Уровень</span>
					</th>
				</tr>
				<xsl:for-each select="$rate/competence">
					<tr>
						<td class="col-text-">
							<xsl:call-template name="draw_competence_title"/>
						</td>
						<td class="col-text- col-center-">
							<xsl:value-of select="$rate/options/option[@mark = current()/@mark]/@mark_title"/>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
