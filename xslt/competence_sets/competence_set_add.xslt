<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/competence_sets_head2.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'sets'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/competence_sets/')"/>
	<xsl:template match="competence_set_add">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_competence_sets_head2"/>
							<div class="content">
								<h3>Создание набора компетенций</h3>
								<xsl:choose>
									<xsl:when test="$user/@id">
										<form action="{$save_prefix}/competence_set_add/" method="post">
											<xsl:call-template name="draw_dt_edit"/>
											<input type="hidden" name="retpath" value="{$prefix}/sets/set-%ID%/admin/"/>
										</form>
									</xsl:when>
									<xsl:otherwise>
										<p>Чтобы создать набор компетенций, сначала нужно <a href="{$reg_url}">зарегистрироваться</a> и <a href="{$login_url}">войти</a>.</p>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Создание набора компетенций'"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/sets/">Наборы компетенций</a>
		</div>
		<div class="level2- selected-">Создание набора компетенций</div>
	</xsl:template>
</xsl:stylesheet>
