<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/competence_set_head2.inc.xslt"/>
	<xsl:variable name="top_section" select="'sets'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="competence_set_section" select="'profile'"/>
	<xsl:variable name="competence_set_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="$current_competence_set_url"/>
	<xsl:template match="competence_set_profile">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_competence_set_head2"/>
							<h2>Описание набора компетенций</h2>
							<div class="content">
								<xsl:call-template name="_draw_main_info"/>
								<xsl:call-template name="_draw_descr"/>
								<xsl:call-template name="_draw_statistics"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_main_info">
		<h3 class="black">
			<xsl:value-of select="@title"/>
		</h3>
		<div class="kv-info">
			<span class="hl">Дата добавления: </span>
			<xsl:call-template name="get_full_date">
				<xsl:with-param name="datetime" select="@add_time"/>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="_draw_descr">
		<xsl:if test="descr/div/*">
			<h3 class="down">Описание</h3>
			<xsl:copy-of select="descr/div"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="_draw_statistics">
		<h3 class="down">Статистика</h3>
		<div class="kv-info">
			<span class="hl">Количество компетенций: </span>
			<xsl:value-of select="@competence_count_calc"/>
		</div>
		<div class="kv-info">
			<span class="hl">Количество профессий: </span>
			<xsl:value-of select="@professiogram_count_calc"/>
		</div>
		<div class="kv-info">
			<span class="hl">Используется для оценивания</span>
			<xsl:text> в </xsl:text>
			<xsl:value-of select="@project_count_calc"/>
			<xsl:call-template name="count_case">
				<xsl:with-param name="number" select="@project_count_calc"/>
				<xsl:with-param name="word_ns" select="' проекте'"/>
				<xsl:with-param name="word_gs" select="' проектах'"/>
				<xsl:with-param name="word_ap" select="' проектах'"/>
			</xsl:call-template>
		</div>
		<div class="kv-info">
			<span class="hl">Оценок без переводчиков: </span>
			<xsl:call-template name="draw_mark_count_full" match="$currenct_competence_set">
				<xsl:with-param name="with_trans" select="false()"/>
			</xsl:call-template>
		</div>
		<div class="kv-info">
			<span class="hl">Оценок с переводчиками: </span>
			<xsl:call-template name="draw_mark_count_full" match="$currenct_competence_set">
				<xsl:with-param name="with_trans" select="true()"/>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_competence_set/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/sets/">Наборы компетенций</a>
		</div>
		<div class="level2- selected-">
			<xsl:value-of select="$current_competence_set/@title"/>
		</div>
	</xsl:template>
</xsl:stylesheet>
