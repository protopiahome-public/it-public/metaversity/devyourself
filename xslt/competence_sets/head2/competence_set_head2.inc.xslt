<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="current_competence_set" select="/root/competence_set_short[1]"/>
	<xsl:variable name="current_competence_set_url" select="concat($prefix, '/sets/set-', /root/competence_set_short/@id, '/')"/>
	<xsl:variable name="competence_set_access" select="/root/competence_set_access"/>
	<xsl:template name="draw_competence_set_head2">
		<div id="minisite-head">
			<table class="border-">
				<td class="left-">
					<div/>
				</td>
				<td class="main-">
					<div class="title-">
						<div class="site-">набор компетенций</div>
						<xsl:value-of select="/root/competence_set_short/@title"/>
					</div>
					<div class="menu-">
						<div>
							<xsl:attribute name="class"><xsl:text>menu-item- </xsl:text><xsl:if test="$competence_set_section = 'profile'">menu-item-selected- </xsl:if></xsl:attribute>
							<div>
								<xsl:choose>
									<xsl:when test="$competence_set_section = 'profile' and $competence_set_section_main_page and not($get_vars)">Описание</xsl:when>
									<xsl:otherwise>
										<a href="{$current_competence_set_url}">Описание</a>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
						<div>
							<xsl:attribute name="class"><xsl:text>menu-item- </xsl:text><xsl:if test="$competence_set_section = 'competences'">menu-item-selected- </xsl:if></xsl:attribute>
							<div>
								<xsl:choose>
									<xsl:when test="$competence_set_section = 'competences' and $competence_set_section_main_page and not($get_vars)">Компетенции</xsl:when>
									<xsl:otherwise>
										<a href="{$current_competence_set_url}competences/">Компетенции</a>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
						<div>
							<xsl:attribute name="class"><xsl:text>menu-item- </xsl:text><xsl:if test="$competence_set_section = 'professiograms'">menu-item-selected- </xsl:if></xsl:attribute>
							<div>
								<xsl:choose>
									<xsl:when test="$competence_set_section = 'professiograms' and $competence_set_section_main_page and not($get_vars)">Профессии</xsl:when>
									<xsl:otherwise>
										<a href="{$current_competence_set_url}professiograms/">Профессии</a>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
						<xsl:if test="$competence_set_access/@has_admin_rights = 1">
							<div>
								<xsl:attribute name="class"><xsl:text>menu-item- </xsl:text><xsl:if test="$competence_set_section = 'admin'">menu-item-selected- </xsl:if></xsl:attribute>
								<div class="menu-item-admin-">
									<xsl:choose>
										<xsl:when test="$competence_set_section = 'admin' and $competence_set_section_main_page and not($get_vars)">Админка</xsl:when>
										<xsl:otherwise>
											<a href="{$current_competence_set_url}admin/">Админка</a>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
						</xsl:if>
					</div>
					<div class="bottom-border-"/>
				</td>
				<td class="right-">
					<div/>
				</td>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
