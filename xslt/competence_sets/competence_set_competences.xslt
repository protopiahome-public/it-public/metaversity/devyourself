<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/competence_set_head2.inc.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:variable name="top_section" select="'sets'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="competence_set_section" select="'competences'"/>
	<xsl:variable name="competence_set_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($current_competence_set_url, 'competences/')"/>
	<xsl:template match="competence_set_competences">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_competence_set_head2"/>
							<h2>Компетенции</h2>
							<div class="content">
								<xsl:call-template name="_draw_tree"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_tree">
		<xsl:call-template name="draw_tree_head"/>
		<div class="tree">
			<xsl:call-template name="draw_tree_competence_group">
				<xsl:with-param name="competence_is_clickable" select="false()"/>
				<xsl:with-param name="show_bars" select="false()"/>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template mode="tree_right_th" match="group">
		<th class="competence-stat-">
			<div>
				<strong>Количество оценок</strong>
			</div>
		</th>
	</xsl:template>
	<xsl:template mode="tree_right_td" match="competence">
		<td class="competence-stat-">
			<div>
				<span class="with-hint">
					<xsl:call-template name="draw_mark_count_hint"/>
				</span>
			</div>
		</td>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Компетенции &#8212; </xsl:text>
		<xsl:value-of select="$current_competence_set/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/sets/">Наборы компетенций</a>
		</div>
		<div class="level2-">
			<a href="{$current_competence_set_url}">
				<xsl:value-of select="$current_competence_set/@title"/>
			</a>
		</div>
		<div class="level3- selected-">Компетенции</div>
	</xsl:template>
</xsl:stylesheet>
