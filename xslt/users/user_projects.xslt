<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/user_head2.inc.xslt"/>
	<xsl:include href="include/user_projects.inc.xslt"/>
	<xsl:include href="../_core/auxil/paging.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'projects'"/>
	<xsl:variable name="user_section_main_page" select="/root/user_projects/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($prefix, '/users/', $current_user/@login, '/projects/')"/>
	<xsl:template match="user_projects">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_user_head2"/>
							<h2>Участие в проектах</h2>
							<div class="content">
								<xsl:call-template name="_draw_table"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_table">
		<xsl:choose>
			<xsl:when test="project">
				<table class="tbl tbl-top-">
					<xsl:call-template name="draw_user_projects"/>
				</table>
				<xsl:apply-templates select="pages[page]"/>
			</xsl:when>
			<xsl:otherwise>
				<p>Пользователь пока не участвует ни в одном проекте.</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Проекты &#8212; </xsl:text>
		<xsl:value-of select="$user_title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/users/">Люди</a>
		</div>
		<div class="level2-">
			<a href="{$prefix}/users/{$current_user/@login}/">
				<xsl:value-of select="$current_user/@any_name"/>
			</a>
		</div>
		<div class="level3- selected-">
			<xsl:choose>
				<xsl:when test="$user_section_main_page">Проекты</xsl:when>
				<xsl:otherwise>
					<a href="{$module_url}">Проекты</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
