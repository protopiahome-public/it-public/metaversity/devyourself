<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="menu/user_results_submenu.inc.xslt"/>
	<xsl:include href="head2/user_head2.inc.xslt"/>
	<xsl:include href="search/user_results_search.inc.xslt"/>
	<xsl:include href="../_core/auxil/paging.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'results'"/>
	<xsl:variable name="user_section_main_page" select="/root/user_results/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($prefix, '/users/', $current_user/@login, '/results/')"/>
	<xsl:template match="user_results">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_user_head2"/>
							<h2>Результаты</h2>
							<div class="content">
								<xsl:call-template name="draw_user_results_submenu"/>
								<xsl:call-template name="_draw_box"/>
								<xsl:call-template name="_draw_table"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
							<xsl:call-template name="draw_user_results_search"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_box">
		<div class="box">
			<div class="content-">
				<strong>Сначала выберите набор компетенций.</strong> На следующем шаге вы увидите профессии и то, насколько пользователь им соответствует. Далее вы сможете более детально изучить это соответствие. Воспользуйтесь поиском (он справа), чтобы быстрее найти нужную профессию.
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_table">
		<xsl:if test="not(competence_set)">
			<p>По заданным критериям поиска ни одного набора компетенций не найдено.</p>
		</xsl:if>
		<xsl:if test="competence_set">
			<xsl:if test="filter/field[@name = 'search'] != ''">
				<p>
					<xsl:text>Результаты поиска по запросу &#171;</xsl:text>
					<strong>
						<xsl:value-of select="filter/field[@name = 'search']"/>
					</strong>
					<xsl:text>&#187;.</xsl:text>
				</p>
			</xsl:if>
			<p>
				<xsl:text>Отображаются наборы компетенций </xsl:text>
				<xsl:value-of select="pages/@from_row"/>
				<xsl:text> &#8212; </xsl:text>
				<xsl:value-of select="pages/@to_row"/>
				<xsl:text> из </xsl:text>
				<xsl:value-of select="pages/@row_count"/>
				<xsl:text>.</xsl:text>
			</p>
			<table class="tbl">
				<tr>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="'Набор&#160;компетенций'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'profs'"/>
						<xsl:with-param name="title" select="'Профессий'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'marks_trans'"/>
						<xsl:with-param name="title" select="'Оценок'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'marks'"/>
						<xsl:with-param name="title" select="'Оценок&#160;(без&#160;переводчиков)'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<th>
						<span>Ссылки</span>
					</th>
				</tr>
				<xsl:for-each select="competence_set">
					<xsl:variable name="position" select="position()"/>
					<xsl:for-each select="/root/user_competence_sets/competence_set[@id = current()/@id]">
						<tr>
							<td class="col-title-">
								<a href="{$module_url}set-{@id}/">
									<xsl:value-of select="@title"/>
								</a>
							</td>
							<td class="col-digits-">
								<xsl:value-of select="@professiogram_count_calc"/>
							</td>
							<td class="col-digits-">
								<span class="with-hint">
									<xsl:call-template name="draw_mark_count_hint">
										<xsl:with-param name="position" select="0"/>
										<xsl:with-param name="with_trans" select="true()"/>
									</xsl:call-template>
								</span>
								<xsl:if test="@total_mark_count_with_trans_calc != 0 or $position = 1">
									<xsl:text> (</xsl:text>
									<xsl:if test="$position = 1">
										<xsl:text>за </xsl:text>
									</xsl:if>
									<xsl:value-of select="@project_count_with_trans_calc"/>
									<xsl:if test="$position = 1">
										<xsl:call-template name="count_case">
											<xsl:with-param name="number" select="@project_count_with_trans_calc"/>
											<xsl:with-param name="word_ns" select="' проект'"/>
											<xsl:with-param name="word_gs" select="' проекта'"/>
											<xsl:with-param name="word_ap" select="' проектов'"/>
										</xsl:call-template>
									</xsl:if>
									<xsl:text>)</xsl:text>
								</xsl:if>
							</td>
							<td class="col-digits-">
								<span class="with-hint">
									<xsl:call-template name="draw_mark_count_hint">
										<xsl:with-param name="position" select="0"/>
									</xsl:call-template>
								</span>
								<xsl:if test="@total_mark_count_calc != 0 or $position = 1">
									<xsl:text> (</xsl:text>
									<xsl:if test="$position = 1">
										<xsl:text>за </xsl:text>
									</xsl:if>
									<xsl:value-of select="@project_count_calc"/>
									<xsl:if test="$position = 1">
										<xsl:call-template name="count_case">
											<xsl:with-param name="number" select="@project_count_calc"/>
											<xsl:with-param name="word_ns" select="' проект'"/>
											<xsl:with-param name="word_gs" select="' проекта'"/>
											<xsl:with-param name="word_ap" select="' проектов'"/>
										</xsl:call-template>
									</xsl:if>
									<xsl:text>)</xsl:text>
								</xsl:if>
							</td>
							<td class="col-links-">
								<ul>
									<li>
										<a title="Полная компетенционная карта по данному набору компетенций" href="{$module_url}set-{@id}/full/">полная&#160;карта</a>
									</li>
								</ul>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
			<xsl:apply-templates select="pages[page]"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Результаты &#8212; </xsl:text>
		<xsl:value-of select="$user_title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/users/">Люди</a>
		</div>
		<div class="level2-">
			<a href="{$prefix}/users/{$current_user/@login}/">
				<xsl:value-of select="$current_user/@any_name"/>
			</a>
		</div>
		<div class="level3- selected-">
			<xsl:choose>
				<xsl:when test="$user_section_main_page">Результаты</xsl:when>
				<xsl:otherwise>
					<a href="{$current_user_base_url}results/">Результаты</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
