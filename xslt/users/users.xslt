<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/users_head2.inc.xslt"/>
	<xsl:include href="search/users_search.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="/root/users/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($prefix, '/users/')"/>
	<xsl:template match="users">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_users_head2"/>
							<div class="content">
								<xsl:call-template name="_draw_table"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
							<xsl:call-template name="draw_users_search"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_table">
		<xsl:if test="filter/field[@name = 'search'] != ''">
			<p>
				<xsl:text>Результаты поиска по запросу &#171;</xsl:text>
				<strong>
					<xsl:value-of select="filter/field[@name = 'search']"/>
				</strong>
				<xsl:text>&#187;.</xsl:text>
			</p>
		</xsl:if>
		<xsl:if test="not(user)">
			<p>По заданным критериям поиска ни одного пользователя не найдено.</p>
		</xsl:if>
		<xsl:if test="user">
			<p>
				<xsl:text>Отображаются пользователи </xsl:text>
				<xsl:value-of select="pages/@from_row"/>
				<xsl:text> &#8212; </xsl:text>
				<xsl:value-of select="pages/@to_row"/>
				<xsl:text> из </xsl:text>
				<xsl:value-of select="pages/@row_count"/>
				<xsl:text>.</xsl:text>
			</p>
			<table class="tbl">
				<tr>
					<th/>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'name'"/>
						<xsl:with-param name="title" select="'Имя&#160;пользователя'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'mark-count'"/>
						<xsl:with-param name="title" select="'Оценок'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<th>
						<span>Участвует в проектах</span>
					</th>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'reg-time'"/>
						<xsl:with-param name="title" select="'Дата&#160;регистрации'"/>
					</xsl:call-template>
				</tr>
				<xsl:for-each select="user">
					<xsl:variable name="position" select="position()"/>
					<xsl:variable name="context" select="."/>
					<xsl:for-each select="/root/user_short[@id = current()/@id]">
						<xsl:variable name="current_user" select="."/>
						<tr>
							<td class="col-user-avatar-">
								<span>
									<a href="{$prefix}/users/{@login}/">
										<img src="{photo_big/@url}" width="{photo_big/@width}" height="{photo_big/@height}" alt="{@login}"/>
									</a>
								</span>
							</td>
							<td class="col-user-name-">
								<div class="title-">
									<a href="{$prefix}/users/{@login}/">
										<xsl:value-of select="@visible_name"/>
									</a>
								</div>
								<div class="second-">
									<xsl:value-of select="@login"/>
								</div>
							</td>
							<td class="col-digits-">
								<span class="with-hint">
									<xsl:for-each select="$context">
										<xsl:call-template name="draw_mark_count_hint">
											<xsl:with-param name="position" select="0"/>
										</xsl:call-template>
									</xsl:for-each>
								</span>
							</td>
							<td class="col-list-">
								<xsl:if test="/root/user_project_links/user[@id = current()/@id]/project">
									<ul>
										<xsl:for-each select="/root/user_project_links/user[@id = current()/@id]/project">
											<li>
												<span class="hl">
													<!-- @dm9 CR -->
													<!-- @ar -->
													<a href="{$prefix}/p/{@url_name}/">
														<xsl:value-of select="@title"/>
													</a>
												</span>
												<xsl:text> (</xsl:text>
												<span class="with-hint">
													<xsl:call-template name="draw_mark_count_hint">
														<xsl:with-param name="position" select="position()"/>
													</xsl:call-template>
												</span>
												<xsl:text>)&#160;| </xsl:text>
												<!-- @dm9 CR -->
												<!-- @ar -->
												<a href="{$prefix}/p/{@url_name}/users/{$current_user/@login}/">профиль</a>
											</li>
										</xsl:for-each>
									</ul>
								</xsl:if>
							</td>
							<td class="col-date-">
								<xsl:call-template name="get_full_date">
									<xsl:with-param name="datetime" select="@add_time"/>
								</xsl:call-template>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
			<xsl:apply-templates select="pages[page]"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Люди'"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1- selected-">
			<xsl:choose>
				<xsl:when test="$top_section_main_page">Люди</xsl:when>
				<xsl:otherwise>
					<a href="{$prefix}/users/">Люди</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
