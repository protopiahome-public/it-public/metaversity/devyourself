<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="menu/user_results_submenu.inc.xslt"/>
	<xsl:include href="head2/user_head2.inc.xslt"/>
	<xsl:include href="menu/user_results_professiogram_match_submenu.inc.xslt"/>
	<xsl:include href="include/user_results.inc.xslt"/>
	<xsl:include href="../_site/include/rose.inc.xslt"/>
	<xsl:include href="../_site/include/legend.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'results'"/>
	<xsl:variable name="user_section_main_page" select="/root/user_results/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_user_base_url, 'results/prof-', /root/user_results_professiogram_match/@professiogram_id, '/')"/>
	<xsl:template match="user_results_professiogram_match">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_user_head2"/>
							<h2>Результаты</h2>
							<div class="content">
								<xsl:call-template name="draw_user_results_submenu"/>
								<h3 class="pre" title="Набор компетенций">
									<a href="{$current_user_base_url}results/set-{@competence_set_id}/">
										<xsl:value-of select="/root/competence_set_short[@id = current()/@competence_set_id]/@title"/>
									</a>
								</h3>
								<h4 title="Профессия">
									<xsl:choose>
										<xsl:when test="not($get_vars)">
											<xsl:value-of select="@professiogram_title"/>
										</xsl:when>
										<xsl:otherwise>
											<a href="{$prefix}/ratings/prof-{@professiogram_id}/">
												<xsl:value-of select="@professiogram_title"/>
											</a>
										</xsl:otherwise>
									</xsl:choose>
								</h4>
								<xsl:call-template name="draw_user_results_professiogram_match_percent"/>
								<xsl:call-template name="draw_user_results_professiogram_match_submenu"/>
								<xsl:call-template name="_draw_rose"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
							<xsl:call-template name="draw_user_results_project_comparison"/>
							<xsl:call-template name="_draw_legend"/>
							<xsl:call-template name="draw_filter">
								<xsl:with-param name="competence_set_id" select="@competence_set_id"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_legend">
		<xsl:call-template name="draw_legend">
			<xsl:with-param name="type" select="'rose'"/>
			<xsl:with-param name="need_prof" select="true()"/>
			<xsl:with-param name="sum" select="true()"/>
			<xsl:with-param name="self" select="true()"/>
			<xsl:with-param name="focus" select="true()"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="_draw_rose">
		<xsl:call-template name="draw_rose">
			<xsl:with-param name="image_alt" select="concat('Соответствие профессии &#171;', @professiogram_title, '&#187;')"/>
			<xsl:with-param name="focus_competences" select="/root/vectors_data/focus/competence"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script src="{$prefix}/js/project_comparison.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Соответствие: </xsl:text>
		<xsl:value-of select="user_results_professiogram_match/@professiogram_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$user_title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/users/">Люди</a>
		</div>
		<div class="level2-">
			<a href="{$prefix}/users/{$current_user/@login}/">
				<xsl:value-of select="$current_user/@any_name"/>
			</a>
		</div>
		<div class="level3-">
			<xsl:choose>
				<xsl:when test="$user_section_main_page">Результаты</xsl:when>
				<xsl:otherwise>
					<a href="{$current_user_base_url}results/">Результаты</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<div class="level4-">
			<a href="{$current_user_base_url}results/set-{user_results_professiogram_match/@competence_set_id}/">
				<xsl:value-of select="/root/competence_set_short[@id = current()/user_results_professiogram_match/@competence_set_id]/@title"/>
			</a>
		</div>
		<div class="level5- selected-">
			<xsl:value-of select="user_results_professiogram_match/@professiogram_title"/>
		</div>
	</xsl:template>
</xsl:stylesheet>
