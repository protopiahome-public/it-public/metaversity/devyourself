<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_common.xslt"/>
	<xsl:include href="../../resources/include/events_calendar.inc.xslt"/>
	<xsl:variable name="current_user" select="/root/user_short"/>
	<xsl:template match="user_events_calendar">
		<xsl:call-template name="draw_events_calendar"/>
	</xsl:template>
	<xsl:template match="text()"/>
</xsl:stylesheet>
