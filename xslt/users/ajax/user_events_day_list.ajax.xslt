<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_common.xslt"/>
	<xsl:include href="../../resources/include/events_day_list.inc.xslt"/>
	<xsl:template match="user_events">
		<xsl:call-template name="events_day_list"/>
	</xsl:template>
	<xsl:template match="text()"/>
</xsl:stylesheet>
