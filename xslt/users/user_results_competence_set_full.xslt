<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="menu/user_results_submenu.inc.xslt"/>
	<xsl:include href="head2/user_head2.inc.xslt"/>
	<xsl:include href="include/user_results.inc.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:include href="../_site/include/legend.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'results'"/>
	<xsl:variable name="user_section_main_page" select="/root/user_results/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_user_base_url, 'results/set-', /root/user_results_competence_set_full/@competence_set_id, '/full/')"/>
	<xsl:template match="user_results_competence_set_full">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_user_head2"/>
							<h2>Результаты</h2>
							<div class="content">
								<xsl:call-template name="draw_user_results_submenu"/>
								<h3 class="pre" title="Набор компетенций">
									<a href="{$current_user_base_url}results/set-{@competence_set_id}/">
										<xsl:value-of select="/root/competence_set_short[@id = current()/@competence_set_id]/@title"/>
									</a>
								</h3>
								<h4>Полная компетенционная карта</h4>
								<xsl:call-template name="draw_tree_head"/>
								<xsl:call-template name="_draw_tree"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
							<xsl:call-template name="draw_user_results_project_comparison"/>
							<xsl:call-template name="_draw_legend"/>
							<xsl:call-template name="draw_filter">
								<xsl:with-param name="competence_set_id" select="@competence_set_id"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_legend">
		<xsl:call-template name="draw_legend">
			<xsl:with-param name="type" select="'bars'"/>
			<xsl:with-param name="need_future" select="true()"/>
			<xsl:with-param name="sum" select="true()"/>
			<xsl:with-param name="delta" select="true()"/>
			<xsl:with-param name="self" select="true()"/>
			<xsl:with-param name="focus" select="true()"/>
			<xsl:with-param name="marks" select="true()"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="_draw_tree">
		<div class="tree">
			<xsl:call-template name="draw_tree_competence_group">
				<xsl:with-param name="focus_competences" select="/root/vectors_data/focus/competence"/>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:variable name="tree_competences_detailed" select="/root/user_results_competence_set_full/detailed/competence"/>
	<xsl:template mode="tree_marks" match="competence">
		<xsl:variable name="results_competences" select="/root/user_results_competence_set_full/results/competence"/>
		<xsl:variable name="self_competences" select="/root/vectors_data/self/competence"/>
		<xsl:variable name="current_competence_id" select="@id"/>
		<xsl:for-each select="/root/vectors_data/future/vector">
			<xsl:variable name="need_mark">
				<xsl:choose>
					<xsl:when test="competences/competence[@id = $current_competence_id]">
						<xsl:value-of select="competences/competence[@id = $current_competence_id]/@mark"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:call-template name="draw_tree_bar">
				<xsl:with-param name="type" select="'need'"/>
				<xsl:with-param name="mark" select="$need_mark"/>
				<xsl:with-param name="title" select="'Идеальный результат согласно профилю &#171;Я в будущем&#187;'"/>
				<xsl:with-param name="first" select="true()"/>
				<xsl:with-param name="block_class" select="concat('jq-tree-bars-bar-vector jq-tree-bars-bar-vector-', @id)"/>
				<xsl:with-param name="hide" select="position() != 1"/>
			</xsl:call-template>
			<xsl:call-template name="draw_tree_bar">
				<xsl:with-param name="type" select="'sum'"/>
				<xsl:with-param name="mark">
					<xsl:choose>
						<xsl:when test="$results_competences[@id = $current_competence_id]">
							<xsl:value-of select="$results_competences[@id = $current_competence_id]/@mark"/>
						</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="title" select="'Результат'"/>
				<xsl:with-param name="draw_delta" select="true()"/>
				<xsl:with-param name="need_mark" select="$need_mark"/>
				<xsl:with-param name="need_mark_title_dative" select="'профилю &#171;Я в будущем&#187;'"/>
				<xsl:with-param name="block_class" select="concat('jq-tree-bars-bar-vector jq-tree-bars-bar-vector-', @id)"/>
				<xsl:with-param name="hide" select="position() != 1"/>
			</xsl:call-template>
		</xsl:for-each>
		<xsl:if test="not(/root/vectors_data/future/vector)">
			<xsl:call-template name="draw_tree_bar">
				<xsl:with-param name="type" select="'sum'"/>
				<xsl:with-param name="first" select="true()"/>
				<xsl:with-param name="mark">
					<xsl:choose>
						<xsl:when test="$results_competences[@id = $current_competence_id]">
							<xsl:value-of select="$results_competences[@id = $current_competence_id]/@mark"/>
						</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="title" select="'Результат'"/>
				<xsl:with-param name="draw_delta" select="false()"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:call-template name="draw_tree_bar">
			<xsl:with-param name="type" select="'self'"/>
			<xsl:with-param name="mark">
				<xsl:choose>
					<xsl:when test="$self_competences[@id = current()/@id]">
						<xsl:value-of select="$self_competences[@id = current()/@id]/@mark"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="title" select="'Самооценка'"/>
			<xsl:with-param name="block_class" select="'jq-tree-bars-bar-self'"/>
			<xsl:with-param name="hide" select="true()"/>
		</xsl:call-template>
		<xsl:for-each select="$results_competences[@id = current()/@id]/project">
			<xsl:call-template name="draw_tree_bar">
				<xsl:with-param name="type" select="'case2'"/>
				<xsl:with-param name="mark" select="@mark"/>
				<xsl:with-param name="title" select="' '"/>
				<xsl:with-param name="draw_delta" select="false()"/>
				<xsl:with-param name="block_class" select="concat('jq-tree-bars-bar-project jq-tree-bars-bar-project-', @key)"/>
				<xsl:with-param name="hide" select="true()"/>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script src="{$prefix}/js/project_comparison.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Полная компетенционная карта: </xsl:text>
		<xsl:value-of select="/root/competence_set_short[@id = current()/user_results_competence_set_full/@competence_set_id]/@title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$user_title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/users/">Люди</a>
		</div>
		<div class="level2-">
			<a href="{$prefix}/users/{$current_user/@login}/">
				<xsl:value-of select="$current_user/@any_name"/>
			</a>
		</div>
		<div class="level3-">
			<xsl:choose>
				<xsl:when test="$user_section_main_page">Результаты</xsl:when>
				<xsl:otherwise>
					<a href="{$current_user_base_url}results/">Результаты</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<div class="level4-">
			<a href="{$current_user_base_url}results/set-{user_results_competence_set_full/@competence_set_id}/">
				<xsl:value-of select="/root/competence_set_short[@id = current()/user_results_competence_set_full/@competence_set_id]/@title"/>
			</a>
		</div>
		<div class="level5- selected-">Полная компетенционная карта</div>
	</xsl:template>
</xsl:stylesheet>
