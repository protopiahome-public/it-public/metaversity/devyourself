<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/user_head2.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'results'"/>
	<xsl:variable name="user_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($current_user_base_url, 'results/competence-', /root/user_competence_marks/@competence_id, '/')"/>
	<xsl:variable name="competence_title">
		<xsl:call-template name="draw_competence_title">
			<xsl:with-param name="competence" select="/root/competence_short[@id = /root/user_results_competence_marks/@competence_id]"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:template match="user_results_competence_marks">
		<div class="columns-wrap columns-wrap-default">
			<table cellspacing="0" class="columns">
				<tr>
					<td class="center-column">
						<div class="color-blue">
							<xsl:call-template name="draw_user_head2"/>
							<h2>Результаты</h2>
						</div>
						<div class="content">
							<div class="color-blue">
								<h3 class="pre" title="Набор компетенций">
									<a href="{$current_user_base_url}results/set-{@competence_set_id}/">
										<xsl:value-of select="/root/competence_set_short[@id = current()/@competence_set_id]/@title"/>
									</a>
								</h3>
								<h4 title="Компетенция">
									<xsl:value-of select="$competence_title"/>
								</h4>
								<xsl:call-template name="_draw_main_info"/>
							</div>
							<div class="color-green">
								<xsl:call-template name="_draw_projects"/>
							</div>
						</div>
					</td>
					<td class="right-column">
						<div class="color-blue">
							<xsl:call-template name="draw_nav"/>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="_draw_main_info">
		<div class="page-marks">
			<div class="competence-info-">
				<div class="result-">
					<xsl:text>Итоговый уровень: </xsl:text>
					<span class="hl">
						<xsl:call-template name="draw_mark_title">
							<xsl:with-param name="mark" select="@mark"/>
							<xsl:with-param name="start_low_case" select="true()"/>
						</xsl:call-template>
					</span>
				</div>
				<div class="note hint-">Получен как максимальный результат среди выбранных проектов</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_projects">
		<div class="page-marks">
			<div class="projects-">
				<hr/>
				<xsl:if test="not(projects/project[marks/mark])">
					<p>Оценок по данной компетенции нет.</p>
				</xsl:if>
				<xsl:if test="projects/project[marks/mark]">
					<div class="legend-">
						<div class="item-">
							<div class="personal-"/>
							<span> — личная оценка</span>
						</div>
						<div class="item-">
							<div class="group-"/>
							<span> — групповая оценка</span>
						</div>
					</div>
					<h3 class="down super-">Оценки, использованные при расчёте результата</h3>
				</xsl:if>
				<xsl:for-each select="projects/project[marks/mark]">
					<xsl:variable name="project" select="."/>
					<table cellpadding="0" cellspacing="0" class="project-head-">
						<tr>
							<xsl:for-each select="../../../project_short[@id = current()/@id][1]">
								<td class="title-left-"> </td>
								<td class="title-">
									<a href="{@url}">
										<xsl:value-of select="@title"/>
									</a>
								</td>
								<td class="title-right-"> </td>
								<td class="dates-">
									<span class="note">
										<xsl:call-template name="get_full_date">
											<xsl:with-param name="datetime" select="@start_time"/>
										</xsl:call-template>
										<xsl:if test="substring(@start_time, 1, 10) != substring(@finish_time, 1, 10)">
											<xsl:text> &#8212; </xsl:text>
											<xsl:call-template name="get_full_date">
												<xsl:with-param name="datetime" select="@finish_time"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:text>, </xsl:text>
										<xsl:call-template name="draw_project_state">
											<xsl:with-param name="state" select="@state_calc"/>
											<xsl:with-param name="start_low_case" select="true()"/>
										</xsl:call-template>
										<xsl:text>.</xsl:text>
									</span>
								</td>
							</xsl:for-each>
							<td class="graph-">
								<table cellpadding="0" cellspacing="0">
									<tr class="t-">
										<td title="Склонность">
											<div>Ск.</div>
										</td>
										<td title="Способность">
											<div>Сп.</div>
										</td>
										<td class="last-" title="Компетенция">
											<div>К.</div>
										</td>
									</tr>
									<tr class="b-">
										<td colspan="3">
											<div class="left-border-">
												<xsl:attribute name="title">
													<xsl:text>Текущий уровень: </xsl:text>
													<xsl:call-template name="draw_mark_title">
														<xsl:with-param name="mark" select="@mark"/>
													</xsl:call-template>
												</xsl:attribute>
												<div style="width: {40 * @mark}px" class="fill-"/>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<div class="project-summary-">
						<div class="result-">
							<xsl:text>Уровень: </xsl:text>
							<span class="hl">
								<xsl:call-template name="draw_mark_title">
									<xsl:with-param name="mark" select="@mark"/>
									<xsl:with-param name="start_low_case" select="true()"/>
								</xsl:call-template>
							</span>
							<xsl:choose>
								<xsl:when test="@translator_id">
									<xsl:text> (получен </xsl:text>
									<strong>через переводчик</strong>
									<xsl:text> &#171;</xsl:text>
									<xsl:value-of select="../../../translator_short[@id = current()/@translator_id]/@title"/>
									<xsl:text>&#187; из набора компетенций &#171;</xsl:text>
									<xsl:value-of select="../../../competence_set_short[@id = current()/@src_competence_set_id]/@title"/>
									<xsl:text>&#187;).</xsl:text>
								</xsl:when>
								<xsl:otherwise> (результаты получены напрямую, без переводчика).</xsl:otherwise>
							</xsl:choose>
						</div>
						<xsl:if test="@translator_id">
							<div class="translator-info-">
								<xsl:choose>
									<xsl:when test="src_competences/competence[1] and not(src_competences/competence[2])">
										<p>
											<xsl:text>Результат равен результату по компетенции </xsl:text>
											<xsl:call-template name="draw_competence_title">
												<xsl:with-param name="competence" select="src_competences/competence[1]"/>
											</xsl:call-template>
										</p>
									</xsl:when>
									<xsl:otherwise>
										<p>
											<xsl:text>Результат равен </xsl:text>
											<strong>
												<xsl:choose>
													<xsl:when test="@translation_type = 'MIN'">минимальному</xsl:when>
													<xsl:when test="@translation_type = 'AVG'">среднему</xsl:when>
													<xsl:otherwise>неизвестной функции от компетенций:</xsl:otherwise>
												</xsl:choose>
											</strong>
											<xsl:text> из результатов по компетенциям:</xsl:text>
										</p>
										<ul>
											<xsl:for-each select="src_competences/competence">
												<li>
													<xsl:call-template name="draw_competence_title"/>
													<xsl:choose>
														<xsl:when test="../../@translation_type = 'AVG'">
															<span class="hl">
																<xsl:text> (вес = </xsl:text>
																<xsl:value-of select="@weight"/>
																<xsl:text>)</xsl:text>
															</span>
														</xsl:when>
													</xsl:choose>
												</li>
											</xsl:for-each>
										</ul>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</xsl:if>
					</div>
					<div class="precedents-">
						<xsl:for-each select="../../../precedent_ordered_list/precedent[@id = $project/marks/mark/@precedent_id]">
							<div class="precedent-">
								<xsl:for-each select="../../precedent[@id = current()/@id]">
									<xsl:variable name="precedent_id" select="@id"/>
									<xsl:if test="@precedent_group_title != ''">
										<h3 class="down">
											<xsl:value-of select="@precedent_group_title"/>
										</h3>
									</xsl:if>
									<h4>
										<xsl:if test="@precedent_group_title = ''">
											<xsl:attribute name="class">no-precedent_group-</xsl:attribute>
										</xsl:if>
										<span class="title-">
											<xsl:value-of select="@title"/>
										</span>
										<span class="note date-">
											<xsl:call-template name="get_full_date">
												<xsl:with-param name="datetime" select="@add_time"/>
											</xsl:call-template>
										</span>
										<span class="clickable show-descr-" onclick="$('#jq-precedent-descr-{$project/@project_key}-{$precedent_id}').toggle();">описание события</span>
									</h4>
									<div class="color-yellow">
										<div class="box dn" id="jq-precedent-descr-{$project/@project_key}-{@id}">
											<xsl:copy-of select="descr/div"/>
										</div>
									</div>
									<table class="tbl tbl-marks- tbl-nohead-">
										<xsl:for-each select="$project/marks/mark[@precedent_id = current()/@id]">
											<tr>
												<td class="col-user-avatar-small-" style="padding-left: 0;">
													<xsl:variable name="user" select="/root/user_short[@id = current()/@rater_user_id]"/>
													<a href="{$prefix}/users/{$user/@login}/">
														<xsl:attribute name="title">
															<xsl:text>Свидетель: </xsl:text>
															<xsl:value-of select="$user/@any_name"/>
															<xsl:if test="$user/@any_name != $user/@login">
																<xsl:text> (</xsl:text>
																<xsl:value-of select="$user/@login"/>
																<xsl:text>)</xsl:text>
															</xsl:if>
														</xsl:attribute>
														<img src="{$user/photo_small/@url}" width="{$user/photo_small/@width}" height="{$user/photo_small/@height}" alt="{$user/@login}"/>
													</a>
												</td>
												<td class="col-text-">
													<xsl:choose>
														<xsl:when test="@comment != ''">
															<xsl:value-of select="@comment"/>
														</xsl:when>
														<xsl:otherwise>
															<span class="note">Описания оценки нет. Смотрите
																	<span class="clickable" onclick="$('#jq-precedent-descr-{$project/@project_key}-{$precedent_id}').toggle();">описание всего события</span>.
																</span>
														</xsl:otherwise>
													</xsl:choose>
												</td>
												<td class="col-marks-">
													<div class="width-">
														<xsl:if test="@competence_id != $project/../../@competence_id">
															<div class="mark-competence-">
																<xsl:call-template name="draw_competence_title">
																	<xsl:with-param name="competence" select="/root/competence_short[@id = current()/@competence_id]"/>
																</xsl:call-template>
															</div>
														</xsl:if>
														<table class="marks" cellpadding="0" cellspacing="0">
															<tr>
																<xsl:attribute name="class">
																	<xsl:choose>
																		<xsl:when test="@type = 'personal'">personal-</xsl:when>
																		<xsl:otherwise>group-</xsl:otherwise>
																	</xsl:choose>
																</xsl:attribute>
																<xsl:variable name="mark_type_title">
																	<xsl:choose>
																		<xsl:when test="@type = 'personal'">личная</xsl:when>
																		<xsl:otherwise>групповая</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<td>
																	<xsl:if test="@value = 1">
																		<span title="Склонность ({$mark_type_title} оценка)" class="c10">Ск.</span>
																	</xsl:if>
																</td>
																<td>
																	<xsl:if test="@value = 2">
																		<span title="Способность ({$mark_type_title} оценка)" class="c10">Сп.</span>
																	</xsl:if>
																</td>
																<td>
																	<xsl:if test="@value =3">
																		<span title="Компетенция ({$mark_type_title} оценка)" class="c10">К.</span>
																	</xsl:if>
																</td>
															</tr>
														</table>
													</div>
												</td>
											</tr>
										</xsl:for-each>
									</table>
								</xsl:for-each>
							</div>
						</xsl:for-each>
					</div>
				</xsl:for-each>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Компетенция: </xsl:text>
		<xsl:value-of select="$competence_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$user_title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/users/">Люди</a>
		</div>
		<div class="level2-">
			<a href="{$prefix}/users/{$current_user/@login}/">
				<xsl:value-of select="$current_user/@any_name"/>
			</a>
		</div>
		<div class="level3-">
			<xsl:choose>
				<xsl:when test="$user_section_main_page">Результаты</xsl:when>
				<xsl:otherwise>
					<a href="{$current_user_base_url}results/">Результаты</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<div class="level4-">
			<a href="{$current_user_base_url}results/set-{user_results_competence_marks/@competence_set_id}/">
				<xsl:value-of select="/root/competence_set_short[@id = current()/user_results_competence_marks/@competence_set_id]/@title"/>
			</a>
		</div>
		<div class="level5- selected-">
			<xsl:value-of select="$competence_title"/>
		</div>
	</xsl:template>
</xsl:stylesheet>
