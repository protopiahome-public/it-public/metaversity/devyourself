<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/user_head2.inc.xslt"/>
	<xsl:include href="../resources/include/event_head.inc.xslt"/>
	<xsl:include href="../resources/include/events_calendar.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'events'"/>
	<xsl:variable name="events_url" select="concat($prefix, '/users/', $current_user/@login, '/events/')"/>
	<xsl:variable name="module_postfix">
		<xsl:for-each select="/root/user_events">
			<xsl:choose>
				<xsl:when test="@is_archive = 1">
					<xsl:value-of select="'archive/'"/>
				</xsl:when>
				<xsl:when test="@year and @month and @day">
					<xsl:value-of select="concat('calendar/', @year, '/', @month, '/', @day, '/')"/>
				</xsl:when>
				<xsl:when test="@year and @month">
					<xsl:value-of select="concat('calendar/', @year, '/', @month, '/')"/>
				</xsl:when>
				<xsl:when test="@year">
					<xsl:value-of select="concat('calendar/', @year, '/')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="''"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:variable>
	<xsl:variable name="user_section_main_page" select="not($get_vars) and $module_postfix = '' and /root/user_events/pages/@current_page = 1"/>
	<xsl:variable name="module_url" select="concat($events_url, $module_postfix)"/>
	<xsl:template match="user_events">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_user_head2"/>
							<h2>События</h2>
							<div class="content">
								<xsl:call-template name="_draw_list"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
							<xsl:call-template name="draw_events_calendar"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<div id="resources-head">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td class="l-">
						<div class="submenu- submenu-show-events-">
							<xsl:if test="@is_archive = 1">
								<span class="header-">Отображать: </span>
								<span class="item-">
									<a href="{$events_url}">будущие события</a>
								</span>
								<span class="sep-"/>
								<span class="item- selected-">прошедшие события</span>
							</xsl:if>
							<xsl:if test="@is_archive != 1">
								<span class="header-">Отображать: </span>
								<span class="item- selected-">будущие события</span>
								<span class="sep-"/>
								<span class="item-">
									<a href="{$events_url}archive/">прошедшие события</a>
								</span>
							</xsl:if>
						</div>
					</td>
				</tr>
			</table>
			<div class="what-is-shown-">
				<xsl:choose>
					<xsl:when test="@year > 0 and not(@month > 0)">
						<xsl:text>События за </xsl:text>
						<strong>
							<xsl:value-of select="@year"/>
						</strong>
						<xsl:text>. </xsl:text>
					</xsl:when>
					<xsl:when test="@year > 0 and @month > 0 and not(@day > 0)">
						<xsl:text>События за </xsl:text>
						<strong>
							<xsl:call-template name="get_month_name">
								<xsl:with-param name="month_number" select="@month"/>
								<xsl:with-param name="type" select="'month_small'"/>
							</xsl:call-template>
							<xsl:text> </xsl:text>
							<xsl:value-of select="@year"/>
						</strong>
						<xsl:text>. </xsl:text>
					</xsl:when>
					<xsl:when test="@year > 0 and @month > 0 and @day > 0">
						<xsl:text>События за </xsl:text>
						<strong>
							<xsl:value-of select="@day"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="get_month_name">
								<xsl:with-param name="month_number" select="@month"/>
								<xsl:with-param name="type" select="'with_day'"/>
							</xsl:call-template>
							<xsl:text> </xsl:text>
							<xsl:value-of select="@year"/>
						</strong>
						<xsl:text>. </xsl:text>
					</xsl:when>
				</xsl:choose>
			</div>
		</div>
		<xsl:if test="not(event)">
			<p>Ни одного события не найдено.</p>
		</xsl:if>
		<xsl:if test="event">
			<xsl:for-each select="event">
				<xsl:variable name="position" select="position()"/>
				<xsl:variable name="event_data" select="/root/event_short[@id = current()/@id]"/>
				<xsl:variable name="event_status" select="."/>
				<xsl:for-each select="/root/event_short[@id = current()/@id]">
					<xsl:call-template name="draw_event_head">
						<xsl:with-param name="draw_table_header" select="$position = 1"/>
						<xsl:with-param name="show_ignore_link" select="false()"/>
						<xsl:with-param name="show_edit_link" select="false()"/>
						<xsl:with-param name="event_data" select="$event_data"/>
						<xsl:with-param name="event_status" select="$event_status"/>
						<xsl:with-param name="project" select="/root/project_short[@id = $event_status/@project_id]"/>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:for-each>
			<xsl:apply-templates select="pages[page]"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/events_calendar.js"/>
		<script type="text/javascript">
			var resource_name = 'event';
		</script>
		<script type="text/javascript" src="{$prefix}/js/resource_status.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>События пользователя </xsl:text>
		<xsl:value-of select="$user_title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/users/">Люди</a>
		</div>
		<div class="level2-">
			<a href="{$prefix}/users/{$current_user/@login}/">
				<xsl:value-of select="$current_user/@any_name"/>
			</a>
		</div>
		<div class="level3- selected-">
			<xsl:choose>
				<xsl:when test="$user_section_main_page">События</xsl:when>
				<xsl:otherwise>
					<a href="{$current_user_base_url}events/">События</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
