<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/user_head2.inc.xslt"/>
	<xsl:include href="include/user_profile.inc.xslt"/>
	<xsl:include href="include/user_projects.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'profile'"/>
	<xsl:variable name="user_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($prefix, '/users/', $current_user/@login, '/')"/>
	<xsl:template match="user_projects"/>
	<xsl:template match="user_profile">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_user_head2"/>
							<h2>Профиль</h2>
							<div class="content">
								<xsl:call-template name="_draw_main_info"/>
								<xsl:call-template name="draw_user_profile_contacts"/>
								<xsl:call-template name="_draw_projects"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_main_info">
		<h3 class="black">
			<xsl:value-of select="$current_user/@any_name"/>
		</h3>
		<xsl:if test="../university_short[@id = current()/@university_id]">
			<div class="kv-info">
				<span class="hl">Вуз: </span>
				<xsl:value-of select="../university_short[@id = current()/@university_id]/@title_full"/>
				<xsl:text> (</xsl:text>
				<xsl:value-of select="../university_short[@id = current()/@university_id]/@title"/>
				<xsl:text>)</xsl:text>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="_draw_projects">
		<h3 class="down">
			<a href="{$current_user_base_url}projects/">Проекты</a>
		</h3>
		<xsl:for-each select="/root/user_projects">
			<xsl:choose>
				<xsl:when test="not(project)">
					<p>Пользователь пока не участвует ни в одном проекте.</p>
				</xsl:when>
				<xsl:otherwise>
					<table class="tbl">
						<xsl:call-template name="draw_user_projects">
							<xsl:with-param name="no_links" select="true()"/>
						</xsl:call-template>
					</table>
					<xsl:if test="pages/@row_count != pages/@to_row">
						<p>
							<xsl:text>+ </xsl:text>
							<a href="{$module_url}projects/">
								<xsl:text>ещё </xsl:text>
								<xsl:value-of select="pages/@row_count - pages/@to_row"/>
								<xsl:call-template name="count_case">
									<xsl:with-param name="number" select="pages/@row_count - pages/@to_row"/>
									<xsl:with-param name="word_ns" select="' проект'"/>
									<xsl:with-param name="word_gs" select="' проекта'"/>
									<xsl:with-param name="word_ap" select="' проектов'"/>
								</xsl:call-template>
							</a>
						</p>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$user_title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/users/">Люди</a>
		</div>
		<div class="level2- selected-">
			<xsl:choose>
				<xsl:when test="$user_section_main_page">
					<xsl:value-of select="$current_user/@any_name"/>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$module_url}">
						<xsl:value-of select="$current_user/@any_name"/>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
