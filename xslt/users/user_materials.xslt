<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/user_head2.inc.xslt"/>
	<xsl:include href="../resources/include/material_head.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'materials'"/>
	<xsl:variable name="materials_url" select="concat($prefix, '/users/', $current_user/@login, '/materials/')"/>
	<xsl:variable name="module_postfix" select="''"/>
	<xsl:variable name="user_module_postfix">
		<xsl:for-each select="/root/user_materials">
			<xsl:if test="@filter = 'must_read'">
				<xsl:value-of select="''"/>
			</xsl:if>
			<xsl:if test="@filter != 'must_read'">
				<xsl:value-of select="@filter"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	<xsl:variable name="module_url" select="concat($materials_url, $user_module_postfix)"/>
	<xsl:variable name="user_section_main_page" select="not($get_vars) and $user_module_postfix = '' and /root/user_materials/pages/@current_page = 1"/>
	<xsl:template match="user_materials">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_user_head2"/>
							<h2>Материалы</h2>
							<div class="content">
								<xsl:call-template name="_draw_list"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<div id="resources-head">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td class="l-">
						<div class="submenu- submenu-show-materials-">
							<span class="header-">Отображать: </span>
							<xsl:if test="@filter = 'must_read'">
								<span class="item- selected-">изучить обязательно</span>
							</xsl:if>
							<xsl:if test="@filter != 'must_read'">
								<span class="item-">
									<a href="{$materials_url}">изучить обязательно</a>
								</span>
							</xsl:if>
							<span class="sep-"/>
							<xsl:if test="@filter = 'read'">
								<span class="item- selected-">изучить по возможности</span>
							</xsl:if>
							<xsl:if test="@filter != 'read'">
								<span class="item-">
									<a href="{$materials_url}read/">изучить по возможности</a>
								</span>
							</xsl:if>
							<span class="sep-"/>
							<xsl:if test="@filter = 'done'">
								<span class="item- selected-">изученное</span>
							</xsl:if>
							<xsl:if test="@filter != 'done'">
								<span class="item-">
									<a href="{$materials_url}done/">изученное</a>
								</span>
							</xsl:if>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<xsl:if test="not(material)">
			<p>Ни одного материала не найдено.</p>
		</xsl:if>
		<xsl:if test="material">
			<xsl:for-each select="material">
				<xsl:variable name="position" select="position()"/>
				<xsl:variable name="material_data" select="/root/material_short[@id = current()/@id]"/>
				<xsl:variable name="material_status" select="."/>
				<xsl:for-each select="/root/material_short[@id = current()/@id]">
					<xsl:call-template name="draw_material_head">
						<xsl:with-param name="draw_table_header" select="$position = 1"/>
						<xsl:with-param name="show_ignore_link" select="false()"/>
						<xsl:with-param name="show_edit_link" select="false()"/>
						<xsl:with-param name="material_data" select="$material_data"/>
						<xsl:with-param name="material_status" select="$material_status"/>
						<xsl:with-param name="project" select="/root/project_short[@id = $material_status/@project_id]"/>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:for-each>
			<xsl:apply-templates select="pages[page]"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/materials_calendar.js"/>
		<script type="text/javascript">
			var resource_name = 'material';
		</script>
		<script type="text/javascript" src="{$prefix}/js/resource_status.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Материалы пользователя </xsl:text>
		<xsl:value-of select="$user_title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/users/">Люди</a>
		</div>
		<div class="level2-">
			<a href="{$prefix}/users/{$current_user/@login}/">
				<xsl:value-of select="$current_user/@any_name"/>
			</a>
		</div>
		<div class="level3- selected-">
			<xsl:choose>
				<xsl:when test="$user_section_main_page">Материалы</xsl:when>
				<xsl:otherwise>
					<a href="{$current_user_base_url}materials/">Материалы</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
