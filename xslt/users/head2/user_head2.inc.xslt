<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="current_user" select="/root/user_short[1]"/>
	<xsl:variable name="current_user_base_url" select="concat($prefix, '/users/', $current_user/@login, '/')"/>
	<xsl:variable name="user_title">
		<xsl:value-of select="$current_user/@any_name"/>
		<xsl:if test="$current_user/@any_name != $current_user/@login">
			<xsl:text> (</xsl:text>
			<xsl:value-of select="$current_user/@login"/>
			<xsl:text>)</xsl:text>
		</xsl:if>
	</xsl:variable>
	<xsl:template name="draw_user_head2">
		<div class="head2">
			<table class="head2-table-">
				<tr>
					<td class="head2-table-left-">
						<!-- avatar-width- - for IE 6-7 -->
						<div class="avatar-width-" style="width: {$current_user/photo_big/@width + 4}px">
							<div class="avatar-">
								<img src="{$current_user/photo_big/@url}" width="{$current_user/photo_big/@width}" height="{$current_user/photo_big/@height}" alt=""/>
							</div>
						</div>
					</td>
					<td class="head2-table-right-">
						<div class="title-">
							<table class="border-">
								<tr>
									<td class="l-"> </td>
									<td class="c-">
										<h1>
											<xsl:value-of select="$user_title"/>
										</h1>
									</td>
									<td class="r-"> </td>
								</tr>
							</table>
						</div>
						<div class="menu-">
							<div class="menu-item-">
								<xsl:if test="$user_section = 'profile'">
									<xsl:attribute name="class">menu-item- menu-item-selected-</xsl:attribute>
								</xsl:if>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="$user_section = 'profile' and $user_section_main_page and not($get_vars)">Профиль</xsl:when>
												<xsl:otherwise>
													<a href="{$current_user_base_url}">Профиль</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
							<div class="menu-sep-">|</div>
							<div class="menu-item-">
								<xsl:if test="$user_section = 'projects'">
									<xsl:attribute name="class">menu-item- menu-item-selected-</xsl:attribute>
								</xsl:if>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="$user_section = 'projects' and $user_section_main_page and not($get_vars)">Проекты</xsl:when>
												<xsl:otherwise>
													<a href="{$current_user_base_url}projects/">Проекты</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
							<div class="menu-sep-">|</div>
							<div class="menu-item-">
								<xsl:if test="$user_section = 'results'">
									<xsl:attribute name="class">menu-item- menu-item-selected-</xsl:attribute>
								</xsl:if>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="$user_section = 'results' and $user_section_main_page and not($get_vars)">Результаты</xsl:when>
												<xsl:otherwise>
													<a href="{$current_user_base_url}results/">Результаты</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
							<div class="menu-sep-">|</div>
							<div class="menu-item-">
								<xsl:if test="$user_section = 'events'">
									<xsl:attribute name="class">menu-item- menu-item-selected-</xsl:attribute>
								</xsl:if>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="$user_section = 'events' and $user_section_main_page and not($get_vars)">События</xsl:when>
												<xsl:otherwise>
													<a href="{$current_user_base_url}events/">События</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
							<div class="menu-sep-">|</div>
							<div class="menu-item-">
								<xsl:if test="$user_section = 'materials'">
									<xsl:attribute name="class">menu-item- menu-item-selected-</xsl:attribute>
								</xsl:if>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="$user_section = 'materials' and $user_section_main_page and not($get_vars)">Материалы</xsl:when>
												<xsl:otherwise>
													<a href="{$current_user_base_url}materials/">Материалы</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
