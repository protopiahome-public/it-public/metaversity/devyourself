<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="menu/user_results_submenu.inc.xslt"/>
	<xsl:include href="head2/user_head2.inc.xslt"/>
	<xsl:include href="search/user_results_search.inc.xslt"/>
	<xsl:include href="../_core/auxil/paging.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'results'"/>
	<xsl:variable name="user_section_main_page" select="false()"/>
	<xsl:variable name="module_url">
		<xsl:value-of select="concat($current_user_base_url, 'results/')"/>
		<xsl:choose>
			<xsl:when test="/root/user_results_professiograms/@competence_set_id = 0">profs/</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat('set-', /root/user_results_professiograms/@competence_set_id, '/')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="user_results_professiograms">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_user_head2"/>
							<h2>Результаты</h2>
							<div class="content">
								<xsl:call-template name="draw_user_results_submenu"/>
								<xsl:call-template name="_draw_box"/>
								<xsl:call-template name="_draw_tables"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
							<xsl:call-template name="draw_user_results_search"/>
							<xsl:if test="@competence_set_id != 0">
								<xsl:call-template name="draw_filter">
									<xsl:with-param name="competence_set_id" select="@competence_set_id"/>
								</xsl:call-template>
							</xsl:if>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_box">
		<xsl:variable name="competence_set_id" select="@competence_set_id"/>
		<xsl:if test="$competence_set_id = 0">
			<div class="box">
				<div class="content-">
					<strong>Профессии видны сразу.</strong> Но здесь мы выводим мало профессий, чтобы снизить нагрузку на сервер. Чтобы быстрее найти нужную профессию, выбирайте сразу набор компетенций, либо используйте поиск (справа).
				</div>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="_draw_tables">
		<xsl:variable name="module" select="."/>
		<xsl:variable name="competence_set_id" select="@competence_set_id"/>
		<xsl:if test="$competence_set_id = 0">
			<xsl:call-template name="_professiograms_draw_info">
				<xsl:with-param name="competence_set_id" select="$competence_set_id"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:for-each select="competence_sets/competence_set">
			<xsl:variable name="competence_set_position" select="position()"/>
			<xsl:for-each select="/root/user_competence_sets/competence_set[@id = current()/@id]">
				<h3 title="Набор компетенций">
					<xsl:if test="$competence_set_position != 1">
						<xsl:attribute name="class">down</xsl:attribute>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="$competence_set_id = 0">
							<a href="{$current_user_base_url}results/set-{@id}/">
								<xsl:value-of select="@title"/>
							</a>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="@title"/>
						</xsl:otherwise>
					</xsl:choose>
				</h3>
				<p class="note">
					<span class="with-hint">
						<xsl:call-template name="draw_mark_count_hint">
							<xsl:with-param name="position" select="1"/>
							<xsl:with-param name="with_trans" select="true()"/>
						</xsl:call-template>
					</span>
					<xsl:text> (</xsl:text>
					<span class="with-hint">
						<xsl:call-template name="draw_mark_count_hint">
							<xsl:with-param name="position" select="0"/>
						</xsl:call-template>
					</span>
					<xsl:text> без учёта переводчиков), </xsl:text>
					<xsl:value-of select="@professiogram_count_calc"/>
					<xsl:call-template name="count_case">
						<xsl:with-param name="number" select="@professiogram_count_calc"/>
						<xsl:with-param name="word_ns" select="' профессия'"/>
						<xsl:with-param name="word_gs" select="' профессии'"/>
						<xsl:with-param name="word_ap" select="' профессий'"/>
					</xsl:call-template>
					<xsl:text>.</xsl:text>
				</p>
				<xsl:if test="$competence_set_id != 0">
					<xsl:call-template name="_professiograms_draw_info">
						<xsl:with-param name="module" select="$module"/>
						<xsl:with-param name="competence_set_id" select="$competence_set_id"/>
					</xsl:call-template>
				</xsl:if>
				<xsl:if test="$module/professiogram[@competence_set_id = current()/@id]">
					<table class="tbl">
						<xsl:for-each select="$module">
							<tr>
								<xsl:call-template name="draw_tbl_th">
									<xsl:with-param name="name" select="'title'"/>
									<xsl:with-param name="title" select="'Профессия'"/>
									<xsl:with-param name="no_links" select="true()"/>
								</xsl:call-template>
								<xsl:call-template name="draw_tbl_th">
									<xsl:with-param name="name" select="'match'"/>
									<xsl:with-param name="title" select="'Соответствие'"/>
									<xsl:with-param name="no_links" select="true()"/>
									<xsl:with-param name="center" select="true()"/>
								</xsl:call-template>
								<th>
									<span>Детально</span>
								</th>
							</tr>
						</xsl:for-each>
						<xsl:for-each select="$module/professiogram[@competence_set_id = current()/@id]">
							<tr>
								<td class="col-bullet- col-longer-">
									<span class="bullet-">
										<a href="{$current_user_base_url}results/prof-{@id}/{$request/@query_string}">
											<xsl:value-of select="@title"/>
										</a>
									</span>
								</td>
								<td class="col-digits-">
									<xsl:value-of select="@match"/>
									<xsl:text>%</xsl:text>
								</td>
								<td class="col-icons- icons">
									<a class="plot-rose- gray-" title="Соответствие профессии (в виде &#171;розочки&#187;)" href="{$current_user_base_url}results/prof-{@id}/{$request/@query_string}"/>
									<span class="sep-"/>
									<a class="plot-bars- gray-" title="Соответствие профессии (подробно, в виде столбчатой диаграммы)" href="{$current_user_base_url}results/prof-{@id}/detailed/{$request/@query_string}"/>
									<span class="sep-"/>
									<a class="rating- gray-" title="Рейтинг профессии" href="{$prefix}/ratings/prof-{@id}/#user-{$current_user/@login}" onclick="$.cookie('user_to_highlight', '{$current_user/@login}', {{path: '{$prefix}/'}})"/>
								</td>
							</tr>
						</xsl:for-each>
					</table>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:apply-templates select="pages[page]"/>
	</xsl:template>
	<xsl:template name="_professiograms_draw_info">
		<xsl:param name="module" select="."/>
		<xsl:param name="competence_set_id"/>
		<xsl:if test="$module/filter/field[@name = 'search'] != ''">
			<p>
				<xsl:text>Результаты поиска по запросу &#171;</xsl:text>
				<strong>
					<xsl:value-of select="$module/filter/field[@name = 'search']"/>
				</strong>
				<xsl:text>&#187;.</xsl:text>
			</p>
		</xsl:if>
		<xsl:if test="not($module/professiogram)">
			<xsl:choose>
				<xsl:when test="$module/filter/field[@name = 'search'] != ''">
					<p>По заданным критериям поиска ни одной профессии не найдено.</p>
				</xsl:when>
				<xsl:otherwise>
					<p>В данном наборе компетенций нет ни одной профессии.</p>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$module/professiogram">
			<p>
				<xsl:text>Отображаются профессии </xsl:text>
				<xsl:value-of select="$module/pages/@from_row"/>
				<xsl:text> &#8212; </xsl:text>
				<xsl:value-of select="$module/pages/@to_row"/>
				<xsl:text> из </xsl:text>
				<xsl:value-of select="$module/pages/@row_count"/>
				<xsl:text>.</xsl:text>
			</p>
			<xsl:if test="$competence_set_id != 0">
				<p>
					<xsl:text> См. также </xsl:text>
					<a href="{$current_user_base_url}results/set-{@id}/full/">полную компетенционную карту</a>
					<xsl:text>.</xsl:text>
				</p>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:choose>
			<xsl:when test="/root/user_results_professiograms/@competence_set_id = 0">Результаты (развёрнуто)</xsl:when>
			<xsl:otherwise>
				<xsl:text>Результаты: </xsl:text>
				<xsl:value-of select="/root/user_competence_sets/competence_set[@id = /root/user_results_professiograms/@competence_set_id]/@title"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$user_title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/users/">Люди</a>
		</div>
		<div class="level2-">
			<a href="{$prefix}/users/{$current_user/@login}/">
				<xsl:value-of select="$current_user/@any_name"/>
			</a>
		</div>
		<div class="level3-">
			<a href="{$current_user_base_url}results/">Результаты</a>
		</div>
		<div class="level4- selected-">
			<xsl:choose>
				<xsl:when test="/root/user_results_professiograms/@competence_set_id = 0">Развёрнуто</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="/root/user_competence_sets/competence_set[@id = /root/user_results_professiograms/@competence_set_id]/@title"/>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
