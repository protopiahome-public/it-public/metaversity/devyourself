<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_users_admin_search">
		<div class="box" style="float: left; margin-bottom: 10px;">
			<div class="content-">
				<xsl:variable name="module_filters" select="filter"/>
				<form id="search-form" class="search-form" action="{$module_url}" method="get">
					<xsl:for-each select="$get_vars">
						<xsl:if test="not(substring(@name, 1, 6) = 'filter')">
							<xsl:call-template name="get_param_for_form"/>
						</xsl:if>
					</xsl:for-each>
					<div class="line-">
						<div class="el- el-text-">
							<input type="text" id="search_search" name="filter[search]" value="{filters/filter[@name = 'search']/@value}" maxlength="255">
								<xsl:attribute name="jq-placeholder">
									<xsl:text>логин/имя</xsl:text>
									<xsl:if test="$current_project/@show_user_numbers = 1"> или номер (#1234)</xsl:if>
								</xsl:attribute>
							</input>
						</div>
						<div class="el- el-btn-">
							<button class="button button-small">Искать</button>
						</div>
					</div>
				</form>
				<div class="clear"/>
			</div>
		</div>
		<div class="clear"/>
	</xsl:template>
</xsl:stylesheet>
