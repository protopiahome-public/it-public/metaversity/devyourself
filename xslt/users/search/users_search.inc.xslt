<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_users_search">
		<div class="box">
			<div class="content-">
				<form id="search-form" class="search-form search-form-w100p" action="{$module_url}" method="get">
					<xsl:for-each select="$get_vars">
						<xsl:if test="not(substring(@name, 1, 6) = 'filter')">
							<xsl:call-template name="get_param_for_form"/>
						</xsl:if>
					</xsl:for-each>
					<div class="line-">
						<div class="el- el-text-">
							<input type="text" id="search_search" name="filter[search]" jq-placeholder="{filters/filter[@name = 'search']/@title}" value="" maxlength="255">
								<xsl:if test="filters/filter[@name = 'search']/@value != ''">
									<xsl:attribute name="value">
										<xsl:value-of select="filters/filter[@name = 'search']/@value"/>
									</xsl:attribute>
								</xsl:if>
							</input>
						</div>
					</div>
					<xsl:if test="filters/filter[@name = 'project']">
						<div class="line-">
							<div class="el- el-select-">
								<select name="filter[project]" onchange="$(this).parents('form').submit();">
									<option value="0">&#8212; проект &#8212;</option>
									<xsl:for-each select="filters/filter[@name = 'project']/options/option">
										<option value="{@id}">
											<xsl:if test="@id = ../../@value">
												<xsl:attribute name="selected">selected</xsl:attribute>
											</xsl:if>
											<xsl:value-of select="@title"/>
										</option>
									</xsl:for-each>
								</select>
							</div>
						</div>
					</xsl:if>
					<xsl:if test="filters/filter[@name = 'group']">
						<div class="line-">
							<div class="el- el-select-">
								<select name="filter[group]" onchange="$(this).parents('form').submit();">
									<option value="0">&#8212; группа &#8212;</option>
									<xsl:for-each select="filters/filter[@name = 'group']/options/option">
										<option value="{@id}">
											<xsl:if test="@id = ../../@value">
												<xsl:attribute name="selected">selected</xsl:attribute>
											</xsl:if>
											<xsl:value-of select="@title"/>
										</option>
									</xsl:for-each>
								</select>
							</div>
						</div>
					</xsl:if>
					<div class="line-">
						<div class="el- el-btn-">
							<button class="button button-small">Искать</button>
						</div>
					</div>
				</form>
				<div class="clear"/>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
