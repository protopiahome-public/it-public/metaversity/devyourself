<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_user_results_professiogram_match_percent">
		<div class="box box-match-">
			<div class="content-">
				<xsl:text>Соответствие профессии &#8212; </xsl:text>
				<xsl:value-of select="@match"/>
				<xsl:text>%</xsl:text>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="draw_user_results_project_comparison">
		<xsl:if test="project_comparison">
			<div class="widget-wrap">
				<div class="widget widget-project-comparison">
					<div class="header-">
						<table cellspacing="0">
							<tr>
								<td>
									<h2 class="text-">Что отображать</h2>
								</td>
							</tr>
						</table>
					</div>
					<div class="content-">
						<div class="project-comparison" id="project-cmp">
							<xsl:if test="results/competence">
								<xsl:if test="name() = 'project_vector_focus' or name() = 'project_user_vector' and @output_type = 'detailed'">
									<div class="item-">
										<strong>Показывать дефицит:</strong>
									</div>
									<div class="item-">
										<input type="radio" id="project-cmp-rb-calc-base-results" class="project-cmp-rb-calc-base" name="project-cmp-rb-calc-base" value="results">
											<xsl:if test="project_comparison/@calculations_base = 'results'">
												<xsl:attribute name="checked">checked</xsl:attribute>
											</xsl:if>
										</input>
										<label for="project-cmp-rb-calc-base-results">
											<strong>1) </strong>
											<xsl:text>как разницу между необходимым уровнем </xsl:text>
											<img src="{$prefix}/img/pixel.gif" class="legend-bar legend-bar-bars-need"/>
											<xsl:text> и результатом c проектов </xsl:text>
											<img src="{$prefix}/img/pixel.gif" class="legend-bar legend-bar-bars-sum"/>
										</label>
									</div>
									<div class="item-">
										<input type="radio" id="project-cmp-rb-calc-base-self" class="project-cmp-rb-calc-base" name="project-cmp-rb-calc-base" value="self">
											<xsl:if test="project_comparison/@calculations_base = 'self'">
												<xsl:attribute name="checked">checked</xsl:attribute>
											</xsl:if>
										</input>
										<label for="project-cmp-rb-calc-base-self">
											<strong>2) </strong>
											<xsl:text>как разницу между необходимым уровнем </xsl:text>
											<img src="{$prefix}/img/pixel.gif" class="legend-bar legend-bar-bars-need"/>
											<xsl:text> и самооценкой </xsl:text>
											<img src="{$prefix}/img/pixel.gif" class="legend-bar legend-bar-bars-self"/>
										</label>
									</div>
									<xsl:if test="name() != 'project_vector_focus'">
										<div class="item-">
											<xsl:text>(При заполнении фокуса Вы использовали вариант </xsl:text>
											<strong>
												<xsl:choose>
													<xsl:when test="project_comparison/@calculations_base = 'results'">1</xsl:when>
													<xsl:when test="project_comparison/@calculations_base = 'self'">2</xsl:when>
												</xsl:choose>
											</strong>
											<xsl:text>)</xsl:text>
										</div>
									</xsl:if>
									<hr/>
								</xsl:if>
								<xsl:if test="project_comparison/project[2]">
									<div class="item-">
										<input type="radio" id="project-cmp-rb-show-all" name="project-cmp-rb-show" checked="checked" value="1"/>
										<label for="project-cmp-rb-show-all">Лучший результат</label>
									</div>
									<div class="item-">
										<input type="radio" id="project-cmp-rb-show-projects" name="project-cmp-rb-show" value="1"/>
										<label for="project-cmp-rb-show-projects">Проекты по отдельности</label>
									</div>
									<xsl:for-each select="project_comparison/project">
										<xsl:if test="position() &lt; 6">
											<div class="item- item-level-2-">
												<input class="project-cmp-cb-project" type="checkbox" id="project-cmp-cb-project-{@key}" name="project-cmp-cb-project" value="{@key}" disabled="disabled"/>
												<label id="project-cmp-cb-project-{@key}-label" for="project-cmp-cb-project-{@key}">
													<xsl:value-of select="/root/filter/competence_sets/competence_set[@id=current()/@competence_set_id]/projects/project[@id=current()/@id ]/@title"/>
													<xsl:text> </xsl:text>
													<img src="{$prefix}/img/pixel.gif" id="project-cmp-legend-{@key}" class="legend-bar"/>
												</label>
											</div>
										</xsl:if>
									</xsl:for-each>
									<hr/>
								</xsl:if>
								<xsl:if test="/root/user_results_competence_set_full">
									<div class="item-">
										<strong>Профиль &#171;Я в будущем&#187;</strong>
									</div>
									<xsl:for-each select="/root/vectors_data/future/vector">
										<div class="item- item-with-comment-">
											<input type="radio" id="project-cmp-rb-vector-{@id}" class="project-cmp-rb-vector" name="project-cmp-rb-vector" value="{@id}">
												<xsl:if test="position() = 1">
													<xsl:attribute name="checked">checked</xsl:attribute>
												</xsl:if>
												<xsl:if test="last() = 1">
													<xsl:attribute name="disabled">disabled</xsl:attribute>
												</xsl:if>
											</input>
											<label for="project-cmp-rb-vector-{@id}">
												<xsl:value-of select="/root/project_short[@id = current()/@project_id]/@title"/>
											</label>
										</div>
										<div class="comment-">
											<xsl:call-template name="get_full_date">
												<xsl:with-param name="datetime" select="@future_edit_time"/>
											</xsl:call-template>
										</div>
									</xsl:for-each>
									<hr/>
								</xsl:if>
							</xsl:if>
							<div class="item-">
								<input type="checkbox" id="project-cmp-show-self" name="project-cmp-show-self" value="1">
									<xsl:choose>
										<xsl:when test="project_comparison/@calculations_base = 'self'">
											<xsl:attribute name="checked">checked</xsl:attribute>
											<xsl:if test="@output_type = 'detailed'">
												<xsl:attribute name="disabled">disabled</xsl:attribute>
											</xsl:if>
										</xsl:when>
										<xsl:when test="project_comparison/@show_self_by_default = 1">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:when>
									</xsl:choose>
								</input>
								<label for="project-cmp-show-self">Показать самооценку</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
