<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_user_projects">
		<xsl:param name="no_links" select="false()"/>
		<tr>
			<th/>
			<xsl:call-template name="draw_tbl_th">
				<xsl:with-param name="name" select="'date'"/>
				<xsl:with-param name="title" select="'Дата&#160;начала'"/>
				<xsl:with-param name="no_links" select="$no_links"/>
			</xsl:call-template>
			<xsl:call-template name="draw_tbl_th">
				<xsl:with-param name="name" select="'marks'"/>
				<xsl:with-param name="title" select="'Оценок'"/>
				<xsl:with-param name="no_links" select="$no_links"/>
				<xsl:with-param name="center" select="true()"/>
			</xsl:call-template>
			<th>
				<span>Ссылки</span>
			</th>
			<th>
				<span>Наборы&#160;компетенций</span>
			</th>
			<xsl:call-template name="draw_tbl_th">
				<xsl:with-param name="name" select="'date_add'"/>
				<xsl:with-param name="title">
					<xsl:choose>
						<xsl:when test="$current_user/@sex = 'f'">Присоединилась</xsl:when>
						<xsl:otherwise>Присоединился</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="no_links" select="$no_links"/>
			</xsl:call-template>
		</tr>
		<xsl:for-each select="project">
			<xsl:variable name="project_id" select="@id"/>
			<xsl:variable name="position" select="position()"/>
			<tr>
				<xsl:for-each select="/root/project_short[@id = current()/@id]">
					<td class="col-logo-">
						<a href="{@url}">
							<img src="{logo_small/@url}" width="{logo_small/@width}" height="{logo_small/@height}" alt="{@title}" title=""/>
						</a>
					</td>
					<td class="col-project-title-">
						<div class="title-">
							<a href="{@url}">
								<xsl:value-of select="@title"/>
							</a>
						</div>
						<div class="dates-">
							<xsl:call-template name="get_full_date">
								<xsl:with-param name="datetime" select="@start_time"/>
							</xsl:call-template>
							<xsl:if test="substring(@start_time, 1, 10) != substring(@finish_time, 1, 10)">
								<xsl:text> &#8212; </xsl:text>
								<xsl:choose>
									<xsl:when test="@finish_time != ''">
										<xsl:call-template name="get_full_date">
											<xsl:with-param name="datetime" select="@finish_time"/>
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>...</xsl:otherwise>
								</xsl:choose>
							</xsl:if>
						</div>
						<div class="state-">
							<xsl:call-template name="draw_project_state">
								<xsl:with-param name="state" select="@state_calc"/>
							</xsl:call-template>
						</div>
					</td>
				</xsl:for-each>
				<td class="col-digits-">
					<span class="with-hint">
						<xsl:call-template name="draw_mark_count_hint"/>
					</span>
				</td>
				<td class="col-links-">
					<a href="{/root/project_short[@id = current()/@id]/@url}users/{$current_user/@login}/">профиль</a>
					<br/>
					<xsl:choose>
						<xsl:when test="/root/project_short[@id = current()/@id]/@vector_is_on = 1">
							<a href="{/root/project_short[@id = current()/@id]/@url}users/{$current_user/@login}/vector/">
								<xsl:choose>
									<xsl:when test="/root/vectors/vector[@project_id = current()/@id]">
										<xsl:text>вектор</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="class">disabled</xsl:attribute>
										<xsl:attribute name="title">Не пройден</xsl:attribute>
										<xsl:text>вектор</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</a>
						</xsl:when>
					</xsl:choose>
				</td>
				<td class="col-list-">
					<xsl:if test="/root/user_competence_set_project_links/project[@id = current()/@id]/competence_set">
						<ul class="small">
							<xsl:for-each select="/root/user_competence_set_project_links/project[@id = current()/@id]/competence_set">
								<li>
									<xsl:value-of select="@title"/>
									<xsl:text> (</xsl:text>
									<span class="with-hint">
										<xsl:call-template name="draw_mark_count_hint">
											<xsl:with-param name="position" select="position()"/>
										</xsl:call-template>
									</span>
									<xsl:text>)</xsl:text>
								</li>
							</xsl:for-each>
						</ul>
					</xsl:if>
				</td>
				<td class="col-date-">
					<xsl:call-template name="get_full_date">
						<xsl:with-param name="datetime" select="@add_time"/>
					</xsl:call-template>
				</td>
			</tr>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
