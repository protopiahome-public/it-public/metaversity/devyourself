<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_user_profile_contacts">
		<xsl:for-each select="/root/user_profile/contacts[1]">
			<xsl:if test="contact">
				<h3 class="down">Как связаться</h3>
				<xsl:variable name="contact_half_index" select="round(count(contact) div 2)"/>
				<table class="kv-info-2-columns">
					<tr>
						<td>
							<xsl:for-each select="contact[position() &lt;= $contact_half_index]">
								<xsl:call-template name="draw_user_profile_contact"/>
							</xsl:for-each>
						</td>
						<td>
							<xsl:for-each select="contact[position() &gt; $contact_half_index]">
								<xsl:call-template name="draw_user_profile_contact"/>
							</xsl:for-each>
						</td>
					</tr>
				</table>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="draw_user_profile_contact">
		<div class="kv-info">
			<span class="hl">
				<xsl:choose>
					<xsl:when test="@type = 'email'">Email</xsl:when>
					<xsl:when test="@type = 'phone'">Phone</xsl:when>
					<xsl:when test="@type = 'skype'">Skype</xsl:when>
					<xsl:when test="@type = 'icq'">ICQ</xsl:when>
					<xsl:when test="@type = 'www'">Web</xsl:when>
				</xsl:choose>
				<xsl:text>: </xsl:text>
			</span>
			<xsl:choose>
				<xsl:when test="@type = 'email'">
					<a class="email" href="mailto:{@obscured}">
						<xsl:value-of select="@obscured"/>
					</a>
				</xsl:when>
				<xsl:when test="@type = 'phone'">
					<xsl:value-of select="."/>
				</xsl:when>
				<xsl:when test="@type = 'skype'">
					<xsl:value-of select="."/>
					<xsl:text> (</xsl:text>
					<a href="skype:{.}?call">Вызов</a>
					<xsl:text>)</xsl:text>
				</xsl:when>
				<xsl:when test="@type = 'icq'">
					<a href="http://www.icq.com/people/about_me.php?uin={.}">
						<xsl:value-of select="."/>
					</a>
				</xsl:when>
				<xsl:when test="@type = 'www'">
					<a href="{.}">
						<xsl:value-of select="@url_human"/>
					</a>
				</xsl:when>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
