<?xml version="1.0" encoding="UTF-8"?>
<!-- todelete -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_user_results_competence_set_submenu">
		<div class="submenu">
			<span class="inner-">
				<span class="item-">
					<xsl:if test="/root/user_results_competence_set_full">
						<xsl:attribute name="class">selected-</xsl:attribute>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="/root/user_results_competence_set_full and not($get_vars)">Полная компетенционная карта</xsl:when>
						<xsl:otherwise>
							<a href="{$current_user_base_url}results/set-{/root/competence_set_short[1]/@id}/full/{$request/@query_string}">Полная компетенционная карта</a>
						</xsl:otherwise>
					</xsl:choose>
				</span>
				<span class="sep-"> | </span>
				<span class="item-">
					<xsl:if test="/root/user_results_competence_set_future">
						<xsl:attribute name="class">selected-</xsl:attribute>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="/root/user_results_competence_set_future and not($get_vars)">&#171;Я в будущем&#187;</xsl:when>
						<xsl:otherwise>
							<a href="{$current_user_base_url}results/set-{/root/competence_set_short[1]/@id}/future/{$request/@query_string}">&#171;Я в будущем&#187;</a>
						</xsl:otherwise>
					</xsl:choose>
				</span>
				<span class="sep-"> | </span>
				<span class="item-">
					<xsl:if test="/root/user_results_competence_set_focus">
						<xsl:attribute name="class">selected-</xsl:attribute>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="/root/user_results_competence_set_focus and not($get_vars)">Фокус</xsl:when>
						<xsl:otherwise>
							<a href="{$current_user_base_url}results/set-{/root/competence_set_short[1]/@id}/focus/{$request/@query_string}">Фокус</a>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</span>
		</div>
	</xsl:template>
</xsl:stylesheet>
