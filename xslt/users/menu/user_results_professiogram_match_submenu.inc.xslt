<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_user_results_professiogram_match_submenu">
		<div class="submenu">
			<span class="inner-">
				<xsl:for-each select="/root/user_results_professiogram_match[1]">
					<span class="item-">
						<xsl:if test="@output_type = 'rose'">
							<xsl:attribute name="class">selected-</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="@output_type = 'rose' and not($get_vars)">&#171;Розочкой&#187;</xsl:when>
							<xsl:otherwise>
								<a href="{$current_user_base_url}results/prof-{@professiogram_id}/{$request/@query_string}">&#171;Розочкой&#187;</a>
							</xsl:otherwise>
						</xsl:choose>
					</span>
					<span class="sep-"> | </span>
					<span class="item-">
						<xsl:if test="@output_type = 'detailed'">
							<xsl:attribute name="class">selected-</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="@output_type = 'detailed' and not($get_vars)">Подробно</xsl:when>
							<xsl:otherwise>
								<a href="{$current_user_base_url}results/prof-{@professiogram_id}/detailed/{$request/@query_string}">Подробно</a>
							</xsl:otherwise>
						</xsl:choose>
					</span>
				</xsl:for-each>
			</span>
		</div>
	</xsl:template>
</xsl:stylesheet>
