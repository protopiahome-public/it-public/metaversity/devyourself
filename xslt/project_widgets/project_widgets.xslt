<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="../base_widgets/include/widget_base.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'main'"/>
	<xsl:variable name="project_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="$current_project/@url"/>
	<xsl:template match="project_widgets">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap columns-wrap-widgets">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns columns-widgets">
					<tr>
						<td class="left-column narrow-column">
							<xsl:call-template name="_draw_widgets">
								<xsl:with-param name="column" select="1"/>
							</xsl:call-template>
						</td>
						<td class="center-column wide-column">
							<xsl:call-template name="_draw_widgets">
								<xsl:with-param name="column" select="2"/>
							</xsl:call-template>
						</td>
						<td class="right-column narrow-column">
							<xsl:call-template name="_draw_widgets">
								<xsl:with-param name="column" select="3"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_widgets">
		<xsl:param name="column"/>
		<xsl:if test="$project_access/@can_moderate_widgets = 1">
			<div class="jq-widget-add widget widget-add dn" jq-column="{$column}">
				<div class="content- ico-links-small">
					<a class="add-" href="#">
						<span class="clickable">Добавить виджет</span>
					</a>
				</div>
			</div>
		</xsl:if>
		<div class="jq-widgets">
			<div class="jq-widgets-inner widgets-inner-" id="jq-widgets-{$column}">
				<xsl:for-each select="/root/project_widgets/widget[@column = $column]">
					<div id="widget-{@id}" jq-widget-id="{@id}" class="jq-widget">
						<xsl:apply-templates mode="widget_base" select=".">
							<xsl:with-param name="can_moderate_widgets" select="$project_access/@can_moderate_widgets = 1"/>
						</xsl:apply-templates>
					</div>
				</xsl:for-each>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<xsl:if test="$project_access/@can_moderate_widgets = 1">
			<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
			<script type="text/javascript" src="{$local_prefix}/editors/tiny_mce/tiny_mce.js"/>
			<script type="text/javascript" src="{$prefix}/js/widgets.js"/>
			<script type="text/javascript" src="{$prefix}/js/widgets_edit.js"/>
			<script type="text/javascript" src="{$prefix}/js/events_calendar.js"/>
			<script type="text/javascript">
				var widget_container_type = 'project';
				var post_vars = {
					project_id : '<xsl:value-of select="$current_project/@id"/>'
				};
				$('head').data('calendar_no_list', 52);
			</script>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
