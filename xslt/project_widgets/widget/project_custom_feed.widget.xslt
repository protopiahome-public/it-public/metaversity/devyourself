<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="../../base_widgets/widget/base_custom_feed.widget.xslt"/>
	<xsl:template match="project_widget_custom_feed" mode="widget">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<xsl:call-template name="base_widget_custom_feed">
			<xsl:with-param name="data" select="$data"/>
			<xsl:with-param name="can_moderate_widgets" select="$can_moderate_widgets"/>
			<xsl:with-param name="current_feed" select="/root/project_custom_feed_full[@id = current()/@project_custom_feed_id][1]"/>
			<xsl:with-param name="current_feed_posts" select="/root/project_custom_feed[@feed_id = current()/@project_custom_feed_id][1]"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>
