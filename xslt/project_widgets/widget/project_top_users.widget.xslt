<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="../../base_widgets/widget/base_top_users.widget.xslt"/>
	<xsl:template match="project_widget_top_users[@by_comments = 0]" mode="widget">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<xsl:call-template name="base_widget_top_users">
			<xsl:with-param name="data" select="$data"/>
			<xsl:with-param name="current_container" select="$current_project"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>
