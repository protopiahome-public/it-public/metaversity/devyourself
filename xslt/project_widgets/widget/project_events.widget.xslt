<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="events"/>
	<xsl:template match="project_widget_events" mode="widget">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<div class="widget widget-events">
			<xsl:call-template name="draw_widget_header">
				<xsl:with-param name="title" select="$data/@title"/>
				<xsl:with-param name="light_header" select="true()"/>
			</xsl:call-template>
			<xsl:choose>
				<xsl:when test="@module_disabled = 1">
					<div class="content-message- content-message-access-">
						<p class="text-">События в данном проекте отключены.</p>
					</div>
				</xsl:when>
				<xsl:when test="@module_no_access = 1">
					<!-- @dm9 Блок с сообщением об ограниченном доступе-->
					<div class="content-message- content-message-access-">
						<xsl:call-template name="draw_access">
							<xsl:with-param name="status" select="$project_access/@access_events_read"/>
							<xsl:with-param name="entity_genitiv" select="'проекта'"/>
							<xsl:with-param name="join_premoderation" select="$project_access/@join_premoderation = 1"/>
							<xsl:with-param name="for">Доступ к событиям ограничен. Для получения доступа</xsl:with-param>
							<xsl:with-param name="draw_instruction" select="true()"/>
						</xsl:call-template>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<xsl:for-each select="/root/events[1]">
						<xsl:choose>
							<xsl:when test="not(event)">
								<div class="content-message- content-message-empty-">
									<p class="text-">Ни одного события не найдено.</p>
								</div>
							</xsl:when>
							<xsl:otherwise>
								<div class="content-">
									<xsl:for-each select="event">
										<xsl:variable name="position" select="position()"/>
										<xsl:variable name="event_status" select="."/>
										<xsl:variable name="project" select="/root/project_short[@id = $event_status/@reflex_project_id]"/>
										<xsl:for-each select="/root/event_short[@id = current()/@id]">
											<xsl:variable name="current_event_url">
												<xsl:choose>
													<xsl:when test="not($project)">
														<xsl:value-of select="concat($current_project/@url, 'events/', @id, '/')"/>
													</xsl:when>
													<xsl:when test="$event_status and $event_status/@reflex_id &gt; 0">
														<xsl:value-of select="concat($project/@url, 'events/', $event_status/@reflex_id, '/')"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="concat($project/@url, 'events/', @id, '/')"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<div class="item-">
												<table class="table-">
													<tr class="resource-head">
														<td class="col-datetime-">
															<div class="sep-">
																<div class="date-">
																	<span class="day-">
																		<xsl:value-of select="substring(@start_time, 9, 2)"/>
																	</span>
																	<span class="month-">
																		<xsl:call-template name="get_month_name">
																			<xsl:with-param name="month_number" select="substring(@start_time, 6, 2)"/>
																			<xsl:with-param name="type" select="'short_small'"/>
																		</xsl:call-template>
																	</span>
																</div>
																<div class="week-day-weekend-">
																	<xsl:variable name="week_day_number" select="substring(@start_time, 21, 1)"/>
																	<xsl:attribute name="class">
																		<xsl:text>week-day-</xsl:text>
																		<xsl:if test="$week_day_number = 0 or $week_day_number = 6 or $week_day_number = 7"> week-day-weekend-</xsl:if>
																	</xsl:attribute>
																	<xsl:call-template name="get_week_day_name">
																		<xsl:with-param name="week_day_number" select="$week_day_number"/>
																		<xsl:with-param name="type" select="'short_small'"/>
																	</xsl:call-template>
																</div>
																<div class="time-">
																	<xsl:value-of select="substring(@start_time, 12, 5)"/>
																</div>
															</div>
														</td>
														<td class="col-main-">
															<div class="title-">
																<xsl:if test="$project">
																	<span class="project-">
																		<a href="{$project/@url}">
																			<xsl:value-of select="$project/@title"/>
																		</a>
																		<xsl:text> / </xsl:text>
																	</span>
																</xsl:if>
																<a href="{$current_event_url}">
																	<xsl:value-of select="@title"/>
																</a>
															</div>
															<div class="gray-info-">
																<xsl:call-template name="get_full_datetime_range">
																	<xsl:with-param name="start_datetime" select="@start_time"/>
																	<xsl:with-param name="finish_datetime" select="@finish_time"/>
																</xsl:call-template>
																<xsl:if test="@place != ''">
																	<xsl:text> @ </xsl:text>
																	<xsl:value-of select="@place"/>
																</xsl:if>
															</div>
														</td>
													</tr>
												</table>
												<div class="comments-">
													<span>
														<xsl:attribute name="class">text- small
															<xsl:if test="$position = 1"> first-</xsl:if>
														</xsl:attribute>
														<xsl:text>Комментарии: </xsl:text>
													</span>
													<a href="{$current_event_url}/#comments">
														<xsl:attribute name="class">bigger 
															<xsl:choose>
																<xsl:when test="@comment_count_calc > 0"> bold</xsl:when>
															</xsl:choose>
														</xsl:attribute>
														<xsl:value-of select="@comment_count_calc"/>
													</a>
												</div>
											</div>
										</xsl:for-each>
									</xsl:for-each>
								</div>
								<div class="footer-">
									<a href="{$current_project/@url}events/">Все события</a>
								</div>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
