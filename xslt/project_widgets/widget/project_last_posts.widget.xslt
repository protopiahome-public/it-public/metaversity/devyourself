<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="../../base_widgets/widget/base_last_posts.widget.xslt"/>
	<xsl:template match="project_widget_last_posts" mode="widget">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<xsl:call-template name="base_widget_last_posts">
			<xsl:with-param name="data" select="$data"/>
			<xsl:with-param name="feed_url" select="concat($current_project/@url, 'co/feed/')"/>
			<xsl:with-param name="type" select="'project'"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>
