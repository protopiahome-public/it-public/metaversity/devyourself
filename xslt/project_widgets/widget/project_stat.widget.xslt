<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="project_widget_stat" mode="widget">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<div class="widget widget-stat">
			<xsl:call-template name="draw_widget_header">
				<xsl:with-param name="title" select="$data/@title"/>
				<xsl:with-param name="light_header" select="false()"/>
			</xsl:call-template>
			<div class="content-">
				<p>
					<span class="text-">Людей:</span>
					<a href="{$current_project/@url}users/">
						<xsl:value-of select="@user_count_calc"/>
					</a>
				</p>
				<xsl:if test="$current_project/@communities_are_on = 1">
					<p>
						<span class="text-">Сообществ:</span>
						<a href="{$current_project/@url}co/">
							<xsl:value-of select="@community_count_calc"/>
						</a>
					</p>
					<p>
						<span class="text-">Записей:</span>
						<a href="{$current_project/@url}co/feed/">
							<xsl:value-of select="@post_count_calc"/>
						</a>
					</p>
				</xsl:if>
				<xsl:if test="$current_project/@communities_are_on = 1 or $current_project/@events_are_on = 1 or $current_project/@materials_are_on = 1">
					<p>
						<span class="text-">Комментариев:</span>
						<xsl:value-of select="@comment_count_calc"/>
					</p>
				</xsl:if>
				<xsl:if test="$current_project/@materials_are_on = 1">
					<p>
						<span class="text-">Материалов:</span>
						<a href="{$current_project/@url}materials/">
							<xsl:value-of select="@material_count_calc"/>
						</a>
					</p>
				</xsl:if>
				<xsl:if test="$current_project/@events_are_on = 1">
					<p>
						<span class="text-">Событий:</span>
						<a href="{$current_project/@url}events/">
							<xsl:value-of select="@event_count_calc"/>
						</a>
					</p>
				</xsl:if>
				<xsl:if test="$current_project/@marks_are_on = 1">
					<p>
						<span class="text-">Прецедентов:</span>
						<a href="{$current_project/@url}precedents/">
							<xsl:value-of select="@precedent_count_calc"/>
						</a>
					</p>
					<p>
						<span class="text-">Оценок:</span>
						<span class="with-hint">
							<xsl:call-template name="draw_mark_count_hint"/>
						</span>
					</p>
				</xsl:if>
				<p>
					<a href="{$current_project/@url}profile/">Подробнее</a>
				</p>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
