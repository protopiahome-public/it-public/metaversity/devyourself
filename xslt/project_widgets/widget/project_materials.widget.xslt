<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="materials">
		<!-- fix for paging template not start -->
	</xsl:template>
	<xsl:template match="project_widget_materials" mode="widget">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<div class="widget widget-materials">
			<xsl:call-template name="draw_widget_header">
				<xsl:with-param name="title" select="$data/@title"/>
				<xsl:with-param name="light_header" select="true()"/>
			</xsl:call-template>
			<xsl:choose>
				<xsl:when test="@module_disabled = 1">
					<div class="content-message- content-message-access-">
						<p class="text-">Материалы в данном проекте отключены.</p>
					</div>
				</xsl:when>
				<xsl:when test="@module_no_access = 1">
					<!-- @dm9 Блок с сообщением об ограниченном доступе-->
					<div class="content-message- content-message-access-">
						<xsl:call-template name="draw_access">
							<xsl:with-param name="status" select="$project_access/@access_materials_read"/>
							<xsl:with-param name="entity_genitiv" select="'проекта'"/>
							<xsl:with-param name="join_premoderation" select="$project_access/@join_premoderation = 1"/>
							<xsl:with-param name="for">Доступ к материалам ограничен. Для получения доступа</xsl:with-param>
							<xsl:with-param name="draw_instruction" select="true()"/>
						</xsl:call-template>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<xsl:for-each select="/root/materials[1]">
						<xsl:choose>
							<xsl:when test="not(material)">
								<div class="content-message- content-message-empty-">
									<p class="text-">Ни одного материала не найдено.</p>
								</div>
							</xsl:when>
							<xsl:otherwise>
								<div class="content-">
									<xsl:for-each select="material">
										<xsl:variable name="position" select="position()"/>
										<xsl:variable name="material_status" select="."/>
										<xsl:variable name="project" select="/root/project_short[@id = $material_status/@reflex_project_id]"/>
										<xsl:for-each select="/root/material_short[@id = current()/@id]">
											<xsl:variable name="current_material_url">
												<xsl:choose>
													<xsl:when test="not($project)">
														<xsl:value-of select="concat($current_project/@url, 'materials/', @id, '/')"/>
													</xsl:when>
													<xsl:when test="$material_status and $material_status/@reflex_id &gt; 0">
														<xsl:value-of select="concat($project/@url, 'materials/', $material_status/@reflex_id, '/')"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="concat($project/@url, 'materials/', @id, '/')"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<div class="item-">
												<div class="title- bigger">
													<xsl:if test="$project">
														<span class="project-">
															<a href="{$project/@url}">
																<xsl:value-of select="$project/@title"/>
															</a>
															<xsl:text> / </xsl:text>
														</span>
													</xsl:if>
													<a href="{$current_material_url}">
														<xsl:value-of select="@title"/>
													</a>
												</div>
												<div class="comments-">
													<span>
														<xsl:attribute name="class">
															<xsl:text>text- small</xsl:text>
															<xsl:if test="$position = 1"> first-</xsl:if>
														</xsl:attribute>
														<xsl:text>Комментарии: </xsl:text>
													</span>
													<a href="{$current_material_url}/#comments">
														<xsl:attribute name="class">bigger <xsl:choose>
																<xsl:when test="@comment_count_calc > 0"> bold</xsl:when>
															</xsl:choose>
														</xsl:attribute>
														<xsl:value-of select="@comment_count_calc"/>
													</a>
												</div>
											</div>
										</xsl:for-each>
									</xsl:for-each>
								</div>
								<div class="footer-">
									<a href="{$current_project/@url}materials/">Все материалы</a>
								</div>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
