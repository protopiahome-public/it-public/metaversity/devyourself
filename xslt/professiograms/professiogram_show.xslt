<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../competence_sets/head2/competence_set_head2.inc.xslt"/>
	<xsl:include href="menu/professiogram_submenu.inc.xslt"/>
	<xsl:include href="../competence_sets/include/rate_show.inc.xslt"/>
	<xsl:variable name="professiograms_url" select="concat($current_competence_set_url, 'professiograms/')"/>
	<xsl:variable name="top_section" select="'sets'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="competence_set_section" select="'professiograms'"/>
	<xsl:variable name="competence_set_section_main_page" select="false()"/>
	<xsl:variable name="professiogram_id" select="/root/professiogram_show/@id"/>
	<xsl:variable name="professiogram_title" select="/root/professiogram_show/@title"/>
	<xsl:variable name="module_url" select="concat($current_competence_set_url, 'professiograms/', $professiogram_id, '/')"/>
	<xsl:template match="professiogram_show">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_competence_set_head2"/>
							<h2>Профессии</h2>
							<div class="content">
								<h3>
									<xsl:if test="@enabled = 0">
										<xsl:text>[Отключена] </xsl:text>
									</xsl:if>
									<xsl:value-of select="$professiogram_title"/>
								</h3>
								<xsl:if test="$competence_set_access/@can_moderate_professiograms = 1">
									<xsl:call-template name="draw_professiogram_submenu"/>
								</xsl:if>
								<p>
									<xsl:text> См. также </xsl:text>
									<a href="{$prefix}/ratings/prof-{@id}/">рейтинг по профессии</a>
									<xsl:text>.</xsl:text>
								</p>
								<xsl:call-template name="draw_rate_show">
									<xsl:with-param name="show_used_competence_set" select="false()"/>
								</xsl:call-template>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Профессия &#171;</xsl:text>
		<xsl:value-of select="/root/professiogram_show/@title"/>
		<xsl:text>&#187; &#8212; </xsl:text>
		<xsl:value-of select="$current_competence_set/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/sets/">Наборы компетенций</a>
		</div>
		<div class="level2-">
			<a href="{$current_competence_set_url}">
				<xsl:value-of select="$current_competence_set/@title"/>
			</a>
		</div>
		<div class="level3-">
			<a href="{$professiograms_url}">Профессии</a>
		</div>
		<div class="level4- selected-">
			<xsl:value-of select="$professiogram_title"/>
		</div>
	</xsl:template>
</xsl:stylesheet>
