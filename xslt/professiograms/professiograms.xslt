<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../competence_sets/head2/competence_set_head2.inc.xslt"/>
	<xsl:variable name="top_section" select="'sets'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="competence_set_section" select="'professiograms'"/>
	<xsl:variable name="competence_set_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_competence_set_url, 'professiograms/')"/>
	<xsl:variable name="professiograms_url" select="concat($current_competence_set_url, 'professiograms/')"/>
	<xsl:template match="professiograms">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_competence_set_head2"/>
							<h2>Профессии</h2>
							<div class="content">
								<xsl:call-template name="_draw_list"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<xsl:if test="$competence_set_access/@can_moderate_professiograms = 1">
			<div class="ico-links">
				<a class="add-" href="{$module_url}add/">
					<span>добавить профессию</span>
				</a>
			</div>
		</xsl:if>
		<xsl:if test="not(professiogram)">
			<p>Ни одной профессии пока не создано.</p>
		</xsl:if>
		<xsl:if test="professiogram">
			<table class="tbl">
				<tr>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="'Профессия'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'competences'"/>
						<xsl:with-param name="title" select="'Кол-во&#160;компетенций'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<xsl:if test="$competence_set_access/@can_moderate_professiograms = 1">
						<th/>
					</xsl:if>
				</tr>
				<xsl:for-each select="professiogram">
					<tr>
						<td class="col-title-">
							<xsl:if test="@enabled = 0">
								<xsl:attribute name="class">disabled</xsl:attribute>
							</xsl:if>
							<a href="{$module_url}{@id}/">
								<xsl:if test="@enabled = 0">
									<xsl:text>[Отключена] </xsl:text>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</a>
						</td>
						<td class="col-digits-">
							<xsl:value-of select="@competence_count_calc"/>
						</td>
						<xsl:if test="$competence_set_access/@can_moderate_professiograms = 1">
							<td class="col-icons- icons">
								<a class="rating- gray-" href="{$prefix}/ratings/prof-{@id}/" title="Рейтинг"/>
								<span class="sep-"/>
								<a class="edit- gray-" href="{$module_url}{@id}/edit/" title="Редактировать"/>
								<span class="sep-"/>
								<a class="delete- gray-" href="{$module_url}{@id}/delete/" title="Удалить"/>
							</td>
						</xsl:if>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Профессии &#8212; </xsl:text>
		<xsl:value-of select="$current_competence_set/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/sets/">Наборы компетенций</a>
		</div>
		<div class="level2-">
			<a href="{$current_competence_set_url}">
				<xsl:value-of select="$current_competence_set/@title"/>
			</a>
		</div>
		<div class="level3- selected-">Профессии</div>
	</xsl:template>
</xsl:stylesheet>
