<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="professiogram_url" select="concat($current_competence_set_url, 'professiograms/', $professiogram_id, '/')"/>
	<xsl:template name="draw_professiogram_submenu">
		<div class="ico-links">
			<xsl:choose>
				<xsl:when test="/root/professiogram_edit">
					<strong class="edit- current-">
						<span>редактировать</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="edit-" href="{$professiogram_url}edit/">
						<span>редактировать</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/professiogram_delete">
					<strong class="delete- current-">
						<span>удалить</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="delete-" href="{$professiogram_url}delete/">
						<span>удалить</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
