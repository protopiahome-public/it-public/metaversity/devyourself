<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../competence_sets/head2/competence_set_head2.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:include href="../competence_sets/include/rate_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'sets'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="competence_set_section" select="'professiograms'"/>
	<xsl:variable name="competence_set_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($current_competence_set_url, 'professiograms/add/')"/>
	<xsl:variable name="professiograms_url" select="concat($current_competence_set_url, 'professiograms/')"/>
	<xsl:template match="error_403">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_competence_set_head2"/>
							<h2>Профессии</h2>
							<div class="content">
								<div class="box box-stop">
									<h3>Доступ запрещён</h3>
									<p>У вас нет прав на редактирование профессий.</p>
									<p>Для редактирования профессий нужно быть модератором или администратором текущего набора компетенций.</p>
								</div>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Нет доступа &#8212; </xsl:text>
		<xsl:value-of select="$current_competence_set/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/sets/">Наборы компетенций</a>
		</div>
		<div class="level2-">
			<a href="{$current_competence_set_url}">
				<xsl:value-of select="$current_competence_set/@title"/>
			</a>
		</div>
		<div class="level3-">
			<a href="{$professiograms_url}">Профессии</a>
		</div>
	</xsl:template>
</xsl:stylesheet>
