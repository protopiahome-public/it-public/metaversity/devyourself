<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../competence_sets/head2/competence_set_head2.inc.xslt"/>
	<xsl:include href="menu/professiogram_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/delete.inc.xslt"/>
	<xsl:include href="../competence_sets/include/rate_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'sets'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="competence_set_section" select="'professiograms'"/>
	<xsl:variable name="competence_set_section_main_page" select="false()"/>
	<xsl:variable name="professiogram_id" select="/root/professiogram_delete/@id"/>
	<xsl:variable name="professiogram_title" select="/root/professiogram_delete/@title"/>
	<xsl:variable name="module_url" select="concat($current_competence_set_url, 'professiograms/', $professiogram_id, '/delete/')"/>
	<xsl:variable name="professiograms_url" select="concat($current_competence_set_url, 'professiograms/')"/>
	<xsl:template match="professiogram_delete">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_competence_set_head2"/>
							<h2>Профессии</h2>
							<div class="content">
								<h3>
									<a href="{$professiogram_url}">
										<xsl:value-of select="$professiogram_title"/>
									</a>
								</h3>
								<xsl:call-template name="draw_professiogram_submenu"/>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/professiogram_delete/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$professiograms_url}"/>
			<input type="hidden" name="competence_set_id" value="{$current_competence_set/@id}"/>
			<xsl:if test="$pass_info/error[@name = 'CHILDREN_EXIST']">
				<div class="error">
					<span>Группа прецедентов не может быть удалена, так как в ней есть дочерние прецеденты.</span>
				</div>
			</xsl:if>
			<xsl:call-template name="draw_delete">
				<xsl:with-param name="title_akk" select="'роль'"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Удаление профессии &#171;</xsl:text>
		<xsl:value-of select="$professiogram_title"/>
		<xsl:text>&#187; &#8212; </xsl:text>
		<xsl:value-of select="$current_competence_set/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/sets/">Наборы компетенций</a>
		</div>
		<div class="level2-">
			<a href="{$current_competence_set_url}">
				<xsl:value-of select="$current_competence_set/@title"/>
			</a>
		</div>
		<div class="level3-">
			<a href="{$professiograms_url}">Профессии</a>
		</div>
		<div class="level4-">
			<a href="{$professiogram_url}">
				<xsl:value-of select="$professiogram_title"/>
			</a>
		</div>
		<div class="level5- selected-">Удаление</div>
	</xsl:template>
</xsl:stylesheet>
