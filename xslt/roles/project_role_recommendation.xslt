<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="../users/search/users_search.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'roles'"/>
	<xsl:variable name="project_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'roles/')"/>
	<xsl:template match="project_role_recommendation">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Рекомендация ролей</h2>
							<div class="content">
								<div class="box" style="float: left;">
									Модуль находится в разработке.
								</div>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Рекомендация ролей &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/projects/">Проекты</a>
		</div>
		<div class="level2-">
			<a href="{$current_project/@url}">
				<xsl:value-of select="$current_project/@title"/>
			</a>
		</div>
		<div class="level3- selected-">
			<xsl:choose>
				<xsl:when test="$project_section_main_page">Рекомендации ролей</xsl:when>
				<xsl:otherwise>
					<a href="{$module_url}">Рекомендации ролей</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>