<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_common.xslt"/>
	<xsl:include href="../include/comment.inc.xslt"/>
	<xsl:template match="comments">
		<xsl:for-each select="comment">
			<xsl:call-template name="draw_comment">
				<xsl:with-param name="can_moderate" select="true()"/>
				<xsl:with-param name="can_comment" select="true()"/>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="text()"/>
</xsl:stylesheet>