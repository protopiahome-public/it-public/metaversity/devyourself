<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="../../_site/include/access.inc.xslt"/>
	<xsl:include href="comment.inc.xslt"/>
	<xsl:template name="draw_comments">
		<xsl:param name="can_moderate" select="false()"/>
		<xsl:param name="can_comment" select="false()"/>
		<xsl:param name="can_comment_html"/>
		<xsl:param name="title_akk" select="'сущность'"/>
		<div class="comments" id="jq-comments" data-type="{@type}" data-object-id="{@object_id}">
			<a class="anchor-" name="comments"/>
			<h3 class="down">
				<xsl:value-of select="concat('Комментарии (', count(comment), ')')"/>
				<xsl:if test="$user/@id">
					<xsl:choose>
						<xsl:when test="/root/comment_subscription[@object_id = current()/@object_id and @type = current()/@type]/@is_subscribed = 1">
							<span id="jq-comments-subscribe" class="dn" data-subscribe="1">
								<i>подписаться на обновления</i>
							</span>
							<span id="jq-comments-unsubscribe" data-subscribe="0" class="subscribed-">
								<i>вы подписаны на обновления</i>
							</span>
						</xsl:when>
						<xsl:otherwise>
							<span id="jq-comments-subscribe" data-subscribe="1">
								<i>подписаться на обновления</i>
							</span>
							<span id="jq-comments-unsubscribe" data-subscribe="0" class="subscribed- dn">
								<i>вы подписаны на обновления</i>
							</span>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</h3>
			<div id="jq-comment-reply-form-inner" class="jq-comment-form reply-inner- dn">
				<div class="textarea-">
					<textarea rows="10" cols="60"/>
				</div>
				<div class="button-">
					<button class="jq-comment-add-submit jq-manual-submit button button-small">Отправить</button>
				</div>
			</div>
			<div class="list-">
				<xsl:for-each select="comment">
					<xsl:call-template name="draw_comment">
						<xsl:with-param name="can_moderate" select="$can_moderate"/>
						<xsl:with-param name="can_comment" select="$can_comment"/>
					</xsl:call-template>
				</xsl:for-each>
			</div>
			<div id="jq-comment-main-reply" class="main-reply-">
				<a class="anchor-" name="add"/>
				<div class="head-">
					<xsl:text>Прокомментировать </xsl:text>
					<xsl:value-of select="$title_akk"/>
					<xsl:text>:</xsl:text>
				</div>
				<xsl:choose>
					<xsl:when test="$can_comment">
						<div class="jq-comment-form reply-inner-">
							<div class="textarea-">
								<textarea rows="10" cols="60"/>
							</div>
							<div class="button-">
								<button class="jq-comment-add-submit jq-manual-submit button button-small">Отправить</button>
							</div>
						</div>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy-of select="$can_comment_html"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
