<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_comment">
		<xsl:param name="can_moderate" select="false()"/>
		<xsl:param name="can_comment" select="false()"/>
		<div class="jq-comment comment-" id="comment-container-{@id}" style="margin-left: {30 * @level}px;" data-id="{@id}">
			<a class="anchor-" name="comment-{@id}"/>
			<xsl:if test="$can_moderate and @is_deleted = 0">
				<div class="icons-small">
					<a href="#" class="gray- delete- jq-comment-delete-link"/>
				</div>
			</xsl:if>
			<div class="date-">
				<xsl:call-template name="get_full_datetime">
					<xsl:with-param name="datetime" select="@add_time"/>
				</xsl:call-template>
			</div>
			<div class="link-">
				<a href="#comment-{@id}">#</a>
			</div>
			<xsl:choose>
				<xsl:when test="@is_deleted = 0">
					<xsl:for-each select="/root/user_short[@id = current()/@author_user_id]">
						<table class="user-block">
							<tr>
								<td class="avatar-">
									<span>
										<a href="{$prefix}/users/{@login}/">
											<img src="{photo_small/@url}" width="{photo_small/@width}" height="{photo_small/@height}" alt="{@login}"/>
										</a>
									</span>
								</td>
								<td class="text-">
									<a href="{$prefix}/users/{@login}/">
										<xsl:value-of select="@visible_name"/>
									</a>
									<b>
										<xsl:value-of select="@login"/>
									</b>
								</td>
							</tr>
						</table>
					</xsl:for-each>
					<div class="content-">
						<xsl:copy-of select="html/*"/>
					</div>
					<xsl:if test="$can_comment">
						<div class="reply-">
							<span class="jq-comment-reply-link">Ответить</span>
						</div>
						<div class="jq-comment-reply-form form-"/>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<div class="deleted-">
						<span class="note">
							<xsl:text>Комментарий удален. </xsl:text>
							<xsl:if test="$can_moderate">
								<span class="clickable jq-comment-restore-link">Восстановить</span>
								<xsl:text>.</xsl:text>
							</xsl:if>
						</span>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
