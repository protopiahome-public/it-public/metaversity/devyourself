<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="top_section" select="'research'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/research/cluster-users/')"/>
	<xsl:template match="research_cluster_users">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3 class="pre">
									<a href="{$prefix}/research/">Исследование результатов</a>
								</h3>
								<h4>
									<xsl:choose>
										<xsl:when test="$get_vars">
											<a href="{$module_url}">Кластеризация участников</a>
										</xsl:when>
										<xsl:otherwise>Кластеризация участников</xsl:otherwise>
									</xsl:choose>
								</h4>
								<div class="box">
									<p>Кластеризация игроков по результатам. Топ профессиограмм и компетенций внутри каждого кластера.</p>
								</div>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$module_url}" method="get">
			<fieldset>
				<div class="field">
					<label class="title-">Проект:</label>
					<select name="project_id">
						<xsl:for-each select="projects/project">
							<option value="{@id}">
								<xsl:if test="../../@project_id = @id">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<label class="title-">Набор компетенций:</label>
					<select name="competence_set_id">
						<xsl:for-each select="competence_sets/competence_set">
							<option value="{@id}">
								<xsl:if test="../../@competence_set_id = @id">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<label class="title-">Метод кластеризации:</label>
					<select name="clustering_method">
						<xsl:for-each select="clustering_methods/method">
							<option value="{@key}">
								<xsl:if test="../../@clustering_method = @key">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<label class="title-">Функция расстояния:</label>
					<select name="distance">
						<xsl:for-each select="distances/distance">
							<option value="{@key}">
								<xsl:if test="../../@distance = @key">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<input id="normalize" type="checkbox" class="input-checkbox" name="normalize" value="1">
						<xsl:if test="@normalize = 1">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input>
					<label for="normalize" class="inline-">Нормализовать векторы</label>
				</div>
				<div class="field">
					<label class="title-">Минимальное кол-во оценок у участника:</label>
					<input type="text" class="input-text narrow-" name="mark_count_threshold" value="{@mark_count_threshold}"/>
				</div>
				<div class="field">
					<label class="title-">Количество кластеров участников:</label>
					<input type="text" class="input-text narrow-" name="cluster_count" value="{@cluster_count}"/>
				</div>
				<div class="field">
					<label class="title-">Оставить только эти кластеры:</label>
					<input type="text" class="input-text narrow-" name="clusters_to_leave" value="{@clusters_to_leave}"/>
					<div class="comment-">Например: 1, 2, 8</div>
				</div>
				<button style="margin-bottom: 10px;" class="button button-small">Далее</button>
			</fieldset>
		</form>
		<xsl:if test="$get_vars and @error != ''">
			<p class="red">
				<strong>
					<xsl:value-of select="concat('Error: ', @error)"/>
				</strong>
			</p>
		</xsl:if>
		<xsl:if test="@competence_count">
			<p>
				<xsl:value-of select="concat('Компетенций: ', @competence_count)"/>
			</p>
		</xsl:if>
		<xsl:if test="@user_count">
			<p>
				<xsl:value-of select="concat('Людей: ', @user_count)"/>
			</p>
		</xsl:if>
		<xsl:if test="@used_mark_count">
			<p>
				<xsl:value-of select="concat('Оценок: ', @used_mark_count)"/>
			</p>
		</xsl:if>
		<xsl:if test="code">
			<p>
				<xsl:text>Код для Mathematica:</xsl:text>
				<br/>
				<textarea rows="3" cols="80">
					<xsl:value-of select="code"/>
				</textarea>
			</p>
		</xsl:if>
		<xsl:if test="code_draw">
			<p>
				<xsl:text>Код для отрисовки кластеров в 2D:</xsl:text>
				<br/>
				<textarea rows="3" cols="80">
					<xsl:value-of select="code_draw"/>
				</textarea>
			</p>
		</xsl:if>
		<xsl:if test="clusters">
			<h3>Результаты</h3>
			<p class="note">
				<span>match &#8212; среднее соответствие профессии в кластере</span>
				<br/>
				<span>result &#8212; средней результат по компетенции в кластере</span>
				<br/>
				<span>delta &#8212; отклонение от среднего, вычисленного по всем участникам</span>
			</p>
			<xsl:for-each select="clusters/cluster">
				<fieldset>
					<legend class="toggle" jq-toggle="c{@number}">
						<xsl:value-of select="@number"/>
					</legend>
					<div id="c{@number}" style="margin-bottom: 10px; line-height: 18px;">
						<span class="hl">Люди: </span>
						<xsl:for-each select="users/user">
							<a href="{$prefix}/users/{@login}/">
								<xsl:value-of select="@full_name"/>
							</a>
							<xsl:value-of select="concat(' (', @id, ')')"/>
							<xsl:if test="position() != last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
						<br/>
						<span class="hl">Профессии:</span>
						<br/>
						<xsl:for-each select="professiograms/professiogram">
							<xsl:if test="@number &lt;= 3">
								<xsl:value-of select="@title"/>
								<span class="hl">
									<xsl:value-of select="concat(' (match = ', @match, ', delta = ', @delta, ')')"/>
								</span>
								<br/>
							</xsl:if>
						</xsl:for-each>
						<span class="note clickable toggle" jq-toggle="с{@number}_prof">далее</span>
						<br/>
						<div id="с{@number}_prof" class="dn">
							<xsl:for-each select="professiograms/professiogram">
								<xsl:if test="@number &gt; 3">
									<xsl:value-of select="@title"/>
									<span class="hl">
										<xsl:value-of select="concat(' (match = ', @match, ', delta = ', @delta, ')')"/>
									</span>
									<br/>
								</xsl:if>
							</xsl:for-each>
						</div>
						<span class="hl">Компетенции:</span>
						<br/>
						<xsl:for-each select="competences/competence">
							<xsl:if test="@number &lt;= 5">
								<xsl:value-of select="concat('(', @id, ') ')"/>
								<xsl:value-of select="@title"/>
								<span class="hl">
									<xsl:value-of select="concat(' (result = ', @result, ', delta = ', @delta, ')')"/>
								</span>
								<br/>
							</xsl:if>
						</xsl:for-each>
						<span class="note clickable toggle" jq-toggle="с{@number}_cmp">далее</span>
						<br/>
						<div id="с{@number}_cmp" class="dn">
							<xsl:for-each select="competences/competence">
								<xsl:if test="@number &gt; 5">
									<xsl:value-of select="@title"/>
									<span class="hl">
										<xsl:value-of select="concat(' (result = ', @result, ', delta = ', @delta, ')')"/>
									</span>
									<br/>
								</xsl:if>
							</xsl:for-each>
						</div>
					</div>
				</fieldset>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Кластеризация участников'"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		
	</xsl:template>
</xsl:stylesheet>
