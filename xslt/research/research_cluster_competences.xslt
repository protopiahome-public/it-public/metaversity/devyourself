<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="top_section" select="'research'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/research/cluster-competences/')"/>
	<xsl:template match="research_cluster_competences">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3 class="pre">
									<a href="{$prefix}/research/">Исследование результатов</a>
								</h3>
								<h4>
									<xsl:choose>
										<xsl:when test="$get_vars">
											<a href="{$module_url}">Кластеризация компетенций</a>
										</xsl:when>
										<xsl:otherwise>Кластеризация компетенций</xsl:otherwise>
									</xsl:choose>
								</h4>
								<div class="box">
									<p>Изучение зависимостей между компетенциями.</p>
								</div>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$module_url}" method="get">
			<fieldset>
				<div class="field">
					<label class="title-">Проект:</label>
					<select name="project_id">
						<xsl:for-each select="projects/project">
							<option value="{@id}">
								<xsl:if test="../../@project_id = @id">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<label class="title-">Набор компетенций:</label>
					<select name="competence_set_id">
						<xsl:for-each select="competence_sets/competence_set">
							<option value="{@id}">
								<xsl:if test="../../@competence_set_id = @id">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<label class="title-">Метод кластеризации:</label>
					<select name="clustering_method">
						<xsl:for-each select="clustering_methods/method">
							<option value="{@key}">
								<xsl:if test="../../@clustering_method = @key">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<label class="title-">Функция расстояния:</label>
					<select name="distance">
						<xsl:for-each select="distances/distance">
							<option value="{@key}">
								<xsl:if test="../../@distance = @key">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<input id="normalize" type="checkbox" class="input-checkbox" name="normalize" value="1">
						<xsl:if test="@normalize = 1">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input>
					<label for="normalize" class="inline-">Нормализовать векторы</label>
				</div>
				<div class="field">
					<label class="title-">Пространство векторов:</label>
					<select name="vector_dimension">
						<xsl:for-each select="vector_dimensions/dimension">
							<option value="{@key}">
								<xsl:if test="../../@vector_dimension = @key">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<label class="title-">Минимальное кол-во оценок у участника:</label>
					<input type="text" class="input-text narrow-" name="user_mark_count_threshold" value="{@user_mark_count_threshold}"/>
				</div>
				<div class="field">
					<label class="title-">Минимальное кол-во оценок по компетенции:</label>
					<input type="text" class="input-text narrow-" name="competence_mark_count_threshold" value="{@competence_mark_count_threshold}"/>
				</div>
				<div class="field">
					<label class="title-">Количество кластеров компетенций:</label>
					<input type="text" class="input-text narrow-" name="cluster_count" value="{@cluster_count}"/>
				</div>
				<div class="field">
					<label class="title-">Оставить только эти кластеры:</label>
					<input type="text" class="input-text narrow-" name="clusters_to_leave" value="{@clusters_to_leave}"/>
					<div class="comment-">Например: 1, 2, 8</div>
				</div>
				<button style="margin-bottom: 10px;" class="button button-small">Далее</button>
			</fieldset>
		</form>
		<xsl:if test="$get_vars and @error != ''">
			<p class="red">
				<strong>
					<xsl:value-of select="concat('Error: ', @error)"/>
				</strong>
			</p>
		</xsl:if>
		<xsl:if test="@competence_count">
			<p>
				<xsl:value-of select="concat('Компетенций: ', @competence_count)"/>
			</p>
		</xsl:if>
		<xsl:if test="@user_count">
			<p>
				<xsl:value-of select="concat('Людей: ', @user_count)"/>
			</p>
		</xsl:if>
		<xsl:if test="@used_mark_count">
			<p>
				<xsl:value-of select="concat('Оценок: ', @used_mark_count)"/>
			</p>
		</xsl:if>
		<xsl:if test="code">
			<p>
				<xsl:text>Код для Mathematica:</xsl:text>
				<br/>
				<textarea rows="3" cols="80">
					<xsl:value-of select="code"/>
				</textarea>
			</p>
		</xsl:if>
		<xsl:if test="clusters">
			<h3>Результаты</h3>
			<table cellpadding="0" cellspacing="2">
				<xsl:for-each select="report/param">
					<tr>
						<td align="left" valign="middle" style="background: #eee;">
							<xsl:value-of select="@name"/>
						</td>
						<td align="left" valign="middle" style="background: #eee;">
							<xsl:choose>
								<xsl:when test="string-length(@value) &lt; 200">
									<xsl:value-of select="@value"/>
								</xsl:when>
								<xsl:otherwise>
									<span class="clickable note toggle" jq-toggle="report_{@name}">показать</span>
									<div id="report_{@name}" class="dn">
										<xsl:value-of select="@value"/>
									</div>
								</xsl:otherwise>
							</xsl:choose>
							
						</td>
					</tr>
				</xsl:for-each>
			</table>
			<xsl:for-each select="clusters/cluster">
				<fieldset>
					<legend class="toggle" jq-toggle="c{@number}">
						<xsl:value-of select="@number"/>
					</legend>
					<div id="c{@number}" style="margin-bottom: 10px; line-height: 18px;">
						<xsl:for-each select="competence">
							<xsl:value-of select="concat('(', @id, ') ')"/>
							<xsl:value-of select="@title"/>
							<br/>
						</xsl:for-each>
					</div>
				</fieldset>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Кластеризация компетенций'"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		
	</xsl:template>
</xsl:stylesheet>
