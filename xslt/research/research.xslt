<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="top_section" select="'research'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/users/')"/>
	<xsl:template match="research">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3>Исследование результатов</h3>
								<h4>
									<a href="{$prefix}/research/group-results-clusters/">Групповые результаты</a>
								</h4>
								<p style="padding-left: 20px;">Сравнение усреднённых результатов групп внутри кластеров компетенций.</p>
								<h4>
									<a href="{$prefix}/research/cluster-competences/">Кластеризация компетенций</a>
								</h4>
								<p style="padding-left: 20px;">Изучение зависимостей между компетенциями.</p>
								<h4>
									<a href="{$prefix}/research/cluster-users/">Кластеризация участников</a>
								</h4>
								<p style="padding-left: 20px;">Кластеризация игроков по результатам. Топ профессиограмм и компетенций внутри каждого кластера.</p>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Исследование результатов'"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		
	</xsl:template>
</xsl:stylesheet>
