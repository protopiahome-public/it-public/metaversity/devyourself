<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="top_section" select="'research'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/research/group-results-clusters/')"/>
	<xsl:template match="research_group_results_clusters">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3 class="pre">
									<a href="{$prefix}/research/">Исследование результатов</a>
								</h3>
								<h4>
									<xsl:choose>
										<xsl:when test="$get_vars">
											<a href="{$module_url}">Групповые результаты</a>
										</xsl:when>
										<xsl:otherwise>Групповые результаты</xsl:otherwise>
									</xsl:choose>
								</h4>
								<div class="box">
									<p>Сравнение усреднённых результатов групп внутри кластеров компетенций.</p>
								</div>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$module_url}" method="get">
			<fieldset>
				<div class="field">
					<label class="title-">Проект:</label>
					<select name="project_id">
						<xsl:for-each select="projects/project">
							<option value="{@id}">
								<xsl:if test="../../@project_id = @id">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<label class="title-">Набор компетенций:</label>
					<select name="competence_set_id">
						<xsl:for-each select="competence_sets/competence_set">
							<option value="{@id}">
								<xsl:if test="../../@competence_set_id = @id">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<label class="title-">Метод кластеризации:</label>
					<select name="clustering_method">
						<xsl:for-each select="clustering_methods/method">
							<option value="{@key}">
								<xsl:if test="../../@clustering_method = @key">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<label class="title-">Функция расстояния:</label>
					<select name="distance">
						<xsl:for-each select="distances/distance">
							<option value="{@key}">
								<xsl:if test="../../@distance = @key">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div class="field">
					<input id="normalize" type="checkbox" class="input-checkbox" name="normalize" value="1">
						<xsl:if test="@normalize = 1">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input>
					<label for="normalize" class="inline-">Нормализовать векторы</label>
				</div>
				<div class="field">
					<label class="title-">Минимальное кол-во оценок у участника:</label>
					<input type="text" class="input-text narrow-" name="user_mark_count_threshold" value="{@user_mark_count_threshold}"/>
				</div>
				<div class="field">
					<label class="title-">Минимальное кол-во оценок по компетенции:</label>
					<input type="text" class="input-text narrow-" name="competence_mark_count_threshold" value="{@competence_mark_count_threshold}"/>
				</div>
				<div class="field">
					<label class="title-">Количество кластеров компетенций:</label>
					<input type="text" class="input-text narrow-" name="cluster_count" value="{@cluster_count}"/>
				</div>
				<div class="field">
					<label class="title-">Оставить только эти кластеры:</label>
					<input type="text" class="input-text narrow-" name="clusters_to_leave" value="{@clusters_to_leave}"/>
					<div class="comment-">Например: 1, 2, 8</div>
				</div>
				<div class="field">
					<label class="title-">Группы участников</label>
					<div class="input-set-" style="overflow: auto; max-height: 200px;">
						<xsl:for-each select="groups/group">
							<div class="input-">
								<input id="group_{@id}" type="checkbox" class="input-checkbox" name="groups[]" value="{@id}">
									<xsl:if test="../../groups_selected/group[@id = current()/@id]">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>
								<label for="group_{@id}" class="inline-">
									<xsl:value-of select="@title"/>
									<xsl:value-of select="concat(' (', @user_count, ')')"/>
								</label>
							</div>
						</xsl:for-each>
					</div>
				</div>
				<button style="margin-bottom: 10px;" class="button button-small">Далее</button>
			</fieldset>
		</form>
		<xsl:if test="$get_vars and @error != ''">
			<p class="red">
				<strong>
					<xsl:value-of select="concat('Error: ', @error)"/>
				</strong>
			</p>
		</xsl:if>
		<xsl:if test="@competence_count">
			<p>
				<xsl:value-of select="concat('Компетенций: ', @competence_count)"/>
			</p>
		</xsl:if>
		<xsl:if test="@user_count">
			<p>
				<xsl:value-of select="concat('Людей: ', @user_count)"/>
			</p>
		</xsl:if>
		<xsl:if test="@used_mark_count">
			<p>
				<xsl:value-of select="concat('Оценок: ', @used_mark_count)"/>
			</p>
		</xsl:if>
		<xsl:if test="code">
			<p>
				<xsl:text>Код для Mathematica:</xsl:text>
				<br/>
				<textarea rows="3" cols="80">
					<xsl:value-of select="code"/>
				</textarea>
			</p>
		</xsl:if>
		<xsl:if test="clusters">
			<h3>Результаты</h3>
			<p>
				<strong>Группы:</strong>
				<br/>
				<br/>
				<xsl:for-each select="group_users/group">
					<strong>
						<xsl:value-of select="../../groups/group[@id = current()/@id]/@title"/>
					</strong>
					<xsl:value-of select="concat(' (', count(user), ')')"/>
					<xsl:value-of select="': '"/>
					<xsl:for-each select="user">
						<a href="{$prefix}/users/{@login}/">
							<xsl:value-of select="@full_name"/>
						</a>
						<xsl:value-of select="concat(' (', @id, ')')"/>
						<xsl:if test="position() != last()">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
					<br/>
					<br/>
				</xsl:for-each>
			</p>
			<table cellpadding="0" cellspacing="2">
				<tr>
					<th>Кластер</th>
					<th>Компетенции</th>
					<xsl:for-each select="clusters/cluster[1]/groups/group">
						<th>
							<xsl:value-of select="../../../../groups/group[@id = current()/@id]/@title"/>
						</th>
					</xsl:for-each>
				</tr>
				<xsl:for-each select="clusters/cluster">
					<tr>
						<td align="center" valign="middle" style="background: #eee;">
							<xsl:value-of select="@number"/>
						</td>
						<td align="left" valign="top" style="background: #eee;">
							<xsl:for-each select="competences/competence">
								<xsl:value-of select="concat('(', @id, ') ')"/>
								<xsl:value-of select="@title"/>
								<br/>
							</xsl:for-each>
						</td>
						<xsl:for-each select="groups/group">
							<td align="center" valign="middle" style="background: #f0f0f0;">
								<xsl:if test="@is_leader = 1">
									<xsl:attribute name="style">background: #0a0;</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@result"/>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Групповые результаты'"/>
	</xsl:template>
</xsl:stylesheet>
