<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_tree_head">
		<div class="tree-head">
			<span class="item- clickable" id="tree-collapse">свернуть всё</span>
			<span class="sep-"> | </span>
			<span class="item- clickable" id="tree-groups">показать только группы</span>
			<span class="sep-"> | </span>
			<span class="item- clickable" id="tree-expand">развернуть всё</span>
		</div>
	</xsl:template>
	<xsl:template name="draw_tree_competence_group">
		<xsl:param name="competences" select="/root/competence_set_competences//competence"/>
		<xsl:param name="focus_competences" select="/"/>
		<xsl:param name="competence_is_clickable" select="true()"/>
		<xsl:param name="draw_checkboxes" select="false()"/>
		<xsl:param name="show_bars" select="true()"/>
		<xsl:param name="parent_node" select="/root/competence_set_competences"/>
		<xsl:param name="level" select="0"/>
		<xsl:for-each select="$parent_node/group[.//competence[@id = $competences/@id]]">
			<xsl:variable name="group_id" select="@id"/>
			<table cellspacing="0" cellpadding="0">
				<xsl:attribute name="class">
					<xsl:text>group- jq-group-</xsl:text>
					<xsl:value-of select="@id"/>
					<xsl:if test="position() = 1"> group-first-</xsl:if>
				</xsl:attribute>
				<tr>
					<xsl:attribute name="class">
						<xsl:text>jq-tree-group-head</xsl:text>
						<xsl:if test="not(competence[@id = $competences/@id])"> head-only-</xsl:if>
					</xsl:attribute>
					<th class="col-group-title-">
						<xsl:if test="not(position() = 1 and $level = 0)">
							<xsl:attribute name="colspan">4</xsl:attribute>
						</xsl:if>
						<span class="clickable">
							<q class="dn">
								<xsl:value-of select="@id"/>
							</q>
							<xsl:value-of select="@title"/>
						</span>
					</th>
					<xsl:if test="position() = 1 and $level = 0">
						<xsl:if test="$show_bars">
							<th class="col-bars-legend-">
								<div class="width-">
									<table cellpadding="0" cellspacing="0">
										<tr>
											<td title="Склонность">Ск.</td>
											<td title="Способность">Сп.</td>
											<td title="Компетенция">К.</td>
										</tr>
									</table>
								</div>
							</th>
						</xsl:if>
						<xsl:apply-templates mode="tree_right_th" select="."/>
					</xsl:if>
				</tr>
				<xsl:for-each select="competence[@id = $competences/@id]">
					<xsl:variable name="mark_details_url">
						<xsl:if test="$competence_is_clickable">
							<xsl:value-of select="$prefix"/>
							<xsl:text>/users/</xsl:text>
							<xsl:value-of select="$current_user/@login"/>
							<xsl:text>/results/competence-</xsl:text>
							<xsl:value-of select="@id"/>
							<xsl:text>/</xsl:text>
							<xsl:value-of select="$request/@query_string"/>
						</xsl:if>
					</xsl:variable>
					<tr class="jq-tree-group-competence jq-tree-group-{$group_id}-row">
						<td class="col-title-">
							<xsl:if test="$focus_competences[@id = current()/@id]">
								<xsl:attribute name="class">col-title- col-title-focus-</xsl:attribute>
								<xsl:attribute name="title">
									<xsl:choose>
										<xsl:when test="$focus_competences[@id = current()/@id]/vector">
											<xsl:text>Компетенция была в фокусе: </xsl:text>
											<xsl:for-each select="$focus_competences[@id = current()/@id]/vector">
												<xsl:if test="@project_id">
													<xsl:text>проект &#171;</xsl:text>
													<xsl:value-of select="/root/project_short[@id = current()/@project_id]/@title"/>
													<xsl:text>&#187; (</xsl:text>
													<xsl:call-template name="get_full_date">
														<xsl:with-param name="datetime" select="@add_time"/>
													</xsl:call-template>
													<xsl:text>)</xsl:text>
												</xsl:if>
												<xsl:choose>
													<xsl:when test="position() != last()">, </xsl:when>
													<xsl:otherwise>.</xsl:otherwise>
												</xsl:choose>
											</xsl:for-each>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>Компетенция в фокусе</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
							</xsl:if>
							<xsl:if test="$draw_checkboxes">
								<input type="checkbox" id="competence-{@id}" name="competence-{@id}" value="1">
									<xsl:choose>
										<xsl:when test="$pass_info/vars/var[@name = concat('competence-', current()/@id)]">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:when>
										<xsl:when test="$pass_info"/>
										<xsl:when test="$focus_competences[@id = current()/@id]">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:when>
									</xsl:choose>
								</input>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="$competence_is_clickable">
									<a class="clickable" href="{$mark_details_url}">
										<xsl:call-template name="draw_competence_title"/>
									</a>
								</xsl:when>
								<xsl:when test="$draw_checkboxes">
									<label for="competence-{@id}">
										<xsl:call-template name="draw_competence_title"/>
									</label>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="draw_competence_title"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
						<xsl:if test="$show_bars">
							<td class="col-bars-">
								<div class="width-">
									<table class="bars" cellpadding="0" cellspacing="0">
										<xsl:apply-templates mode="tree_marks" select="."/>
									</table>
								</div>
							</td>
						</xsl:if>
						<xsl:apply-templates mode="tree_right_td" select=".">
							<xsl:with-param name="mark_details_url" select="$mark_details_url"/>
						</xsl:apply-templates>
					</tr>
				</xsl:for-each>
			</table>
			<div class="jq-tree-group-children jq-tree-group-{@id}-children" style="padding-left: {($level + 1) * 21}px;">
				<q class="dn">
					<xsl:value-of select="@id"/>
				</q>
				<xsl:call-template name="draw_tree_competence_group">
					<xsl:with-param name="competences" select="$competences"/>
					<xsl:with-param name="focus_competences" select="$focus_competences"/>
					<xsl:with-param name="competence_is_clickable" select="$competence_is_clickable"/>
					<xsl:with-param name="draw_checkboxes" select="$draw_checkboxes"/>
					<xsl:with-param name="show_bars" select="$show_bars"/>
					<xsl:with-param name="parent_node" select="."/>
					<xsl:with-param name="level" select="$level + 1"/>
				</xsl:call-template>
			</div>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="draw_tree_bar">
		<xsl:param name="type"/>
		<xsl:param name="mark"/>
		<xsl:param name="title"/>
		<xsl:param name="draw_delta" select="false()"/>
		<xsl:param name="need_mark" select="0"/>
		<!-- need_mark_title_dative (примеры): профессии | профилю &#171;Я в будущем&#187; -->
		<xsl:param name="need_mark_title_dative" select="''"/>
		<xsl:param name="block_class" select="''"/>
		<xsl:param name="first" select="false()"/>
		<xsl:param name="hide" select="false()"/>
		<xsl:variable name="delta_mark">
			<xsl:choose>
				<xsl:when test="$mark &lt; $need_mark">
					<xsl:value-of select="$need_mark - $mark"/>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<tr>
			<xsl:attribute name="class">
				<xsl:if test="$hide">dn </xsl:if>
				<xsl:if test="$first">first- </xsl:if>
				<xsl:text>type-</xsl:text>
				<xsl:value-of select="$type"/>
				<xsl:text>- </xsl:text>
				<xsl:text>width-</xsl:text>
				<xsl:value-of select="$mark"/>
				<xsl:text>-</xsl:text>
				<xsl:if test="$draw_delta">
					<xsl:value-of select="$delta_mark"/>
					<xsl:text>-</xsl:text>
				</xsl:if>
				<xsl:if test="$block_class != ''">
					<xsl:text> </xsl:text>
					<xsl:value-of select="$block_class"/>
				</xsl:if>
			</xsl:attribute>
			<td>
				<table>
					<tr>
						<td class="main-">
							<xsl:attribute name="title">
								<xsl:value-of select="$title"/>
								<xsl:text> (</xsl:text>
								<xsl:choose>
									<xsl:when test="$mark = 0">нет</xsl:when>
									<xsl:when test="$mark = 1">склонность</xsl:when>
									<xsl:when test="$mark = 2">способность</xsl:when>
									<xsl:when test="$mark = 3">компетенция</xsl:when>
								</xsl:choose>
								<xsl:text>)</xsl:text>
							</xsl:attribute>
							<div/>
						</td>
						<xsl:if test="$draw_delta and $delta_mark != 0">
							<td class="delta-">
								<xsl:attribute name="title">
									<xsl:text>Дефицит (сколько нужно развить для соответствия </xsl:text>
									<xsl:value-of select="$need_mark_title_dative"/>
									<xsl:text>)</xsl:text>
								</xsl:attribute>
								<div/>
							</td>
						</xsl:if>
					</tr>
				</table>
			</td>
		</tr>
	</xsl:template>
	<xsl:template mode="tree_right_th" match="group">
		<th class="col-details1-legend-">
			<div class="width-">
				<div class="title-" title="Количество личных (красный цвет) и групповых (синий цвет) оценок">
					<span>кол-во оценок</span>
				</div>
				<table class="title-" cellpadding="0" cellspacing="0">
					<tr>
						<td title="Склонность">Ск.</td>
						<td title="Способность">Сп.</td>
						<td title="Компетенция">К.</td>
					</tr>
				</table>
			</div>
		</th>
		<th class="col-details2-legend-">
			<div class="width-">
				<div class="title-" title="Количество свидетелей и событий для каждой компетенции">свиделей<br/>и событий</div>
			</div>
		</th>
	</xsl:template>
	<xsl:template mode="tree_right_td" match="competence">
		<xsl:param name="mark_details_url"/>
		<td class="col-details1-">
			<div class="width-">
				<xsl:for-each select="$tree_competences_detailed[@id = current()/@id]">
					<table class="marks" cellpadding="0" cellspacing="0">
						<tr class="personal-">
							<td>
								<a href="{$mark_details_url}" title="Количество личных оценок &#171;Склонность&#187;">
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="@marks_p1 &gt; 10">c10</xsl:when>
											<xsl:otherwise>
												<xsl:text>c</xsl:text>
												<xsl:value-of select="@marks_p1"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<xsl:if test="@marks_p1 != 0">
										<xsl:value-of select="@marks_p1"/>
									</xsl:if>
								</a>
							</td>
							<td>
								<a href="{$mark_details_url}" title="Количество личных оценок &#171;Способность&#187;">
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="@marks_p2 &gt; 10">c10</xsl:when>
											<xsl:otherwise>
												<xsl:text>c</xsl:text>
												<xsl:value-of select="@marks_p2"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<xsl:if test="@marks_p2 != 0">
										<xsl:value-of select="@marks_p2"/>
									</xsl:if>
								</a>
							</td>
							<td>
								<a href="{$mark_details_url}" title="Количество личных оценок &#171;Компетенция&#187;">
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="@marks_p3 &gt; 10">c10</xsl:when>
											<xsl:otherwise>
												<xsl:text>c</xsl:text>
												<xsl:value-of select="@marks_p3"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<xsl:if test="@marks_p3 != 0">
										<xsl:value-of select="@marks_p3"/>
									</xsl:if>
								</a>
							</td>
						</tr>
						<tr class="group-">
							<td>
								<a href="{$mark_details_url}" title="Количество групповых оценок &#171;Склонность&#187;">
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="@marks_g1 &gt; 10">c10</xsl:when>
											<xsl:otherwise>
												<xsl:text>c</xsl:text>
												<xsl:value-of select="@marks_g1"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<xsl:if test="@marks_g1 != 0">
										<xsl:value-of select="@marks_g1"/>
									</xsl:if>
								</a>
							</td>
							<td>
								<a href="{$mark_details_url}" title="Количество групповых оценок &#171;Способность&#187;">
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="@marks_g2 &gt; 10">c10</xsl:when>
											<xsl:otherwise>
												<xsl:text>c</xsl:text>
												<xsl:value-of select="@marks_g2"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<xsl:if test="@marks_g2 != 0">
										<xsl:value-of select="@marks_g2"/>
									</xsl:if>
								</a>
							</td>
							<td>
								<a href="{$mark_details_url}" title="Количество групповых оценок &#171;Компетенция&#187;">
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="@marks_g3 &gt; 10">c10</xsl:when>
											<xsl:otherwise>
												<xsl:text>c</xsl:text>
												<xsl:value-of select="@marks_g3"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<xsl:if test="@marks_g3 != 0">
										<xsl:value-of select="@marks_g3"/>
									</xsl:if>
								</a>
							</td>
						</tr>
					</table>
				</xsl:for-each>
			</div>
		</td>
		<td class="col-details2-">
			<div>
				<xsl:if test="$tree_competences_detailed[@id = current()/@id]">
					<xsl:for-each select="$tree_competences_detailed[@id = current()/@id]">
						<xsl:variable name="title">
							<xsl:value-of select="@raters"/>
							<xsl:call-template name="count_case">
								<xsl:with-param name="number" select="@raters"/>
								<xsl:with-param name="word_ns" select="' свидетель, '"/>
								<xsl:with-param name="word_gs" select="' свидетеля, '"/>
								<xsl:with-param name="word_ap" select="' свидетелей, '"/>
							</xsl:call-template>
							<xsl:value-of select="@precedents"/>
							<xsl:call-template name="count_case">
								<xsl:with-param name="number" select="@precedents"/>
								<xsl:with-param name="word_ns" select="' событие'"/>
								<xsl:with-param name="word_gs" select="' события'"/>
								<xsl:with-param name="word_ap" select="' событий'"/>
							</xsl:call-template>
						</xsl:variable>
						<a class="rater-" href="{$mark_details_url}" title="{$title}">
							<xsl:value-of select="@raters"/>
						</a>
						<a class="precedent-" href="{$mark_details_url}" title="{$title}">
							<xsl:value-of select="@precedents"/>
						</a>
						<!--
						<xsl:if test="@stat_type_own = 1">
							<span style="padding-left: 4px;" title="Есть оценки, полученные напрямую, без переводчика">Прям. </span>
							<br/>
						</xsl:if>
						<xsl:if test="@stat_type_translator = 1">
							<span style="padding-left: 4px;" title="Есть оценки, полученные через переводчик">Пер.</span>
							<br/>
						</xsl:if>
						-->
					</xsl:for-each>
				</xsl:if>
			</div>
		</td>
	</xsl:template>
</xsl:stylesheet>
