<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_admin_admins">
		<xsl:param name="save_ctrl"/>
		<xsl:param name="aux_params"/>
		<xsl:choose>
			<xsl:when test="$pass_info/error[@name = 'USER_NOT_FOUND']">
				<div class="error">
					<span>
						<xsl:text>Пользователя </xsl:text>
						<strong>
							<xsl:value-of select="$pass_info/error[@name = 'USER_NOT_FOUND']"/>
						</strong>
						<xsl:text> не существует. Проверьте, что вы корректно вводите логин.</xsl:text>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'ALREADY_ADMIN']">
				<div class="error">
					<span>
						<xsl:text>Пользователь </xsl:text>
						<strong>
							<xsl:value-of select="$pass_info/error[@name = 'ALREADY_ADMIN']"/>
						</strong>
						<xsl:text> уже является администратором</xsl:text>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/info[@name = 'ADMIN_ADDED']">
				<div class="info">
					<span>
						<xsl:text>Добавлен администратор </xsl:text>
						<strong>
							<xsl:value-of select="$pass_info/info[@name = 'ADMIN_ADDED']"/>
						</strong>
					</span>
				</div>
				<xsl:if test="$pass_info/info[@name = 'WAS_MODERATOR']">
					<div class="box box-warning">
						<div class="content-">
							<p style="padding: 8px 0;">Добавленный администратор перенесен из модераторов.</p>
						</div>
					</div>
				</xsl:if>
				<xsl:if test="$pass_info/info[@name = 'WAS_NOT_MEMBER']">
					<div class="box box-warning">
						<div class="content-">
							<p style="padding: 8px 0;">Добавленный администратор не был участником сообщества, но стал им, так как вы обладаете правами суперадминистратора.</p>
						</div>
					</div>
				</xsl:if>
			</xsl:when>
			<xsl:when test="$pass_info/info[@name = 'ADMIN_DELETED']">
				<div class="info">
					<span>
						<strong>
							<xsl:value-of select="$pass_info/info[@name = 'ADMIN_DELETED']"/>
						</strong>
						<xsl:text> теперь не администратор</xsl:text>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'NOT_MEMBER']">
				<div class="error">
					<span>
						<xsl:text>Пользователь </xsl:text>
						<strong>
							<xsl:value-of select="$pass_info/error[@name = 'NOT_MEMBER']"/>
						</strong>
						<xsl:text> </xsl:text>
						<xsl:text> &#8212; не участник сообщества, поэтому его нельзя сделать администратором. Попросите его сначала подключиться к сообществу.</xsl:text>
					</span>
				</div>
			</xsl:when>
		</xsl:choose>
		<form action="{$save_prefix}/{$save_ctrl}/" method="post">
			<div class="field">
				<div class="inline-">
					<input type="text" class="input-text narrow- short-" name="user_login" value="" jq-placeholder="Логин">
						<xsl:if test="$pass_info/error and $pass_info/vars/var[@name = 'action'] = 'add'">
							<xsl:attribute name="value">
								<xsl:value-of select="$pass_info/vars/var[@name = 'user_login']"/>
							</xsl:attribute>
						</xsl:if>
					</input>
				</div>
				<div class="inline-">
					<button class="button button-small">Добавить администратора</button>
				</div>
				<div class="clear"/>
			</div>
			<input type="hidden" name="action" value="add"/>
			<xsl:copy-of select="$aux_params"/>
		</form>
		<div id="jq-dialog-confirm" class="dn">
			<p>
				<xsl:text>Подтвердите: администратор </xsl:text>
				<strong id="jq-dialog-confirm-login">???</strong>
				<xsl:text> будет удалён.</xsl:text>
			</p>
		</div>
		<xsl:variable name="module" select="."/>
		<xsl:if test="not(admin)">
			<p>Администраторы не назначены.</p>
		</xsl:if>
		<xsl:if test="admin">
			<table class="tbl tbl-nohead-">
				<xsl:for-each select="admin">
					<tr>
						<xsl:for-each select="/root/user_short[@id = current()/@user_id]">
							<xsl:if test="@login = $pass_info/info[@name = 'ADMIN_ADDED'] and count($module/admin) &gt; 1">
								<xsl:attribute name="class">hl-</xsl:attribute>
							</xsl:if>
							<td class="col-user-avatar-small-">
								<span>
									<a href="{$prefix}/users/{@login}/">
										<img src="{photo_small/@url}" width="{photo_small/@width}" height="{photo_small/@height}" alt="{@login}"/>
									</a>
								</span>
							</td>
							<td class="col-user-name-small-">
								<div class="title-">
									<a name="user-{@login}" href="{$prefix}/users/{@login}/">
										<xsl:value-of select="@visible_name"/>
									</a>
								</div>
								<div class="second-">
									<xsl:value-of select="@login"/>
								</div>
							</td>
							<td class="col-icons-">
								<form action="{$save_prefix}/{$save_ctrl}/" method="post">
									<div class="icons">
										<a class="jq-delete-button delete- gray-" href="javascript:void(0);" data-user-id="{@id}" data-user-login="{@login}" title="Удалить администратора"/>
									</div>
									<input type="hidden" name="retpath" value="{$module_url}"/>
									<input type="hidden" name="action" value="delete"/>
									<input type="hidden" name="user_id" value="{@id}"/>
									<xsl:copy-of select="$aux_params"/>
								</form>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
		<xsl:apply-templates select="pages[page]"/>
	</xsl:template>
</xsl:stylesheet>
