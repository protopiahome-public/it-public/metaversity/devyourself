<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_rose">
		<xsl:param name="competences" select="rose_competences"/>
		<xsl:param name="image_alt"/>
		<xsl:param name="rose_param_name" select="'img'"/>
		<xsl:param name="marks_default_project_id" select="0"/>
		<xsl:param name="show_self_by_default" select="false()"/>
		<xsl:param name="focus_competences" select="/"/>
		<table class="rose" cellpadding="0" cellspacing="0">
			<tr>
				<xsl:if test="$competences/competence">
					<td class="rose-">
						<div class="rose-container-">
							<map id="match" name="match">
								<xsl:for-each select="$competences/competence">
									<xsl:variable name="title">
										<xsl:call-template name="draw_competence_title"/>
									</xsl:variable>
									<area shape="poly" coords="{@map_area_coords}" alt="" title="{$title}"/>
								</xsl:for-each>
							</map>
							<img class="project-cmp-rose" usemap="#match" alt="{$user_title}: {$image_alt}" title="">
								<xsl:attribute name="src">
									<xsl:choose>
										<xsl:when test="$show_self_by_default">
											<xsl:call-template name="url_replace_param">
												<xsl:with-param name="url_base" select="$module_url"/>
												<xsl:with-param name="param_name" select="$rose_param_name"/>
												<xsl:with-param name="param_value" select="1"/>
												<xsl:with-param name="param2_name" select="'self'"/>
												<xsl:with-param name="param2_value" select="1"/>
											</xsl:call-template>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="url_replace_param">
												<xsl:with-param name="url_base" select="$module_url"/>
												<xsl:with-param name="param_name" select="$rose_param_name"/>
												<xsl:with-param name="param_value" select="1"/>
											</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
							</img>
							<div class="project-cmp-rose-loading cover- dn">
								<div/>
							</div>
						</div>
					</td>
				</xsl:if>
				<td class="descr-">
					<xsl:if test="$competences/competence">
						<table class="tbl tbl-no-hor-lines-">
							<xsl:for-each select="$competences/competence">
								<xsl:variable name="mark_details_url">
									<xsl:value-of select="$prefix"/>
									<xsl:text>/users/</xsl:text>
									<xsl:value-of select="$current_user/@login"/>
									<xsl:text>/results/competence-</xsl:text>
									<xsl:value-of select="@id"/>
									<xsl:text>/</xsl:text>
									<xsl:value-of select="$request/@query_string"/>
									<xsl:if test="$marks_default_project_id != 0">
										<xsl:choose>
											<xsl:when test="$request/@query_string != ''">&amp;</xsl:when>
											<xsl:otherwise>?</xsl:otherwise>
										</xsl:choose>
										<xsl:text>default_project_id=</xsl:text>
										<xsl:value-of select="$marks_default_project_id"/>
									</xsl:if>
								</xsl:variable>
								<tr>
									<td>
										<a class="clickable" href="{$mark_details_url}">
											<xsl:if test="$focus_competences[@id = current()/@id]">
												<xsl:attribute name="style">font-weight: bold;</xsl:attribute>
												<xsl:attribute name="title">
													<xsl:choose>
														<xsl:when test="$focus_competences[@id = current()/@id]/vector">
															<xsl:text>Компетенция была в фокусе: </xsl:text>
															<xsl:for-each select="$focus_competences[@id = current()/@id]/vector">
																<xsl:if test="@project_id">
																	<xsl:text>проект &#171;</xsl:text>
																	<xsl:value-of select="/root/project_short[@id = current()/@project_id]/@title"/>
																	<xsl:text>&#187; (</xsl:text>
																	<xsl:call-template name="get_full_date">
																		<xsl:with-param name="datetime" select="@add_time"/>
																	</xsl:call-template>
																	<xsl:text>)</xsl:text>
																</xsl:if>
																<xsl:choose>
																	<xsl:when test="position() != last()">, </xsl:when>
																	<xsl:otherwise>.</xsl:otherwise>
																</xsl:choose>
															</xsl:for-each>
														</xsl:when>
														<xsl:otherwise>
															<xsl:text>Компетенция в фокусе</xsl:text>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:attribute>
											</xsl:if>
											<xsl:call-template name="draw_competence_title">
												<xsl:with-param name="red_number" select="position() = 1"/>
											</xsl:call-template>
										</a>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</xsl:if>
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>
