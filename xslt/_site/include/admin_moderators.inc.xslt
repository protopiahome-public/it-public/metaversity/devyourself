<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_admin_moderators">
		<xsl:param name="save_ctrl"/>
		<xsl:param name="aux_params"/>
		<xsl:choose>
			<xsl:when test="$pass_info/error[@name = 'USER_NOT_FOUND']">
				<div class="error">
					<span>
						<xsl:text>Пользователя </xsl:text>
						<xsl:if test="$pass_info/error[@name = 'USER_NOT_FOUND'] != ''">
							<strong>
								<xsl:value-of select="$pass_info/error[@name = 'USER_NOT_FOUND']"/>
							</strong>
							<xsl:text> </xsl:text>
						</xsl:if>
						<xsl:text>не существует. Проверьте, что вы корректно вводите логин.</xsl:text>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'ALREADY_MODERATOR']">
				<div class="error">
					<span>
						<xsl:text>Пользователь </xsl:text>
						<xsl:if test="$pass_info/error[@name = 'ALREADY_MODERATOR'] != ''">
							<strong>
								<xsl:value-of select="$pass_info/error[@name = 'ALREADY_MODERATOR']"/>
							</strong>
							<xsl:text> </xsl:text>
						</xsl:if>
						<xsl:text>уже является модератором</xsl:text>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'IS_ADMIN']">
				<div class="error">
					<span>
						<strong>
							<xsl:value-of select="$pass_info/error[@name = 'IS_ADMIN']"/>
						</strong>
						<xsl:text> &#8212; администратор и уже имеет полные права на модерирование</xsl:text>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/info[@name = 'MODERATOR_ADDED']">
				<div class="info">
					<span>
						<xsl:text>Добавлен модератор </xsl:text>
						<strong>
							<xsl:value-of select="$pass_info/info[@name = 'MODERATOR_ADDED']"/>
						</strong>
					</span>
				</div>
				<xsl:if test="$pass_info/info[@name = 'WAS_NOT_MEMBER']">
					<div class="box box-warning">
						<div class="content-">
							<p style="padding: 8px 0;">Добавленный модератор не был участником проекта, но стал им, так как вы обладаете правами суперадминистратора.</p>
						</div>
					</div>
				</xsl:if>
			</xsl:when>
			<xsl:when test="$pass_info/info[@name = 'RIGHTS_CHANGED']">
				<div class="info">
					<span>
						<xsl:text>Права модераторов изменены</xsl:text>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'NOT_MEMBER']">
				<div class="error">
					<span>
						<xsl:text>Пользователь </xsl:text>
						<xsl:if test="$pass_info/error[@name = 'NOT_MEMBER'] != ''">
							<strong>
								<xsl:value-of select="$pass_info/error[@name = 'NOT_MEMBER']"/>
							</strong>
							<xsl:text> </xsl:text>
						</xsl:if>
						<xsl:text>не является участником проекта.</xsl:text>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/info[@name = 'MODERATOR_DELETED']">
				<div class="info">
					<span>
						<strong>
							<xsl:value-of select="$pass_info/info[@name = 'MODERATOR_DELETED']"/>
						</strong>
						<xsl:text> теперь не модератор</xsl:text>
					</span>
				</div>
			</xsl:when>
		</xsl:choose>
		<form action="{$save_prefix}/{$save_ctrl}/" method="post">
			<div class="field">
				<div class="inline-">
					<input type="text" class="input-text narrow- short-" name="user_login" value="" jq-placeholder="Логин">
						<xsl:if test="$pass_info/error and $pass_info/vars/var[@name = 'action'] = 'add'">
							<xsl:attribute name="value">
								<xsl:value-of select="$pass_info/vars/var[@name = 'user_login']"/>
							</xsl:attribute>
						</xsl:if>
					</input>
				</div>
				<div class="inline-">
					<button class="button button-small">Добавить модератора</button>
				</div>
				<div class="clear"/>
			</div>
			<input type="hidden" name="action" value="add"/>
			<xsl:copy-of select="$aux_params"/>
		</form>
		<div id="jq-dialog-confirm" class="dn">
			<p>
				<xsl:text>Подтвердите: модератор </xsl:text>
				<strong id="jq-dialog-confirm-login">???</strong>
				<xsl:text> будет удалён.</xsl:text>
			</p>
		</div>
		<xsl:variable name="module" select="."/>
		<xsl:if test="not(moderator)">
			<p>Модераторы не назначены.</p>
		</xsl:if>
		<xsl:if test="moderator">
			<form action="{$save_prefix}/{$save_ctrl}/" method="post">
				<input type="hidden" name="action" value="edit"/>
				<xsl:copy-of select="$aux_params"/>
				<table class="tbl">
					<xsl:call-template name="draw_moderators_rights_headers"/>
					<xsl:for-each select="moderator">
						<xsl:variable name="current_user_rights" select="current()"/>
						<tr>
							<xsl:for-each select="/root/user_short[@id = current()/@user_id]">
								<xsl:if test="@login = $pass_info/info[@name = 'MODERATOR_ADDED'] and count($module/moderator) &gt; 1">
									<xsl:attribute name="class">hl-</xsl:attribute>
								</xsl:if>
								<td class="col-user-avatar-small-">
									<span>
										<a href="{$prefix}/users/{@login}/">
											<img src="{photo_small/@url}" width="{photo_small/@width}" height="{photo_small/@height}" alt="{@login}"/>
										</a>
									</span>
								</td>
								<td class="col-user-name-small-">
									<input type="hidden" name="rights[{@id}][exists]" value="1"/>
									<div class="title-">
										<a name="user-{@login}" href="{$prefix}/users/{@login}/">
											<xsl:value-of select="@visible_name"/>
										</a>
									</div>
									<div class="second-">
										<xsl:value-of select="@login"/>
									</div>
								</td>
							</xsl:for-each>
							<xsl:call-template name="draw_moderators_rights_checkboxes"/>
							<xsl:for-each select="/root/user_short[@id = current()/@user_id]">
								<td class="col-icons-">
									<div class="icons">
										<a class="jq-delete-button delete- gray-" href="javascript:void(0);" data-user-id="{@id}" data-user-login="{@login}" title="Удалить модератора"/>
									</div>
								</td>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</table>
				<div style="padding-top: 10px;">
					<button class="button button-small">Сохранить изменения</button>
				</div>
			</form>
		</xsl:if>
		<xsl:for-each select="moderator">
			<xsl:for-each select="/root/user_short[@id = current()/@user_id]">
				<form id="jq-delete-form-{@id}" action="{$save_prefix}/{$save_ctrl}/" method="post">
					<input type="hidden" name="retpath" value="{$module_url}"/>
					<input type="hidden" name="action" value="delete"/>
					<input type="hidden" name="user_id" value="{@id}"/>
					<xsl:copy-of select="$aux_params"/>
				</form>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:apply-templates select="pages[page]"/>
	</xsl:template>
</xsl:stylesheet>
