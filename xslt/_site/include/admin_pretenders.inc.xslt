<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_admin_pretenders">
		<xsl:param name="save_ctrl"/>
		<xsl:param name="item_id"/>
		<xsl:param name="aux_params"/>
		<xsl:param name="bulk_operations_threshold" select="3"/>
		<xsl:variable name="user_count" select="count(user)"/>
		<xsl:call-template name="draw_pretenders_not_found"/>
		<xsl:if test="user">
			<p>
				<xsl:text>Отображаются участники </xsl:text>
				<xsl:value-of select="pages/@from_row"/>
				<xsl:text> &#8212; </xsl:text>
				<xsl:value-of select="pages/@to_row"/>
				<xsl:text> из </xsl:text>
				<xsl:value-of select="pages/@row_count"/>
				<xsl:text>.</xsl:text>
			</p>
			<table class="tbl" id="jq-precedent-raters">
				<tr>
					<xsl:if test="$user_count &gt;= $bulk_operations_threshold">
						<th>
							<span>
								<input type="checkbox" class="input-checkbox check-all" data-check-all-class="jq-cb" name="all" value="1"/>
							</span>
						</th>
					</xsl:if>
					<xsl:if test="$current_project/@show_user_numbers = 1">
						<xsl:call-template name="draw_tbl_th">
							<xsl:with-param name="name" select="'number'"/>
							<xsl:with-param name="title" select="'#'"/>
						</xsl:call-template>
					</xsl:if>
					<th/>
					<th>
						<span>Логин</span>
					</th>
					<xsl:if test="$current_project/@use_game_name = 1">
						<th>
							<span>Игровое&#160;имя</span>
						</th>
					</xsl:if>
					<th>
						<span>Имя</span>
					</th>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'time'"/>
						<xsl:with-param name="title" select="'Дата&#160;запроса'"/>
					</xsl:call-template>
					<th/>
					<th/>
				</tr>
				<xsl:for-each select="user">
					<xsl:variable name="user_context" select="."/>
					<tr>
						<xsl:if test="$user_count &gt;= $bulk_operations_threshold">
							<td>
								<input type="checkbox" class="jq-cb input-checkbox" name="user-{@id}" value="1"/>
							</td>
						</xsl:if>
						<xsl:if test="$current_project/@show_user_numbers = 1">
							<td>
								<xsl:value-of select="@number"/>
							</td>
						</xsl:if>
						<xsl:for-each select="/root/user_short[@id = current()/@id][1]">
							<td class="col-user-avatar-small-">
								<span>
									<a href="{$prefix}/users/{@login}/">
										<img src="{photo_small/@url}" width="{photo_small/@width}" height="{photo_small/@height}" alt="{@login}"/>
									</a>
								</span>
							</td>
							<td>
								<a href="{$prefix}/users/{@login}/">
									<xsl:value-of select="@login"/>
								</a>
							</td>
							<xsl:if test="$current_project/@use_game_name = 1">
								<td>
									<xsl:value-of select="@game_name"/>
								</td>
							</xsl:if>
							<td>
								<xsl:value-of select="@visible_name"/>
							</td>
						</xsl:for-each>
						<td class="col-date-">
							<xsl:call-template name="get_full_datetime">
								<xsl:with-param name="datetime" select="@edit_time"/>
							</xsl:call-template>
						</td>
						<td class="col-button-">
							<button class="button button-small button-green" name="accept-user-{@id}">Принять</button>
						</td>
						<td class="col-button-">
							<button class="button button-small button-red" name="decline-user-{@id}">Отклонить</button>
						</td>
					</tr>
				</xsl:for-each>
			</table>
			<xsl:if test="$user_count &gt;= $bulk_operations_threshold">
				<div class="bulk-button-box">
					<button class="button button-small button-green" name="accept-all">Принять всех, отмеченных галочкой</button>
				</div>
				<div class="bulk-button-box">
					<button class="button button-small button-red" name="decline-all">Отклонить всех, отмеченных галочкой</button>
				</div>
			</xsl:if>
		</xsl:if>
		<xsl:apply-templates select="pages[page]"/>
	</xsl:template>
</xsl:stylesheet>
