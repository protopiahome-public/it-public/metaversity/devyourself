<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_access">
		<xsl:param name="status"/>
		<xsl:param name="entity_genitiv" select="'сущности'"/>
		<xsl:param name="join_premoderation" select="false()"/>
		<xsl:param name="special_moderator_entity_genitiv" select="'&lt;сущности, которая не указана разработчиками&gt;'"/>
		<xsl:param name="for" select="'Для получения доступа'"/>
		<xsl:param name="draw_instruction" select="true()"/>
		<p>
			<xsl:value-of select="$for"/>
			<xsl:text> необходимо стать </xsl:text>
			<xsl:choose>
				<xsl:when test="$status = 'user'">
					<strong>зарегистрированным пользователем</strong>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="$status = 'project_member'">
					<strong>
						<xsl:text>участником </xsl:text>
						<xsl:value-of select="$entity_genitiv"/>
					</strong>
					<xsl:text> или </xsl:text>
					<strong>
						<xsl:text>участником проекта </xsl:text>
						<a href="{$current_project/@url}">
							<xsl:value-of select="$current_project/@title"/>
						</a>
					</strong>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="$status = 'member'">
					<strong>
						<xsl:text>участником </xsl:text>
						<xsl:value-of select="$entity_genitiv"/>
					</strong>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="$status = 'moderator'">
					<strong>
						<xsl:text>модератором </xsl:text>
						<xsl:value-of select="$entity_genitiv"/>
					</strong>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="$status = 'special_moderator'">
					<strong>
						<xsl:text>модератором </xsl:text>
						<xsl:value-of select="$special_moderator_entity_genitiv"/>
					</strong>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="$status = 'admin'">
					<strong>
						<xsl:text>администратором </xsl:text>
						<xsl:value-of select="$entity_genitiv"/>
					</strong>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
		</p>
		<xsl:choose>
			<xsl:when test="not($draw_instruction)"/>
			<xsl:when test="$status = 'user'">
				<xsl:if test="not($user/@id)">
					<p>
						<xsl:call-template name="_draw_access_enter_capital"/>
						<xsl:text>.</xsl:text>
					</p>
				</xsl:if>
			</xsl:when>
			<xsl:when test="$status = 'project_member' or $status = 'member'">
				<p>
					<xsl:choose>
						<xsl:when test="$join_premoderation">Чтобы подать заявку на подключение</xsl:when>
						<xsl:otherwise>
							<xsl:text>Чтобы стать участником </xsl:text>
							<xsl:value-of select="$entity_genitiv"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>, </xsl:text>
					<xsl:if test="not($user/@id)">
						<xsl:call-template name="_draw_access_enter"/>
						<xsl:text> и затем </xsl:text>
					</xsl:if>
					<strong>нажмите на кнопку &#171;присоединиться&#187;</strong>
					<xsl:text> в шапке </xsl:text>
					<xsl:value-of select="$entity_genitiv"/>
					<xsl:text>.</xsl:text>
				</p>
			</xsl:when>
			<xsl:when test="$status = 'moderator' or $status = 'admin'">
				<xsl:if test="not($user/@id)">
					<p>
						<xsl:call-template name="_draw_access_enter_capital"/>
						<xsl:text>.</xsl:text>
					</p>
				</xsl:if>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="_draw_access_enter">
		<a href="{$login_url}">войдите</a>
		<xsl:text> или </xsl:text>
		<a href="{$reg_url}">зарегистрируйтесь</a>
	</xsl:template>
	<xsl:template name="_draw_access_enter_capital">
		<a href="{$login_url}">Войдите</a>
		<xsl:text> или </xsl:text>
		<a href="{$reg_url}">зарегистрируйтесь</a>
	</xsl:template>
</xsl:stylesheet>
