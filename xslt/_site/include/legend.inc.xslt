<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_legend">
		<!-- 'bars' or 'rose' -->
		<xsl:param name="type" select="'bars'"/>
		<xsl:param name="need_future" select="false()"/>
		<xsl:param name="need_prof" select="false()"/>
		<xsl:param name="sum" select="false()"/>
		<xsl:param name="sum_current" select="false()"/>
		<xsl:param name="delta" select="false()"/>
		<xsl:param name="self" select="false()"/>
		<xsl:param name="focus" select="false()"/>
		<xsl:param name="marks" select="false()"/>
		<div class="box">
			<div class="content-">
				<div class="legend">
					<div class="item-">
						<span>
							<strong>Ск., Сп., К.</strong> (Склонность, способность и компетенция) — это уровни развития компетенции.
						</span>
					</div>
					<xsl:if test="$need_future">
						<table class="item-">
							<tr>
								<td class="img-">
									<div class="legend-bar legend-bar-{$type}-need"/>
								</td>
								<td class="text-">
									<strong>— необходимый уровень</strong> для профессий из профиля «Я в будущем».
								</td>
							</tr>
						</table>
					</xsl:if>
					<xsl:if test="$need_prof">
						<table class="item-">
							<tr>
								<td class="img-">
									<div class="legend-bar legend-bar-{$type}-need"/>
								</td>
								<td class="text-">
									<strong>— уровень, необходимый для профессии</strong>.
								</td>
							</tr>
						</table>
					</xsl:if>
					<xsl:if test="$sum">
						<table class="item-">
							<tr>
								<td class="img-">
									<div class="legend-bar legend-bar-{$type}-sum"/>
								</td>
								<td class="text-">
									<strong>— текущий уровень</strong> (лучший результат из всех проектов).
								</td>
							</tr>
						</table>
					</xsl:if>
					<xsl:if test="$sum_current">
						<table class="item-">
							<tr>
								<td class="img-">
									<div class="legend-bar legend-bar-{$type}-sum"/>
								</td>
								<td class="text-">
									<strong>— результаты проекта</strong>.
								</td>
							</tr>
						</table>
					</xsl:if>
					<xsl:if test="$delta">
						<table class="item-">
							<tr>
								<td class="img-">
									<div class="legend-bar legend-bar-{$type}-delta"/>
								</td>
								<td class="text-">
									<strong>— дефицит</strong>.
								</td>
							</tr>
						</table>
					</xsl:if>
					<xsl:if test="$self">
						<table class="item-">
							<tr>
								<td class="img-">
									<div class="legend-bar legend-bar-{$type}-self"/>
								</td>
								<td class="text-">
									<strong>— самооценка</strong>.
								</td>
							</tr>
						</table>
					</xsl:if>
					<xsl:if test="$focus">
						<div class="item-">
							<span>
								<strong>Жирный шрифт</strong> — компетенция в фокусе.
							</span>
						</div>
					</xsl:if>
					<xsl:if test="$marks">
						<table class="item-">
							<tr>
								<td class="img-">
									<div class="legend-bar legend-bar-bars-pmarks"/>
								</td>
								<td class="text-">
									<strong>— личные оценки</strong> (чем больше оценок, тем ярче).
								</td>
							</tr>
						</table>
						<table class="item-">
							<tr>
								<td class="img-">
									<div class="legend-bar legend-bar-bars-gmarks"/>
								</td>
								<td class="text-">
									<strong>— групповые оценки</strong>, ставятся сразу нескольким участникам.
								</td>
							</tr>
						</table>
					</xsl:if>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
