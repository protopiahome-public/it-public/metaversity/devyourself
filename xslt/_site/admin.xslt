<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'admin'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/admin/')"/>
	<xsl:template match="admin">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Настройка инсталляции</h2>
							<xsl:call-template name="_draw_form"/>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<div class="content">
			<form action="{$save_prefix}/admin/" method="post" enctype="multipart/form-data">
				<xsl:call-template name="draw_dt_edit"/>
			</form>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Настройка инсталляции</xsl:text>
	</xsl:template>
</xsl:stylesheet>
