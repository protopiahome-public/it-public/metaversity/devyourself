<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="html" encoding="UTF-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
	<xsl:include href="base_common.xslt"/>
	<xsl:include href="base_head.xslt"/>
	<xsl:template match="text()" mode="head"/>
	<xsl:template match="text()" mode="left_menu"/>
	<xsl:template match="/root">
		<html>
			<head>
				<title>
					<xsl:apply-templates mode="title" select="/root"/>
				</title>
				<link rel="stylesheet" type="text/css" href="{$prefix}/css/main.css?v1942"/>
				<link rel="stylesheet" type="text/css" href="{$prefix}/css/jquery-ui.css"/>
				<link rel="stylesheet" type="text/css" href="{$prefix}/css/jqtree.css"/>
				<xsl:text disable-output-escaping="yes">&lt;!--[if lt IE 9]&gt; </xsl:text>
				<link href="{$prefix}/css/ie.css" type="text/css" rel="stylesheet"/> 
				<xsl:text disable-output-escaping="yes"> &lt;![endif]--&gt;</xsl:text>
				<script type="text/javascript">
					var _gaq = _gaq || [];
				</script>
				<xsl:if test="$request/@main_host_name and not($debug) and $sys_params/@ga_tracking_id != ''">
					<xsl:variable name="ga_domain_name">
						<xsl:choose>
							<xsl:when test="$request/@is_project_domain = 1 and /root/project_short[1]/@domain_is_on = 1">
								<xsl:value-of select="/root/project_short[1]/@domain_name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$request/@main_host_name"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<script type="text/javascript">
						_gaq.push(['_setAccount', '<xsl:value-of select="$sys_params/@ga_tracking_id"/>']);
						_gaq.push(['_setDomainName', '<xsl:value-of select="$ga_domain_name"/>']);
						_gaq.push(['_setAllowLinker', true]);
						_gaq.push(['_trackPageview']);
						
						(function() {
							var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
							ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
							var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
						})();
						
						window.onerror = function(msg, url, line) {
							var preventErrorAlert = true;
							_gaq.push(['_trackEvent', 'JS Error', msg, navigator.userAgent + ' -- ' + url + " : " + line + ' -- ' + location.href, 0, true]);
							return preventErrorAlert;
						};
					</script>
				</xsl:if>
				<script type="text/javascript" src="{$prefix}/js/head.js"> </script>
				<xsl:if test="$request/@user_agent = 'ie' and $request/@user_agent_version &lt; 10">
					<script type="text/javascript" src="{$prefix}/js/PIE.js"> </script>
				</xsl:if>
				<xsl:if test="$debug">
					<script type="text/javascript" src="{$prefix}/js/debug.js"> </script>
				</xsl:if>
				<xsl:apply-templates mode="jquery" select="/root"/>
				<xsl:if test="$request/@main_host_name">
					<script type="text/javascript" src="{$prefix}/js/ga_urls.js"> </script>
				</xsl:if>
				<xsl:if test="not($debug)">
					<script type="text/javascript">
						jQuery.error = function(message) {
							_gaq.push(['_trackEvent', 'jQuery Error', message, navigator.userAgent, 0, true]);
						}
					</script>
				</xsl:if>
				<script type="text/javascript" src="{$prefix}/js/jquery.cookie.js"> </script>
				<script type="text/javascript" src="{$prefix}/js/jquery.placeholder.js"> </script>
				<script type="text/javascript" src="{$prefix}/js/jquery.adjust.js"> </script>
				<script type="text/javascript" src="{$prefix}/js/jquery.mouse_hint.js"> </script>
				<script type="text/javascript" src="{$prefix}/js/common.js"> </script>
				<script type="text/javascript" src="{$prefix}/js/filter_main.js"> </script>
				<script type="text/javascript" src="{$prefix}/js/xpost.js"> </script>
				<script type="text/javascript" src="{$prefix}/js/xlightbox.js"> </script>
				<script type="text/javascript">
					var global = {
						prefix: '<xsl:if test="not($request/@is_project_domain = 1)">
						<xsl:value-of select="$prefix"/>
					</xsl:if>/',
						main_host_name: '<xsl:value-of select="$request/@main_host_name"/>',
						ajax_prefix: '<xsl:value-of select="$ajax_prefix"/>/',
						session_cookie_name: 'X_SESSION_ID6'
					}
				</script>
				<xsl:apply-templates mode="head" select="/root"/>
				<xsl:apply-templates mode="favicon" select="/root"/>
				<script type="text/javascript" src="{$prefix}/js/lang/ru.js"> </script>
				<xsl:if test="$debug">
					<script type="text/javascript" src="{$prefix}/js/debug_auxil.js"> </script>
				</xsl:if>
			</head>
			<body>
				<div id="main">
					<!--xsl:if test="/root/project_intmenu/link">
						<div class="intmenu">
							<ul class="list-">
								<li class="project-name-">
									<div>
										<xsl:value-of select="/root/project_intmenu/@project_title"/>
									</div>
								</li>
								<xsl:for-each select="/root/project_intmenu/link">
									<li class="item-">
										<a href="{@url}">
											<xsl:value-of select="@title"/>
										</a>
									</li>
								</xsl:for-each>
							</ul>
						</div>
					</xsl:if-->
					<xsl:if test="$debug">
						<div id="debug"/>
					</xsl:if>
					<xsl:call-template name="draw_head"/>
					<xsl:apply-templates/>
					<xsl:call-template name="draw_footer"/>
					<xsl:call-template name="draw_debug_info"/>
				</div>
				<xsl:apply-templates mode="ajax_error" select="/root"/>
				<xsl:apply-templates mode="ajax_error_bad_rights" select="/root"/>
				<div id="jq-xpost-loading" class="xpost-loading dn">
					<div class="content-"/>
				</div>
				<div id="mouse_hint"/>
				<div class="color-green">
					<div id="jq-lightbox" class="lightbox dn">
						<div class="overlay-"/>
						<div id="jq-lightbox-dialog-box" class="dialog-box-">
							<div id="jq-lightbox-dialog" class="dialog-">
								<div class="head-">
									<div id="jq-lightbox-close" class="close-"/>
									<div id="jq-lightbox-title" class="title-"/>
								</div>
								<div class="main-">
									<div id="jq-lightbox-content" class="content-"/>
									<div id="jq-lightbox-button-box" class="button-box-">
										<button class="button button-small" id="jq-lightbox-button"/>
										<button class="button button-right button-gray button-small" id="jq-lightbox-button-cancel">Отмена</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$sys_params/@title"/>
	</xsl:template>
	<xsl:template mode="favicon" match="/root">
		<link rel="shortcut icon" type="image/gif" href="{$prefix}/favicon.png"/>
	</xsl:template>
	<xsl:template mode="jquery" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery.js"> </script>
	</xsl:template>
	<xsl:template mode="ajax_error" match="/root">
		<div id="jq-error" class="dn" title="Произошла ошибка">
			<p>При отправке данных на сервер произошла ошибка. Проверьте соединение с интернетом и попробуйте перезагрузить страницу.</p>
		</div>
	</xsl:template>
	<xsl:template mode="ajax_error_bad_rights" match="/root">
		<div id="jq-error-rights" class="dn" title="Ошибка прав доступа">
			<p>У Вас не хватает прав на выполнение операции. Данные не были сохранены.</p>
		</div>
	</xsl:template>
</xsl:stylesheet>
