<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="top_section" select="'bad_domain'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/projects/')"/>
	<xsl:template match="unknown_host">
		<div class="content page404">
			<div class="head2-">
				<xsl:text>Запрошенный домен не зарегистрирован на </xsl:text>
				<xsl:choose>
					<xsl:when test="$sys_params/@is_devyourself = 1">DevYourself</xsl:when>
					<xsl:otherwise>
						<xsl:text>проекте </xsl:text>
						<xsl:value-of select="$sys_params/@title"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<div class="text-">
				<p>— Проверьте адрес в адресной строке.</p>
				<p>— Попробуйте найти нужный проект в <a href="{$prefix}/projects/">списке проектов</a>.</p>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Неверный домен &#8212; сервер '"/>
		<xsl:choose>
			<xsl:when test="$sys_params/@is_devyourself = 1">DevYourself</xsl:when>
			<xsl:otherwise>
				<xsl:text>проекта </xsl:text>
				<xsl:value-of select="$sys_params/@title"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
