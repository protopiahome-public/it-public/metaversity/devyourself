<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_head">
		<div id="head">
			<xsl:if test="$user/@id">
				<div class="user-dd-container-">
					<div id="head-user-dd" class="user-dd-">
						<div class="user-dd-head-">
							<xsl:value-of select="concat('Вы ', $user/@visible_name)"/>
						</div>
						<div class="user-dd-link-">
							<a href="{$prefix}/users/{$user/@login}/">Личная страница</a>
						</div>
						<div class="user-dd-link-">
							<a href="{$prefix}/settings/">Редактировать профиль</a>
						</div>
						<div class="user-dd-link-">
							<a href="{$logout_url}">Выход</a>
						</div>
					</div>
				</div>
			</xsl:if>
			<div class="head-outer-">
				<xsl:apply-templates select="/root" mode="head_left_left"/>
				<xsl:choose>
					<xsl:when test="$user/@id">
						<div class="r- user-">
							<a id="head-user" href="{$prefix}/users/{$user/@login}/">
								<span>
									<xsl:value-of select="$user/@login"/>
								</span>
								<xsl:choose>
									<xsl:when test="$user/@photo_small_width != 0">
										<img src="{$user/@photo_small_url}" alt="" width="{$user/@photo_small_width}" height="{$user/@photo_small_height}"/>
									</xsl:when>
									<xsl:otherwise>
										<img src="{$prefix}/img/defaults/user-small-default.jpg" alt="" width="24" height="24"/>
									</xsl:otherwise>
								</xsl:choose>
							</a>
						</div>
						<xsl:choose>
							<xsl:when test="/root/user_head/@awaiting_event_count &gt; 0">
								<div class="r- ico- ico-number-">
									<a href="{$prefix}/users/{$user/@login}/events/archive/" class="ico-calendar-">
										<xsl:attribute name="title">
											<xsl:text>Ваш календарь событий (</xsl:text>
											<xsl:value-of select="/root/user_head/@awaiting_event_count"/>
											<xsl:call-template name="count_case">
												<xsl:with-param name="number" select="/root/user_head/@awaiting_event_count"/>
												<xsl:with-param name="word_ns" select="' событие закончено'"/>
												<xsl:with-param name="word_gs" select="' события зкончены'"/>
												<xsl:with-param name="word_ap" select="' событий зкончено'"/>
											</xsl:call-template>
											<xsl:text>)</xsl:text>
										</xsl:attribute>
										<span>
											<xsl:value-of select="/root/user_head/@awaiting_event_count"/>
										</span>
									</a>
								</div>
							</xsl:when>
							<xsl:otherwise>
								<div class="r- ico-">
									<a href="{$prefix}/users/{$user/@login}/events/" title="Ваш календарь событий" class="ico-calendar-"/>
								</div>
							</xsl:otherwise>
						</xsl:choose>
						<div class="r- ico-">
							<a href="{$prefix}/users/{$user/@login}/materials/" title="Материалы для изучения" class="ico-read-"/>
						</div>
						<!--div class="r- ico- ico-number-">
						<a href="#" title="Персональные сообщения" class="ico-pm-">
							<span>99?</span>
						</a>
					</div-->
					</xsl:when>
					<xsl:otherwise>
						<div class="r- r-link-">
							<a href="{$login_url}">вход</a>
						</div>
						<div class="r- r-link-">
							<a href="{$reg_url}">регистрация</a>
						</div>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:apply-templates select="/root" mode="head_left_right"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="/root" mode="head_left_left">
		<xsl:variable name="logo_content">
			<xsl:choose>
				<xsl:when test="$sys_params/@is_devyourself = 1">
					<img src="{$prefix}/img/logo.gif" alt="DevYourself" title="" width="108" height="16"/>
				</xsl:when>
				<xsl:when test="$sys_params/logo_main/@is_uploaded = 1">
					<img style="margin-top: {(30 - $sys_params/logo_main/@height) div 2}px" src="{$sys_params/logo_main/@url}" alt="{$sys_params/@title}" title="" width="{$sys_params/logo_main/@width}" height="{$sys_params/logo_main/@height}"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$sys_params/@title"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<div class="l- logo-">
			<xsl:choose>
				<xsl:when test="$top_section = 'main' and $top_section_main_page and not($get_vars)">
					<xsl:copy-of select="$logo_content"/>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$prefix}/">
						<xsl:copy-of select="$logo_content"/>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template match="/root" mode="head_left_right">
		<div>
			<xsl:attribute name="class">
				<xsl:text>l- link-</xsl:text>
				<xsl:if test="$top_section = 'users'"> link-current-</xsl:if>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="$top_section = 'users' and $top_section_main_page">Люди</xsl:when>
				<xsl:otherwise>
					<a href="{$prefix}/users/">Люди</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<div>
			<xsl:attribute name="class">
				<xsl:text>l- link-</xsl:text>
				<xsl:if test="$top_section = 'projects'"> link-current-</xsl:if>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="$top_section = 'projects' and $top_section_main_page">Проекты</xsl:when>
				<xsl:otherwise>
					<a href="{$prefix}/projects/">Проекты</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<div>
			<xsl:attribute name="class">
				<xsl:text>l- link-</xsl:text>
				<xsl:if test="$top_section = 'sets'"> link-current-</xsl:if>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="$top_section = 'sets' and $top_section_main_page">Компетенции</xsl:when>
				<xsl:otherwise>
					<a href="{$prefix}/sets/">Компетенции</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<div>
			<xsl:attribute name="class">
				<xsl:text>l- link-</xsl:text>
				<xsl:if test="$top_section = 'ratings'"> link-current-</xsl:if>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="$top_section = 'ratings' and $top_section_main_page">Рейтинги</xsl:when>
				<xsl:otherwise>
					<a href="{$prefix}/ratings/">Рейтинги</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<xsl:if test="$user/@is_admin = 1">
			<div>
				<xsl:attribute name="class">
					<xsl:text>l- link-</xsl:text>
					<xsl:if test="$top_section = 'admin'"> link-current-</xsl:if>
				</xsl:attribute>
				<xsl:choose>
					<xsl:when test="$top_section = 'admin' and $top_section_main_page">Инсталляция</xsl:when>
					<xsl:otherwise>
						<a href="{$prefix}/admin/">Инсталляция</a>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
