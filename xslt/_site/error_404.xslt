<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="top_section" select="'page404'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/404/')"/>
	<xsl:template match="error_404">
		<div class="content page404">
			<div class="head1-">404</div>
			<div class="head2-">Страница не найдена</div>
			<div class="text-">
				<p>— Проверьте адрес в адресной строке.</p>
				<p>— Попробуйте найти нужную информацию через верхнее меню.</p>
			</div>
			<div class="text-">
				<div class="head3-">Сообщите об ошибке и сделайте мир лучше</div>
				<p>— Вы перешли по ссылке с другого сайта? Сообщите тому, кто разместил ссылку.</p>
				<p>— Неверная ссылка на нашем сайте? Напишите на <a class="email" href="mailto:{$sys_params/@info_email_obscured}?subject=Broken%20link">
					<xsl:value-of select="$sys_params/@info_email_obscured"/>
				</a>.</p>
				<p>— Не уверены, кто виноват? Не беспокойтесь, мы ведём учёт посещений<br/>404-х страниц и исправляем наиболее серьёзные ошибки.</p>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			var referrer = '<xsl:value-of select="$request/@referrer"/>';
			_gaq.push(['_trackEvent', 'Error 404', location.href, referrer ? referrer : ':direct:', 0, true]);
		</script>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Ошибка 404: страница не найдена'"/>
	</xsl:template>
</xsl:stylesheet>
