<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="top_section" select="'main'"/>
	<xsl:variable name="top_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($prefix, '/projects/')"/>
	<xsl:template match="main">
		<div class="color-yellow">
			<div class="content content-about">
				<xsl:copy-of select="main_page_text/div"/>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
