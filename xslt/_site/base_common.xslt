<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../_core/auxil/datetime.inc.xslt"/>
	<xsl:include href="../_core/auxil/count_case.inc.xslt"/>
	<xsl:include href="../_core/auxil/file_size.inc.xslt"/>
	<xsl:decimal-format decimal-separator="." grouping-separator=" "/>
	<xsl:variable name="request" select="/root/request"/>
	<xsl:variable name="sys_params" select="/root/sys_params"/>
	<xsl:variable name="get_vars" select="/root/request/params/get/var"/>
	<xsl:variable name="prefix" select="/root/request/@prefix"/>
	<xsl:variable name="save_prefix" select="/root/request/@save_prefix"/>
	<xsl:variable name="ajax_prefix" select="/root/request/@ajax_prefix"/>
	<xsl:variable name="local_prefix" select="/root/request/@local_prefix"/>
	<xsl:variable name="debug" select="/root/request/@debug = 1"/>
	<xsl:variable name="pass_info" select="/root/pass_info"/>
	<xsl:variable name="user" select="/root/user"/>
	<xsl:template match="text()"/>
	<xsl:variable name="auth_url_project_param">
		<xsl:choose>
			<xsl:when test="/root/login/@project_id">
				<xsl:text>&amp;project=</xsl:text>
				<xsl:value-of select="/root/login/@project_id"/>
				<xsl:if test="$get_vars[@name = 'can_cancel'] = 1">&amp;can_cancel=1</xsl:if>
			</xsl:when>
			<xsl:when test="/root/register/@project_id">
				<xsl:text>&amp;project=</xsl:text>
				<xsl:value-of select="/root/register/@project_id"/>
				<xsl:if test="$get_vars[@name = 'can_cancel'] = 1">&amp;can_cancel=1</xsl:if>
			</xsl:when>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="reg_url">
		<xsl:choose>
			<xsl:when test="$request/@is_project_domain = 1">
				<xsl:value-of select="concat($prefix, '/auth/?do=reg&amp;retpath=', $request/@request_url_escaped)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($prefix, '/reg/', '?retpath=', $request/@retpath_escaped, $auth_url_project_param)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="login_url">
		<xsl:choose>
			<xsl:when test="$request/@is_project_domain = 1">
				<xsl:value-of select="concat($prefix, '/auth/?do=login&amp;retpath=', $request/@request_url_escaped)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($prefix, '/login/', '?retpath=', $request/@retpath_escaped, $auth_url_project_param)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="logout_url">
		<xsl:choose>
			<xsl:when test="$request/@is_project_domain = 1">
				<xsl:value-of select="concat($prefix, '/auth/?do=logout&amp;retpath=', $request/@domain_prefix, '/')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($prefix, '/save/logout/')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template name="draw_nav">
		<div class="widget-wrap">
			<div class="widget widget-nav">
				<div class="header-">
					<table cellspacing="0">
						<tr>
							<td>
								<h2 class="text-">Навигация</h2>
							</td>
						</tr>
					</table>
				</div>
				<div class="content-">
					<xsl:apply-templates mode="nav" select="/root"/>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="draw_tbl_th">
		<xsl:param name="name"/>
		<xsl:param name="title"/>
		<xsl:param name="center" select="false()"/>
		<xsl:param name="no_links" select="false()"/>
		<th class="sortable-">
			<xsl:if test="$center">
				<xsl:attribute name="class">sortable- center-</xsl:attribute>
			</xsl:if>
			<span>
				<xsl:if test="sort/@order = $name">
					<xsl:attribute name="class">
						<xsl:text>selected-</xsl:text>
						<xsl:if test="sort/@back = 1"> sort-back-</xsl:if>
					</xsl:attribute>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$no_links">
						<xsl:value-of select="$title"/>
					</xsl:when>
					<xsl:otherwise>
						<a>
							<xsl:attribute name="href">
								<xsl:call-template name="url_replace_param">
									<xsl:with-param name="url_base" select="$module_url"/>
									<xsl:with-param name="param_name" select="'sort'"/>
									<xsl:with-param name="param_value" select="$name"/>
									<xsl:with-param name="param2_name" select="'sort_back'"/>
									<xsl:with-param name="param2_value">
										<xsl:choose>
											<xsl:when test="sort/@order = $name and sort/@back = 0">1</xsl:when>
											<xsl:otherwise>0</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:attribute>
							<xsl:value-of select="$title"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</span>
		</th>
	</xsl:template>
	<xsl:template name="draw_competence_title">
		<xsl:param name="competence" select="."/>
		<xsl:param name="red_number" select="false()"/>
		<xsl:choose>
			<xsl:when test="$red_number">
				<span class="red">
					<xsl:value-of select="$competence/@id"/>
					<xsl:text>.</xsl:text>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$competence/@id"/>
				<xsl:text>.</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> </xsl:text>
		<xsl:variable name="title">
			<xsl:choose>
				<xsl:when test="$competence/@title">
					<xsl:value-of select="$competence/@title"/>
				</xsl:when>
				<xsl:when test="/root/competence_short[@id = $competence/@id]/@title">
					<xsl:value-of select="/root/competence_short[@id = $competence/@id]/@title"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="/root/competence_set_competences//competence[@id = $competence/@id]/@title"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$title"/>
		<xsl:if test="substring($title, string-length($title), 1) != '.'">.</xsl:if>
	</xsl:template>
	<xsl:template name="draw_project_state">
		<xsl:param name="state"/>
		<xsl:param name="start_low_case" select="false()"/>
		<span class="project-state-{$state}">
			<xsl:choose>
				<xsl:when test="$start_low_case">проект </xsl:when>
				<xsl:otherwise>Проект </xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="$state = 'NOT_ST'">не начался</xsl:when>
				<xsl:when test="$state = 'RUNNING'">активен</xsl:when>
				<xsl:when test="$state = 'FINISHED'">закончен</xsl:when>
			</xsl:choose>
		</span>
	</xsl:template>
	<xsl:template name="draw_mark_count_main">
		<xsl:param name="position" select="0"/>
		<xsl:param name="with_trans" select="false()"/>
		<xsl:variable name="total_mark_count_calc">
			<xsl:choose>
				<xsl:when test="$with_trans">
					<xsl:value-of select="@total_mark_count_with_trans_calc"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@total_mark_count_calc"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$total_mark_count_calc"/>
		<xsl:if test="$position = 1">
			<xsl:call-template name="count_case">
				<xsl:with-param name="number" select="$total_mark_count_calc"/>
				<xsl:with-param name="word_ns" select="' оценка'"/>
				<xsl:with-param name="word_gs" select="' оценки'"/>
				<xsl:with-param name="word_ap" select="' оценок'"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="draw_mark_count_details">
		<xsl:param name="with_trans" select="false()"/>
		<xsl:variable name="personal_mark_count_calc">
			<xsl:choose>
				<xsl:when test="$with_trans">
					<xsl:value-of select="@personal_mark_count_with_trans_calc"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@personal_mark_count_calc"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="group_mark_count_calc">
			<xsl:choose>
				<xsl:when test="$with_trans">
					<xsl:value-of select="@group_mark_count_with_trans_calc"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@group_mark_count_calc"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="group_mark_raw_count_calc">
			<xsl:choose>
				<xsl:when test="$with_trans">
					<xsl:value-of select="@group_mark_raw_count_with_trans_calc"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@group_mark_raw_count_calc"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$personal_mark_count_calc"/>
		<xsl:call-template name="count_case">
			<xsl:with-param name="number" select="$personal_mark_count_calc"/>
			<xsl:with-param name="word_ns" select="' личная оценка + '"/>
			<xsl:with-param name="word_gs" select="' личные оценки + '"/>
			<xsl:with-param name="word_ap" select="' личных оценок + '"/>
		</xsl:call-template>
		<xsl:value-of select="$group_mark_count_calc"/>
		<xsl:if test="$group_mark_count_calc &gt; 0 and $group_mark_raw_count_calc != ''">
			<xsl:text> (</xsl:text>
			<xsl:value-of select="$group_mark_raw_count_calc"/>
			<xsl:text>)</xsl:text>
		</xsl:if>
		<xsl:call-template name="count_case">
			<xsl:with-param name="number" select="$group_mark_count_calc"/>
			<xsl:with-param name="word_ns" select="' групповая'"/>
			<xsl:with-param name="word_gs" select="' групповых'"/>
			<xsl:with-param name="word_ap" select="' групповых'"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="draw_mark_count_full">
		<xsl:param name="with_trans" select="false()"/>
		<xsl:call-template name="draw_mark_count_main">
			<xsl:with-param name="position" select="1"/>
			<xsl:with-param name="with_trans" select="$with_trans"/>
		</xsl:call-template>
		<xsl:variable name="total_mark_count_calc">
			<xsl:choose>
				<xsl:when test="$with_trans">
					<xsl:value-of select="@total_mark_count_with_trans_calc"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@total_mark_count_calc"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="$total_mark_count_calc &gt; 0">
			<xsl:text> (</xsl:text>
			<xsl:call-template name="draw_mark_count_details">
				<xsl:with-param name="with_trans" select="$with_trans"/>
			</xsl:call-template>
			<xsl:text>)</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template name="draw_mark_count_hint">
		<xsl:param name="position" select="0"/>
		<xsl:param name="with_trans" select="false()"/>
		<xsl:attribute name="title">
			<xsl:call-template name="draw_mark_count_details">
				<xsl:with-param name="with_trans" select="$with_trans"/>
			</xsl:call-template>
		</xsl:attribute>
		<xsl:call-template name="draw_mark_count_main">
			<xsl:with-param name="position" select="$position"/>
			<xsl:with-param name="with_trans" select="$with_trans"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="draw_filter">
		<xsl:param name="competence_set_id"/>
		<xsl:param name="read_only" select="false()"/>
		<xsl:param name="go_url" select="''"/>
		<xsl:param name="show_filter_if_no_marks" select="true()"/>
		<xsl:for-each select="/root/filter[@competence_set_id = $competence_set_id and ($show_filter_if_no_marks or @used_mark_count &gt; 0)]">
			<div class="widget-wrap">
				<div class="widget widget-filter">
					<div class="header-">
						<table cellspacing="0">
							<tr>
								<td>
									<h2 class="text-">Фильтр оценок</h2>
								</td>
							</tr>
						</table>
					</div>
					<form id="filter_main" class="" action="{$module_url}" method="get">
						<xsl:for-each select="$get_vars[substring(@name, 1, 8) != 'project_' and substring(@name, 1, 11) != 'translator_' and substring(@name, 1, 19) != 'rater_user_groups']">
							<xsl:call-template name="get_param_for_form"/>
						</xsl:for-each>
						<xsl:if test="@used_mark_count">
							<div class="mark-count-">
								<div class="caption-">
									<xsl:call-template name="count_case">
										<xsl:with-param name="number" select="@used_mark_count"/>
										<xsl:with-param name="word_ns" select="'Для расчёта использована'"/>
										<xsl:with-param name="word_gs" select="'Для расчёта использованы'"/>
										<xsl:with-param name="word_ap" select="'Для расчёта использовано'"/>
									</xsl:call-template>
								</div>
								<div class="number-">
									<span class="digits-">
										<xsl:value-of select="@used_mark_count"/>
									</span>
									<span class="text-">
										<xsl:call-template name="count_case">
											<xsl:with-param name="number" select="@used_mark_count"/>
											<xsl:with-param name="word_ns" select="' оценка'"/>
											<xsl:with-param name="word_gs" select="' оценки'"/>
											<xsl:with-param name="word_ap" select="' оценок'"/>
										</xsl:call-template>
									</span>
								</div>
							</div>
						</xsl:if>
						<xsl:if test="competence_sets/competence_set/projects/project">
							<div class="subhead-">Проекты, участвующие в расчетах</div>
						</xsl:if>
						<div class="block- projects-">
							<xsl:for-each select="competence_sets/competence_set">
								<xsl:if test="projects/project[@is_selected = 1] or not($read_only)">
									<xsl:variable name="src_competence_set_id" select="@id"/>
									<xsl:choose>
										<xsl:when test="$src_competence_set_id != $competence_set_id">
											<div class="translator-">
												<div class="subtitle-">
													<span class="clickable-">
														<xsl:text>Из набора компетенций </xsl:text>
														<strong>
															<xsl:value-of select="translators/translator/@from_competence_set_title"/>
														</strong>
														<xsl:text>:</xsl:text>
													</span>
												</div>
												<div class="expandee- dn">
													<div class="text-">Переводчик результатов:</div>
													<div class="select-">
														<select name="translator_{$src_competence_set_id}" title="translator_{$src_competence_set_id}">
															<xsl:if test="$read_only">
																<xsl:attribute name="disabled">disabled</xsl:attribute>
															</xsl:if>
															<xsl:for-each select="translators/translator">
																<option value="{@translator_id}">
																	<xsl:choose>
																		<xsl:when test="@is_selected = 1">
																			<xsl:attribute name="selected">selected</xsl:attribute>
																		</xsl:when>
																	</xsl:choose>
																	<xsl:value-of select="@translator_title"/>
																</option>
															</xsl:for-each>
														</select>
													</div>
												</div>
											</div>
										</xsl:when>
										<xsl:otherwise>
											<div class="subtitle-">Результаты, полученные &#171;напрямую&#187;:</div>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:for-each select="projects/project">
										<div class="cb-">
											<xsl:if test="not($read_only)">
												<input type="checkbox" id="project_{$src_competence_set_id}_{@id}" name="project_{$src_competence_set_id}_{@id}" title="project_{$src_competence_set_id}_{@id}" value="1">
													<xsl:if test="@is_selected = 1">
														<xsl:attribute name="checked">checked</xsl:attribute>
													</xsl:if>
												</input>
											</xsl:if>
											<label for="project_{$src_competence_set_id}_{@id}">
												<xsl:value-of select="@title"/>
												<span class="hl">
													<xsl:text> (</xsl:text>
													<xsl:value-of select="@total_mark_count"/>
													<xsl:if test="position() = 1">
														<xsl:call-template name="count_case">
															<xsl:with-param name="number" select="@total_mark_count"/>
															<xsl:with-param name="word_ns" select="' оценка'"/>
															<xsl:with-param name="word_gs" select="' оценки'"/>
															<xsl:with-param name="word_ap" select="' оценок'"/>
														</xsl:call-template>
													</xsl:if>
													<xsl:text>)</xsl:text>
												</span>
											</label>
										</div>
									</xsl:for-each>
								</xsl:if>
							</xsl:for-each>
							<xsl:if test="$read_only and not(competence_sets/competence_set/projects/project[@is_selected = 1])">
								<div>Нет</div>
							</xsl:if>
							<xsl:if test="$read_only and competence_sets/competence_set/projects/project[not(@is_selected = 1)]">
								<div>+ возможно подключение других проектов.</div>
							</xsl:if>
						</div>
						<xsl:if test="$read_only and $go_url != ''">
							<div class="go-">
								<a href="{$go_url}">перейти к фильтру</a>
							</div>
						</xsl:if>
						<xsl:if test="not($read_only)">
							<div class="subhead-">Группы свидетелей</div>
							<div class="block- raters-">
								<div class="subtitle-">
									<xsl:text>Учтены оценки </xsl:text>
									<xsl:value-of select="rater_user_groups/@used_rater_count"/>
									<xsl:call-template name="count_case">
										<xsl:with-param name="number" select="rater_user_groups/@used_rater_count"/>
										<xsl:with-param name="word_ns" select="' свидетеля'"/>
										<xsl:with-param name="word_gs" select="' свидетелей'"/>
										<xsl:with-param name="word_ap" select="' свидетелей'"/>
									</xsl:call-template>
									<xsl:text> из </xsl:text>
									<xsl:value-of select="rater_user_groups/@total_rater_count"/>
									<xsl:text>.</xsl:text>
								</div>
								<div class="cb-">
									<input type="checkbox" id="rater_user_groups_all" name="rater_user_groups_all">
										<xsl:if test="rater_user_groups/@select_all_checked = 1">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:if>
									</input>
									<label for="rater_user_groups_all">Все</label>
								</div>
								<xsl:for-each select="rater_user_groups/user_group">
									<div class="cb-">
										<input type="checkbox" id="rater_user_groups_{@id}" name="rater_user_groups[]" value="{@id}">
											<xsl:if test="@is_selected = 1">
												<xsl:attribute name="checked">checked</xsl:attribute>
											</xsl:if>
										</input>
										<label for="rater_user_groups_{@id}">
											<xsl:value-of select="@title"/>
											<xsl:text> (</xsl:text>
											<xsl:value-of select="@user_count"/>
											<xsl:if test="position() = 1">
												<xsl:call-template name="count_case">
													<xsl:with-param name="number" select="@user_count"/>
													<xsl:with-param name="word_ns" select="' человек'"/>
													<xsl:with-param name="word_gs" select="' человека'"/>
													<xsl:with-param name="word_ap" select="' человек'"/>
												</xsl:call-template>
											</xsl:if>
											<xsl:text>)</xsl:text>
										</label>
									</div>
								</xsl:for-each>
								<xsl:if test="rater_user_groups/@not_grouped_rater_count != 0">
									<div class="cb-">
										<xsl:text>+ </xsl:text>
										<xsl:value-of select="rater_user_groups/@not_grouped_rater_count"/>
										<xsl:call-template name="count_case">
											<xsl:with-param name="number" select="rater_user_groups/@not_grouped_rater_count"/>
											<xsl:with-param name="word_ns" select="' человек'"/>
											<xsl:with-param name="word_gs" select="' человека'"/>
											<xsl:with-param name="word_ap" select="' человек'"/>
										</xsl:call-template>
										<xsl:text> вне групп.</xsl:text>
									</div>
								</xsl:if>
							</div>
							<div class="submit-">
								<div class="input-">
									<button class="button button-small">Пересчитать</button>
								</div>
								<div class="drop-">
									<a>
										<xsl:attribute name="href">
											<xsl:value-of select="$request/@url_base"/>
											<xsl:for-each select="$get_vars[substring(@name, 1, 8) != 'project_' and substring(@name, 1, 11) != 'translator_' and substring(@name, 1, 19) != 'rater_user_groups']">
												<xsl:call-template name="get_param_for_url">
													<xsl:with-param name="position" select="position()"/>
												</xsl:call-template>
											</xsl:for-each>
										</xsl:attribute>
										<xsl:text>Сбросить фильтр</xsl:text>
									</a>
								</div>
							</div>
						</xsl:if>
					</form>
				</div>
			</div>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="draw_mark_title">
		<xsl:param name="mark"/>
		<xsl:param name="start_low_case" select="false()"/>
		<xsl:choose>
			<xsl:when test="$start_low_case">
				<xsl:choose>
					<xsl:when test="$mark = 0">не проявлена</xsl:when>
					<xsl:when test="$mark = 1">склонность</xsl:when>
					<xsl:when test="$mark = 2">способность</xsl:when>
					<xsl:when test="$mark = 3">компетенция</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$mark = 0">Не проявлена</xsl:when>
					<xsl:when test="$mark = 1">Склонность</xsl:when>
					<xsl:when test="$mark = 2">Способность</xsl:when>
					<xsl:when test="$mark = 3">Компетенция</xsl:when>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="url_replace_param">
		<xsl:param name="url_base" select="$request/@url_base"/>
		<xsl:param name="param_name"/>
		<xsl:param name="param_value" select="1"/>
		<xsl:param name="param2_name" select="''"/>
		<xsl:param name="param2_value" select="1"/>
		<xsl:value-of select="$url_base"/>
		<xsl:for-each select="$get_vars[@name != $param_name and @name != $param2_name]">
			<xsl:call-template name="get_param_for_url">
				<xsl:with-param name="position" select="position()"/>
			</xsl:call-template>
		</xsl:for-each>
		<xsl:choose>
			<xsl:when test="$get_vars[@name != $param_name and @name != $param2_name]">&amp;</xsl:when>
			<xsl:otherwise>?</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$param_name"/>
		<xsl:text>=</xsl:text>
		<xsl:value-of select="$param_value"/>
		<xsl:if test="$param2_name != ''">
			<xsl:text>&amp;</xsl:text>
			<xsl:value-of select="$param2_name"/>
			<xsl:text>=</xsl:text>
			<xsl:value-of select="$param2_value"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="url_delete_param">
		<xsl:param name="url_base" select="$request/@url_base"/>
		<xsl:param name="param_name"/>
		<xsl:value-of select="$url_base"/>
		<xsl:for-each select="$get_vars[@name != $param_name]">
			<xsl:call-template name="get_param_for_url">
				<xsl:with-param name="position" select="position()"/>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="get_param_for_form">
		<xsl:choose>
			<xsl:when test="item">
				<xsl:for-each select="item">
					<input type="hidden" name="{../@name}[]" value="{.}"/>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<input type="hidden" name="{@name}" value="{.}"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="get_param_for_url">
		<xsl:param name="position"/>
		<xsl:choose>
			<xsl:when test="item">
				<xsl:for-each select="item">
					<xsl:choose>
						<xsl:when test="$position = 1 and position() = 1">?</xsl:when>
						<xsl:otherwise>&amp;</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="../@name"/>
					<xsl:text>%5B%5D=</xsl:text>
					<xsl:value-of select="@value_escaped"/>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$position = 1">?</xsl:when>
					<xsl:otherwise>&amp;</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="@name"/>
				<xsl:text>=</xsl:text>
				<xsl:value-of select="@value_escaped"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="draw_footer">
		<div id="footer">
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="/root/project_widgets or /root/community_widgets">footer-wide-</xsl:when>
				</xsl:choose>
			</xsl:attribute>
			<p>
				<xsl:choose>
					<xsl:when test="$sys_params/@is_devyourself = 1">
						<xsl:text>&#169;</xsl:text>
						<span title="Образовательное бюро"> ОБ </span>
						<xsl:text>&#171;</xsl:text>
						<a href="http://soling.su/">Солинг</a>
						<xsl:text>&#187;, </xsl:text>
						<xsl:value-of select="$sys_params/@copyright_start_year"/>
						<xsl:if test="$sys_params/@copyright_start_year &lt; substring(/root/@time, 1, 4)">
							<xsl:text>&#8211;</xsl:text>
							<xsl:value-of select="substring(/root/@time, 1, 4)"/>
						</xsl:if>
						<xsl:text>.</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>&#169; </xsl:text>
						<xsl:value-of select="$sys_params/@copyright_start_year"/>
						<xsl:if test="$sys_params/@copyright_start_year &lt; substring(/root/@time, 1, 4)">
							<xsl:text>&#8211;</xsl:text>
							<xsl:value-of select="substring(/root/@time, 1, 4)"/>
						</xsl:if>
						<xsl:text>.</xsl:text>
						<xsl:text> Сделано на платформе </xsl:text>
						<a href="http://devyourself.ru/">DevYourself</a>
						<xsl:text>. </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</p>
			<p>
				<a class="email" href="mailto:{$sys_params/@info_email_obscured}">
					<xsl:value-of select="$sys_params/@info_email_obscured"/>
				</a>
			</p>
		</div>
	</xsl:template>
	<xsl:template name="draw_debug_info">
		<div id="debug-info">
			<span>
				<xsl:text>cache: </xsl:text>
				<xsl:for-each select="/root/stat/cache/state[@count != 0]">
					<i class="{@name}" title="{@controls}">
						<xsl:value-of select="@name"/>
						<xsl:text> (</xsl:text>
						<xsl:value-of select="@count"/>
						<xsl:text>)</xsl:text>
					</i>
					<xsl:if test="position() != last()">, </xsl:if>
				</xsl:for-each>
			</span>
			<span>db queries: <b>
					<xsl:value-of select="/root/stat/db/@query_count"/>
				</b>
			</span>
			<span>time: <b>
					<xsl:value-of select="substring(/root/stat/time/@load, 1, 5)"/>
				</b>
			</span>
		</div>
	</xsl:template>
</xsl:stylesheet>
