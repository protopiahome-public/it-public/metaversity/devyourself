<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="menu/ratings_submenu.inc.xslt"/>
	<xsl:include href="head2/ratings_head2.inc.xslt"/>
	<xsl:include href="../users/search/users_search.inc.xslt"/>
	<xsl:include href="../_core/auxil/paging.inc.xslt"/>
	<xsl:variable name="top_section" select="'ratings'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/ratings/prof-', /root/rating/@professiogram_id, '/')"/>
	<xsl:template match="rating">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_ratings_head2"/>
							<div class="content">
								<xsl:call-template name="draw_ratings_submenu"/>
								<h3 class="pre" title="Набор компетенций">
									<a href="{$prefix}/ratings/set-{@competence_set_id}/">
										<xsl:value-of select="/root/competence_set_short[@id = current()/@competence_set_id]/@title"/>
									</a>
								</h3>
								<h4 title="Профессия">
									<xsl:choose>
										<xsl:when test="/root/rating/pages/@current_page = 1 and not($get_vars)">
											<xsl:value-of select="@professiogram_title"/>
										</xsl:when>
										<xsl:otherwise>
											<a href="{$prefix}/ratings/prof-{@professiogram_id}/">
												<xsl:value-of select="@professiogram_title"/>
											</a>
										</xsl:otherwise>
									</xsl:choose>
								</h4>
								<xsl:call-template name="_draw_table"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
							<xsl:call-template name="draw_users_search"/>
							<xsl:call-template name="draw_filter">
								<xsl:with-param name="competence_set_id" select="@competence_set_id"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_table">
		<xsl:variable name="module" select="."/>
		<xsl:for-each select="/root/professiogram_show[@id = current()/@professiogram_id][1]">
			<xsl:if test="descr/div/*">
				<div class="box">
					<xsl:copy-of select="descr/div"/>
				</div>
			</xsl:if>
		</xsl:for-each>
		<xsl:if test="not(user)">
			<p>По заданным критериям поиска ни одного пользователя не найдено.</p>
		</xsl:if>
		<xsl:if test="user">
			<xsl:if test="filter/field[@name = 'search'] != ''">
				<p>
					<xsl:text>Результаты поиска по запросу &#171;</xsl:text>
					<strong>
						<xsl:value-of select="filter/field[@name = 'search']"/>
					</strong>
					<xsl:text>&#187;.</xsl:text>
				</p>
			</xsl:if>
			<p>
				<xsl:text>Отображаются пользователи </xsl:text>
				<xsl:value-of select="pages/@from_row"/>
				<xsl:text> &#8212; </xsl:text>
				<xsl:value-of select="pages/@to_row"/>
				<xsl:text> из </xsl:text>
				<xsl:value-of select="pages/@row_count"/>
				<xsl:text>.</xsl:text>
			</p>
			<table class="tbl">
				<tr>
					<th/>
					<th>
						<span>Пользователь</span>
					</th>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'match'"/>
						<xsl:with-param name="title" select="'Соответствие'"/>
						<xsl:with-param name="no_links" select="true()"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<th>
						<span>Детально</span>
					</th>
				</tr>
				<xsl:for-each select="user">
					<xsl:variable name="position" select="position()"/>
					<xsl:variable name="match" select="@match"/>
					<xsl:for-each select="/root/user_short[@id = current()/@id]">
						<tr>
							<xsl:if test="@login = $module/@user_to_highlight">
								<xsl:attribute name="class">hl-</xsl:attribute>
							</xsl:if>
							<td class="col-user-avatar-small-">
								<span>
									<a href="{$prefix}/users/{@login}/results/prof-{$module/@professiogram_id}/{$request/@query_string}">
										<img src="{photo_small/@url}" width="{photo_small/@width}" height="{photo_small/@height}" alt="{@login}"/>
									</a>
								</span>
							</td>
							<td class="col-user-name-small-">
								<div class="title-">
									<a name="user-{@login}" href="{$prefix}/users/{@login}/results/prof-{$module/@professiogram_id}/{$request/@query_string}">
										<xsl:value-of select="@visible_name"/>
									</a>
								</div>
								<div class="second-">
									<xsl:value-of select="@login"/>
								</div>
							</td>
							<td class="col-digits-">
								<xsl:value-of select="$match"/>
								<xsl:text>%</xsl:text>
							</td>
							<td class="col-icons- icons">
								<a class="plot-rose- gray-" title="Соответствие профессии (в виде &#171;розочки&#187;)" href="{$prefix}/users/{@login}/results/prof-{$module/@professiogram_id}/{$request/@query_string}"/>
								<span class="sep-"/>
								<a class="plot-bars- gray-" title="Соответствие профессии (подробно, в виде столбчатой диаграммы)" href="{$prefix}/users/{@login}/results/prof-{$module/@professiogram_id}/detailed/{$request/@query_string}"/>
								<span class="sep-"/>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
			<xsl:apply-templates select="pages[page]"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Рейтинг: </xsl:text>
		<xsl:value-of select="rating/@professiogram_title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/ratings/">Рейтинги</a>
		</div>
		<div class="level2-">
			<a href="{$prefix}/ratings/set-{rating/@competence_set_id}/">
				<xsl:value-of select="competence_set_short[@id = current()/rating/@competence_set_id]/@title"/>
			</a>
		</div>
		<div class="level3- selected-">
			<xsl:choose>
				<xsl:when test="rating/pages/@current_page = 1 and not($get_vars)">
					<xsl:value-of select="rating/@professiogram_title"/>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$prefix}/ratings/prof-{rating/@professiogram_id}/">
						<xsl:value-of select="rating/@professiogram_title"/>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
