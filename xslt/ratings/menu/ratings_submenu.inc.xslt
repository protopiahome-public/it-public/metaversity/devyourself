<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_ratings_submenu">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td style="vertical-align: top; text-align: left;">
					<xsl:choose>
						<xsl:when test="not(/root/ratings or /root/ratings_professiograms/@competence_set_id = 0)">
							<p class="note small">
								<span style="font-size: 14px;">← </span>
								<a href="{$prefix}/ratings/">к наборам компетенций</a>
							</p>
						</xsl:when>
						<xsl:otherwise>
							<div class="submenu submenu-left-">
								<span class="inner-">
									<span class="item-">
										<xsl:if test="/root/ratings">
											<xsl:attribute name="class">selected-</xsl:attribute>
										</xsl:if>
										<xsl:choose>
											<xsl:when test="$top_section = 'ratings' and $top_section_main_page">Кратко</xsl:when>
											<xsl:otherwise>
												<a href="{$prefix}/ratings/">Кратко</a>
											</xsl:otherwise>
										</xsl:choose>
									</span>
									<span class="sep-"> | </span>
									<span class="item-">
										<xsl:if test="/root/ratings_professiograms">
											<xsl:attribute name="class">selected-</xsl:attribute>
										</xsl:if>
										<xsl:choose>
											<xsl:when test="/root/ratings_professiograms/pages/@current_page = 1 and not($get_vars)">Развёрнуто</xsl:when>
											<xsl:otherwise>
												<a href="{$prefix}/ratings/profs/">Развёрнуто</a>
											</xsl:otherwise>
										</xsl:choose>
									</span>
								</span>
							</div>
						</xsl:otherwise>
					</xsl:choose>
				</td>
				<td style="vertical-align: top; text-align: right;">
					<xsl:if test="/root/competence_set_fast_navigation">
						<select id="jq-competence-set-dd-nav" style="margin: 0; padding: 0;">
							<option value="0">— Быстрая навигация по наборам компетенций —</option>
							<xsl:for-each select="/root/competence_set_fast_navigation/competence_set">
								<option value="{@id}">
									<xsl:if test="/root/ratings_professiograms/@competence_set_id = @id or /root/rating/@competence_set_id = @id">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="@title"/>
								</option>
							</xsl:for-each>
						</select>
						<script type="text/javascript">
							$('#jq-competence-set-dd-nav').change(function() {
								var urlbase = '<xsl:value-of select="$prefix"/>/ratings/';
								var competence_set_id = Number(this.options[this.selectedIndex].value);
								location.href = competence_set_id ? urlbase + 'set-' + competence_set_id + '/' : urlbase;
							});
						</script>
					</xsl:if>
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>
