<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="menu/ratings_submenu.inc.xslt"/>
	<xsl:include href="head2/ratings_head2.inc.xslt"/>
	<xsl:include href="search/ratings_search.inc.xslt"/>
	<xsl:include href="../_core/auxil/paging.inc.xslt"/>
	<xsl:variable name="top_section" select="'ratings'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="module_url">
		<xsl:value-of select="concat($prefix, '/ratings/')"/>
		<xsl:choose>
			<xsl:when test="/root/ratings_professiograms/@competence_set_id = 0">profs/</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat('set-', /root/ratings_professiograms/@competence_set_id, '/')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="ratings_professiograms">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_ratings_head2"/>
							<div class="content">
								<xsl:call-template name="draw_ratings_submenu"/>
								<xsl:call-template name="_draw_box"/>
								<xsl:call-template name="_draw_tables"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
							<xsl:call-template name="draw_ratings_search"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_box">
		<xsl:variable name="competence_set_id" select="@competence_set_id"/>
		<xsl:if test="$competence_set_id = 0">
			<div class="box">
				<div class="content-">
					<strong>Профессии видны сразу.</strong> Наборы компетенций отсортированы по количеству доступных оценок. Учтите, что часть профессий последнего на странице набора компетенций может быть перенесна на следующую страницу. Не забывайте про поиск (справа).
				</div>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="_draw_tables">
		<xsl:variable name="module" select="."/>
		<xsl:variable name="competence_set_id" select="@competence_set_id"/>
		<xsl:if test="$competence_set_id = 0">
			<xsl:call-template name="_professiograms_draw_info"/>
		</xsl:if>
		<xsl:for-each select="competence_sets/competence_set">
			<xsl:variable name="competence_set_position" select="position()"/>
			<xsl:for-each select="/root/competence_set_full[@id = current()/@id]">
				<h3 title="Набор компетенций">
					<xsl:if test="$competence_set_position != 1">
						<xsl:attribute name="class">down</xsl:attribute>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="$competence_set_id = 0">
							<a href="{$prefix}/ratings/set-{@id}/">
								<xsl:value-of select="@title"/>
							</a>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="@title"/>
						</xsl:otherwise>
					</xsl:choose>
				</h3>
				<p class="note">
					<span class="with-hint">
						<xsl:call-template name="draw_mark_count_hint">
							<xsl:with-param name="position" select="1"/>
							<xsl:with-param name="with_trans" select="true()"/>
						</xsl:call-template>
					</span>
					<xsl:text> (</xsl:text>
					<span class="with-hint">
						<xsl:call-template name="draw_mark_count_hint">
							<xsl:with-param name="position" select="0"/>
						</xsl:call-template>
					</span>
					<xsl:text> без учёта переводчиков), </xsl:text>
					<xsl:value-of select="@professiogram_count_calc"/>
					<xsl:call-template name="count_case">
						<xsl:with-param name="number" select="@professiogram_count_calc"/>
						<xsl:with-param name="word_ns" select="' профессия'"/>
						<xsl:with-param name="word_gs" select="' профессии'"/>
						<xsl:with-param name="word_ap" select="' профессий'"/>
					</xsl:call-template>
					<xsl:text>.</xsl:text>
				</p>
				<xsl:if test="$competence_set_id != 0">
					<xsl:call-template name="_professiograms_draw_info">
						<xsl:with-param name="module" select="$module"/>
					</xsl:call-template>
				</xsl:if>
				<xsl:if test="$module/professiogram[@competence_set_id = current()/@id]">
					<table class="tbl tbl-nohead-">
						<xsl:for-each select="$module/professiogram[@competence_set_id = current()/@id]">
							<tr>
								<td class="col-bullet- col-longer-">
									<span class="bullet-">
										<a href="{$prefix}/ratings/prof-{@id}/">
											<xsl:value-of select="@title"/>
										</a>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</table>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:apply-templates select="pages[page]"/>
	</xsl:template>
	<xsl:template name="_professiograms_draw_info">
		<xsl:param name="module" select="."/>
		<xsl:if test="$module/filter/field[@name = 'search'] != ''">
			<p>
				<xsl:text>Результаты поиска по запросу &#171;</xsl:text>
				<strong>
					<xsl:value-of select="$module/filter/field[@name = 'search']"/>
				</strong>
				<xsl:text>&#187;.</xsl:text>
			</p>
		</xsl:if>
		<xsl:if test="not($module/professiogram)">
			<xsl:choose>
				<xsl:when test="$module/filter/field[@name = 'search'] != ''">
					<p>По заданным критериям поиска ни одной профессии не найдено.</p>
				</xsl:when>
				<xsl:otherwise>
					<p>В данном наборе компетенций нет ни одной профессии</p>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$module/professiogram">
			<p>
				<xsl:text>Отображаются профессии </xsl:text>
				<xsl:value-of select="$module/pages/@from_row"/>
				<xsl:text> &#8212; </xsl:text>
				<xsl:value-of select="$module/pages/@to_row"/>
				<xsl:text> из </xsl:text>
				<xsl:value-of select="$module/pages/@row_count"/>
				<xsl:text>.</xsl:text>
			</p>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:choose>
			<xsl:when test="/root/ratings_professiograms/@competence_set_id = 0">Рейтинги (развёрнуто)</xsl:when>
			<xsl:otherwise>
				<xsl:text>Рейтинги: </xsl:text>
				<xsl:value-of select="/root/competence_set_full[@id = /root/ratings_professiograms/@competence_set_id]/@title"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/ratings/">Рейтинги</a>
		</div>
		<div class="level2- selected-">
			<xsl:choose>
				<xsl:when test="/root/ratings_professiograms/@competence_set_id = 0">
					<xsl:choose>
						<xsl:when test="/root/ratings_professiograms/pages/@current_page = 1 and not($get_vars)">Развёрнуто</xsl:when>
						<xsl:otherwise>
							<a href="{$prefix}/ratings/profs/">Развёрнуто</a>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="not(/root/ratings_professiograms/pages/@current_page != 1) and not($get_vars)">
							<xsl:value-of select="/root/competence_set_full[@id = /root/ratings_professiograms/@competence_set_id]/@title"/>
						</xsl:when>
						<xsl:otherwise>
							<a href="{$prefix}/ratings/set-{/root/ratings_professiograms/@competence_set_id}/">
								<xsl:value-of select="/root/competence_set_full[@id = /root/ratings_professiograms/@competence_set_id]/@title"/>
							</a>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
