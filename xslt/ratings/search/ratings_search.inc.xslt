<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_ratings_search">
		<div class="box">
			<div class="content-">
				<xsl:variable name="module_filter" select="filter"/>
				<xsl:if test="not(name() = 'ratings_professiograms' and @competence_set_id != 0)">
					<form id="search-form" class="search-form search-form-w100p" action="{$prefix}/ratings/" method="get">
						<xsl:for-each select="$get_vars">
							<xsl:if test="not($module_filter/field[@name = current()/@name])">
								<xsl:call-template name="get_param_for_form"/>
							</xsl:if>
						</xsl:for-each>
						<div class="line-">
							<div class="el- el-text-">
								<input type="text" id="search_search" name="search" jq-placeholder="набор компетенций" value="" maxlength="255">
									<xsl:if test="name() = 'ratings' and filter/@user_filter = 1 and filter/field[@name = 'search'] != ''">
										<xsl:attribute name="value">
											<xsl:value-of select="filter/field[@name = 'search']"/>
										</xsl:attribute>
									</xsl:if>
								</input>
								<div class="example-">Примеры:
									<span>ФГОС</span>,
									<span>2030</span>
								</div>
							</div>
						</div>
						<div class="line-">
							<div class="el- el-btn-">
								<button class="button button-small">Искать</button>
							</div>
						</div>
					</form>
					<hr/>
				</xsl:if>
				<form id="search-form-2" class="search-form search-form-w100p" action="{$prefix}/ratings/profs/" method="get">
					<xsl:if test="@competence_set_id != 0">
						<xsl:attribute name="action">
							<xsl:value-of select="concat($prefix, '/ratings/set-', @competence_set_id, '/')"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:for-each select="$get_vars">
						<xsl:if test="not($module_filter/field[@name = current()/@name])">
							<xsl:call-template name="get_param_for_form"/>
						</xsl:if>
					</xsl:for-each>
					<xsl:if test="name() = 'ratings_professiograms' and @competence_set_id != 0">
						<p>
							<strong>Поиск по текущему набору компетенций</strong>
						</p>
					</xsl:if>
					<div class="line-">
						<div class="el- el-text-">
							<input type="text" id="search2_search" name="search" jq-placeholder="профессия" value="" maxlength="255">
								<xsl:if test="name() = 'ratings_professiograms' and filter/@user_filter = 1 and filter/field[@name = 'search'] != ''">
									<xsl:attribute name="value">
										<xsl:value-of select="filter/field[@name = 'search']"/>
									</xsl:attribute>
								</xsl:if>
							</input>
							<div class="example-">Примеры:
								<span>журналист</span>,
								<span>аналитик</span>
							</div>
						</div>
					</div>
					<div class="line-">
						<div class="el- el-btn-">
							<button class="button button-small">Искать</button>
						</div>
					</div>
				</form>
				<div class="clear"/>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
