<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="../_site/include/access.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'precedents'"/>
	<xsl:variable name="project_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/admins/')"/>
	<xsl:template match="error_403">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Прецеденты</h2>
							<div class="content">
								<div class="box box-stop">
									<h3>Доступ к прецедентам ограничен</h3>
									<xsl:call-template name="draw_access">
										<xsl:with-param name="status" select="$project_access/@access_precedents_read"/>
										<xsl:with-param name="entity_genitiv" select="'проекта'"/>
										<xsl:with-param name="join_premoderation" select="$project_access/@join_premoderation = 1"/>
										<xsl:with-param name="special_moderator_entity_genitiv" select="'прецедентов'"/>
									</xsl:call-template>
								</div>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Нет доступа &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
