<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="precedent_full" select="/root/precedent_full"/>
	<xsl:variable name="precedent_data" select="/root/precedent_data"/>
	<xsl:variable name="precedents_url" select="concat($current_project/@url, 'precedents/')"/>
	<xsl:variable name="precedent_url" select="concat($current_project/@url, 'precedents/precedent-', $precedent_full/@id, '/')"/>
	<xsl:template name="draw_precedent_edit_submenu">
		<xsl:for-each select="/root[1]">
			<table class="precedent-edit-menu">
				<tr>
					<td>
						<xsl:attribute name="class">
							<xsl:choose>
								<xsl:when test="precedent_edit or precedent_add">sep- sep-z1-</xsl:when>
								<xsl:otherwise>sep- sep-z0-</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</td>
					<td class="item-">
						<xsl:variable name="item_title" select="'Основное'"/>
						<xsl:choose>
							<xsl:when test="precedent_edit or precedent_add">
								<xsl:attribute name="class">item- item-selected-</xsl:attribute>
								<strong>
									<xsl:value-of select="$item_title"/>
								</strong>
							</xsl:when>
							<xsl:otherwise>
								<a href="{$precedent_url}edit/">
									<xsl:value-of select="$item_title"/>
								</a>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:attribute name="class">
							<xsl:choose>
								<xsl:when test="precedent_edit or precedent_add">sep- sep-10-</xsl:when>
								<xsl:when test="precedent_edit_ratees">sep- sep-01-</xsl:when>
								<xsl:otherwise>sep- sep-00-</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</td>
					<td class="item-">
						<xsl:variable name="item_title" select="'Участники'"/>
						<xsl:choose>
							<xsl:when test="precedent_add">
								<span>
									<xsl:value-of select="$item_title"/>
								</span>
							</xsl:when>
							<xsl:when test="precedent_edit_ratees">
								<xsl:attribute name="class">item- item-selected-</xsl:attribute>
								<strong>
									<xsl:value-of select="$item_title"/>
								</strong>
							</xsl:when>
							<xsl:otherwise>
								<a href="{$precedent_url}edit/ratees/">
									<xsl:value-of select="$item_title"/>
								</a>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:attribute name="class">
							<xsl:choose>
								<xsl:when test="precedent_edit_ratees">sep- sep-10-</xsl:when>
								<xsl:when test="precedent_edit_flash_groups">sep- sep-01-</xsl:when>
								<xsl:otherwise>sep- sep-00-</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</td>
					<td class="item-">
						<xsl:variable name="item_title" select="'Флэш-группы'"/>
						<xsl:choose>
							<xsl:when test="precedent_add">
								<span>
									<xsl:value-of select="$item_title"/>
								</span>
							</xsl:when>
							<xsl:when test="precedent_edit_flash_groups">
								<xsl:attribute name="class">item- item-selected-</xsl:attribute>
								<strong>
									<xsl:value-of select="$item_title"/>
								</strong>
							</xsl:when>
							<xsl:otherwise>
								<a href="{$precedent_url}edit/flash-groups/">
									<xsl:value-of select="$item_title"/>
								</a>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:attribute name="class">
							<xsl:choose>
								<xsl:when test="precedent_edit_flash_groups">sep- sep-10-</xsl:when>
								<xsl:when test="precedent_edit_raters">sep- sep-01-</xsl:when>
								<xsl:otherwise>sep- sep-00-</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</td>
					<td class="item-">
						<xsl:variable name="item_title" select="'Оценщики'"/>
						<xsl:choose>
							<xsl:when test="precedent_add">
								<span>
									<xsl:value-of select="$item_title"/>
								</span>
							</xsl:when>
							<xsl:when test="precedent_edit_raters">
								<xsl:attribute name="class">item- item-selected-</xsl:attribute>
								<strong>
									<xsl:value-of select="$item_title"/>
								</strong>
							</xsl:when>
							<xsl:otherwise>
								<a href="{$precedent_url}edit/raters/">
									<xsl:value-of select="$item_title"/>
								</a>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:attribute name="class">
							<xsl:choose>
								<xsl:when test="precedent_edit_raters">sep- sep-10-</xsl:when>
								<xsl:when test="precedent_edit_competences">sep- sep-01-</xsl:when>
								<xsl:otherwise>sep- sep-00-</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</td>
					<td class="item-">
						<xsl:variable name="item_title" select="'Компетенции'"/>
						<xsl:choose>
							<xsl:when test="precedent_add">
								<span>
									<xsl:value-of select="$item_title"/>
								</span>
							</xsl:when>
							<xsl:when test="precedent_edit_competences">
								<xsl:attribute name="class">item- item-selected-</xsl:attribute>
								<strong>
									<xsl:value-of select="$item_title"/>
								</strong>
							</xsl:when>
							<xsl:otherwise>
								<a href="{$precedent_url}edit/competences/">
									<xsl:value-of select="$item_title"/>
								</a>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:attribute name="class">
							<xsl:choose>
								<xsl:when test="precedent_edit_competences">sep- sep-10-</xsl:when>
								<xsl:when test="precedent_edit_marks">sep- sep-01-</xsl:when>
								<xsl:otherwise>sep- sep-00-</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</td>
					<td class="item-">
						<xsl:variable name="item_title" select="'Оценки'"/>
						<xsl:choose>
							<xsl:when test="precedent_add">
								<span>
									<xsl:value-of select="$item_title"/>
								</span>
							</xsl:when>
							<xsl:when test="precedent_edit_marks">
								<xsl:attribute name="class">item- item-selected-</xsl:attribute>
								<strong>
									<xsl:value-of select="$item_title"/>
								</strong>
							</xsl:when>
							<xsl:otherwise>
								<a href="{$precedent_url}edit/marks/">
									<xsl:value-of select="$item_title"/>
								</a>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:attribute name="class">
							<xsl:choose>
								<xsl:when test="precedent_edit_marks">sep- sep-1z-</xsl:when>
								<xsl:otherwise>sep- sep-0z-</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</td>
					<xsl:if test="not(precedent_add)">
						<td class="item-right-">
							<div class="ico-links">
								<xsl:choose>
									<xsl:when test="/root/precedent_delete">
										<strong class="delete- current-">
											<span>удалить</span>
										</strong>
									</xsl:when>
									<xsl:otherwise>
										<a class="delete- gray-" href="{$precedent_url}delete/">
											<span>удалить</span>
										</a>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</xsl:if>
				</tr>
			</table>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
