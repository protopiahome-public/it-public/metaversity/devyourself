<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/precedent_edit_submenu.inc.xslt"/>
	<xsl:include href="include/precedent_edit_users.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'precedents'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($precedent_url, 'edit/ratees/')"/>
	<xsl:template match="precedent_edit_ratees">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Прецеденты</h2>
							<div class="content">
								<h3>
									<a href="{$precedent_url}">
										<xsl:value-of select="$precedent_full/@title"/>
									</a>
								</h3>
								<xsl:call-template name="draw_precedent_edit_submenu"/>
								<h4 class="precedent-edit-header">Участники</h4>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<xsl:call-template name="draw_precedent_edit_users_added">
			<xsl:with-param name="from" select="$precedent_data/ratees/user"/>
			<xsl:with-param name="on_empty_text" select="'Список участников прецедента пока пуст.'"/>
		</xsl:call-template>
		<h5 class="precedent-edit-header">Добавить участника:</h5>
		<xsl:call-template name="draw_precedent_edit_users_in_project">
			<xsl:with-param name="for" select="/root/precedent_edit_ratees/fake_mark_count"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="_check_member_in_use">
		<xsl:param name="id"/>
		<xsl:if test="$precedent_data/ratees/user[@id = $id]/@personal_mark_count_calc > 0">
			<xsl:attribute name="class">jq-member-in-use</xsl:attribute>
		</xsl:if>
	</xsl:template>
	<xsl:template name="_draw_user_buttons">
		<td class="col-add- jq-add-button">
			<span title="Добавить участника" user-id="{@id}">
				<xsl:if test="$precedent_data/ratees/user[@id = current()/@id]">
					<xsl:attribute name="class">added-</xsl:attribute>
					<xsl:attribute name="title">Участник добавлен</xsl:attribute>
				</xsl:if>
			</span>
		</td>
		<td class="col-icons- icons jq-remove-button">
			<a class="delete- gray-" title="Удалить участника"/>
		</td>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/table_filter.js"/>
		<script type="text/javascript" src="{$prefix}/js/precedent_edit_users.js"/>
		<script type="text/javascript">
			var post_vars = {
				project_id : <xsl:value-of select="precedent_edit_ratees/@project_id"/>,
				precedent_id : <xsl:value-of select="precedent_edit_ratees/@precedent_id"/>,
				target : 'ratee'
			};
			var precedent_edit_users_params = {
				show_user_numbers : <xsl:value-of select="$current_project/@show_user_numbers"/>,
				use_game_name : <xsl:value-of select="$current_project/@use_game_name"/>
			};
		</script>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Участники &#8212; </xsl:text>
		<xsl:value-of select="$precedent_full/@title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
