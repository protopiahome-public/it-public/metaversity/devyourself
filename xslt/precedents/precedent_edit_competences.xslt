<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/precedent_edit_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'precedents'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($precedent_url, 'edit/competences/')"/>
	<xsl:variable name="default_competence_set_id" select="/root/precedent_edit_competences/@default_competence_set_id"/>
	<xsl:variable name="selected_competence_set_id" select="/root/precedent_edit_competences/@selected_competence_set_id"/>
	<xsl:template match="precedent_edit_competences">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Прецеденты</h2>
							<div class="content">
								<h3>
									<a href="{$precedent_url}">
										<xsl:value-of select="$precedent_full/@title"/>
									</a>
								</h3>
								<xsl:call-template name="draw_precedent_edit_submenu"/>
								<h4 class="precedent-edit-header">Компетенции</h4>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<fieldset>
			<div class="field">
				<select id="jq-competence-set-select">
					<optgroup label="Набор компетенций по умолчанию">
						<xsl:call-template name="_draw_competence_set_option">
							<xsl:with-param name="id" select="$default_competence_set_id"/>
						</xsl:call-template>
					</optgroup>
					<optgroup label="Используемые наборы компетенций">
						<xsl:for-each select="$precedent_data/competence_sets/competence_set">
							<xsl:if test="@id != $default_competence_set_id">
								<xsl:call-template name="_draw_competence_set_option"/>
							</xsl:if>
						</xsl:for-each>
					</optgroup>
					<optgroup label="Добавить набор компетенций">
						<xsl:for-each select="/root/competence_set_fast_navigation/competence_set">
							<xsl:if test="not ($precedent_data/competence_sets/competence_set[@id = current()/@id])">
								<xsl:call-template name="_draw_competence_set_option"/>
							</xsl:if>
						</xsl:for-each>
					</optgroup>
				</select>
				<label class="bigger" style="padding: 0 10px;">&#8594;</label>
				<a href="{$prefix}/sets/set-{/root/competence_set_competences/@competence_set_id}/">Показать&#160;описание</a>
			</div>
			<div class="field">
				<xsl:call-template name="_draw_competence_groups"/>
			</div>
		</fieldset>
	</xsl:template>
	<xsl:template name="_draw_competence_groups">
		<xsl:param name="from" select="/root/competence_set_competences"/>
		<xsl:param name="level" select="0"/>
		<div>
			<xsl:choose>
				<xsl:when test="$level = 0">
					<xsl:attribute name="id">jq-competence-set-competences</xsl:attribute>
					<xsl:attribute name="class">input-set-</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="class">input-set-no-border-</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:for-each select="$from/group">
				<xsl:variable name="competences" select="competence"/>
				<div class="field">
					<xsl:choose>
						<xsl:when test="$competences">
							<input id="checkbox-{@id}" type="checkbox" class="jq-competence-group input-checkbox" data-competence-group-id="{@id}">
								<xsl:if test="/root/precedent_edit_competences/competence_group[@id = current()/@id]">
									<xsl:attribute name="checked">1</xsl:attribute>
									<xsl:if test="/root/precedent_edit_competences/competence_group[@id = current()/@id]/@has_marks = 1">
										<xsl:attribute name="disabled">1</xsl:attribute>
									</xsl:if>
								</xsl:if>
							</input>
							<label for="checkbox-{@id}" class="inline-">
								<xsl:value-of select="@title"/>
							</label>
						</xsl:when>
						<xsl:otherwise>
							<input type="checkbox" style="visibility: hidden"/>
							<label class="inline-" style="font-weight: bold;">
								<xsl:value-of select="@title"/>
							</label>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:call-template name="_draw_competence_groups">
						<xsl:with-param name="from" select="current()"/>
						<xsl:with-param name="level" select="$level+1"/>
					</xsl:call-template>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template name="_draw_competence_set_option">
		<xsl:param name="id" select="current()/@id"/>
		<option value="{$id}">
			<xsl:if test="$id = $selected_competence_set_id">
				<xsl:attribute name="selected"/>
			</xsl:if>
			<xsl:value-of select="/root/competence_set_fast_navigation/competence_set[@id = $id]/@title"/>
		</option>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/precedent_edit_competences.js"/>
		<script type="text/javascript">
			var post_vars = {
				project_id : <xsl:value-of select="precedent_edit_competences/@project_id"/>,
				precedent_id : <xsl:value-of select="precedent_edit_competences/@precedent_id"/>
			};
		</script>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Компетенции &#8212; </xsl:text>
		<xsl:value-of select="$precedent_full/@title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
