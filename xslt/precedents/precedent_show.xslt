<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="include/precedent.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'precedents'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'precedents/precedent-', /root/precedent_show/@id, '/')"/>
	<xsl:variable name="precedent_full" select="/root/precedent_full"/>
	<xsl:variable name="precedent_data" select="/root/precedent_data"/>
	<xsl:template match="precedent_show">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Прецеденты</h2>
							<div class="content">
								<h3>
									<xsl:value-of select="$precedent_full/@title"/>
									<xsl:if test="$project_access/@can_moderate_precedents=1">
										<span class="icons- icons-small">
											<a class="edit- gray-" href="{$module_url}edit/" title="Редактировать"/>
										</span>
									</xsl:if>
								</h3>
								<xsl:copy-of select="$precedent_full/descr"/>
								<xsl:choose>
									<xsl:when test="$precedent_data//marks/mark">
										<xsl:call-template name="_draw_marks"/>
									</xsl:when>
									<xsl:otherwise>
										<p class="note">Оценки пока не выставлены.</p>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
						<td class="right-column">
							<xsl:if test="$precedent_data/raters/user">
								<xsl:call-template name="_draw_raters"/>
							</xsl:if>
							<xsl:if test="$precedent_data/ratees/user">
								<xsl:call-template name="_draw_ratees"/>
							</xsl:if>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_raters">
		<div class="widget-wrap">
			<div class="widget widget-users">
				<div class="header-">
					<table cellspacing="0">
						<tr>
							<td>
								<h2 class="text-">Оценщики</h2>
							</td>
						</tr>
					</table>
				</div>
				<xsl:for-each select="$precedent_data/raters/user">
					<table>
						<xsl:attribute name="class">
							<xsl:text>item-</xsl:text>
							<xsl:if test="position() = 1"> item-first-</xsl:if>
						</xsl:attribute>
						<tr>
							<td class="user-">
								<table class="user-block">
									<tr>
										<td class="avatar-">
											<xsl:call-template name="draw_precedent_user_avatar">
												<xsl:with-param name="user_id" select="@id"/>
											</xsl:call-template>
										</td>
										<td class="text-">
											<xsl:call-template name="draw_precedent_user_link">
												<xsl:with-param name="user_id" select="@id"/>
											</xsl:call-template>
										</td>
									</tr>
								</table>
							</td>
							<td class="digit-">
								<i>
									<xsl:call-template name="_draw_marks_total">
										<xsl:with-param name="case" select="'Поставил'"/>
									</xsl:call-template>
								</i>
							</td>
						</tr>
					</table>
				</xsl:for-each>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_ratees">
		<div class="widget-wrap">
			<div class="widget widget-users">
				<div class="header-">
					<table cellspacing="0">
						<tr>
							<td>
								<h2 class="text-">Участники</h2>
							</td>
						</tr>
					</table>
				</div>
				<xsl:for-each select="$precedent_data/ratees/user">
					<table>
						<xsl:attribute name="class">
							<xsl:text>item-</xsl:text>
							<xsl:if test="position() = 1"> item-first-</xsl:if>
						</xsl:attribute>
						<tr>
							<td class="user-">
								<table class="user-block">
									<tr>
										<td class="avatar-">
											<xsl:call-template name="draw_precedent_user_avatar">
												<xsl:with-param name="user_id" select="@id"/>
											</xsl:call-template>
										</td>
										<td class="text-">
											<xsl:call-template name="draw_precedent_user_link">
												<xsl:with-param name="user_id" select="@id"/>
											</xsl:call-template>
										</td>
									</tr>
								</table>
							</td>
							<td class="digit-">
								<i>
									<xsl:call-template name="_draw_marks_total"/>
								</i>
							</td>
						</tr>
					</table>
				</xsl:for-each>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_marks">
		<h3 class="down black">Оценки</h3>
		<!-- @ar Только те, у которых есть оценки! -->
		<xsl:for-each select="$precedent_data/competence_sets/competence_set">
			<xsl:variable name="competence_set_id" select="@id"/>
			<p class="small note">
				<xsl:text>Используемый набор компетенций: </xsl:text>
				<a href="{$prefix}/sets/set-{@id}">
					<xsl:value-of select="/root/competence_set_short[@id = current()/@id]/@title"/>
				</a>
				<xsl:text>.</xsl:text>
			</p>
			<div class="precedent-show-marks">
				<xsl:for-each select="$precedent_data/flash_group">
					<xsl:variable name="group_marks" select="marks/mark[@competence_set_id = $competence_set_id]"/>
					<!-- групповые оценки, таблица -->
					<div class="precedent-show-marks-user-">
						<table class="user-block">
							<tr>
								<!-- аватарки участников -->
								<xsl:for-each select="user">
									<td class="avatar-">
										<xsl:call-template name="draw_precedent_user_avatar">
											<xsl:with-param name="user_id" select="@id"/>
											<xsl:with-param name="role" select="'Участник'"/>
										</xsl:call-template>
									</td>
								</xsl:for-each>
								<!-- название группы -->
								<td class="text-">
									<span>Группа <xsl:if test="@title != ''">&#171;<xsl:value-of select="@title"/>&#187;</xsl:if>
									</span>
									<b>Групповые оценки, идут всем участникам</b>
								</td>
							</tr>
						</table>
						<table class="tbl tbl-marks- tbl-nohead-">
							<!-- оценки группы -->
							<xsl:for-each select="$group_marks">
								<tr>
									<td class="col-user-avatar-small-" style="padding-left: 0;">
										<xsl:call-template name="draw_precedent_user_avatar">
											<xsl:with-param name="user_id" select="@rater_user_id"/>
											<xsl:with-param name="role" select="'Оценщик'"/>
										</xsl:call-template>
									</td>
									<td class="col-text-" style="white-space: pre-wrap;">
										<xsl:call-template name="_draw_mark_comment"/>
									</td>
									<td class="col-marks-">
										<xsl:call-template name="_draw_mark">
											<xsl:with-param name="type" select="'group'"/>
										</xsl:call-template>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</div>
					<!-- личные оценки, таблица -->
					<div class="precedent-show-marks-user-">
						<xsl:for-each select="user">
							<xsl:variable name="personal_marks" select="marks/mark[@competence_set_id = $competence_set_id]"/>
							<xsl:if test="$personal_marks">
								<!-- участник -->
								<table class="user-block">
									<tr>
										<td class="avatar-">
											<xsl:call-template name="draw_precedent_user_avatar">
												<xsl:with-param name="user_id" select="@id"/>
												<xsl:with-param name="role" select="'Участник'"/>
											</xsl:call-template>
										</td>
										<td class="text-">
											<xsl:call-template name="draw_precedent_user_link">
												<xsl:with-param name="user_id" select="@id"/>
											</xsl:call-template>
										</td>
									</tr>
								</table>
								<!-- оценки участника -->
								<table class="tbl tbl-marks- tbl-nohead-">
									<xsl:for-each select="$personal_marks">
										<tr>
											<td class="col-user-avatar-small-" style="padding-left: 0;">
												<xsl:call-template name="draw_precedent_user_avatar">
													<xsl:with-param name="user_id" select="@rater_user_id"/>
													<xsl:with-param name="role" select="'Оценщик'"/>
												</xsl:call-template>
											</td>
											<td class="col-text-" style="white-space: pre-wrap;">
												<xsl:call-template name="_draw_mark_comment"/>
											</td>
											<td class="col-marks-">
												<xsl:call-template name="_draw_mark"/>
											</td>
										</tr>
									</xsl:for-each>
								</table>
							</xsl:if>
						</xsl:for-each>
					</div>
				</xsl:for-each>
			</div>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="_draw_marks_total">
		<xsl:param name="case" select="'Получил'"/>
		<span class="with-hint">
			<xsl:attribute name="title">
				<xsl:value-of select="$case"/>
				<xsl:text> оценок: </xsl:text>
				<xsl:value-of select="@total_mark_count_calc"/>
				<xsl:text> (</xsl:text>
				<xsl:value-of select="@personal_mark_count_calc"/>
				<xsl:call-template name="count_case">
					<xsl:with-param name="number" select="@personal_mark_count_calc"/>
					<xsl:with-param name="word_ns" select="' личная + '"/>
					<xsl:with-param name="word_gs" select="' личные + '"/>
					<xsl:with-param name="word_ap" select="' личных + '"/>
				</xsl:call-template>
				<xsl:value-of select="@group_mark_count_calc"/>
				<xsl:if test="@group_mark_count_calc &gt; 0 and @group_mark_raw_count_calc != ''">
					<xsl:text> (</xsl:text>
					<xsl:value-of select="@group_mark_raw_count_calc"/>
					<xsl:text>)</xsl:text>
				</xsl:if>
				<xsl:call-template name="count_case">
					<xsl:with-param name="number" select="@group_mark_count_calc"/>
					<xsl:with-param name="word_ns" select="' групповая'"/>
					<xsl:with-param name="word_gs" select="' групповых'"/>
					<xsl:with-param name="word_ap" select="' групповых'"/>
				</xsl:call-template>
				<xsl:text>)</xsl:text>
			</xsl:attribute>
			<xsl:value-of select="@total_mark_count_calc"/>
		</span>
	</xsl:template>
	<xsl:template name="_draw_mark_comment">
		<xsl:variable name="comment" select="./comment/text()"/>
		<xsl:choose>
			<xsl:when test="$comment != ''">
				<xsl:copy-of select="$comment"/>
			</xsl:when>
			<xsl:otherwise>
				<span class="note">без комментария</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="_draw_mark">
		<xsl:param name="type" select="'personal'"/>
		<xsl:variable name="type_hint">
			<xsl:choose>
				<xsl:when test="$type = 'personal'">личная</xsl:when>
				<xsl:otherwise>групповая</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<div class="width-">
			<div class="mark-competence-">
				<xsl:value-of select="@competence_id"/>. <xsl:value-of select="/root/competence_short[@id = current()/@competence_id]/@title"/>.</div>
			<table class="marks" cellpadding="0" cellspacing="0">
				<tbody>
					<tr class="{$type}-">
						<td>
							<xsl:if test="@value = 1">
								<span title="Склонность ({$type_hint} оценка)" class="c10">Ск.</span>
							</xsl:if>
						</td>
						<td>
							<xsl:if test="@value = 2">
								<span title="Способность ({$type_hint} оценка)" class="c10">Сп.</span>
							</xsl:if>
						</td>
						<td>
							<xsl:if test="@value = 3">
								<span title="Способность ({$type_hint} оценка)" class="c10">К.</span>
							</xsl:if>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$precedent_full/@title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
