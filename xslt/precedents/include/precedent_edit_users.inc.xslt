<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_precedent_edit_users_added">
		<xsl:param name="from"/>
		<xsl:param name="on_empty_text"/>
		<style type="text/css">
			#jq-precedent-users .jq-add-button {display:none}
			#jq-project-members .jq-mark-count {display:none}
			#jq-project-members .jq-remove-button {display:none}
			tr.jq-member-in-use td.jq-remove-button a.delete- {display:none}
		</style>
		<xsl:if test="$on_empty_text">
			<div id="jq-precedent-users-empty" class="box-light">
				<xsl:if test="$from">
					<xsl:attribute name="class" style="float: left; margin-bottom: 10px;">box-light dn</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="$on_empty_text"/>
			</div>
			<div class="clear"/>
		</xsl:if>
		<table class="tbl" id="jq-precedent-users">
			<xsl:if test="not($from)">
				<xsl:attribute name="class">tbl dn</xsl:attribute>
			</xsl:if>
			<tr>
				<xsl:if test="$current_project/@show_user_numbers = 1">
					<th>
						<span>#</span>
					</th>
				</xsl:if>
				<th/>
				<th>
					<span>Логин</span>
				</th>
				<xsl:if test="$current_project/@use_game_name = 1">
					<th>
						<span>Игровое имя</span>
					</th>
				</xsl:if>
				<th>
					<span>Имя</span>
				</th>
				<th>
					<span>Оценок</span>
				</th>
				<th/>
			</tr>
			<xsl:for-each select="$from">
				<xsl:call-template name="_draw_user_row">
					<xsl:with-param name="user" select="/root/user_short[@id = current()/@id]"/>
				</xsl:call-template>
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template name="draw_precedent_edit_users_in_project">
		<xsl:param name="for"/>
		<div class="precedent-edit-search-user">
			<input jq-placeholder="поиск..." id="jq-precedent-edit-search-user-input" type="text" value=""/>
		</div>
		<table class="tbl" id="jq-project-members">
			<tr>
				<xsl:if test="$current_project/@show_user_numbers = 1">
					<th>
						<span>#</span>
					</th>
				</xsl:if>
				<th/>
				<th>
					<span>Логин</span>
				</th>
				<xsl:if test="$current_project/@use_game_name = 1">
					<th>
						<span>Игровое имя</span>
					</th>
				</xsl:if>
				<th>
					<span>Имя</span>
				</th>
				<th/>
			</tr>
			<xsl:for-each select="/root/project_members/user">
				<xsl:variable name="user" select="current()"/>
				<xsl:for-each select="$for">
					<xsl:call-template name="_draw_user_row">
						<xsl:with-param name="user" select="$user"/>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template name="_draw_user_row">
		<xsl:param name="user"/>
		<tr data-user-id="{$user/@id}">
			<xsl:call-template name="_check_member_in_use">
				<xsl:with-param name="id" select="$user/@id"/>
			</xsl:call-template>
			<xsl:if test="$current_project/@show_user_numbers = 1">
				<td>
					<xsl:value-of select="$user/@number"/>
					<span class="dn">
						<xsl:value-of select="concat('#', $user/@number)"/>
					</span>
				</td>
			</xsl:if>
			<td class="col-user-avatar-small-">
				<span>
					<a href="{$current_project/@url}users/{$user/@login}/">
						<img src="{$user/photo_small/@url}" width="{$user/photo_small/@width}" height="{$user/photo_small/@height}" alt="{$user/@login}"/>
					</a>
				</span>
			</td>
			<td>
				<a href="{$current_project/@url}users/{$user/@login}/">
					<xsl:value-of select="$user/@login"/>
				</a>
			</td>
			<xsl:if test="$current_project/@use_game_name = 1">
				<td>
					<xsl:value-of select="$user/@game_name"/>
				</td>
			</xsl:if>
			<td>
				<xsl:value-of select="$user/@full_name"/>
			</td>
			<td class="col-digits- jq-mark-count">
				<span class="with-hint">
					<xsl:call-template name="draw_mark_count_hint"/>
				</span>
			</td>
			<xsl:for-each select="$user">
				<xsl:call-template name="_draw_user_buttons"/>
			</xsl:for-each>
		</tr>
	</xsl:template>
</xsl:stylesheet>
