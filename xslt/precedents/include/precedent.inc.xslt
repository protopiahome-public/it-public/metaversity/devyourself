<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_precedent_user_avatar">
		<xsl:param name="user_id"/>
		<xsl:param name="role"/>
		<xsl:variable name="this_user" select="/root/user_short[@id = $user_id]"/>
		<xsl:variable name="title">
			<xsl:value-of select="concat('#', $user_id)"/>
			<xsl:if test="$role">
				<xsl:value-of select="concat(' ', $role)"/>
			</xsl:if>
			<xsl:value-of select="concat(': ', $this_user/@visible_name, ' ', $this_user/@login)"/>
		</xsl:variable>
		<span>
			<a href="{$current_project/@url}users/{$this_user/@login}/" title="{$title}">
				<img src="{$this_user/photo_small/@url}" width="{$this_user/photo_small/@width}" height="{$this_user/photo_small/@height}" alt="{$this_user/@login}"/>
			</a>
		</span>
	</xsl:template>
	<xsl:template name="draw_precedent_user_link">
		<xsl:param name="user_id"/>
		<xsl:variable name="this_user" select="/root/user_short[@id = $user_id]"/>
		<a href="{$current_project/@url}users/{$this_user/@login}/">
			<xsl:value-of select="$this_user/@visible_name"/>
		</a>
		<b>
			<xsl:value-of select="$this_user/@login"/>
		</b>
	</xsl:template>
</xsl:stylesheet>
