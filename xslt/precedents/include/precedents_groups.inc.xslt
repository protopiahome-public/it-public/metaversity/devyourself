<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format name="number" NaN="0"/>
	<xsl:template name="draw_precedents_groups">
		<xsl:variable name="this" select="/root/precedents_user_summary"/>
		<xsl:variable name="pgroups" select="/root/precedent_groups[@project_id = $current_project/@id]"/>
		<xsl:if test="$pgroups/group">
			<div class="widget-wrap">
				<div class="widget widget-categories">
					<div class="header- header-light-">
						<table cellspacing="0">
							<tr>
								<td>
									<h2 class="text-">Разделы</h2>
								</td>
							</tr>
						</table>
					</div>
					<div class="items-">
						<div>
							<xsl:attribute name="class">
								<xsl:text>item-outer- </xsl:text>
								<xsl:if test="$this/groups/@selected = 'all'">item-outer-selected-</xsl:if>
							</xsl:attribute>
							<div class="item- level-1-">
								<xsl:choose>
									<xsl:when test="$this/groups/@selected = 'all'">
										<strong>Все</strong>
									</xsl:when>
									<xsl:otherwise>
										<a>
											<xsl:attribute name="href">
												<xsl:choose>
													<xsl:when test="$this/@is_filter = 1">
														<xsl:value-of select="$module_url"/>
														<xsl:if test="$this/statuses/@selected != 'all'">?status=<xsl:value-of select="$this/statuses/@selected"/></xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:call-template name="url_delete_param">
															<xsl:with-param name="param_name" select="'group'"/>
														</xsl:call-template>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:text>Все</xsl:text>
										</a>
									</xsl:otherwise>
								</xsl:choose>
								<span class="count-">
									<xsl:value-of select="$this/groups/@precedent_count"/>
								</span>
							</div>
						</div>
						<xsl:for-each select="$pgroups/group">
							<xsl:variable name="is_current" select="@id = $this/groups/@selected"/>
							<xsl:variable name="precedent_count">
								<xsl:choose>
									<xsl:when test="$this/groups/group[@id=current()/@id]">
										<xsl:value-of select="$this/groups/group[@id=current()/@id]/@precedent_count"/>
									</xsl:when>
									<xsl:otherwise>0</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<div>
								<xsl:attribute name="class">
									<xsl:text>item-outer- </xsl:text>
									<xsl:if test="$is_current">item-outer-selected-</xsl:if>
								</xsl:attribute>
								<div class="item- level-1-">
									<xsl:choose>
										<xsl:when test="$is_current">
											<strong>
												<xsl:value-of select="@title"/>
											</strong>
										</xsl:when>
										<xsl:otherwise>
											<a>
												<xsl:attribute name="href">
													<xsl:choose>
														<xsl:when test="$this/@is_filter = 1">
															<xsl:value-of select="concat($module_url, '?')"/>
															<xsl:if test="$this/statuses/@selected != 'all'">status=<xsl:value-of select="$this/statuses/@selected"/>&amp;</xsl:if>
															<xsl:value-of select="concat('group=', @id)"/>
														</xsl:when>
														<xsl:otherwise>
															<xsl:call-template name="url_replace_param">
																<xsl:with-param name="param_name" select="'group'"/>
																<xsl:with-param name="param_value" select="@id"/>
															</xsl:call-template>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:attribute>
												<xsl:value-of select="@title"/>
											</a>
										</xsl:otherwise>
									</xsl:choose>
									<span class="count-">
										<xsl:value-of select="$precedent_count"/>
									</span>
								</div>
							</div>
						</xsl:for-each>
					</div>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
