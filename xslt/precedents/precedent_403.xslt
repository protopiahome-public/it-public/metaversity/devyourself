<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="search/precedents_search.inc.xslt"/>
	<xsl:include href="include/precedents_groups.inc.xslt"/>
	<xsl:include href="include/precedent.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'precedents'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="precedents_url" select="concat($current_project/@url, 'precedents/')"/>
	<xsl:variable name="module_url" select="$precedents_url"/>
	<xsl:variable name="precedent_full" select="/root/precedent_full"/>
	<xsl:variable name="precedent_data" select="/root/precedent_data"/>
	<xsl:template match="error_403">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Прецеденты</h2>
							<div class="content">
								<div class="box box-stop">
									<h3>Доступ запрещён</h3>
									<p>У вас нет прав на редактирование прецедентов в этом проекте.</p>
									<p>Чтобы редактировать прецеденты, нужно быть администратором или модератором прецедентов.</p>
								</div>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Нет доступа &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
