<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/precedent_edit_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'precedents'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($precedents_url, 'add/')"/>
	<xsl:template match="precedent_add">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Прецеденты</h2>
							<div class="content">
								<h3>Добавление прецедента</h3>
								<xsl:call-template name="draw_precedent_edit_submenu"/>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/precedent_add/" method="post">
			<input type="hidden" name="retpath" value="{$precedents_url}precedent-%ID%/edit/ratees/"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<xsl:call-template name="draw_dt_edit">
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Добавление прецедента &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
