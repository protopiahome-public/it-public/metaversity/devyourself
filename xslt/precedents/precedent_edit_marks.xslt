<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/precedent_edit_submenu.inc.xslt"/>
	<xsl:include href="include/precedent.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'precedents'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($precedent_url, 'edit/marks/')"/>
	<xsl:variable name="default_competence_set_id" select="/root/precedent_edit_marks/@default_competence_set_id"/>
	<xsl:variable name="selected_competence_set_id" select="/root/precedent_edit_marks/@selected_competence_set_id"/>
	<xsl:variable name="competence_set_competences" select="/root/competence_set_competences[@competence_set_id = $selected_competence_set_id]"/>
	<xsl:variable name="rater_user_id" select="/root/precedent_edit_marks/@rater_user_id"/>
<!--		<xsl:choose>
			<xsl:when test="/root/precedent_edit_marks/@rater_user_id">
				<xsl:value-of select="/root/precedent_edit_marks/@rater_user_id"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$user/@id"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable> -->
	<xsl:template match="precedent_edit_marks">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Прецеденты</h2>
							<div class="content">
								<h3>
									<a href="{$precedent_url}">
										<xsl:value-of select="$precedent_full/@title"/>
									</a>
								</h3>
								<xsl:call-template name="draw_precedent_edit_submenu"/>
								<h4 class="precedent-edit-header">Оценки</h4>
								<xsl:variable name="has_ratees" select="$precedent_data/ratees/user"/>
								<xsl:variable name="has_competence_groups" select="./competence_group"/>
								<xsl:choose>
									<!-- @dm9 CR -->
									<xsl:when test="$has_ratees and $has_competence_groups">
										<div>
											<xsl:if test="($rater_user_id = 0) or (@can_select_ratee)">
												<xsl:call-template name="_draw_select_rater"/>
											</xsl:if>
											<xsl:if test="$rater_user_id != 0">
												<xsl:call-template name="_draw_select_competence_set"/>
											</xsl:if>
										</div>
										<xsl:if test="$rater_user_id != 0">
											<div class="clear"/>
											<xsl:call-template name="_draw_form"/>
										</xsl:if>
									</xsl:when>
									<xsl:otherwise>
										<div class="box-light" style="float: left;">
											<xsl:text>Чтобы проставить оценки, нужно сначала выбрать </xsl:text>
											<xsl:if test="not($has_ratees)">
												<a href="{$precedent_url}edit/ratees/">участников прецедента</a>
												<xsl:if test="not($has_competence_groups)"> и </xsl:if>
											</xsl:if>
											<xsl:if test="not($has_competence_groups)">
												<a href="{$precedent_url}edit/competences/">группы компетенций</a>
											</xsl:if>
											<xsl:text>.</xsl:text>
										</div>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="dn">
			<div id="jq-lightbox-edit-mark">
				<!-- fake user -->
				<div class="user-" id="ratee-block-placeholder"></div>
				<!-- fake competence -->
				<div class="text-" id="competence-title-placeholder"></div>
				<!-- /fake competence -->
				<div id="jq-edit-mark-value" class="jq-v0">
					<span class="mark0 clickable" data-value="0" title="Нет оценки">Нет&#160;оценки</span>
					<span class="mark1 clickable" data-value="1" title="Склонность">Склонность</span>
					<span class="mark2 clickable" data-value="2" title="�пособность">Способность</span>
					<span class="mark3 clickable" data-value="3" title="Компетенция">Компетенция</span>
				</div>
				<textarea id="jq-edit-mark-comment"/>
			</div>
			<xsl:for-each select="$precedent_data/flash_group">
				<div id="{concat('group-block-', @id)}">
					<b>
						<xsl:value-of select="@title"/>
					</b>
					<xsl:text> &#8212; групповая оценка</xsl:text>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template name="_draw_select_rater">
		<div class="field">
			<label for="jq-rater-select" class="select-label-">Проставить оценки от имени: </label>
			<select id="jq-rater-select" class="select-">
				<xsl:if test="$rater_user_id = 0">
					<option value="0">(выберите пользователя)</option>
				</xsl:if>
				<optgroup label="От моего имени">
					<option value="{$user/@id}">
						<xsl:if test="$rater_user_id = $user/@id">
							<xsl:attribute name="selected"/>
						</xsl:if>
						<xsl:value-of select="$user/@visible_name"/>
					</option>
				</optgroup>
				<xsl:if test="$precedent_data/raters/user[@id != $user/@id]">
					<optgroup label="Другие оценщики">
						<xsl:for-each select="$precedent_data/raters/user[@id != $user/@id]">
							<option value="{@id}">
								<xsl:if test="$rater_user_id = @id">
									<xsl:attribute name="selected"/>
								</xsl:if>
								<xsl:value-of select="/root/user_short[@id = current()/@id]/@visible_name"/>
							</option>
						</xsl:for-each>
					</optgroup>
				</xsl:if>
			</select>
		</div>
	</xsl:template>
	<xsl:template name="_draw_select_competence_set">
		<div class="field">
			<select id="jq-competence-set-select" class="select-">
				<xsl:if test="$precedent_data/competence_sets/competence_set[@id = $default_competence_set_id]">
					<optgroup label="Набор компетенций по умолчанию">
						<xsl:call-template name="_draw_competence_set_option">
							<xsl:with-param name="id" select="$default_competence_set_id"/>
						</xsl:call-template>
					</optgroup>
				</xsl:if>
				<xsl:if test="$precedent_data/competence_sets/competence_set[@id != $default_competence_set_id]">
					<optgroup label="Используемые наборы компетенций">
						<xsl:for-each select="$precedent_data/competence_sets/competence_set[@id != $default_competence_set_id]">
							<xsl:call-template name="_draw_competence_set_option"/>
						</xsl:for-each>
					</optgroup>
				</xsl:if>
			</select>
			<label class="bigger" style="padding: 0 10px;">&#8594;</label>
			<a href="{$prefix}/sets/set-{$selected_competence_set_id}/">Показать&#160;описание</a>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<table class="jq-mark-table precedent-edit-marks-table">
			<xsl:for-each select="$competence_set_competences//group">
				<xsl:variable name="competence_group" select="current()"/>
				<xsl:if test="/root/precedent_edit_marks/competence_group[@id = current()/@id] and current()/competence">
					<xsl:if test="count($precedent_data/flash_group) > 1">
						<xsl:variable name="rows" select="count($competence_group/competence) + 2"/>
						<tr class="sub-header-">
							<th/>
							<xsl:for-each select="$precedent_data/flash_group">
								<xsl:if test="user or marks/mark">
									<xsl:if test="position() > 1">
										<th class="separate-" rowspan="{$rows}">
											<div/>
										</th>
									</xsl:if>
									<th colspan="{count(user) + 1}">
										<xsl:attribute name="class">
											<xsl:text>tab-</xsl:text>
											<xsl:choose>
												<xsl:when test="position() = 1">
													<xsl:text> first-</xsl:text>
												</xsl:when>
											</xsl:choose>
										</xsl:attribute>
										<xsl:value-of select="@title"/>
									</th>
								</xsl:if>
							</xsl:for-each>
						</tr>
					</xsl:if>
					<tr class="header- odd-">
						<td class="title-">
							<xsl:value-of select="$competence_group/@title"/>
						</td>
						<xsl:for-each select="$precedent_data/flash_group">
							<xsl:if test="user or marks/mark">
								<xsl:for-each select="user">
									<td>
										<xsl:attribute name="class">
											<xsl:text>user-</xsl:text>
											<xsl:if test="position() = 1">
												<xsl:text> first-</xsl:text>
											</xsl:if>
										</xsl:attribute>
										<table class="user-block" id="{concat('user-block-', @link_id)}">
											<tr>
												<td class="avatar-">
													<xsl:call-template name="draw_precedent_user_avatar">
														<xsl:with-param name="user_id" select="@id"/>
													</xsl:call-template>
												</td>
												<td class="text-">
													<xsl:call-template name="draw_precedent_user_link">
														<xsl:with-param name="user_id" select="@id"/>
													</xsl:call-template>
												</td>
											</tr>
										</table>
									</td>
								</xsl:for-each>
								<td class="mark-">Групповая оценка</td>
							</xsl:if>
						</xsl:for-each>
					</tr>
					<xsl:for-each select="$competence_group/competence">
						<xsl:variable name="competence" select="current()"/>
						<tr>
							<xsl:attribute name="class">
								<xsl:text>body-</xsl:text>
								<xsl:choose>
									<xsl:when test="(position() mod 2) != 1">
										<xsl:text> odd-</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text> even-</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:if test="position() = last()">
									<xsl:text> last-</xsl:text>
								</xsl:if>
							</xsl:attribute>
							<td class="title-" id="competence-{@id}">
								<xsl:value-of select="@title"/>
							</td>
							<xsl:for-each select="$precedent_data/flash_group">
								<xsl:if test="user or marks/mark">
									<xsl:for-each select="user">
										<td data-competence-id="{$competence/@id}" data-competence-set-id="{$selected_competence_set_id}" data-user-link-id="{@link_id}">
											<xsl:attribute name="class">
												<xsl:text>jq-mark-personal mark-</xsl:text>
												<xsl:if test="position() = 1">
													<xsl:text> first-</xsl:text>
												</xsl:if>
											</xsl:attribute>
											<xsl:call-template name="_draw_mark">
												<xsl:with-param name="mark" select="marks/mark[(@competence_id = $competence/@id) and (@rater_user_id = $rater_user_id)]"/>
											</xsl:call-template>
										</td>
									</xsl:for-each>
									<td class="jq-mark-group mark- mark-group-" data-competence-id="{$competence/@id}" data-competence-set-id="{$selected_competence_set_id}" data-flash-group-id="{@id}">
										<xsl:call-template name="_draw_mark">
											<xsl:with-param name="mark" select="marks/mark[(@competence_id = $competence/@id) and (@rater_user_id = $rater_user_id)]"/>
										</xsl:call-template>
									</td>
								</xsl:if>
							</xsl:for-each>
						</tr>
						<xsl:if test="position() = last()">
							<tr class="separate-line-">
								<td colspan="{count($precedent_data/flash_group/user) + count($precedent_data/flash_group) * 2}">&#160;</td>
							</tr>
						</xsl:if>
					</xsl:for-each>
				</xsl:if>
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template name="_draw_competence_set_option">
		<xsl:param name="id" select="current()/@id"/>
		<option value="{$id}">
			<xsl:if test="$id = $selected_competence_set_id">
				<xsl:attribute name="selected"/>
			</xsl:if>
			<xsl:value-of select="/root/competence_set_fast_navigation/competence_set[@id = $id]/@title"/>
		</option>
	</xsl:template>
	<xsl:template name="_draw_mark">
		<xsl:param name="mark"/>
		<table>
			<td>
				<xsl:attribute name="class">
					<xsl:text>jq-mark-value value- </xsl:text>
					<xsl:choose>
						<xsl:when test="$mark">jq-v<xsl:value-of select="$mark/@value"/>
						</xsl:when>
						<xsl:otherwise>jq-v0</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="data-value">
					<xsl:choose>
						<xsl:when test="$mark">
							<xsl:value-of select="$mark/@value"/>
						</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<span class="mark1">Ск.</span>
				<span class="mark2">Сп.</span>
				<span class="mark3">К.</span>
			</td>
			<td class="jq-mark-comment text-">
				<xsl:if test="$mark">
					<xsl:copy-of select="$mark/comment/text()"/>
				</xsl:if>
			</td>
		</table>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<!-- @dm9 CR -->
		<style type="text/css">
			.jq-mark-value span {display:none}
			.jq-mark-value.jq-v1 span.mark1 {display:inline-block}
			.jq-mark-value.jq-v2 span.mark2 {display:inline-block}
			.jq-mark-value.jq-v3 span.mark3  {display:inline-block}
			.jq-mark-comment {white-space: pre-wrap; }
			
			#jq-lightbox-edit-mark .text- {margin: 5px 0 13px;}
			#jq-lightbox-edit-mark .user-block .avatar- span {margin-top: 1px;}
			#jq-lightbox-edit-mark .user-block .text- {padding-top: 0px;}
			
			#jq-edit-mark-value {}
			#jq-edit-mark-value span {margin: 0 14px 0 0; padding:1px 0px 1px 0px; border-bottom: 1px dashed #000000;}
			
			#jq-lightbox-edit-mark.mark-group-	#jq-edit-mark-value.jq-v0 span.mark0,
			#jq-lightbox-edit-mark.mark-group- #jq-edit-mark-value.jq-v1 span.mark1,
			#jq-lightbox-edit-mark.mark-group- #jq-edit-mark-value.jq-v2 span.mark2,
			#jq-lightbox-edit-mark.mark-group- #jq-edit-mark-value.jq-v3 span.mark3 {background: #0030df; color: #fff; border: none;}
			#jq-lightbox-edit-mark.mark-personal- #jq-edit-mark-value.jq-v0 span.mark0,
			#jq-lightbox-edit-mark.mark-personal- #jq-edit-mark-value.jq-v1 span.mark1,
			#jq-lightbox-edit-mark.mark-personal- #jq-edit-mark-value.jq-v2 span.mark2,
			#jq-lightbox-edit-mark.mark-personal- #jq-edit-mark-value.jq-v3 span.mark3 {background: #df0000; color: #fff; border: none;}
			
			#jq-edit-mark-comment.jq-disabled {background:#e7e7e7; font-size: 12px; color: #999999;}
			#jq-edit-mark-comment {width: 389px; height: 75px; margin-top: 20px; border: solid 1px #c4c4c4; font-size: 12px;}
		</style>
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/precedent_edit_marks.js"/>
		<script type="text/javascript">
			 window.post_vars = {
				project_id : <xsl:value-of select="precedent_edit_marks/@project_id"/>,
				precedent_id : <xsl:value-of select="precedent_edit_marks/@precedent_id"/>,
				rater_user_id : <xsl:value-of select="$rater_user_id"/>
			};
		</script>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Оценки &#8212; </xsl:text>
		<xsl:value-of select="$precedent_full/@title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
