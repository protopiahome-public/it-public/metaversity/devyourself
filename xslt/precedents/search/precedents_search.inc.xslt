<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_precedents_search">
		<div class="box-light" style="width: 280px; float: right; margin-bottom: 10px;">
			<div class="content-">
				<xsl:variable name="module_filters" select="filter"/>
				<form id="search-form" class="search-form" action="{$module_url}" method="get">
					<xsl:for-each select="$get_vars">
						<xsl:if test="not(substring(@name, 1, 6) = 'filter' or @name = 'status' or @name = 'group')">
							<xsl:call-template name="get_param_for_form"/>
						</xsl:if>
					</xsl:for-each>
					<div class="line-">
						<div class="el- el-text-">
							<input type="text" id="search_search" name="filter[search]" jq-placeholder="поиск по названию" value="{filters/filter[@name = 'search']/@value}" maxlength="255"/>
						</div>
						<div class="el- el-btn-">
							<button class="button button-small">Искать</button>
						</div>
					</div>
				</form>
				<div class="clear"/>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
