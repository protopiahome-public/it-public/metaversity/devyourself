<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/precedent_edit_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'precedents'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($precedent_url, 'edit/')"/>
	<xsl:template match="precedent_edit">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Прецеденты</h2>
							<div class="content">
								<h3>
									<a href="{$precedent_url}">
										<xsl:value-of select="$precedent_full/@title"/>
									</a>
								</h3>
								<xsl:call-template name="draw_precedent_edit_submenu"/>
								<h4 class="precedent-edit-header">Основное</h4>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/precedent_edit/" method="post">
			<input type="hidden" name="retpath" value="{$precedent_url}edit/ratees/"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<xsl:call-template name="draw_dt_edit">
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Основное &#8212; </xsl:text>
		<xsl:value-of select="$precedent_full/@title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
