<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/precedent_edit_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'precedents'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($precedent_url, 'edit/flash-groups/')"/>
	<xsl:template match="precedent_edit_flash_groups">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Прецеденты</h2>
							<div class="content">
								<h3>
									<a href="{$precedent_url}">
										<xsl:value-of select="$precedent_full/@title"/>
									</a>
								</h3>
								<xsl:call-template name="draw_precedent_edit_submenu"/>
								<h4 class="precedent-edit-header">Флэш-группы</h4>
								<xsl:choose>
									<xsl:when test="$precedent_data/ratees/user">
										<xsl:call-template name="_draw_form"/>
									</xsl:when>
									<xsl:otherwise>
										<div class="box-light" style="float: left;">Чтобы формировать флеш-группы, нужно сначала <a href="{$precedent_url}edit/ratees/">выбрать участников прецедента</a>.</div>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div id="jq-dialog-confirm" class="dn">
			<p>
				<xsl:text>Подтвердите: флэш-группа </xsl:text>
				<strong id="jq-dialog-confirm-flash-group-title">???</strong>
				<xsl:text> будет удалена.</xsl:text>
			</p>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<xsl:variable name="precedent_data" select="/root/precedent_data"/>
		<xsl:variable name="default_flash_group_id" select="$precedent_data/@default_flash_group_id"/>
		<table class="tbl" id="jq-flash-groups">
			<tr>
				<th>
					<span>#</span>
				</th>
				<th/>
				<th>
					<span>Логин</span>
				</th>
				<th/>
				<th>
					<span>Имя</span>
				</th>
				<xsl:for-each select="$precedent_data/flash_group">
					<xsl:sort select="@id != $default_flash_group_id"/>
					<th>
						<span>
							<span id="jq-flash-group-title-{@id}">
								<xsl:value-of select="@title"/>
							</span>
							<span class="icons-small" data-flash-group-id="{@id}">
								<xsl:if test="(@id != $default_flash_group_id)">
									<a class="jq-flash-group-edit edit- gray-" href="#" title="Редактировать"/>
								</xsl:if>
								<xsl:if test="(@id != $default_flash_group_id) and not (marks/mark or user/marks/mark)">
									<a class="jq-flash-group-delete delete- gray-" href="#" title="Удалить"/>
								</xsl:if>
							</span>
						</span>
					</th>
				</xsl:for-each>
				<th>
					<span class="jq-flash-group-add ico-links-small">
						<a class="link add-" style="margin-left: 29px;">
							<span class="clickable">Создать группу</span>
						</a>
					</span>
				</th>
			</tr>
			<xsl:for-each select="$precedent_data/flash_group[@id=$default_flash_group_id]/user">
				<xsl:variable name="user" select="/root/user_short[@id = current()/@id]"/>
				<tr>
					<td>
						<xsl:value-of select="$user/@id"/>
						<span class="dn">#<xsl:value-of select="$user/@id"/>
						</span>
					</td>
					<td class="col-user-avatar-small-">
						<span>
							<a href="{$current_project/@url}users/{$user/@login}/">
								<img src="{$user/photo_small/@url}" width="{$user/photo_small/@width}" height="{$user/photo_small/@height}" alt="{$user/@login}"/>
							</a>
						</span>
					</td>
					<td>
						<a href="{$current_project/@url}users/{$user/@login}/">
							<xsl:value-of select="$user/@login"/>
						</a>
					</td>
					<td>
						<xsl:value-of select="$user/@game_name"/>
					</td>
					<td>
						<xsl:value-of select="$user/@full_name"/>
					</td>
					<xsl:for-each select="$precedent_data/flash_group">
						<xsl:sort select="@id != $default_flash_group_id"/>
						<td class="col-center-">
							<input type="checkbox" class="jq-user-selection" data-user-id="{$user/@id}" data-flash-group-id="{@id}">
								<xsl:if test="(@id = $default_flash_group_id) or (user[@id = $user/@id]/marks/mark)">
									<xsl:attribute name="disabled">1</xsl:attribute>
								</xsl:if>
								<xsl:if test="$precedent_data/flash_group[@id = current()/@id]/user[@id = $user/@id]">
									<xsl:attribute name="checked">1</xsl:attribute>
								</xsl:if>
							</input>
						</td>
					</xsl:for-each>
					<td class="col-no-line-">&#160;</td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/precedent_edit_flash_groups.js"/>
		<script type="text/javascript">
			var post_vars = {
				project_id : <xsl:value-of select="precedent_edit_flash_groups/@project_id"/>,
				precedent_id : <xsl:value-of select="precedent_edit_flash_groups/@precedent_id"/>
			};
		</script>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Флэш-группы &#8212; </xsl:text>
		<xsl:value-of select="$precedent_full/@title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
