<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="search/precedents_search.inc.xslt"/>
	<xsl:include href="include/precedents_groups.inc.xslt"/>
	<xsl:include href="include/precedent.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'precedents'"/>
	<xsl:variable name="project_section_main_page" select="/root/precedents/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="precedents_url" select="concat($current_project/@url, 'precedents/')"/>
	<xsl:variable name="module_url" select="$precedents_url"/>
	<xsl:template match="precedents">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Прецеденты</h2>
							<div class="content">
								<xsl:call-template name="draw_precedents_search"/>
								<xsl:call-template name="_draw_table"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:if test="$user/@id">
								<xsl:call-template name="_draw_my_summary"/>
							</xsl:if>
							<xsl:call-template name="draw_precedents_groups"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="status">
		<span class="with-hint">
			<xsl:call-template name="draw_mark_count_hint">
				<xsl:with-param name="position" select="1"/>
			</xsl:call-template>
		</span>
	</xsl:template>
	<xsl:template name="_draw_my_summary">
		<xsl:variable name="summary" select="/root/precedents_user_summary/statuses"/>
		<div class="widget-wrap">
			<div class="widget widget-precedents-my-summary">
				<div class="header-">
					<table cellspacing="0">
						<tr>
							<td>
								<h2 class="text-">Ваши прецеденты</h2>
							</td>
						</tr>
					</table>
				</div>
				<!-- блок отображается всегда -->
				<div class="block- block-first-">
					<div class="head-">Вас оценивают</div>
					<div class="line-">
						<a href="{$module_url}?status=ratee">
							<xsl:call-template name="_draw_precedent_count">
								<xsl:with-param name="count" select="$summary/status[@type='ratee']/@precedent_count"/>
							</xsl:call-template>
						</a>
					</div>
					<div class="line-">
						<xsl:apply-templates select="$summary/status[@type='ratee']"/>
					</div>
				</div>
				<!-- только если есть права оценивания -->
				<xsl:if test="$summary/status[@type='rater']">
					<hr/>
					<div class="block-">
						<div class="head-">Вы оцениваете</div>
						<div class="line-">
							<a href="{$module_url}?status=rater">
								<xsl:call-template name="_draw_precedent_count">
									<xsl:with-param name="count" select="$summary/status[@type='rater']/@precedent_count"/>
								</xsl:call-template>
							</a>
							<xsl:if test="$summary/status[@type='rater_todo']">
								<xsl:text> (</xsl:text>
								<a href="{$module_url}?status=rater-todo">
									<xsl:text>не готово </xsl:text>
									<xsl:value-of select="$summary/status[@type='rater_todo']/@precedent_count"/>
								</a>
								<xsl:text>)</xsl:text>
							</xsl:if>
						</div>
						<div class="line-">
							<xsl:apply-templates select="$summary/status[@type='rater']"/>
						</div>
					</div>
				</xsl:if>
				<!-- только если есть права оценивания и рекомендованные прецеденты -->
				<xsl:if test="$summary/status[@type='unrated']">
					<hr/>
					<div class="block-">
						<div class="head-">Рекомендуем оценить</div>
						<div class="line-">
							<a href="{$module_url}?status=unrated">
								<xsl:call-template name="_draw_precedent_count">
									<xsl:with-param name="count" select="$summary/status[@type='unrated']/@precedent_count"/>
								</xsl:call-template>
							</a>
						</div>
						<div class="comment-">(вы участник, но пока не оценивали)</div>
					</div>
				</xsl:if>
				<!-- только если ты аналитик -->
				<xsl:if test="$summary/status[@type='responsible']">
					<hr/>
					<div class="block-">
						<div class="head-">Вы ответственный</div>
						<div class="line-">
							<a href="{$module_url}?status=responsible">
								<xsl:call-template name="_draw_precedent_count">
									<xsl:with-param name="count" select="$summary/status[@type='responsible']/@precedent_count"/>
								</xsl:call-template>
							</a>
							<xsl:if test="$summary/status[@type='responsible_todo']">
								<xsl:text> (</xsl:text>
								<a href="{$module_url}?status=responsible-todo">
									<xsl:text>не готово </xsl:text>
									<xsl:value-of select="$summary/status[@type='responsible_todo']/@precedent_count"/>
								</a>
								<xsl:text>)</xsl:text>
							</xsl:if>
						</div>
						<div class="line-">
							<xsl:apply-templates select="$summary/status[@type='responsible']"/>
						</div>
					</div>
				</xsl:if>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_table">
		<xsl:variable name="precedents_user_summary" select="/root/precedents_user_summary"/>
		<xsl:variable name="selected_group" select="$precedents_user_summary/groups/@selected"/>
		<xsl:variable name="selected_status" select="$precedents_user_summary/statuses/@selected"/>
		<xsl:variable name="href_without_group">
			<xsl:choose>
				<xsl:when test="$precedents_user_summary/@is_filter = 1">
					<xsl:value-of select="$module_url"/>
					<xsl:if test="$selected_status != 'all'">?status=<xsl:value-of select="$selected_status"/>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="url_delete_param">
						<xsl:with-param name="param_name" select="'group'"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<div class="ico-links">
			<xsl:if test="$project_access/@can_moderate_precedents=1">
				<a class="add-" href="{$precedents_url}add/">
					<span>добавить прецедент</span>
				</a>
			</xsl:if>
		</div>
		<xsl:choose>
			<xsl:when test="filters/filter[@name='search']/@is_active = '1'">
				<!-- Independent case 1: search -->
				<p style="margin-top: 40px;">
					Поиск по названию: <strong>
						<xsl:value-of select="filters/filter[@name='search']/@value"/>
					</strong>. <a href="{$module_url}">Показать все прецеденты</a>.
				</p>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$selected_status = 'ratee'">
						<!-- Case 2A: status=ratee -->
						<h3 class="down">Прецеденты с вашим участием <a class="minor" href="{$module_url}">показать все</a>
						</h3>
					</xsl:when>
					<xsl:when test="$selected_status = 'rater'">
						<!-- Case 2B: status=rater -->
						<h3 class="down">Прецеденты, оцененные вами <a class="minor" href="{$module_url}">показать все</a>
						</h3>
					</xsl:when>
					<xsl:when test="$selected_status = 'rater-todo'">
						<!-- Case 2C: status=rater-todo -->
						<h3 class="down">Прецеденты, оцененные вами (не готовые) <a class="minor" href="{$module_url}">показать все</a>
						</h3>
						<div class="box">
							<p>Прецедент будет считаться &#171;готовым&#187; только когда все три иконки готовности: описание, участники и оценки будут переключены в &#171;зелёное&#187; состояние. Переключить иконки можно на странице редактирования прецедента, нажав на них.</p>
						</div>
					</xsl:when>
					<xsl:when test="$selected_status = 'unrated'">
						<!-- Case 2D: status=unrated -->
						<h3 class="down">Прецеденты, рекомендованны к оценке <a class="minor" href="{$module_url}">показать все</a>
						</h3>
						<div class="box">
							<p>Показаны те прецеденты, в которых вы отмечены как участник, но в которых вы пока не ставили оценок.</p>
						</div>
					</xsl:when>
					<xsl:when test="$selected_status = 'responsible'">
						<!-- Case 2E: status=responsible -->
						<h3 class="down">Прецеденты, за которые вы отвечаете <a class="minor" href="{$module_url}">показать все</a>
						</h3>
					</xsl:when>
					<xsl:when test="$selected_status = 'responsible-todo'">
						<!-- Case 2F: status=responsible-todo -->
						<h3 class="down">Прецеденты, за которые вы отвечаете (не готовые) <a class="minor" href="{$module_url}">показать все</a>
						</h3>
						<div class="box">
							<p>Прецедент будет считаться &#171;готовым&#187; только когда все три иконки готовности: описание, участники и оценки будут переключены в &#171;зелёное&#187; состояние. Переключить иконки можно на странице редактирования прецедента, нажав на них.</p>
						</div>
					</xsl:when>
				</xsl:choose>
				<xsl:if test="$selected_group != 'all'">
					<!-- Case 3: group filtering -->
					<p>
						<xsl:if test="$selected_status = 'all'">
							<xsl:attribute name="style">margin-top: 40px;</xsl:attribute>
						</xsl:if>
						Группа: <strong>
							<xsl:value-of select="/root/precedent_groups/group[@id=$selected_group]/@title"/>
						</strong>. <a class="minor" href="{$href_without_group}">убрать фильтрацию по группе</a>
					</p>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="not(precedent)">
			<p>Ни одного прецедента не найдено.</p>
		</xsl:if>
		<xsl:if test="precedent">
			<p class="note">
				<xsl:text>Отображаются прецеденты </xsl:text>
				<xsl:value-of select="pages/@from_row"/>
				<xsl:text> &#8212; </xsl:text>
				<xsl:value-of select="pages/@to_row"/>
				<xsl:text> из </xsl:text>
				<xsl:value-of select="pages/@row_count"/>
				<xsl:text>.</xsl:text>
			</p>
			<table class="tbl">
				<tr>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="'Прецедент'"/>
					</xsl:call-template>
					<th>
						<span>Участники</span>
					</th>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'marks'"/>
						<xsl:with-param name="title" select="'Оценок'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<th>
						<span>Оценщики</span>
					</th>
					<xsl:if test="$project_access/@can_moderate_precedents = 1">
						<th>
							<span>Готовность</span>
						</th>
						<xsl:call-template name="draw_tbl_th">
							<xsl:with-param name="name" select="'access'"/>
							<xsl:with-param name="title" select="'Доступ'"/>
							<xsl:with-param name="center" select="true()"/>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="$project_access/@can_moderate_marks = 1">
						<th>
							<span>Отвечает</span>
						</th>
					</xsl:if>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'date'"/>
						<xsl:with-param name="title" select="'Добавлен'"/>
					</xsl:call-template>
					<!-- @prec -->
					<!--<th/>-->
				</tr>
				<xsl:for-each select="precedent">
					<tr>
						<td class="col-title-" style="min-width: 260px;">
							<a href="{$module_url}precedent-{@id}/">
								<xsl:value-of select="@title"/>
							</a>
							<xsl:if test="$project_access/@can_moderate_precedents=1">
								<span class="icons- icons-small">
									<a class="edit- gray-" href="{$module_url}precedent-{@id}/edit/" title="Редактировать"/>
								</span>
							</xsl:if>
						</td>
						<td class="col-user-avatars-">
							<xsl:for-each select="/root/precedent_to_ratee/link[@precedent_id=current()/@id]">
								<xsl:call-template name="draw_precedent_user_avatar">
									<xsl:with-param name="user_id" select="@user_id"/>
								</xsl:call-template>
							</xsl:for-each>
						</td>
						<td class="col-digits-">
							<span class="with-hint">
								<xsl:call-template name="draw_mark_count_hint"/>
							</span>
						</td>
						<td class="col-user-avatars-">
							<xsl:for-each select="/root/precedent_to_rater/link[@precedent_id=current()/@id]">
								<xsl:call-template name="draw_precedent_user_avatar">
									<xsl:with-param name="user_id" select="@user_id"/>
								</xsl:call-template>
							</xsl:for-each>
						</td>
						<xsl:if test="$project_access/@can_moderate_precedents = 1">
							<td>
								<div class="precedent-complete">
									<div class="outer-">
										<span class="descr-">
											<b>
												<xsl:attribute name="title">
													<xsl:choose>
														<xsl:when test="@complete_descr = 1">Описание заполнено</xsl:when>
														<xsl:otherwise>Описание не заполнено</xsl:otherwise>
													</xsl:choose>
												</xsl:attribute>
												<xsl:if test="@complete_descr = 1">
													<xsl:attribute name="class">complete-</xsl:attribute>
												</xsl:if>
											</b>
										</span>
										<span class="users-">
											<b>
												<xsl:attribute name="title">
													<xsl:choose>
														<xsl:when test="@complete_members = 1">Участники указаны</xsl:when>
														<xsl:otherwise>Участники не указаны</xsl:otherwise>
													</xsl:choose>
												</xsl:attribute>
												<xsl:if test="@complete_members = 1">
													<xsl:attribute name="class">complete-</xsl:attribute>
												</xsl:if>
											</b>
										</span>
										<span class="marks-">
											<b>
												<xsl:attribute name="title">
													<xsl:choose>
														<xsl:when test="@complete_marks = 1">Оценки проставлены</xsl:when>
														<xsl:otherwise>Оценки не проставлены</xsl:otherwise>
													</xsl:choose>
												</xsl:attribute>
												<xsl:if test="@complete_marks = 1">
													<xsl:attribute name="class">complete-</xsl:attribute>
												</xsl:if>
											</b>
										</span>
									</div>
								</div>
							</td>
							<td class="col-precedent-access-">
								<div>
									<xsl:choose>
										<xsl:when test="@access = 'public'">
											<xsl:attribute name="class">precedent-access precedent-access-public-</xsl:attribute>
											<xsl:attribute name="title">Публичный доступ</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="class">precedent-access precedent-access-private-</xsl:attribute>
											<xsl:attribute name="title">Закрыт до окончания проекта</xsl:attribute>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</td>
						</xsl:if>
						<xsl:if test="$project_access/@can_moderate_marks = 1">
							<td class="col-user-avatars-">
								<xsl:if test="@responsible_user_id &gt; 0">
									<xsl:call-template name="draw_precedent_user_avatar">
										<xsl:with-param name="user_id" select="@responsible_user_id"/>
									</xsl:call-template>
								</xsl:if>
							</td>
						</xsl:if>
						<td class="col-date-">
							<xsl:call-template name="get_full_datetime">
								<xsl:with-param name="datetime" select="@add_time"/>
							</xsl:call-template>
						</td>
						<!-- @prec -->
						<!--<xsl:if test="$project_access/@can_moderate_precedents = 1">
							<td class="col-icons- icons">
								<a class="edit- gray-" href="{$module_url}precedent-{@id}/edit/" title="Редактировать"/>
							</td>
						</xsl:if>-->
					</tr>
				</xsl:for-each>
			</table>
			<xsl:apply-templates select="pages[page]"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="_draw_precedent_count">
		<xsl:param name="count"/>
		<xsl:value-of select="$count"/>
		<xsl:call-template name="count_case">
			<xsl:with-param name="number" select="$count"/>
			<xsl:with-param name="word_ns" select="' прецедент'"/>
			<xsl:with-param name="word_gs" select="' прецедента'"/>
			<xsl:with-param name="word_ap" select="' прецедентов'"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="precedent_to_ratee"/>
	<xsl:template match="precedent_to_rater"/>
	<xsl:template match="precedents_user_summary"/>
	<xsl:template mode="title" match="/root">
		<xsl:text>Прецеденты &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
