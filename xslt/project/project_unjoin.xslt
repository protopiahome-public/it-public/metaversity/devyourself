<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/project_head2.inc.xslt"/>
	<xsl:include href="../users/include/user_profile.inc.xslt"/>
	<xsl:include href="menu/project_settings_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'settings'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="settings_section" select="'edit'"/>
	<xsl:variable name="settings_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'settings/')"/>
	<xsl:template match="project_unjoin">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Настройка проектного профиля</h2>
							<xsl:call-template name="draw_project_settings_submenu"/>
							<div class="content">
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form style="margin-top: 40px;" action="{$save_prefix}/project_join/" method="post">
			<input type="hidden" name="retpath" value="{$current_project/@url}"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<input type="hidden" name="join" value="0"/>
			<p>
				<button class="button button-red">Отсоединиться от проекта</button>
			</p>
			<p class="note">После отсоединения Вы больше не будете появляться в списке участников проекта.</p>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Отсоединение от проекта &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
