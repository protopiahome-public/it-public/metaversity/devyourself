<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../include/game_name.inc.xslt"/>
	<xsl:variable name="current_project" select="/root/project_short[1]"/>
	<xsl:variable name="project_access" select="/root/project_access"/>
	<xsl:template name="draw_project_head2">
		<table id="project-head">
			<xsl:if test="$current_project/bg_head_main/@is_uploaded = 1">
				<xsl:attribute name="style">
					<xsl:text>background-repeat: repeat; background-image: url('</xsl:text>
					<xsl:value-of select="$current_project/bg_head_main/@url"/>
					<xsl:text>');</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<tr>
				<td rowspan="2" class="logo-">
					<div class="sep-">
						<div class="transparent- bottomline-"/>
						<xsl:for-each select="$current_project/logo_big">
							<xsl:choose>
								<xsl:when test="$project_section = 'main' and $project_section_main_page">
									<img src="{@url}" width="{@width}" height="{@height}" alt="{../@title}" title=""/>
								</xsl:when>
								<xsl:otherwise>
									<a href="{$current_project/@url}">
										<img src="{@url}" width="{@width}" height="{@height}" alt="{../@title}" title=""/>
									</a>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</div>
				</td>
				<td class="top-">
					<div class="top-wrap-">
						<xsl:if test="$project_access/@is_member = 1 or $project_access/@is_pretender = 1">
							<div class="status-">
								<div>
									<xsl:choose>
										<xsl:when test="$project_access/@status = 'pretender'">
											<xsl:attribute name="class">warning-</xsl:attribute>
											<xsl:text>Ваша заявка на вступление в проект</xsl:text>
											<br/>
											<xsl:text>рассматривается администраторами.</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="class">info- transparent-</xsl:attribute>
											<xsl:choose>
												<xsl:when test="$project_access/@status = 'member'">Вы участник проекта</xsl:when>
												<xsl:when test="$project_access/@status = 'admin'">Вы администратор проекта</xsl:when>
												<xsl:when test="$project_access/@status = 'moderator'">Вы модератор проекта</xsl:when>
											</xsl:choose>
										</xsl:otherwise>
									</xsl:choose>
									<div>
										<xsl:choose>
											<xsl:when test="$current_project/@use_game_name = 1">
												<a href="{$current_project/@url}settings/">настройка профиля</a>
											</xsl:when>
											<xsl:otherwise>
												<a href="#" onclick="$('#project-join-form').submit(); return false;">отсоединиться</a>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
								<form id="project-join-form" action="{$save_prefix}/project_join/" method="post">
									<input type="hidden" name="retpath" value="{$current_project/@url}"/>
									<input type="hidden" name="project_id" value="{$current_project/@id}"/>
									<input type="hidden" name="join" value="0"/>
								</form>
							</div>
						</xsl:if>
						<div class="title-wrap-">
							<div class="title- transparent-">
								<a href="{$current_project/@url}">
									<xsl:value-of select="$current_project/@title"/>
								</a>
							</div>
							<xsl:choose>
								<xsl:when test="$project_access/@status = 'guest'"/>
								<xsl:when test="$project_access/@is_member = 1 or $project_access/@is_pretender = 1"/>
								<xsl:otherwise>
									<div class="button-">
										<form action="{$save_prefix}/project_join/" method="post">
											<input type="hidden" name="project_id" value="{$current_project/@id}"/>
											<input id="jq-join-game-name" type="hidden" name="game_name" value=""/>
											<input type="hidden" name="join" value="1"/>
											<button id="jq-button-join" class="jq-manual-submit button-join">+&#160;присоединиться</button>
										</form>
									</div>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="bottom-">
					<div class="menu-dd-wrap-">
						<xsl:for-each select="/root/project_menu/item[@type = 'folder' and @show_in_menu = 1]">
							<div id="jq-dd-menu-{@id}" class="menu-dd- dn">
								<table cellspacing="0">
									<xsl:for-each select="item[@show_in_menu = 1]">
										<tr>
											<td>
												<xsl:choose>
													<xsl:when test="@type = 'module'">
														<xsl:if test="$project_section = @module_front_section">
															<xsl:attribute name="class">menu-dd-item-selected-</xsl:attribute>
														</xsl:if>
														<a href="{$current_project/@url}{@module_front_url}">
															<xsl:value-of select="@title"/>
														</a>
													</xsl:when>
													<xsl:when test="@type = 'static_page'">
														<xsl:if test="/root/static_page_full[@id = current()/@id or @id = current()//item[@show_in_menu = 1]/@id]">
															<xsl:attribute name="class">menu-dd-item-selected-</xsl:attribute>
														</xsl:if>
														<a href="{$current_project/@url}{@static_page_name}/">
															<xsl:value-of select="@title"/>
														</a>
													</xsl:when>
													<xsl:when test="@type = 'link'">
														<a href="{@link_url}">
															<xsl:value-of select="@title"/>
														</a>
													</xsl:when>
												</xsl:choose>
											</td>
										</tr>
									</xsl:for-each>
								</table>
							</div>
						</xsl:for-each>
					</div>
					<div class="jq-menu menu- transparent-">
						<xsl:for-each select="/root/project_menu/item[@show_in_menu = 1]">
							<xsl:if test="position() = 1 or $project_section != '403'">
								<div class="item-">
									<xsl:choose>
										<xsl:when test="@type = 'module'">
											<xsl:if test="$project_section = @module_front_section">
												<xsl:attribute name="class">item- item-selected-</xsl:attribute>
											</xsl:if>
											<div class="item-inner-">
												<xsl:if test="@module_name = 'admin'">
													<xsl:attribute name="class">item-inner- item-admin-</xsl:attribute>
												</xsl:if>
												<xsl:choose>
													<xsl:when test="$project_section = @module_front_section and $project_section_main_page and not($get_vars)">
														<xsl:value-of select="@title"/>
													</xsl:when>
													<xsl:otherwise>
														<a href="{$current_project/@url}{@module_front_url}">
															<xsl:value-of select="@title"/>
														</a>
													</xsl:otherwise>
												</xsl:choose>
											</div>
										</xsl:when>
										<xsl:when test="@type = 'static_page'">
											<xsl:if test="/root/static_page_full[@id = current()/@id or @id = current()//item[@show_in_menu = 1]/@id]">
												<xsl:attribute name="class">item- item-selected-</xsl:attribute>
											</xsl:if>
											<div class="item-inner-">
												<xsl:choose>
													<xsl:when test="/root/static_page_full[@id = current()/@id]">
														<xsl:value-of select="@title"/>
													</xsl:when>
													<xsl:otherwise>
														<a href="{$current_project/@url}{@static_page_name}/">
															<xsl:value-of select="@title"/>
														</a>
													</xsl:otherwise>
												</xsl:choose>
											</div>
										</xsl:when>
										<xsl:when test="@type = 'link'">
											<div class="item-inner-">
												<a href="{@link_url}">
													<xsl:value-of select="@title"/>
												</a>
											</div>
										</xsl:when>
										<xsl:when test="@type = 'folder'">
											<xsl:if test="/root/static_page_full[@id = current()//item[@show_in_menu = 1]/@id] or $project_section = current()//item[@show_in_menu = 1]/@module_front_section">
												<xsl:attribute name="class">item- item-selected-</xsl:attribute>
											</xsl:if>
											<div class="item-inner-">
												<span data-dd-menu-id="jq-dd-menu-{@id}" class="jq-dd-menu-link clickable">
													<xsl:value-of select="@title"/>
												</span>
											</div>
										</xsl:when>
									</xsl:choose>
								</div>
							</xsl:if>
						</xsl:for-each>
						<xsl:if test="$project_access/@has_admin_rights = 1 or $project_access/@can_moderate_roles = 1">
							<div class="item-">
								<xsl:if test="$project_section = 'admin'">
									<xsl:attribute name="class">item- item-selected-</xsl:attribute>
								</xsl:if>
								<div class="item-inner- item-admin-">
									<xsl:choose>
										<xsl:when test="$project_section = 'admin' and $project_section_main_page and not($get_vars)">
											<xsl:text>Админка</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<a href="{$current_project/@url}admin/">Админка</a>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
						</xsl:if>
						<xsl:if test="/root/project_widgets and $project_access/@can_moderate_widgets = 1">
							<div class="item-">
								<div class="item-inner- item-admin-">
									<span id="jq-widgets-edit-link" class="clickable">Виджеты</span>
								</div>
							</div>
						</xsl:if>
					</div>
				</td>
			</tr>
		</table>
		<xsl:if test="$current_project/@use_game_name = 1">
			<xsl:choose>
				<xsl:when test="$project_access/@status = 'guest'"/>
				<xsl:when test="$project_access/@is_member = 1 or $project_access/@is_pretender = 1"/>
				<xsl:otherwise>
					<div class="dn">
						<div id="jq-join-form">
							<xsl:call-template name="draw_game_name_field"/>
						</div>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="favicon" match="/root">
		<xsl:choose>
			<xsl:when test="$current_project/favicon_main/@is_uploaded = 1">
				<link rel="shortcut icon" type="image/png" href="{$current_project/favicon_main/@url}"/>
			</xsl:when>
			<xsl:otherwise>
				<link rel="shortcut icon" type="image/gif" href="{$prefix}/favicon.png"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="draw_project_bg">
		<xsl:if test="$current_project/bg_body_main/@is_uploaded = 1">
			<xsl:attribute name="style">
				<xsl:value-of select="concat('background-image: url(', $current_project/bg_body_main/@url, '); background-position: 0 0; background-repeat: repeat;')"/>
			</xsl:attribute>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
