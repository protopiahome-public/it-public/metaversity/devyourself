<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/project_head2.inc.xslt"/>
	<xsl:include href="../users/search/users_search.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'users'"/>
	<xsl:variable name="project_section_main_page" select="/root/project_users/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'users/')"/>
	<xsl:template match="project_users">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Участники</h2>
							<div class="content">
								<xsl:call-template name="_draw_table"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_users_search"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_table">
		<xsl:if test="filter/field[@name = 'search'] != ''">
			<p>
				<xsl:text>Результаты поиска по запросу &#171;</xsl:text>
				<strong>
					<xsl:value-of select="filter/field[@name = 'search']"/>
				</strong>
				<xsl:text>&#187;.</xsl:text>
			</p>
		</xsl:if>
		<xsl:if test="not(user)">
			<p>По заданным критериям поиска ни одного пользователя не найдено.</p>
		</xsl:if>
		<xsl:if test="user">
			<p>
				<xsl:text>Отображаются пользователи </xsl:text>
				<xsl:value-of select="pages/@from_row"/>
				<xsl:text> &#8212; </xsl:text>
				<xsl:value-of select="pages/@to_row"/>
				<xsl:text> из </xsl:text>
				<xsl:value-of select="pages/@row_count"/>
				<xsl:text>.</xsl:text>
			</p>
			<table class="tbl">
				<tr>
					<xsl:if test="$current_project/@show_user_numbers = 1">
						<xsl:call-template name="draw_tbl_th">
							<xsl:with-param name="name" select="'number'"/>
							<xsl:with-param name="title" select="'#'"/>
						</xsl:call-template>
					</xsl:if>
					<th/>
					<th>
						<span>
							<xsl:choose>
								<xsl:when test="$current_project/@use_game_name = 1">Игровое имя</xsl:when>
								<xsl:otherwise>Имя</xsl:otherwise>
							</xsl:choose>
						</span>
					</th>
					<th>
						<span>Основной&#160;профиль</span>
					</th>
					<xsl:if test="$current_project/@marks_are_on = 1">
						<xsl:call-template name="draw_tbl_th">
							<xsl:with-param name="name" select="'mark-count'"/>
							<xsl:with-param name="title" select="'Оценок&#160;за&#160;проект'"/>
							<xsl:with-param name="center" select="true()"/>
						</xsl:call-template>
					</xsl:if>
					<th>
						<span>Ссылки</span>
					</th>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'join-time'"/>
						<xsl:with-param name="title" select="'Присоединился'"/>
					</xsl:call-template>
				</tr>
				<xsl:for-each select="user">
					<xsl:variable name="position" select="position()"/>
					<xsl:variable name="user_link" select="."/>
					<tr>
						<xsl:if test="$current_project/@show_user_numbers = 1">
							<td class="col-digits-">
								<xsl:value-of select="@number"/>
							</td>
						</xsl:if>
						<xsl:for-each select="/root/user_short[@id = current()/@id]">
							<td class="col-user-avatar-">
								<span>
									<a href="{$module_url}{@login}/">
										<img src="{photo_big/@url}" width="{photo_big/@width}" height="{photo_big/@height}" alt="{@login}"/>
									</a>
								</span>
							</td>
							<td class="col-user-name-">
								<div class="title-">
									<a href="{$module_url}{@login}/">
										<xsl:value-of select="@visible_name"/>
									</a>
								</div>
								<xsl:if test="$current_project/@use_game_name = 1">
									<div class="second-">
										<xsl:value-of select="@login"/>
									</div>
								</xsl:if>
								<xsl:choose>
									<xsl:when test="$user_link/@status = 'admin'">
										<div class="key- key-admin-">Администратор проекта</div>
									</xsl:when>
									<xsl:when test="$user_link/@status = 'moderator'">
										<div class="key- key-moderator-">Модератор проекта</div>
									</xsl:when>
								</xsl:choose>
							</td>
							<td class="col-user-name-2-">
								<div class="title-">
									<a href="{$prefix}/users/{@login}/">
										<xsl:choose>
											<xsl:when test="$current_project/@use_game_name = 1">
												<xsl:value-of select="@full_name"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="@login"/>
											</xsl:otherwise>
										</xsl:choose>
									</a>
								</div>
							</td>
						</xsl:for-each>
						<xsl:if test="$current_project/@marks_are_on = 1">
							<td class="col-digits-">
								<span class="with-hint">
									<xsl:call-template name="draw_mark_count_hint">
										<xsl:with-param name="position" select="0"/>
									</xsl:call-template>
								</span>
							</td>
						</xsl:if>
						<td class="col-links-">
							<a href="{$module_url}{/root/user_short[@id = current()/@id]/@login}/">профиль</a>
							<xsl:if test="$current_project/@vector_is_on = 1">
								<br/>
								<a href="{$module_url}{/root/user_short[@id = current()/@id]/@login}/vector/">
									<xsl:choose>
										<xsl:when test="/root/vectors/vector[@user_id = current()/@id]">
											<xsl:text>вектор</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="class">disabled</xsl:attribute>
											<xsl:attribute name="title">Не пройден</xsl:attribute>
											<xsl:text>вектор</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</a>
							</xsl:if>
						</td>
						<td class="col-date-">
							<xsl:call-template name="get_full_date">
								<xsl:with-param name="datetime" select="@add_time"/>
							</xsl:call-template>
						</td>
					</tr>
				</xsl:for-each>
			</table>
			<xsl:apply-templates select="pages[page]"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Участники &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
