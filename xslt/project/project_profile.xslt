<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/project_head2.inc.xslt"/>
	<xsl:include href="include/project_competence_sets.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'profile'"/>
	<xsl:variable name="project_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'profile/')"/>
	<xsl:template match="project_profile">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Профиль проекта</h2>
							<div class="content">
								<xsl:call-template name="_draw_main_info"/>
								<xsl:call-template name="_draw_descr"/>
								<xsl:if test="$current_project/@marks_are_on = 1">
									<xsl:call-template name="_draw_competence_sets"/>
								</xsl:if>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_main_info">
		<h3 class="black">
			<xsl:value-of select="$current_project/@title"/>
		</h3>
		<xsl:if test="$current_project/@main_url_human_calc != ''">
			<div class="kv-info">
				<span class="hl">Сайт проекта: </span>
				<a target="_blank" class="extlink" href="{$current_project/@main_url}">
					<xsl:value-of select="$current_project/@main_url_human_calc"/>
				</a>
			</div>
		</xsl:if>
		<div class="kv-info">
			<xsl:choose>
				<xsl:when test="$current_project/@finish_time != ''">
					<span class="hl">Дата проведения: </span>
					<xsl:call-template name="get_full_date">
						<xsl:with-param name="datetime" select="$current_project/@start_time"/>
					</xsl:call-template>
					<xsl:if test="substring($current_project/@start_time, 1, 10) != substring($current_project/@finish_time, 1, 10)">
						<xsl:text> &#8212; </xsl:text>
						<xsl:call-template name="get_full_date">
							<xsl:with-param name="datetime" select="$current_project/@finish_time"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<span class="hl">Дата начала: </span>
					<xsl:call-template name="get_full_date">
						<xsl:with-param name="datetime" select="$current_project/@start_time"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text> (</xsl:text>
			<xsl:call-template name="draw_project_state">
				<xsl:with-param name="state" select="$current_project/@state_calc"/>
			</xsl:call-template>
			<xsl:text>).</xsl:text>
		</div>
		<div class="kv-info">
			<span class="hl">Участников: </span>
			<xsl:value-of select="$current_project/@user_count_calc"/>
		</div>
		<xsl:if test="$current_project/@marks_are_on = 1">
			<div class="kv-info">
				<span class="hl">Прецедентов: </span>
				<xsl:value-of select="$current_project/@precedent_count_calc"/>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="_draw_descr">
		<xsl:if test="descr/div/*">
			<h3 class="down">Описание проекта</h3>
			<xsl:copy-of select="descr/div"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="_draw_competence_sets">
		<xsl:for-each select="$current_project">
			<xsl:if test="@vector_competence_set != '' or @marks_competence_set != '' or @total_mark_count_calc != 0">
				<h3 class="down">Оценки и наборы компетенций</h3>
				<div class="kv-info">
					<span class="hl">Всего в проекте поставлено </span>
					<xsl:call-template name="draw_mark_count_details"/>
				</div>
				<div class="kv-info">
					<ul>
						<xsl:call-template name="draw_project_competence_sets">
							<xsl:with-param name="project_id" select="@id"/>
						</xsl:call-template>
					</ul>
				</div>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/projects/">Проекты</a>
		</div>
		<div class="level2- selected-">
			<xsl:choose>
				<xsl:when test="$project_section_main_page">
					<xsl:value-of select="$current_project/@title"/>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$module_url}">
						<xsl:value-of select="$current_project/@title"/>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
