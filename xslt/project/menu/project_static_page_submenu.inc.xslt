<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_project_static_page_submenu">
		<xsl:variable name="current_page" select="."/>
		<xsl:variable name="current_item" select="/root/project_menu//item[@id = $current_page/@id and @show_in_menu = 1 and @type = 'static_page']"/>
		<xsl:variable name="current_branch_root" select="$current_item/ancestor-or-self::item[parent::project_menu and @show_in_menu = 1 and @type = 'static_page']"/>
		<xsl:if test="$current_branch_root/item[@show_in_menu = 1 and @type = 'static_page']">
			<div class="widget-wrap">
				<div class="widget widget-categories">
					<div class="header- header-light-">
						<table cellspacing="0">
							<tr>
								<td>
									<h2 class="text-">Навигация</h2>
								</td>
							</tr>
						</table>
					</div>
					<div class="items-">
						<xsl:for-each select="$current_branch_root | $current_branch_root/item[@show_in_menu = 1 and @type = 'static_page']">
							<div>
								<xsl:attribute name="class">
									<xsl:text>item-outer- </xsl:text>
									<xsl:if test="@id = $current_page/@id">item-outer-selected-</xsl:if>
								</xsl:attribute>
								<div class="item- level-{@level}-">
									<xsl:choose>
										<xsl:when test="@id = $current_page/@id">
											<strong>
												<xsl:value-of select="@title"/>
											</strong>
										</xsl:when>
										<xsl:otherwise>
											<a href="{$current_project/@url}{@static_page_name}/">
												<xsl:value-of select="@title"/>
											</a>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
						</xsl:for-each>
					</div>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
