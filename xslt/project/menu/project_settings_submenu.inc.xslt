<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_project_settings_submenu">
		<div class="head2 head2-admin-">
			<table class="head2-table-">
				<tr>
					<td class="head2-table-right- no-avatar-">
						<div class="menu-">
							<div>
								<xsl:attribute name="class">
									<xsl:text>menu-item- </xsl:text>
									<xsl:if test="/root/project_settings">menu-item-selected- </xsl:if>
								</xsl:attribute>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="/root/project_settings">Редактирование</xsl:when>
												<xsl:otherwise>
													<a href="{$current_project/@url}settings/">Редактирование</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
							<div class="menu-sep-">|</div>
							<div>
								<xsl:attribute name="class">
									<xsl:text>menu-item- </xsl:text>
									<xsl:if test="/root/project_unjoin">menu-item-selected- </xsl:if>
								</xsl:attribute>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="/root/project_unjoin">Отсоединиться</xsl:when>
												<xsl:otherwise>
													<a href="{$current_project/@url}unjoin/">Отсоединиться</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
