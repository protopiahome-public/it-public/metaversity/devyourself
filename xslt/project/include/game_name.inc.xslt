<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_game_name_field">
		<xsl:param name="game_name" select="$user/@visible_name"/>
		<xsl:param name="no_fieldset" select="false()"/>
		<xsl:choose>
			<xsl:when test="$no_fieldset">
				<xsl:call-template name="_draw_game_name_field_inner">
					<xsl:with-param name="game_name" select="$game_name"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<fieldset class="fake-">
					<xsl:call-template name="_draw_game_name_field_inner">
						<xsl:with-param name="game_name" select="$game_name"/>
					</xsl:call-template>
				</fieldset>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="_draw_game_name_field_inner">
		<xsl:param name="game_name"/>
		<div class="field">
			<label class="title-" for="f-game_name">Игровое имя:</label>
			<div class="input-">
				<input id="jq-game-name-input" name="game_name" type="text" class="input-text" value="{$game_name}" maxlength="100">
					<xsl:if test="$pass_info/vars/var[@name = 'game_name']">
						<xsl:attribute name="value">
							<xsl:value-of select="$pass_info/vars/var[@name = 'game_name']"/>
						</xsl:attribute>
					</xsl:if>
				</input>
			</div>
			<div class="comment-">Это имя будет появляться в проекте рядом в вашими записями, комментариями и проч.</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
