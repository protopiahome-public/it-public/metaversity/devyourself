<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_project_competence_sets">
		<xsl:param name="project_id"/>
		<xsl:variable name="vector_competence_set_id" select="@vector_competence_set_id"/>
		<xsl:variable name="marks_competence_set_id" select="@marks_competence_set_id"/>
		<xsl:choose>
			<xsl:when test="@vector_competence_set_id = @marks_competence_set_id and @marks_competence_set_id != ''">
				<xsl:call-template name="draw_project_competence_set">
					<xsl:with-param name="project_id" select="$project_id"/>
					<xsl:with-param name="competence_set_id" select="@vector_competence_set_id"/>
					<xsl:with-param name="show_text" select="true()"/>
					<xsl:with-param name="caption" select="'Вектор и оценивание'"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="@vector_competence_set_id != '' or @marks_competence_set_id != ''">
				<xsl:if test="@vector_competence_set_id != ''">
					<xsl:call-template name="draw_project_competence_set">
						<xsl:with-param name="project_id" select="$project_id"/>
						<xsl:with-param name="competence_set_id" select="@vector_competence_set_id"/>
						<xsl:with-param name="show_text" select="true()"/>
						<xsl:with-param name="caption" select="'Вектор'"/>
					</xsl:call-template>
				</xsl:if>
				<xsl:if test="@marks_competence_set_id != ''">
					<xsl:call-template name="draw_project_competence_set">
						<xsl:with-param name="project_id" select="$project_id"/>
						<xsl:with-param name="competence_set_id" select="@marks_competence_set_id"/>
						<xsl:with-param name="show_text" select="not(/root/project_competence_set_links/project[@id = $project_id]/competence_set[@id = current()/@vector_competence_set_id])"/>
						<xsl:with-param name="caption" select="'Оценивание'"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:when>
		</xsl:choose>
		<xsl:for-each select="/root/project_competence_set_links/project[@id = current()/@id]/competence_set[@id != current()/@vector_competence_set_id and @id != current()/@marks_competence_set_id]">
			<xsl:call-template name="draw_project_competence_set">
				<xsl:with-param name="project_id" select="$project_id"/>
				<xsl:with-param name="competence_set_id" select="@id"/>
				<xsl:with-param name="show_text" select="not(/root/project_competence_set_links/project[@id = $project_id]/competence_set[@id = $vector_competence_set_id]) and not(/root/project_competence_set_links/project[@id = $project_id]/competence_set[@id = $marks_competence_set_id]) and position() = 1"/>
				<xsl:with-param name="caption" select="'Оценивание (доп. набор)'"/>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="draw_project_competence_set">
		<xsl:param name="project_id"/>
		<xsl:param name="competence_set_id"/>
		<xsl:param name="show_text"/>
		<xsl:param name="caption"/>
		<li>
			<span class="hl">
				<xsl:value-of select="$caption"/>
				<xsl:text>: </xsl:text>
			</span>
			<xsl:variable name="project_competence_set_link" select="/root/project_competence_set_links/project[@id = $project_id]/competence_set[@id = $competence_set_id]"/>
			<xsl:choose>
				<xsl:when test="$project_competence_set_link">
					<xsl:for-each select="$project_competence_set_link">
						<xsl:value-of select="@title"/>
						<xsl:text> (</xsl:text>
						<span class="with-hint">
							<xsl:call-template name="draw_mark_count_hint">
								<xsl:with-param name="position">
									<xsl:choose>
										<xsl:when test="$show_text">1</xsl:when>
										<xsl:otherwise>0</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
						</span>
						<xsl:text>)</xsl:text>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="/root/competence_set_short[@id = $competence_set_id]/@title"/>
				</xsl:otherwise>
			</xsl:choose>
		</li>
	</xsl:template>
</xsl:stylesheet>
