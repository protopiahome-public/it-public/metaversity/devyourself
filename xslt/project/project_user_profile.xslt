<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/project_head2.inc.xslt"/>
	<xsl:include href="head2/project_user_head2.inc.xslt"/>
	<xsl:include href="../users/include/user_profile.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'users'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'profile'"/>
	<xsl:variable name="user_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="$current_user_base_url"/>
	<xsl:template match="project_user_profile">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_project_user_head2"/>
							<h2>Проектный профиль</h2>
							<div class="content">
								<xsl:call-template name="_draw_main_info"/>
								<xsl:call-template name="draw_user_profile_contacts"/>
								<xsl:call-template name="_draw_vector"/>
								<xsl:call-template name="_draw_results"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_main_info">
		<h3 class="black">
			<xsl:value-of select="$current_user/@any_name"/>
		</h3>
		<div style="font-size: 15px; padding-bottom: 10px;">
			<span class="hl">Основной профиль: </span>
			<a href="{$prefix}/users/{$current_user/@login}/">
				<xsl:choose>
					<xsl:when test="$current_project/@use_game_name = 1">
						<xsl:value-of select="$current_user/@full_name"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$current_user/@login"/>
					</xsl:otherwise>
				</xsl:choose>
			</a>
		</div>
		<xsl:if test="$current_project/@show_user_numbers = 1">
			<div style="font-size: 15px; padding-bottom: 10px;">
				<span class="hl">Уникальный номер в проекте: </span>
				<xsl:value-of select="@number"/>
			</div>
		</xsl:if>
		<xsl:if test="../university_short[@id = current()/@university_id]">
			<div class="kv-info">
				<span class="hl">Вуз: </span>
				<xsl:value-of select="../university_short[@id = current()/@university_id]/@title_full"/>
				<xsl:text> (</xsl:text>
				<xsl:value-of select="../university_short[@id = current()/@university_id]/@title"/>
				<xsl:text>)</xsl:text>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="_draw_vector">
		<xsl:if test="$current_project/@vector_is_on = 1">
			<h3 class="down">
				<a href="{$current_user_base_url}vector/">Вектор</a>
			</h3>
			<xsl:choose>
				<xsl:when test="/root/vector/@id">
					<xsl:for-each select="/root/vector[1]">
						<p>
							<xsl:text>&#171;Я в будущем&#187; &#8212; выбрано </xsl:text>
							<xsl:value-of select="@future_professiogram_count"/>
							<xsl:call-template name="count_case">
								<xsl:with-param name="number" select="@future_professiogram_count"/>
								<xsl:with-param name="word_ns" select="' профессия'"/>
								<xsl:with-param name="word_gs" select="' профессии'"/>
								<xsl:with-param name="word_ap" select="' профессий'"/>
							</xsl:call-template>
							<xsl:choose>
								<xsl:when test="@future_professiogram_count &gt; 0">:</xsl:when>
								<xsl:otherwise>.</xsl:otherwise>
							</xsl:choose>
						</p>
						<xsl:if test="@future_professiogram_count &gt; 0">
							<ul>
								<xsl:for-each select="future/professiogram">
									<li>
										<xsl:value-of select="@title"/>
									</li>
								</xsl:for-each>
							</ul>
						</xsl:if>
						<p class="note">
							<xsl:text>Самооценка заполнена для </xsl:text>
							<xsl:value-of select="self/@mark_count"/>
							<xsl:call-template name="count_case">
								<xsl:with-param name="number" select="self/@mark_count"/>
								<xsl:with-param name="word_ns" select="' компетенции, '"/>
								<xsl:with-param name="word_gs" select="' компетенций, '"/>
								<xsl:with-param name="word_ap" select="' компетенций, '"/>
							</xsl:call-template>
							<xsl:value-of select="@focus_competence_count"/>
							<xsl:call-template name="count_case">
								<xsl:with-param name="number" select="@focus_competence_count"/>
								<xsl:with-param name="word_ns" select="' компетенция'"/>
								<xsl:with-param name="word_gs" select="' компетенции'"/>
								<xsl:with-param name="word_ap" select="' компетенций'"/>
							</xsl:call-template>
							<xsl:text> в фокусе.</xsl:text>
						</p>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<p>Не пройден.</p>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template name="_draw_results">
		<h3 class="down">
			<!--<a href="{$current_user_base_url}results/">Результаты</a>-->
			Результаты
		</h3>
		<p>
			<xsl:text>За проект </xsl:text>
			<xsl:call-template name="count_case">
				<xsl:with-param name="number" select="@total_mark_count_calc"/>
				<xsl:with-param name="word_ns" select="'получена '"/>
				<xsl:with-param name="word_gs" select="'получены '"/>
				<xsl:with-param name="word_ap" select="'получено '"/>
			</xsl:call-template>
			<xsl:call-template name="draw_mark_count_full"/>
			<xsl:text>.</xsl:text>
		</p>
		<xsl:if test="../project_user_profile_competence_sets/competence_set">
			<p>Наборы компетенций:</p>
			<ul>
				<xsl:for-each select="../project_user_profile_competence_sets/competence_set">
					<li>
						<xsl:value-of select="@title"/>
						<xsl:text> &#8212; </xsl:text>
						<xsl:call-template name="draw_mark_count_full">
							<xsl:with-param name="position" select="position()"/>
						</xsl:call-template>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$user_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
