<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/project_head2.inc.xslt"/>
	<xsl:include href="../users/include/user_profile.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'settings'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="settings_section" select="'edit'"/>
	<xsl:variable name="settings_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'settings/')"/>
	<xsl:template match="error_403">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Настройка проектного профиля</h2>
							<div class="content">
								<div class="box box-stop">
									<h3>Невозможно получить доступ</h3>
									<p>Вы пытаетесь получить доступ к странице редактирования проектного профиля.</p>
									<xsl:choose>
										<xsl:when test="$user/@id">
											<p>Для доступа необходимо сначала стать участником проекта. Для этого необходимо нажать на кнопку «присоединиться» в верхнем меню.</p>
										</xsl:when>
										<xsl:otherwise>
											<p>Для доступа необходимо <a href="{$login_url}">войти</a> или <a href="{$reg_url}">зарегистрироваться</a>.</p>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Редактирование проектного профиля &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
