<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/project_head2.inc.xslt"/>
	<xsl:include href="../block_sets/include/block_set_show.inc.xslt"/>
	<xsl:include href="menu/project_static_page_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'profile'"/>
	<xsl:variable name="project_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="/root/static_page_full/@url"/>
	<xsl:template match="static_page_full">
		<xsl:variable name="block_set" select="/root/block_set[@id = current()/@block_set_id]"/>
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3>
									<xsl:value-of select="@title"/>
									<xsl:if test="$project_access/@has_admin_rights = 1">
										<span class="icons- icons-small">
											<a class="edit- gray-" href="{$current_project/@url}admin/static-pages/{@static_page_name}/" title="Редактировать"/>
										</span>
									</xsl:if>
								</h3>
								<xsl:if test="$block_set">
									<div class="content-">
										<xsl:for-each select="$block_set">
											<xsl:call-template name="draw_block_set_show">
												<xsl:with-param name="object_url" select="$module_url"/>
											</xsl:call-template>
										</xsl:for-each>
									</div>
								</xsl:if>
								<xsl:if test="false() and not($block_set/block)">
									<xsl:call-template name="draw_project_static_page_submenu"/>
								</xsl:if>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_project_static_page_submenu"/>
							<xsl:if test="$block_set/block">
							</xsl:if>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="static_page_full/@title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
