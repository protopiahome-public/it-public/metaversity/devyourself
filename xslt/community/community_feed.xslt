<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="menu/feed_submenu.inc.xslt"/>
	<xsl:include href="menu/community_feed_filter_submenu.inc.xslt"/>
	<xsl:include href="include/community.inc.xslt"/>
	<xsl:include href="include/community_sections.inc.xslt"/>
	<xsl:include href="include/post.inc.xslt"/>
	<xsl:include href="../block_sets/include/block_set_show.inc.xslt"/>
	<xsl:variable name="feed_url" select="$current_community/@url"/>
	<xsl:variable name="feed_filter" select="/root/community_feed/@filter"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'feed'"/>
	<xsl:variable name="community_section_main_page" select="/root/community_feed/pages/@current_page = 1 and not(/root/community_feed/@section_id) and not($get_vars) and ($feed_filter = 'all' or $current_community/@child_community_count_calc = 0)"/>
	<xsl:variable name="module_url">
		<xsl:choose>
			<xsl:when test="/root/community_feed/@section_id">
				<xsl:value-of select="/root/section_full[@id = /root/community_feed/@section_id]/@url"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($current_community/@url, 'feed/')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="community_feed">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<xsl:call-template name="draw_feed_submenu"/>
								<xsl:call-template name="draw_community_feed_filter_submenu"/>
								<h3 class="black">
									<xsl:choose>
										<xsl:when test="@section_id and pages/@current_page = 1">
											<xsl:value-of select="/root/section_full[@id = current()/@section_id]/@title"/>
										</xsl:when>
										<xsl:when test="@section_id and pages/@current_page &gt; 1">
											<a href="{/root/section_full[@id = current()/@section_id]/@url}">
												<xsl:value-of select="/root/section_full[@id = current()/@section_id]/@title"/>
											</a>
										</xsl:when>
										<xsl:when test="pages/@current_page = 1">Все записи</xsl:when>
										<xsl:otherwise>
											<a href="{$feed_url}feed/">Все записи</a>
										</xsl:otherwise>
									</xsl:choose>
								</h3>
								<xsl:if test="@section_id">
									<xsl:if test="/root/section_full[@id = current()/@section_id]/descr/div/*">
										<div class="box">
											<xsl:copy-of select="/root/section_full[@id = current()/@section_id]/descr/div"/>
										</div>
									</xsl:if>
								</xsl:if>
								<xsl:call-template name="_draw_list"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_community_post_add_button"/>
							<xsl:call-template name="draw_community_sections"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<xsl:if test="not(post)">
			<p>
				<xsl:text>Ни одной записи не найдено. </xsl:text>
			</p>
		</xsl:if>
		<xsl:for-each select="post">
			<xsl:variable name="context" select="."/>
			<xsl:for-each select="/root/post_full[@id = current()/@id]">
				<xsl:call-template name="draw_post">
					<xsl:with-param name="list" select="true()"/>
					<xsl:with-param name="show_community_link" select="$feed_filter = 'all' or $feed_filter = 'sub'"/>
					<xsl:with-param name="show_link" select="true()"/>
					<xsl:with-param name="show_edit_link" select="true()"/>
					<xsl:with-param name="is_admin" select="$community_access/@has_admin_rights = 1"/>
				</xsl:call-template>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:apply-templates select="pages[page]"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:choose>
			<xsl:when test="/root/community_feed/@section_id">
				<xsl:value-of select="/root/section_full[@id = /root/community_feed/@section_id]/@title"/>
			</xsl:when>
			<xsl:otherwise>Все записи</xsl:otherwise>
		</xsl:choose>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_community/@title"/>
	</xsl:template>
</xsl:stylesheet>
