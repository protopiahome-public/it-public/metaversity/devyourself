<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="include/community.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'subcommunities'"/>
	<xsl:variable name="community_section_main_page" select="false()"/>
	<xsl:variable name="communities_url" select="concat($current_project/@url, 'co/')"/>
	<xsl:variable name="subcommunities_url" select="concat($current_community/@url, 'subcommunities/')"/>
	<xsl:variable name="module_url" select="concat($current_community/@url, 'subcommunities/add/')"/>
	<xsl:template match="community_child_community_add">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3 class="pre black">
									<a href="{$subcommunities_url}">Подсообщества</a>
								</h3>
								<h4>Создание дочернего сообщества</h4>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/community_child_community_add/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$communities_url}/%NAME%/"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<input type="hidden" name="community_id" value="{$current_community/@id}"/>
			<xsl:call-template name="draw_dt_edit"/>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Создание дочернего сообщества &#8212; </xsl:text>
		<xsl:value-of select="$current_community/@title"/>
	</xsl:template>
</xsl:stylesheet>
