<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="../_site/include/access.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'subcommunities'"/>
	<xsl:variable name="community_section_main_page" select="false()"/>
	<xsl:variable name="communities_url" select="concat($current_project/@url, 'co/')"/>
	<xsl:variable name="subcommunities_url" select="concat($current_community/@url, 'subcommunities/')"/>
	<xsl:variable name="module_url" select="concat($current_community/@url, 'subcommunities/add/')"/>
	<xsl:template match="error_403">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3 class="pre black">
									<a href="{$subcommunities_url}">Подсообщества</a>
								</h3>
								<h4>Создание дочернего сообщества</h4>
								<div class="box box-stop">
									<h3>Создание сообществ ограничено</h3>
									<xsl:choose>
										<xsl:when test="$current_community/@allow_child_communities = 'nobody'">
											<p>Создание дочерних сообществ отключено администраторами.</p>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="draw_access">
												<xsl:with-param name="status" select="$project_access/@access_communities_add"/>
												<xsl:with-param name="entity_genitiv" select="'проекта'"/>
												<xsl:with-param name="join_premoderation" select="$project_access/@join_premoderation = 1"/>
												<xsl:with-param name="special_moderator_entity_genitiv" select="'сообществ'"/>
											</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Нет доступа &#8212; </xsl:text>
		<xsl:value-of select="$current_community/@title"/>
	</xsl:template>
</xsl:stylesheet>
