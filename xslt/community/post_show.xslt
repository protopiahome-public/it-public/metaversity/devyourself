<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="include/community.inc.xslt"/>
	<xsl:include href="include/community_sections.inc.xslt"/>
	<xsl:include href="include/post.inc.xslt"/>
	<xsl:include href="../block_sets/include/block_set_show.inc.xslt"/>
	<xsl:include href="../comments/include/comments.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'post'"/>
	<xsl:variable name="community_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($current_community/@url, 'post-', /root/post_full/@id, '/')"/>
	<xsl:variable name="base_block_set_file_url" select="$module_url"/>
	<xsl:template match="post_full">
		<xsl:variable name="post_access" select="/root/post_access[@post_id = current()/@id]"/>
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<xsl:call-template name="draw_post">
									<xsl:with-param name="show_edit_link" select="true()"/>
									<xsl:with-param name="is_admin" select="$community_access/@has_admin_rights = 1"/>
								</xsl:call-template>
								<xsl:for-each select="/root/comments">
									<xsl:call-template name="draw_comments">
										<xsl:with-param name="can_moderate" select="$community_access/@has_admin_rights = 1"/>
										<xsl:with-param name="can_comment" select="$post_access/@can_comment = 1"/>
										<xsl:with-param name="can_comment_html">
											<div class="box" style="margin-top: 8px;">
												<xsl:call-template name="draw_access">
													<xsl:with-param name="for" select="'Для комментирования записи'"/>
													<xsl:with-param name="status" select="$post_access/@access_comment"/>
													<xsl:with-param name="entity_genitiv" select="'сообщества'"/>
													<xsl:with-param name="join_premoderation" select="$community_access/@join_premoderation = 1"/>
												</xsl:call-template>
											</div>
										</xsl:with-param>
										<xsl:with-param name="title_akk" select="'запись'"/>
									</xsl:call-template>
								</xsl:for-each>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_community_post_add_button"/>
							<xsl:call-template name="draw_community_sections">
								<xsl:with-param name="current_community_section" select="@section_id"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			var comment_add_ctrl = 'post_comment_add';
			var comment_delete_ctrl = 'post_comment_delete';
			var comments_subscribe_ctrl = 'post_comments_subscribe';
			var comments_post_vars = {
				community_id : '<xsl:value-of select="$current_community/@id"/>',
				project_id : '<xsl:value-of select="$current_project/@id"/>'
			};
			var can_moderate_comments = <xsl:value-of select="$community_access/@has_admin_rights"/>;
		</script>
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.scrollTo.js"/>
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/comments.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="post_full/@title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_community/@title"/>
	</xsl:template>
</xsl:stylesheet>
