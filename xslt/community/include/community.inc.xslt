<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="../../_site/include/access.inc.xslt"/>
	<xsl:template name="draw_community_post_add_button">
		<div class="widget-wrap">
			<div class="widget widget-post-add">
				<div class="content-">
					<xsl:choose>
						<xsl:when test="
							not(@section_id &gt; 0) and /root/sections_accesses/@any_section_can_add_post = 1 
							or @section_id &gt; 0 and /root/sections_accesses/section_access[@section_id = current()/@section_id]/@can_add_post = 1
							or not(/root/sections_accesses/@any_section_add_post_access_status)
							">
							<a class="button-add">
								<xsl:attribute name="href">
									<xsl:value-of select="concat($current_community/@url, 'add/')"/>
									<xsl:if test="@section_id &gt; 0">
										<xsl:value-of select="concat('?section=', /root/section_full[@id = current()/@section_id]/@name)"/>
									</xsl:if>
								</xsl:attribute>
								<xsl:text>Добавить запись</xsl:text>
							</a>
						</xsl:when>
						<xsl:otherwise>
							<span class="button-add button-add-disabled">Добавить запись</span>
							<xsl:variable name="add_post_access">
								<xsl:choose>
									<xsl:when test="@section_id &gt; 0">
										<xsl:value-of select="/root/sections_accesses/section_access[@section_id = current()/@section_id]/@access_add_post"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="/root/sections_accesses/@any_section_add_post_access_status"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:call-template name="draw_access">
								<xsl:with-param name="status" select="$add_post_access"/>
								<xsl:with-param name="entity_genitiv" select="'сообщества'"/>
								<xsl:with-param name="join_premoderation" select="$community_access/@join_premoderation = 1"/>
								<xsl:with-param name="for">
									<xsl:choose>
										<xsl:when test="@section_id &gt; 0">Чтобы написать в текущий раздел,</xsl:when>
										<xsl:otherwise>Для добавления записи</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
								<xsl:with-param name="draw_instruction" select="false()"/>
							</xsl:call-template>
							<xsl:if test="@section_id &gt; 0">
								<xsl:if test="/root/sections_accesses/@any_section_can_add_post = 1">
									<p>
										<a href="{$current_community/@url}add/">
											<xsl:text>Написать в другой раздел</xsl:text>
										</a>
									</p>
								</xsl:if>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
