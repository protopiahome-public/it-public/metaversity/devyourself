<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_post">
		<xsl:param name="list" select="false()"/>
		<xsl:param name="show_link" select="false()"/>
		<xsl:param name="show_edit_link" select="false()"/>
		<xsl:param name="show_community_link" select="false()"/>
		<xsl:param name="is_admin" select="false()"/>
		<xsl:variable name="post_url" select="concat(/root/community_short[@id = current()/@community_id]/@url, 'post-', @id, '/')"/>
		<xsl:variable name="post_access" select="/root/post_access[@post_id = current()/@id]"/>
		<div class="post">
			<xsl:for-each select="/root/user_short[@id = current()/@author_user_id][1]">
				<table class="author-">
					<tr>
						<td class="text-">
							<a href="{$current_project/@url}users/{@login}/">
								<xsl:value-of select="@visible_name"/>
							</a>
							<b>
								<xsl:value-of select="@login"/>
							</b>
						</td>
						<td class="avatar-">
							<span>
								<a href="{$current_project/@url}users/{@login}/" title="">
									<img src="{photo_mid/@url}" width="{photo_mid/@width}" height="{photo_mid/@height}" alt="{@login}" title=""/>
								</a>
							</span>
						</td>
					</tr>
				</table>
			</xsl:for-each>
			<h3>
				<xsl:attribute name="class">
					<xsl:if test="$list">down </xsl:if>
					<xsl:if test="not($show_link)">black</xsl:if>
				</xsl:attribute>
				<xsl:choose>
					<xsl:when test="$show_link">
						<a href="{$post_url}">
							<xsl:value-of select="@title"/>
						</a>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@title"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="$show_edit_link and $post_access/@can_edit = 1">
					<span class="icons- icons-small">
						<a class="edit- gray-" href="{$post_url}edit/" title="Редактировать"/>
					</span>
				</xsl:if>
			</h3>
			<xsl:if test="$show_community_link or @section_id &gt; 1">
				<!-- and if section is set -->
				<div class="section-">
					<span class="caption-">Из: </span>
					<xsl:if test="$show_community_link">
						<span class="link-">
							<a href="{/root/community_short[@id = current()/@community_id]/@url}">
								<xsl:value-of select="/root/community_short[@id = current()/@community_id]/@title"/>
							</a>
						</span>
					</xsl:if>
					<xsl:if test="$show_community_link and @section_id &gt; 1">
						<!-- and if section is set -->
						<span class="section-sep">&#160;</span>
					</xsl:if>
					<!-- if section is set -->
					<xsl:if test="@section_id &gt; 1">
						<span class="link-">
							<a href="{/root/community_short[@id = current()/@community_id]/@url}{@section_name}/">
								<xsl:value-of select="@section_title"/>
							</a>
						</span>
					</xsl:if>
				</div>
			</xsl:if>
			<xsl:if test="/root/block_set[@id = current()/@block_set_id]">
				<div class="content-">
					<xsl:for-each select="/root/block_set[@id = current()/@block_set_id]">
						<xsl:call-template name="draw_block_set_show">
							<xsl:with-param name="object_url" select="$post_url"/>
							<xsl:with-param name="list" select="$list"/>
						</xsl:call-template>
					</xsl:for-each>
				</div>
			</xsl:if>
			<div class="nav-">
				<div class="block- block-first- block-comments-">
					<span class="caption-">Ответило: </span>
					<span class="count-">
						<a href="{$post_url}#comments">
							<xsl:value-of select="@comment_count_calc"/>
						</a>
					</span>
				</div>
				<div class="block- block-write-">
					<span class="link-">
						<a href="{$post_url}#add">Добавить комментарий</a>
					</span>
				</div>
				<div class="block- block-last- block-date-">
					<span class="date-">
						<xsl:call-template name="get_full_datetime">
							<xsl:with-param name="datetime" select="@add_time"/>
						</xsl:call-template>
					</span>
				</div>
			</div>
			<div class="clear"/>
			<div class="hr-"/>
		</div>
	</xsl:template>
</xsl:stylesheet>
