<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format name="number" NaN="0"/>
	<xsl:template name="draw_community_sections">
		<xsl:param name="current_community_section" select="/root/community_feed/@section_id"/>
		<xsl:variable name="sections" select="/root/community_sections"/>
		<xsl:if test="$sections/section">
			<div class="widget-wrap">
				<div class="widget widget-categories">
					<div class="header- header-light-">
						<table cellspacing="0">
							<tr>
								<td>
									<h2 class="text-">Разделы</h2>
								</td>
							</tr>
						</table>
					</div>
					<div class="items-">
						<div>
							<xsl:attribute name="class">
								<xsl:text>item-outer- </xsl:text>
								<xsl:if test="not(/root/community_widgets) and (not($current_community_section) or $current_community_section = '')">item-outer-selected-</xsl:if>
							</xsl:attribute>
							<div class="item- level-1-">
								<xsl:choose>
									<xsl:when test="(not($current_community_section) or $current_community_section = '') and /root/community_feed/pages/@current_page = 1">
										<strong>Все</strong>
									</xsl:when>
									<xsl:otherwise>
										<a href="{$current_community/@url}feed/">
											<xsl:text>Все</xsl:text>
										</a>
									</xsl:otherwise>
								</xsl:choose>
								<span class="count-">
									<xsl:value-of select="$sections/@full_count"/>
								</span>
							</div>
						</div>
						<xsl:for-each select="$sections/section">
							<xsl:variable name="section_full" select="/root/section_full[@id = current()/@id]"/>
							<div>
								<xsl:attribute name="class">
									<xsl:text>item-outer- </xsl:text>
									<xsl:if test="@id = $current_community_section">item-outer-selected-</xsl:if>
								</xsl:attribute>
								<div class="item- level-1-">
									<xsl:choose>
										<xsl:when test="@id = $current_community_section and /root/community_feed/pages/@current_page = 1">
											<strong>
												<xsl:value-of select="$section_full/@title"/>
											</strong>
										</xsl:when>
										<xsl:otherwise>
											<a href="{$section_full/@url}">
												<xsl:value-of select="$section_full/@title"/>
											</a>
										</xsl:otherwise>
									</xsl:choose>
									<span class="count-">
										<xsl:value-of select="@post_count_calc"/>
									</span>
								</div>
							</div>
						</xsl:for-each>
					</div>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
