<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../../project/head2/project_head2.inc.xslt"/>
	<xsl:variable name="current_community" select="/root/community_short[1]"/>
	<xsl:variable name="current_community_color_scheme">
		<xsl:choose>
			<xsl:when test="$current_community/@color_scheme = 'project'">
				<xsl:value-of select="$current_project/@color_scheme"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$current_community/@color_scheme"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="parent_community" select="/root/community_short[@id = $current_community/@parent_id]"/>
	<xsl:variable name="community_access" select="/root/community_access"/>
	<xsl:template name="draw_community_head2">
		<table id="community-head">
			<xsl:if test="$current_community/bg_head_main/@is_uploaded = 1">
				<xsl:attribute name="style">
					<xsl:text>background-repeat: repeat; background-image: url('</xsl:text>
					<xsl:value-of select="$current_community/bg_head_main/@url"/>
					<xsl:text>');</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<tr>
				<td rowspan="2" class="logo-">
					<div class="sep-">
						<div class="transparent- bottomline-"/>
						<xsl:for-each select="$current_community/logo_big">
							<xsl:choose>
								<xsl:when test="$community_section = 'main' and $community_section_main_page">
									<img src="{@url}" width="{@width}" height="{@height}" alt="{../@title}" title=""/>
								</xsl:when>
								<xsl:otherwise>
									<a href="{$current_community/@url}">
										<img src="{@url}" width="{@width}" height="{@height}" alt="{../@title}" title=""/>
									</a>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</div>
				</td>
				<td class="top-">
					<div class="top-wrap-">
						<xsl:if test="$community_access/@is_member = 1 or $community_access/@is_pretender = 1">
							<div class="status-">
								<div>
									<xsl:choose>
										<xsl:when test="$community_access/@status = 'pretender'">
											<xsl:attribute name="class">warning-</xsl:attribute>
											<xsl:text>Ваша заявка на вступление в сообщество</xsl:text>
											<br/>
											<xsl:text>рассматривается администраторами.</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="class">info- transparent-</xsl:attribute>
											<xsl:choose>
												<xsl:when test="$community_access/@status = 'member'">Вы участник сообщества</xsl:when>
												<xsl:when test="$community_access/@status = 'admin'">Вы администратор сообщества</xsl:when>
											</xsl:choose>
										</xsl:otherwise>
									</xsl:choose>
									<div>
										<a href="#" onclick="$('#community-join-form').submit(); return false;">отсоединиться</a>
									</div>
									<form id="community-join-form" action="{$save_prefix}/community_join/" method="post">
										<input type="hidden" name="retpath" value="{$current_community/@url}"/>
										<input type="hidden" name="project_id" value="{$current_project/@id}"/>
										<input type="hidden" name="community_id" value="{$current_community/@id}"/>
										<input type="hidden" name="join" value="0"/>
									</form>
								</div>
							</div>
						</xsl:if>
						<div>
							<xsl:attribute name="class">
								<xsl:if test="$parent_community">title-wrap-wide- transparent-</xsl:if>
							</xsl:attribute>
							<div class="title-wrap-">
								<div class="title-">
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="$parent_community">title-</xsl:when>
											<xsl:otherwise>title- transparent-</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<a href="{$current_community/@url}">
										<xsl:value-of select="$current_community/@title"/>
									</a>
								</div>
								<xsl:choose>
									<xsl:when test="$community_access/@status = 'guest'"/>
									<xsl:when test="$community_access/@is_member = 1 or $community_access/@is_pretender = 1"/>
									<xsl:otherwise>
										<div class="button-">
											<form action="{$save_prefix}/community_join/" method="post">
												<input type="hidden" name="project_id" value="{$current_project/@id}"/>
												<input type="hidden" name="community_id" value="{$current_community/@id}"/>
												<input type="hidden" name="join" value="1"/>
												<button class="button-join">+&#160;присоединиться</button>
											</form>
										</div>
									</xsl:otherwise>
								</xsl:choose>
							</div>
							<div class="descr-">
								<xsl:if test="$parent_community">
									<xsl:choose>
										<xsl:when test="$current_community/@parent_approved = 1">
											<div>
												<xsl:text>Родительское сообщество: </xsl:text>
												<a href="{$parent_community/@url}">
													<xsl:value-of select="$parent_community/@title"/>
												</a>
											</div>
										</xsl:when>
										<xsl:when test="$current_community/@parent_approved = 0 and $community_access/@has_admin_rights = 1">
											<div>
												<xsl:text>Родительское сообщество: </xsl:text>
												<a href="{$parent_community/@url}">
													<xsl:value-of select="$parent_community/@title"/>
												</a>
												<xsl:text> (пока не одобрено)</xsl:text>
											</div>
										</xsl:when>
									</xsl:choose>
								</xsl:if>
							</div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="bottom-">
					<div class="menu- transparent-">
						<div class="item-">
							<xsl:if test="$community_section = 'main'">
								<xsl:attribute name="class">item- item-selected-</xsl:attribute>
							</xsl:if>
							<div class="item-inner-">
								<xsl:choose>
									<xsl:when test="$community_section = 'main' and $community_section_main_page and not($get_vars)">
										<xsl:text>Главная</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<a href="{$current_community/@url}">Главная</a>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
						<xsl:if test="$community_section != '403'">
							<div class="item-">
								<xsl:if test="$community_section = 'feed'">
									<xsl:attribute name="class">item- item-selected-</xsl:attribute>
								</xsl:if>
								<div class="item-inner-">
									<xsl:choose>
										<xsl:when test="$community_section = 'feed' and $community_section_main_page and not($get_vars)">
											<xsl:text>Все записи</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<a href="{$current_community/@url}feed/">Все записи</a>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
							<xsl:if test="$current_community/@custom_feed_count_calc &gt; 0">
								<div class="item-">
									<xsl:if test="$community_section = 'feeds'">
										<xsl:attribute name="class">item- item-selected-</xsl:attribute>
									</xsl:if>
									<div class="item-inner-">
										<xsl:choose>
											<xsl:when test="$community_section = 'feeds' and $community_section_main_page and not($get_vars)">
												<xsl:text>Ленты</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<a href="{$current_community/@url}feeds/">Ленты</a>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
							</xsl:if>
							<div class="item-">
								<xsl:if test="$community_section = 'users'">
									<xsl:attribute name="class">item- item-selected-</xsl:attribute>
								</xsl:if>
								<div class="item-inner-">
									<xsl:choose>
										<xsl:when test="$community_section = 'users' and $community_section_main_page and not($get_vars)">
											<xsl:text>Участники</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<a href="{$current_community/@url}users/">Участники</a>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
							<xsl:if test="$current_community/@child_community_count_calc &gt; 0 or $current_community/@allow_child_communities != 'nobody'">
								<div class="item-">
									<xsl:if test="$community_section = 'subcommunities'">
										<xsl:attribute name="class">item- item-selected-</xsl:attribute>
									</xsl:if>
									<div class="item-inner-">
										<xsl:choose>
											<xsl:when test="$community_section = 'subcommunities' and $community_section_main_page and not($get_vars)">
												<xsl:text>Подсообщества</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<a href="{$current_community/@url}subcommunities/">Подсообщества</a>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
							</xsl:if>
							<xsl:if test="$community_access/@has_admin_rights = 1">
								<div class="item-">
									<xsl:if test="$community_section = 'admin'">
										<xsl:attribute name="class">item- item-selected-</xsl:attribute>
									</xsl:if>
									<div class="item-inner-">
										<xsl:attribute name="class">item-inner- item-admin-</xsl:attribute>
										<xsl:choose>
											<xsl:when test="$community_section = 'admin' and $community_section_main_page and not($get_vars)">
												<xsl:text>Админка</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<a href="{$current_community/@url}admin/">Админка</a>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
							</xsl:if>
							<xsl:if test="/root/community_widgets and $community_access/@has_admin_rights = 1">
								<div class="item-">
									<div class="item-inner- item-admin-">
										<span id="jq-widgets-edit-link" class="clickable">Виджеты</span>
									</div>
								</div>
							</xsl:if>
						</xsl:if>
					</div>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template match="/root" mode="head_left_left">
		<div class="l- project-arr-">←</div>
		<div class="l- link- link-project- link-project-left-">
			<a href="{$current_project/@url}">
				<xsl:value-of select="$current_project/@title"/>
			</a>
		</div>
	</xsl:template>
	<xsl:template match="/root" mode="head_left_right">
		<div class="l- project-sep-">|</div>
		<div class="l- link- link-project-">
			<a href="{$current_project/@url}co/">Все сообщества</a>
		</div>
		<div class="l- project-sep-">|</div>
		<div class="l- link- link-project-">
			<a href="{$current_project/@url}co/feed/">Все записи</a>
		</div>
	</xsl:template>
	<xsl:template name="draw_community_bg">
		<xsl:if test="$current_community/bg_body_main/@is_uploaded = 1">
			<xsl:attribute name="style">
				<xsl:value-of select="concat('background-image: url(', $current_community/bg_body_main/@url, '); background-position: 0 0; background-repeat: repeat;')"/>
			</xsl:attribute>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
