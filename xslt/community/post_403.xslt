<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'feed'"/>
	<xsl:variable name="community_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/403/')"/>
	<xsl:template match="error_403">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<div class="box box-stop">
									<h3>Доступ запрещён</h3>
									<p>У вас нет прав на редактирование этой записи.</p>
									<p>Чтобы её отредактировать, нужно быть её автором, либо администратором данного сообщества.</p>
									<p>Если вы являетесь автором, значит, вы утратили доступ на управление записями в текущем разделе сообщества.</p>
								</div>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Нет доступа</xsl:text>
	</xsl:template>
</xsl:stylesheet>
