<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="include/feeds.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'feeds'"/>
	<xsl:variable name="community_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_community/@url, 'feeds/')"/>
	<xsl:template match="community_custom_feeds">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3 class="black">Ленты</h3>
								<xsl:call-template name="_draw_list"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<xsl:call-template name="draw_feeds_help"/>
		<xsl:if test="not(feed)">
			<p>Лент пока нет.</p>
		</xsl:if>
		<xsl:if test="feed">
			<table class="tbl">
				<tr>
					<th>
						<span>Название ленты</span>
					</th>
				</tr>
				<xsl:for-each select="feed">
					<xsl:for-each select="/root/community_custom_feed_full[@id = current()/@id]">
						<tr>
							<td class="col-title-">
								<a href="{@url}">
									<xsl:value-of select="@title"/>
								</a>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Ленты &#8212; </xsl:text>
		<xsl:value-of select="$current_community/@title"/>
	</xsl:template>
</xsl:stylesheet>
