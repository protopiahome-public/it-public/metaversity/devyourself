<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="menu/post_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/delete.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'post'"/>
	<xsl:variable name="community_section_main_page" select="false()"/>
	<xsl:variable name="post_id" select="/root/post_delete/@id"/>
	<xsl:variable name="post_title" select="/root/post_delete/@title"/>
	<xsl:variable name="module_url" select="concat($post_url, 'delete/')"/>
	<xsl:template match="post_delete">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3>
									<a href="{$post_url}">
										<xsl:value-of select="$post_title"/>
									</a>
								</h3>
								<xsl:call-template name="draw_post_submenu"/>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/post_delete/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$current_community/@url}feed/"/>
			<input type="hidden" name="cancelpath" value="{$post_url}"/>
			<input type="hidden" name="community_id" value="{$current_community/@id}"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<xsl:call-template name="draw_delete">
				<xsl:with-param name="title_akk" select="'запись'"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Удаление записи &#8212; </xsl:text>
		<xsl:value-of select="$post_title"/>
	</xsl:template>
</xsl:stylesheet>
