<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="../users/search/users_search.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'users'"/>
	<xsl:variable name="community_section_main_page" select="/root/community_users/pages/@current_page = 1"/>
	<xsl:variable name="module_url" select="concat($current_community/@url, 'users/')"/>
	<xsl:template match="community_users">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3 class="black">Участники сообщества</h3>
								<xsl:call-template name="_draw_table"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_users_search"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_table">
		<xsl:if test="not(admin)">
			<p>Ни одного участника не найдено.</p>
		</xsl:if>
		<xsl:if test="admin">
			<p>
				<xsl:text>Отображаются пользователи </xsl:text>
				<xsl:value-of select="pages/@from_row"/>
				<xsl:text> &#8212; </xsl:text>
				<xsl:value-of select="pages/@to_row"/>
				<xsl:text> из </xsl:text>
				<xsl:value-of select="pages/@row_count"/>
				<xsl:text>.</xsl:text>
			</p>
			<table class="tbl">
				<tr>
					<xsl:if test="$current_project/@show_user_numbers = 1">
						<xsl:call-template name="draw_tbl_th">
							<xsl:with-param name="name" select="'number'"/>
							<xsl:with-param name="title" select="'#'"/>
						</xsl:call-template>
					</xsl:if>
					<th/>
					<th>Имя&#160;пользователя</th>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'post-count'"/>
						<xsl:with-param name="title" select="'Записей'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'comment-count'"/>
						<xsl:with-param name="title" select="'Комментариев'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'join-time'"/>
						<xsl:with-param name="title" select="'Присоединился'"/>
					</xsl:call-template>
				</tr>
				<xsl:for-each select="admin">
					<xsl:variable name="position" select="position()"/>
					<xsl:variable name="user_link" select="."/>
					<xsl:for-each select="/root/user_short[@id = current()/@user_id]">
						<xsl:variable name="current_user" select="."/>
						<tr>
							<xsl:if test="$current_project/@show_user_numbers = 1">
								<td class="col-digits-">
									<xsl:value-of select="@number"/>
								</td>
							</xsl:if>
							<td class="col-user-avatar-">
								<span>
									<a href="{$current_project/@url}users/{@login}/">
										<img src="{photo_big/@url}" width="{photo_big/@width}" height="{photo_big/@height}" alt="{@login}"/>
									</a>
								</span>
							</td>
							<td class="col-user-name-">
								<div class="title-">
									<a href="{$current_project/@url}users/{@login}/">
										<xsl:value-of select="@visible_name"/>
									</a>
								</div>
								<div class="second-">
									<xsl:value-of select="@login"/>
								</div>
								<xsl:choose>
									<xsl:when test="$user_link/@status = 'admin'">
										<div class="key- key-admin-">Администратор сообщества</div>
									</xsl:when>
									<xsl:when test="$user_link/@status = 'moderator'">
										<div class="key- key-moderator-">Модератор сообщества</div>
									</xsl:when>
								</xsl:choose>
							</td>
							<td class="col-digits-">
								<xsl:value-of select="$user_link/@post_count_calc"/>
							</td>
							<td class="col-digits-">
								<xsl:value-of select="$user_link/@comment_count_calc"/>
							</td>
							<td class="col-date-">
								<xsl:call-template name="get_full_date">
									<xsl:with-param name="datetime" select="$user_link/@add_time"/>
								</xsl:call-template>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
			<xsl:apply-templates select="pages[page]"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Участники сообщества &#8212; </xsl:text>
		<xsl:value-of select="$current_community/@title"/>
	</xsl:template>
</xsl:stylesheet>
