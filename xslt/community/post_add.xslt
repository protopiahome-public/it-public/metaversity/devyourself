<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="../block_sets/include/block_set_edit.inc.xslt"/>
	<xsl:include href="../_site/include/access.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'post_add'"/>
	<xsl:variable name="community_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($current_community/@url, 'add/')"/>
	<xsl:template match="post_add">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3 class="black">Добавление записи</h3>
								<xsl:choose>
									<xsl:when test="not(/root/sections_accesses/@allow_posting_without_section = 1) and not(/root/community_sections/section)">
										<div class="box box-warning">
											<h3>В сообществе не созданы разделы</h3>
											<p>Администратор сообщества отключил возможность добавлять записи в корень сообщества и при этом не создал ни одного раздела. Поэтому добавить запись сейчас нельзя.</p>
											<xsl:if test="$community_access/@has_admin_rights = 1">
												<p>Создать разделы можно в <a href="{$current_community/@url}admin/sections/">управлении разделами</a>.</p>
												<p>Разрешить писать в корень сообщества можно в <a href="{$current_community/@url}admin/access/#access_posts">управлении доступом</a> (галочка &#171;Разрешить публиковать записи в корень сообщества&#187;).</p>
											</xsl:if>
										</div>
									</xsl:when>
									<xsl:when test="/root/sections_accesses/@any_section_can_add_post = 0">
										<div class="box box-stop">
											<h3>Добавление записей ограничено</h3>
											<xsl:call-template name="draw_access">
												<xsl:with-param name="status" select="/root/sections_accesses/@any_section_add_post_access_status"/>
												<xsl:with-param name="entity_genitiv" select="'сообщества'"/>
												<xsl:with-param name="join_premoderation" select="$community_access/@join_premoderation = 1"/>
												<xsl:with-param name="for" select="'Для добавления записи'"/>
												<xsl:with-param name="draw_instruction" select="true()"/>
											</xsl:call-template>
										</div>
									</xsl:when>
									<xsl:when test="$user/@id">
										<xsl:call-template name="_draw_form"/>
									</xsl:when>
									<xsl:otherwise>
										<p>Чтобы добавить запись, сначала нужно <a href="{$reg_url}">зарегистрироваться</a> и <a href="{$login_url}">войти</a>.</p>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/post_add/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$current_community/@url}post-%ID%/"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<input type="hidden" name="community_id" value="{$current_community/@id}"/>
			<xsl:call-template name="draw_dt_edit">
				<xsl:with-param name="draw_button" select="false()"/>
			</xsl:call-template>
			<xsl:for-each select="/root/block_set">
				<xsl:call-template name="draw_block_set_edit">
					<xsl:with-param name="object_url" select="concat($current_community/@url, 'add/')"/>
				</xsl:call-template>
			</xsl:for-each>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			var block_set_post_vars = {
				project_id : '<xsl:value-of select="$current_project/@id"/>',
				community_id : '<xsl:value-of select="$current_community/@id"/>',
				post_id : '0',
				block_set_id : '0'
			};
			var block_set_ajax_url = global.ajax_prefix + 'post_block_set/';
		</script>
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/swfobject.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.uploadify.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.scrollTo.js"/>
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
		<script type="text/javascript" src="{$prefix}/js/block_set.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Добавление записи &#8212; </xsl:text>
		<xsl:value-of select="$current_community/@title"/>
	</xsl:template>
</xsl:stylesheet>
