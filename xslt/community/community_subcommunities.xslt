<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="include/community.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'subcommunities'"/>
	<xsl:variable name="community_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_community/@url, 'subcommunities/')"/>
	<xsl:template match="community_subcommunities">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3 class="black">Подсообщества</h3>
								<xsl:call-template name="_draw_table"/>
							</div>
						</td>
						<td class="right-column">

						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_table">
		<xsl:choose>
			<xsl:when test="$current_community/@allow_child_communities = 'nobody'">
				<div class="box">
					<p>Создание дочерних сообществ запрещено администраторами.</p>
				</div>
			</xsl:when>
			<xsl:when test="not($project_access/@can_add_communities = 1)">
			</xsl:when>
			<xsl:otherwise>
				<div class="ico-links">
					<a class="add-" href="{$module_url}add/">
						<span>
							<xsl:text>создать дочернее сообщество</xsl:text>
							<xsl:if test="$current_community/@allow_child_communities = 'user_premoderation' and not($community_access/@has_admin_rights = 1)"> (потребуется премодерация)</xsl:if>
						</span>
					</a>
				</div>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="not(tree//community)">
			<p>Ни одного подсообщества не найдено.</p>
		</xsl:if>
		<xsl:if test="tree//community">
			<table class="tbl">
				<tr>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="'Название'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'posts'"/>
						<xsl:with-param name="title" select="'Записей'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'comments'"/>
						<xsl:with-param name="title" select="'Комментариев'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'date'"/>
						<xsl:with-param name="title" select="'Дата&#160;добавления'"/>
					</xsl:call-template>
				</tr>
				<xsl:for-each select="tree//community">
					<xsl:variable name="community_id" select="@id"/>
					<xsl:variable name="position" select="position()"/>
					<xsl:variable name="context" select="."/>
					<xsl:for-each select="/root/community_short[@id = current()/@id]">
						<tr>
							<td class="col-subtable-">
								<table style="margin-left: {50 * ($context/@level - 1)}px;">
									<tr>
										<td class="col-logo-">
											<a href="{@url}">
												<img src="{logo_small/@url}" width="{logo_small/@width}" height="{logo_small/@height}" alt="{@title}" title=""/>
											</a>
										</td>
										<td class="col-community-title-">
											<div class="title-">
												<a href="{@url}">
													<xsl:value-of select="@title"/>
												</a>
											</div>
											<xsl:if test="@descr_short != ''">
												<div class="descr-">
													<xsl:value-of select="@descr_short"/>
												</div>
											</xsl:if>
										</td>
									</tr>
								</table>
							</td>
							<td class="col-digits-">
								<xsl:value-of select="$context/@post_count_calc"/>
							</td>
							<td class="col-digits-">
								<xsl:value-of select="$context/@comment_count_calc"/>
							</td>
							<td class="col-date-">
								<xsl:call-template name="get_full_datetime">
									<xsl:with-param name="datetime" select="@add_time"/>
								</xsl:call-template>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Подсообщества &#8212; </xsl:text>
		<xsl:value-of select="$current_community/@title"/>
	</xsl:template>
</xsl:stylesheet>
