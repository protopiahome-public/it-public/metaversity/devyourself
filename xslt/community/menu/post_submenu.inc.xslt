<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="post_url" select="concat($current_community/@url, 'post-', $post_id, '/')"/>
	<xsl:template name="draw_post_submenu">
		<div class="ico-links">
			<xsl:choose>
				<xsl:when test="/root/post_edit">
					<strong class="edit- current-">
						<span>редактировать запись</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="edit-" href="{$post_url}edit/">
						<span>редактировать запись</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/post_delete">
					<strong class="delete- current-">
						<span>удалить</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="delete-" href="{$post_url}delete/">
						<span>удалить</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
