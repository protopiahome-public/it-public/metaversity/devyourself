<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_community_feed_filter_submenu">
		<xsl:if test="not(@section_id) and $current_community/@child_community_count_calc &gt; 0">
			<div class="submenu submenu-left-">
				<span class="inner-">
					<span class="item-">
						<xsl:if test="$feed_filter = 'all'">
							<xsl:attribute name="class">selected-</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="$feed_filter = 'all' and not($get_vars) and pages/@current_page = 1">Все записи</xsl:when>
							<xsl:otherwise>
								<a href="{$module_url}">Все записи</a>
							</xsl:otherwise>
						</xsl:choose>
					</span>
					<span class="sep-"> | </span>
					<span class="item-">
						<xsl:if test="$feed_filter = 'own'">
							<xsl:attribute name="class">selected-</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="$feed_filter = 'own' and not($get_vars) and pages/@current_page = 1">Только этого сообщества</xsl:when>
							<xsl:otherwise>
								<a href="{$module_url}own/">Только этого сообщества</a>
							</xsl:otherwise>
						</xsl:choose>
					</span>
					<span class="sep-"> | </span>
					<span class="item-">
						<xsl:if test="$feed_filter = 'sub'">
							<xsl:attribute name="class">selected-</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="$feed_filter = 'sub' and not($get_vars) and pages/@current_page = 1">Только подсообществ</xsl:when>
							<xsl:otherwise>
								<a href="{$module_url}sub/">Только подсообществ</a>
							</xsl:otherwise>
						</xsl:choose>
					</span>
				</span>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
