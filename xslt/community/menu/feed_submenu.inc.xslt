<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_feed_submenu">
		<div class="feed-type-menu">
			<div>
				<xsl:attribute name="class">
					<xsl:text>block- block-first-</xsl:text>
					<xsl:if test="@output_type = 'short'"> block-selected-</xsl:if>
				</xsl:attribute>
				<xsl:choose>
					<xsl:when test="@output_type = 'short' and pages/@current_page = 1 and not($get_vars)">Сжато</xsl:when>
					<xsl:otherwise>
						<a href="{$save_prefix}/feed/?type=short">Сжато</a>
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<div>
				<xsl:attribute name="class">
					<xsl:text>block- block-last-</xsl:text>
					<xsl:if test="@output_type = 'expanded'"> block-selected-</xsl:if>
				</xsl:attribute>
				<xsl:choose>
					<xsl:when test="@output_type = 'expanded' and pages/@current_page = 1 and not($get_vars)">Развернуто</xsl:when>
					<xsl:otherwise>
						<a href="{$save_prefix}/feed/?type=expanded">Развернуто</a>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
