<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="menu/post_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="../block_sets/include/block_set_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'post'"/>
	<xsl:variable name="community_section_main_page" select="false()"/>
	<xsl:variable name="post_id" select="/root/post_edit/document/@id"/>
	<xsl:variable name="post_title" select="/root/post_edit/document/field[@name = 'title']"/>
	<xsl:variable name="module_url" select="concat($post_url, 'edit/')"/>
	<xsl:template match="post_edit">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<h3>
									<a href="{$post_url}">
										<xsl:value-of select="$post_title"/>
									</a>
								</h3>
								<xsl:call-template name="draw_post_submenu"/>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/post_edit/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<input type="hidden" name="community_id" value="{$current_community/@id}"/>
			<input type="hidden" name="retpath" value="{$post_url}"/>
			<xsl:call-template name="draw_dt_edit">
				<xsl:with-param name="draw_button" select="false()"/>
			</xsl:call-template>
			<xsl:for-each select="/root/block_set">
				<xsl:call-template name="draw_block_set_edit">
					<xsl:with-param name="object_url" select="$post_url"/>
				</xsl:call-template>
			</xsl:for-each>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			var block_set_post_vars = {
				project_id : '<xsl:value-of select="$current_project/@id"/>',
				community_id : '<xsl:value-of select="$current_community/@id"/>',
				post_id : '<xsl:value-of select="$post_id"/>',
				block_set_id : '<xsl:value-of select="/root/block_set/@id"/>'
			};
			var block_set_ajax_url = global.ajax_prefix + 'post_block_set/';
		</script>
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/swfobject.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.uploadify.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.scrollTo.js"/>
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
		<script type="text/javascript" src="{$prefix}/js/block_set.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Редактирование записи &#8212; </xsl:text>
		<xsl:value-of select="$post_title"/>
	</xsl:template>
</xsl:stylesheet>
