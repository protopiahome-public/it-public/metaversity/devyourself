<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_delete">
		<xsl:param name="title_akk" select="'сущность'"/>
		<xsl:param name="disable_saved_info" select="false()"/>
		<xsl:if test="not($disable_saved_info) and $pass_info/info[@name = 'DELETED']">
			<div class="info">
				<span>Данные удалены</span>
			</div>
		</xsl:if>
		<input type="hidden" name="id" value="{@id}"/>
		<div class="box box-red">
			<xsl:text>Подтвердите, что вы хотите удалить </xsl:text>
			<xsl:value-of select="$title_akk"/>
			<xsl:text> &#171;</xsl:text>
			<strong>
				<xsl:value-of select="@title"/>
			</strong>
			<xsl:text>&#187;. Восстановление будет невозможно.</xsl:text>
		</div>
		<button class="button button-red">Удалить</button>
		<button name="cancel" class="button button-gray button-right">Отменить</button>
	</xsl:template>
</xsl:stylesheet>
