<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_dt_edit">
		<xsl:param name="disable_saved_info" select="false()"/>
		<xsl:param name="draw_button" select="true()"/>
		<xsl:param name="button_title" select="false()"/>
		<xsl:param name="no_fieldset" select="false()"/>
		<xsl:if test="not($disable_saved_info) and $pass_info/info[@name = 'SAVED']">
			<div class="info">
				<span>Данные сохранены</span>
			</div>
		</xsl:if>
		<xsl:if test="$pass_info/error or $pass_info/vars">
			<div class="error">
				<span>При заполненнии формы допущены ошибки. Данные не были сохранены.</span>
			</div>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="@action = 'dt_add'">
				<input type="hidden" name="id" value="0"/>
			</xsl:when>
			<xsl:otherwise>
				<input type="hidden" name="id" value="{document/@id}"/>
			</xsl:otherwise>
		</xsl:choose>
		<p>Звёздочкой (<span class="star">*</span>) помечены поля, обязательные для заполнения.</p>
		<xsl:choose>
			<xsl:when test="@enable_blocks = 1">
				<xsl:for-each select="doctype/block">
					<a name="{@name}"/>
					<fieldset>
						<xsl:if test="@title != ''">
							<legend>
								<xsl:value-of select="@title"/>
							</legend>
						</xsl:if>
						<xsl:apply-templates select="field" mode="dtf"/>
					</fieldset>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="doctype/block[1]">
					<xsl:choose>
						<xsl:when test="$no_fieldset">
							<xsl:apply-templates select="field" mode="dtf"/>
						</xsl:when>
						<xsl:otherwise>
							<fieldset class="fake-">
								<xsl:apply-templates select="field" mode="dtf"/>
							</fieldset>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="$draw_button">
			<button class="button">
				<xsl:choose>
					<xsl:when test="$button_title">
						<xsl:value-of select="$button_title"/>
					</xsl:when>
					<xsl:otherwise>Сохранить</xsl:otherwise>
				</xsl:choose>
			</button>
		</xsl:if>
		<script type="text/javascript">
			var dt_deps = [];
			<xsl:for-each select="doctype/block/field/dependency">
				dt_deps.push({
					dependee: '<xsl:value-of select="parent::field/@name"/>',
					depends_from: '<xsl:value-of select="@inspected_column"/>',
					activation_value: '<xsl:value-of select="@activation_value"/>',
					actions: <xsl:value-of select="@actions"/>
				});
			</xsl:for-each>
		</script>
	</xsl:template>
</xsl:stylesheet>
