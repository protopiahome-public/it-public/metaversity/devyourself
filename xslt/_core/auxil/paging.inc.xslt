<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="pages">
		<xsl:param name="url_base" select="$module_url"/>
		<xsl:param name="url_suffix" select="$request/@query_string"/>
		<xsl:if test="page">
			<div class="paging">
				<table class="border-">
					<tr>
						<xsl:for-each select="page">
							<xsl:choose>
								<xsl:when test="@type = 'prev'">
									<td class="l-"> </td>
									<td class="c- c-arrow-">
										<xsl:choose>
											<xsl:when test="@number = 1">
												<a href="{$url_base}{$url_suffix}">&#8592;</a>
											</xsl:when>
											<xsl:otherwise>
												<a href="{$url_base}page-{@number}/{$url_suffix}">&#8592;</a>
											</xsl:otherwise>
										</xsl:choose>
									</td>
									<td class="r-"> </td>
								</xsl:when>
								<xsl:when test="@type = 'current'">
									<td class="l- l-sel-"> </td>
									<td class="c- c-sel-">
										<b>
											<xsl:value-of select="@number"/>
										</b>
									</td>
									<td class="r- r-sel-"> </td>
								</xsl:when>
								<xsl:when test="@type = 'delimeter'">
									<td class="ellipsis-">
										<span>...</span>
									</td>
								</xsl:when>
								<xsl:when test="@type = 'page'">
									<td class="l-"> </td>
									<td class="c-">
										<a href="{$url_base}page-{@number}/{$url_suffix}">
											<xsl:if test="@number = 1">
												<xsl:attribute name="href">
													<xsl:value-of select="concat($url_base, $url_suffix)"/>
												</xsl:attribute>
											</xsl:if>
											<xsl:value-of select="@number"/>
										</a>
									</td>
									<td class="r-"> </td>
								</xsl:when>
								<xsl:when test="current()[@type='next']">
									<td class="l-"> </td>
									<td class="c- c-arrow-">
										<a href="{$url_base}page-{@number}/{$url_suffix}">&#8594;</a>
									</td>
									<td class="r-"> </td>
								</xsl:when>
							</xsl:choose>
							<xsl:if test="position() != last()">
								<td class="sep-"> </td>
							</xsl:if>
						</xsl:for-each>
					</tr>
				</table>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
