<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="count_case">
		<xsl:param name="number"/>
		<!-- nominative case, singular, именительный падеж, ед. число -->
		<xsl:param name="word_ns"/>
		<!-- genitive case, singular, родительный падеж, ед. число -->
		<xsl:param name="word_gs"/>
		<!-- accusative case, plural, винительный падеж, мн. число -->
		<xsl:param name="word_ap"/>
		<xsl:if test="string-length($number) &gt; 0">
			<xsl:variable name="last" select="substring($number, string-length($number), 1)"/>
			<xsl:variable name="last_but_one" select="substring($number, string-length($number)-1, 1)"/>
			<xsl:choose>
				<xsl:when test="$last = '0'">
					<xsl:value-of select="$word_ap"/>
				</xsl:when>
				<xsl:when test="$last_but_one = '1' or $last &gt;= '5' and $last &lt;= '9'">
					<xsl:value-of select="$word_ap"/>
				</xsl:when>
				<xsl:when test="$last &gt;= '2' and $last &lt;= '4'">
					<xsl:value-of select="$word_gs"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$word_ns"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
