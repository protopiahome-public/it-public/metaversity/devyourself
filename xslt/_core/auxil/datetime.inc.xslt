<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="get_full_date">
		<xsl:param name="datetime"/>
		<xsl:variable name="day" select="substring($datetime, 9, 2)"/>
		<xsl:variable name="month" select="substring($datetime, 6, 2)"/>
		<xsl:variable name="year" select="substring($datetime, 1, 4)"/>
		<xsl:choose>
			<xsl:when test="substring($day, 1, 1) = '0'">
				<xsl:value-of select="substring($day, 2, 1)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$day"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>&#160;</xsl:text>
		<xsl:call-template name="get_month_name">
			<xsl:with-param name="month_number" select="$month"/>
			<xsl:with-param name="type" select="'with_day'"/>
		</xsl:call-template>
		<xsl:if test="substring(/root/@time, 1, 4) != $year">
			<xsl:text> </xsl:text>
			<xsl:value-of select="$year"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="get_full_datetime">
		<xsl:param name="datetime"/>
		<xsl:variable name="day" select="substring($datetime, 9, 2)"/>
		<xsl:variable name="month" select="substring($datetime, 6, 2)"/>
		<xsl:variable name="year" select="substring($datetime, 1, 4)"/>
		<xsl:variable name="hours" select="substring($datetime, 12, 2)"/>
		<xsl:variable name="minutes" select="substring($datetime, 15, 2)"/>
		<xsl:call-template name="get_full_date">
			<xsl:with-param name="datetime" select="$datetime"/>
		</xsl:call-template>
		<xsl:text> в&#160;</xsl:text>
		<xsl:if test="string-length($hours) = 1">0</xsl:if>
		<xsl:value-of select="$hours"/>
		<xsl:text>:</xsl:text>
		<xsl:if test="string-length($minutes) = 1">0</xsl:if>
		<xsl:value-of select="$minutes"/>
	</xsl:template>
	<xsl:template name="get_full_datetime_range">
		<xsl:param name="start_datetime"/>
		<xsl:param name="finish_datetime"/>
		<xsl:choose>
			<xsl:when test="$finish_datetime = ''">
				<xsl:call-template name="get_full_datetime">
					<xsl:with-param name="datetime" select="$start_datetime"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="substring($start_datetime, 1, 10) = substring($finish_datetime, 1, 10)">
				<xsl:call-template name="get_full_date">
					<xsl:with-param name="datetime" select="$start_datetime"/>
				</xsl:call-template>
				<xsl:text> с&#160;</xsl:text>
				<xsl:value-of select="substring($start_datetime, 12, 5)"/>
				<xsl:text> до&#160;</xsl:text>
				<xsl:value-of select="substring($finish_datetime, 12, 5)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="get_full_date">
					<xsl:with-param name="datetime" select="$start_datetime"/>
				</xsl:call-template>
				<xsl:text> </xsl:text>
				<xsl:value-of select="substring($start_datetime, 12, 5)"/>
				<xsl:text>&#160;&#8212; </xsl:text>
				<xsl:call-template name="get_full_date">
					<xsl:with-param name="datetime" select="$finish_datetime"/>
				</xsl:call-template>
				<xsl:text> </xsl:text>
				<xsl:value-of select="substring($finish_datetime, 12, 5)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="get_month_name">
		<xsl:param name="month_number"/>
		<xsl:param name="type" select="'month_capital'"/>
		<xsl:choose>
			<xsl:when test="$type = 'with_day'">
				<xsl:choose>
					<xsl:when test="$month_number = 1">января</xsl:when>
					<xsl:when test="$month_number = 2">февраля</xsl:when>
					<xsl:when test="$month_number = 3">марта</xsl:when>
					<xsl:when test="$month_number = 4">апреля</xsl:when>
					<xsl:when test="$month_number = 5">мая</xsl:when>
					<xsl:when test="$month_number = 6">июня</xsl:when>
					<xsl:when test="$month_number = 7">июля</xsl:when>
					<xsl:when test="$month_number = 8">августа</xsl:when>
					<xsl:when test="$month_number = 9">сентября</xsl:when>
					<xsl:when test="$month_number = 10">октября</xsl:when>
					<xsl:when test="$month_number = 11">ноября</xsl:when>
					<xsl:when test="$month_number = 12">декабря</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$type = 'month_capital'">
				<xsl:choose>
					<xsl:when test="$month_number = 1">Январь</xsl:when>
					<xsl:when test="$month_number = 2">Февраль</xsl:when>
					<xsl:when test="$month_number = 3">Март</xsl:when>
					<xsl:when test="$month_number = 4">Апрель</xsl:when>
					<xsl:when test="$month_number = 5">Май</xsl:when>
					<xsl:when test="$month_number = 6">Июнь</xsl:when>
					<xsl:when test="$month_number = 7">Июль</xsl:when>
					<xsl:when test="$month_number = 8">Август</xsl:when>
					<xsl:when test="$month_number = 9">Сентябрь</xsl:when>
					<xsl:when test="$month_number = 10">Октябрь</xsl:when>
					<xsl:when test="$month_number = 11">Ноябрь</xsl:when>
					<xsl:when test="$month_number = 12">Декабрь</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$type = 'month_small'">
				<xsl:choose>
					<xsl:when test="$month_number = 1">январь</xsl:when>
					<xsl:when test="$month_number = 2">февраль</xsl:when>
					<xsl:when test="$month_number = 3">март</xsl:when>
					<xsl:when test="$month_number = 4">апрель</xsl:when>
					<xsl:when test="$month_number = 5">май</xsl:when>
					<xsl:when test="$month_number = 6">июнь</xsl:when>
					<xsl:when test="$month_number = 7">июль</xsl:when>
					<xsl:when test="$month_number = 8">август</xsl:when>
					<xsl:when test="$month_number = 9">сентябрь</xsl:when>
					<xsl:when test="$month_number = 10">октябрь</xsl:when>
					<xsl:when test="$month_number = 11">ноябрь</xsl:when>
					<xsl:when test="$month_number = 12">декабрь</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$type = 'short_small'">
				<xsl:choose>
					<xsl:when test="$month_number = 1">янв</xsl:when>
					<xsl:when test="$month_number = 2">фев</xsl:when>
					<xsl:when test="$month_number = 3">мар</xsl:when>
					<xsl:when test="$month_number = 4">апр</xsl:when>
					<xsl:when test="$month_number = 5">май</xsl:when>
					<xsl:when test="$month_number = 6">июн</xsl:when>
					<xsl:when test="$month_number = 7">июл</xsl:when>
					<xsl:when test="$month_number = 8">авг</xsl:when>
					<xsl:when test="$month_number = 9">сен</xsl:when>
					<xsl:when test="$month_number = 10">окт</xsl:when>
					<xsl:when test="$month_number = 11">ноя</xsl:when>
					<xsl:when test="$month_number = 12">дек</xsl:when>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="get_week_day_name">
		<xsl:param name="week_day_number"/>
		<xsl:param name="type" select="'day_capital'"/>
		<xsl:choose>
			<xsl:when test="$type = 'day_capital'">
				<xsl:choose>
					<xsl:when test="$week_day_number = 1">Понедельник</xsl:when>
					<xsl:when test="$week_day_number = 2">Вторник</xsl:when>
					<xsl:when test="$week_day_number = 3">Среда</xsl:when>
					<xsl:when test="$week_day_number = 4">Четверг</xsl:when>
					<xsl:when test="$week_day_number = 5">Пятница</xsl:when>
					<xsl:when test="$week_day_number = 6">Суббота</xsl:when>
					<xsl:when test="$week_day_number = 7">Воскресенье</xsl:when>
					<xsl:when test="$week_day_number = 0">Воскресенье</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$type = 'day_small'">
				<xsl:choose>
					<xsl:when test="$week_day_number = 1">понедельник</xsl:when>
					<xsl:when test="$week_day_number = 2">вторник</xsl:when>
					<xsl:when test="$week_day_number = 3">среда</xsl:when>
					<xsl:when test="$week_day_number = 4">четверг</xsl:when>
					<xsl:when test="$week_day_number = 5">пятница</xsl:when>
					<xsl:when test="$week_day_number = 6">суббота</xsl:when>
					<xsl:when test="$week_day_number = 7">воскресенье</xsl:when>
					<xsl:when test="$week_day_number = 0">воскресенье</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$type = 'short_small'">
				<xsl:choose>
					<xsl:when test="$week_day_number = 1">пн</xsl:when>
					<xsl:when test="$week_day_number = 2">вт</xsl:when>
					<xsl:when test="$week_day_number = 3">ср</xsl:when>
					<xsl:when test="$week_day_number = 4">чт</xsl:when>
					<xsl:when test="$week_day_number = 5">пт</xsl:when>
					<xsl:when test="$week_day_number = 6">сб</xsl:when>
					<xsl:when test="$week_day_number = 7">вс</xsl:when>
					<xsl:when test="$week_day_number = 0">вс</xsl:when>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
