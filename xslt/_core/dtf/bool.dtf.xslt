<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'bool']" mode="dtf">
		<xsl:variable name="is_add" select="ancestor::doctype/../@action = 'dt_add'"/>
		<xsl:variable name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<div class="input-">
				<input id="f-{@name}" class="input-checkbox" type="checkbox" name="{@name}">
					<xsl:choose>
						<xsl:when test="$pass_info/vars/var[@name = current()/@name]">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:when>
						<xsl:when test="$pass_info/vars/var"/>
						<xsl:when test="$document_field = 1">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:when>
						<xsl:when test="$is_add and @default_value = 1">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:when>
					</xsl:choose>
				</input>
				<label for="f-{@name}" class="inline-">
					<xsl:value-of select="@title"/>
				</label>
			</div>
			<xsl:if test="@comment != ''">
				<div class="comment-">
					<xsl:value-of select="@comment"/>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
