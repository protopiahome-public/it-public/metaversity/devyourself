<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'datetime']" mode="dtf">
		<xsl:variable name="is_add" select="ancestor::doctype/../@action = 'dt_add'"/>
		<xsl:variable name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<xsl:if test="@must_be_later_than_field_name != ''">
				<xsl:attribute name="jq-later-than">
					<xsl:value-of select="@must_be_later_than_field_name"/>
				</xsl:attribute>
				<xsl:attribute name="jq-can-be-equal">
					<xsl:value-of select="@can_be_equal"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:variable name="can_be_hidden" select="@hide_by_default = 1"/>
			<xsl:variable name="pass_info_hide" select="$pass_info/vars/var[@name = concat(current()/@name, '_hide')]"/>
			<xsl:variable name="hide" select="$can_be_hidden and ($is_add or $document_field/@unset = 1 or $pass_info_hide = 1) and not($pass_info_hide = 0)"/>
			<input type="hidden" id="f-{@name}_hide" name="{@name}_hide">
				<xsl:attribute name="value">
					<xsl:choose>
						<xsl:when test="$hide">1</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</input>
			<xsl:if test="$can_be_hidden">
				<div id="field-{@name}-enabler">
					<xsl:attribute name="class">
						<xsl:text>enabler-</xsl:text>
						<xsl:if test="not($hide)"> dn</xsl:if>
					</xsl:attribute>
					<span class="clickable">
						<xsl:apply-templates mode="dtf_datetime_show_finish_time" select="."/>
					</span>
				</div>
			</xsl:if>
			<div id="field-{@name}-content">
				<xsl:attribute name="class">
					<xsl:text>content-</xsl:text>
					<xsl:if test="$hide"> dn</xsl:if>
				</xsl:attribute>
				<label class="title-" for="f-{@name}_day">
					<xsl:value-of select="@title"/>
					<xsl:text>:</xsl:text>
				</label>
				<div class="input-">
					<xsl:variable name="year">
						<xsl:choose>
							<xsl:when test="$is_add">
								<xsl:value-of select="substring(/root/@time, 1, 4)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$document_field/@year"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="month">
						<xsl:choose>
							<xsl:when test="$is_add">
								<xsl:value-of select="substring(/root/@time, 6, 2)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$document_field/@month"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="day">
						<xsl:choose>
							<xsl:when test="$is_add">
								<xsl:value-of select="substring(/root/@time, 9, 2)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$document_field/@day"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<select id="f-{@name}_day" name="{@name}_day" class="right-margin- f-{@name}">
						<xsl:for-each select="days_for_select/day">
							<option value="{@key}">
								<xsl:choose>
									<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_day')] = @key">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:when>
									<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_day')]"/>
									<xsl:when test="$day = @key">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:when>
								</xsl:choose>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
					<select id="f-{@name}_month" name="{@name}_month" class="right-margin- f-{@name}">
						<xsl:for-each select="months_for_select/month">
							<option value="{@key}">
								<xsl:choose>
									<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_month')] = @key">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:when>
									<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_month')]"/>
									<xsl:when test="$month = @key">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:when>
								</xsl:choose>
								<xsl:call-template name="get_month_name">
									<xsl:with-param name="month_number" select="@key"/>
									<xsl:with-param name="type" select="'with_day'"/>
								</xsl:call-template>
							</option>
						</xsl:for-each>
					</select>
					<select id="f-{@name}_year" name="{@name}_year" class="right-margin- f-{@name}">
						<xsl:choose>
							<xsl:when test="$document_field/years_for_select">
								<xsl:for-each select="$document_field/years_for_select/year">
									<option value="{@key}">
										<xsl:choose>
											<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_year')] = @key">
												<xsl:attribute name="selected">selected</xsl:attribute>
											</xsl:when>
											<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_year')]"/>
											<xsl:when test="$year = @key">
												<xsl:attribute name="selected">selected</xsl:attribute>
											</xsl:when>
										</xsl:choose>
										<xsl:value-of select="@title"/>
									</option>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<xsl:for-each select="years_for_select/year">
									<option value="{@key}">
										<xsl:choose>
											<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_year')] = @key">
												<xsl:attribute name="selected">selected</xsl:attribute>
											</xsl:when>
											<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_year')]"/>
											<xsl:when test="$year = @key">
												<xsl:attribute name="selected">selected</xsl:attribute>
											</xsl:when>
										</xsl:choose>
										<xsl:value-of select="@title"/>
									</option>
								</xsl:for-each>
							</xsl:otherwise>
						</xsl:choose>
					</select>
					<xsl:if test="@date_only = 0">
						<xsl:variable name="hours">
							<xsl:choose>
								<xsl:when test="$is_add">
									<xsl:value-of select="'12'"/>
									<!--<xsl:value-of select="substring(/root/@time, 12, 2)"/>-->
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$document_field/@hours"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="minutes">
							<xsl:choose>
								<xsl:when test="$is_add">
									<xsl:value-of select="'00'"/>
									<!--<xsl:value-of select="substring(/root/@time, 15, 2)"/>-->
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$document_field/@minutes"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<select id="f-{@name}_hours" name="{@name}_hours" class="right-margin- left-margin- f-{@name}">
							<xsl:for-each select="hours_for_select/hour">
								<option value="{@key}">
									<xsl:choose>
										<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_hours')] = @key">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
										<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_hours')]"/>
										<xsl:when test="$hours = @key">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
									</xsl:choose>
									<xsl:value-of select="@title"/>
								</option>
							</xsl:for-each>
						</select>
						<xsl:text>:</xsl:text>
						<select id="f-{@name}_minutes" name="{@name}_minutes" class="left-margin- right-margin- f-{@name}">
							<xsl:for-each select="minutes_for_select/minute">
								<option value="{@key}">
									<xsl:choose>
										<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_minutes')] = @key">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
										<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_minutes')]"/>
										<xsl:when test="$minutes = @key">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
									</xsl:choose>
									<xsl:value-of select="@title"/>
								</option>
							</xsl:for-each>
						</select>
					</xsl:if>
					<input id="f-{@name}-datepicker" class="datepicker-" type="text"/>
					<xsl:if test="$can_be_hidden">
						<span id="f-{@name}-deleter" class="disabler- icons-small">
							<span class="delete-"/>
						</span>
					</xsl:if>
				</div>
				<xsl:apply-templates mode="dtf_error" select="."/>
				<xsl:if test="@comment != ''">
					<div class="comment-">
						<xsl:value-of select="@comment"/>
					</div>
				</xsl:if>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
