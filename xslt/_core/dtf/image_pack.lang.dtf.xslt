<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'image_pack' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_MUCH_SIZE']">
					<div class="error-">
						<span>
							<xsl:text>Размер файла слишком большой. Максимально допустимый размер </xsl:text>
							<xsl:value-of select="@max_file_size_formatted"/>
							<xsl:text>, а у Вас </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'TOO_MUCH_SIZE']"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNKNOWN_TYPE']">
					<div class="error-">
						<span>Неизвестный тип изображения. Мы понимаем jpeg, gif и png</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'CANT_RESIZE']">
					<div class="error-">
						<span>
							<xsl:text>Произошла ошибка во время преобразования изображения (код: </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'CANT_RESIZE']"/>
							<xsl:text>)</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_MUCH_SIZE_INI']">
					<div class="error-">
						<span>Размер файла слишком большой.</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UPLOAD_ERR_FORM_SIZE']">
					<div class="error-">
						<span>Размер файла слишком большой.</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UPLOAD_ERR_PARTIAL']">
					<div class="error-">
						<span>Загрузка файла произошла только частично. Попробуйте загрузить файл ещё раз</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UPLOAD_ERR_NO_TMP_DIR']">
					<div class="error-">
						<span>Отсутствует директория для временной загрузки файла. Обратитесь к администратору проекта</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UPLOAD_ERR_CANT_WRITE']">
					<div class="error-">
						<span>Ошибка записи загруженного файла на диск. Обратитесь к администратору проекта</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UPLOAD_ERR_EXTENSION']">
					<div class="error-">
						<span>Попытка загрузки файла с запрещённым расширением</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNKNOWN_UPLOAD_ERROR']">
					<div class="error-">
						<span>Произошла неизвестная ошибка при загрузке файла.</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
	<xsl:template match="field[@type = 'image_pack' and ancestor::doctype]" mode="dtf_image_pack_delete_image">
		<xsl:text>Удалить изображение</xsl:text>
	</xsl:template>
	<xsl:template match="field[@type = 'image_pack' and ancestor::doctype]" mode="dtf_image_pack_see">
		<xsl:text>Смотреть</xsl:text>
	</xsl:template>
</xsl:stylesheet>
