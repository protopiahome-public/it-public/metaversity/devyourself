<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'password' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNFILLED']">
					<div class="error-">
						<span>Пароль не должен быть пустым</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_SHORT']">
					<div class="error-">
						<span>
							<xsl:text>Пароль слишком короткий. Минимально допустимая длина </xsl:text>
							<xsl:value-of select="@min_length"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="count_case">
								<xsl:with-param name="number" select="@min_length"/>
								<xsl:with-param name="word_ns" select="' символ'"/>
								<xsl:with-param name="word_gs" select="' символа'"/>
								<xsl:with-param name="word_ap" select="' символов'"/>
							</xsl:call-template>
							<xsl:text>, а у Вас </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'TOO_SHORT']"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_LONG']">
					<div class="error-">
						<span>
							<xsl:text>Пароль слишком длинный. Максимально допустимая длина </xsl:text>
							<xsl:value-of select="@max_length"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="count_case">
								<xsl:with-param name="number" select="@max_length"/>
								<xsl:with-param name="word_ns" select="' символ'"/>
								<xsl:with-param name="word_gs" select="' символа'"/>
								<xsl:with-param name="word_ap" select="' символов'"/>
							</xsl:call-template>
							<xsl:text>, а у Вас </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'TOO_LONG']"/>
						</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
	<xsl:template match="field[@type = 'password' and ancestor::doctype]" mode="dtf_password_draw_warning">
		<xsl:variable name="is_add" select="ancestor::doctype/../@action = 'dt_add'"/>
		<xsl:variable name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<xsl:if test="not($is_add) or $pass_info and not($pass_info/error[@field = current()/@name])">
			<div class="comment-">
				<xsl:variable name="blank_allowed" select="not($is_add) and not($document_field/@is_blank = 1)"/>
				<xsl:if test="$blank_allowed">Оставьте поле пустым, чтобы не менять пароль</xsl:if>
				<xsl:if test="not($pass_info/error[@field = current()/@name])">
					<xsl:if test="$blank_allowed">. </xsl:if>
					<xsl:text>Минимальная длина &#8212; </xsl:text>
					<xsl:value-of select="@min_length"/>
					<xsl:text> </xsl:text>
					<xsl:call-template name="count_case">
						<xsl:with-param name="number" select="@min_length"/>
						<xsl:with-param name="word_ns" select="' символ'"/>
						<xsl:with-param name="word_gs" select="' символа'"/>
						<xsl:with-param name="word_ap" select="' символов'"/>
					</xsl:call-template>
				</xsl:if>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template match="field[@type = 'password' and ancestor::doctype]" mode="dtf_error_2">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'PASSWORDS_NOT_EQUAL']">
					<div class="error-">
						<span>Введённые пароли не совпадают</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
