<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'datetime' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'INCORRECT_VALUE']">
					<div class="error-">
						<span>Некорректное значение одного из параметров</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNALLOWED_YEAR']">
					<div class="error-">
						<span>Некорректный год</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNALLOWED_MONTH']">
					<div class="error-">
						<span>Некорректный месяц</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNALLOWED_DAY']">
					<div class="error-">
						<span>Некорректный день</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNALLOWED_HOURS']">
					<div class="error-">
						<span>Некорректное значение часов</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNALLOWED_MINUTES']">
					<div class="error-">
						<span>Некорректное значение минут</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'FAILED_DEPENDENCY']">
					<div class="error-">
						<span>
							<xsl:text>Дата должна быть </xsl:text>
							<xsl:choose>
								<xsl:when test="@can_be_equal = 1">не меньше</xsl:when>
								<xsl:otherwise>больше</xsl:otherwise>
							</xsl:choose>
							<xsl:text>, чем &#171;</xsl:text>
							<xsl:value-of select="../field[@name = current()/@must_be_later_than_field_name]/@title"/>
							<xsl:text>&#187;</xsl:text>
						</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
	<xsl:template match="field[@type = 'datetime' and ancestor::doctype]" mode="dtf_datetime_show_finish_time">
		<xsl:text>Указать дату окончания</xsl:text>
	</xsl:template>
</xsl:stylesheet>
