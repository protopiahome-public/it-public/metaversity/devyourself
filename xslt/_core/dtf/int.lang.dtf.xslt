<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'int' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNFILLED']">
					<div class="error-">
						<span>Это обязательное поле, и оно должно быть заполнено</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'NOT_INT']">
					<div class="error-">
						<span>Введённое значение должно быть целым числом</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_SMALL']">
					<div class="error-">
						<span>
							<xsl:text>Введённое число слишком маленькое. Минимально допустимое значение &#8212; </xsl:text>
							<xsl:value-of select="@min_value"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_BIG']">
					<div class="error-">
						<span>
							<xsl:text>Введённое число слишком большое. Максимально допустимое значение &#8212; </xsl:text>
							<xsl:value-of select="@max_value"/>
						</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
</xsl:stylesheet>
