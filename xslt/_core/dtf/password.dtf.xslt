<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'password']" mode="dtf">
		<xsl:variable name="is_add" select="ancestor::doctype/../@action = 'dt_add'"/>
		<xsl:variable name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<label class="title-" for="f-{@name}">
				<xsl:value-of select="@title"/>
				<xsl:text>:</xsl:text>
				<xsl:if test="$is_add">
					<span class="star">
						<xsl:text>&#160;*</xsl:text>
					</span>
				</xsl:if>
			</label>
			<div class="input-">
				<input id="f-{@name}" class="input-password f-{@name}" type="password" name="{@name}" value="">
					<xsl:if test="@max_length != 0">
						<xsl:attribute name="maxlength">
							<xsl:value-of select="@max_length"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="$pass_info/vars/var[@name = current()/@name]">
						<xsl:attribute name="value">
							<xsl:value-of select="$pass_info/vars/var[@name = current()/@name]"/>
						</xsl:attribute>
					</xsl:if>
				</input>
			</div>
			<xsl:apply-templates mode="dtf_error" select="."/>
			<xsl:apply-templates mode="dtf_password_draw_warning" select="."/>
		</div>
		<div class="field">
			<label class="title-" for="f-{@name}-check">
				<xsl:text>Пароль, еще раз:</xsl:text>
				<xsl:if test="$is_add">
					<span class="star">
						<xsl:text>&#160;*</xsl:text>
					</span>
				</xsl:if>
			</label>
			<div class="input-">
				<input id="f-{@name}-check" class="input-password f-{@name}" type="password" name="{@name}_check" value="">
					<xsl:if test="@max_length != 0">
						<xsl:attribute name="maxlength">
							<xsl:value-of select="@max_length"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="$pass_info/vars/var[@name = concat(current()/@name, '_check')]">
						<xsl:attribute name="value">
							<xsl:value-of select="$pass_info/vars/var[@name = concat(current()/@name, '_check')]"/>
						</xsl:attribute>
					</xsl:if>
				</input>
			</div>
			<xsl:apply-templates mode="dtf_error_2" select="."/>
		</div>
	</xsl:template>
</xsl:stylesheet>
