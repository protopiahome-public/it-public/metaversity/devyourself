<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'text_br']" mode="dtf">
		<xsl:variable name="is_add" select="ancestor::doctype/../@action = 'dt_add'"/>
		<xsl:variable name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<label class="title-" for="f-{@name}">
				<xsl:value-of select="@title"/>
				<xsl:text>:</xsl:text>
				<span class="star">
					<xsl:if test="not(@is_important = 1)">
						<xsl:attribute name="class">star dn</xsl:attribute>
					</xsl:if>
					<xsl:text>&#160;*</xsl:text>
				</span>
			</label>
			<textarea id="f-{@name}" class="textarea-br- f-{@name}" name="{@name}" style="height: {@editor_height}px">
				<xsl:choose>
					<xsl:when test="$pass_info/vars/var[@name = current()/@name]">
						<xsl:value-of select="$pass_info/vars/var[@name = current()/@name]"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$document_field"/>
					</xsl:otherwise>
				</xsl:choose>
			</textarea>
			<xsl:apply-templates mode="dtf_error" select="."/>
			<xsl:if test="@comment != ''">
				<div class="comment-">
					<xsl:value-of select="@comment"/>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
