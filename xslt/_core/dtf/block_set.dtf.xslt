<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../../block_sets/include/block_set_edit.inc.xslt"/>
	<xsl:template match="field[@type = 'block_set']" mode="dtf">
		<xsl:variable name="is_add" select="ancestor::doctype/../@action = 'dt_add'"/>
		<xsl:variable name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<div class="block-set-editor-field-title-">
				<xsl:value-of select="concat(@title, ':')"/>
			</div>
			<xsl:choose>
				<xsl:when test="$document_field">
					<xsl:for-each select="/root/block_set[@id = $document_field/text()]">
						<xsl:call-template name="draw_block_set_edit"/>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:for-each select="/root/block_set[not(@id)]">
						<xsl:call-template name="draw_block_set_edit"/>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
