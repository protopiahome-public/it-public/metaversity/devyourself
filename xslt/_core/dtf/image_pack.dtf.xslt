<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'image_pack']" mode="dtf">
		<xsl:variable name="is_add" select="ancestor::doctype/../@action = 'dt_add'"/>
		<xsl:variable name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<label class="title-" for="f-{@name}">
				<xsl:value-of select="@title"/>
				<xsl:text>:</xsl:text>
			</label>
			<xsl:if test="$document_field/image/@is_uploaded = 1">
				<div class="image-">
					<xsl:if test="not($document_field/image[@is_uploaded = 1 and @width &lt;= 100 and @height &lt;= 100])">
						<a href="{$document_field/image[1]/@url}">
							<xsl:apply-templates mode="dtf_image_pack_see" select="."/>
						</a>
					</xsl:if>
					<xsl:for-each select="$document_field/image[@is_uploaded = 1 and @width &lt;= 100 and @height &lt;= 100][1]">
						<xsl:choose>
							<xsl:when test="$document_field/image[@is_uploaded = 1 and @width &gt; 100 or @height &gt; 100]">
								<a href="{$document_field/image[@is_uploaded = 1 and @width &gt; 100 or @height &gt; 100]/@url}">
									<img src="{@url}" alt="{@name}" width="{@width}" height="{@height}" style="margin-right: 5px"/>
								</a>
							</xsl:when>
							<xsl:otherwise>
								<img src="{@url}" alt="{@name}" width="{@width}" height="{@height}" style="margin-right: 5px"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</div>
			</xsl:if>
			<input id="f-{@name}" class="input-file f-{@name}" type="file" name="{@name}" value="{$document_field}"/>
			<xsl:apply-templates mode="dtf_error" select="."/>
			<xsl:if test="@comment != ''">
				<div class="comment-">
					<xsl:value-of select="@comment"/>
				</div>
			</xsl:if>
			<xsl:if test="$document_field/image/@is_uploaded = 1">
				<div class="image-delete-">
					<input id="f-{@name}_delete" class="input-checkbox f-{@name}" type="checkbox" name="{@name}_delete">
						<xsl:choose>
							<xsl:when test="$pass_info/vars/var[@name = concat(current()/@name, '_delete')]">
								<xsl:attribute name="checked">checked</xsl:attribute>
							</xsl:when>
						</xsl:choose>
					</input>
					<label for="f-{@name}_delete" class="inline-">
						<xsl:apply-templates mode="dtf_image_pack_delete_image" select="."/>
					</label>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
