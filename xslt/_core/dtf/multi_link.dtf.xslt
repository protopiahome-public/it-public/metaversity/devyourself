<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'multi_link']" mode="dtf">
		<xsl:variable name="is_add" select="ancestor::doctype/../@action = 'dt_add'"/>
		<xsl:variable name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<xsl:variable name="doctype_field" select="."/>
		<xsl:if test=".//item">
			<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
				<label class="title-" for="f-{@name}">
					<xsl:value-of select="@title"/>
					<xsl:text>:</xsl:text>
				</label>
				<div class="input-set-">
					<xsl:if test="@max_height">
						<xsl:attribute name="style">
							<xsl:text>overflow: auto; max-height: </xsl:text>
							<xsl:value-of select="@max_height"/>
							<xsl:text>px</xsl:text>
						</xsl:attribute>
					</xsl:if>
					<xsl:for-each select=".//item">
						<div class="input-">
							<xsl:if test="$doctype_field/@tree_mode">
								<xsl:attribute name="style">
									<xsl:value-of select="concat('margin-left: ', @level * 20 - 20, 'px;')"/>
								</xsl:attribute>
							</xsl:if>
							<input id="f-{$doctype_field/@name}-{@id}" class="input-checkbox jq-dtf-multi-link-item" type="checkbox"
								name="{$doctype_field/@name}[{@id}]" value="1" jq-dtf-name="{$doctype_field/@name}">
								<xsl:attribute name="jq-ancestors">
									<xsl:value-of select="@ancestors"/>
								</xsl:attribute>
								<xsl:attribute name="jq-descendants">
									<xsl:value-of select="@descendants"/>
								</xsl:attribute>
								<xsl:if test="$pass_info/vars/var[@name = $doctype_field/@name]/item[@index = current()/@id] = 1">
									<xsl:attribute name="checked">checked</xsl:attribute>
								</xsl:if>
								<xsl:if test="$document_field/item[@id = current()/@id]">
									<xsl:attribute name="checked">checked</xsl:attribute>
								</xsl:if>
							</input>
							<label for="f-{$doctype_field/@name}-{@id}" class="inline-">
								<xsl:value-of select="@title"/>
							</label>
						</div>
					</xsl:for-each>
				</div>
				<xsl:apply-templates mode="dtf_error" select="."/>
				<xsl:if test="@comment != ''">
					<div class="comment-">
						<xsl:value-of select="@comment"/>
					</div>
				</xsl:if>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
