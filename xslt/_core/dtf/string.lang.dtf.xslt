<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'string' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNFILLED']">
					<div class="error-">
						<span>Это обязательное поле, и оно должно быть заполнено</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNIQUE_CHECK_FAILED']">
					<div class="error-">
						<span>
							<xsl:value-of select="@unique_error_phrase"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_SHORT']">
					<div class="error-">
						<span>
							<xsl:text>Строка слишком короткая. Минимально допустимая длина </xsl:text>
							<xsl:value-of select="@min_length"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="count_case">
								<xsl:with-param name="number" select="@min_length"/>
								<xsl:with-param name="word_ns" select="' символ'"/>
								<xsl:with-param name="word_gs" select="' символа'"/>
								<xsl:with-param name="word_ap" select="' символов'"/>
							</xsl:call-template>
							<xsl:text>, а у Вас </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'TOO_SHORT']"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_LONG']">
					<div class="error-">
						<span>
							<xsl:text>Строка слишком длинная. Максимально допустимая длина </xsl:text>
							<xsl:value-of select="@max_length"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="count_case">
								<xsl:with-param name="number" select="@max_length"/>
								<xsl:with-param name="word_ns" select="' символ'"/>
								<xsl:with-param name="word_gs" select="' символа'"/>
								<xsl:with-param name="word_ap" select="' символов'"/>
							</xsl:call-template>
							<xsl:text>, а у Вас </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'TOO_LONG']"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'REGEXP_INCOMPATIBILITY']">
					<div class="error-">
						<span>
							<xsl:value-of select="@regexp_error_phrase"/>
						</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
</xsl:stylesheet>
