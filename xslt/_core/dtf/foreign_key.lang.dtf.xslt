<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'foreign_key' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNFILLED']">
					<div class="error-">
						<span>Это обязательное поле, и оно должно быть заполнено</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNEXISTED_VALUE']">
					<div class="error-">
						<span>Выбрано несуществующее значение. Попробуйте ещё раз</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
	<xsl:template match="field[@type = 'foreign_key' and ancestor::doctype]" mode="dtf_foreign_key_not_selected">
		<xsl:text>Не выбрано</xsl:text>
	</xsl:template>
</xsl:stylesheet>
