<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'url_name']" mode="dtf">
		<xsl:variable name="is_add" select="ancestor::doctype/../@action = 'dt_add'"/>
		<xsl:variable name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<label class="title-" for="f-{@name}">
				<xsl:value-of select="@title"/>
				<xsl:text>:</xsl:text>
				<span class="star">&#160;*</span>
			</label>
			<div class="input-">
				<input id="f-{@name}" class="input-text f-{@name}" type="text" name="{@name}" value="{$document_field}">
					<xsl:if test="@max_length != 0">
						<xsl:attribute name="maxlength">
							<xsl:value-of select="@max_length"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="$pass_info/vars/var[@name = current()/@name]">
						<xsl:attribute name="value">
							<xsl:value-of select="$pass_info/vars/var[@name = current()/@name]"/>
						</xsl:attribute>
					</xsl:if>
				</input>
			</div>
			<xsl:apply-templates mode="dtf_error" select="."/>
			<xsl:if test="@comment != ''">
				<div class="comment-">
					<xsl:value-of select="@comment"/>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
