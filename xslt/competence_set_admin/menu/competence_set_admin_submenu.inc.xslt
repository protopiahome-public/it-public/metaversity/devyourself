<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_project_admin_submenu">
		<xsl:param name="base_url"/>
		<div class="widget-wrap">
			<div class="widget widget-menu">
				<div class="items-">
					<div>
						<xsl:attribute name="class"><xsl:text>menu-item- </xsl:text><xsl:if test="$admin_section = 'general'">menu-item-selected- </xsl:if></xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'general' and $admin_section_main_page and not($get_vars)">Основные свойства</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}">Основные свойства</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div>
						<xsl:attribute name="class"><xsl:text>menu-item- </xsl:text><xsl:if test="$admin_section = 'tree'">menu-item-selected- </xsl:if></xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'tree' and $admin_section_main_page and not($get_vars)">Дерево компетенций</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}tree/">Дерево компетенций</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div>
						<xsl:attribute name="class"><xsl:text>menu-item- </xsl:text><xsl:if test="$admin_section = 'moderators'">menu-item-selected- </xsl:if></xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'moderators' and $admin_section_main_page and not($get_vars)">Модераторы</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}moderators/">Модераторы</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div>
						<xsl:attribute name="class"><xsl:text>menu-item- </xsl:text><xsl:if test="$admin_section = 'admins'">menu-item-selected- </xsl:if></xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'admins' and $admin_section_main_page and not($get_vars)">Администраторы</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}admins/">Администраторы</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div class="clear"/>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
