<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../competence_sets/head2/competence_set_head2.inc.xslt"/>
	<xsl:include href="menu/competence_set_admin_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'sets'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="competence_set_section" select="'admin'"/>
	<xsl:variable name="competence_set_section_main_page" select="true()"/>
	<xsl:variable name="admin_section" select="'general'"/>
	<xsl:variable name="admin_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($current_competence_set_url, 'admin/')"/>
	<xsl:template match="competence_set_admin_general">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_competence_set_head2"/>
							<h2>Настройка набора компетенций</h2>
							<div class="content">
								<h3 class="first">Основные свойства</h3>
								<form action="{$save_prefix}/competence_set_admin_general/" method="post" enctype="multipart/form-data">
									<xsl:if test="$pass_info/info[@name = 'action'] = 'dt_add' and $pass_info/info[@name = 'SAVED']">
										<div class="box">
											<div class="content- bigger">
												<strong>Вы только что создали набор компетенций!</strong>
												<br/>
												<br/>Вы можете продолжить настройку (используйте для этого меню справа) или 
												<a href="{$current_competence_set_url}">перейти на главную страницу</a> набора.
											</div>
										</div>
									</xsl:if>
									<xsl:call-template name="draw_dt_edit">
										<xsl:with-param name="disable_saved_info" select="$pass_info/info[@name = 'action'] = 'dt_add'"/>
									</xsl:call-template>
								</form>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
							<xsl:call-template name="draw_project_admin_submenu">
								<xsl:with-param name="base_url" select="concat($prefix, '/sets/set-', $current_competence_set/@id, '/admin/')"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Основные свойства &#8212; </xsl:text>
		<xsl:value-of select="$current_competence_set/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/sets/">Наборы компетенций</a>
		</div>
		<div class="level2-">
			<a href="{$current_competence_set_url}">
				<xsl:value-of select="$current_competence_set/@title"/>
			</a>
		</div>
		<div class="level3- selected-">Настройка</div>
	</xsl:template>
</xsl:stylesheet>
