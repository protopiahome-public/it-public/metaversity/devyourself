<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../competence_sets/head2/competence_set_head2.inc.xslt"/>
	<xsl:include href="menu/competence_set_admin_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'sets'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="competence_set_section" select="'admin'"/>
	<xsl:variable name="competence_set_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'tree'"/>
	<xsl:variable name="admin_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($current_competence_set_url, 'admin/tree/')"/>
	<xsl:template match="competence_set_admin_tree">
		<div id="dialog-confirm-delete" class="dn">
			<p>Подтвердите: пункт будет удалён.</p>
		</div>
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_competence_set_head2"/>
							<h2>Настройка набора компетенций</h2>
							<div class="content">
								<h3>Дерево компетенций</h3>
								<xsl:call-template name="_draw_tree"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
							<xsl:call-template name="draw_project_admin_submenu">
								<xsl:with-param name="base_url" select="concat($prefix, '/sets/set-', $current_competence_set/@id, '/admin/')"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_tree">
		<xsl:choose>
			<xsl:when test="@multi_link">
				<p>Это дерево компетенций не предназначено для редактирования.</p>
			</xsl:when>
			<xsl:otherwise>
				<div class="ico-links-small">
					<strong id="tree_button_add_group" class="add-group- disabled-">
						<span>добавить группу</span>
					</strong>
					<strong id="tree_button_add_item" class="add- disabled-">
						<span>добавить компетенцию</span>
					</strong>
					<strong id="tree_button_edit" class="edit- disabled-">
						<span>редактировать</span>
					</strong>
					<strong id="tree_button_delete" class="delete- disabled-">
						<span>удалить</span>
					</strong>
				</div>
				<div id="competences_tree" class="competences_tree"/>
				<script type="text/javascript">
					var competences_ajax_url = global.ajax_prefix + "competence_set_admin_tree/";
					var competence_set_id = <xsl:value-of select="@competence_set_id"/>;
				</script>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Дерево компетенций &#8212; </xsl:text>
		<xsl:value-of select="$current_competence_set/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/sets/">Наборы компетенций</a>
		</div>
		<div class="level2-">
			<a href="{$current_competence_set_url}">
				<xsl:value-of select="$current_competence_set/@title"/>
			</a>
		</div>
		<div class="level3-">
			<a href="{$current_competence_set_url}admin/">Настройка</a>
		</div>
		<div class="level4- selected-">Дерево компетенций</div>
	</xsl:template>
	<xsl:template mode="jquery" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery161.js"> </script>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery.hotkeys.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.jstree.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.scrollTo.js"/>
		<script type="text/javascript" src="{$prefix}/js/competence_set_admin_tree.js"/>
	</xsl:template>
</xsl:stylesheet>
