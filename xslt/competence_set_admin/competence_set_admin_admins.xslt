<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../competence_sets/head2/competence_set_head2.inc.xslt"/>
	<xsl:include href="../_site/include/admin_admins.inc.xslt"/>
	<xsl:include href="menu/competence_set_admin_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'sets'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="competence_set_section" select="'admin'"/>
	<xsl:variable name="competence_set_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'admins'"/>
	<xsl:variable name="admin_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($current_competence_set_url, 'admin/admins/')"/>
	<xsl:template match="competence_set_admin_admins">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_competence_set_head2"/>
							<h2>Настройка набора компетенций</h2>
							<div class="content">
								<h3>Администраторы</h3>
								<xsl:call-template name="draw_admin_admins">
									<xsl:with-param name="save_ctrl" select="'competence_set_admin_admins'"/>
									<xsl:with-param name="aux_params">
										<input type="hidden" name="competence_set_id" value="{$current_competence_set/@id}"/>
									</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
							<xsl:call-template name="draw_project_admin_submenu">
								<xsl:with-param name="base_url" select="concat($prefix, '/sets/set-', $current_competence_set/@id, '/admin/')"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/admin_admins.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Администраторы &#8212; </xsl:text>
		<xsl:value-of select="$current_competence_set/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/sets/">Наборы компетенций</a>
		</div>
		<div class="level2-">
			<a href="{$current_competence_set_url}">
				<xsl:value-of select="$current_competence_set/@title"/>
			</a>
		</div>
		<div class="level3-">
			<a href="{$current_competence_set_url}admin/">Настройка</a>
		</div>
		<div class="level4- selected-">Администраторы</div>
	</xsl:template>
</xsl:stylesheet>
