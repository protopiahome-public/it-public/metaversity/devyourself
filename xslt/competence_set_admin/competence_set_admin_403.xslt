<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../competence_sets/head2/competence_set_head2.inc.xslt"/>
	<xsl:variable name="top_section" select="'sets'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="competence_set_section" select="'admin'"/>
	<xsl:variable name="competence_set_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'admins'"/>
	<xsl:variable name="admin_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($current_competence_set_url, 'admin/admins/')"/>
	<xsl:template match="error_403">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_competence_set_head2"/>
							<h2>Настройка набора компетенций</h2>
							<div class="content">
								<div class="box box-stop">
									<h3>Доступ запрещён</h3>
									<p>У вас нет прав на управление этим набором компетенций.</p>
								</div>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_nav"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/admins.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Доступ запрещён &#8212; </xsl:text>
		<xsl:value-of select="$current_competence_set/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/sets/">Наборы компетенций</a>
		</div>
		<div class="level2-">
			<a href="{$current_competence_set_url}">
				<xsl:value-of select="$current_competence_set/@title"/>
			</a>
		</div>
	</xsl:template>
</xsl:stylesheet>
