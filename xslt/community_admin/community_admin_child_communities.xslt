<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../community/head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="menu/community_admin_submenu.inc.xslt"/>
	<xsl:include href="../_site/include/admin_members.inc.xslt"/>
	<xsl:include href="../_site/include/access.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'admin'"/>
	<xsl:variable name="community_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'child-communities'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_community/@url, 'admin/child-communities/')"/>
	<xsl:template match="community_admin_child_communities">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Настройка сообщества</h2>
							<div class="content">
								<h3>Дочерние сообщества</h3>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_community_admin_submenu">
								<xsl:with-param name="base_url" select="concat($current_community/@url, 'admin/')"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/community_admin_child_communities/" method="post">
			<input type="hidden" name="retpath" value="{$module_url}"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<input type="hidden" name="community_id" value="{$current_community/@id}"/>
			<input id="jq-delete-community-id" type="hidden" name="delete_community_id" value="0"/>
			<div id="jq-dialog-confirm" class="dn">
				<p>
					<xsl:text>Подтвердите: дочернее сообщество </xsl:text>
					<strong id="jq-dialog-confirm-title">???</strong>
					<xsl:text> будет отсоединено.</xsl:text>
				</p>
			</div>
			<xsl:choose>
				<xsl:when test="$current_community/@allow_child_communities = 'nobody'">
					<div class="box">
						<p>
							<xsl:text>Создание дочерних сообществ отключено. Включить можно в разделе </xsl:text>
							<a href="{$current_community/@url}admin/access/#access_child_communities">Управление доступом</a>
							<xsl:text>.</xsl:text>
						</p>
					</div>
				</xsl:when>
				<xsl:when test="not($project_access/@can_add_communities = 1)">
					<div class="box">
						<p>Создание сообществ ограничено.</p>
						<xsl:call-template name="draw_access">
							<xsl:with-param name="status" select="$project_access/@access_communities_add"/>
							<xsl:with-param name="entity_genitiv" select="'проекта'"/>
							<xsl:with-param name="join_premoderation" select="$project_access/@join_premoderation = 1"/>
							<xsl:with-param name="special_moderator_entity_genitiv" select="'сообществ'"/>
							<xsl:with-param name="for" select="'Для создания сообщества'"/>
						</xsl:call-template>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<div class="ico-links">
						<a class="add-" href="{$current_community/@url}subcommunities/add/">
							<span>
								<xsl:text>создать дочернее сообщество</xsl:text>
							</span>
						</a>
					</div>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="not(community)">
				<p>
					<xsl:text>Ни одного дочернего сообщества не найдено.</xsl:text>
				</p>
			</xsl:if>
			<xsl:if test="community">
				<table class="tbl">
					<tr>
						<th/>
						<xsl:call-template name="draw_tbl_th">
							<xsl:with-param name="name" select="'title'"/>
							<xsl:with-param name="title" select="'Название'"/>
						</xsl:call-template>
						<xsl:call-template name="draw_tbl_th">
							<xsl:with-param name="name" select="'posts'"/>
							<xsl:with-param name="title" select="'Записей'"/>
							<xsl:with-param name="center" select="true()"/>
						</xsl:call-template>
						<xsl:call-template name="draw_tbl_th">
							<xsl:with-param name="name" select="'comments'"/>
							<xsl:with-param name="title" select="'Комментариев'"/>
							<xsl:with-param name="center" select="true()"/>
						</xsl:call-template>
						<xsl:call-template name="draw_tbl_th">
							<xsl:with-param name="name" select="'date'"/>
							<xsl:with-param name="title" select="'Дата&#160;добавления'"/>
						</xsl:call-template>
						<th>
							<span>Отсоединить</span>
						</th>
					</tr>
					<xsl:for-each select="community">
						<xsl:variable name="community_id" select="@id"/>
						<xsl:variable name="position" select="position()"/>
						<xsl:variable name="context" select="."/>
						<xsl:for-each select="/root/community_short[@id = current()/@id]">
							<tr>
								<td class="col-logo-">
									<a href="{@url}">
										<img src="{logo_small/@url}" width="{logo_small/@width}" height="{logo_small/@height}" alt="{@title}" title=""/>
									</a>
								</td>
								<td class="col-community-title-">
									<div class="title-">
										<a href="{@url}">
											<xsl:value-of select="@title"/>
										</a>
									</div>
									<xsl:if test="@descr_short != ''">
										<div class="descr-">
											<xsl:value-of select="@descr_short"/>
										</div>
									</xsl:if>
								</td>
								<td class="col-digits-">
									<xsl:value-of select="$context/@post_count_calc"/>
								</td>
								<td class="col-digits-">
									<xsl:value-of select="$context/@comment_count_calc"/>
								</td>
								<td class="col-date-">
									<xsl:call-template name="get_full_datetime">
										<xsl:with-param name="datetime" select="@add_time"/>
									</xsl:call-template>
								</td>
								<td class="col-icons- icons">
									<a class="jq-delete-button delete- gray-" data-community-id="{@id}" data-community-title="{@title}" href="javascript:void(0);" title="Отсоединить подсообщество"/>
								</td>
							</tr>
						</xsl:for-each>
					</xsl:for-each>
				</table>
			</xsl:if>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/community_admin_child_communities.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Дочерние сообщества &#8212; </xsl:text>
		<xsl:value-of select="$current_community/@title"/>
	</xsl:template>
</xsl:stylesheet>
