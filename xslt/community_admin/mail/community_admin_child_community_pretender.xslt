<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="xml" encoding="UTF-8"/>
	<xsl:variable name="request" select="/root/request"/>
	<xsl:variable name="prefix" select="$request/@prefix"/>
	<xsl:variable name="sys_params" select="/root/sys_params"/>
	<xsl:variable name="domain_prefix" select="$request/@domain_prefix"/>
	<xsl:variable name="current_project" select="/root/project_short[1]"/>
	<xsl:variable name="current_community" select="/root/community_short[1]"/>
	<xsl:variable name="pretender_community" select="/root/community_short[2]"/>
	<xsl:variable name="admin" select="/root/user_short[1]"/>
	<xsl:template match="/root">
		<html>
			<head>
				<title>
					<xsl:choose>
						<xsl:when test="/root/is_now_child_community = 1">Ваше сообщество присоединили к сообществу </xsl:when>
						<xsl:otherwise>Отклонена заявка Вашего сообщества на присоединение к сообществу </xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="$current_community/@title"/>
				</title>
			</head>
			<body>
				<style type="text/css">
					body {padding: 0; margin: 0;}
					body, td {font-family: Arial, Tahoma, Verdana, serif; font-size: 10pt; line-height: 13pt; background: #fff; color: #000; text-align: left;}
					p {margin: 0 0 5pt; padding: 0;}
					img {border: 0; vertical-align: middle;}
					ul, ol, li {margin: 0; padding: 0;}
					ul, ol {margin-bottom: 5pt;}
					li {margin: 0 0 5pt 20pt;}
					ol li {list-style-type: decimal;}
					ul li {list-style-type: square;}
					a {color: #463d37; text-decoration: underline;}
					.main {padding: 5pt 5pt 0;}
					.bot {color: #666; margin-bottom: 12pt; margin-top: 15pt;}
				</style>
				<div class="main">
					<p>
						<xsl:text>Администратор </xsl:text>
						<a href="{$prefix}/users/{$admin/@login}/">
							<xsl:value-of select="$admin/@visible_name"/>
						</a>
						<xsl:choose>
							<xsl:when test="/root/is_now_child_community = 1"> присоединил Ваше сообщество </xsl:when>
							<xsl:otherwise> отклонил заявку на присоединение Вашего сообщества </xsl:otherwise>
						</xsl:choose>
						<a href="{$domain_prefix}{$pretender_community/@url}">
							<xsl:value-of select="$pretender_community/@title"/>
						</a>
						<xsl:text> к сообществу </xsl:text>
						<a href="{$domain_prefix}{$current_community/@url}">
							<xsl:value-of select="$current_community/@title"/>
						</a>
						<xsl:text> проекта </xsl:text>
						<a href="{$domain_prefix}{$current_project/@url}">
							<xsl:value-of select="$current_project/@title"/>
						</a>
						<xsl:text>.</xsl:text>
					</p>
					<div class="bot">
						<xsl:text>Если Вы не подавали заявку на присоединение Вашего сообщества, напишите администрации сервиса на email </xsl:text>
						<a class="email" href="mailto:{$sys_params/@info_email}">
							<xsl:value-of select="$sys_params/@info_email"/>
						</a>
						<xsl:text>.</xsl:text>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
