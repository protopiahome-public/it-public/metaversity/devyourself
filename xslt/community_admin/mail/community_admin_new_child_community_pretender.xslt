<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="xml" encoding="UTF-8"/>
	<xsl:variable name="request" select="/root/request"/>
	<xsl:variable name="prefix" select="$request/@prefix"/>
	<xsl:variable name="domain_prefix" select="$request/@domain_prefix"/>
	<xsl:variable name="current_project" select="/root/project_short[1]"/>
	<xsl:variable name="current_community" select="/root/community_short[1]"/>
	<xsl:variable name="pretender_community" select="/root/community_short[2]"/>
	<xsl:template match="/root">
		<html>
			<head>
				<title>
					<xsl:text>Новое подсообщество в сообществе </xsl:text>
					<xsl:value-of select="$current_community/@title"/>
				</title>
			</head>
			<body>
				<style type="text/css">
					body {padding: 0; margin: 0;}
					body, td {font-family: Arial, Tahoma, Verdana, serif; font-size: 10pt; line-height: 13pt; background: #fff; color: #000; text-align: left;}
					p {margin: 0 0 5pt; padding: 0;}
					img {border: 0; vertical-align: middle;}
					ul, ol, li {margin: 0; padding: 0;}
					ul, ol {margin-bottom: 5pt;}
					li {margin: 0 0 5pt 20pt;}
					ol li {list-style-type: decimal;}
					ul li {list-style-type: square;}
					a {color: #463d37; text-decoration: underline;}
					.main {padding: 5pt 5pt 0;}
					.bot {color: #666; margin-bottom: 12pt; margin-top: 15pt;}
				</style>
				<div class="main">
					<p>
						<xsl:text>В сообщество </xsl:text>
						<a href="{$domain_prefix}{$current_community/@url}">
							<xsl:value-of select="$current_community/@title"/>
						</a>
						<xsl:text> проекта </xsl:text>
						<a href="{$domain_prefix}{$current_project/@url}">
							<xsl:value-of select="$current_project/@title"/>
						</a>
						<xsl:text> отправлен запрос на присоединение от сообщества </xsl:text>
						<a href="{$domain_prefix}{$pretender_community/@url}">
							<xsl:value-of select="$pretender_community/@title"/>
						</a>
						<xsl:text>.</xsl:text>
					</p>
					<p>Данное сообщение отправлено Вам потому, что Вы являетесь администратором сообщества, в которое подан запрос.</p>
					<p>
						<xsl:text>Сделать это сообщество дочерним или отклонить заявку можно на странице &#171;</xsl:text>
						<a href="{$domain_prefix}{$current_community/@url}admin/child-communities-pretenders/">Модерация дочерних сообществ</a>
						<xsl:text>&#187;.</xsl:text>
					</p>
					<div class="bot">
						<xsl:text>Отписаться от уведомлений можно на </xsl:text>
						<a href="{$prefix}/settings/#notifications">странице настроек</a>
						<xsl:text> (&#171;Оповещения&#187;)</xsl:text>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
