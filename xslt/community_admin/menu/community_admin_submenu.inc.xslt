<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_community_admin_submenu">
		<xsl:param name="base_url"/>
		<div class="widget-wrap">
			<div class="widget widget-menu">
				<div class="items-">
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'general'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'general' and $admin_section_main_page">Основные свойства</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}">Основные свойства</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'access'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'access' and $admin_section_main_page">Управление доступом</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}access/">Управление доступом</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'design'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'design' and $admin_section_main_page">Оформление</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}design/">Оформление</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'sections'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'sections' and $admin_section_main_page">Разделы</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}sections/">Разделы</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'feeds'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'feeds' and $admin_section_main_page">Ленты</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}feeds/">Ленты</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div class="menu-item- menu-item-expander-">
						<div>
							<span class="toggle" jq-toggle="jq-menu-child-communities" jq-toggle-group="jq-menu-subblocks">Иерархия</span>
						</div>
					</div>
					<div id="jq-menu-child-communities">
						<xsl:attribute name="class">
							<xsl:text>jq-menu-subblocks</xsl:text>
							<xsl:if test="not($admin_section = 'child-communities-pretenders' or $admin_section = 'child-communities' or $admin_section = 'parent-community')"> dn</xsl:if>
						</xsl:attribute>
						<xsl:if test="$current_community/@allow_child_communities = 'user_premoderation'">
							<div>
								<xsl:attribute name="class">
									<xsl:text>menu-item- menu-item-level2- </xsl:text>
									<xsl:if test="$admin_section = 'child-communities-pretenders'">menu-item-selected- </xsl:if>
								</xsl:attribute>
								<div>
									<xsl:choose>
										<xsl:when test="$admin_section = 'child-communities-pretenders' and $admin_section_main_page">Модерация дочек</xsl:when>
										<xsl:otherwise>
											<a href="{$base_url}child-communities-pretenders/">Модерация дочек</a>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
						</xsl:if>
						<div>
							<xsl:attribute name="class">
								<xsl:text>menu-item- menu-item-level2- </xsl:text>
								<xsl:if test="$admin_section = 'child-communities'">menu-item-selected- </xsl:if>
							</xsl:attribute>
							<div>
								<xsl:choose>
									<xsl:when test="$admin_section = 'child-communities' and $admin_section_main_page">Дочерние сообщества</xsl:when>
									<xsl:otherwise>
										<a href="{$base_url}child-communities/">Дочерние сообщества</a>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
						<div>
							<xsl:attribute name="class">
								<xsl:text>menu-item- menu-item-level2- </xsl:text>
								<xsl:if test="$admin_section = 'parent-community'">menu-item-selected- </xsl:if>
							</xsl:attribute>
							<div>
								<xsl:choose>
									<xsl:when test="$admin_section = 'parent-community' and $admin_section_main_page">Родитель</xsl:when>
									<xsl:otherwise>
										<a href="{$base_url}parent-community/">Родитель</a>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
					</div>
					<div class="menu-item- menu-item-expander-">
						<div>
							<span class="toggle" jq-toggle="jq-menu-users" jq-toggle-group="jq-menu-subblocks">Люди и их права</span>
						</div>
					</div>
					<div id="jq-menu-users">
						<xsl:attribute name="class">
							<xsl:text>jq-menu-subblocks</xsl:text>
							<xsl:if test="not($admin_section = 'pretenders' or $admin_section = 'members' or $admin_section = 'admins')"> dn</xsl:if>
						</xsl:attribute>
						<xsl:if test="$current_community/@join_premoderation = 1">
							<div>
								<xsl:attribute name="class">
									<xsl:text>menu-item- menu-item-level2- </xsl:text>
									<xsl:if test="$admin_section = 'pretenders'">menu-item-selected- </xsl:if>
								</xsl:attribute>
								<div>
									<xsl:choose>
										<xsl:when test="$admin_section = 'pretenders' and $admin_section_main_page">Модерация участников</xsl:when>
										<xsl:otherwise>
											<a href="{$base_url}pretenders/">Модерация участников</a>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
						</xsl:if>
						<div>
							<xsl:attribute name="class">
								<xsl:text>menu-item- menu-item-level2- </xsl:text>
								<xsl:if test="$admin_section = 'members'">menu-item-selected- </xsl:if>
							</xsl:attribute>
							<div>
								<xsl:choose>
									<xsl:when test="$admin_section = 'members' and $admin_section_main_page">Участники</xsl:when>
									<xsl:otherwise>
										<a href="{$base_url}members/">Участники</a>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
						<div>
							<xsl:attribute name="class">
								<xsl:text>menu-item- menu-item-level2- </xsl:text>
								<xsl:if test="$admin_section = 'admins'">menu-item-selected- </xsl:if>
							</xsl:attribute>
							<div>
								<xsl:choose>
									<xsl:when test="$admin_section = 'admins' and $admin_section_main_page">Администраторы</xsl:when>
									<xsl:otherwise>
										<a href="{$base_url}admins/">Администраторы</a>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
					</div>
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'delete'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'delete' and $admin_section_main_page">Удаление сообщества</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}delete/">Удаление сообщества</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div class="clear"/>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
