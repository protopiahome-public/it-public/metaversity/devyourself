<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../community/head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="menu/community_admin_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'admin'"/>
	<xsl:variable name="community_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'sections'"/>
	<xsl:variable name="admin_section_main_page" select="true()"/>
	<xsl:variable name="communities_url" select="concat($current_project/@url, 'co/')"/>
	<xsl:variable name="module_url" select="concat($current_community/@url, 'admin/sections/')"/>
	<xsl:template match="community_admin_sections">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Настройка сообщества</h2>
							<div class="content">
								<h3>Разделы</h3>
								<xsl:call-template name="_draw_list"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_community_admin_submenu">
								<xsl:with-param name="base_url" select="concat($current_community/@url, 'admin/')"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<div class="ico-links-small">
			<a class="add-" href="{$module_url}add/">
				<span>добавить раздел</span>
			</a>
		</div>
		<div id="jq-sort" class="sort-list">
			<xsl:choose>
				<xsl:when test="$pass_info/info[@name = 'action'] = 'dt_add' and $pass_info/info[@name = 'SAVED']">
					<div class="info">
						<span>Добавлен новый раздел</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/info[@name = 'DELETED']">
					<div class="info">
						<span>Раздел удален</span>
					</div>
				</xsl:when>
			</xsl:choose>
			<xsl:for-each select="section">
				<div id="i-{@id}" class="item-">
					<span class="sort-handler"/>
					<span class="title-">
						<a target="_blank" href="{$current_community/@url}{@name}/">
							<xsl:value-of select="@title"/>
						</a>
					</span>
					<span class="icons- icons-small">
						<a class="edit- gray-" href="{$module_url}{@id}/" title="Редактировать"/>
						<a class="delete- gray-" href="{$module_url}{@id}/delete/" title="Удалить"/>
					</span>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/sort.js"/>
		<script type="text/javascript">
			var post_vars = {
				project_id : '<xsl:value-of select="$current_project/@id"/>',
				community_id : '<xsl:value-of select="$current_community/@id"/>'
			};
			var sort_ajax_ctrl = 'community_admin_sections_sort';
		</script>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Разделы &#8212; </xsl:text>
		<xsl:value-of select="$current_community/@title"/>
	</xsl:template>
</xsl:stylesheet>
