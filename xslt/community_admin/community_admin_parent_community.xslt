<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../community/head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="menu/community_admin_submenu.inc.xslt"/>
	<xsl:include href="../_site/include/admin_members.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'admin'"/>
	<xsl:variable name="community_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'parent-community'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_community/@url, 'admin/parent-community/')"/>
	<xsl:template match="community_admin_parent_community">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Настройка сообщества</h2>
							<div class="content">
								<h3>Родительское сообщество</h3>
								<xsl:call-template name="_draw_сhange_form"/>
								<xsl:if test="$parent_community">
									<xsl:call-template name="_draw_disconnect_form"/>
								</xsl:if>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_community_admin_submenu">
								<xsl:with-param name="base_url" select="concat($current_community/@url, 'admin/')"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_сhange_form">
		<div class="box">
			<xsl:choose>
				<xsl:when test="$parent_community and not($current_community/@parent_approved = 1)">
					<p>
						<xsl:text>Данное сообщество </xsl:text>
						<strong>претендует</strong>
						<xsl:text> на то, чтобы стать подсообществом сообщества </xsl:text>
						<a href="{$parent_community/@url}">
							<xsl:value-of select="$parent_community/@title"/>
						</a>
						<xsl:text>.</xsl:text>
					</p>
					<p>
						<strong>Администрация родительского сообщества пока ещё не одобрила заявку.</strong>
					</p>
				</xsl:when>
				<xsl:when test="$parent_community">
					<p>
						<xsl:text>Данное сообщество имеет родительское сообщество </xsl:text>
						<a href="{$parent_community/@url}">
							<xsl:value-of select="$parent_community/@title"/>
						</a>
						<xsl:text>.</xsl:text>
					</p>
				</xsl:when>
				<xsl:otherwise>
					<p>Данное сообщество находится в корне проекта. Оно не имеет родительского сообщества.</p>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<fieldset>
			<legend>
				<xsl:choose>
					<xsl:when test="$parent_community">Сменить</xsl:when>
					<xsl:otherwise>Указать</xsl:otherwise>
				</xsl:choose>
				<xsl:text> родительское сообщество</xsl:text>
			</legend>
			<div class="field">
				<form action="{$save_prefix}/community_admin_parent_community/" method="post">
					<label class="title-">
						<xsl:text>Введите URL родительского сообщества (или любой его страницы)</xsl:text>
						<xsl:text>:</xsl:text>
						<span class="star">
							<xsl:text>&#160;*</xsl:text>
						</span>
					</label>
					<div class="input-">
						<input class="input-text" type="text" name="parent_community_url" value="">
							<xsl:if test="$pass_info/vars/var[@name = 'parent_community_url']">
								<xsl:attribute name="value">
									<xsl:value-of select="$pass_info/vars/var[@name = 'parent_community_url']"/>
								</xsl:attribute>
							</xsl:if>
						</input>
						<xsl:choose>
							<xsl:when test="$pass_info/error[@name = 'WRONG_PARENT_COMMUNITY_URL']">
								<div class="error-">
									<span>Некорректный адрес сообщества</span>
								</div>
							</xsl:when>
							<xsl:when test="$pass_info/error[@name = 'PARENT_SELF']">
								<div class="error-">
									<span>Указанное сообщество не может быть родительским: Вы пытаетесь связать ряд сообществ в &#171;кольцо&#187;</span>
								</div>
							</xsl:when>
							<xsl:when test="$pass_info/error[@name = 'SUBCOMMUNITIES_DISABLED']">
								<div class="error-">
									<span>Администрация указанного сообщества не разрешила создание дочерних сообществ</span>
								</div>
							</xsl:when>
							<xsl:when test="$pass_info/error[@name = 'ALREADY_PARENT']">
								<div class="error-">
									<span>Сообщество уже является родительским</span>
								</div>
							</xsl:when>
						</xsl:choose>
						<div class="comment-">
							<xsl:text>Например: </xsl:text>
							<xsl:value-of select="$request/@domain_prefix"/>
							<xsl:value-of select="$current_project/@url"/>
							<xsl:text>co/somecommunity/</xsl:text>
						</div>
						<input type="hidden" name="project_id" value="{$current_project/@id}"/>
						<input type="hidden" name="community_id" value="{$current_community/@id}"/>
					</div>
					<button class="button button-small" style="margin-top: 10px;">Сохранить</button>
				</form>
			</div>
		</fieldset>
	</xsl:template>
	<xsl:template name="_draw_disconnect_form">
		<fieldset>
			<legend>Удалить привязку к родителю</legend>
			<div class="field">
				<form action="{$save_prefix}/community_admin_parent_community/" method="post">
					<input type="hidden" name="project_id" value="{$current_project/@id}"/>
					<input type="hidden" name="community_id" value="{$current_community/@id}"/>
					<input type="hidden" name="disconnect" value="1"/>
					<p>Здесь можно удалить привязку к родительскому сообществу. После этого данное сообщество будет находиться в корне проекта.</p>
					<button class="button button-small">Удалить привязку к родителю</button>
				</form>
			</div>
		</fieldset>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/community_admin_child_communities.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Родительское сообщество &#8212; </xsl:text>
		<xsl:value-of select="$current_community/@title"/>
	</xsl:template>
</xsl:stylesheet>
