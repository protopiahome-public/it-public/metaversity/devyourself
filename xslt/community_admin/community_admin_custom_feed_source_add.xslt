<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../community/head2/community_head2.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="menu/community_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/community_admin_custom_feed_submenu.inc.xslt"/>
	<xsl:include href="include/admin_custom_feed_source_add.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="community_section" select="'admin'"/>
	<xsl:variable name="community_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'feeds'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($current_community/@url, 'admin/feeds/', $current_feed/@id, '/sources/add/')"/>
	<xsl:template match="community_admin_custom_feed_source_add">
		<div class="color-{$current_community_color_scheme}">
			<xsl:call-template name="draw_communities_submenu"/>
			<xsl:call-template name="draw_community_head2"/>
			<div class="columns-wrap columns-wrap-community">
				<xsl:call-template name="draw_community_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Настройка сообщества</h2>
							<div class="content">
								<h3 class="pre">
									<a href="{$feeds_url}">Ленты</a>
								</h3>
								<h4>
										<xsl:value-of select="$current_feed/@title"/>
								</h4>
								<xsl:call-template name="draw_community_admin_custom_feed_submenu"/>
								<xsl:call-template name="draw_admin_custom_feed_source_add">
									<xsl:with-param name="save_ctrl" select="'community_admin_custom_feed_sources'"/>
									<xsl:with-param name="aux_params">
										<input type="hidden" name="project_id" value="{$current_project/@id}"/>
										<input type="hidden" name="community_id" value="{$current_community/@id}"/>
									</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_community_admin_submenu">
								<xsl:with-param name="base_url" select="concat($current_community/@url, 'admin/')"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Добавить источник в ленту &#171;</xsl:text>
		<xsl:value-of select="$current_feed/@title"/>
		<xsl:text>&#187; &#8212; </xsl:text>
		<xsl:value-of select="$current_community/@title"/>
	</xsl:template>
</xsl:stylesheet>
