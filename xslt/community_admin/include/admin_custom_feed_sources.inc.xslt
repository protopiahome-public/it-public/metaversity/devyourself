<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_admin_custom_feed_sources">
		<xsl:param name="save_ctrl"/>
		<xsl:param name="aux_params"/>
		<div class="clear"/>
		<xsl:if test="not(feed_source)">
			<p>Ни одного источника пока нет.</p>
		</xsl:if>
		<div class="ico-links-small">
			<a class="add-" href="{$module_url}add/">
				<span>добавить источник</span>
			</a>
		</div>
		<div class="sort-list">
			<xsl:for-each select="feed_source">
				<xsl:variable name="current_item" select="."/>
				<xsl:variable name="current_item_community" select="/root/community_short[@id = $current_item/@community_id]"/>
				<xsl:variable name="current_item_section" select="/root/section_full[@id = $current_item/@section_id]"/>
				<div class="item-">
					<span class="title-">
						<a href="{$current_item_community/@url}">
							<xsl:value-of select="$current_item_community/@title"/>
						</a>
						<xsl:if test="$current_item_section">
							<span class="section-sep">&#160;</span>
							<a href="{$current_item_section/@url}">
								<xsl:value-of select="$current_item_section/@title"/>
							</a>
						</xsl:if>
					</span>
					<span class="icons- icons-small">
						<form action="{$save_prefix}/{$save_ctrl}/" method="post">
							<a class="jq-delete-button delete- gray-" href="javascript:void(0);" title="Удалить из ленты"/>
							<input type="hidden" name="action" value="delete"/>
							<input type="hidden" name="feed_id" value="{$current_feed/@id}"/>
							<input type="hidden" name="delete_item_id" value="{$current_item/@id}"/>
							<input type="hidden" name="retpath" value="{$module_url}"/>
							<xsl:copy-of select="$aux_params"/>
						</form>
					</span>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>
</xsl:stylesheet>
