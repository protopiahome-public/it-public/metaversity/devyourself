<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_admin_custom_feed_source_add">
		<xsl:param name="save_ctrl"/>
		<xsl:param name="aux_params"/>
		<fieldset>
			<legend>Добавить источник</legend>
			<div class="field">
				<form action="{$save_prefix}/{$save_ctrl}/" method="post">
					<input type="hidden" name="action" value="add"/>
					<input type="hidden" name="feed_id" value="{$current_feed/@id}"/>
					<input type="hidden" name="retpath" value="{$feed_edit_url}sources/"/>
					<label class="title-">
						<xsl:text>Введите URL сообщества или его раздела</xsl:text>
						<xsl:text>:</xsl:text>
						<span class="star">
							<xsl:text>&#160;*</xsl:text>
						</span>
					</label>
					<div class="input-">
						<input class="input-text" type="text" name="add_item_url" value="">
							<xsl:if test="$pass_info/vars/var[@name = 'add_item_url']">
								<xsl:attribute name="value">
									<xsl:value-of select="$pass_info/vars/var[@name = 'add_item_url']"/>
								</xsl:attribute>
							</xsl:if>
						</input>
						<xsl:if test="$pass_info/error[@name = 'wrong_add_item_url']">
							<div class="error-">
								<span>Неверный адрес сообщества или раздела</span>
							</div>
						</xsl:if>
						<xsl:if test="$pass_info/error[@name = 'add_item_already_exists']">
							<div class="error-">
								<span>Источник уже добавлен</span>
							</div>
						</xsl:if>
						<xsl:copy-of select="$aux_params"/>
					</div>
					<div class="comment-">
						<xsl:text>Например: </xsl:text>
						<xsl:value-of select="$request/@domain_prefix"/>
						<xsl:value-of select="$current_project/@url"/>
						<xsl:text>co/somecommunity/. Разрешено ссылаться только на сообщества </xsl:text>
						<strong>текущего</strong>
						<xsl:text> проекта.</xsl:text>
					</div>
					<button class="button button-small" style="margin-top: 10px;">Добавить</button>
				</form>
			</div>
		</fieldset>
	</xsl:template>
</xsl:stylesheet>
