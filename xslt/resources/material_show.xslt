<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/material_submenu.inc.xslt"/>
	<xsl:include href="include/resource_rate.inc.xslt"/>
	<xsl:include href="include/material_head.inc.xslt"/>
	<xsl:include href="../block_sets/include/block_set_show.inc.xslt"/>
	<xsl:include href="../comments/include/comments.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'materials'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="material_id" select="/root/material_show/@id"/>
	<xsl:variable name="material_title" select="/root/material_show/@title"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'materials/', $material_id, '/')"/>
	<xsl:variable name="material_data" select="/root/material_show"/>
	<xsl:variable name="material_status" select="/root/material_show"/>
	<xsl:template match="material_show">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Материалы</h2>
							<div class="content">
								<xsl:call-template name="_draw_material"/>
								<xsl:for-each select="/root/comments">
									<xsl:call-template name="draw_comments">
										<xsl:with-param name="can_moderate" select="$project_access/@can_moderate_materials = 1"/>
										<xsl:with-param name="can_comment" select="$project_access/@can_comment_materials = 1"/>
										<xsl:with-param name="can_comment_html">
											<div class="box" style="margin-top: 8px;">
												<xsl:call-template name="draw_access">
													<xsl:with-param name="for" select="'Для комментирования материала'"/>
													<xsl:with-param name="status" select="$project_access/@access_materials_comment"/>
													<xsl:with-param name="entity_genitiv" select="'проекта'"/>
													<xsl:with-param name="join_premoderation" select="$project_access/@join_premoderation = 1"/>
												</xsl:call-template>
											</div>
										</xsl:with-param>
										<xsl:with-param name="title_akk" select="'материал'"/>
									</xsl:call-template>
								</xsl:for-each>
								<xsl:call-template name="_draw_competences"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_material">
		<xsl:call-template name="draw_material_head">
			<xsl:with-param name="show_link" select="false()"/>
			<xsl:with-param name="show_announce" select="false()"/>
			<xsl:with-param name="add_bottom_margin" select="false()"/>
			<xsl:with-param name="material_data" select="$material_data"/>
			<xsl:with-param name="material_status" select="$material_status"/>
			<xsl:with-param name="show_edit_link" select="$project_access/@can_moderate_materials = 1"/>
		</xsl:call-template>
		<div class="resource-body">
			<div class="descr-">
				<xsl:for-each select="/root/block_set">
					<xsl:call-template name="draw_block_set_show">
						<xsl:with-param name="object_url" select="concat($current_project/@url, 'materials/', $material_id, '/')"/>
					</xsl:call-template>
				</xsl:for-each>
			</div>
			<xsl:for-each select="$material_status">
				<xsl:if test="@before_status">
					<div class="resource-head">
						<div class="col-main- .statuses-">
							<xsl:call-template name="draw_material_status">
								<xsl:with-param name="footer" select="true()"/>
							</xsl:call-template>
						</div>
					</div>
				</xsl:if>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template name="_draw_competences">
		<div>
			<a name="competences"/>
			<xsl:call-template name="draw_resource_rate_competences"/>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			var resource_name = 'material';
			var comment_add_ctrl = 'material_comment_add';
			var comment_delete_ctrl = 'material_comment_delete';
			var comments_subscribe_ctrl = 'material_comments_subscribe';
			var comments_post_vars = {
				project_id : '<xsl:value-of select="$current_project/@id"/>'
			};
			var can_moderate_comments = <xsl:value-of select="$project_access/@can_moderate_materials"/>;
		</script>
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.scrollTo.js"/>
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/resource_ignore.js"/>
		<script type="text/javascript" src="{$prefix}/js/resource_status.js"/>
		<script type="text/javascript" src="{$prefix}/js/comments.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$material_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		
	</xsl:template>
</xsl:stylesheet>
