<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="include/resources_import_project_add.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'materials'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'materials/import/add/')"/>
	<xsl:template match="materials_import_project_add">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Материалы</h2>
							<div class="content">
								<h3>Импорт материалов: добавление проекта</h3>
								<xsl:call-template name="draw_resources_import_project_add_form">
									<xsl:with-param name="resource_name" select="'material'"/>
								</xsl:call-template>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery.hotkeys.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/resource_import.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Импорт материалов &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
