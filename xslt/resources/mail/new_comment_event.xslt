<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="xml" encoding="UTF-8"/>
	<xsl:variable name="request" select="/root/request"/>
	<xsl:variable name="prefix" select="$request/@prefix"/>
	<xsl:variable name="domain_prefix" select="$request/@domain_prefix"/>
	<xsl:variable name="user" select="/root/user"/>
	<xsl:variable name="comment" select="/root/comments[1]/comment"/>
	<xsl:variable name="comment_user" select="/root/user_short[@id = /root/comments[1]/comment/@author_user_id]"/>
	<xsl:variable name="parent_comment" select="/root/comments[2]/comment"/>
	<xsl:variable name="parent_comment_user" select="/root/user_short[@id = /root/comments[2]/comment/@author_user_id]"/>
	<xsl:template match="/root">
		<html>
			<head>
				<title>
					<xsl:choose>
						<xsl:when test="$parent_comment">Вам ответили в событии</xsl:when>
						<xsl:otherwise>Новый комментарий к событию</xsl:otherwise>
					</xsl:choose>
					<xsl:text> &#171;</xsl:text>
					<xsl:value-of select="event_short/@title"/>
					<xsl:text>&#187;</xsl:text>
				</title>
			</head>
			<body>
				<style type="text/css">
					body {padding: 0; margin: 0;}
					body, td {font-family: Arial, Tahoma, Verdana, serif; font-size: 10pt; line-height: 13pt; background: #fff; color: #000; text-align: left;}
					p {margin: 0 0 5pt; padding: 0;}
					img {border: 0; vertical-align: middle;}
					ul, ol, li {margin: 0; padding: 0;}
					ul, ol {margin-bottom: 5pt;}
					li {margin: 0 0 5pt 20pt;}
					ol li {list-style-type: decimal;}
					ul li {list-style-type: square;}
					a {color: #463d37; text-decoration: underline;}
					.b {padding: 0 5pt;}
					.h, .hh, .bot {background: #ddd; padding: 5pt;}
					.h {font-size: 12pt; line-height: 15pt; margin-bottom: 12pt;}
					.hh {font-size: 12pt; line-height: 15pt; margin-bottom: 12pt; margin-top: 15pt;}
					.bot {color: #666; margin-bottom: 12pt; margin-top: 15pt;}
				</style>
				<div class="b h">
					<xsl:text>К событию </xsl:text>
					<a href="{$domain_prefix}{event_short/@url}">
						<strong>
							<xsl:value-of select="event_short/@title"/>
						</strong>
					</a>
					<xsl:text> добавился </xsl:text>
					<xsl:choose>
						<xsl:when test="$parent_comment">
							<a href="{$domain_prefix}{event_short/@url}#comment-{$comment/@id}">ответ</a>
							<xsl:text> на Ваш </xsl:text>
							<a href="{$domain_prefix}{event_short/@url}#comment-{$parent_comment/@id}">комментарий</a>
						</xsl:when>
						<xsl:otherwise>
							<a href="{$domain_prefix}{event_short/@url}#comment-{$comment/@id}">новый комментарий</a>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text> от пользователя </xsl:text>
					<a href="{$domain_prefix}{project_short/@url}users/{$comment_user/@login}/">
						<xsl:value-of select="$comment_user/@login"/>
					</a>
					<xsl:text> (</xsl:text>
					<xsl:value-of select="$comment_user/@visible_name"/>
					<xsl:text>):</xsl:text>
				</div>
				<div class="b">
					<xsl:copy-of select="$comment/html/div"/>
					<p>
						<a href="{$domain_prefix}{event_short/@url}#comment-{$comment/@id}">Перейти к комментарию</a>
					</p>
				</div>
				<xsl:if test="$parent_comment">
					<div class="b hh">
						<xsl:choose>
							<xsl:when test="$parent_comment">
								<xsl:text>Ваш </xsl:text>
								<a href="{$domain_prefix}{event_short/@url}#comment-{$parent_comment/@id}">комментарий</a>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Это ответ на </xsl:text>
								<a href="{$domain_prefix}{event_short/@url}#comment-{$parent_comment/@id}">комментарий</a>
								<xsl:text> от пользователя </xsl:text>
								<a href="{$domain_prefix}{project_short/@url}users/{$parent_comment_user/@login}/">
									<xsl:value-of select="$parent_comment_user/@login"/>
								</a>
								<xsl:text> (</xsl:text>
								<xsl:value-of select="$parent_comment_user/@visible_name"/>
								<xsl:text>):</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</div>
					<div class="b">
						<xsl:copy-of select="$parent_comment/html/div"/>
						<p>
							<a href="{$domain_prefix}{event_short/@url}#comment-{$parent_comment/@id}">Перейти к комментарию</a>
						</p>
					</div>
				</xsl:if>
				<div class="b bot">
					<xsl:choose>
						<xsl:when test="$parent_comment">
							<xsl:text>Отписаться от ответов на Ваши комментарии можно на </xsl:text>
							<a href="{$prefix}/settings/#notifications">странице настроек</a>
							<xsl:text> (&#171;Оповещения&#187;)</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>Отписаться от новых комментариев можно на </xsl:text>
							<a href="{$domain_prefix}{event_short/@url}#comments">странице события</a>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>.</xsl:text>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
