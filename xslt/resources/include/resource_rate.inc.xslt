<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_resource_rate_vector_invite">
		<xsl:if test="@vector_exists = 0">
			<div class="box box-warning">
				<p style="padding: 8px 0;">
					<xsl:choose>
						<xsl:when test="$user/@id">Для отображения личных рекомендаций пройдите, пожалуйста, </xsl:when>
						<xsl:otherwise>Для отображения личных рекомендаций <a href="{$reg_url}">зарегистрируйтесь</a> и пройдите </xsl:otherwise>
					</xsl:choose>
					<a href="{$current_project/@url}vector/">Вектор</a>
					<xsl:text>.</xsl:text>
				</p>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="draw_resource_rate_match">
		<xsl:param name="draw_table_header"/>
		<xsl:param name="resource_url"/>
		<xsl:param name="can_edit_rate"/>
		<div class="sep-">
			<xsl:if test="$current_project/@vector_is_on = 1">
				<xsl:if test="$draw_table_header and ../@vector_exists = 1">
					<div class="header-">Полезность для вас</div>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="/root/rate[@id = current()/@rate_id]/competence">
						<xsl:if test="@match">
							<div class="resource-match" title="Полезность для вас">
								<a href="{$resource_url}#competences" jq-toggle="jq-resource-matche-details-{@id}">
									<xsl:attribute name="class">
										<xsl:text>toggle</xsl:text>
										<xsl:if test="@match = 10"> full-</xsl:if>
									</xsl:attribute>
									<span class="fill-" style="width: {@match * 10}px;">
										<!--<span>
										<xsl:value-of select="@match"/>
									</span>-->
									</span>
								</a>
							</div>
							<div class="dd- dn" id="jq-resource-matche-details-{@id}">
								<div class="box-light">
									<xsl:call-template name="draw_resource_rate_competences">
										<xsl:with-param name="rate" select="/root/rate[@id = current()/@rate_id]"/>
									</xsl:call-template>
								</div>
							</div>
						</xsl:if>
					</xsl:when>
					<xsl:when test="$can_edit_rate">
						<div class="fill-rate-">
							<xsl:text>не&#160;заполнены </xsl:text>
							<a href="{$resource_url}edit/rate/">компетенции</a>
						</div>
					</xsl:when>
				</xsl:choose>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template name="draw_resource_rate_competences">
		<xsl:param name="rate" select="/root/rate[1]"/>
		<div class="resource-rate">
			<h3 class="down">Развиваемые компетенции</h3>
			<xsl:if test="not($rate/competence)">
				<p>Компетенции не указаны.</p>
			</xsl:if>
			<xsl:if test="$rate/competence[@focus = 1]">
				<div class="subhead-">Компетенции из <a href="{$current_project/@url}vector/my/#focus">вашего фокуса</a>:</div>
				<xsl:call-template name="_resource_rate_draw_competences">
					<xsl:with-param name="rate" select="$rate"/>
					<xsl:with-param name="competences" select="$rate/competence[@focus = 1]"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="$rate/competence[@future = 1 and not(@focus = 1)]">
				<div class="subhead-">Компетенции из <a href="{$current_project/@url}vector/my/#future">вашего профиля &#171;Я в будущем&#187;</a>:</div>
				<xsl:call-template name="_resource_rate_draw_competences">
					<xsl:with-param name="rate" select="$rate"/>
					<xsl:with-param name="competences" select="$rate/competence[@future = 1 and not(@focus = 1)]"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="$rate/competence[@future = 1 or @focus = 1] and $rate/competence[not(@focus = 1) and not(@future = 1)]">
				<div class="subhead-">Другие компетенции</div>
			</xsl:if>
			<xsl:call-template name="_resource_rate_draw_competences">
				<xsl:with-param name="rate" select="$rate"/>
				<xsl:with-param name="competences" select="$rate/competence[not(@focus = 1) and not(@future = 1)]"/>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="_resource_rate_draw_competences">
		<xsl:param name="rate"/>
		<xsl:param name="competences"/>
		<ul>
			<xsl:for-each select="$competences">
				<li>
					<xsl:call-template name="draw_competence_title"/>
					<span class="hl">
						<xsl:text> (</xsl:text>
						<xsl:value-of select="$rate/options/option[@mark = current()/@mark]/@mark_title"/>
						<xsl:text>)</xsl:text>
					</span>
				</li>
			</xsl:for-each>
		</ul>
	</xsl:template>
</xsl:stylesheet>
