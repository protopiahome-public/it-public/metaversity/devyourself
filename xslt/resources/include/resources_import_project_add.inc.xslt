<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_resources_import_project_add_form">
		<xsl:param name="resource_name" />
		<fieldset class="fake-">
			<div class="field">
				<form action="{$save_prefix}/{$resource_name}s_import_projects/" method="post">
					<label class="title-" for="f-{@name}">
						<xsl:text>Введите URL проекта (или любой его страницы)</xsl:text>
						<xsl:text>:</xsl:text>
						<span class="star">
							<xsl:text>&#160;*</xsl:text>
						</span>
					</label>
					<div class="input-">
						<input class="input-text" type="text" name="add_project_url" value="">
							<xsl:if test="@max_length != 0">
								<xsl:attribute name="maxlength">
									<xsl:value-of select="@max_length"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:if test="$pass_info/vars/var[@name = 'add_project_url']">
								<xsl:attribute name="value">
									<xsl:value-of select="$pass_info/vars/var[@name = 'add_project_url']"/>
								</xsl:attribute>
							</xsl:if>
						</input>
						<xsl:if test="$pass_info/error[@name = 'wrong_add_project_url']">
							<div class="error-">
								<span>Неверный адрес проекта</span>
							</div>
						</xsl:if>
						<xsl:if test="$pass_info/error[@name = 'add_itself']">
							<div class="error-">
								<span>Нельзя импортировать из самого себя</span>
							</div>
						</xsl:if>
						<input type="hidden" name="project_id" value="{$current_project/@id}"/>
						<input type="hidden" name="action" value="add"/>
						<input type="hidden" name="retpath" value="{$current_project/@url}{$resource_name}s/import/"/>
					</div>
					<xsl:if test="/root/project_short[1]">
						<div class="comment-">
							<xsl:text>Например: </xsl:text>
							<xsl:value-of select="$request/@domain_prefix"/>
							<xsl:value-of select="$current_project/@url"/>
						</div>
					</xsl:if>
					<button class="button button-small" style="margin-top: 10px;">Добавить</button>
				</form>
			</div>
		</fieldset>
	</xsl:template>
</xsl:stylesheet>
