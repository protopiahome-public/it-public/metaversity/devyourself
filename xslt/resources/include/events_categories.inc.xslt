<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format name="number" NaN="0"/>
	<xsl:template name="draw_events_categories">
		<xsl:param name="cats"/>
		<xsl:if test="$cats//category">
			<div class="widget-wrap">
				<div class="widget widget-categories">
					<div class="header- header-light-">
						<table cellspacing="0">
							<tr>
								<td>
									<h2 class="text-">Разделы</h2>
								</td>
							</tr>
						</table>
					</div>
					<div class="items-">
						<div>
							<xsl:attribute name="class">
								<xsl:text>item-outer- </xsl:text>
								<xsl:if test="not(/root/events/@cat_id)">item-outer-selected-</xsl:if>
							</xsl:attribute>
							<div class="item- level-1-">
								<xsl:choose>
									<xsl:when test="not(/root/events/@cat_id)">
										<strong>Все</strong>
									</xsl:when>
									<xsl:otherwise>
										<a>
											<xsl:attribute name="href">
												<xsl:call-template name="url_delete_param">
													<xsl:with-param name="url_base" select="$module_url"/>
													<xsl:with-param name="param_name" select="'cat'"/>
												</xsl:call-template>
											</xsl:attribute>
											<xsl:text>Все</xsl:text>
										</a>
									</xsl:otherwise>
								</xsl:choose>
								<span class="count-">
									<xsl:value-of select="categories/@count_total"/>
								</span>
							</div>
						</div>
						<xsl:for-each select="$cats//category">
							<div>
								<xsl:attribute name="class">
									<xsl:text>item-outer- </xsl:text>
									<xsl:if test="@id = /root/events/@cat_id">item-outer-selected-</xsl:if>
								</xsl:attribute>
								<div class="item- level-{@level}-">
									<xsl:choose>
										<xsl:when test="@id = /root/events/@cat_id">
											<strong>
												<xsl:value-of select="@title"/>
											</strong>
										</xsl:when>
										<xsl:otherwise>
											<a>
												<xsl:attribute name="href">
													<xsl:call-template name="url_replace_param">
														<xsl:with-param name="url_base" select="$module_url"/>
														<xsl:with-param name="param_name" select="'cat'"/>
														<xsl:with-param name="param_value" select="@id"/>
													</xsl:call-template>
												</xsl:attribute>
												<xsl:value-of select="@title"/>
											</a>
										</xsl:otherwise>
									</xsl:choose>
									<span class="count-">
										<xsl:value-of select="format-number(/root/events/categories/category[@id = current()/@id]/@count, 0, 'number')"/>
									</span>
								</div>
							</div>
						</xsl:for-each>
					</div>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
