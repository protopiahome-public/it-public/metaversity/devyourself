<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="is_project_day_list" select="not(not(/root/events))"/>
	<xsl:template name="events_day_list">
		<div class="content-inner-">
			<div class="jq-close close-"> </div>
			<div class="date-">
				<a>
					<xsl:attribute name="href">
						<xsl:choose>
							<xsl:when test="$is_project_day_list">
								<xsl:value-of select="concat(/root/project_short[@id = current()/@project_id]/@url, 'events/calendar/', @year, '/', @month, '/', @day, '/')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="concat($prefix, '/users/', $user/@login, '/events/calendar/', @year, '/', @month, '/', @day, '/')"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:value-of select="@day"/>
					<xsl:text> </xsl:text>
					<xsl:call-template name="get_month_name">
						<xsl:with-param name="month_number" select="@month"/>
						<xsl:with-param name="type" select="'with_day'"/>
					</xsl:call-template>
				</a>
			</div>
			<xsl:if test="$is_project_day_list and event">
				<xsl:for-each select="event">
					<xsl:variable name="current_event" select="."/>
					<xsl:variable name="current_event_position" select="position()"/>
					<xsl:for-each select="/root/event_short[@id = current()/@id][1]">
						<div class="event-">
							<xsl:if test="$current_event_position != 1">
								<hr/>
							</xsl:if>
							<div class="title-">
								<xsl:choose>
									<xsl:when test="$current_event/@reflex_id &gt; 0">
										<a href="{/root/project_short[@id = $current_event/@reflex_project_id]/@url}events/{$current_event/@reflex_id}/">
											<xsl:value-of select="@title"/>
										</a>
									</xsl:when>
									<xsl:otherwise>
										<a href="{/root/project_short[@id = $current_event/../@project_id]/@url}events/{@id}/">
											<xsl:value-of select="@title"/>
										</a>
									</xsl:otherwise>
								</xsl:choose>
								<span class="time-">
									<xsl:value-of select="substring(@start_time, 12, 5)"/>
								</span>
							</div>
							<xsl:variable name="match" select="/root/events/event[@id = current()/@id]/@match"/>
							<xsl:if test="$match &gt; 0">
								<div title="Полезность" class="resource-match">
									<a href="{/root/project_short[@id = $current_event/../@project_id]/@url}events/{@id}/#competences">
										<span class="fill-" style="width: {$match * 10}px;">
											<!--<span>
												<xsl:value-of select="$match"/>
											</span>-->
										</span>
									</a>
								</div>
							</xsl:if>
						</div>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="/root/user_events/event">
				<xsl:if test="$is_project_day_list">
					<hr/>
					<div class="my-events-head-">
						<a href="{$prefix}/users/{$user/@login}/events/">Ваш календарь</a>
					</div>
				</xsl:if>
				<xsl:for-each select="/root/user_events/event">
					<xsl:variable name="current_event" select="."/>
					<xsl:variable name="current_event_position" select="position()"/>
					<xsl:for-each select="/root/event_short[@id = current()/@id][1]">
						<div>
							<xsl:attribute name="class">
								<xsl:text>event-</xsl:text>
								<xsl:if test="$is_project_day_list"> event-no-hr-</xsl:if>
							</xsl:attribute>
							<xsl:if test="not($is_project_day_list) and $current_event_position != 1">
								<hr/>
							</xsl:if>
							<div class="title-">
								<a href="{/root/project_short[@id = $current_event/@project_id]/@url}events/{@id}/">
									<xsl:value-of select="@title"/>
								</a>
								<span class="time-">
									<xsl:value-of select="substring(@start_time, 12, 5)"/>
								</span>
							</div>
						</div>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
