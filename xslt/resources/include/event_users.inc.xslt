<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_event_users">
		<xsl:choose>
			<xsl:when test="@status = 'not_started'">
				<xsl:call-template name="_draw_event_users_block">
					<xsl:with-param name="title" select="'Точно пойдут'"/>
					<xsl:with-param name="users" select="/root/event_users/user[@before_status = 'go']"/>
				</xsl:call-template>
				<xsl:call-template name="_draw_event_users_block">
					<xsl:with-param name="title" select="'Возможно, пойдут'"/>
					<xsl:with-param name="users" select="/root/event_users/user[@before_status = 'maybe']"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="_draw_event_users_block">
					<xsl:with-param name="title" select="'Участвовали'"/>
					<xsl:with-param name="users" select="/root/event_users/user[@after_status = 'visited']"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="_draw_event_users_block">
		<xsl:param name="title"/>
		<xsl:param name="users"/>
		<div class="widget-wrap">
			<div class="widget widget-users">
				<div class="header-">
					<table cellspacing="0">
						<tr>
							<td>
								<h2 class="text-">
									<xsl:value-of select="$title"/>
									<xsl:if test="count($users) &gt; 0">
										<xsl:value-of select="concat(' (', count($users), ')')"/>
									</xsl:if>
								</h2>
							</td>
						</tr>
					</table>
				</div>
				<xsl:if test="not($users)">
					<div class="nobody-">Никого нет.</div>
				</xsl:if>
				<xsl:for-each select="$users">
					<xsl:variable name="context" select="."/>
					<xsl:variable name="position" select="position()"/>
					<xsl:for-each select="/root/user_short[@id = current()/@id]">
						<table>
							<xsl:attribute name="class">
								<xsl:text>item-</xsl:text>
								<xsl:if test="$position = 1"> item-first-</xsl:if>
							</xsl:attribute>
							<tr>
								<td class="user-">
									<table class="user-block">
										<tr>
											<td class="avatar-">
												<div class="sep-">
													<span>
														<a href="{$current_project/@url}users/{@login}/" title="">
															<img src="{photo_small/@url}" width="{photo_small/@width}" height="{photo_small/@height}" alt="{@login}" title=""/>
														</a>
													</span>
												</div>
											</td>
											<td class="text-">
												<a href="{$current_project/@url}users/{@login}/">
													<xsl:value-of select="@visible_name"/>
												</a>
												<b>
													<xsl:value-of select="@login"/>
												</b>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</xsl:for-each>
				</xsl:for-each>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
