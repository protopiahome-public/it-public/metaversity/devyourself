<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="material_status.inc.xslt"/>
	<xsl:template name="draw_material_head">
		<xsl:param name="draw_table_header" select="true()"/>
		<xsl:param name="show_checkbox" select="false()"/>
		<xsl:param name="show_link" select="true()"/>
		<xsl:param name="show_edit_link" select="true()"/>
		<xsl:param name="show_ignore_link" select="true()"/>
		<xsl:param name="show_categories" select="true()"/>
		<xsl:param name="show_announce" select="true()"/>
		<xsl:param name="add_bottom_margin" select="true()"/>
		<xsl:param name="remove_on_ignore" select="false()"/>
		<xsl:param name="show_comment_count" select="false()"/>
		<xsl:param name="material_status"/>
		<xsl:param name="project"/>
		<xsl:variable name="current_material_url">
			<xsl:choose>
				<xsl:when test="not($project)">
					<xsl:value-of select="concat($materials_url, @id, '/')"/>
				</xsl:when>
				<xsl:when test="$material_status and $material_status/@reflex_id &gt; 0">
					<xsl:value-of select="concat($project/@url, 'materials/', $material_status/@reflex_id, '/')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat($project/@url, 'materials/', @id, '/')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<table id="jq-resource-head-{@id}" cellpadding="0" cellspacing="0">
			<xsl:attribute name="class">
				<xsl:text>resource-head </xsl:text>
				<xsl:if test="$add_bottom_margin">resource-head-bottom-margin-</xsl:if>
			</xsl:attribute>
			<tr>
				<xsl:if test="$show_checkbox">
					<td class="col-checkbox-">
						<input type="checkbox" class="input-checkbox jq-resource-checkbox" name="select_material_ids[{@id}]" value="{@id}"/>
					</td>
				</xsl:if>
				<td class="col-main-">
					<div class="title-">
						<xsl:choose>
							<xsl:when test="$show_link">
								<xsl:if test="$project">
									<span class="project-">
										<a href="{$project/@url}">
											<xsl:value-of select="$project/@title"/>
										</a>
										<xsl:text> / </xsl:text>
									</span>
								</xsl:if>
								<a href="{$current_material_url}">
									<xsl:value-of select="@title"/>
								</a>
							</xsl:when>
							<xsl:otherwise>
								<h3>
									<xsl:value-of select="@title"/>
								</h3>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:if test="$user/@id">
							<span class="icons- icons-small">
								<xsl:if test="$show_ignore_link">
									<xsl:variable name="ignore">
										<xsl:choose>
											<xsl:when test="@ignore">
												<xsl:value-of select="@ignore"/>
											</xsl:when>
											<xsl:when test="/root/materials/user_ignore_list/material[@id = current()/@id]">1</xsl:when>
											<xsl:otherwise>0</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
									<span id="jq-resource-ignore-{@id}" data-id="{@id}" data-ignore="{$ignore}" title="Больше не показывать в моей ленте материалов">
										<xsl:attribute name="data-remove-on-ignore">
											<xsl:choose>
												<xsl:when test="$remove_on_ignore">1</xsl:when>
												<xsl:otherwise>0</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<xsl:attribute name="class">
											<xsl:text>jq-resource-ignore </xsl:text>
											<xsl:text>ignore- </xsl:text>
											<xsl:if test="$ignore = 0">gray-</xsl:if>
										</xsl:attribute>
									</span>
								</xsl:if>
								<xsl:if test="$show_edit_link">
									<a class="edit- gray-" href="{$materials_url}{@id}/edit/" title="Редактировать"/>
								</xsl:if>
							</span>
						</xsl:if>
					</div>
					<xsl:if test="$show_categories and categories//item">
						<div class="categories-">
							<span class="hl">
								<xsl:choose>
									<xsl:when test="count(categories//item) &gt; 1">
										<xsl:text>Разделы: </xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>Раздел: </xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</span>
							<xsl:for-each select="categories//item">
								<a>
									<xsl:attribute name="href">
										<xsl:call-template name="url_replace_param">
											<xsl:with-param name="url_base">
												<xsl:choose>
													<xsl:when test="/root/materials">
														<xsl:value-of select="$module_url"/>
													</xsl:when>
													<xsl:when test="$project">
														<xsl:value-of select="concat($project/@url, 'materials/', $module_postfix)"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$materials_url"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:with-param>
											<xsl:with-param name="param_name" select="'cat'"/>
											<xsl:with-param name="param_value" select="@id"/>
										</xsl:call-template>
									</xsl:attribute>
									<xsl:value-of select="@title"/>
								</a>
								<xsl:if test="position() != last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:for-each>
						</div>
					</xsl:if>
					<xsl:if test="$show_announce">
						<div class="announce-">
							<xsl:copy-of select="announce/div"/>
						</div>
					</xsl:if>
					<xsl:if test="@comment_count_calc &gt; 0 and $show_comment_count">
						<div class="comments-">
							<span>
								<a href="{$current_material_url}#comments">
									<xsl:value-of select="@comment_count_calc"/>
									<xsl:call-template name="count_case">
										<xsl:with-param name="number" select="@comment_count_calc"/>
										<xsl:with-param name="word_ns" select="' комменарий'"/>
										<xsl:with-param name="word_gs" select="' комментария'"/>
										<xsl:with-param name="word_ap" select="' комментариев'"/>
									</xsl:call-template>
								</a>
							</span>
						</div>
					</xsl:if>
					<xsl:if test="$material_status">
						<xsl:for-each select="$material_status">
							<xsl:if test="@before_status">
								<xsl:call-template name="draw_material_status"/>
							</xsl:if>
						</xsl:for-each>
					</xsl:if>
				</td>
				<td class="col-match-">
					<xsl:for-each select="/root/materials/material[@id = current()/@id] | /root/material_show">
						<xsl:call-template name="draw_resource_rate_match">
							<xsl:with-param name="draw_table_header" select="$draw_table_header"/>
							<xsl:with-param name="resource_url" select="concat($materials_url, @id, '/')"/>
							<xsl:with-param name="can_edit_rate" select="$show_edit_link"/>
						</xsl:call-template>
					</xsl:for-each>
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>
