<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_event_status">
		<xsl:param name="freeze" select="false()"/>
		<div>
			<xsl:attribute name="data-id">
				<xsl:choose>
					<xsl:when test="@reflex_id &gt; 0">
						<xsl:value-of select="@reflex_id"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@id"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="class">
				<xsl:text>statuses-</xsl:text>
				<xsl:if test="$freeze"> freeze</xsl:if>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@status = 'not_started' and @before_status = 'none'">
					<span class="button- check-dark- jq-button" data-status-type="before_status" data-status="go">
						<i>Точно пойду</i>
					</span>
					<span class="button- check-light- jq-button" data-status-type="before_status" data-status="maybe">
						<i>Возможно, пойду</i>
					</span>
				</xsl:when>
				<xsl:when test="@status = 'not_started' and @before_status = 'maybe'">
					<span class="button- check-dark- jq-button" data-status-type="before_status" data-status="go">
						<i>Точно пойду</i>
					</span>
					<span class="button- selected-light-" data-status-type="before_status" data-status="maybe">
						<i>Возможно, пойду</i>
					</span>
					<span class="button- jq-button" data-status-type="before_status" data-status="none">
						<i>Не пойду</i>
					</span>
				</xsl:when>
				<xsl:when test="@status = 'not_started' and @before_status = 'go'">
					<span class="button- selected-dark-" data-status-type="before_status" data-status="go">
						<i>Точно пойду</i>
					</span>
					<span class="button- check-light- jq-button" data-status-type="before_status" data-status="maybe">
						<i>Возможно, пойду</i>
					</span>
					<span class="button- jq-button" data-status-type="before_status" data-status="none">
						<i>Не пойду</i>
					</span>
				</xsl:when>
				<xsl:when test="(@status = 'finished' and @before_status = 'none' and (@after_status = 'unknown' or @after_status = 'missed')) or
								(@status = 'finished' and (@before_status = 'maybe' or @before_status = 'go') and @after_status = 'missed')">
					<span class="text-gray- no-left-pad-">
						<i>Событие закончено.</i>
					</span>
					<span class="dd-outer-">
						<div id="jq-event-{@id}-statuses-dd-REMOVE2" class="dd- dn">
							<span class="text-">
								<i>Изменить статус:</i>
							</span>
							<span class="button- check- jq-button" data-status-type="after_status" data-status="visited">
								<i>я участвовал</i>
							</span>
						</div>
						<span class="toggle button-" jq-toggle="jq-event-{@id}-statuses-dd-REMOVE2">
							<i>Вы не участвовали</i>
							<xsl:text>.</xsl:text>
						</span>
					</span>
				</xsl:when>
				<xsl:when test="@status = 'finished' and @after_status = 'visited'">
					<span class="text-gray- no-left-pad-">
						<i>Событие закончено.</i>
					</span>
					<span class="dd-outer-">
						<div id="jq-event-{@id}-statuses-dd-REMOVE1" class="dd- dn">
							<span class="text-">
								<i>Изменить статус:</i>
							</span>
							<span class="button- jq-button" data-status-type="after_status" data-status="missed">
								<i>я не участвовал</i>
							</span>
						</div>
						<span class="toggle button- check-dark-" jq-toggle="jq-event-{@id}-statuses-dd-REMOVE1">
							<i>Вы участвовали</i>
							<xsl:text>.</xsl:text>
						</span>
					</span>
				</xsl:when>
				<xsl:when test="@status = 'finished' and (@before_status = 'maybe' or @before_status = 'go') and @after_status = 'unknown'">
					<span class="excl-">
						<i> </i>
					</span>
					<span class="text-">
						<i>Событие из вашего календаря закончено. 
					<strong>Вы участвовали?</strong>
						</i>
					</span>
					<span class="button- yes- jq-button" data-status-type="after_status" data-status="visited">
						<i>Да</i>
					</span>
					<span class="button- no- jq-button" data-status-type="after_status" data-status="missed">
						<i>Нет</i>
					</span>
				</xsl:when>
			</xsl:choose>
			<span class="loading-"/>
		</div>
	</xsl:template>
</xsl:stylesheet>
