<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="event_status.inc.xslt"/>
	<xsl:template name="draw_event_head">
		<xsl:param name="draw_table_header" select="true()"/>
		<xsl:param name="show_checkbox" select="false()"/>
		<xsl:param name="show_datetime_column" select="true()"/>
		<xsl:param name="show_link" select="true()"/>
		<xsl:param name="show_edit_link" select="true()"/>
		<xsl:param name="show_ignore_link" select="true()"/>
		<xsl:param name="show_categories" select="true()"/>
		<xsl:param name="show_announce" select="true()"/>
		<xsl:param name="add_bottom_margin" select="true()"/>
		<xsl:param name="remove_on_ignore" select="false()"/>
		<xsl:param name="freeze_statuses" select="false()"/>
		<xsl:param name="show_comment_count" select="false()"/>
		<xsl:param name="event_status"/>
		<xsl:param name="project"/>
		<xsl:variable name="current_event_url">
			<xsl:choose>
				<xsl:when test="not($project)">
					<xsl:value-of select="concat($events_url, @id, '/')"/>
				</xsl:when>
				<xsl:when test="$event_status and $event_status/@reflex_id &gt; 0">
					<xsl:value-of select="concat($project/@url, 'events/', $event_status/@reflex_id, '/')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat($project/@url, 'events/', @id, '/')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<table id="jq-resource-head-{@id}" cellpadding="0" cellspacing="0">
			<xsl:attribute name="class">
				<xsl:text>resource-head </xsl:text>
				<xsl:if test="$add_bottom_margin">resource-head-bottom-margin-</xsl:if>
			</xsl:attribute>
			<tr>
				<xsl:if test="$show_checkbox">
					<td class="col-checkbox-">
						<input type="checkbox" class="input-checkbox jq-resource-checkbox" name="select_event_ids[{@id}]" value="{@id}"/>
					</td>
				</xsl:if>
				<xsl:if test="$show_datetime_column">
					<td class="col-datetime-">
						<div class="sep-">
							<div class="date-">
								<span class="day-">
									<xsl:value-of select="substring(@start_time, 9, 2)"/>
								</span>
								<span class="month-">
									<xsl:call-template name="get_month_name">
										<xsl:with-param name="month_number" select="substring(@start_time, 6, 2)"/>
										<xsl:with-param name="type" select="'short_small'"/>
									</xsl:call-template>
								</span>
							</div>
							<div class="week-day-weekend-">
								<xsl:variable name="week_day_number" select="substring(@start_time, 21, 1)"/>
								<xsl:attribute name="class">
									<xsl:text>week-day-</xsl:text>
									<xsl:if test="$week_day_number = 0 or $week_day_number = 6 or $week_day_number = 7"> week-day-weekend-</xsl:if>
								</xsl:attribute>
								<xsl:call-template name="get_week_day_name">
									<xsl:with-param name="week_day_number" select="$week_day_number"/>
									<xsl:with-param name="type" select="'short_small'"/>
								</xsl:call-template>
							</div>
							<div class="time-">
								<xsl:value-of select="substring(@start_time, 12, 5)"/>
							</div>
						</div>
					</td>
				</xsl:if>
				<td class="col-main-">
					<div class="title-">
						<xsl:choose>
							<xsl:when test="$show_link">
								<xsl:if test="$project">
									<span class="project-">
										<a href="{$project/@url}">
											<xsl:value-of select="$project/@title"/>
										</a>
										<xsl:text> / </xsl:text>
									</span>
								</xsl:if>
								<a href="{$current_event_url}">
									<xsl:value-of select="@title"/>
								</a>
							</xsl:when>
							<xsl:otherwise>
								<h3>
									<xsl:value-of select="@title"/>
								</h3>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:if test="$user/@id">
							<span class="icons- icons-small">
								<xsl:if test="$show_ignore_link">
									<xsl:variable name="ignore">
										<xsl:choose>
											<xsl:when test="@ignore">
												<xsl:value-of select="@ignore"/>
											</xsl:when>
											<xsl:when test="/root/events/user_ignore_list/event[@id = current()/@id]">1</xsl:when>
											<xsl:otherwise>0</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
									<span id="jq-resource-ignore-{@id}" data-id="{@id}" data-ignore="{$ignore}" title="Больше не показывать в моей ленте событий">
										<xsl:attribute name="data-remove-on-ignore">
											<xsl:choose>
												<xsl:when test="$remove_on_ignore">1</xsl:when>
												<xsl:otherwise>0</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<xsl:attribute name="class">
											<xsl:text>jq-resource-ignore </xsl:text>
											<xsl:text>ignore- </xsl:text>
											<xsl:if test="$ignore = 0">gray-</xsl:if>
										</xsl:attribute>
									</span>
								</xsl:if>
								<xsl:if test="$show_edit_link">
									<a class="edit- gray-" href="{$events_url}{@id}/edit/" title="Редактировать"/>
								</xsl:if>
							</span>
						</xsl:if>
					</div>
					<div class="gray-info-">
						<xsl:call-template name="get_full_datetime_range">
							<xsl:with-param name="start_datetime" select="@start_time"/>
							<xsl:with-param name="finish_datetime" select="@finish_time"/>
						</xsl:call-template>
						<xsl:if test="@place != ''">
							<xsl:text> @ </xsl:text>
							<xsl:value-of select="@place"/>
						</xsl:if>
					</div>
					<xsl:if test="$show_categories and categories//item">
						<div class="categories-">
							<span class="hl">
								<xsl:choose>
									<xsl:when test="count(categories//item) &gt; 1">
										<xsl:text>Разделы: </xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>Раздел: </xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</span>
							<xsl:for-each select="categories//item">
								<a>
									<xsl:attribute name="href">
										<xsl:call-template name="url_replace_param">
											<xsl:with-param name="url_base">
												<xsl:choose>
													<xsl:when test="/root/events">
														<xsl:value-of select="$module_url"/>
													</xsl:when>
													<xsl:when test="$project">
														<xsl:value-of select="concat($project/@url, 'events/', $module_postfix)"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$events_url"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:with-param>
											<xsl:with-param name="param_name" select="'cat'"/>
											<xsl:with-param name="param_value" select="@id"/>
										</xsl:call-template>
									</xsl:attribute>
									<xsl:value-of select="@title"/>
								</a>
								<xsl:if test="position() != last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:for-each>
						</div>
					</xsl:if>
					<xsl:if test="$show_announce">
						<div class="announce-">
							<xsl:copy-of select="announce/div"/>
						</div>
					</xsl:if>
					<xsl:if test="@comment_count_calc &gt; 0 and $show_comment_count">
						<div class="comments-">
							<span>
								<a href="{$current_event_url}#comments">
									<xsl:value-of select="@comment_count_calc"/>
									<xsl:call-template name="count_case">
										<xsl:with-param name="number" select="@comment_count_calc"/>
										<xsl:with-param name="word_ns" select="' комменарий'"/>
										<xsl:with-param name="word_gs" select="' комментария'"/>
										<xsl:with-param name="word_ap" select="' комментариев'"/>
									</xsl:call-template>
								</a>
							</span>
						</div>
					</xsl:if>
					<xsl:if test="$event_status">
						<xsl:for-each select="$event_status">
							<xsl:if test="@before_status">
								<xsl:call-template name="draw_event_status">
									<xsl:with-param name="freeze" select="$freeze_statuses"/>
								</xsl:call-template>
							</xsl:if>
						</xsl:for-each>
					</xsl:if>
				</td>
				<td class="col-match-">
					<xsl:for-each select="/root/events/event[@id = current()/@id] | /root/event_show">
						<xsl:call-template name="draw_resource_rate_match">
							<xsl:with-param name="draw_table_header" select="$draw_table_header"/>
							<xsl:with-param name="resource_url" select="concat($events_url, @id, '/')"/>
							<xsl:with-param name="can_edit_rate" select="$show_edit_link"/>
						</xsl:call-template>
					</xsl:for-each>
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>
