<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_resources_import_projects_form">
		<xsl:param name="resource_name"/>
		<div id="dialog-confirm-delete" class="dn">
			<p>Подтвердите: проект будет удалён из списка.</p>
		</div>
		<div class="ico-links-small">
			<a class="add-" href="{$module_url}add/">
				<span>добавить проект</span>
			</a>
		</div>
		<xsl:if test="project_import">
			<form action="{$save_prefix}/{$resource_name}s_import_search/" method="post">
				<fieldset>
					<legend>Проекты, из которых импортируем</legend>
					<xsl:for-each select="project_import">
						<xsl:variable name="current_project_import" select="."/>
						<xsl:for-each select="/root/project_short[@id = current()/@from_project_id]">
							<div class="field">
								<div class="input-">
									<input class="input-checkbox" type="checkbox" name="select_project_ids[{@id}]" value="{@id}">
										<xsl:choose>
											<xsl:when test="$current_project_import/@checked_status = '1'">
												<xsl:attribute name="checked">checked</xsl:attribute>
											</xsl:when>
											<xsl:when test="$pass_info/vars/var[@name = 'select_project_ids']/item[@index = current()/@id] &gt; 0">
												<xsl:attribute name="checked">checked</xsl:attribute>
											</xsl:when>
										</xsl:choose>
									</input>
									<label class="inline-">
										<a href="{@url}">
											<xsl:value-of select="@title"/>
										</a>
									</label>
									<span class="icons- icons-small">
										<a class="delete- gray- jq-import-project-delete" href="javascript:void(0);" data-project-id="{@id}" data-project-title="{@title}" title="Удалить из списка"/>
									</span>
								</div>
							</div>
						</xsl:for-each>
					</xsl:for-each>
				</fieldset>
				<input type="hidden" name="project_id" value="{$current_project/@id}"/>
				<input type="hidden" name="retpath" value="{$module_url}select/"/>
				<button class="button button-small">
					<xsl:call-template name="_draw_import_button_title"/>
					<xsl:text> </xsl:text>
					<span class="arrow-">→</span>
				</button>
			</form>
		</xsl:if>
		<xsl:for-each select="/root/project_short[@id = current()/project_import/@from_project_id]">
			<form class="dn" id="project_delete_form_{@id}" action="{$save_prefix}/{$resource_name}s_import_projects/" method="post">
				<input type="hidden" name="action" value="delete"/>
				<input type="hidden" name="delete_project_id" value="{@id}"/>
				<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			</form>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
