<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_material_status">
		<xsl:param name="footer" value="false()"/>
		<div>
			<xsl:attribute name="data-id">
				<xsl:choose>
					<xsl:when test="@reflex_id &gt; 0">
						<xsl:value-of select="@reflex_id"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@id"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="$footer">statuses- statuses-materials- footer-</xsl:when>
					<xsl:otherwise>statuses- statuses-materials- header-</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="not($footer) and (@after_status = 'unknown' or @after_status = 'not_read')">
				<xsl:choose>
					<xsl:when test="@before_status = 'none'">
						<span class="button- check-dark- jq-button" data-status-type="before_status" data-status="read">
							<i>Изучить обязательно</i>
						</span>
						<span class="button- check-light- jq-button" data-status-type="before_status" data-status="maybe">
							<i>Изучить по возможности</i>
						</span>
					</xsl:when>
					<xsl:when test="@before_status = 'maybe'">
						<span class="button- check-dark- jq-button" data-status-type="before_status" data-status="read">
							<i>Изучить обязательно</i>
						</span>
						<span class="button- selected-light-" data-status-type="before_status" data-status="maybe">
							<i>Изучить по возможности</i>
						</span>
						<span class="button- jq-button" data-status-type="before_status" data-status="none">
							<i>Не изучать</i>
						</span>
					</xsl:when>
					<xsl:when test="@before_status = 'read'">
						<span class="button- selected-dark-" data-status-type="before_status" data-status="read">
							<i>Изучить обязательно</i>
						</span>
						<span class="button- check-light- jq-button" data-status-type="before_status" data-status="maybe">
							<i>Изучить по возможности</i>
						</span>
						<span class="button- jq-button" data-status-type="before_status" data-status="none">
							<i>Не изучать</i>
						</span>
					</xsl:when>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="$footer or @after_status = 'have_read'">
				<xsl:choose>
					<xsl:when test="@after_status = 'unknown' or @after_status = 'not_read'">
						<span class="dd-outer-">
							<span class="button- jq-button no-left-pad-" data-status-type="after_status" data-status="have_read">
								<i>Отметить материал как изученный</i>
							</span>
							<span class="text-gray-">
								<i> (для статистики)</i>
							</span>
						</span>
					</xsl:when>
					<xsl:when test="@after_status = 'have_read'">
						<span class="dd-outer-">
							<span class="check-dark-">
								<i>Вы изучили этот материал</i>.
							</span>
							<span class="button- jq-button" data-status-type="after_status" data-status="not_read">
								<i>Отменить</i>.
							</span>
						</span>
					</xsl:when>
				</xsl:choose>
			</xsl:if>
			<span class="loading-"/>
		</div>
	</xsl:template>
</xsl:stylesheet>
