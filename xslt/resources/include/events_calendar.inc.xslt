<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="current_calendar" select="(/root/events_calendar | /root/user_events_calendar)[1]"/>
	<xsl:variable name="is_project_calendar" select="not(not(/root/events_calendar))"/>
	<xsl:template name="draw_events_calendar">
		<div class="calendar-day-outer">
			<div id="jq-calendar-day-details" class="box-light calendar-day dn">
				<div class="content-">
				</div>
			</div>
		</div>
		<xsl:call-template name="draw_events_calendar_inner"/>
		<script type="text/javascript">
			<xsl:choose>
				<xsl:when test="$is_project_calendar">
					var events_calendar_ajax_url = global.ajax_prefix + 'events_calendar/';
					var events_calendar_request_params = '?project_id=' + '<xsl:value-of select="$current_project/@id"/>';
					var events_day_list_ajax_url = global.ajax_prefix + 'events_day_list/';
					var events_day_list_request_params = events_calendar_request_params;
				</xsl:when>
				<xsl:otherwise>
					var events_calendar_ajax_url = global.ajax_prefix + 'user_events_calendar/';
					var events_calendar_request_params = '?user_id=' + '<xsl:value-of select="$current_user/@id"/>';
					var events_day_list_ajax_url = global.ajax_prefix + 'user_events_day_list/';
					var events_day_list_request_params = events_calendar_request_params;
				</xsl:otherwise>
			</xsl:choose>
			var events_calendar_current_year = <xsl:value-of select="$current_calendar/@year"/>;
			var events_calendar_current_month = <xsl:value-of select="$current_calendar/@month"/>;
		</script>
	</xsl:template>
	<xsl:template name="draw_events_calendar_inner">
		<xsl:for-each select="$current_calendar">
			<div id="jq-calendar">
				<div class="widget-wrap">
					<div class="widget widget-calendar">
						<table class="head-" cellpadding="0" cellspacing="0">
							<tr>
								<td class="arrow- arrow-prev-">
									<div id="jq-calendar-prev" data-year="{@prev_year}" data-month="{@prev_month}">←</div>
								</td>
								<td class="date-">
									<table>
										<tr>
											<td class="left-"/>
											<td class="link- month-">
												<div class="relative-">
													<div id="jq-calendar-month-dd" class="jq-calendar-dd dd-menu- dn">
														<xsl:for-each select="months/month">
															<div data-month="{@number}" class="item-">
																<span>
																	<xsl:call-template name="get_month_name">
																		<xsl:with-param name="month_number" select="@number"/>
																	</xsl:call-template>
																</span>
															</div>
														</xsl:for-each>
													</div>
													<a>
														<xsl:attribute name="href">
															<xsl:choose>
																<xsl:when test="$is_project_calendar">
																	<xsl:value-of select="concat($current_project/@url, 'events/calendar/', @year, '/', @month, '/')"/>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:value-of select="concat($prefix, '/users/', $current_user/@login, '/events/calendar/', @year, '/', @month, '/')"/>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:attribute>
														<xsl:call-template name="get_month_name">
															<xsl:with-param name="month_number" select="@month"/>
														</xsl:call-template>
													</a>
												</div>
											</td>
											<td id="jq-calendar-call-month-dd" class="arr- month-"> </td>
											<td class="sep-"> </td>
											<td class="link- year-">
												<div class="relative-">
													<div id="jq-calendar-year-dd" class="jq-calendar-dd dd-menu- dn">
														<xsl:for-each select="years/year">
															<div data-year="{@number}" class="item-">
																<span>
																	<xsl:value-of select="@number"/>
																</span>
															</div>
														</xsl:for-each>
													</div>
													<a>
														<xsl:attribute name="href">
															<xsl:choose>
																<xsl:when test="$is_project_calendar">
																	<xsl:value-of select="concat($current_project/@url, 'events/calendar/', @year, '/')"/>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:value-of select="concat($prefix, '/users/', $current_user/@login, '/events/calendar/', @year, '/')"/>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:attribute>
														<xsl:value-of select="@year"/>
													</a>
												</div>
											</td>
											<td id="jq-calendar-call-year-dd" class="arr- year-"> </td>
										</tr>
									</table>
								</td>
								<td class="arrow- arrow-next-">
									<div id="jq-calendar-next" data-year="{@next_year}" data-month="{@next_month}">→</div>
								</td>
							</tr>
						</table>
						<table class="days-head-" cellpadding="0" cellspacing="0">
							<tr>
								<th>пн</th>
								<th>вт</th>
								<th>ср</th>
								<th>чт</th>
								<th>пт</th>
								<th>сб</th>
								<th>вс</th>
							</tr>
						</table>
						<table class="days-" cellpadding="0" cellspacing="0">
							<xsl:for-each select="week">
								<tr>
									<xsl:for-each select="day">
										<xsl:variable name="my" select="/root/user_events_calendar/week/day[@number = current()/@number and @month = current()/@month]/@object_count &gt; 0"/>
										<td>
											<div>
												<xsl:if test="@number">
													<xsl:attribute name="class">
														<xsl:choose>
															<xsl:when test="$is_project_calendar">
																<xsl:if test="@object_count &gt; 0">hl- </xsl:if>
																<xsl:if test="$my">hl-my- </xsl:if>
															</xsl:when>
															<xsl:when test="$user/@id = $current_user/@id">
																<xsl:if test="@object_count &gt; 0">hl-my- </xsl:if>
															</xsl:when>
															<xsl:otherwise>
																<xsl:if test="@object_count &gt; 0">hl- </xsl:if>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:attribute>
													<span>
														<xsl:attribute name="class">
															<xsl:if test="@prev or @next">
																<xsl:text>gray- </xsl:text>
															</xsl:if>
															<xsl:if test="@year = /root/@year and @month = /root/@month and @number = /root/@day">
																<xsl:text>today- </xsl:text>
															</xsl:if>
														</xsl:attribute>
														<xsl:choose>
															<xsl:when test="@object_count &gt; 0 or $my">
																<a class="jq-calendar-day" data-year="{@year}" data-month="{@month}" data-day="{@number}">
																	<xsl:attribute name="href">
																		<xsl:choose>
																			<xsl:when test="not($is_project_calendar)">
																				<xsl:value-of select="concat($prefix, '/users/', $current_user/@login, '/events/calendar/', @year, '/', @month, '/', @number, '/')"/>
																			</xsl:when>
																			<xsl:when test="@object_count = 0">
																				<xsl:value-of select="concat($prefix, '/users/', $user/@login, '/events/calendar/', @year, '/', @month, '/', @number, '/')"/>
																			</xsl:when>
																			<xsl:otherwise>
																				<xsl:value-of select="concat($current_project/@url, 'events/calendar/', @year, '/', @month, '/', @number, '/')"/>
																			</xsl:otherwise>
																		</xsl:choose>
																	</xsl:attribute>
																	<xsl:if test="@prev">
																		<xsl:attribute name="data-year">
																			<xsl:value-of select="../../@prev_year"/>
																		</xsl:attribute>
																		<xsl:attribute name="data-month">
																			<xsl:value-of select="../../@prev_month"/>
																		</xsl:attribute>
																	</xsl:if>
																	<xsl:if test="@next">
																		<xsl:attribute name="data-year">
																			<xsl:value-of select="../../@next_year"/>
																		</xsl:attribute>
																		<xsl:attribute name="data-month">
																			<xsl:value-of select="../../@next_month"/>
																		</xsl:attribute>
																	</xsl:if>
																	<xsl:value-of select="@number"/>
																</a>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="@number"/>
															</xsl:otherwise>
														</xsl:choose>
													</span>
												</xsl:if>
											</div>
										</td>
									</xsl:for-each>
								</tr>
							</xsl:for-each>
						</table>
						<xsl:if test="$is_project_calendar">
							<div class="archive-">
								<a href="{$current_project/@url}events/archive/">Все прошедшие события</a>
							</div>
						</xsl:if>
					</div>
				</div>
			</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
