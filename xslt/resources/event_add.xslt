<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:include href="../competence_sets/include/rate_edit.inc.xslt"/>
	<xsl:include href="include/events_calendar.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'events'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="events_url" select="concat($current_project/@url, 'events/')"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'events/add/')"/>
	<xsl:template match="event_add">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>События</h2>
							<div class="content">
								<h3>Добавление события</h3>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/event_add/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$events_url}/%ID%/"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<xsl:call-template name="draw_dt_edit"/>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
		<script type="text/javascript" src="{$prefix}/js/events_calendar.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Добавление события &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/projects/">Проекты</a>
		</div>
		<div class="level2-">
			<a href="{$current_project/@url}">
				<xsl:value-of select="$current_project/@title"/>
			</a>
		</div>
		<div class="level3-">
			<a href="{$current_project/@url}admin/">Админка</a>
		</div>
	</xsl:template>
</xsl:stylesheet>
