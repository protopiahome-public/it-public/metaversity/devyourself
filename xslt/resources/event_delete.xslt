<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/event_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/delete.inc.xslt"/>
	<xsl:include href="include/event_head.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'events'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="event_id" select="/root/event_delete/@id"/>
	<xsl:variable name="event_title" select="/root/event_delete/@title"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'events/', $event_id, '/delete/')"/>
	<xsl:variable name="event_data" select="/root/event_delete"/>
	<xsl:variable name="event_status" select="/root/event_delete"/>
	<xsl:template match="event_delete">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>События</h2>
							<div class="content">
								<xsl:for-each select="/root/event_short">
									<xsl:call-template name="draw_event_head">
										<xsl:with-param name="show_link" select="true()"/>
										<xsl:with-param name="show_edit_link" select="false()"/>
										<xsl:with-param name="show_ignore_link" select="false()"/>
										<xsl:with-param name="show_announce" select="false()"/>
										<xsl:with-param name="show_match" select="false()"/>
										<xsl:with-param name="add_bottom_margin" select="false()"/>
										<xsl:with-param name="event_data" select="$event_data"/>
										<xsl:with-param name="event_status" select="$event_status"/>
									</xsl:call-template>
								</xsl:for-each>
								<xsl:call-template name="draw_event_submenu"/>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/event_delete/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$events_url}"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<xsl:call-template name="draw_delete">
				<xsl:with-param name="title_akk" select="'событие'"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Удаление события &#8212; </xsl:text>
		<xsl:value-of select="$event_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		
	</xsl:template>
</xsl:stylesheet>
