<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="include/materials_categories.inc.xslt"/>
	<xsl:include href="include/resource_rate.inc.xslt"/>
	<xsl:include href="include/material_head.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'materials'"/>
	<xsl:variable name="project_section_main_page" select="/root/materials/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="materials_url" select="concat($current_project/@url, 'materials/')"/>
	<xsl:variable name="module_url" select="$materials_url"/>
	<xsl:template match="materials">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Материалы</h2>
							<div class="content">
								<xsl:call-template name="_draw_list"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_materials_categories">
								<xsl:with-param name="cats" select="/root/materials_categories"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<xsl:if test="$project_access/@can_moderate_materials = 1">
			<div class="ico-links">
				<a class="add-" href="{$materials_url}add/">
					<span>добавить материал</span>
				</a>
				<a class="import-" href="{$materials_url}import/">
					<span>импортировать материалы</span>
				</a>
			</div>
		</xsl:if>
		<xsl:call-template name="draw_resource_rate_vector_invite"/>
		<div id="resources-head">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td class="l-">
						<div class="submenu- submenu-sort-">
							<span class="header-">Сортировать </span>
							<xsl:if test="@vector_exists = 1">
								<span>
									<xsl:attribute name="class">
										<xsl:text>item- </xsl:text>
										<xsl:if test="@is_vector_sort = 1">selected-</xsl:if>
									</xsl:attribute>
									<xsl:choose>
										<xsl:when test="@is_vector_sort = 1">по полезности</xsl:when>
										<xsl:otherwise>
											<a>
												<xsl:attribute name="href">
													<xsl:call-template name="url_delete_param">
														<xsl:with-param name="param_name" select="'sort'"/>
													</xsl:call-template>
												</xsl:attribute>
												<xsl:text>по полезности</xsl:text>
											</a>
										</xsl:otherwise>
									</xsl:choose>
								</span>
								<span class="sep-"/>
							</xsl:if>
							<span>
								<xsl:attribute name="class">
									<xsl:text>item- </xsl:text>
									<xsl:if test="@is_vector_sort = 0">selected-</xsl:if>
								</xsl:attribute>
								<xsl:choose>
									<xsl:when test="@is_vector_sort = 0">по дате добавления</xsl:when>
									<xsl:otherwise>
										<a>
											<xsl:attribute name="href">
												<xsl:call-template name="url_replace_param">
													<xsl:with-param name="param_name" select="'sort'"/>
													<xsl:with-param name="param_value" select="'date'"/>
												</xsl:call-template>
											</xsl:attribute>
											<xsl:text>по дате добавления</xsl:text>
										</a>
									</xsl:otherwise>
								</xsl:choose>
							</span>
						</div>
					</td>
					<td class="r-">
						<xsl:if test="$user/@id">
							<div class="settings-">
								<div id="jq-materials-settings-dd" class="box-light dd- dn">
									<xsl:if test="@vector_exists = 1">
										<div class="field setting-">
											<div class="line- line-head-">
												<strong>Расчёт полезности</strong>
											</div>
											<div class="line-">
												<span>Материал полезен, если он развивает компетенции:</span>
											</div>
											<div class="line-">
												<input type="radio" class="input-radio" onclick="$.cookie('vector-calc-base', 'focus', {{path: global.prefix}}); location.reload(true);" name="vector-base" value="focus" id="f-vector-base-focus">
													<xsl:attribute name="data-sort-url">
														<xsl:call-template name="url_replace_param">
															<xsl:with-param name="url_base" select="$module_url"/>
															<xsl:with-param name="param_name" select="'vector-calc-base'"/>
															<xsl:with-param name="param_value" select="'focus'"/>
														</xsl:call-template>
													</xsl:attribute>
													<xsl:if test="@vector_calc_base = 'focus'">
														<xsl:attribute name="checked">
															<xsl:text>checked</xsl:text>
														</xsl:attribute>
													</xsl:if>
												</input>
												<label for="f-vector-base-focus" class="inline-">из вашего <a href="#">фокуса</a>
												</label>
											</div>
											<div class="line-">
												<input type="radio" class="input-radio" onclick="$.cookie('vector-calc-base', 'future', {{path: global.prefix}}); location.reload(true);" name="vector-base" value="future" id="f-vector-base-future">
													<xsl:attribute name="data-sort-url">
														<xsl:call-template name="url_replace_param">
															<xsl:with-param name="url_base" select="$module_url"/>
															<xsl:with-param name="param_name" select="'vector-calc-base'"/>
															<xsl:with-param name="param_value" select="'future'"/>
														</xsl:call-template>
													</xsl:attribute>
													<xsl:if test="@vector_calc_base = 'future'">
														<xsl:attribute name="checked">
															<xsl:text>checked</xsl:text>
														</xsl:attribute>
													</xsl:if>
												</input>
												<label for="f-vector-base-future" class="inline-">из профиля &#171;<a href="#">Я в будущем</a>&#187;</label>
											</div>
										</div>
									</xsl:if>
									<xsl:if test="@vector_exists = 1">
										<hr/>
									</xsl:if>
									<div class="field setting-">
										<input type="checkbox" class="input-checkbox" onchange="location.href = $(this).attr('data-filter-url');" name="with-ignored" value="1" id="f-ignore-on">
											<xsl:attribute name="data-filter-url">
												<xsl:if test="@with_ignored = 1">
													<xsl:call-template name="url_delete_param">
														<xsl:with-param name="url_base" select="$module_url"/>
														<xsl:with-param name="param_name" select="'with-ignored'"/>
													</xsl:call-template>
												</xsl:if>
												<xsl:if test="not(@with_ignored = 1)">
													<xsl:call-template name="url_replace_param">
														<xsl:with-param name="url_base" select="$module_url"/>
														<xsl:with-param name="param_name" select="'with-ignored'"/>
														<xsl:with-param name="param_value" select="'1'"/>
													</xsl:call-template>
												</xsl:if>
											</xsl:attribute>
											<xsl:if test="@with_ignored = 1">
												<xsl:attribute name="checked">checked</xsl:attribute>
											</xsl:if>
										</input>
										<label for="f-ignore-on" class="inline-">
											<xsl:text>Показывать материалы, удалённые из ленты кнопкой </xsl:text>
											<span class="icons- icons-small">
												<span class="ignore-"/>
											</span>
										</label>
									</div>
								</div>
								<span class="toggle link-" jq-toggle="jq-materials-settings-dd">Настройка ленты</span>
							</div>
						</xsl:if>
					</td>
				</tr>
			</table>
			<div class="what-is-shown-">
				<xsl:if test="@cat_id > 0">
					<xsl:text>Раздел: </xsl:text>
					<strong>
						<xsl:value-of select="/root/materials_categories//category[@id = current()/@cat_id]/@title"/>
					</strong>
					<xsl:text>. </xsl:text>
				</xsl:if>
				<xsl:if test="@with_ignored = 1">
					<xsl:text>Отображаются скрытые вами материалы.</xsl:text>
				</xsl:if>
			</div>
		</div>
		<xsl:if test="not(material)">
			<p>Ни одного материала не найдено.</p>
		</xsl:if>
		<xsl:if test="material">
			<xsl:for-each select="material">
				<xsl:variable name="position" select="position()"/>
				<xsl:variable name="material_status" select="."/>
				<xsl:for-each select="/root/material_short[@id = current()/@id]">
					<xsl:call-template name="draw_material_head">
						<xsl:with-param name="draw_table_header" select="$position = 1"/>
						<xsl:with-param name="remove_on_ignore" select="not(/root/materials/@with_ignored = 1)"/>
						<xsl:with-param name="show_ignore_link" select="true()"/>
						<xsl:with-param name="show_edit_link" select="$project_access/@can_moderate_materials = 1"/>
						<xsl:with-param name="material_status" select="$material_status"/>
						<xsl:with-param name="project" select="/root/project_short[@id = $material_status/@reflex_project_id]"/>
						<xsl:with-param name="show_comment_count" select="true()"/>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:for-each>
			<xsl:apply-templates select="pages[page]"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript">
			var resource_name = 'material';
		</script>
		<script type="text/javascript" src="{$prefix}/js/resource_ignore.js"/>
		<script type="text/javascript" src="{$prefix}/js/resource_status.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Материалы &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		
	</xsl:template>
</xsl:stylesheet>
