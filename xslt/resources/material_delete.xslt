<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/material_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/delete.inc.xslt"/>
	<xsl:include href="include/material_head.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'materials'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="material_id" select="/root/material_delete/@id"/>
	<xsl:variable name="material_title" select="/root/material_delete/@title"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'materials/', $material_id, '/delete/')"/>
	<xsl:variable name="material_data" select="/root/material_delete"/>
	<xsl:variable name="material_status" select="/root/material_delete"/>
	<xsl:template match="material_delete">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Материалы</h2>
							<div class="content">
								<xsl:for-each select="/root/material_short">
									<xsl:call-template name="draw_material_head">
										<xsl:with-param name="show_link" select="true()"/>
										<xsl:with-param name="show_edit_link" select="false()"/>
										<xsl:with-param name="show_ignore_link" select="false()"/>
										<xsl:with-param name="show_announce" select="false()"/>
										<xsl:with-param name="show_match" select="false()"/>
										<xsl:with-param name="add_bottom_margin" select="false()"/>
										<xsl:with-param name="material_data" select="$material_data"/>
										<xsl:with-param name="material_status" select="$material_status"/>
									</xsl:call-template>
								</xsl:for-each>
								<xsl:call-template name="draw_material_submenu"/>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/material_delete/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$materials_url}"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<xsl:call-template name="draw_delete">
				<xsl:with-param name="title_akk" select="'материал'"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Удаление материала &#8212; </xsl:text>
		<xsl:value-of select="$material_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		
	</xsl:template>
</xsl:stylesheet>
