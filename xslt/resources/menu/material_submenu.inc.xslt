<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="materials_url" select="concat($current_project/@url, 'materials/')"/>
	<xsl:variable name="material_url" select="concat($current_project/@url, 'materials/', $material_id, '/')"/>
	<xsl:template name="draw_material_submenu">
		<div class="ico-links" style="margin: 8px 0;">
			<xsl:choose>
				<xsl:when test="/root/material_edit">
					<strong class="edit- current-">
						<span>редактировать материал</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="edit-" href="{$material_url}edit/">
						<span>редактировать материал</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/material_edit_rate">
					<strong class="rate- current-">
						<span>развиваемые компетенции</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="rate-" href="{$material_url}edit/rate/">
						<span>развиваемые компетенции</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/material_delete">
					<strong class="delete- current-">
						<span>удалить</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="delete-" href="{$material_url}delete/">
						<span>удалить</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<xsl:if test="@reflex_id &gt; 0">
			<div class="box box-warning">
				<p style="padding: 8px 0;">
					<xsl:text>Данный материал импортирован из проекта </xsl:text>
					<a href="{/root/project_short[@id = current()/@reflex_project_id]/@url}">
						<xsl:value-of select="/root/project_short[@id = current()/@reflex_project_id]/@title"/>
					</a>
					<xsl:text> </xsl:text>
					<xsl:call-template name="get_full_datetime">
						<xsl:with-param name="datetime" select="@add_time"/>
					</xsl:call-template>
					<xsl:text>.</xsl:text>
				</p>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
