<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="events_url" select="concat($current_project/@url, 'events/')"/>
	<xsl:variable name="event_url" select="concat($current_project/@url, 'events/', $event_id, '/')"/>
	<xsl:template name="draw_event_submenu">
		<div class="ico-links" style="margin: 8px 0;">
			<xsl:choose>
				<xsl:when test="/root/event_edit">
					<strong class="edit- current-">
						<span>редактировать событие</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="edit-" href="{$event_url}edit/">
						<span>редактировать событие</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/event_edit_rate">
					<strong class="rate- current-">
						<span>развиваемые компетенции</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="rate-" href="{$event_url}edit/rate/">
						<span>развиваемые компетенции</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/event_delete">
					<strong class="delete- current-">
						<span>удалить</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="delete-" href="{$event_url}delete/">
						<span>удалить</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<xsl:if test="@reflex_id &gt; 0">
			<div class="box box-warning">
				<p style="padding: 8px 0;">
					<xsl:text>Данное событие импортировано из проекта </xsl:text>
					<a href="{/root/project_short[@id = current()/@reflex_project_id]/@url}">
						<xsl:value-of select="/root/project_short[@id = current()/@reflex_project_id]/@title"/>
					</a>
					<xsl:text> </xsl:text>
					<xsl:call-template name="get_full_datetime">
						<xsl:with-param name="datetime" select="@add_time"/>
					</xsl:call-template>
					<xsl:text>.</xsl:text>
				</p>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
