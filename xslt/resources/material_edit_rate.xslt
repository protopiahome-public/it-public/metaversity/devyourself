<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/material_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:include href="../competence_sets/include/rate_edit.inc.xslt"/>
	<xsl:include href="include/material_head.inc.xslt"/>
	<xsl:include href="../vectors/include/vector_messages.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'materials'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="material_id" select="/root/material_edit_rate/document/@id"/>
	<xsl:variable name="material_title" select="/root/material_edit_rate/document/field[@name = 'title']"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'materials/', /root/material_edit/document/@id, '/edit/')"/>
	<xsl:variable name="material_data" select="/root/material_edit_rate"/>
	<xsl:variable name="material_status" select="/root/material_edit_rate"/>
	<xsl:template match="material_edit_rate">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Материалы</h2>
							<div class="content">
								<xsl:for-each select="/root/material_short">
									<xsl:call-template name="draw_material_head">
										<xsl:with-param name="show_link" select="true()"/>
										<xsl:with-param name="show_edit_link" select="false()"/>
										<xsl:with-param name="show_ignore_link" select="false()"/>
										<xsl:with-param name="show_announce" select="false()"/>
										<xsl:with-param name="show_match" select="false()"/>
										<xsl:with-param name="add_bottom_margin" select="false()"/>
										<xsl:with-param name="material_data" select="$material_data"/>
										<xsl:with-param name="material_status" select="$material_status"/>
									</xsl:call-template>
								</xsl:for-each>
								<xsl:call-template name="draw_material_submenu"/>
								<xsl:choose>
									<xsl:when test="$current_project/@vector_competence_set_id &gt; 0">
										<xsl:call-template name="_draw_form"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="vector_messages_no_competence_set">
											<xsl:with-param name="text" select="'Редактировать компетенции нельзя'"/>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/material_edit_rate/" method="post" enctype="multipart/form-data">
			<xsl:choose>
				<xsl:when test="@reflex_id &gt; 0">
					<input type="hidden" name="retpath" value="{$current_project/@url}materials/"/>
				</xsl:when>
				<xsl:otherwise>
					<input type="hidden" name="retpath" value="{$material_url}"/>
				</xsl:otherwise>
			</xsl:choose>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<input type="hidden" name="id" value="{$material_id}"/>
			<xsl:call-template name="draw_rate_edit">
				<xsl:with-param name="legend" select="'Развиваемые компетенции'"/>
				<xsl:with-param name="descr">
					<p>
						<xsl:text>Используется набор компетенций </xsl:text>
						<xsl:for-each select="/root/competence_set_short[@id = /root/competence_set_competences/@competence_set_id]">
							<a href="{$prefix}/sets/set-{@id}/">
								<xsl:value-of select="@title"/>
							</a>
						</xsl:for-each>
						<xsl:text>.</xsl:text>
					</p>
				</xsl:with-param>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Редактирование рейтов материала &#8212; </xsl:text>
		<xsl:value-of select="$material_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		
	</xsl:template>
</xsl:stylesheet>
