<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_common.xslt"/>
	<xsl:include href="../include/event_status.inc.xslt"/>
	<xsl:template match="event_status">
		<xsl:call-template name="draw_event_status">
			<xsl:with-param name="freeze" select="@freeze = 1"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="text()"/>
</xsl:stylesheet>
