<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/event_submenu.inc.xslt"/>
	<xsl:include href="include/events_calendar.inc.xslt"/>
	<xsl:include href="include/event_users.inc.xslt"/>
	<xsl:include href="include/resource_rate.inc.xslt"/>
	<xsl:include href="include/event_head.inc.xslt"/>
	<xsl:include href="../comments/include/comments.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'events'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="event_id" select="/root/event_show/@id"/>
	<xsl:variable name="event_title" select="/root/event_show/@title"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'events/', $event_id, '/')"/>
	<xsl:variable name="event_data" select="/root/event_show"/>
	<xsl:variable name="event_status" select="/root/event_show"/>
	<xsl:template match="event_show">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>События</h2>
							<div class="content">
								<xsl:call-template name="_draw_event"/>
								<xsl:for-each select="/root/comments">
									<xsl:call-template name="draw_comments">
										<xsl:with-param name="can_moderate" select="$project_access/@can_moderate_events = 1"/>
										<xsl:with-param name="can_comment" select="$project_access/@can_comment_events = 1"/>
										<xsl:with-param name="can_comment_html">
											<div class="box" style="margin-top: 8px;">
												<xsl:call-template name="draw_access">
													<xsl:with-param name="for" select="'Для комментирования события'"/>
													<xsl:with-param name="status" select="$project_access/@access_events_comment"/>
													<xsl:with-param name="entity_genitiv" select="'проекта'"/>
													<xsl:with-param name="join_premoderation" select="$project_access/@join_premoderation = 1"/>
												</xsl:call-template>
											</div>
										</xsl:with-param>
										<xsl:with-param name="title_akk" select="'событие'"/>
									</xsl:call-template>
								</xsl:for-each>
								<xsl:call-template name="_draw_competences"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_events_calendar"/>
							<xsl:call-template name="draw_event_users"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_event">
		<xsl:call-template name="draw_event_head">
			<xsl:with-param name="show_link" select="false()"/>
			<xsl:with-param name="show_announce" select="false()"/>
			<xsl:with-param name="add_bottom_margin" select="false()"/>
			<xsl:with-param name="event_data" select="$event_data"/>
			<xsl:with-param name="event_status" select="$event_status"/>
			<xsl:with-param name="freeze_statuses" select="true()"/>
			<xsl:with-param name="show_edit_link" select="$project_access/@can_moderate_events = 1"/>
		</xsl:call-template>
		<div class="resource-body">
			<div class="descr-">
				<xsl:copy-of select="descr/div"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_competences">
		<div>
			<a name="competences"/>
			<xsl:call-template name="draw_resource_rate_competences"/>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			var resource_name = 'event';
			var comment_add_ctrl = 'event_comment_add';
			var comment_delete_ctrl = 'event_comment_delete';
			var comments_subscribe_ctrl = 'event_comments_subscribe';
			var comments_post_vars = {
				project_id : '<xsl:value-of select="$current_project/@id"/>'
			};
			var can_moderate_comments = <xsl:value-of select="$project_access/@can_moderate_events"/>;
		</script>
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.scrollTo.js"/>
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/events_calendar.js"/>
		<script type="text/javascript" src="{$prefix}/js/resource_ignore.js"/>
		<script type="text/javascript" src="{$prefix}/js/resource_status.js"/>
		<script type="text/javascript" src="{$prefix}/js/comments.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$event_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		
	</xsl:template>
</xsl:stylesheet>
