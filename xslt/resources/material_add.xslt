<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:include href="../competence_sets/include/rate_edit.inc.xslt"/>
	<xsl:include href="../block_sets/include/block_set_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'materials'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="materials_url" select="concat($current_project/@url, 'materials/')"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'materials/add/')"/>
	<xsl:template match="material_add">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<h2>Материалы</h2>
							<div class="content">
								<h3>Добавление материала</h3>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/material_add/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$materials_url}/%ID%/"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<xsl:call-template name="draw_dt_edit">
				<xsl:with-param name="draw_button" select="false()"/>
			</xsl:call-template>
			<xsl:for-each select="/root/block_set">
				<xsl:call-template name="draw_block_set_edit">
					<xsl:with-param name="object_url" select="concat($current_project/@url, 'materials/add/')"/>
				</xsl:call-template>
			</xsl:for-each>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			var block_set_post_vars = {
				project_id : '<xsl:value-of select="$current_project/@id"/>',
				material_id : '0',
				block_set_id : '0'
			};
			var block_set_ajax_url = global.ajax_prefix + 'material_block_set/';
		</script>
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/swfobject.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.uploadify.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.scrollTo.js"/>
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
		<script type="text/javascript" src="{$prefix}/js/block_set.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Добавление материала &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/projects/">Проекты</a>
		</div>
		<div class="level2-">
			<a href="{$current_project/@url}">
				<xsl:value-of select="$current_project/@title"/>
			</a>
		</div>
		<div class="level3-">
			<a href="{$current_project/@url}admin/">Админка</a>
		</div>
	</xsl:template>
</xsl:stylesheet>
