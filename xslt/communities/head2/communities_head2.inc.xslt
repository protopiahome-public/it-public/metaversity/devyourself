<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_communities_head2">
		<div class="head2">
			<table class="head2-table- head2-table-narrow-">
				<tr>
					<td class="head2-table-right- no-avatar-">
						<div class="menu-">
							<div class="menu-item-">
								<xsl:if test="$communities_section = 'list'">
									<xsl:attribute name="class">menu-item- menu-item-selected-</xsl:attribute>
								</xsl:if>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="$communities_section = 'list' and $communities_section_main_page and not($get_vars)">Все сообщества</xsl:when>
												<xsl:otherwise>
													<a href="{$current_project/@url}co/">Все сообщества</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
							<div class="menu-sep-">|</div>
							<div class="menu-item-">
								<xsl:if test="$communities_section = 'feed'">
									<xsl:attribute name="class">menu-item- menu-item-selected-</xsl:attribute>
								</xsl:if>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="$communities_section = 'feed' and $communities_section_main_page and not($get_vars)">Все записи</xsl:when>
												<xsl:otherwise>
													<a href="{$current_project/@url}co/feed/">Все записи</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
							<xsl:if test="$current_project/@custom_feed_count_calc &gt; 0">
								<div class="menu-sep-">|</div>
								<div class="menu-item-">
									<xsl:if test="$communities_section = 'feeds'">
										<xsl:attribute name="class">menu-item- menu-item-selected-</xsl:attribute>
									</xsl:if>
									<table class="border-">
										<tr>
											<td class="l-"> </td>
											<td class="c-">
												<xsl:choose>
													<xsl:when test="$communities_section = 'feeds' and $communities_section_main_page and not($get_vars)">Ленты</xsl:when>
													<xsl:otherwise>
														<a href="{$current_project/@url}co/feeds/">Ленты</a>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td class="r-"> </td>
										</tr>
									</table>
								</div>
							</xsl:if>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
