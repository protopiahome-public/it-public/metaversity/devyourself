<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="head2/communities_head2.inc.xslt"/>
	<xsl:include href="menu/communities_submenu.inc.xslt"/>
	<xsl:include href="../community/menu/feed_submenu.inc.xslt"/>
	<xsl:include href="../community/include/post.inc.xslt"/>
	<xsl:include href="../block_sets/include/block_set_show.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="communities_section" select="'feeds'"/>
	<xsl:variable name="communities_section_main_page" select="false()"/>
	<xsl:variable name="current_feed" select="/root/project_custom_feed_full"/>
	<xsl:variable name="module_url" select="$current_feed/@url"/>
	<xsl:variable name="feed_url" select="$current_feed/@url"/>
	<xsl:template match="project_custom_feed">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_communities_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_feed_submenu"/>
							<xsl:call-template name="draw_communities_head2"/>
							<div class="content">
								<h3>
									<xsl:value-of select="$current_feed/@title"/>
								</h3>
								<xsl:call-template name="_draw_list"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<xsl:if test="$current_feed/descr/div/*">
			<div class="box">
				<xsl:copy-of select="$current_feed/descr/div"/>
			</div>
		</xsl:if>
		<xsl:if test="not(post)">
			<p>Ни одной записи не найдено.</p>
		</xsl:if>
		<xsl:for-each select="post">
			<xsl:variable name="context" select="."/>
			<xsl:for-each select="/root/post_full[@id = current()/@id]">
				<xsl:call-template name="draw_post">
					<xsl:with-param name="list" select="true()"/>
					<xsl:with-param name="show_link" select="true()"/>
					<xsl:with-param name="show_edit_link" select="true()"/>
					<xsl:with-param name="show_community_link" select="true()"/>
				</xsl:call-template>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:apply-templates select="pages[page]"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_feed/@title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
