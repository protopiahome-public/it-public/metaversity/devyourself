<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_communities_submenu">
		<xsl:if test="/root/communities_menu/item">
			<ul id="communities-menu">
				<xsl:for-each select="/root/communities_menu/item">
					<xsl:variable name="community" select="/root/community_short[@id = current()/@community_id][1]"/>
					<li>
						<a href="{$community/@url}">
							<xsl:value-of select="$community/@title"/>
						</a>
					</li>		
				
				</xsl:for-each>
			</ul>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
