<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="head2/communities_head2.inc.xslt"/>
	<xsl:include href="menu/communities_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'communities'"/>
	<xsl:variable name="project_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="communities_section" select="'list'"/>
	<xsl:variable name="communities_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'co/')"/>
	<xsl:template match="communities">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_communities_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_communities_head2"/>
							<div class="content">
								<xsl:call-template name="_draw_table"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_table">
		<xsl:if test="$project_access/@can_add_communities = 1">
			<div class="ico-links">
				<a class="add-" href="{$current_project/@url}co/add/">
					<span>создать сообщество</span>
				</a>
			</div>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="community">
			</xsl:when>
			<xsl:otherwise>
				<p>
					<xsl:text>Ни одного сообщества не найдено. </xsl:text>
					<a href="{$current_project/@url}co/add/">Создать?</a>
				</p>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="community">
			<table class="tbl">
				<tr>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="'Название'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'posts'"/>
						<xsl:with-param name="title" select="'Записей'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'comments'"/>
						<xsl:with-param name="title" select="'Комментариев'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'date'"/>
						<xsl:with-param name="title" select="'Дата&#160;добавления'"/>
					</xsl:call-template>
				</tr>
				<xsl:for-each select="tree//community">
					<xsl:variable name="community_id" select="@id"/>
					<xsl:variable name="position" select="position()"/>
					<xsl:variable name="context" select="."/>
					<xsl:for-each select="/root/community_short[@id = current()/@id]">
						<tr>
							<td class="col-subtable-">
								<table style="margin-left: {50 * ($context/@level - 1)}px;">
									<tr>
										<td class="col-logo-">
											<div class="community-logo-sep-">
												<a href="{@url}">
													<img src="{logo_small/@url}" width="{logo_small/@width}" height="{logo_small/@height}" alt="{@title}" title=""/>
												</a>
											</div>
										</td>
										<td class="col-community-title-">
											<div class="title-">
												<a href="{@url}">
													<xsl:value-of select="@title"/>
												</a>
											</div>
											<xsl:if test="@descr_short != ''">
												<div class="descr-">
													<xsl:value-of select="@descr_short"/>
												</div>
											</xsl:if>
										</td>
									</tr>
								</table>
							</td>
							<td class="col-digits-">
								<xsl:value-of select="$context/@post_count_calc"/>
							</td>
							<td class="col-digits-">
								<xsl:value-of select="$context/@comment_count_calc"/>
							</td>
							<td class="col-date-">
								<xsl:call-template name="get_full_datetime">
									<xsl:with-param name="datetime" select="@add_time"/>
								</xsl:call-template>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Все сообщества &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
