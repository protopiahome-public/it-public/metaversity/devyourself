<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="../../base_widgets/widget_edit/base_text.widget_edit.xslt"/>
	<xsl:template match="community_widget_text_edit">
		<xsl:call-template name="base_widget_text_edit"/>
	</xsl:template>
</xsl:stylesheet>
