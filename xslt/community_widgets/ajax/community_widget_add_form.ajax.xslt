<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_common.xslt"/>
	<xsl:include href="../../community/head2/community_head2.inc.xslt"/>
	<xsl:include href="../../base_widgets/include/widget_add_form.inc.xslt"/>
	<xsl:template match="/root/community_widget_add_form">
		<xsl:call-template name="draw_widget_add_form"/>
	</xsl:template>
	<xsl:template match="text()"/>
</xsl:stylesheet>
