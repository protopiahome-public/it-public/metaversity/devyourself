<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- @todo необоснованное дублирование файла (см. community_widget.ajax.xslt). Аналогично с project_widget -->
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_common.xslt"/>
	<xsl:include href="../../community/head2/community_head2.inc.xslt"/>
	<xsl:include href="../../base_widgets/include/widget_base.inc.xslt"/>
	<xsl:template match="/root/community_widgets">
		<xsl:for-each select="widget[1]">
			<div id="widget-{@id}" jq-widget-id="{@id}" class="jq-widget widget-wrap">
				<xsl:apply-templates mode="widget_base" select=".">
					<xsl:with-param name="hide_widget" select="true()"/>
					<xsl:with-param name="can_moderate_widgets" select="$community_access/@has_admin_rights = 1"/>
				</xsl:apply-templates>
			</div>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="text()"/>
</xsl:stylesheet>
