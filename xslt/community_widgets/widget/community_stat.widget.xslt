<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="community_widget_stat" mode="widget">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<div class="widget widget-stat">
			<xsl:call-template name="draw_widget_header">
				<xsl:with-param name="title" select="$data/@title"/>
				<xsl:with-param name="light_header" select="false()"/>
			</xsl:call-template>
			<div class="content-">
				<p>
					<span class="text-">Людей:</span>
					<a href="{$current_community/@url}users/">
						<xsl:value-of select="@member_count_calc"/>
					</a>
				</p>
				<p>
					<span class="text-">Записей:</span>
					<a href="{$current_community/@url}feed/">
						<xsl:value-of select="@post_count_calc"/>
					</a>
				</p>
				<p>
					<span class="text-">Комментариев:</span>
					<xsl:value-of select="@comment_count_calc"/>
				</p>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
