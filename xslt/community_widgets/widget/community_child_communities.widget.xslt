<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="community_widget_child_communities" mode="widget">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<div class="widget widget-gray widget-communities">
			<xsl:call-template name="draw_widget_header">
				<xsl:with-param name="title" select="$data/@title"/>
				<xsl:with-param name="light_header" select="false()"/>
			</xsl:call-template>
			<xsl:choose>
				<xsl:when test="community">
					<div class="content-">
						<ul>
							<xsl:for-each select="community">
								<li class="item-">
									<xsl:for-each select="/root/community_short[@id = current()/@id]">
										<span class="logo-">
											<a href="{@url}">
												<img src="{logo_small/@url}" width="{logo_small/@width}" height="{logo_small/@height}" alt=""/>
											</a>
										</span>
										<span class="title- bigger">
											<a href="{@url}">
												<xsl:value-of select="@title"/>
											</a>
										</span>
									</xsl:for-each>
								</li>
							</xsl:for-each>
						</ul>
					</div>
					<div class="footer-">
						<a href="{$current_community/@url}subcommunities/">Список подсообществ</a>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<div class="content-message- content-message-empty-">
						<p class="text-">Ни одного дочернего сообщества не найдено.</p>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
