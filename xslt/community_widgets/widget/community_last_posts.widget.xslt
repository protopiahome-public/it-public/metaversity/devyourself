<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="../../base_widgets/include/widget_posts_common.inc.xslt"/>
	<xsl:template match="community_widget_last_posts[1]" mode="widget">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<div class="widget widget-last-posts">
			<xsl:call-template name="draw_widget_header">
				<xsl:with-param name="title" select="$data/@title"/>
				<xsl:with-param name="light_header" select="true()"/>
			</xsl:call-template>
			<xsl:choose>
				<xsl:when test="@module_disabled = 1">
					<div class="content-message- content-message-access-">
						<p class="text-">Сообщества и записи в данном проекте отключены.</p>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="/root/community_widget_last_posts[2]">
						<div class="content-">
							<div class="buttons- buttons-active- wide-only-">
								<h3 class="toggle button-selected- button-" jq-toggle="jq-toggle-last-posts-main" jq-toggle-group="jq-toggle-last-posts" jq-toggle-active-element="jq-toggle-last-posts-main" jq-toggle-active-class="button-selected-">
									<span class="text-">Все записи</span>
								</h3>
								<h3 class="toggle button-" jq-toggle="jq-toggle-last-posts-own" jq-toggle-group="jq-toggle-last-posts" jq-toggle-active-element="jq-toggle-last-posts-own" jq-toggle-active-class="button-selected-">
									<span class="text-">Только этого сообщества</span>
								</h3>
								<h3 class="toggle button-" jq-toggle="jq-toggle-last-posts-sub" jq-toggle-group="jq-toggle-last-posts" jq-toggle-active-element="jq-toggle-last-posts-sub" jq-toggle-active-class="button-selected-">
									<span class="text-">Только подсообществ</span>
								</h3>
							</div>
							<div class="buttons- buttons-active- narrow-only-">
								<h3 class="toggle button-selected- button-" jq-toggle="jq-toggle-last-posts-main" jq-toggle-group="jq-toggle-last-posts" jq-toggle-active-element="jq-toggle-last-posts-main" jq-toggle-active-class="button-selected-">
									<span class="text-">Все</span>
								</h3>
								<h3 class="toggle button-" jq-toggle="jq-toggle-last-posts-own" jq-toggle-group="jq-toggle-last-posts" jq-toggle-active-element="jq-toggle-last-posts-own" jq-toggle-active-class="button-selected-">
									<span class="text-">Этого сообщества</span>
								</h3>
							</div>
						</div>
					</xsl:if>
					<div id="jq-toggle-last-posts-main" class="jq-toggle-last-posts">
						<xsl:call-template name="draw_widget_posts">
							<xsl:with-param name="data" select="$data"/>
							<xsl:with-param name="feed_url" select="concat($current_community, 'feed/')"/>
						</xsl:call-template>
					</div>
					<xsl:for-each select="/root/community_widget_last_posts[2]">
						<div id="jq-toggle-last-posts-own" class="jq-toggle-last-posts dn">
							<xsl:call-template name="draw_widget_posts">
								<xsl:with-param name="data" select="$data"/>
								<xsl:with-param name="feed_url" select="concat($current_community, 'feed/own/')"/>
							</xsl:call-template>
						</div>
					</xsl:for-each>
					<xsl:for-each select="/root/community_widget_last_posts[3]">
						<div id="jq-toggle-last-posts-sub" class="jq-toggle-last-posts dn">
							<xsl:call-template name="draw_widget_posts">
								<xsl:with-param name="data" select="$data"/>
								<xsl:with-param name="feed_url" select="concat($current_community, 'feed/sub/')"/>
							</xsl:call-template>
						</div>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
