<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="../../base_widgets/widget/base_communities.widget.xslt"/>
	<xsl:template match="community_widget_communities" mode="widget">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<xsl:call-template name="base_widget_communities">
			<xsl:with-param name="data" select="$data"/>
			<xsl:with-param name="can_moderate_widgets" select="$can_moderate_widgets"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>
