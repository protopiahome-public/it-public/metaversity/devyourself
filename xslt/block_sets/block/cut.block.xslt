<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="block[@type = 'cut']" mode="block_edit">
		<xsl:call-template name="draw_cut_block"/>
	</xsl:template>
	<xsl:template match="block[@type = 'cut']" mode="block_show">
		<xsl:param name="list" select="false()"/>
		<xsl:param name="object_url"/>
		<div class="block-cut-content-">
			<xsl:choose>
				<xsl:when test="$list">
					<p>
						<a href="{$object_url}#cut">
							<xsl:apply-templates select="." mode="block_content"/>
						</a>
					</p>
				</xsl:when>
				<xsl:otherwise>
					<a name="cut"/>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template match="block[@type = 'cut']" mode="block_content">
		<span class="jq-cut-title">
			<xsl:value-of select="@title"/>
		</span>
	</xsl:template>
	<xsl:template name="draw_cut_block">
		<xsl:param name="template" select="false()"/>
		<table data-block-type="cut">
			<xsl:attribute name="class">
				<xsl:if test="$template">dn jq-block-cut-empty </xsl:if>
				<xsl:text>jq-block jq-block-type-cut jq-block-to-init block- block-cut-</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="data-title">
				<xsl:choose>
					<xsl:when test="$template">Читать далее...</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@title"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<tr>
				<td class="l-">
					<div class="controls- dn">
						<div class="jq-move-handler btn-move-"/>
						<span class="icons- icons-small">
							<a class="delete- gray-" href="#" title="Удалить"/>
							<a class="edit- gray-" href="#" title="Редактировать"/>
						</span>
					</div>
				</td>
				<td class="r-">
					<div class="border-fix-">
						<div class="jq-block-content content- block-cut-content-">
							<div class="cut-info-outer-">
								<div class="cut-info-">
									<p>
										<a>
											<xsl:choose>
												<xsl:when test="$template">
													<span class="jq-cut-title">Читать далее...</span>
												</xsl:when>
												<xsl:otherwise>
													<xsl:apply-templates select="." mode="block_content"/>
												</xsl:otherwise>
											</xsl:choose>
										</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>
