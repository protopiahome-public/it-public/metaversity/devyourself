<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="block[@type = 'file']" mode="block_edit">
		<xsl:param name="object_url"/>
		<table class="jq-block jq-block-type-file jq-block-to-init block- block-file-" data-block-type="file" data-block-id="{@id}" data-size="{@size}" data-file-name="{@file_name}" data-title="{@title}">
			<tr>
				<td class="l-">
					<div class="controls- dn">
						<div class="jq-move-handler btn-move-"/>
						<span class="icons- icons-small">
							<a class="delete- gray-" href="#" title="Удалить"/>
							<a class="edit- gray-" href="#" title="Редактировать"/>
						</span>
					</div>
				</td>
				<td class="r-">
					<div class="border-fix-">
						<div class="jq-block-content content- block-file-content-">
							<xsl:apply-templates select="." mode="block_content">
								<xsl:with-param name="object_url" select="$object_url"/>
							</xsl:apply-templates>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template match="block[@type = 'file']" mode="block_show">
		<xsl:param name="object_url"/>
		<div class="block-file-content-">
			<xsl:apply-templates select="." mode="block_content">
				<xsl:with-param name="object_url" select="$object_url"/>
			</xsl:apply-templates>
		</div>
	</xsl:template>
	<xsl:template match="block[@type = 'file']" mode="block_content">
		<xsl:param name="object_url"/>
		<table class="file-info-">
			<tr>
				<xsl:if test="@file_type != ''">
					<td class="td-file-icon-">
						<a target="_blank" href="{$object_url}file-{@id}/">
							<span class="image- file-type file-type-{@file_type}">
								<b>
									<xsl:value-of select="@ext"/>
								</b>
							</span>
						</a>
					</td>
				</xsl:if>
				<td class="td-caption-">
					<a target="_blank" href="{$object_url}file-{@id}/">
						<xsl:value-of select="@title"/>
					</a>
					<span class="size- hl">
						<xsl:text> (</xsl:text>
						<!--<xsl:value-of select="@file_name"/>
						<xsl:text>, </xsl:text>-->
						<xsl:call-template name="file_size"/>
						<xsl:text>)</xsl:text>
					</span>
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>
