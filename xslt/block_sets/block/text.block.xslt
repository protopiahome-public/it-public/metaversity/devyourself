<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="block[@type = 'text']" mode="block_edit">
		<table class="jq-block jq-block-type-text jq-block-to-init block- block-text-" data-block-type="text">
			<tr>
				<td class="l-">
					<div class="controls- dn">
						<div class="jq-move-handler btn-move-"/>
						<span class="icons- icons-small">
							<a class="delete- gray-" href="#" title="Удалить"/>
							<a class="edit- gray-" href="#" title="Редактировать"/>
						</span>
					</div>
				</td>
				<td class="r-">
					<div class="border-fix-">
						<div class="jq-block-content content- block-text-content-">
							<xsl:apply-templates select="." mode="block_content"/>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template match="block[@type = 'text']" mode="block_show">
		<div class="block-text-content-">
			<xsl:apply-templates select="." mode="block_content"/>
		</div>
	</xsl:template>
	<xsl:template match="block[@type = 'text']" mode="block_content">
		<xsl:copy-of select="html/*"/>
	</xsl:template>
	<xsl:template name="draw_text_block_template">
		<table id="jq-block-text-editor" class="preload jq-block-type-text-editor block- block-text-editor-" data-block-type="text_editor">
			<tr>
				<td class="l-">
				</td>
				<td class="r-">
					<div class="jq-block-content content-">
						<textarea id="jq-block-text-editor-textarea">Initial</textarea>
					</div>
				</td>
			</tr>
		</table>		
	</xsl:template>
</xsl:stylesheet>
