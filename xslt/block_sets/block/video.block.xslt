<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="block[@type = 'video']" mode="block_edit">
		<table class="jq-block jq-block-type-video jq-block-to-init block- block-video-" data-block-type="video" data-block-id="{@id}" data-video-title="{@title}" data-video-code="{@html}">
			<tr>
				<td class="l-">
					<div class="controls- dn">
						<div class="jq-move-handler btn-move-"/>
						<span class="icons- icons-small">
							<a class="delete- gray-" href="#" title="Удалить"/>
							<a class="edit- gray-" href="#" title="Редактировать"/>
						</span>
					</div>
				</td>
				<td class="r-">
					<div class="border-fix-">
						<div class="jq-block-content content- block-video-content-">
							<xsl:apply-templates select="." mode="block_content"/>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template match="block[@type = 'video']" mode="block_show">
		<div class="block-video-content-">
			<xsl:apply-templates select="." mode="block_content"/>
		</div>
	</xsl:template>
	<xsl:template match="block[@type = 'video']" mode="block_content">
		<div class="video-">
			<xsl:if test="@title != ''">
				<h3 class="black">
					<xsl:value-of select="@title"/>
				</h3>
			</xsl:if>
			<xsl:copy-of select="html/div"/>
		</div>
	</xsl:template>
</xsl:stylesheet>
