<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../block/text.block.xslt"/>
	<xsl:include href="../block/file.block.xslt"/>
	<xsl:include href="../block/video.block.xslt"/>
	<xsl:include href="../block/cut.block.xslt"/>
	<xsl:template name="draw_block_set_show">
		<xsl:param name="list" select="false()"/>
		<xsl:param name="object_url"/>
		<div class="block-set">
			<xsl:choose>
				<xsl:when test="$list">
					<xsl:apply-templates mode="block_show" select="block[following-sibling::block[@type='cut'] or @type='cut' or not(../block[@type='cut'])]">
						<xsl:with-param name="list" select="$list"/>
						<xsl:with-param name="object_url" select="$object_url"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates mode="block_show" select="block">
						<xsl:with-param name="list" select="$list"/>
						<xsl:with-param name="object_url" select="$object_url"/>
					</xsl:apply-templates>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
