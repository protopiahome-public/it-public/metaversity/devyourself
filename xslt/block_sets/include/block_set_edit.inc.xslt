<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../block/text.block.xslt"/>
	<xsl:include href="../block/file.block.xslt"/>
	<xsl:include href="../block/video.block.xslt"/>
	<xsl:include href="../block/cut.block.xslt"/>
	<xsl:template name="draw_block_set_edit">
		<xsl:param name="object_url"/>
		<div class="field">
			<div class="block-set-editor-field-title-">
				<xsl:text>Контент:</xsl:text>
			</div>
			<div class="block-set-editor">
				<hr/>
				<div id="jq-block-set-buttons" class="buttons- freeze">
					<div class="left-">Добавить<br/>блок:</div>
					<div id="jq-block-set-btn-text" class="btn- btn-text-"/>
					<div id="jq-block-set-btn-video" class="btn- btn-video-"/>
					<div id="jq-block-set-btn-file" class="btn- btn-file-"/>
					<xsl:if test="@with_cut = 1">
						<div id="jq-block-set-btn-cut" class="btn- btn-cut-"/>
					</xsl:if>
				</div>
				<div class="jq-block-set-blocks block-set">
					<xsl:choose>
						<xsl:when test="$pass_info/block_set">
							<xsl:for-each select="$pass_info/block_set">
								<xsl:apply-templates mode="block_edit" select="block">
									<xsl:with-param name="object_url" select="$object_url"/>
								</xsl:apply-templates>
					</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates mode="block_edit" select="block">
								<xsl:with-param name="object_url" select="$object_url"/>
							</xsl:apply-templates>
						</xsl:otherwise>
					</xsl:choose>
				</div>
				<div class="block-set">
					<table class="block- block-submit-" data-block-type="submit">
						<tr>
							<td class="l-">
								<div class="controls-"/>
							</td>
							<td class="r-">
								<div class="content-">
									<button id="jq-block-set-save-btn" class="button">Сохранить</button>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<table class="dn jq-block-text-empty jq-block jq-block-type-text jq-block-to-init block- block-text-" data-block-type="text">
					<tr>
						<td class="l-">
							<div class="controls- dn">
								<div class="jq-move-handler btn-move-"/>
								<span class="icons- icons-small">
									<a class="delete- gray-" href="#" title="Удалить"/>
									<a class="edit- gray-" href="#" title="Редактировать"/>
								</span>
							</div>
						</td>
						<td class="r-">
							<div class="border-fix-">
								<div class="jq-block-content content- block-text-content-">
									<xsl:text>EmptyBlock</xsl:text>
								</div>
							</div>
						</td>
					</tr>
				</table>
				<xsl:call-template name="draw_cut_block">
					<xsl:with-param name="template" select="true()"/>
				</xsl:call-template>
				<xsl:call-template name="draw_text_block_template"/>
				<div id="jq-block-set-temp-container" class=""/>
			</div>
			<input id="block_set_data" name="block_set_data" type="hidden" value=""/>
			<input id="block_set_id" name="block_set_id" type="hidden" value="{@id}"/>
			<div id="jq-block-set-window-file-dialog" class="block-set-window dn">
				<div class="overlay-"/>
				<div id="jq-block-set-window-file-lightbox" class="win- win-file-">
					<div class="head-">
						<div id="jq-block-set-window-file-close" class="close-"/>
						<div class="title-">Свойства файла</div>
					</div>
					<div class="file-input-real-container-">
						<div id="file-input-real-container-inner" class="file-input-real-container-inner-">
							<div id="jq-block-set-window-file-file-input"/>
						</div>
					</div>
					<div id="jq-block-set-window-file-content-main" class="content-">
						<div id="jq-block-set-window-file-reupload-container" class="b-reupload-">
							<span>Загрузить другой файл вместо существующего</span>
						</div>
						<div id="jq-block-set-window-file-file-input-container" class="b-file-">
							<div class="file-">
								<div class="button-placeholder-"/>
								<!--<input id="jq-block-set-window-file-file-input" type="file" name="jq-block-set-window-file-input"/>-->
								<input id="jq-block-set-window-file-name" type="text" class="file-name-" value="" readonly="readonly"/>
							</div>
							<div id="jq-block-set-window-file-file-error" class="error-">
								<span>Ошибка</span>
							</div>
							<div class="clear"/>
						</div>
						<div id="jq-block-set-window-file-title-container" class="b-string-">
							<label for="jq-block-set-window-file-title">Название ссылки:</label>
							<input id="jq-block-set-window-file-title" type="text" maxlength="255"/>
							<div id="jq-block-set-window-file-title-error" class="error-">
								<span>Ошибка</span>
							</div>
							<div class="clear"/>
						</div>
						<div class="b-btn-">
							<button class="button button-small jq-manual-submit" id="jq-block-set-window-file-submit">Сохранить</button>
						</div>
					</div>
					<div id="jq-block-set-window-file-content-submitting" class="content-submitting-">
						<div class="b-submitting-head-">Сохранение...</div>
						<div class="b-submitting-loader-"/>
						<div class="b-submitting-cancel-">
							<span id="jq-block-set-window-file-cancel">Прервать сохранение</span>
						</div>
					</div>
					<div id="jq-block-set-window-file-content-submitting-file" class="content-submitting- content-submitting-file-">
						<div class="b-submitting-head-">Загрузка файла</div>
						<div class="b-submitting-loader-">
							<div id="jq-block-set-window-file-submitting-loader-fill" class="loader-fill-"/>
						</div>
						<div class="b-submitting-cancel-">
							<span id="jq-block-set-window-file-cancel-upload">Прервать загрузку</span>
						</div>
					</div>
				</div>
			</div>
			<div id="jq-block-set-window-video-dialog" class="block-set-window dn">
				<div class="overlay-"/>
				<div id="jq-block-set-window-video-lightbox" class="win- win-video-">
					<div class="head-">
						<div id="jq-block-set-window-video-close" class="close-"/>
						<div class="title-">Свойства видео</div>
					</div>
					<div id="jq-block-set-window-video-content-main" class="content-">
						<div class="b-text-">
							<label for="jq-block-set-window-video-code-input">Ссылка на видео или embed-код:</label>
							<textarea rows="20" cols="4" id="jq-block-set-window-video-code-input"/>
							<div id="jq-block-set-window-video-code-error" class="error-">
								<span>Ошибка</span>
							</div>
							<div id="jq-block-set-window-video-code-error-long" class="error- error-long-">
								<span>Ошибка</span>
							</div>
							<div class="comment-">Просто ссылку можно вводить для YouTube, Smotri.com, Google Video. Мы сами преобразуем её в embed-код.</div>
							<div class="clear"/>
						</div>
						<div class="b-string-">
							<label for="jq-block-set-window-video-title-input">Заголовок видео (не обязательно):</label>
							<input id="jq-block-set-window-video-title-input" type="text" maxlength="255"/>
							<div id="jq-block-set-window-video-title-error" class="error-">
								<span>Ошибка</span>
							</div>
							<div class="clear"/>
						</div>
						<div class="b-btn-">
							<button class="button button-small jq-manual-submit" id="jq-block-set-window-video-submit">Сохранить</button>
						</div>
					</div>
					<div id="jq-block-set-window-video-content-submitting" class="content-submitting-">
						<div class="b-submitting-head-">Сохранение...</div>
						<div class="b-submitting-loader-"/>
						<div class="b-submitting-cancel-">
							<span id="jq-block-set-window-video-cancel">Прервать сохранение</span>
						</div>
					</div>
				</div>
			</div>
			<div id="jq-block-set-window-cut-dialog" class="block-set-window dn">
				<div class="overlay-"/>
				<div id="jq-block-set-window-cut-lightbox" class="win- win-cut-">
					<div class="head-">
						<div id="jq-block-set-window-cut-close" class="close-"/>
						<div class="title-">Обрезание текста</div>
					</div>
					<div id="jq-block-set-window-cut-content-main" class="content-">
						<div class="b-string-">
							<label for="jq-block-set-window-cut-title-input">Текст ссылки для перехода к нижней части статьи:</label>
							<input id="jq-block-set-window-cut-title-input" type="text" maxlength="255"/>
							<div id="jq-block-set-window-cut-title-error" class="error-">
								<span>Ошибка</span>
							</div>
							<div class="clear"/>
						</div>
						<div class="b-btn-">
							<button class="button button-small button-gray jq-manual-submit" id="jq-block-set-window-cut-submit">Сохранить</button>
						</div>
					</div>
				</div>
			</div>
			<div id="jq-block-set-window-error-dialog" class="block-set-window dn">
				<div class="overlay-"/>
				<div id="jq-block-set-window-error-lightbox" class="win- win-error-">
					<div class="head-">
						<div id="jq-block-set-window-error-close" class="close-"/>
						<div class="title-" id="jq-block-set-window-error-title">Ошибка</div>
					</div>
					<div class="content-">
						<div class="b-msg-" id="jq-block-set-window-error-message"/>
						<div class="b-btn-">
							<button class="button button-small button-red jq-manual-submit" id="jq-block-set-window-error-submit">ОК</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
