<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_common.xslt"/>
	<xsl:variable name="object_url" select="/root/object_url"/>
	<xsl:template match="block">
		<xsl:apply-templates mode="block_edit" select=".">
			<xsl:with-param name="object_url" select="$object_url"/>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="text()"/>
</xsl:stylesheet>
