<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="vector_base_url" select="concat($current_project/@url, 'vector/')"/>
	<xsl:variable name="current_user" select="$user"/>
	<xsl:variable name="user_title">
		<xsl:value-of select="$current_user/@any_name"/>
		<xsl:if test="$current_user/@any_name != $current_user/@login">
			<xsl:text> (</xsl:text>
			<xsl:value-of select="$current_user/@login"/>
			<xsl:text>)</xsl:text>
		</xsl:if>
	</xsl:variable>
	<xsl:template name="draw_project_vector_head2">
		<div class="head2">
			<table class="head2-table-">
				<tr>
					<td class="head2-table-right- no-avatar-">
						<div class="menu-">
							<div class="menu-item-">
								<xsl:if test="$vector_section = 'intro'">
									<xsl:attribute name="class">menu-item- menu-item-selected-</xsl:attribute>
								</xsl:if>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="$vector_section = 'intro' and $vector_section_main_page and not($get_vars)">Что такое вектор</xsl:when>
												<xsl:otherwise>
													<a href="{$vector_base_url}intro/">Что такое вектор</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
							<div class="menu-sep-">|</div>
							<div class="menu-item-">
								<xsl:if test="$vector_section = 'my'">
									<xsl:attribute name="class">menu-item- menu-item-selected-</xsl:attribute>
								</xsl:if>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="$vector_section = 'my' and $vector_section_main_page and not($get_vars)">Мой вектор</xsl:when>
												<xsl:otherwise>
													<a href="{$vector_base_url}my/">Мой вектор</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
							<div class="menu-sep-">|</div>
							<div class="menu-item-">
								<xsl:if test="$vector_section = 'pass'">
									<xsl:attribute name="class">menu-item- menu-item-selected-</xsl:attribute>
								</xsl:if>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="$vector_section = 'pass' and $vector_section_main_page and not($get_vars)">Пройти вектор</xsl:when>
												<xsl:otherwise>
													<a href="{$vector_base_url}future/">Пройти вектор</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
							<div class="menu-sep-">|</div>
							<div class="menu-item-">
								<xsl:if test="$vector_section = 'stat-choice'">
									<xsl:attribute name="class">menu-item- menu-item-selected-</xsl:attribute>
								</xsl:if>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="$vector_section = 'stat-choice' and $vector_section_main_page and not($get_vars)">Статистика моего развития</xsl:when>
												<xsl:otherwise>
													<a href="{$vector_base_url}stat-choice/">Статистика моего развития</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
