<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="../project/head2/project_user_head2.inc.xslt"/>
	<xsl:include href="include/stat_choice.inc.xslt"/>
	<xsl:include href="menu/stat_choice_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'users'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'stat-choice'"/>
	<xsl:variable name="user_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_user_base_url, 'stat-choice/')"/>
	<xsl:template match="project_user_stat_choice">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_project_user_head2"/>
							<h2>Статистика развития</h2>
							<div class="content">
								<xsl:call-template name="draw_stat_choice_submenu"/>
								<xsl:call-template name="draw_stat_choice">
									<xsl:with-param name="foreign" select="true()"/>
								</xsl:call-template>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Статистика развития &#8212; </xsl:text>
		<xsl:value-of select="$user_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
