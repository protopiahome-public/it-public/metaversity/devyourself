<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_form_focus">
		<xsl:if test="not(@step_required)">
			<xsl:if test="$pass_info/error[@name = 'TOO_SMALL_COUNT']">
				<div class="error">
					<span>Выбрано слишком мало компетенций (<xsl:value-of select="$pass_info/error[@name = 'TOO_SMALL_COUNT']"/>). Можно поставить от 1 до 10 галочек.</span>
				</div>
			</xsl:if>
			<xsl:if test="$pass_info/error[@name = 'TOO_BIG_COUNT']">
				<div class="error">
					<span>Выбрано слишком много компетенций (<xsl:value-of select="$pass_info/error[@name = 'TOO_BIG_COUNT']"/>). Можно поставить от 1 до 10 галочек.</span>
				</div>
			</xsl:if>
			<form method="post" action="{$save_prefix}/vector_focus/">
				<input type="hidden" name="project_id" value="{$current_project/@id}"/>
				<input type="hidden" name="retpath" value="{$current_project/@url}vector/recommend/"/>
				<xsl:call-template name="draw_vector_step2_step3_detailed"/>
				<div>
					<button class="button">
						<xsl:text>Далее </xsl:text>
						<span class="arrow-">→</span>
					</button>
				</div>
			</form>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
