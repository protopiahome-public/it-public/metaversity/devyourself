<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_step1_rose">
		<xsl:variable name="module_context" select="."/>
		<div>
			<a name="future"/>
		</div>
		<xsl:for-each select="/root/vector[1]">
			<div style="height: 7px; font-size: 1px; line-height: 1px;"> </div>
			<table cellpadding="0" cellspacing="0" class="vector-resume-step">
				<tr class="head-">
					<td class="step-">
						<div>Шаг 1</div>
					</td>
					<td class="title-">
						<table class="vector-step-caption vector-step-caption-red-">
							<tr>
								<td>
									<div>«Я в будущем»</div>
								</td>
							</tr>
						</table>
					</td>
					<td class="question-">
						<span>— «Кем я хочу быть по профессии?»</span>
					</td>
				</tr>
				<tr class="body-">
					<td> </td>
					<td colspan="2" class="content-">
						<span>
							<xsl:choose>
								<xsl:when test="@future_professiogram_count &gt; 0">
									<strong>
										<xsl:call-template name="count_case">
											<xsl:with-param name="number" select="@future_professiogram_count"/>
											<xsl:with-param name="word_ns" select="'Выбрана '"/>
											<xsl:with-param name="word_gs" select="'Выбраны '"/>
											<xsl:with-param name="word_ap" select="'Выбрано '"/>
										</xsl:call-template>
									</strong>
									<xsl:value-of select="@future_professiogram_count"/>
									<xsl:call-template name="count_case">
										<xsl:with-param name="number" select="@future_professiogram_count"/>
										<xsl:with-param name="word_ns" select="' профессия: '"/>
										<xsl:with-param name="word_gs" select="' профессии: '"/>
										<xsl:with-param name="word_ap" select="' профессий: '"/>
									</xsl:call-template>
									<xsl:for-each select="future/professiogram">
										<xsl:value-of select="@title"/>
										<xsl:choose>
											<xsl:when test="position() != last()">, </xsl:when>
											<xsl:otherwise>.</xsl:otherwise>
										</xsl:choose>
									</xsl:for-each>
									<xsl:if test="future_competences/competence">
										<span class="minor">
											<span id="jq-vector-step1-rose-expand-future" class="note clickable">Показать компетенции</span>
										</span>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>Профиль не заполнен.</xsl:otherwise>
							</xsl:choose>
						</span>
						<!--<span class="change-">
							<a class="hl" href="#">Изменить данные</a>
						</span>-->
					</td>
				</tr>
				<tr class="body2- dn" id="jq-vector-step1-rose-competences">
					<td colspan="3" class="content-">
						<xsl:if test="future_competences/competence">
							<xsl:for-each select="$module_context">
								<xsl:call-template name="draw_rose">
									<xsl:with-param name="competences" select="future_rose_competences"/>
									<xsl:with-param name="image_alt" select="'Компетенции &#171;Я в будущем&#187; и результаты'"/>
									<xsl:with-param name="marks_default_project_id" select="@project_id"/>
									<xsl:with-param name="show_self_by_default" select="/root/project_user_vector/project_comparison/@show_self_by_default = 1"/>
									<xsl:with-param name="rose_param_name" select="'img-future'"/>
									<xsl:with-param name="focus_competences" select="/root/vector/focus/competence"/>
								</xsl:call-template>
							</xsl:for-each>
						</xsl:if>
					</td>
				</tr>
			</table>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
