<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../../_site/include/tree.inc.xslt"/>
	<xsl:variable name="rate_competences" select="/root/vector/self/competence"/>
	<xsl:variable name="rate_options" select="/root/project_vector_self/options/option"/>
	<xsl:template name="draw_vector_form_self">
		<h3>Чтобы понять, над какими компетенциями необходимо работать, заполните<br/>самооценку по компетенциям, которые входят в выбранные вами профессии:</h3>
		<xsl:if test="$pass_info/info[@name = 'SELF_NOT_SET'] or $get_vars[@name = 'warning'] = 1">
			<div class="error">
				<span>Для перехода к следующему шагу необходимо заполнить самооценку по всем компетенциям</span>
			</div>
		</xsl:if>
		<form method="post" action="{$save_prefix}/vector_self/">
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<input type="hidden" name="retpath" value="{$current_project/@url}vector/focus/"/>
			<xsl:call-template name="draw_tree_head"/>
			<div class="tree">
				<xsl:call-template name="draw_tree_competence_group">
					<xsl:with-param name="competences" select="/root/vector/future_competences/competence"/>
					<xsl:with-param name="competence_is_clickable" select="false()"/>
					<xsl:with-param name="show_bars" select="false()"/>
				</xsl:call-template>
			</div>
			<div style="margin-top: 12px;">
				<button class="button">
					<xsl:text>Далее </xsl:text>
					<span class="arrow-">→</span>
				</button>
			</div>
		</form>
	</xsl:template>
	<xsl:template mode="tree_right_th" match="group">
		<th class="rate-select-">
			<div>
				<strong style="margin-right: 20px;">Самооценка</strong>
			</div>
		</th>
	</xsl:template>
	<xsl:template mode="tree_right_td" match="competence">
		<td class="rate-select-">
			<div>
				<select name="competence-{current()/@id}">
					<xsl:variable name="current_competence" select="current()"/>
					<xsl:for-each select="$rate_options">
						<option value="{@mark}">
							<xsl:choose>
								<xsl:when test="$rate_competences[@id = $current_competence/@id]/@mark = @mark">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:when>
								<xsl:when test="$pass_info/vars/var[@name = 'rates']/item[@index = $current_rate/@mark] = current()/@id">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:when>
							</xsl:choose>
							<xsl:value-of select="@mark_title"/>
						</option>
					</xsl:for-each>
				</select>
			</div>
		</td>
	</xsl:template>
</xsl:stylesheet>
