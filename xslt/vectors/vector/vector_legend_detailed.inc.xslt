<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_legend_detailed">
		<xsl:call-template name="draw_legend">
			<xsl:with-param name="type" select="'bars'"/>
			<xsl:with-param name="need_future" select="true()"/>
			<xsl:with-param name="sum_current" select="$module/results/competence"/>
			<xsl:with-param name="delta" select="true()"/>
			<xsl:with-param name="self" select="true()"/>
			<xsl:with-param name="focus" select="true()"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>
