<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_no_data">
		<xsl:if test="not(/root/vector[1])">
			<p>Пользователь не прошёл этот вектор.</p>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
