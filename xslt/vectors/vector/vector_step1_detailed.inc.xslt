<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_step1_detailed">
		<xsl:for-each select="/root/vector[1]">
			<h3 style="margin-bottom: 10px;">
				<xsl:choose>
					<xsl:when test="$vector_for_form">
						<xsl:text>Итак, Вы выбрали </xsl:text>
						<xsl:value-of select="@future_professiogram_count"/>
						<xsl:call-template name="count_case">
							<xsl:with-param name="number" select="@future_professiogram_count"/>
							<xsl:with-param name="word_ns" select="' профессию'"/>
							<xsl:with-param name="word_gs" select="' профессии'"/>
							<xsl:with-param name="word_ap" select="' профессий'"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Профиль &#171;Я в будущем&#187; &#8212; </xsl:text>
						<xsl:call-template name="count_case">
							<xsl:with-param name="number" select="@future_professiogram_count"/>
							<xsl:with-param name="word_ns" select="' выбрана '"/>
							<xsl:with-param name="word_gs" select="' выбраны '"/>
							<xsl:with-param name="word_ap" select="' выбрано '"/>
						</xsl:call-template>
						<xsl:value-of select="@future_professiogram_count"/>
						<xsl:call-template name="count_case">
							<xsl:with-param name="number" select="@future_professiogram_count"/>
							<xsl:with-param name="word_ns" select="' профессия'"/>
							<xsl:with-param name="word_gs" select="' профессии'"/>
							<xsl:with-param name="word_ap" select="' профессий'"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="@future_professiogram_count &gt; 0">:</xsl:if>
			</h3>
			<xsl:if test="@future_professiogram_count &gt; 0">
				<table class="tbl">
					<tr>
						<th/>
						<xsl:for-each select="$module">
							<xsl:call-template name="draw_tbl_th">
								<xsl:with-param name="name" select="'title'"/>
								<xsl:with-param name="title" select="'Профессия'"/>
								<xsl:with-param name="no_links" select="true()"/>
							</xsl:call-template>
							<xsl:if test="$module/results/competence">
								<xsl:call-template name="draw_tbl_th">
									<xsl:with-param name="name" select="'match'"/>
									<xsl:with-param name="title" select="'Соответствие по результатам проектов'"/>
									<xsl:with-param name="no_links" select="true()"/>
								</xsl:call-template>
							</xsl:if>
							<xsl:call-template name="draw_tbl_th">
								<xsl:with-param name="name" select="'match-self'"/>
								<xsl:with-param name="title" select="'Соответствие из самооценки'"/>
								<xsl:with-param name="no_links" select="true()"/>
							</xsl:call-template>
						</xsl:for-each>
					</tr>
					<xsl:for-each select="future/professiogram">
						<tr>
							<td class="">
								<xsl:variable name="prof_index" select="$module/professiograms/professiogram[@id = current()/@id]/@index"/>
								<div class="vector-prof-index vector-prof-index-{($prof_index - 1) mod 5 + 1}-">
									<xsl:value-of select="$prof_index"/>
								</div>
							</td>
							<td class="col-longer-">
								<xsl:value-of select="@title"/>
							</td>
							<xsl:if test="$module/results/competence">
								<td class="col-digits-">
									<xsl:value-of select="$module/professiograms/professiogram[@id = current()/@id]/@match"/>
									<xsl:text>%</xsl:text>
								</td>
							</xsl:if>
							<td class="col-digits-">
								<xsl:value-of select="@match"/>
								<xsl:text>%</xsl:text>
							</td>
						</tr>
					</xsl:for-each>
				</table>
				<!--<p>
					<a class="hl" href="#">Выбрать другие профессии</a>
				</p>-->
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
