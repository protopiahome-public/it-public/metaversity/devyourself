<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_no_my_data">
		<xsl:if test="not(/root/vector[1])">
			<p>Вы ещё не прошли этот вектор. <a href="{$current_project/@url}vector/future/">Пройти?</a></p>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
