<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_step2_rose">
		<div>
			<a name="self"/>
		</div>
		<xsl:for-each select="/root/vector[1]">
			<table cellpadding="0" cellspacing="0" class="vector-resume-step">
				<tr class="head-">
					<td class="step-">
						<div>Шаг 2</div>
					</td>
					<td class="title-">
						<table class="vector-step-caption vector-step-caption-red-">
							<tr>
								<td>
									<div>Самооценка</div>
								</td>
							</tr>
						</table>
					</td>
					<td class="question-">
						<span>— «Каков мой сегодняшний уровень развития?»</span>
					</td>
				</tr>
				<tr class="body-">
					<td> </td>
					<td colspan="2" class="content-">
						<span>
							<xsl:choose>
								<xsl:when test="self/@mark_count &gt; 0">
									<strong>Заполнена</strong>
									<xsl:text> для </xsl:text>
									<xsl:value-of select="self/@mark_count"/>
									<xsl:call-template name="count_case">
										<xsl:with-param name="number" select="self/@mark_count"/>
										<xsl:with-param name="word_ns" select="' компетенции.'"/>
										<xsl:with-param name="word_gs" select="' компетенций.'"/>
										<xsl:with-param name="word_ap" select="' компетенций.'"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<strong>Не заполнена.</strong>
								</xsl:otherwise>
							</xsl:choose>
						</span>
						<!--<span class="change-">
							<a class="hl" href="#">Изменить данные</a>
						</span>-->
					</td>
				</tr>
			</table>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
