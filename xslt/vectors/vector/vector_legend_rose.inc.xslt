<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_legend_rose">
		<xsl:call-template name="draw_legend">
			<xsl:with-param name="type" select="'rose'"/>
			<xsl:with-param name="need_future" select="true()"/>
			<xsl:with-param name="sum_current" select="true()"/>
			<xsl:with-param name="self" select="true()"/>
			<xsl:with-param name="focus" select="true()"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>
