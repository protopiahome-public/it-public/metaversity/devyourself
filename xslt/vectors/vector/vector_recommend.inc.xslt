<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_recommend">
		<xsl:if test="not($current_project/@role_recommendation_is_on = 1) and not($current_project/@events_are_on = 1) and not($current_project/@materials_are_on = 1)">
			<p>Ничего не найдено :-(</p>
		</xsl:if>
		<xsl:if test="$current_project/@role_recommendation_is_on = 1">
			<p>
				<a href="{$current_project/@url}roles/">Рекомендация ролей</a>
			</p>
		</xsl:if>
		<xsl:if test="$current_project/@events_are_on = 1">
			<p>
				<a href="{$current_project/@url}events/">События</a>
			</p>
		</xsl:if>
		<xsl:if test="$current_project/@materials_are_on = 1">
			<p>
				<a href="{$current_project/@url}materials/">Материалы</a>
			</p>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
