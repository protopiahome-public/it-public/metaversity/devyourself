<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="vector_recommend.inc.xslt"/>
	<xsl:template name="draw_vector_step4">
		<xsl:param name="detailed" select="false()"/>
		<xsl:for-each select="/root/vector[1]">
			<table cellpadding="0" cellspacing="0" class="vector-resume-step">
				<tr class="head-">
					<xsl:if test="not($detailed)">
						<td class="step-">
							<div>Шаг 4</div>
						</td>
					</xsl:if>
					<td class="title-">
						<table class="vector-step-caption vector-step-caption-green-">
							<tr>
								<td>
									<div>Рекомендации</div>
								</td>
							</tr>
						</table>
					</td>
					<td class="question-">
						<span>— «Где это можно развивать?»</span>
					</td>
				</tr>
				<tr class="body-">
					<xsl:if test="not($detailed)">
						<td> </td>
					</xsl:if>
					<td colspan="2" class="content-">
						<xsl:call-template name="draw_vector_recommend"/>
					</td>
				</tr>
			</table>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
