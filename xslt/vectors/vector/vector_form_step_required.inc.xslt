<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_form_step_required">
		<xsl:if test="@step_required = 1">
			<p>Сначала вам необходимо заполнить <a href="{$current_project/@url}vector/future/">профиль &#171;Я в будущем&#187;</a>.</p>
		</xsl:if>
		<xsl:if test="@step_required = 2">
			<p>Сначала вам необходимо заполнить <a href="{$current_project/@url}vector/self/?warning=1">самооценку</a>.</p>
		</xsl:if>
		<xsl:if test="@step_required = 3">
			<p>Сначала вам необходимо заполнить <a href="{$current_project/@url}vector/focus/">фокус</a>.</p>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
