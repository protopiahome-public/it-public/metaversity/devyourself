<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_form_future_top">
		<h3>Выберите, кем по профессии вы себя видите:</h3>
		<p>Можно поставить от 1 до 4 галочек.</p>
	</xsl:template>
	<xsl:template name="draw_vector_form_future">
		<xsl:if test="not(professiogram)">
			<p>Ни одной профессии не найдено. Вероятно, администраторы проекта ещё не произвели нужную настройку.</p>
		</xsl:if>
		<xsl:if test="professiogram">
			<xsl:if test="$pass_info/error[@name = 'TOO_SMALL_COUNT']">
				<div class="error">
					<span>Выбрано слишком мало профессий (<xsl:value-of select="$pass_info/error[@name = 'TOO_SMALL_COUNT']"/>). Можно поставить от 1 до 4 галочек.</span>
				</div>
			</xsl:if>
			<xsl:if test="$pass_info/error[@name = 'TOO_BIG_COUNT']">
				<div class="error">
					<span>Выбрано слишком много профессий (<xsl:value-of select="$pass_info/error[@name = 'TOO_BIG_COUNT']"/>). Можно поставить от 1 до 4 галочек.</span>
				</div>
			</xsl:if>
			<form id="jq-vector-form-future" method="post" action="{$save_prefix}/vector_future/">
				<input type="hidden" name="project_id" value="{$current_project/@id}"/>
				<input type="hidden" name="retpath" value="{$current_project/@url}vector/self/"/>
				<table class="tbl">
					<tr>
						<th/>
						<xsl:call-template name="draw_tbl_th">
							<xsl:with-param name="name" select="'title'"/>
							<xsl:with-param name="title" select="'Профессия'"/>
						</xsl:call-template>
						<xsl:if test="professiogram[1][@match]">
							<xsl:call-template name="draw_tbl_th">
								<xsl:with-param name="name" select="'match'"/>
								<xsl:with-param name="title" select="'Соответствие&#160;по&#160;результатам&#160;предыдущих&#160;проектов'"/>
							</xsl:call-template>
						</xsl:if>
					</tr>
					<xsl:for-each select="professiogram">
						<tr>
							<td class="col-checkbox-">
								<input type="checkbox" id="prof-{@id}" name="prof-{@id}" value="1">
									<xsl:choose>
										<xsl:when test="$pass_info/vars/var[@name = concat('prof-', current()/@id)]">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:when>
										<xsl:when test="$pass_info/vars"/>
										<xsl:when test="@checked = 1">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:when>
									</xsl:choose>
								</input>
							</td>
							<td>
								<label for="prof-{@id}">
									<xsl:value-of select="@title"/>
								</label>
							</td>
							<xsl:if test="@match">
								<td class="col-digits-">
									<xsl:value-of select="@match"/>
									<xsl:text>%</xsl:text>
								</td>
							</xsl:if>
						</tr>
					</xsl:for-each>
				</table>
				<div style="margin-top: 12px;">
					<button class="button">
						<xsl:text>Далее </xsl:text>
						<span class="arrow-">→</span>
					</button>
				</div>
			</form>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
