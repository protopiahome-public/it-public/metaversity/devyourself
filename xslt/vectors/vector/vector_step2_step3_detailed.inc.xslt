<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../../_site/include/tree.inc.xslt"/>
	<xsl:template name="draw_vector_step2_step3_detailed">
		<xsl:for-each select="/root/vector[1]">
			<xsl:choose>
				<xsl:when test="$vector_for_form">
					<h3 class="down">Теперь выберите, на развитии каких компетенций Вы хотите сфокусироваться в первую очередь:</h3>
					<p>Можно поставить от 1 до 10 галочек.</p>
				</xsl:when>
				<xsl:otherwise>
					<h3 class="down">Самооценка и компетенции, находящиеся в фокусе развития в данный момент:</h3>
					<p>
						<xsl:text>(Можно было поставить </xsl:text>
						<strong>
							<xsl:text>от </xsl:text>
							<xsl:value-of select="/root/vector_config/@vector_focus_min_competence_count"/>
							<xsl:text> до </xsl:text>
							<xsl:value-of select="/root/vector_config/@vector_focus_max_competence_count"/>
							<xsl:text> галочек</xsl:text>
						</strong>
						<xsl:if test="/root/vector_config/@vector_focus_can_skip = 1"> или пропустить этот шаг</xsl:if>
						<xsl:text>. Фокусные компетенции отмечены </xsl:text>
						<strong>жирным шрифтом</strong>
						<xsl:text>.)</xsl:text>
					</p>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:call-template name="draw_tree_head"/>
			<xsl:call-template name="_draw_tree"/>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="_draw_tree">
		<div class="tree" style="margin-bottom: 40px;">
			<xsl:call-template name="draw_tree_competence_group">
				<xsl:with-param name="competences" select="/root/vector/future_competences/competence | focus/competence"/>
				<xsl:with-param name="focus_competences" select="/root/vector/focus/competence"/>
				<xsl:with-param name="competence_is_clickable" select="not($vector_for_form)"/>
				<xsl:with-param name="draw_checkboxes" select="$vector_for_form"/>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:variable name="tree_competences_detailed" select="/"/>
	<xsl:template mode="tree_marks" match="competence">
		<xsl:variable name="need_competences" select="/root/vector/future_competences/competence"/>
		<xsl:variable name="results_competences" select="$module/results/competence"/>
		<xsl:variable name="self_competences" select="/root/vector/self/competence"/>
		<xsl:variable name="current_competence_id" select="@id"/>
		<xsl:variable name="need_mark">
			<xsl:choose>
				<xsl:when test="$need_competences[@id = current()/@id]">
					<xsl:value-of select="$need_competences[@id = current()/@id]/@mark"/>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:call-template name="draw_tree_bar">
			<xsl:with-param name="type" select="'need'"/>
			<xsl:with-param name="mark" select="$need_mark"/>
			<xsl:with-param name="title" select="'Уровень, необходимый для профиля &#171;Я в будущем&#187;'"/>
			<xsl:with-param name="first" select="true()"/>
		</xsl:call-template>
		<xsl:if test="$results_competences">
			<xsl:call-template name="draw_tree_bar">
				<xsl:with-param name="type" select="'sum'"/>
				<xsl:with-param name="mark">
					<xsl:choose>
						<xsl:when test="$results_competences[@id = $current_competence_id]">
							<xsl:value-of select="$results_competences[@id = $current_competence_id]/@mark"/>
						</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="title" select="'Результат'"/>
				<xsl:with-param name="block_class" select="'jq-tree-bars-bar-calculations-base jq-tree-bars-bar-calculations-base-results'"/>
				<xsl:with-param name="draw_delta" select="true()"/>
				<xsl:with-param name="need_mark" select="$need_mark"/>
				<xsl:with-param name="need_mark_title_dative" select="'профилю &#171;Я в будущем&#187;'"/>
				<xsl:with-param name="hide" select="not($module/project_comparison/@calculations_base = 'results')"/>
			</xsl:call-template>
			<xsl:call-template name="draw_tree_bar">
				<xsl:with-param name="type" select="'self'"/>
				<xsl:with-param name="mark">
					<xsl:choose>
						<xsl:when test="$self_competences[@id = current()/@id]">
							<xsl:value-of select="$self_competences[@id = current()/@id]/@mark"/>
						</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="title" select="'Самооценка'"/>
				<xsl:with-param name="block_class" select="'jq-tree-bars-bar-self jq-tree-bars-bar-calculations-base jq-tree-bars-bar-calculations-base-results'"/>
				<xsl:with-param name="hide" select="not($module/project_comparison/@show_self_by_default = 1) or not($module/project_comparison/@calculations_base = 'results')"/>
			</xsl:call-template>
			<xsl:call-template name="draw_tree_bar">
				<xsl:with-param name="type" select="'sum'"/>
				<xsl:with-param name="mark">
					<xsl:choose>
						<xsl:when test="$results_competences[@id = $current_competence_id]">
							<xsl:value-of select="$results_competences[@id = $current_competence_id]/@mark"/>
						</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="title" select="'Результат'"/>
				<xsl:with-param name="block_class" select="'jq-tree-bars-bar-calculations-base jq-tree-bars-bar-calculations-base-self'"/>
				<xsl:with-param name="hide" select="not($module/project_comparison/@calculations_base = 'self')"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:call-template name="draw_tree_bar">
			<xsl:with-param name="type" select="'self'"/>
			<xsl:with-param name="mark">
				<xsl:choose>
					<xsl:when test="$self_competences[@id = current()/@id]">
						<xsl:value-of select="$self_competences[@id = current()/@id]/@mark"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="title" select="'Самооценка'"/>
			<xsl:with-param name="block_class" select="'jq-tree-bars-bar-self jq-tree-bars-bar-calculations-base jq-tree-bars-bar-calculations-base-self'"/>
			<xsl:with-param name="draw_delta" select="true()"/>
			<xsl:with-param name="need_mark" select="$need_mark"/>
			<xsl:with-param name="need_mark_title_dative" select="'профилю &#171;Я в будущем&#187;'"/>
			<xsl:with-param name="hide" select="$results_competences and (not($module/project_comparison/@show_self_by_default = 1) or not($module/project_comparison/@calculations_base = 'self'))"/>
		</xsl:call-template>
		<xsl:for-each select="$results_competences[@id = current()/@id]/project">
			<xsl:call-template name="draw_tree_bar">
				<xsl:with-param name="type" select="'case2'"/>
				<xsl:with-param name="mark" select="@mark"/>
				<xsl:with-param name="title" select="' '"/>
				<xsl:with-param name="draw_delta" select="false()"/>
				<xsl:with-param name="block_class" select="concat('jq-tree-bars-bar-project jq-tree-bars-bar-project-', @key)"/>
				<xsl:with-param name="hide" select="true()"/>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>
	<xsl:variable name="right_column_width">
		<xsl:choose>
			<xsl:when test="count(/root/vector/future/professiogram) * 24 + 12 &lt; 140">140</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="count(/root/vector/future/professiogram) * 24 + 12"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template mode="tree_right_th" match="group">
		<th class="col-professions-">
			<div style="width: {$right_column_width}px">
				<div class="title-">Для каких профессий<br/>нужна компетенция</div>
			</div>
		</th>
	</xsl:template>
	<xsl:template mode="tree_right_td" match="competence">
		<td class="col-professions-">
			<div style="width:{$right_column_width}px">
				<xsl:variable name="future_competence" select="/root/vector/future_competences_details/competence[@id = current()/@id]"/>
				<xsl:if test="$future_competence">
					<table class="professions-">
						<tr>
							<xsl:for-each select="$module/professiograms/professiogram">
								<xsl:choose>
									<xsl:when test="$future_competence/professiogram[@id = current()/@id]">
										<td>
											<xsl:variable name="prof_index" select="@index"/>
											<xsl:variable name="mark" select="$future_competence/professiogram[@id = current()/@id]/@mark"/>
											<xsl:attribute name="title">
												<xsl:text>Уровень «</xsl:text>
												<xsl:call-template name="draw_mark_title">
													<xsl:with-param name="mark" select="$future_competence/professiogram[@id = current()/@id]/@mark"/>
												</xsl:call-template>
												<xsl:text>» необходим для профессии «</xsl:text>
												<xsl:value-of select="@title"/>
												<xsl:text>»</xsl:text>
											</xsl:attribute>
											<div class="vector-prof-index vector-prof-index-{($prof_index - 1) mod 5 + 1}-">
												<xsl:value-of select="$prof_index"/>
											</div>
											<div class="mark-title-">
												<xsl:choose>
													<xsl:when test="$mark = 1">ск.</xsl:when>
													<xsl:when test="$mark = 2">сп.</xsl:when>
													<xsl:when test="$mark = 3">к.</xsl:when>
												</xsl:choose>
											</div>
										</td>
									</xsl:when>
									<xsl:otherwise>
										<td class="blank-"> </td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</tr>
					</table>
				</xsl:if>
			</div>
		</td>
	</xsl:template>
</xsl:stylesheet>
