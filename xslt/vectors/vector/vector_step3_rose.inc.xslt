<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_step3_rose">
		<xsl:variable name="module_context" select="."/>
		<div>
			<a name="focus"/>
		</div>
		<xsl:for-each select="/root/vector[1]">
			<table cellpadding="0" cellspacing="0" class="vector-resume-step">
				<tr class="head-">
					<td class="step-">
						<div>Шаг 3</div>
					</td>
					<td class="title-">
						<table class="vector-step-caption vector-step-caption-red-">
							<tr>
								<td>
									<div>Фокус</div>
								</td>
							</tr>
						</table>
					</td>
					<td class="question-">
						<span>— «Какие компетенции я хочу развивать в первую очередь?»</span>
					</td>
				</tr>
				<tr class="body-">
					<td> </td>
					<td colspan="2" class="content-">
						<span>
							<xsl:choose>
								<xsl:when test="@focus_competence_count &gt; 0">
									<strong>
										<xsl:call-template name="count_case">
											<xsl:with-param name="number" select="@focus_competence_count"/>
											<xsl:with-param name="word_ns" select="'Выбрана '"/>
											<xsl:with-param name="word_gs" select="'Выбраны '"/>
											<xsl:with-param name="word_ap" select="'Выбрано '"/>
										</xsl:call-template>
									</strong>
									<xsl:value-of select="@focus_competence_count"/>
									<xsl:call-template name="count_case">
										<xsl:with-param name="number" select="@focus_competence_count"/>
										<xsl:with-param name="word_ns" select="' компетенция.'"/>
										<xsl:with-param name="word_gs" select="' компетенции.'"/>
										<xsl:with-param name="word_ap" select="' компетенций.'"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<strong>Не заполнен.</strong>
								</xsl:otherwise>
							</xsl:choose>
						</span>
						<!--<span class="change-">
							<a class="hl" href="#">Изменить данные</a>
						</span>-->
					</td>
				</tr>
				<tr class="body2-">
					<td colspan="3" class="content-">
						<xsl:if test="focus/competence">
							<xsl:for-each select="$module_context">
								<xsl:call-template name="draw_rose">
									<xsl:with-param name="competences" select="focus_rose_competences"/>
									<xsl:with-param name="image_alt" select="'Фокусные компетенции и результаты'"/>
									<xsl:with-param name="marks_default_project_id" select="@project_id"/>
									<xsl:with-param name="show_self_by_default" select="/root/project_user_vector/project_comparison/@show_self_by_default = 1"/>
									<xsl:with-param name="rose_param_name" select="'img-focus'"/>
								</xsl:call-template>
							</xsl:for-each>
						</xsl:if>
					</td>
				</tr>
			</table>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
