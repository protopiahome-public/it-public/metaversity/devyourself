<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="vector_messages_no_competence_set">
		<xsl:param name="text" select="'Действие невозможно'"/>
		<div class="box box-warning">
			<p>
				<xsl:value-of select="$text"/>
				<xsl:text>, так как </xsl:text>
				<strong>не назначен набор компетенций для Вектора</strong>
				<xsl:text>.</xsl:text>
			</p>
			<p>Перейдите на страницу &#171;<a href="{$current_project/@url}admin/vector/">Вектор</a>&#187;, чтобы выставить эту настройку.</p>
		</div>
	</xsl:template>
</xsl:stylesheet>
