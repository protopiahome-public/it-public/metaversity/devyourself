<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_stat_choice">
		<xsl:param name="foreign" select="false()"/>
		<xsl:choose>
			<xsl:when test="$current_project/@events_are_on = 0 and $current_project/@materials_are_on = 0">
				<div class="box box-warning">
					<p style="padding: 8px 0;">Статистики нет, так как и события и материалы в данном проекте выключены.</p>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$foreign">
						<div class="box box-light">
							<p>
								<xsl:text>Здесь вы можете увидеть, какие компетенции развиваются больше всего у пользователя </xsl:text>
								<strong>
									<xsl:value-of select="$current_user/@login"/>
								</strong>
								<xsl:if test="$current_project/@events_are_on = 1"> на событиях, на которые он записывался</xsl:if>
								<xsl:if test="$current_project/@events_are_on = 1 and $current_project/@materials_are_on = 1">, и</xsl:if>
								<xsl:if test="$current_project/@materials_are_on = 1"> теми материалами, которые он изучил (либо собирается изучить)</xsl:if>
								<xsl:text>.</xsl:text>
							</p>
						</div>
						<xsl:if test="@no_focus = 1">
							<div class="box box-warning">
								<p style="padding: 8px;">Фокусные компетенции пользователем не указаны. Чтобы их указать, нужно пройти <a href="{$current_project/@url}vector/">Вектор</a>.</p>
							</div>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<div class="box box-light">
							<p>
								<xsl:text>Здесь вы можете увидеть, какие компетенции развиваются больше всего</xsl:text>
								<xsl:if test="$current_project/@events_are_on = 1"> на событиях, на которые вы записывались</xsl:if>
								<xsl:if test="$current_project/@events_are_on = 1 and $current_project/@materials_are_on = 1">, и</xsl:if>
								<xsl:if test="$current_project/@materials_are_on = 1"> теми материалами, которые вы изучили (либо собираетесь изучить)</xsl:if>
								<xsl:text>.</xsl:text>
							</p>
						</div>
						<xsl:if test="@no_focus = 1">
							<div class="box box-warning">
								<p style="padding: 8px;">Фокусные компетенции не указаны. Нужно пройти <a href="{$current_project/@url}vector/">Вектор</a> и указать компетенции, на развитии которых вы хотите фокусироваться.</p>
							</div>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:for-each select="stat">
			<h3>
				<xsl:if test="position() != 1">
					<xsl:attribute name="class">down</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="@title"/>
			</h3>
			<xsl:if test="not(competence/resource)">
				<p>
					<xsl:text>Пока ничего не найдено. </xsl:text>
					<xsl:value-of select="@empty_note"/>
				</p>
			</xsl:if>
			<xsl:if test="competence/resource">
				<table class="choise-stat" cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="2" class="legend-">
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<div class="block-">
											<xsl:attribute name="style">
												<xsl:value-of select="concat('height: ', @block_height, 'px; ')"/>
												<xsl:value-of select="concat('width: ', @block_width, 'px; ')"/>
											</xsl:attribute>
										</div>
									</td>
									<td>
										<xsl:text>&#160;&#8212;&#160;</xsl:text>
										<xsl:value-of select="@not_finished_title"/>
									</td>
									<td>
										<div class="legend-sep-"/>
									</td>
									<td>
										<div class="block- block-finished-">
											<xsl:attribute name="style">
												<xsl:value-of select="concat('height: ', @block_height, 'px; ')"/>
												<xsl:value-of select="concat('width: ', @block_width, 'px; ')"/>
											</xsl:attribute>
										</div>
									</td>
									<td>
										<xsl:text>&#160;&#8212;&#160;</xsl:text>
										<xsl:value-of select="@finished_title"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="graph-">
							<table class="choices-" cellpadding="0" cellspacing="0">
								<tr>
									<xsl:for-each select="competence">
										<td>
											<xsl:for-each select="resource">
												<a>
													<xsl:attribute name="class">
														<xsl:text>block-</xsl:text>
														<xsl:if test="@finished = 1"> block-finished-</xsl:if>
													</xsl:attribute>
													<xsl:attribute name="style">
														<xsl:value-of select="concat('height: ', ../../@block_height, 'px; ')"/>
													</xsl:attribute>
													<xsl:for-each select="/root/*[name() = concat(current()/../../@name, '_short') and @id = current()/@id]">
														<xsl:attribute name="title">
															<xsl:value-of select="@title"/>
														</xsl:attribute>
														<xsl:attribute name="href">
															<xsl:value-of select="@url"/>
														</xsl:attribute>
													</xsl:for-each>
												</a>
											</xsl:for-each>
										</td>
									</xsl:for-each>
								</tr>
								<tr>
									<xsl:for-each select="competence">
										<th>
											<xsl:attribute name="style">
												<xsl:value-of select="concat('width: ', ../@block_width, 'px; ')"/>
											</xsl:attribute>
											<xsl:attribute name="title">
												<xsl:call-template name="draw_competence_title"/>
											</xsl:attribute>
											<xsl:value-of select="@id"/>
										</th>
									</xsl:for-each>
								</tr>
							</table>
						</td>
						<td class="competences-">
							<xsl:if test="../@page_type = 'focus'">
								<p>
									<strong>
										<xsl:text>Отображается статистика только </xsl:text>
										<a>
											<xsl:attribute name="href">
												<xsl:choose>
													<xsl:when test="$foreign">
														<xsl:value-of select="concat($current_user_base_url, 'vector/#focus')"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="concat($current_project/@url, 'vector/my/#focus')"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:text>фокусным компетенциям</xsl:text>
										</a>
										<xsl:text>:</xsl:text>
									</strong>
								</p>
							</xsl:if>
							<table class="tbl tbl-no-hor-lines-">
								<xsl:for-each select="competence">
									<tr>
										<td>
											<xsl:call-template name="draw_competence_title">
												<xsl:with-param name="red_number" select="false()"/>
											</xsl:call-template>
										</td>
									</tr>
								</xsl:for-each>
							</table>
						</td>
					</tr>
				</table>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
