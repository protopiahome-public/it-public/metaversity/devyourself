<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="head2/project_vector_head2.inc.xslt"/>
	<xsl:include href="menu/vector_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'vector'"/>
	<xsl:variable name="project_section_main_page" select="true()"/>
	<xsl:variable name="vector_section" select="'intro'"/>
	<xsl:variable name="vector_section_main_page" select="true()"/>
	<xsl:variable name="module_url">
		<xsl:value-of select="$current_project/@url"/>
		<xsl:text>vector/my/</xsl:text>
	</xsl:variable>
	<xsl:variable name="vector_config" select="/root/vector_config"/>
	<xsl:template match="project_vector_intro">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_project_vector_head2"/>
							<xsl:call-template name="_draw_intro"/>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_intro">
		<div class="content vector-intro">
			<div class="what-is-">Вектор — это рекомендательная система. Определив ваши профессиональные цели, он подскажет, как и где можно развиваться.</div>
			<div class="is-inner-">
				<xsl:text>Для участия в проекте «</xsl:text>
				<xsl:value-of select="$current_project/@title"/>
				<xsl:text>» вам нужно пройти вектор. Он нужен для того, чтобы сфокусировать вас на развитии определённых компетенций в проекте (тех, которые вы сами выберете и чётко обозначите)</xsl:text>
				<xsl:if test="$current_project/@role_recommendation_is_on = 1 or $current_project/@events_are_on = 1 or $current_project/@materials_are_on = 1">
					<xsl:text>, а также для </xsl:text>
					<em>
						<xsl:text>рекомендации </xsl:text>
						<xsl:if test="$current_project/@role_recommendation_is_on = 1">
							<xsl:text>ролей</xsl:text>
						</xsl:if>
						<xsl:if test="$current_project/@events_are_on = 1">
							<xsl:if test="$current_project/@role_recommendation_is_on = 1">, </xsl:if>
							<xsl:text>событий</xsl:text>
						</xsl:if>
						<xsl:if test="$current_project/@materials_are_on = 1">
							<xsl:if test="$current_project/@role_recommendation_is_on = 1 or $current_project/@events_are_on = 1">, </xsl:if>
							<xsl:text>материалов</xsl:text>
						</xsl:if>
					</em>
				</xsl:if>
				<xsl:text>.</xsl:text>
			</div>
			<!--<div class="later-">
				<xsl:text>Позже вы можете пройти </xsl:text>
				<strong>глобальный Вектор</strong>
				<xsl:text> (который вы найдёте в верхнем меню), чтобы мы могли порекомендовать вам другие события и материалы.</xsl:text>
			</div>-->
			<table class="steps-" cellpadding="0" cellspacing="0">
				<tr>
					<th colspan="3">
						<xsl:choose>
							<xsl:when test="$vector_config/@vector_self_must_skip = 1">Три</xsl:when>
							<xsl:otherwise>Четыре</xsl:otherwise>
						</xsl:choose>
						<xsl:text> шага вектора:</xsl:text>
					</th>
				</tr>
				<tr>
					<td class="l-">
						<span class="red-">1</span>
					</td>
					<td class="c-">
						<table class="vector-step-caption vector-step-caption-red-">
							<tr>
								<td>
									<div>«Я в будущем»</div>
								</td>
							</tr>
						</table>
					</td>
					<td class="r-">
						<div class="question-">— Кем я хочу быть по профессии?</div>
					</td>
				</tr>
				<tr>
					<td class="l-">
						<span class="red-">2</span>
					</td>
					<td class="c-">
						<table class="vector-step-caption vector-step-caption-red-">
							<tr>
								<td>
									<div>Самооценка</div>
								</td>
							</tr>
						</table>
					</td>
					<td class="r-">
						<div class="question-">— Каков мой сегодняшний уровень развития?</div>
						<div class="note-">(шаг можно опустить, если есть результаты <br/>
											с предыдущих проектов)</div>
					</td>
				</tr>
				<xsl:if test="$vector_config/@vector_self_must_skip = 0">
					<tr>
						<td class="l-">
							<span class="red-">3</span>
						</td>
						<td class="c-">
							<table class="vector-step-caption vector-step-caption-red-">
								<tr>
									<td>
										<div>Фокус</div>
									</td>
								</tr>
							</table>
						</td>
						<td class="r-">
							<div class="question-">— Какие компетенции я хочу развивать в первую очередь?</div>
						</td>
					</tr>
				</xsl:if>
				<tr>
					<td class="l-">
						<span class="green-">
							<xsl:choose>
								<xsl:when test="$vector_config/@vector_self_must_skip = 1">3</xsl:when>
								<xsl:otherwise>4</xsl:otherwise>
							</xsl:choose>
						</span>
					</td>
					<td class="c-">
						<table class="vector-step-caption vector-step-caption-green-">
							<tr>
								<td>
									<div>Рекомендации</div>
								</td>
							</tr>
						</table>
					</td>
					<td class="r-">
						<div class="question-">— Где это можно развивать?</div>
						<xsl:if test="$current_project/@role_recommendation_is_on = 1 or $current_project/@events_are_on = 1 or $current_project/@materials_are_on = 1">
							<div class="note-">
								<xsl:text>(</xsl:text>
								<xsl:text>рекомендация </xsl:text>
								<xsl:if test="$current_project/@role_recommendation_is_on = 1">
									<xsl:text>ролей</xsl:text>
								</xsl:if>
								<xsl:if test="$current_project/@events_are_on = 1">
									<xsl:if test="$current_project/@role_recommendation_is_on = 1">, </xsl:if>
									<xsl:text>событий</xsl:text>
								</xsl:if>
								<xsl:if test="$current_project/@materials_are_on = 1">
									<xsl:if test="$current_project/@role_recommendation_is_on = 1 or $current_project/@events_are_on = 1">, </xsl:if>
									<xsl:text>материалов</xsl:text>
								</xsl:if>
								<xsl:text>)</xsl:text>
							</div>
						</xsl:if>
					</td>
				</tr>
			</table>
			<div class="button-">
				<a class="button button-large button-block" href="{$vector_base_url}future/">
					<xsl:text>Пройти вектор </xsl:text>
					<span class="arrow-">→</span>
				</a>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Что такое вектор</xsl:text>
	</xsl:template>
</xsl:stylesheet>
