<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="../project/head2/project_user_head2.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'users'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'vector'"/>
	<xsl:variable name="user_section_main_page" select="not($get_vars)"/>
	<xsl:template match="error_404">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_project_user_head2"/>
							<h2>Вектор</h2>
							<div class="clear"/>
							<div class="content">
								<p>Вектор в данном проекте отключен.</p>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script src="{$prefix}/js/project_comparison.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Вектор отключен</xsl:text>
	</xsl:template>
</xsl:stylesheet>
