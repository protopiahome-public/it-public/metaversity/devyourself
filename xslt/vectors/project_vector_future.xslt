<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="head2/project_vector_head2.inc.xslt"/>
	<xsl:include href="menu/project_vector_menu.inc.xslt"/>
	<xsl:include href="vector/vector_form_future.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'vector'"/>
	<xsl:variable name="project_section_main_page" select="true()"/>
	<xsl:variable name="vector_section" select="'pass'"/>
	<xsl:variable name="vector_section_main_page" select="true()"/>
	<xsl:variable name="module_url">
		<xsl:value-of select="$current_project/@url"/>
		<xsl:text>vector/future/</xsl:text>
	</xsl:variable>
	<xsl:variable name="vector_config" select="/root/vector_config"/>
	<xsl:template match="project_vector_future">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns columns-top">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_project_vector_head2"/>
							<div class="content">
								<xsl:call-template name="draw_project_vector_menu"/>
								<xsl:call-template name="draw_vector_form_future_top"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
				<table cellspacing="0" class="columns columns-bottom">
					<tr>
						<td class="center-column">
							<div class="content">
								<xsl:call-template name="draw_vector_form_future"/>
							</div>
						</td>
						<td class="right-column">
							<xsl:call-template name="draw_filter">
								<xsl:with-param name="competence_set_id" select="$vector_config/@vector_competence_set_id"/>
							</xsl:call-template>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script src="{$prefix}/js/vector.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Я в будущем &#8212; Вектор &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
