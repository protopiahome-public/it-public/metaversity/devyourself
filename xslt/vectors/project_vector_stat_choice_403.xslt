<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="head2/project_vector_head2.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'vector'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="vector_section" select="'403'"/>
	<xsl:variable name="vector_section_main_page" select="false()"/>
	<xsl:template match="error_403">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_project_vector_head2"/>
							<h2>Статистика моего развития</h2>
							<div class="clear"/>
							<div class="content">
								<p>Для доступа к статистике необходимо <a href="{$login_url}">войти</a> или <a href="{$reg_url}">зарегистрироваться</a>.</p>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Статистика недоступна</xsl:text>
	</xsl:template>
</xsl:stylesheet>
