<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_project_vector_menu">
		<xsl:for-each select="/root/project_vector_menu[1]">
			<table id="vector-menu" cellpadding="0" cellspacing="0">
				<tr class="t-">
					<xsl:for-each select="step">
						<th>
							<div>
								<xsl:if test="@available = 0">
									<xsl:attribute name="class">disabled-</xsl:attribute>
								</xsl:if>
								<xsl:if test="@is_current = 1">
									<xsl:attribute name="class">current-</xsl:attribute>
								</xsl:if>
								<xsl:variable name="step_url">
									<xsl:value-of select="concat($current_project/@url, 'vector/')"/>
									<xsl:choose>
										<xsl:when test="@name = 'future'">future</xsl:when>
										<xsl:when test="@name = 'self'">self</xsl:when>
										<xsl:when test="@name = 'focus'">focus</xsl:when>
										<xsl:when test="@name = 'recommend'">recommend</xsl:when>
									</xsl:choose>
									<xsl:text>/</xsl:text>
								</xsl:variable>
								<xsl:choose>
									<xsl:when test="@available = 1 and not(@is_current = 1)">
										<a href="{$step_url}">
											<xsl:value-of select="concat('Шаг ', @number)"/>
										</a>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="concat('Шаг ', @number)"/>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</th>
						<xsl:if test="position() != last()">
							<th class="sep-">
								<div>
									<span>→</span>
								</div>
							</th>
						</xsl:if>
					</xsl:for-each>
				</tr>
				<tr class="b-">
					<xsl:for-each select="step">
						<td>
							<table>
								<xsl:attribute name="class">
									<xsl:text>vector-step-caption vector-step-caption-fixed </xsl:text>
									<xsl:choose>
										<xsl:when test="@available = 0">vector-step-caption-gray- </xsl:when>
										<xsl:when test="@name = 'recommend'">vector-step-caption-green- </xsl:when>
										<xsl:otherwise>vector-step-caption-red- </xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
								<tr>
									<td>
										<div>
											<xsl:choose>
												<xsl:when test="@name = 'future'">«Я в будущем»</xsl:when>
												<xsl:when test="@name = 'self'">Самооценка</xsl:when>
												<xsl:when test="@name = 'focus'">Фокус</xsl:when>
												<xsl:when test="@name = 'recommend'">Рекомендации</xsl:when>
											</xsl:choose>
										</div>
									</td>
								</tr>
							</table>
						</td>
						<xsl:if test="position() != last()">
							<td class="sep-">
							</td>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</table>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
