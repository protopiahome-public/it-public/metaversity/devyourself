<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_vector_submenu">
		<xsl:param name="base_url"/>
		<div class="submenu submenu-left-">
			<span class="inner-">
				<xsl:for-each select="/root/project_user_vector[1]">
					<span class="item-">
						<xsl:if test="@output_type = 'rose'">
							<xsl:attribute name="class">selected-</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="@output_type = 'rose' and not($get_vars)">Резюме</xsl:when>
							<xsl:otherwise>
								<a href="{$base_url}{$request/@query_string}">Резюме</a>
							</xsl:otherwise>
						</xsl:choose>
					</span>
					<span class="sep-"> | </span>
					<span class="item-">
						<xsl:if test="@output_type = 'detailed'">
							<xsl:attribute name="class">selected-</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="@output_type = 'detailed' and not($get_vars)">Подробно</xsl:when>
							<xsl:otherwise>
								<a href="{$base_url}detailed/{$request/@query_string}">Подробно</a>
							</xsl:otherwise>
						</xsl:choose>
					</span>
				</xsl:for-each>
			</span>
		</div>
	</xsl:template>
</xsl:stylesheet>
