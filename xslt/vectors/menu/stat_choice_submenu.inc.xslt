<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_stat_choice_submenu">
		<div class="submenu submenu-left-">
			<span class="inner-">
				<span class="item-">
					<xsl:if test="@page_type = 'all'">
						<xsl:attribute name="class">selected-</xsl:attribute>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="@page_type = 'all' and not($get_vars)">Показать все компетенции</xsl:when>
						<xsl:otherwise>
							<a href="{$module_url}all/{$request/@query_string}">Показать все компетенции</a>
						</xsl:otherwise>
					</xsl:choose>
				</span>
				<span class="sep-"> | </span>
				<span class="item-">
					<xsl:if test="@page_type = 'focus'">
						<xsl:attribute name="class">selected-</xsl:attribute>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="@page_type = 'focus' and not($get_vars)">Только фокусные</xsl:when>
						<xsl:otherwise>
							<a href="{$module_url}focus/{$request/@query_string}">Только фокусные</a>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</span>
		</div>
	</xsl:template>
</xsl:stylesheet>
