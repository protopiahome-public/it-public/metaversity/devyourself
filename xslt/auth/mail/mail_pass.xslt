<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="xml" encoding="UTF-8"/>
	<xsl:variable name="request" select="/root/request"/>
	<xsl:variable name="prefix" select="$request/@prefix"/>
	<xsl:variable name="user" select="/root/user_pass_restore_mail"/>
	<xsl:template match="/root">
		<html>
			<head>
				<title>Восстановление пароля (<xsl:value-of select="$user/@login"/>)</title>
			</head>
			<body>
				<style type="text/css">
					body {padding: 0; margin: 0;}
					body, td {font-family: Arial, Tahoma, Verdana, serif; font-size: 10pt; line-height: 13pt; background: #fff; color: #000; text-align: left;}
					p {margin: 0 0 5pt; padding: 0;}
					img {border: 0; vertical-align: middle;}
					ul, ol, li {margin: 0; padding: 0;}
					ul, ol {margin-bottom: 5pt;}
					li {margin: 0 0 5pt 20pt;}
					ol li {list-style-type: decimal;}
					ul li {list-style-type: square;}
					a {color: #463d37; text-decoration: underline;}
					.b {padding: 0 5pt;}
					.h, .hh, .bot {background: #ddd; padding: 5pt;}
					.h {font-size: 12pt; line-height: 15pt; margin-bottom: 12pt;}
					.hh {font-size: 12pt; line-height: 15pt; margin-bottom: 12pt; margin-top: 15pt;}
					.bot {color: #666; margin-bottom: 12pt; margin-top: 15pt;}
				</style>
				<div class="b">
					<p>Здравствуйте!</p>
					<p>Вы получили это письмо в ответ на запрос о восстановлении пароля на 
						<a href="{$prefix}/">
							<xsl:value-of select="concat($prefix, '/')"/>
						</a>
					</p>
					<p>Пожалуйста, перейдите по ссылке, чтобы установить новый пароль:</p>
					<xsl:variable name="link">
						<xsl:value-of select="concat($prefix, '/pass/?login=', $user/@login, '&amp;key=', $user/@pass_recovery_key, $user/@retpath_suffix)"/>
					</xsl:variable>
					<p>
						<a href="{$link}">
							<xsl:value-of select="$link"/>
						</a>
					</p>
					<p>Если вы получили это письмо по ошибке, просто распечатайте и сожгите его.</p>
					<p>-- <br/>С уважением, <br/>администрация сервиса.</p>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
