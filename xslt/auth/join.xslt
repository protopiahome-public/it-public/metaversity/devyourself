<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/login_head2.inc.xslt"/>
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="include/login.inc.xslt"/>
	<xsl:variable name="top_section" select="'login'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/join/')"/>
	<xsl:template match="join">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_login_head2"/>
							<xsl:call-template name="_draw_form"/>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<div class="content">
			<div class="box1">
				<p>Чтобы завершить вход, Вы должны стать участником проекта &#171;<xsl:value-of select="$current_project/@title"/>&#187;.</p>
			</div>
			<form action="{$save_prefix}/project_join/" method="post">
				<xsl:if test="$get_vars[@name='retpath']">
					<input type="hidden" name="retpath" value="{$get_vars[@name='retpath']}"/>
				</xsl:if>
				<input type="hidden" name="project_id" value="{$current_project/@id}"/>
				<input type="hidden" name="join" value="1"/>
				<xsl:if test="$current_project/@use_game_name = 1">
					<xsl:call-template name="draw_game_name_field"/>
				</xsl:if>
				<button class="button button-green">Присоединиться к проекту</button>
				<button name="cancel" class="button button-gray button-right">Отменить</button>
			</form>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Вход'"/>
	</xsl:template>
</xsl:stylesheet>
