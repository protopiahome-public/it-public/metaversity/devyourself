<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/settings_head2.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'settings'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/settings/')"/>
	<xsl:template match="settings">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_settings_head2"/>
							<xsl:call-template name="_draw_form"/>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<div class="content">
			<form action="{$save_prefix}/settings/" method="post" enctype="multipart/form-data">
				<xsl:call-template name="draw_dt_edit"/>
			</form>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Редактирование профиля</xsl:text>
	</xsl:template>
</xsl:stylesheet>
