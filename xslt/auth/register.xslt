<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="include/auth_common.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="../project/include/game_name.inc.xslt"/>
	<xsl:variable name="top_section" select="'reg'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:template match="register">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content auth-page auth-page-register">
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<xsl:call-template name="draw_auth_page_project_title"/>
		<h3>
			<xsl:choose>
				<xsl:when test="@project_id">Регистрация в проекте</xsl:when>
				<xsl:otherwise>Регистрация</xsl:otherwise>
			</xsl:choose>
		</h3>
		<xsl:call-template name="draw_auth_page_united_reg_note"/>
		<div class="registered-question- note">
			<a href="{$login_url}">Уже зарегистрированы?</a>
		</div>
		<table class="form-" cellspacing="0">
			<tr>
				<td>
					<form action="{$save_prefix}/register/" method="post" enctype="multipart/form-data">
						<xsl:if test="$get_vars[@name = 'retpath']">
							<input type="hidden" name="retpath" value="{$get_vars[@name = 'retpath']}"/>
						</xsl:if>
						<xsl:if test="@project_id">
							<input type="hidden" name="project_id" value="{@project_id}"/>
						</xsl:if>
						<xsl:call-template name="draw_dt_edit">
							<xsl:with-param name="no_fieldset" select="true()"/>
							<xsl:with-param name="draw_button" select="false()"/>
						</xsl:call-template>
						<xsl:if test="@project_id and /root/project_short[@id = current()/@project_id]/@use_game_name = 1">
							<h5>
								<xsl:text>Данные для проекта &#171;</xsl:text>
								<xsl:value-of select="/root/project_short[@id = current()/@project_id]/@title"/>
								<xsl:text>&#187;:</xsl:text>
							</h5>
							<xsl:call-template name="draw_game_name_field">
								<xsl:with-param name="no_fieldset" select="true()"/>
							</xsl:call-template>
						</xsl:if>
						<button class="button">Зарегистрироваться</button>
						<xsl:if test="$get_vars[@name = 'can_cancel']">
							<button name="cancel" class="button button-gray button-right">Отменить</button>
						</xsl:if>
					</form>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Регистрация</xsl:text>
	</xsl:template>
</xsl:stylesheet>
