<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="module_url" select="concat($prefix, '/pass/')"/>
	<xsl:variable name="top_section" select="'pass'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:template match="pass">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="head2">
								<div class="title-">
									<table class="border-">
										<tr>
											<td class="l-"> </td>
											<td class="c-">
												<h1>Восстановление пароля</h1>
											</td>
											<td class="r-"> </td>
										</tr>
									</table>
								</div>
							</div>
							<xsl:call-template name="_draw_form"/>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<xsl:if test="$get_vars[@name='retpath']">
			<input type="hidden" name="retpath2" value="{$get_vars[@name='retpath']}"/>
		</xsl:if>
		<div class="content">
			<form action="{$save_prefix}/pass/" method="post">
				<xsl:choose>
					<xsl:when test="@step = 1 and $pass_info/info[@name = 'OK']">
						<xsl:call-template name="_draw_step1_ok"/>
					</xsl:when>
					<xsl:when test="@step = 1">
						<xsl:call-template name="_draw_step1"/>
					</xsl:when>
					<xsl:when test="@step = 2 and (@error or $pass_info/error[@name = 'KEY_EXPIRED'] or $pass_info/error[@name = 'BAD_KEY'])">
						<xsl:call-template name="_draw_step2_error"/>
					</xsl:when>
					<xsl:when test="@step = 2">
						<xsl:call-template name="_draw_step2"/>
					</xsl:when>
				</xsl:choose>
			</form>
		</div>
	</xsl:template>
	<xsl:template name="_draw_step1">
		<div class="box">
			<p>
				<strong>Шаг 1.</strong> Укажите email <strong>или</strong> логин. Мы отправим вам ссылку для восстановления пароля.</p>
		</div>
		<xsl:choose>
			<xsl:when test="$pass_info/error[@name = 'BLANK']">
				<div class="error">
					<span>Укажите email или логин</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'BAD_LOGIN']">
				<div class="error">
					<span>Такого логина не существует</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'BAD_EMAIL']">
				<div class="error">
					<span>Пользователей с таким email-адресом у нас нет</span>
				</div>
			</xsl:when>
		</xsl:choose>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<label class="title-" for="f-{@name}">Email или логин:</label>
			<div class="input-">
				<input class="input-text narrow-" type="text" name="login" maxlength="40" value="{$pass_info/vars/var[@name = 'login']}"/>
			</div>
		</div>
		<button class="button button-small">Восстановить пароль</button>
		<div style="margin-top: 20px;" class="box">
			<p>
				<strong>Шаг 2.</strong> Перейдите по ссылке и установите новый пароль.</p>
		</div>
	</xsl:template>
	<xsl:template name="_draw_step1_ok">
		<div class="box-light">
			<xsl:text>Вам отправлено письмо на email </xsl:text>
			<strong>
				<xsl:value-of select="$pass_info/info[@name = 'OK']"/>
			</strong>
			<xsl:text> (полный адрес скрыт из соображений безопасности).</xsl:text>
			<br/>
			<br/>
			<xsl:text>Чтобы установить новый пароль, перейдите по ссылке в письме.</xsl:text>
		</div>
	</xsl:template>
	<xsl:template name="_draw_step2_error">
		<xsl:choose>
			<xsl:when test="@error = 'KEY_EXPIRED' or $pass_info/error[@name = 'KEY_EXPIRED']">
				<div class="error">
					<span>Ключ для восстановления пароля устарел. <a href="{$prefix}/pass/">Попробуйте еще раз</a>.
					</span>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<div class="error">
					<span>Ключ для восстановления пароля устарел. <a href="{$prefix}/pass/">Попробуйте еще раз</a>.
					</span>
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="_draw_step2">
		<div class="box">
			<p>Укажите новый пароль.</p>
		</div>
		<input type="hidden" name="login" value="{@login}"/>
		<input type="hidden" name="key" value="{@key}"/>
		<xsl:choose>
			<xsl:when test="$pass_info/error[@field = 'password' and @name = 'UNFILLED']">
				<div class="error">
					<span>Пароль не должен быть пустым</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@field = 'password' and @name = 'PASSWORDS_NOT_EQUAL']">
				<div class="error">
					<span>Введённые пароли не совпадают</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@field = 'password' and @name = 'TOO_SHORT']">
				<div class="error">
					<span>
						<xsl:text>Пароль слишком короткий. Минимально допустимая длина 5 символов, а у Вас </xsl:text>
						<xsl:value-of select="$pass_info/error[@field = 'password' and @name = 'TOO_SHORT']"/>
					</span>
				</div>
			</xsl:when>
		</xsl:choose>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<label class="title-" for="f-{@name}">
				<xsl:text>Пароль:</xsl:text>
				<span class="star">
					<xsl:text>&#160;*</xsl:text>
				</span>
			</label>
			<div class="input-">
				<input class="input-text narrow-" type="password" name="password" maxlength="255" value="{$pass_info/vars/var[@name = 'password']}"/>
			</div>
		</div>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<label class="title-" for="f-{@name}">
				<xsl:text>Пароль, еще раз:</xsl:text>
				<span class="star">
					<xsl:text>&#160;*</xsl:text>
				</span>
			</label>
			<div class="input-">
				<input class="input-text narrow-" type="password" name="password_check" maxlength="255" value="{$pass_info/vars/var[@name = 'password_check']}"/>
			</div>
		</div>
		<button class="button button-small">Установить пароль</button>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Восстановление пароля'"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1- selected-">Восстановление пароля</div>
	</xsl:template>
</xsl:stylesheet>
