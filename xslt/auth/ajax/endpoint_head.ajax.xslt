<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_common.xslt"/>
	<xsl:include href="../../_site/base_head.xslt"/>
	<xsl:include href="../../project/head2/project_head2.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="''"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="''"/>
	<xsl:template match="/root">
		<xsl:call-template name="draw_head"/>
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
		</div>
	</xsl:template>
	<xsl:template match="text()"/>
</xsl:stylesheet>
