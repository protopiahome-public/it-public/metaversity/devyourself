<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="include/auth_common.inc.xslt"/>
	<xsl:include href="include/login.inc.xslt"/>
	<xsl:variable name="top_section" select="'login'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($prefix, '/login/')"/>
	<xsl:template match="login">
		<div class="color-yellow">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content auth-page auth-page-login">
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<xsl:call-template name="draw_auth_page_project_title"/>
		<table class="form-" cellspacing="0">
			<tr>
				<td>
					<h3>
						<xsl:choose>
							<xsl:when test="@project_id">Вход в проект</xsl:when>
							<xsl:otherwise>Вход</xsl:otherwise>
						</xsl:choose>
					</h3>
					<form action="{$save_prefix}/login/" method="post">
						<xsl:if test="$get_vars[@name='retpath']">
							<input type="hidden" name="retpath" value="{$get_vars[@name='retpath']}"/>
						</xsl:if>
						<xsl:call-template name="draw_login">
							<xsl:with-param name="can_cancel" select="$get_vars[@name='can_cancel']"/>
						</xsl:call-template>
					</form>
				</td>
			</tr>
		</table>
		<xsl:if test="@project_id">
			<xsl:call-template name="draw_auth_page_united_reg_note"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Вход'"/>
	</xsl:template>
</xsl:stylesheet>
