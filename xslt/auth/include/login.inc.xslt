<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_login">
		<xsl:param name="can_cancel" select="true()"/>
		<xsl:if test="$pass_info/error[@name = 'BAD_LOGIN_OR_PASSWORD']">
			<div class="error">
				<span>Неверный логин или пароль. Попробуйте ещё раз.</span>
			</div>
		</xsl:if>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<label class="title-" for="f-{@name}">Логин:</label>
			<div class="input-">
				<input class="input-text narrow-" type="text" name="login" maxlength="40" value="{$pass_info/vars/var[@name = 'login']}"/>
			</div>
		</div>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<label class="title-" for="f-{@name}">Пароль:</label>
			<div class="input-">
				<input class="input-text narrow-" type="password" name="password" maxlength="255" value="{$pass_info/vars/var[@name = 'password']}"/>
			</div>
		</div>
		<button class="button">Войти</button>
		<xsl:if test="$can_cancel">
			<button name="cancel" class="button button-gray button-right">Отменить</button>
		</xsl:if>
		<xsl:variable name="get_suffix">
			<xsl:if test="$get_vars[@name='retpath']">
				<xsl:value-of select="concat('?retpath=',$get_vars[@name='retpath']/@value_escaped)"/>
			</xsl:if>
		</xsl:variable>
		<p style="margin-top:20px; margin-bottom: 20px;">
			<a href="{$reg_url}">Зарегистрироваться</a> | <a href="{$prefix}/pass/{$get_suffix}">Я забыл пароль</a>
		</p>
	</xsl:template>
</xsl:stylesheet>
