<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_auth_page_project_title">
		<xsl:if test="@project_id">
			<xsl:for-each select="/root/project_short[@id = current()/@project_id]">
				<table class="project-title-" cellspacing="0">
					<tr>
						<td class="logo-">
							<a href="{@url}">
								<img src="{logo_small/@url}" width="{logo_small/@url}" height="{logo_small/@url}" alt=""/>
							</a>
						</td>
						<td class="title-">
							<a href="{@url}">
								<xsl:value-of select="@title"/>
							</a>
						</td>
					</tr>
				</table>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="draw_auth_page_united_reg_note">
		<div class="note">
			<xsl:text>Мы используем единую регистрацию для </xsl:text>
			<a href="{$prefix}/projects/">всех проектов</a>
			<xsl:text> сайта </xsl:text>
			<xsl:value-of select="$sys_params/@title"/>
			<xsl:text>.</xsl:text>
		</div>
	</xsl:template>
</xsl:stylesheet>
