<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/projects_head2.inc.xslt"/>
	<xsl:include href="search/projects_search.inc.xslt"/>
	<xsl:include href="../project/include/project_competence_sets.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="/root/projects/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($prefix, '/projects/')"/>
	<xsl:template match="projects">
		<div class="color-green">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_projects_head2"/>
							<div class="content">
								<xsl:call-template name="draw_projects_search"/>
								<xsl:call-template name="_draw_table"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_table">
		<xsl:if test="filter/field[@name = 'search'] != ''">
			<p>
				<xsl:text>Результаты поиска по запросу &#171;</xsl:text>
				<strong>
					<xsl:value-of select="filter/field[@name = 'search']"/>
				</strong>
				<xsl:text>&#187;.</xsl:text>
			</p>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="project">
				<p>
					<xsl:text>Отображаются проекты </xsl:text>
					<xsl:value-of select="pages/@from_row"/>
					<xsl:text> &#8212; </xsl:text>
					<xsl:value-of select="pages/@to_row"/>
					<xsl:text> из </xsl:text>
					<xsl:value-of select="pages/@row_count"/>
					<xsl:if test="filter/field[@name = 'hide_no_marks'] = 1"> (проекты без оценок скрыты)</xsl:if>
					<xsl:text>.</xsl:text>
				</p>
			</xsl:when>
			<xsl:otherwise>
				<p>По заданным критериям поиска ни одного проекта не найдено.</p>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="$user/@id">
			<div class="ico-links">
				<a class="add-" href="{$prefix}/projects/add/">
					<span>создать проект</span>
				</a>
			</div>
		</xsl:if>
		<xsl:if test="project">
			<table class="tbl">
				<tr>
					<th/>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'date'"/>
						<xsl:with-param name="title" select="'Дата&#160;начала'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'users'"/>
						<xsl:with-param name="title" select="'Участников'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'precedents'"/>
						<xsl:with-param name="title" select="'Прецедентов'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'marks'"/>
						<xsl:with-param name="title" select="'Оценок'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<!--<th>
						<span>Наборы компетенций</span>
					</th>-->
				</tr>
				<xsl:for-each select="project">
					<xsl:variable name="project_id" select="@id"/>
					<xsl:variable name="context" select="."/>
					<xsl:variable name="position" select="position()"/>
					<xsl:for-each select="/root/project_short[@id = current()/@id]">
						<tr>
							<td class="col-logo-">
								<a href="{@url}">
									<img src="{logo_small/@url}" width="{logo_small/@width}" height="{logo_small/@height}" alt="{@title}" title=""/>
								</a>
							</td>
							<td class="col-project-title-">
								<div class="title-">
									<a href="{@url}">
										<xsl:value-of select="@title"/>
									</a>
								</div>
								<div class="dates-">
									<xsl:call-template name="get_full_date">
										<xsl:with-param name="datetime" select="@start_time"/>
									</xsl:call-template>
									<xsl:if test="substring(@start_time, 1, 10) != substring(@finish_time, 1, 10)">
										<xsl:text> &#8212; </xsl:text>
										<xsl:choose>
											<xsl:when test="@finish_time != ''">
												<xsl:call-template name="get_full_date">
													<xsl:with-param name="datetime" select="@finish_time"/>
												</xsl:call-template>
											</xsl:when>
											<xsl:otherwise>...</xsl:otherwise>
										</xsl:choose>
									</xsl:if>
								</div>
								<div class="state-">
									<xsl:call-template name="draw_project_state">
										<xsl:with-param name="state" select="@state_calc"/>
									</xsl:call-template>
								</div>
							</td>
							<td class="col-digits-">
								<xsl:value-of select="$context/@user_count_calc"/>
							</td>
							<td class="col-digits-">
								<xsl:value-of select="@precedent_count_calc"/>
							</td>
							<td class="col-digits-">
								<span class="with-hint">
									<xsl:call-template name="draw_mark_count_hint"/>
								</span>
							</td>
							<!--<td class="col-list-">
								<ul>
									<xsl:call-template name="draw_project_competence_sets">
										<xsl:with-param name="project_id" select="$project_id"/>
									</xsl:call-template>
								</ul>
							</td>-->
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
			<xsl:apply-templates select="pages[page]"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Проекты'"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1- selected-">
			<xsl:choose>
				<xsl:when test="$top_section_main_page">Проекты</xsl:when>
				<xsl:otherwise>
					<a href="{$prefix}/projects/">Проекты</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
