<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_projects_search">
		<div class="box" style="width: 300px; float: right; margin-bottom: 10px;">
			<div class="content-">
				<xsl:variable name="module_filters" select="filter"/>
				<form id="search-form" class="search-form" action="{$module_url}" method="get">
					<xsl:for-each select="$get_vars">
						<xsl:if test="not(substring(@name, 1, 6) = 'filter')">
							<xsl:call-template name="get_param_for_form"/>
						</xsl:if>
					</xsl:for-each>
					<div class="line-">
						<div class="el- el-text-">
							<input type="text" id="search_search" jq-placeholder="проект" name="filter[search]" value="" maxlength="255">
								<xsl:if test="filters/filter[@name = 'search']/@value != ''">
									<xsl:attribute name="value">
										<xsl:value-of select="filters/filter[@name = 'search']/@value"/>
									</xsl:attribute>
								</xsl:if>
							</input>
						</div>
						<div class="el- el-btn-">
							<button class="button button-small">Искать</button>
						</div>
					</div>
					<div class="line-">
						<div class="el- el-cb-">
							<input id="search_hide_no_marks" type="checkbox" name="filter[hide_no_marks]" value="1" onclick="$(this).parents('form').submit(); $(this).parents('form').submit();">
								<xsl:if test="filters/filter[@name = 'hide_no_marks']/@checked = 1">
									<xsl:attribute name="checked">checked</xsl:attribute>
								</xsl:if>
							</input>
							<label for="search_hide_no_marks">Скрывать проекты без оценок</label>
						</div>
					</div>
				</form>
				<div class="clear"/>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
