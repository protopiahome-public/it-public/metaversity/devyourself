<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="head2/projects_head2.inc.xslt"/>
	<xsl:include href="search/projects_search.inc.xslt"/>
	<xsl:include href="../project/include/project_competence_sets.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="/root/projects/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($prefix, '/projects/')"/>
	<xsl:template match="project_add">
		<div class="color-green">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_projects_head2"/>
							<div class="content">
								<h3>Создание проекта</h3>
								<xsl:choose>
									<xsl:when test="$user/@id">
										<form action="{$save_prefix}/project_add/" method="post" enctype="multipart/form-data">
											<input type="hidden" name="retpath" value="{$prefix}/p/%NAME%/admin/modules/"/>
											<xsl:call-template name="draw_dt_edit"/>
										</form>
									</xsl:when>
									<xsl:otherwise>
										<p>Чтобы создать проект, сначала нужно <a href="{$reg_url}">зарегистрироваться</a> и <a href="{$login_url}">войти</a>.</p>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Создание проекта'"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/projects/">Проекты</a>
		</div>
		<div class="level2- selected-">Создание проекта</div>
	</xsl:template>
</xsl:stylesheet>
