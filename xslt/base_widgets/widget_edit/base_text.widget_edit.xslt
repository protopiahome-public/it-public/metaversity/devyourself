<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="base_widget_text_edit">
		<input type="hidden" id="widget_text_id" value="{@widget_id}"/>
		<textarea rows="18" cols="40" id="widget_html" name="widget_html" style="width: 100%">
			<xsl:copy-of select="text/html/div/*"/>
		</textarea>
		<br/>
		<input type="hidden" id="jq-widget-edit-id" value="{@widget_id}"/>
		<script type="text/javascript">
			widget_text_edit_init();
		</script>
	</xsl:template>
</xsl:stylesheet>
