<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="base_widget_communities_edit">
		<div class="widget-edit-communities">
			<table>
				<tr>
					<th>Доступные сообщества</th>
					<th>Выбранные сообщества</th>
				</tr>
				<tr>
					<td class="l-">
						<div id="jq-communities-not-selected" class="jq-communities jq-xlightbox-autoheight column-">
							<xsl:for-each select="community">
								<xsl:if test="not(../selected/community[@id = current()/@id])">
									<xsl:call-template name="_draw_community"/>
								</xsl:if>
							</xsl:for-each>
						</div>
					</td>
					<td class="r-">
						<div id="jq-communities-selected" class="jq-communities jq-xlightbox-autoheight column-">
							<xsl:for-each select="selected/community">
								<xsl:call-template name="_draw_community"/>
							</xsl:for-each>
						</div>
					</td>
				</tr>
			</table>
			<div class="note-">Перетащите мышкой в правую колонку те сообщества, которые вы хотите увидеть в виджете.</div>
		</div>
		<input type="hidden" id="jq-widget-edit-id" value="{@widget_id}"/>
		<script type="text/javascript">
			widget_communities_edit_init();
		</script>
	</xsl:template>
	<xsl:template name="_draw_community">
		<xsl:for-each select="/root/community_short[@id = current()/@id]">
			<table class="jq-community community-" jq-community-id="{@id}" id="jq-community-{@id}" jq-sort-data="{@title}">
				<tr>
					<td class="logo- jq-move-handle">
						<img src="{logo_small/@url}" width="{logo_small/@width}" height="{logo_small/@height}" alt=""/>
					</td>
					<td class="title- jq-move-handle">
						<a target="_blank" href="{@url}">
							<xsl:value-of select="@title"/>
						</a>
					</td>
				</tr>
			</table>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
