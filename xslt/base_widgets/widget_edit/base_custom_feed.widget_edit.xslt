<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="base_widget_custom_feed_edit">
		<xsl:choose>
			<xsl:when test="/root/*[name() = 'project_custom_feed_full' or name() = 'community_custom_feed_full'][@id = current()/feed/@id]">
				<div class="field">
					<label class="title-" for="jq-widget-feed-selected-id">Выберите ленту:</label>
					<div class="input-">
						<select id="jq-widget-feed-selected-id">
							<xsl:variable name="data" select="."/>
							<xsl:for-each select="feed">
								<xsl:for-each select="/root/*[name() = 'project_custom_feed_full' or name() = 'community_custom_feed_full'][@id = current()/@id]">
									<option value="{@id}">
										<xsl:if test="$data/@selected_feed_id = @id">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="@title"/>
									</option>
								</xsl:for-each>
							</xsl:for-each>
						</select>
					</div>
				</div>
				<div class="field">
					<label class="title-" for="jq-widget-lister-item-count">Количество записей в виджете:</label>
					<div class="input-">
						<input id="jq-widget-lister-item-count" class="input-text" value="{@lister_item_count}"/>
					</div>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<p>Ни одной ленты не найдено.</p>
				<input type="hidden" id="jq-widget-edit-no-feeds" value="1"/>
			</xsl:otherwise>
		</xsl:choose>
		<input type="hidden" id="jq-widget-edit-id" value="{@widget_id}"/>
		<script type="text/javascript">
			widget_custom_feed_edit_init();
		</script>
	</xsl:template>
</xsl:stylesheet>
