<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="base_widget_lister_edit">
		<div class="field">
			<label class="title-" for="jq-widget-lister-item-count">
				<xsl:text>Количество </xsl:text>
				<xsl:choose>
					<xsl:when test="@type_id = 'last_posts' or @type_id = 'last_comments' or @type_id = 'custom_feed'">
						<xsl:text>записей</xsl:text>
					</xsl:when>
					<xsl:when test="@type_id = 'last_users' or @type_id = 'top_users'">
						<xsl:text>пользователей</xsl:text>
					</xsl:when>
					<xsl:when test="@type_id = 'materials'">
						<xsl:text>материалов</xsl:text>
					</xsl:when>
					<xsl:when test="@type_id = 'events'">
						<xsl:text>событий</xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:text> в виджете:</xsl:text>
			</label>
			<div class="input-">
				<input id="jq-widget-lister-item-count" class="input-text" value="{@lister_item_count}"/>
			</div>
		</div>
		<input type="hidden" id="jq-widget-edit-id" value="{@widget_id}"/>
		<script type="text/javascript">
			widget_lister_edit_init();
		</script>
	</xsl:template>
</xsl:stylesheet>
