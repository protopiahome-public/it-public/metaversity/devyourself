<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_widget_add_form">
		<div class="widgets-add">
			<div class="buttons-">
				<xsl:for-each select="widget">
					<xsl:choose>
						<xsl:when test="@type_id = 'last_posts' and $current_project/@communities_are_on = '0'"/>
						<xsl:when test="@type_id = 'last_comments' and $current_project/@communities_are_on = '0'"/>
						<xsl:when test="@type_id = 'communities' and $current_project/@communities_are_on = '0'"/>
						<xsl:when test="@type_id = 'custom_feed' and $current_project/@communities_are_on = '0'"/>
						<xsl:when test="@type_id = 'events' and $current_project/@events_are_on = '0'"/>
						<xsl:when test="@type_id = 'calendar' and $current_project/@events_are_on = '0'"/>
						<xsl:when test="@type_id = 'materials' and $current_project/@materials_are_on = '0'"/>
						<xsl:when test="/root/community_short and @type_id = 'child_communities' and $current_community/@allow_child_communities = 'nobody' and $current_community/@child_community_count_calc = 0"/>
						<xsl:otherwise>
							<a href="#" jq-type-id="{@type_id}" jq-column="{../@column}">
								<xsl:attribute name="class">
									<xsl:text>button-add</xsl:text>
									<xsl:choose>
										<xsl:when test="@disabled = 1"> button-add-disabled</xsl:when>
										<xsl:otherwise> jq-widget-add-type</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
								<span>
									<xsl:value-of select="@title"/>
								</span>
							</a>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</div>
			<div class="clear"/>
			<div class="bottom-">Если какая-то кнопка неактивна, значит, виджет уже добавлен на страницу, а второй такой же добавлять не имеет смысла.</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
