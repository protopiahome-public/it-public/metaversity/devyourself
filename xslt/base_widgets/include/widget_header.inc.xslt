<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_widget_header">
		<xsl:param name="title"/>
		<xsl:param name="light_header"/>
		<div>
			<xsl:attribute name="class">
				<xsl:text>jq-widget-header header-</xsl:text>
				<xsl:if test="$light_header"> header-light-</xsl:if>
				<xsl:if test="$title = ''"> header-hidden-</xsl:if>
			</xsl:attribute>
			<table cellspacing="0">
				<tr>
					<td>
						<h2 class="text-">
							<xsl:value-of select="$title"/>
						</h2>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
