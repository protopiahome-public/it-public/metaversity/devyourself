<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_widget_posts">
		<xsl:param name="data"/>
		<xsl:param name="feed_url"/>
		<xsl:choose>
			<xsl:when test="not(post)">
				<div class="content-message- content-message-empty-">
					<p class="text-">Ни одной записи не найдено.</p>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<div class="content-">
					<ul>
						<xsl:for-each select="post">
							<xsl:variable name="post" select="/root/post_full[@id = current()/@id]"/>
							<xsl:variable name="post_user" select="/root/user_short[@id = $post/@author_user_id]"/>
							<xsl:variable name="post_community" select="/root/community_short[@id = $post/@community_id]"/>
							<li class="item-">
								<div class="title- bigger">
									<a href="{$post_community/@url}post-{@id}/">
										<xsl:value-of select="$post/@title"/>
									</a>
								</div>
								<div class="section-">
									<span class="label-">Из: </span>
									<a href="{$post_community/@url}">
										<xsl:value-of select="$post_community/@title"/>
									</a>
									<xsl:if test="$post/@section_id &gt; 0">
										<span class="section-sep">&#160;</span>
										<a href="{$post_community/@url}{$post/@section_name}/">
											<xsl:value-of select="$post/@section_title"/>
										</a>
									</xsl:if>
								</div>
								<div class="author- user-block">
									<span class="label-">Автор: </span>
									<span class="avatar-">
										<span>
											<a href="{$current_project/@url}users/{$post_user/@login}/">
												<img src="{$post_user/photo_small/@url}" width="{$post_user/photo_small/@width}" height="{$post_user/photo_small/@height}" alt="{$post_user/@login}"/>
											</a>
										</span>
									</span>
									<span class="text-">
										<a href="{$current_project/@url}users/{$post_user/@login}/">
											<xsl:value-of select="$post_user/@visible_name"/>
										</a>
										<b>
											<xsl:value-of select="$post_user/@login"/>
										</b>
									</span>
								</div>
								<div class="comments-">
									<span>
										<xsl:attribute name="class">
											<xsl:text>text- small</xsl:text>
											<xsl:if test="position() = 1"> first-</xsl:if>
										</xsl:attribute>
										<xsl:text>Комментарии: </xsl:text>
									</span>
									<a href="{$post_community/@url}post-{@id}/#comments">
										<xsl:attribute name="class">bigger 
											<xsl:choose>
												<xsl:when test="$post/@comment_count_calc > 0"> bold</xsl:when>
											</xsl:choose>
										</xsl:attribute>
										<xsl:value-of select="$post/@comment_count_calc"/>
									</a>
								</div>
							</li>
						</xsl:for-each>
					</ul>
				</div>
			</xsl:otherwise>
		</xsl:choose>
		<div class="footer-">
			<a href="{$feed_url}">Все записи</a>
		</div>
	</xsl:template>
</xsl:stylesheet>
