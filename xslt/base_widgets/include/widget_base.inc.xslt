<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="widget_header.inc.xslt"/>
	<xsl:include href="../../_site/include/access.inc.xslt"/>
	<xsl:template match="widget" mode="widget_base">
		<xsl:param name="hide_widget" select="false()"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<xsl:if test="$hide_widget">
			<xsl:attribute name="style">display: none;</xsl:attribute>
		</xsl:if>
		<xsl:if test="$can_moderate_widgets = 1">
			<div class="jq-widget-edit-mode widget-controls dn">
				<span class="jq-widget-edit-mode jq-move-handle button- move-" title="Двигать (зацепите иконку мышкой и перетащите виджет на его новое место)">&#160;</span>
				<xsl:if test="@is_editable = 1">
					<span class="jq-widget-edit button- edit-" title="Редактировать">&#160;</span>
				</xsl:if>
				<span class="jq-widget-set-title button- title-" title="Редактировать заголовок">&#160;</span>
				<span class="jq-widget-delete button- delete-" title="Удалить виджет">&#160;</span>
			</div>
		</xsl:if>
		<div class="widget-inner-wrap-">
			<xsl:apply-templates select="/root/node()[@widget_id = current()/@id]" mode="widget">
				<xsl:with-param name="data" select="."/>
				<xsl:with-param name="can_moderate_widgets" select="$can_moderate_widgets"/>
			</xsl:apply-templates>
		</div>
	</xsl:template>
</xsl:stylesheet>
