<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- @2xxx судя по названию файла, шаблоны должны именоваться на так,
	 а base_text_widget и т. п. Лишние инверсии выносят мозг. 
	 
	 UPDATE: давай обсудим именование шаблонов
	 -->
	<xsl:template name="base_text_widget">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<div class="widget widget-text">
			<xsl:call-template name="draw_widget_header">
				<xsl:with-param name="title" select="$data/@title"/>
				<xsl:with-param name="light_header" select="false()"/>
			</xsl:call-template>
			<div class="content-">
				<xsl:copy-of select="html/div"/>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
