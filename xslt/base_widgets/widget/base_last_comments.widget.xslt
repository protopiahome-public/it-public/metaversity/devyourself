<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="base_widget_last_comments">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<xsl:param name="current_container"/>
		<div class="widget widget-last-comments">
			<xsl:call-template name="draw_widget_header">
				<xsl:with-param name="title" select="$data/@title"/>
				<xsl:with-param name="light_header" select="true()"/>
			</xsl:call-template>
			<xsl:choose>
				<xsl:when test="@module_disabled = 1">
					<div class="content-message- content-message-access-">
						<p class="text-">Сообщества и записи в данном проекте отключены.</p>
					</div>
				</xsl:when>
				<xsl:when test="@module_no_access = 1">
					<!-- @dm9 Блок с сообщением об ограниченном доступе-->
					<div class="content-message- content-message-access-">
						<xsl:call-template name="draw_access">
							<xsl:with-param name="status" select="$project_access/@access_communities_read"/>
							<xsl:with-param name="entity_genitiv" select="'проекта'"/>
							<xsl:with-param name="join_premoderation" select="$project_access/@join_premoderation = 1"/>
							<xsl:with-param name="for">Доступ к сообществам ограничен. Для получения доступа</xsl:with-param>
							<xsl:with-param name="draw_instruction" select="true()"/>
						</xsl:call-template>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="not(post)">
							<div class="content-message- content-message-empty-">
								<p class="text-">Ни одной записи не найдено.</p>
							</div>
						</xsl:when>
						<xsl:otherwise>
							<div class="content-">
								<ul>
									<xsl:for-each select="post">
										<xsl:variable name="post" select="/root/post_full[@id = current()/@id]"/>
										<xsl:variable name="comment_user" select="/root/user_short[@id = current()/@last_comment_author_user_id]"/>
										<xsl:variable name="post_community" select="/root/community_short[@id = $post/@community_id]"/>
										<li>
											<xsl:attribute name="class">
												<xsl:text>item-</xsl:text>
												<xsl:if test="position() = 1"> item-first-</xsl:if>
												<xsl:if test="position() = last()"> item-last-</xsl:if>
											</xsl:attribute>
											<div class="title- bigger">
												<a href="{$post_community/@url}post-{@id}/">
													<xsl:value-of select="$post/@title"/>
												</a>
											</div>
											<div class="section-">
												<span class="label-">Из: </span>
												<a href="{$post_community/@url}">
													<xsl:value-of select="$post_community/@title"/>
												</a>
												<xsl:if test="$post/@section_id &gt; 0">
													<span class="section-sep">&#160;</span>
													<a href="{$post_community/@url}{$post/@section_name}/">
														<xsl:value-of select="$post/@section_title"/>
													</a>
												</xsl:if>
											</div>
											<div class="author- user-block">
												<span class="label-">Автор: </span>
												<span class="avatar-">
													<span>
														<a href="{$current_project/@url}users/{$comment_user/@login}/">
															<img src="{$comment_user/photo_small/@url}" width="{$comment_user/photo_small/@width}" height="{$comment_user/photo_small/@height}" alt="{$comment_user/@login}"/>
														</a>
													</span>
												</span>
												<span class="text-">
													<a href="{$current_project/@url}users/{$comment_user/@login}/">
														<xsl:value-of select="$comment_user/@visible_name"/>
													</a>
													<b>
														<xsl:value-of select="$comment_user/@login"/>
													</b>
												</span>
											</div>
											<div class="comment-">
												<xsl:copy-of select="last_comment_html/div"/>
												<a class="readmore-" href="{$post_community/@url}post-{@id}/#comment-{@last_comment_id}">...</a>
											</div>
										</li>
									</xsl:for-each>
								</ul>
							</div>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
