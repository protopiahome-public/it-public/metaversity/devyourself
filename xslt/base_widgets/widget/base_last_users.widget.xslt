<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="base_widget_last_users">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<xsl:param name="current_container"/>
		<div class="widget widget-last-users">
			<xsl:call-template name="draw_widget_header">
				<xsl:with-param name="title" select="$data/@title"/>
				<xsl:with-param name="light_header" select="false()"/>
			</xsl:call-template>
			<xsl:choose>
				<xsl:when test="user">
					<div class="content- user-block">
						<ul>
							<xsl:for-each select="user">
								<xsl:call-template name="_last_users_draw_user">
									<xsl:with-param name="current_container" select="$current_container"/>
								</xsl:call-template>
							</xsl:for-each>
						</ul>
					</div>
					<div class="footer-">
						<a href="{$current_container/@url}users/">Все участники</a>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<div class="content-message- content-message-empty-">
						<p class="text-">Ни одного активного участника пока не найдено.</p>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="_last_users_draw_user">
		<xsl:param name="current_container"/>
		<li class="item-">
			<xsl:for-each select="/root/user_short[@id = current()/@id][1]">
				<span class="avatar-">
					<span>
						<a href="{$current_project/@url}users/{@login}/">
							<img src="{photo_small/@url}" width="{photo_small/@width}" height="{photo_small/@height}" alt="{@login}"/>
						</a>
					</span>
				</span>
				<span class="text-">
					<a href="{$current_project/@url}users/{@login}/">
						<xsl:value-of select="@visible_name"/>
					</a>
					<b>
						<xsl:value-of select="@login"/>
					</b>
				</span>
				<div class="clear">&#160;</div>
			</xsl:for-each>
		</li>
	</xsl:template>
</xsl:stylesheet>
