<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="../include/widget_posts_common.inc.xslt"/>
	<xsl:template name="base_widget_custom_feed">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<xsl:param name="current_feed"/>
		<xsl:param name="current_feed_posts"/>
		<div class="widget widget-last-posts">
			<xsl:call-template name="draw_widget_header">
				<xsl:with-param name="title" select="$data/@title"/>
				<xsl:with-param name="light_header" select="true()"/>
			</xsl:call-template>
			<xsl:choose>
				<xsl:when test="@module_disabled = 1">
					<div class="content-message- content-message-access-">
						<p class="text-">Сообщества и записи в данном проекте отключены.</p>
					</div>
				</xsl:when>
				<xsl:when test="@module_no_access = 1">
					<!-- @dm9 Блок с сообщением об ограниченном доступе-->
					<div class="content-message- content-message-access-">
						<xsl:call-template name="draw_access">
							<xsl:with-param name="status" select="$project_access/@access_communities_read"/>
							<xsl:with-param name="entity_genitiv" select="'проекта'"/>
							<xsl:with-param name="join_premoderation" select="$project_access/@join_premoderation = 1"/>
							<xsl:with-param name="for">Доступ к сообществам ограничен. Для получения доступа</xsl:with-param>
							<xsl:with-param name="draw_instruction" select="true()"/>
						</xsl:call-template>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="not($current_feed/@id)">
							<div class="content-message- content-message-config-">
								<xsl:choose>
									<xsl:when test="$can_moderate_widgets = 1">
										<p class="text-">Вам необходимо выбрать ленту — источник записей.</p>
										<p class="button-">
											<span class="jq-widget-edit b-text- clickable">Настроить виджет</span>
										</p>
									</xsl:when>
									<xsl:otherwise>
										<p class="text-">Виджет пока не настроен.</p>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</xsl:when>
						<xsl:otherwise>
							<div class="content- content-feed-">
								<xsl:for-each select="$current_feed_posts">
									<xsl:call-template name="draw_widget_posts">
										<xsl:with-param name="data" select="$data"/>
										<xsl:with-param name="feed_url" select="$current_feed/@url"/>
									</xsl:call-template>
								</xsl:for-each>
							</div>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
