<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="base_widget_top_users">
		<xsl:param name="data"/>
		<xsl:param name="can_moderate_widgets" select="false()"/>
		<xsl:param name="current_container"/>
		<div class="widget widget-top-users">
			<xsl:call-template name="draw_widget_header">
				<xsl:with-param name="title" select="$data/@title"/>
				<xsl:with-param name="light_header" select="false()"/>
			</xsl:call-template>
			<xsl:choose>
				<xsl:when test="/root/node()[@widget_id = $data/@id][@by_comments >= 0]/user">
					<div class="content-">
						<div class="buttons- wide-only-">
							<h3 class="button- button-selected-">
								<span class="text-">По записям</span>
							</h3>
							<h3 class="button- button-right- button-selected-">
								<span class="text-">По комментариям</span>
							</h3>
						</div>
						<div class="buttons- buttons-active- narrow-only-">
							<h3 class="toggle button-selected- button-" jq-toggle="jq-toggle-top-users-posts" jq-toggle-group="jq-toggle-top-users" jq-toggle-active-element="jq-toggle-top-users-posts" jq-toggle-active-class="button-selected-">
								<span class="text-">По записям</span>
							</h3>
							<h3 class="toggle button-" jq-toggle="jq-toggle-top-users-comments" jq-toggle-group="jq-toggle-top-users" jq-toggle-active-element="jq-toggle-top-users-posts" jq-toggle-active-class="button-selected-">
								<span class="text-">По комментариям</span>
							</h3>
						</div>
						<div class="user-block content-wrap-">
							<ul id="jq-toggle-top-users-posts" class="jq-toggle-top-users column-">
								<xsl:for-each select="/root/node()[@widget_id = $data/@id][@by_comments = 0]/user">
									<xsl:call-template name="_top_users_draw_user">
										<xsl:with-param name="current_container" select="$current_container"/>
										<xsl:with-param name="rating" select="@post_count_calc"/>
										<xsl:with-param name="type" select="'post'"/>
									</xsl:call-template>
								</xsl:for-each>
							</ul>
							<ul id="jq-toggle-top-users-comments" class="jq-toggle-top-users column- column-right- column-hidden-">
								<xsl:for-each select="/root/node()[@widget_id = $data/@id][@by_comments = 1]/user">
									<xsl:call-template name="_top_users_draw_user">
										<xsl:with-param name="current_container" select="$current_container"/>
										<xsl:with-param name="rating" select="@comment_count_calc"/>
										<xsl:with-param name="type" select="'comment'"/>
									</xsl:call-template>
								</xsl:for-each>
							</ul>
						</div>
					</div>
					<div class="footer-">
						<a href="{$current_container/@url}users/">Все участники</a>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<div class="content-message- content-message-empty-">
						<p class="text-">Ни одного активного участника пока не найдено.</p>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="_top_users_draw_user">
		<xsl:param name="current_container"/>
		<xsl:param name="rating"/>
		<xsl:param name="type"/>
		<li class="item-">
			<xsl:for-each select="/root/user_short[@id = current()/@id][1]">
				<span class="avatar-">
					<span>
						<a href="{$current_project/@url}users/{@login}/">
							<img src="{photo_small/@url}" width="{photo_small/@width}" height="{photo_small/@height}" alt="{@login}"/>
						</a>
					</span>
				</span>
				<span class="text-">
					<a href="{$current_project/@url}users/{@login}/">
						<xsl:value-of select="@visible_name"/>
					</a>
					<b>
						<xsl:value-of select="@login"/>
					</b>
				</span>
				<span class="number-">
					<xsl:attribute name="title">
						<xsl:choose>
							<xsl:when test="$type = 'post'">Количество записей</xsl:when>
							<xsl:when test="$type = 'comment'">Количество комментариев</xsl:when>
						</xsl:choose>
					</xsl:attribute>
					<xsl:value-of select="$rating"/>
				</span>
				<div class="clear">&#160;</div>
			</xsl:for-each>
		</li>
	</xsl:template>
</xsl:stylesheet>
