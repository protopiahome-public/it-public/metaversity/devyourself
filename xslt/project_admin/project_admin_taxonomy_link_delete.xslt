<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/delete.inc.xslt"/>
	<xsl:include href="menu/project_admin_taxonomy_link_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'taxonomy_links'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="taxonomy_link_id" select="/root/project_admin_taxonomy_link_delete/@id"/>
	<xsl:variable name="taxonomy_link_title" select="/root/project_admin_taxonomy_link_delete/@title"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/links/', $taxonomy_link_id, '/delete/')"/>
	<xsl:template match="project_admin_taxonomy_link_delete">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_taxonomy">
								<xsl:with-param name="base_url" select="concat($current_project/@url, 'admin/')"/>
							</xsl:call-template>
						</td>
						<td class="center-column">
							<h2 class="admin">
								<xsl:value-of select="$taxonomy_link_title"/>
								<xsl:text> (внешняя ссылка)</xsl:text>
							</h2>
							<xsl:call-template name="draw_project_admin_taxonomy_link_submenu"/>
							<div class="content">
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/project_admin_taxonomy_link_delete/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$current_project/@url}admin/"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<xsl:call-template name="draw_delete">
				<xsl:with-param name="title_akk" select="'ссылку'"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/tree.jquery.js"/>
		<script type="text/javascript" src="{$prefix}/js/project_admin_taxonomy.js"/>
	</xsl:template>	
	<xsl:template mode="title" match="/root">
		<xsl:text>Удаление внешней ссылки &#8212; </xsl:text>
		<xsl:value-of select="$taxonomy_link_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
