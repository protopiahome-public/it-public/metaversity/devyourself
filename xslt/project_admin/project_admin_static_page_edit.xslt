<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="../block_sets/include/block_set_edit.inc.xslt"/>
	<xsl:include href="menu/project_admin_static_page_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'taxonomy'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="static_page_id" select="/root/project_admin_static_page_edit/document/@id"/>
	<xsl:variable name="static_page_title" select="/root/project_admin_static_page_edit/document/field[@name = 'title']"/>
	<xsl:variable name="static_page_name" select="/root/project_admin_static_page_edit/document/field[@name = 'static_page_name']"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/static-pages/', $static_page_name, '/')"/>
	<xsl:variable name="admin_url" select="concat($current_project/@url, 'admin/')"/>
	<xsl:template match="project_admin_static_page_edit">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_taxonomy">
								<xsl:with-param name="base_url" select="concat($current_project/@url, 'admin/')"/>
							</xsl:call-template>
						</td>
						<td class="center-column">
							<h2 class="admin">
								<xsl:value-of select="$static_page_title"/>
								<xsl:text> (страница)</xsl:text>
							</h2>
							<xsl:call-template name="draw_project_admin_static_page_submenu"/>
							<div class="content">
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/project_admin_static_page_edit/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$admin_url}static-pages/%STATIC_PAGE_NAME%/"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<xsl:if test="$pass_info/info[@name = 'SAVED']">
				<div class="info">
					<span>Данные сохранены. <a href="{$current_project/@url}{document/field[@name = 'static_page_name']}/" target="_blank">Просмотреть результат</a>.</span>
				</div>
			</xsl:if>
			<xsl:call-template name="draw_dt_edit">
				<xsl:with-param name="draw_button" select="false()"/>
				<xsl:with-param name="disable_saved_info" select="true()"/>
			</xsl:call-template>
			<xsl:for-each select="/root/block_set">
				<xsl:call-template name="draw_block_set_edit">
					<xsl:with-param name="object_url" select="$module_url"/>
				</xsl:call-template>
			</xsl:for-each>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			var block_set_post_vars = {
				project_id : '<xsl:value-of select="$current_project/@id"/>',
				static_page_id : '<xsl:value-of select="$static_page_id"/>',
				block_set_id : '<xsl:value-of select="/root/block_set/@id"/>'
			};
			var block_set_ajax_url = global.ajax_prefix + 'project_admin_static_page_block_set/';
		</script>
		<script type="text/javascript" src="{$prefix}/editors/tiny_mce/jquery.tinymce.js"/>
		<script type="text/javascript" src="{$prefix}/js/swfobject.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.uploadify.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.scrollTo.js"/>
		<script type="text/javascript" src="{$prefix}/js/tree.jquery.js"/>
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
		<script type="text/javascript" src="{$prefix}/js/block_set.js"/>
		<script type="text/javascript" src="{$prefix}/js/project_admin_taxonomy.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Основные свойства страницы &#8212; </xsl:text>
		<xsl:value-of select="$static_page_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
