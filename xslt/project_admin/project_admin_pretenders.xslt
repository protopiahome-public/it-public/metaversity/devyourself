<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_users_submenu.inc.xslt"/>
	<xsl:include href="../_site/include/admin_pretenders.inc.xslt"/>
	<xsl:include href="../users/search/users_admin_search.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'pretenders'"/>
	<xsl:variable name="admin_section_main_page" select="/root/project_admin_pretenders/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/users/pretenders/')"/>
	<xsl:template match="project_admin_pretenders">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_users_submenu"/>
						</td>
						<td class="center-column">
							<div class="content">
								<h3>Премодерация участников</h3>
								<xsl:call-template name="draw_users_admin_search"/>
								<form action="{$save_prefix}/project_admin_pretenders/" method="post">
									<input type="hidden" name="retpath" value="{$module_url}"/>
									<input type="hidden" name="project_id" value="{$current_project/@id}"/>
									<xsl:call-template name="draw_admin_pretenders"/>
								</form>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="draw_pretenders_not_found">
		<xsl:if test="not(user)">
			<p>Ни одного участника, ожидающего вступления в проект, не найдено.</p>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="head" match="/root">
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Премодерация участников &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
