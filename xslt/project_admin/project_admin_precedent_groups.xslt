<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_marks_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'pgroups'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/marks/pgroups/')"/>
	<xsl:template match="project_admin_precedent_groups">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_taxonomy">
								<xsl:with-param name="base_url" select="concat($current_project/@url, 'admin/')"/>
							</xsl:call-template>
						</td>
						<td class="center-column">
							<h2 class="admin">Прецеденты</h2>
							<xsl:call-template name="draw_project_admin_marks_submenu"/>
							<div class="content">
								<xsl:call-template name="_draw_list"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<div class="ico-links-small">
			<a class="add-" href="{$module_url}add/">
				<span>добавить группу прецедентов</span>
			</a>
		</div>
		<div id="jq-sort" class="sort-list">
			<xsl:choose>
				<xsl:when test="$pass_info/info[@name = 'action'] = 'dt_add' and $pass_info/info[@name = 'SAVED']">
					<div class="info">
						<span>Добавлена новая группа прецедентов</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/info[@name = 'DELETED']">
					<div class="info">
						<span>Группа прецедентов удалена</span>
					</div>
				</xsl:when>
			</xsl:choose>
			<xsl:for-each select="precedent_group">
				<div id="i-{@id}" class="item-">
					<span class="sort-handler"/>
					<span class="title-">
						<a href="{$module_url}{@id}/">
							<xsl:value-of select="@title"/>
						</a>
						<xsl:text> (</xsl:text>
						<span class="with-hint" title="Количество дочерних прецедентов">
							<xsl:value-of select="@precedent_count"/>
							<xsl:if test="position() = 1">
								<xsl:call-template name="count_case">
									<xsl:with-param name="number" select="@precedent_count"/>
									<xsl:with-param name="word_ns" select="' прецедент'"/>
									<xsl:with-param name="word_gs" select="' прецедента'"/>
									<xsl:with-param name="word_ap" select="' прецедента'"/>
								</xsl:call-template>
							</xsl:if>
						</span>
						<xsl:text>)</xsl:text>
					</span>
					<span class="icons- icons-small">
						<a class="edit- gray-" href="{$module_url}{@id}/" title="Редактировать"/>
						<a class="delete- gray-" href="{$module_url}{@id}/delete/" title="Удалить">
							<xsl:if test="@precedent_count != 0">
								<xsl:attribute name="href">javascript:void(0);</xsl:attribute>
								<xsl:attribute name="title">Удаление невозможно, так как есть дочерние прецеденты</xsl:attribute>
								<xsl:attribute name="class">delete- gray- disabled-</xsl:attribute>
							</xsl:if>
						</a>
					</span>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/sort.js"/>
		<script type="text/javascript">
			var post_vars = {
				project_id : '<xsl:value-of select="$current_project/@id"/>'
			};
			var sort_ajax_ctrl = 'precedent_groups_sort';
		</script>
		<script type="text/javascript" src="{$prefix}/js/tree.jquery.js"/>
		<script type="text/javascript" src="{$prefix}/js/project_admin_taxonomy.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Группы прецедентов &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/projects/">Проекты</a>
		</div>
		<div class="level2-">
			<a href="{$current_project/@url}">
				<xsl:value-of select="$current_project/@title"/>
			</a>
		</div>
		<div class="level3-">
			<a href="{$current_project/@url}admin/">Админка</a>
		</div>
		<div class="level4- selected-">Группы прецедентов</div>
	</xsl:template>
</xsl:stylesheet>
