<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="menu/project_admin_integration_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'endpoint'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/integration/endpoint')"/>
	<xsl:template match="project_admin_endpoint">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
						</td>
						<td class="center-column">
							<div class="content">
								<h3>Аутентификация</h3>
								<xsl:call-template name="_draw_info"/>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_info">
		<div class="box">
			<p>
				<xsl:text>Документация по настройке аутентификации через </xsl:text>
				<xsl:choose>
					<xsl:when test="$sys_params/@is_devyourself = 1">DevYourself</xsl:when>
					<xsl:otherwise>
						<xsl:text>проект </xsl:text>
						<xsl:value-of select="$sys_params/@title"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text> появится позже. Пока используйте данный механизм на свой страх и риск (желательно при этом примерно понимать, как это работает, либо спросить у нас: </xsl:text>
				<a class="email" href="mailto:{$sys_params/@info_email_obscured}?subject=Auth%20integration">
					<xsl:value-of select="$sys_params/@info_email_obscured"/>
				</a>
				<xsl:text>).</xsl:text>
			</p>
			<xsl:if test="@endpoint_last_connect_result != 'NO_DATA'">
				<hr/>
				<p>
					<xsl:text>Последнее обращние к Endpoint URL: </xsl:text>
					<strong>
						<xsl:call-template name="get_full_datetime">
							<xsl:with-param name="datetime" select="@endpoint_last_connect_time"/>
						</xsl:call-template>
					</strong>
					<xsl:text>.</xsl:text>
				</p>
				<p>
					<xsl:text>Результат: </xsl:text>
					<strong>
						<xsl:value-of select="@endpoint_last_connect_result"/>
						<xsl:call-template name="_draw_failed_request_details">
							<xsl:with-param name="curl_errno" select="@endpoint_last_connect_curl_error"/>
							<xsl:with-param name="http_code" select="@endpoint_last_connect_http_code"/>
						</xsl:call-template>
					</strong>
					<xsl:text>.</xsl:text>
				</p>
				<xsl:if test="@api_is_last_attempt">
					<p>Это была пятая и <strong>последняя</strong> неудачная попытка обращения к вашему серверу. Для восстановления коммуникации с вашим сервером выполните вход в проект с вашего сайта.</p>
				</xsl:if>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/project_admin_endpoint/" method="post" enctype="multipart/form-data">
			<xsl:call-template name="draw_dt_edit">
				<xsl:with-param name="disable_saved_info" select="$pass_info/info[@name = 'action'] = 'dt_add'"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template match="field[@type = 'url_string' and ancestor::doctype]" mode="dtf_error_extend">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'PING_FAIL']">
					<xsl:variable name="reason" select="$pass_info/error[@field = current()/@name and @name = 'PING_FAIL']"/>
					<xsl:choose>
						<xsl:when test="$reason = 'REQUEST_FAILED'">
							<div class="error-">
								<span>
									<xsl:text>Не удалось установить соединение с сервером</xsl:text>
									<xsl:call-template name="_draw_failed_request_details">
										<xsl:with-param name="curl_errno" select="$pass_info/info[@name = 'CURL_ERROR']"/>
										<xsl:with-param name="http_code" select="$pass_info/info[@name = 'HTTP_CODE']"/>
									</xsl:call-template>
								</span>
							</div>
						</xsl:when>
						<xsl:when test="$reason = 'INCORRECT_RESPONSE'">
							<div class="error- error-long-">
								<span>Полученный ответ на тестовый запрос имеет некорректный формат. Проверьте, что по данному адресу располагается библиотека, совместимая с <xsl:value-of select="$request/@main_host_name"/>.</span>
							</div>
						</xsl:when>
						<xsl:when test="$reason = 'INCORRECT_VERSION'">
							<div class="error- error-long-">
								<span>
									<xsl:text>Ваша библиотека имеет неверную версию. Ожидаемая версия &#8212; </xsl:text>
									<xsl:value-of select="$pass_info/info[@name = 'API_VERSION']"/>
									<xsl:text>. Ваша версия &#8212; </xsl:text>
									<xsl:value-of select="$pass_info/info[@name = 'YOUR_VERSION']"/>
									<xsl:text>.</xsl:text>
								</span>
							</div>
						</xsl:when>
						<xsl:when test="$reason = 'INCORRECT_TEST_VALUE'">
							<div class="error- error-long-">
								<span>Ваш сервер некорректно ответил на тестовый запрос. Проверьте, что по данному адресу располагается библиотека, совместимая с <xsl:value-of select="$request/@main_host_name"/>, и проверьте, что ответы от данной библиотеки не кешируются.</span>
							</div>
						</xsl:when>
					</xsl:choose>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template name="_draw_failed_request_details">
		<xsl:param name="curl_errno"/>
		<xsl:param name="http_code"/>
		<xsl:if test="$curl_errno != '' or $http_code != ''">
			<xsl:text> (</xsl:text>
			<xsl:if test="$curl_errno != ''">
				<xsl:text>CURL_ERROR=</xsl:text>
				<xsl:value-of select="$curl_errno"/>
			</xsl:if>
			<xsl:if test="$http_code != ''">
				<xsl:if test="$curl_errno != ''">; </xsl:if>
				<xsl:text>HTTP_CODE=</xsl:text>
				<xsl:value-of select="$http_code"/>
			</xsl:if>
			<xsl:text>)</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Аутентификация &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
