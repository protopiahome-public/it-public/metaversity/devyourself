<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_roles_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_role_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="../competence_sets/include/rate_show.inc.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'roles'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="role_id" select="/root/project_admin_role_show/@id"/>
	<xsl:variable name="role_title" select="/root/project_admin_role_show/@title"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/roles/roles/', $role_id, '/')"/>
	<xsl:template match="project_admin_role_show">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<xsl:if test="$project_access/@has_admin_rights = 1">
							<td class="left-column">
								<xsl:call-template name="draw_project_admin_taxonomy">
									<xsl:with-param name="base_url" select="concat($current_project/@url, 'admin/')"/>
								</xsl:call-template>
							</td>
						</xsl:if>
						<td class="center-column">
							<h2 class="admin">Рекомендация ролей</h2>
							<xsl:call-template name="draw_project_admin_roles_submenu"/>
							<div class="content">
								<h3>
									<xsl:if test="@enabled = 0">
										<xsl:text>[Отключена] </xsl:text>
									</xsl:if>
									<xsl:value-of select="$role_title"/>
								</h3>
								<xsl:call-template name="draw_project_admin_role_submenu"/>
								<xsl:call-template name="draw_rate_show"/>
							</div>
						</td>
						<xsl:if test="not($project_access/@has_admin_rights = 1)">
							<td class="right-column">

							</td>
						</xsl:if>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/tree.jquery.js"/>
		<script type="text/javascript" src="{$prefix}/js/project_admin_taxonomy.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$role_title"/>
		<xsl:text> &#8212; Роли &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
