<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_roles_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_role_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/delete.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'roles'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="role_id" select="/root/project_admin_role_delete/@id"/>
	<xsl:variable name="role_title" select="/root/project_admin_role_delete/@title"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/roles/roles/', $role_id, '/delete/')"/>
	<xsl:template match="project_admin_role_delete">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<xsl:if test="$project_access/@has_admin_rights = 1">
							<td class="left-column">
								<xsl:call-template name="draw_project_admin_taxonomy">
									<xsl:with-param name="base_url" select="concat($current_project/@url, 'admin/')"/>
								</xsl:call-template>
							</td>
						</xsl:if>
						<td class="center-column">
							<xsl:call-template name="draw_project_admin_roles_submenu"/>
							<div class="content">
								<h3>
									<a href="{$role_url}">
										<xsl:value-of select="$role_title"/>
									</a>
								</h3>
								<xsl:call-template name="draw_project_admin_role_submenu"/>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
						<xsl:if test="not($project_access/@has_admin_rights = 1)">
							<td class="right-column">

							</td>
						</xsl:if>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/project_admin_role_delete/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$roles_url}"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<xsl:call-template name="draw_delete">
				<xsl:with-param name="title_akk" select="'роль'"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/tree.jquery.js"/>
		<script type="text/javascript" src="{$prefix}/js/project_admin_taxonomy.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Удаление роли &#8212; </xsl:text>
		<xsl:value-of select="$role_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
