<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_communities_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'communities_menu'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/communities/menu/add/')"/>
	<xsl:template match="communities_menu">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<xsl:call-template name="draw_communities_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_taxonomy">
								<xsl:with-param name="base_url" select="concat($current_project/@url, 'admin/')"/>
							</xsl:call-template>
						</td>
						<td class="center-column">
							<h2 class="admin">Сообщества</h2>
							<xsl:call-template name="draw_project_admin_communities_submenu"/>
							<div class="content">
								<h3>Добавление в меню сообщества</h3>
								<fieldset class="fake-">
									<div class="field">
										<form action="{$save_prefix}/project_admin_communities_menu/" method="post">
											<input type="hidden" name="project_id" value="{$current_project/@id}"/>
											<input type="hidden" name="action" value="add"/>
											<input type="hidden" name="retpath" value="{$current_project/@url}admin/communities/menu/"/>
											<label class="title-" for="f-{@name}">
												<xsl:text>Введите URL сообщества (или любой его страницы)</xsl:text>
												<xsl:text>:</xsl:text>
												<span class="star">
													<xsl:text>&#160;*</xsl:text>
												</span>
											</label>
											<div class="input-">
												<input class="input-text" type="text" name="add_community_url" value="">
													<xsl:if test="@max_length != 0">
														<xsl:attribute name="maxlength">
															<xsl:value-of select="@max_length"/>
														</xsl:attribute>
													</xsl:if>
													<xsl:if test="$pass_info/vars/var[@name = 'add_community_url']">
														<xsl:attribute name="value">
															<xsl:value-of select="$pass_info/vars/var[@name = 'add_community_url']"/>
														</xsl:attribute>
													</xsl:if>
												</input>
												<xsl:if test="$pass_info/error[@name = 'wrong_add_community_url']">
													<div class="error-">
														<span>Неверный адрес сообщества</span>
													</div>
												</xsl:if>
												<xsl:if test="$pass_info/error[@name = 'add_community_already_exists']">
													<div class="error-">
														<span>Сообщество уже есть в меню</span>
													</div>
												</xsl:if>
											</div>
											<div class="comment-">
												<xsl:text>Например: </xsl:text>
												<xsl:value-of select="$request/@domain_prefix"/>
												<xsl:value-of select="$current_project/@url"/>
												<xsl:text>co/somecommunity/. Разрешено ссылаться только на сообщества </xsl:text>
												<strong>текущего</strong>
												<xsl:text> проекта.</xsl:text>
											</div>
											<button class="button button-small" style="margin-top: 10px;">Добавить</button>
										</form>
									</div>
								</fieldset>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Добавление в меню сообщества &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
