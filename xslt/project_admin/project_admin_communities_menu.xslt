<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_communities_submenu.inc.xslt"/>
	<xsl:include href="../communities/menu/communities_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'communities_menu'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/communities/menu/')"/>
	<xsl:template match="communities_menu">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<xsl:call-template name="draw_communities_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_taxonomy">
								<xsl:with-param name="base_url" select="concat($current_project/@url, 'admin/')"/>
							</xsl:call-template>
						</td>
						<td class="center-column">
							<h2 class="admin">Сообщества</h2>
							<xsl:call-template name="draw_project_admin_communities_submenu"/>
							<div class="content">
								<xsl:call-template name="_draw_list"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<div id="dialog-confirm-delete" class="dn">
			<p>Подтвердите: сообщество будет удалено из меню.</p>
		</div>
		<div class="box">
			<p>Здесь вы можете создать меню, содержащее самые важные 
				<em>сообщества</em> вашего проекта. Меню будет отображаться только в разделе &#171;Сообщества&#187;.
			</p>
		</div>
		<div class="ico-links-small">
			<a class="add-" href="{$module_url}add/">
				<span>добавить в меню сообщество</span>
			</a>
		</div>
		<div id="jq-sort" class="sort-list">
			<xsl:choose>
				<xsl:when test="$pass_info/info[@name = 'action'] = 'add' and $pass_info/info[@name = 'SAVED']">
					<div class="info">
						<span>Добавлен новый пункт меню</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/info[@name = 'DELETED']">
					<div class="info">
						<span>Пункт меню удален</span>
					</div>
				</xsl:when>
			</xsl:choose>
			<xsl:for-each select="item">
				<div id="i-{@id}" class="item-">
					<xsl:variable name="community" select="/root/community_short[@id = current()/@community_id]"/>
					<span class="sort-handler"/>
					<span class="title-">
						<a target="_blank" href="{$community/@url}">
							<xsl:value-of select="$community/@title"/>
						</a>
					</span>
					<span class="icons- icons-small">
						<a class="delete- gray- jq-community-delete" href="javascript:void(0);" data-community-id="{@community_id}" data-community-title="{@title}" title="Удалить из меню"/>
					</span>
				</div>
			</xsl:for-each>
			<xsl:for-each select="/root/community_short[@id = current()/item/@community_id]">
				<form class="dn" id="community_delete_form_{@id}" action="{$save_prefix}/project_admin_communities_menu/" method="post">
					<input type="hidden" name="action" value="delete"/>
					<input type="hidden" name="delete_community_id" value="{@id}"/>
					<input type="hidden" name="project_id" value="{$current_project/@id}"/>
				</form>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/sort.js"/>
		<script type="text/javascript" src="{$prefix}/js/communities_menu.js"/>
		<script type="text/javascript">
			var post_vars = {
			project_id : '
			<xsl:value-of select="$current_project/@id"/>'
			};
			var sort_ajax_ctrl = 'project_admin_communities_menu_sort';
		</script>
		<script type="text/javascript" src="{$prefix}/js/tree.jquery.js"/>
		<script type="text/javascript" src="{$prefix}/js/project_admin_taxonomy.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Меню сообществ &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
