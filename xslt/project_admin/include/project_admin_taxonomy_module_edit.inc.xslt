<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_project_admin_taxonomy_module_edit">
		<input type="hidden" name="taxonomy_module_id" value="{$selected_taxonomy_item_id}"/>
		<fieldset class="fake-">
			<div class="field">
				<div class="input-">
					<input id="show_in_menu" class="input-checkbox" type="checkbox" name="show_in_menu" value="1">
						<xsl:choose>
							<xsl:when test="$pass_info/vars/var[@name = 'show_in_menu'] = 1">
								<xsl:attribute name="checked">checked</xsl:attribute>
							</xsl:when>
							<xsl:when test="$pass_info/vars/var"/>
							<xsl:when test="$selected_taxonomy_item/@show_in_menu = 1">
								<xsl:attribute name="checked">checked</xsl:attribute>
							</xsl:when>
						</xsl:choose>
					</input>
					<label for="show_in_menu" class="inline-">Показывать в меню</label>
				</div>
			</div>
		</fieldset>
	</xsl:template>
</xsl:stylesheet>
