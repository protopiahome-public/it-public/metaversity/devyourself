<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_project_admin_taxonomy_module_disable">
		<div class="page-admin-structure-module-disable">
			<form action="{$save_prefix}/project_admin_taxonomy_module_delete/" method="post" enctype="multipart/form-data">
				<input type="hidden" name="retpath" value="{$current_project/@url}admin/"/>
				<input type="hidden" name="project_id" value="{$current_project/@id}"/>
				<input type="hidden" name="id" value="{$selected_taxonomy_item_id}"/>
				<button class="button button-red">Отключить модуль</button>
				<div class="delete-note-">
					<div>Модуль будет отключен, но данные не удалятся.</div>
					<div>Вы всегда сможете заново включить модуль на странице <a href="{$current_project/@url}admin/modules/">Управление модулями</a>.</div>
				</div>
			</form>
		</div>
	</xsl:template>
</xsl:stylesheet>
