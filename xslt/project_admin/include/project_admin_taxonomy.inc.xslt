<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="selected_taxonomy_item_id" select="/root/project_admin_taxonomy/@selected_item_id"/>
	<xsl:variable name="selected_taxonomy_item" select="/root/project_menu//item[@id = $selected_taxonomy_item_id]"/>
	<xsl:variable name="selected_taxonomy_item_title" select="$selected_taxonomy_item/@title"/>
	<xsl:template name="draw_project_admin_taxonomy">
		<xsl:for-each select="/root/project_admin_taxonomy[1]">
			<div class="widget-wrap">
				<div class="widget widget-menu widget-menu-admin">
					<div class="header-">
						<table cellspacing="0">
							<tr>
								<td>
									<h2 class="text-">Структура проекта</h2>
								</td>
							</tr>
						</table>
					</div>
					<div class="items-">
						<div>
							<xsl:attribute name="class">
								<xsl:text>menu-item- menu-item-structure- </xsl:text>
								<xsl:if test="$admin_section = 'modules'">menu-item-selected- </xsl:if>
							</xsl:attribute>
							<div>
								<xsl:choose>
									<xsl:when test="$admin_section = 'modules' and not($get_vars)">Управление модулями</xsl:when>
									<xsl:otherwise>
										<a href="{$current_project/@url}admin/modules/">Управление модулями</a>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
						<div class="menu-item- menu-item-add- menu-item-expander-">
							<div>
								<span class="toggle" jq-toggle="jq-menu-add">Добавить</span>
							</div>
						</div>
						<div id="jq-menu-add" class="dd-wrap- dn">
							<div class="dd- dd-add-">
								<table cellspacing="0">
									<tr>
										<td>
											<a class="jq-add" id="jq-add-page" href="javascript:void(0);">
												<span>страницу</span>
											</a>
										</td>
									</tr>
									<tr>
										<td>
											<a class="jq-add" id="jq-add-folder" href="javascript:void(0);">
												<span>выпадающее меню</span>
											</a>
										</td>
									</tr>
									<tr>
										<td>
											<a class="jq-add" id="jq-add-link" href="javascript:void(0);">
												<span>внешнюю ссылку</span>
											</a>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="clear"/>
					</div>
					<hr/>
					<div id="jq-taxonomy-tree" class="jq-taxonomy-tree"/>
					<script type="text/javascript">
						var taxonomy_tree_ajax_url = global.ajax_prefix + 'project_admin_taxonomy/';
						var taxonomy_admin_root_url = '<xsl:value-of select="concat($current_project/@url, 'admin/')"/>';
						var taxonomy_tree_init_data = <xsl:value-of select="tree_init_data"/>;
						var project_id = <xsl:value-of select="$current_project/@id"/>;
					</script>
					<div class="move-hint-">Для пересортировки используйте
						<br/>перетягивание пунктов меню мышью.
					</div>
				</div>
			</div>
			<div class="dn">
				<div id="jq-lightbox-add-page" class="lightbox-admin-structure-add">
					<input id="jq-lightbox-add-page-retpath" type="hidden" value="{$current_project/@url}admin/static-pages/%NAME%/"/>
					<div class="field">
						<label for="jq-lightbox-add-page-title">
							<xsl:text>Заголовок: </xsl:text>
							<span class="star">*</span>
						</label>
						<div class="input-">
							<input id="jq-lightbox-add-page-title" type="text" class="input-text" value="" maxlength="200"/>
						</div>
						<div id="jq-lightbox-add-page-error-title-blank" class="jq-lightbox-add-error error- dn">
							<span>Это поле необходимо заполнить.</span>
						</div>
					</div>
					<div class="field">
						<label for="jq-lightbox-add-page-name">
							<xsl:text>URL: </xsl:text>
							<span class="star">*</span>
						</label>
						<div class="input-">
							<input id="jq-lightbox-add-page-name" type="text" class="input-text" value="" maxlength="200"/>
						</div>
						<div id="jq-lightbox-add-page-error-name-used" class="jq-lightbox-add-error error- dn">
							<span>Этот адрес 
								<a id="jq-lightbox-add-page-error-name-used-link" jq-href-template="{$current_project/@url}admin/static-pages/%NAME%/" target="_blank" href="#">уже используется</a>.
							</span>
						</div>
						<div id="jq-lightbox-add-page-error-name-blank" class="jq-lightbox-add-error error- dn">
							<span>Это поле необходимо заполнить.</span>
						</div>
						<div id="jq-lightbox-add-page-error-name-wrong" class="jq-lightbox-add-error error- dn">
							<span>Использованы недопустимые символы.</span>
						</div>
						<div class="comment-">Разрешены английские буквы, цифры и дефис</div>
					</div>
				</div>
				<div id="jq-lightbox-add-folder" class="lightbox-admin-structure-add">
					<input id="jq-lightbox-add-folder-retpath" type="hidden" value="{$current_project/@url}admin/folders/%ID%/"/>
					<div class="field">
						<label for="jq-lightbox-add-folder-title">
							<xsl:text>Заголовок: </xsl:text>
							<span class="star">*</span>
						</label>
						<div class="input-">
							<input id="jq-lightbox-add-folder-title" type="text" class="input-text" value="" maxlength="200"/>
						</div>
						<div id="jq-lightbox-add-folder-error-title-blank" class="jq-lightbox-add-error error- dn">
							<span>Это поле необходимо заполнить.</span>
						</div>
					</div>
				</div>
				<div id="jq-lightbox-add-link" class="lightbox-admin-structure-add">
					<input id="jq-lightbox-add-link-retpath" type="hidden" value="{$current_project/@url}admin/links/%ID%/"/>
					<div class="field">
						<label for="jq-lightbox-add-link-title">
							<xsl:text>Заголовок: </xsl:text>
							<span class="star">*</span>
						</label>
						<div class="input-">
							<input id="jq-lightbox-add-link-title" type="text" class="input-text" value="" maxlength="200"/>
						</div>
						<div id="jq-lightbox-add-link-error-title-blank" class="jq-lightbox-add-error error- dn">
							<span>Это поле необходимо заполнить.</span>
						</div>
					</div>
					<div class="field">
						<label for="jq-lightbox-add-link-url">
							<xsl:text>Ссылка: </xsl:text>
							<span class="star">*</span>
						</label>
						<div class="input-">
							<input id="jq-lightbox-add-link-url" type="text" class="input-text" value="" maxlength="200"/>
						</div>
						<div id="jq-lightbox-add-link-error-url-blank" class="jq-lightbox-add-error error- dn">
							<span>Это поле необходимо заполнить.</span>
						</div>
						<div id="jq-lightbox-add-link-error-url-wrong" class="jq-lightbox-add-error error- dn">
							<span>Введена некорректная ссылка.</span>
						</div>
						<div class="comment-">Ссылка должна начинаться с http:// или https://</div>
					</div>
				</div>
			</div>
			<!--div style="float: left; padding-right: 30px;">
				<div>
					<button class="toggle" jq-toggle="jq-toggle-item-add-buttons">Добавить</button>
					<div id="jq-toggle-item-add-buttons" class="dn">
						<xsl:for-each select="/root/project_admin_taxonomy_add_form/page_type">
							<div>
								<xsl:choose>
									<xsl:when test="@name = 'static_page'">
										<a>
											<xsl:attribute name="href">
												<xsl:choose>
													<xsl:when test="$selected_taxonomy_item/@type = 'folder' and $selected_taxonomy_item/@parent_id = ''">
														<xsl:value-of select="concat($current_project/@url, 'admin/folders/', $selected_taxonomy_item_id, '/add/static-page/')"/>
													</xsl:when>
													<xsl:when test="$selected_taxonomy_item/@type = 'static_page' and $selected_taxonomy_item/@parent_id = ''">
														<xsl:value-of select="concat($current_project/@url, 'admin/static-pages/', $selected_taxonomy_item/@static_page_name, '/add/static-page/')"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="concat($current_project/@url, 'admin/static-pages/add/')"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:value-of select="@title"/>
										</a>
									</xsl:when>
									<xsl:when test="@name = 'link'">
										<a>
											<xsl:attribute name="href">
												<xsl:choose>
													<xsl:when test="$selected_taxonomy_item/@type = 'folder' and $selected_taxonomy_item/@parent_id = ''">
														<xsl:value-of select="concat($current_project/@url, 'admin/folders/', $selected_taxonomy_item_id, '/add/link/')"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="concat($current_project/@url, 'admin/links/add/')"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:value-of select="@title"/>
										</a>
									</xsl:when>
									<xsl:when test="@name = 'folder'">
										<a href="{$current_project/@url}admin/folders/add/">
											<xsl:value-of select="@title"/>
										</a>
									</xsl:when>
									<xsl:when test="@name = 'module' and @enabled = 0">
										<form method="post" action="{$save_prefix}/project_admin_taxonomy_module_add/">
											<input type="hidden" name="module_name" value="{@module_name}"/>
											<input type="hidden" name="project_id" value="{$current_project/@id}"/>
											<xsl:choose>
												<xsl:when test="$selected_taxonomy_item/@type = 'folder' and $selected_taxonomy_item/@parent_id = ''">
													<input type="hidden" name="parent_id" value="{$selected_taxonomy_item_id}"/>
												</xsl:when>
												<xsl:otherwise>
													<input type="hidden" name="parent_id" value="0"/>
												</xsl:otherwise>
											</xsl:choose>
											<input type="hidden" name="retpath" value="{$current_project/@url}admin/{@module_name}/"/>
											<input type="submit" value="{@title}"/>
										</form>
									</xsl:when>
								</xsl:choose>
							</div>
						</xsl:for-each>
					</div>
				</div>
			</div-->
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
