<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="intmenu_url" select="concat($current_project/@url, 'admin/integration/intmenu/')"/>
	<xsl:variable name="intmenu_item_url" select="concat($intmenu_url, $intmenu_item_id, '/')"/>
	<xsl:template name="draw_project_admin_intmenu_item_submenu">
		<div class="ico-links">
			<xsl:choose>
				<xsl:when test="/root/project_admin_intmenu_item_edit">
					<strong class="edit- current-">
						<span>редактировать</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="edit-" href="{$intmenu_item_url}">
						<span>редактировать</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/project_admin_intmenu_item_delete">
					<strong class="delete- current-">
						<span>удалить</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="delete-" href="{$intmenu_item_url}delete/">
						<span>удалить</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
