<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_project_admin_roles_submenu">
		<xsl:param name="base_url" select="concat($current_project/@url, 'admin/')"/>
		<xsl:if test="$project_access/@has_admin_rights = 1">
			<div class="head2 head2-admin-">
				<table class="head2-table-">
					<tr>
						<td class="head2-table-right- no-avatar-">
							<div class="menu-">
								<div>
									<xsl:attribute name="class">
										<xsl:text>menu-item- </xsl:text>
										<xsl:if test="$admin_section = 'roles_settings'">menu-item-selected- </xsl:if>
									</xsl:attribute>
									<table class="border-">
										<tr>
											<td class="l-"> </td>
											<td class="c-">
												<xsl:choose>
													<xsl:when test="$admin_section = 'roles_settings' and $admin_section_main_page and not($get_vars)">Основные свойства</xsl:when>
													<xsl:otherwise>
														<a href="{$base_url}roles/">Основные свойства</a>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td class="r-"> </td>
										</tr>
									</table>
								</div>
								<div class="menu-sep-">|</div>
								<div>
									<xsl:attribute name="class">
										<xsl:text>menu-item- </xsl:text>
										<xsl:if test="$admin_section = 'roles'">menu-item-selected- </xsl:if>
									</xsl:attribute>
									<table class="border-">
										<tr>
											<td class="l-"> </td>
											<td class="c-">
												<xsl:choose>
													<xsl:when test="$admin_section = 'roles' and $admin_section_main_page and not($get_vars)">Роли</xsl:when>
													<xsl:otherwise>
														<a href="{$base_url}roles/roles/">Роли</a>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td class="r-"> </td>
										</tr>
									</table>
								</div>
								<div class="menu-sep-">|</div>
								<div>
									<xsl:attribute name="class">
										<xsl:text>menu-item- </xsl:text>
										<xsl:if test="$admin_section = 'roles_disable'">menu-item-selected- </xsl:if>
									</xsl:attribute>
									<table class="border-">
										<tr>
											<td class="l-"> </td>
											<td class="c-">
												<xsl:choose>
													<xsl:when test="$admin_section = 'roles_disable' and $admin_section_main_page and not($get_vars)">Отключение</xsl:when>
													<xsl:otherwise>
														<a href="{$base_url}roles/disable/">Отключение</a>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td class="r-"> </td>
										</tr>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
