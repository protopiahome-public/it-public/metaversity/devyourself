<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_project_admin_users_submenu">
		<xsl:param name="base_url" select="concat($current_project/@url, 'admin/')"/>
		<div class="widget-wrap">
			<div class="widget widget-menu">
				<div class="items-">
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'members'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'members' and $admin_section_main_page and not($get_vars)">Участники</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}users/members/">Участники</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<xsl:if test="$current_project/@join_premoderation = 1">
						<div>
							<xsl:attribute name="class">
								<xsl:text>menu-item- </xsl:text>
								<xsl:if test="$admin_section = 'pretenders'">menu-item-selected- </xsl:if>
							</xsl:attribute>
							<div>
								<xsl:choose>
									<xsl:when test="$admin_section = 'pretenders' and $admin_section_main_page and not($get_vars)">Премодерация участников</xsl:when>
									<xsl:otherwise>
										<a href="{$base_url}users/pretenders/">Премодерация участников</a>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
					</xsl:if>
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'moderators'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'moderators' and $admin_section_main_page and not($get_vars)">Модераторы</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}users/moderators/">Модераторы</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'admins'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'admins' and $admin_section_main_page and not($get_vars)">Администраторы</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}users/admins/">Администраторы</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div class="clear"/>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
