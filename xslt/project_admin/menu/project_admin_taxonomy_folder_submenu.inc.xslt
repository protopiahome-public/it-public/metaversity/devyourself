<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="taxonomy_folder_url" select="concat($current_project/@url, 'admin/folders/', $taxonomy_folder_id, '/')"/>
	<xsl:template name="draw_project_admin_taxonomy_folder_submenu">
		<div class="head2 head2-admin-">
			<table class="head2-table-">
				<tr>
					<td class="head2-table-right- no-avatar-">
						<div class="menu-">
							<div>
								<xsl:attribute name="class">
									<xsl:text>menu-item- </xsl:text>
									<xsl:if test="/root/project_admin_taxonomy_folder_edit">menu-item-selected- </xsl:if>
								</xsl:attribute>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="/root/project_admin_taxonomy_folder_edit">Основные свойства</xsl:when>
												<xsl:otherwise>
													<a href="{$taxonomy_folder_url}">Основные свойства</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
							<div class="menu-sep-">|</div>
							<div>
								<xsl:attribute name="class">
									<xsl:text>menu-item- </xsl:text>
									<xsl:if test="/root/project_admin_taxonomy_folder_delete">menu-item-selected- </xsl:if>
								</xsl:attribute>
								<table class="border-">
									<tr>
										<td class="l-"> </td>
										<td class="c-">
											<xsl:choose>
												<xsl:when test="/root/project_admin_taxonomy_folder_delete">Удаление</xsl:when>
												<xsl:otherwise>
													<a href="{$taxonomy_folder_url}delete/">Удаление</a>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td class="r-"> </td>
									</tr>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
