<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="current_feed" select="/root/project_custom_feed_full"/>
	<xsl:variable name="feeds_url" select="concat($current_project/@url, 'admin/communities/feeds/')"/>
	<xsl:variable name="feed_id" select="$current_feed/@id"/>
	<xsl:variable name="feed_title" select="$current_feed/@title"/>
	<xsl:variable name="feeds_edit_url" select="concat($current_project/@url, 'admin/communities/feeds/')"/>
	<xsl:variable name="feed_edit_url" select="concat($current_project/@url, 'admin/communities/feeds/', $feed_id, '/')"/>
	<xsl:template name="draw_project_admin_custom_feed_submenu">
		<div class="ico-links">
			<xsl:choose>
				<xsl:when test="/root/project_admin_custom_feed_edit">
					<strong class="edit- current-">
						<span>редактировать описание</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="edit-" href="{$feed_edit_url}">
						<span>редактировать описание</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/project_admin_custom_feed_sources">
					<strong class="edit- current-">
						<span>источники</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="edit-" href="{$feed_edit_url}sources/">
						<span>
							<xsl:if test="/root/project_admin_custom_feed_source_add">
								<xsl:attribute name="style">font-weight: bold;</xsl:attribute>
							</xsl:if>
							<xsl:text>источники</xsl:text>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/project_admin_custom_feed_delete">
					<strong class="delete- current-">
						<span>удалить</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="delete-" href="{$feed_edit_url}delete/">
						<span>удалить</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<a class="ext-" target="_blank" href="{$current_feed/@url}">
				<span>просмотреть</span>
			</a>
		</div>
	</xsl:template>
</xsl:stylesheet>
