<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_project_admin_settings_submenu">
		<xsl:param name="base_url" select="concat($current_project/@url, 'admin/')"/>
		<div class="widget-wrap">
			<div class="widget widget-menu">
				<div class="items-">
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'general'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'general' and $admin_section_main_page and not($get_vars)">Основные свойства</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}/settings/">Основные свойства</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'access'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'access' and $admin_section_main_page and not($get_vars)">Управление доступом</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}access/">Управление доступом</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'domain'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'domain' and $admin_section_main_page and not($get_vars)">Свой домен</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}domain/">Свой домен</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div class="clear"/>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
