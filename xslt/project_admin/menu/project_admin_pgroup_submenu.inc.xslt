<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="pgroups_url" select="concat($current_project/@url, 'admin/marks/pgroups/')"/>
	<xsl:variable name="pgroup_url" select="concat($current_project/@url, 'admin/marks/pgroups/', $pgroup_id, '/')"/>
	<xsl:template name="draw_project_admin_pgroup_submenu">
		<div class="ico-links">
			<xsl:choose>
				<xsl:when test="/root/project_admin_precedent_group_edit">
					<strong class="edit- current-">
						<span>редактировать</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="edit-" href="{$pgroup_url}">
						<span>редактировать</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/project_admin_precedent_group_delete">
					<strong class="delete- current-">
						<span>удалить</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="delete-" href="{$pgroup_url}delete/">
						<span>удалить</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
