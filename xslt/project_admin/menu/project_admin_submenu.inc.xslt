<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../include/project_admin_taxonomy.inc.xslt"/>
	<xsl:template name="draw_project_admin_submenu">
		<xsl:param name="base_url"/>
		<xsl:choose>
			<xsl:when test="$project_access/@has_admin_rights = 1">
				<div class="project-admin-menu">
					<table cellspacing="0">
						<tr>
							<td class="icon- icon-structure-"/>
							<td class="link-">
								<div>
									<xsl:attribute name="class">
										<xsl:if test="$selected_taxonomy_item_id &gt; -1 or $admin_section = 'taxonomy_root' or $admin_section = 'modules'">selected- </xsl:if>
									</xsl:attribute>
									<xsl:choose>
										<xsl:when test="$admin_section = 'taxonomy_root'">
											<span>Структура проекта</span>
										</xsl:when>
										<xsl:otherwise>
											<a href="{$current_project/@url}admin/">Структура проекта</a>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</td>
							<td class="icon- icon-settings-"/>
							<td class="link-">
								<div>
									<xsl:attribute name="class">
										<xsl:if test="$admin_section = 'general' or $admin_section = 'access' or $admin_section = 'domain'">selected- </xsl:if>
									</xsl:attribute>
									<xsl:choose>
										<xsl:when test="$admin_section = 'general' and $admin_section_main_page and not($get_vars)">
											<span>Свойства проекта</span>
										</xsl:when>
										<xsl:otherwise>
											<a href="{$current_project/@url}admin/settings/">Свойства проекта</a>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</td>
							<td class="icon- icon-design-"/>
							<td class="link-">
								<div>
									<xsl:attribute name="class">
										<xsl:if test="$admin_section = 'design'">selected- </xsl:if>
									</xsl:attribute>
									<xsl:choose>
										<xsl:when test="$admin_section = 'design' and $admin_section_main_page and not($get_vars)">
											<span>Оформление</span>
										</xsl:when>
										<xsl:otherwise>
											<a href="{$current_project/@url}admin/design/">Оформление</a>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</td>
							<td class="icon- icon-integration-"/>
							<td class="link-">
								<div>
									<xsl:attribute name="class">
										<xsl:if test="$admin_section = 'intmenu' or $admin_section = 'endpoint'">selected- </xsl:if>
									</xsl:attribute>
									<xsl:choose>
										<xsl:when test="$admin_section = 'endpoint' and $admin_section_main_page and not($get_vars)">
											<span>Интеграция</span>
										</xsl:when>
										<xsl:otherwise>
											<a href="{$current_project/@url}admin/integration/endpoint/">Интеграция</a>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</td>
							<td class="icon- icon-users-"/>
							<td class="link-">
								<div>
									<xsl:attribute name="class">
										<xsl:if test="$admin_section = 'pretenders' or $admin_section = 'members' or $admin_section = 'moderators' or $admin_section = 'admins'">selected- </xsl:if>
									</xsl:attribute>
									<xsl:choose>
										<xsl:when test="$admin_section = 'members' and $admin_section_main_page and not($get_vars)">
											<span>Люди</span>
										</xsl:when>
										<xsl:otherwise>
											<a href="{$current_project/@url}admin/users/members/">Люди</a>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</xsl:when>
			<xsl:when test="$project_access/@can_moderate_roles = 1">
				<!-- @dm9 check this in new admin -->
				<div class="box-light box-light-menu">
					<p>
						<strong>Вы модератор ролей.</strong>
						<br/>На этой странице формируется список ролей, которые будут рекомендоваться участникам ролевой игры.
					</p>
				</div>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
