<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="roles_url" select="concat($current_project/@url, 'admin/roles/roles/')"/>
	<xsl:variable name="role_url" select="concat($current_project/@url, 'admin/roles/roles/', $role_id, '/')"/>
	<xsl:template name="draw_project_admin_role_submenu">
		<div class="ico-links">
			<xsl:choose>
				<xsl:when test="/root/project_admin_role_edit">
					<strong class="edit- current-">
						<span>редактировать</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="edit-" href="{$role_url}edit/">
						<span>редактировать</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/project_admin_role_delete">
					<strong class="delete- current-">
						<span>удалить</span>
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<a class="delete-" href="{$role_url}delete/">
						<span>удалить</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
