<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_project_admin_integration_submenu">
		<xsl:param name="base_url" select="concat($current_project/@url, 'admin/')"/>
		<div class="widget-wrap">
			<div class="widget widget-menu">
				<div class="items-">
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'endpoint'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'endpoint' and $admin_section_main_page and not($get_vars)">Аутентификация</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}integration/endpoint/">Аутентификация</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div>
						<xsl:attribute name="class">
							<xsl:text>menu-item- </xsl:text>
							<xsl:if test="$admin_section = 'intmenu'">menu-item-selected- </xsl:if>
						</xsl:attribute>
						<div>
							<xsl:choose>
								<xsl:when test="$admin_section = 'intmenu' and $admin_section_main_page and not($get_vars)">Интеграционное меню</xsl:when>
								<xsl:otherwise>
									<a href="{$base_url}integration/intmenu/">Интеграционное меню</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
					<div class="clear"/>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
