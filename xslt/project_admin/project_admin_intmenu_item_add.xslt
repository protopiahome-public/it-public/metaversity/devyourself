<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_integration_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'intmenu'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="intmenu_url" select="concat($current_project/@url, 'admin/integration/intmenu/')"/>
	<xsl:variable name="module_url" select="concat($intmenu_url, 'add/')"/>
	<xsl:template match="project_admin_intmenu_item_add">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_integration_submenu"/>
						</td>
						<td class="center-column">
							<div class="content">
								<h3 class="pre">
									<a href="{$intmenu_url}">Интеграционное меню</a>
								</h3>
								<h4>Добавление пункта меню</h4>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/project_admin_intmenu_item_add/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$intmenu_url}"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<xsl:call-template name="draw_dt_edit"/>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Добавление пункта интеграционного меню &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/projects/">Проекты</a>
		</div>
		<div class="level2-">
			<a href="{$current_project/@url}">
				<xsl:value-of select="$current_project/@title"/>
			</a>
		</div>
		<div class="level3-">
			<a href="{$current_project/@url}admin/">Админка</a>
		</div>
	</xsl:template>
</xsl:stylesheet>
