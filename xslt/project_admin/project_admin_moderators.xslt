<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_users_submenu.inc.xslt"/>
	<xsl:include href="../_site/include/admin_moderators.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'moderators'"/>
	<xsl:variable name="admin_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/users/moderators/')"/>
	<xsl:template match="project_admin_moderators">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_users_submenu"/>
						</td>
						<td class="center-column">
							<div class="content">
								<h3>Модераторы</h3>
								<xsl:call-template name="draw_admin_moderators">
									<xsl:with-param name="save_ctrl" select="'project_admin_moderators'"/>
									<xsl:with-param name="aux_params">
										<input type="hidden" name="project_id" value="{$current_project/@id}"/>
									</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="draw_moderators_rights_headers">
		<tr>
			<th class="no-border-"/>
			<th class="no-border-"/>
			<th colspan="7" class="center- no-border-">Модератор</th>
			<th class="no-border-"/>
		</tr>
		<tr>
			<th/>
			<th/>
			<th>
				<span>главной&#160;страницы</span>
			</th>
			<th>
				<span>сообществ</span>
			</th>
			<th>
				<span>событий</span>
			</th>
			<th>
				<span>материалов</span>
			</th>
			<th>
				<span>ролей</span>
			</th>
			<th>
				<span>прецедентов</span>
			</th>
			<th>
				<span>оценок</span>
			</th>
			<th/>
		</tr>
	</xsl:template>
	<xsl:template name="draw_moderators_rights_checkboxes">
		<td class="col-checkbox- col-center-">
			<input type="checkbox" name="rights[{@user_id}][is_widget_moderator]" value="1">
				<xsl:if test="@is_widget_moderator = 1">
					<xsl:attribute name="checked">checked</xsl:attribute>
				</xsl:if>
			</input>
		</td>
		<td class="col-checkbox- col-center-">
			<input type="checkbox" name="rights[{@user_id}][is_community_moderator]" value="1">
				<xsl:if test="@is_community_moderator = 1">
					<xsl:attribute name="checked">checked</xsl:attribute>
				</xsl:if>
			</input>
		</td>
		<td class="col-checkbox- col-center-">
			<input type="checkbox" name="rights[{@user_id}][is_event_moderator]" value="1">
				<xsl:if test="@is_event_moderator = 1">
					<xsl:attribute name="checked">checked</xsl:attribute>
				</xsl:if>
			</input>
		</td>
		<td class="col-checkbox- col-center-">
			<input type="checkbox" name="rights[{@user_id}][is_material_moderator]" value="1">
				<xsl:if test="@is_material_moderator = 1">
					<xsl:attribute name="checked">checked</xsl:attribute>
				</xsl:if>
			</input>
		</td>
		<td class="col-checkbox- col-center-">
			<input type="checkbox" name="rights[{@user_id}][is_role_moderator]" value="1">
				<xsl:if test="@is_role_moderator = 1">
					<xsl:attribute name="checked">checked</xsl:attribute>
				</xsl:if>
			</input>
		</td>
		<td class="col-checkbox- col-center-">
			<input type="checkbox" name="rights[{@user_id}][is_precedent_moderator]" value="1">
				<xsl:if test="@is_precedent_moderator = 1">
					<xsl:attribute name="checked">checked</xsl:attribute>
				</xsl:if>
			</input>
		</td>
		<td class="col-checkbox- col-center-">
			<input type="checkbox" name="rights[{@user_id}][is_mark_moderator]" value="1">
				<xsl:if test="@is_mark_moderator = 1">
					<xsl:attribute name="checked">checked</xsl:attribute>
				</xsl:if>
			</input>
		</td>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/admin_moderators.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Модераторы &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
