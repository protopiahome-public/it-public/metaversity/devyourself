<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="include/competence_set_foreign_key.dtf.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'modules'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/modules/')"/>
	<xsl:template match="project_admin_modules">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_taxonomy">
								<xsl:with-param name="base_url" select="concat($current_project/@url, 'admin/')"/>
							</xsl:call-template>
						</td>
						<td class="center-column">
							<h2 class="admin">Управление модулями</h2>
							<div class="content page-modules">
								<xsl:choose>
									<xsl:when test="$pass_info/info[@name = 'module_added']">
										<div class="info-big">
											<p>
												<strong>Изменения сохранены</strong>
											</p>
											<p>
												<xsl:text>Вы подключили модуль «</xsl:text>
												<xsl:value-of select="$pass_info/info[@name = 'module_title']"/>
												<xsl:text>». </xsl:text>
												<a href="{$current_project/@url}admin/{$pass_info/info[@name = 'module_name']}/">Перейти к его настройке</a>
												<xsl:text>.</xsl:text>
											</p>
										</div>
									</xsl:when>
									<xsl:when test="$pass_info/info[@name = 'action'] = 'dt_add' and $pass_info/info[@name = 'SAVED']">
										<div class="info-big">
											<div class="content- bigger">
												<p>
													<strong>Вы только что создали проект!</strong>
												</p>
												<p>Теперь мы рекомендуем продолжить настройку, включив нужные вам модули.</p>
											</div>
										</div>
									</xsl:when>
								</xsl:choose>
								<div>Здесь вы сможете подключить в вашему проекту различные модули.</div>
								<xsl:for-each select="/root/project_admin_taxonomy[1]">
									<xsl:for-each select="/root/project_admin_taxonomy_add_form/page_type[@name = 'module']">
										<div class="jq-module module-">
											<h3>
												<xsl:value-of select="@title"/>
											</h3>
											<xsl:choose>
												<xsl:when test="@module_name = 'communities'">
													<p>Механизм сообществ позволяет участникам проекта вести коллективные блоги и обсуждения. Гибкая система прав позволяет 
														разграничить доступ к сообществам и их разделам.
													</p>
												</xsl:when>
												<xsl:when test="@module_name = 'vector'">
													<p>Вектор — это опрос, позволяющий участнику определиться с направлением саморазвития. 
														На основе Вектора может производиться дальнейшая рекомендация событий, материалов и т. д.
													</p>
												</xsl:when>
												<xsl:when test="@module_name = 'events'">
													<p>События помогут вам в организации лекций, семинаров и других мероприятий и рекомендации их на основе Вектора.</p>
												</xsl:when>
												<xsl:when test="@module_name = 'materials'">
													<p>Материалы предоставляют возможность добавления мультимедийных (включающих тексты, файлы и видео) документов для изучения. 
														Рекомендации, основанные на Векторе, помогут сориентироваться даже среди сотен материалов.
													</p>
												</xsl:when>
												<xsl:when test="@module_name = 'roles'">
													<p>Рекомендация ролей — это модуль, который помогает 
														<a href="http://wiki.devyourself.ru/%D0%98%D0%B3%D1%80%D0%BE%D0%B2%D0%BE%D0%B9_%D0%BC%D0%B0%D1%81%D1%82%D0%B5%D1%80">игровым мастерам</a><xsl:text> </xsl:text><a href="http://wiki.devyourself.ru/Метаигра">Метаигр</a>
														 (и просто ролевых игр) подобрать роль для пользователя, основываясь на его Векторе.
													</p>
												</xsl:when>
												<xsl:when test="@module_name = 'marks'">
													<p>Модуль реализует <a href="http://wiki.devyourself.ru/Прецедентная_модель_оценивания_компетенций">Прецедентную модель оценивания компетенций</a>.</p>
												</xsl:when>
											</xsl:choose>
											<xsl:choose>
												<xsl:when test="@enabled = 1">
													<div class="enabled-">
														<span class="checked-">Подключен</span>
														<span class="setup-">
															<a href="{$current_project/@url}admin/{@module_name}/">Настроить</a>
														</span>
														<span class="disable-">
															<form class="jq-disable-form" action="{$save_prefix}/project_admin_taxonomy_module_delete/" method="post" enctype="multipart/form-data">
																<input type="hidden" name="project_id" value="{$current_project/@id}"/>
																<input type="hidden" name="id" value="{/root/project_menu//item[@module_name = current()/@module_name]/@id}"/>
																<a class="jq-disable-link" href="{$current_project/@url}admin/{@module_name}/disable/">Отключить</a>
															</form>
														</span>
														<span class="disable-hint-">(модуль будет отключен, но данные не удалятся)</span>
													</div>
												</xsl:when>	
												<xsl:otherwise>
													<form class="jq-enable-form" method="post" action="{$save_prefix}/project_admin_taxonomy_module_add/">
														<xsl:choose>
															<xsl:when test="@module_name = 'vector'">
																<xsl:for-each select="/root/project_admin_vector_add[1]">
																	<input type="hidden" name="id" value="{document/@id}"/>
																	<xsl:for-each select="doctype/block">
																		<xsl:apply-templates select="field" mode="dtf_modules"/>
																	</xsl:for-each>
																</xsl:for-each>
															</xsl:when>
															<xsl:when test="@module_name = 'marks'">
																<xsl:for-each select="/root/project_admin_marks_add[1]">
																	<input type="hidden" name="id" value="{document/@id}"/>
																	<xsl:for-each select="doctype/block">
																		<xsl:apply-templates select="field" mode="dtf_modules"/>
																	</xsl:for-each>
																</xsl:for-each>
															</xsl:when>
														</xsl:choose>
														<div class="disabled-">
															<span class="button-">
																<input type="hidden" name="module_name" value="{@module_name}"/>
																<input type="hidden" name="project_id" value="{$current_project/@id}"/>
																<input type="hidden" name="parent_id" value="0"/>
																<button class="button-add jq-enable-button">Подключить</button>
															</span>
															<xsl:if test="@module_name = 'vector' or @module_name = 'marks'">
																<span class="jq-select-hint select-hint-">
																	<xsl:text>Сначала выберите набор компетенций. </xsl:text> 
																	<!--a href="#">Справка</a>
																	<xsl:text>.</xsl:text-->
																</span>
															</xsl:if>
														</div>	
													</form>
												</xsl:otherwise>
											</xsl:choose>
											<div class="clear"/>
										</div>
									</xsl:for-each>
								</xsl:for-each>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/dt.js"/>
		<script type="text/javascript" src="{$prefix}/js/tree.jquery.js"/>
		<script type="text/javascript" src="{$prefix}/js/project_admin_taxonomy.js"/>
		<script type="text/javascript" src="{$prefix}/js/project_admin_modules.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Управление модулями &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
