<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_settings_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'join_form'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/join_form/')"/>
	<xsl:template match="project_admin_join_form">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_settings_submenu"/>
						</td>
						<td class="center-column">
							<div class="content">
								<h3>Форма присоединения к проекту</h3>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<div class="box">
			<p>Здесь вы можете указать дополнительные поля, которые будут запрашиваться в форме присоединения к проекту и отображаться в проектном профиле участника проекта.</p>
		</div>
		<div class="ico-links">
			<a class="add-" href="#">
				<span class="clickable">добавить поле</span>
			</a>
		</div>
		<form action="{$save_prefix}/project_admin_join_form/" method="post">
			<div class="">
				<div class="widget-controls">
					<span class="button- move-" hint="Двигать (зацепите иконку мышкой и перетащите поле на его новое место)"> </span>
					<span class="button- delete-" hint="Удалить"> </span>
				</div>
				<fieldset>
					<div class="field">
						<label for="id1" class="title-">
							<xsl:text>Название поля:</xsl:text>
							<span class="star">&#160;*</span>
						</label>
						<div class="input-">
							<input id="id1" class="input-text" type="text" maxlength="255" value="Факультет"/>
						</div>
						<div class="error-">
							<span>Необходимо ввести название поля.</span>
						</div>
					</div>
					<div class="field">
						<div class="input-">
							<input id="id1-1" class="input-checkbox" type="checkbox" value="1"/>
							<label for="id1-1" class="inline-">Поле обязательно к заполнению</label>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="">
				<div class="widget-controls">
					<span class="button- move-" hint="Двигать (зацепите иконку мышкой и перетащите поле на его новое место)"> </span>
				</div>
				<fieldset>
					<div class="field">
						<label for="id2" class="title-">
							<xsl:text>Название поля:</xsl:text>
							<span class="star">&#160;*</span>
						</label>
						<div class="input-">
							<input id="id2" class="input-text" type="text" readonly="readonly" maxlength="255" value="Игровое имя"/>
						</div>
					</div>
					<p>Отключить игровые имена можно в <a href="{$current_project/@url}admin/settings/#main_other">свойствах проекта</a> (раздел &#171;Разное&#187;).</p>
				</fieldset>
			</div>
			<button class="button">Сохранить</button>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Форма присоединения к проекту &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
