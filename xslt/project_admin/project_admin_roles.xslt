<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_roles_submenu.inc.xslt"/>
	<xsl:include href="../vectors/include/vector_messages.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'roles'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/roles/roles/')"/>
	<xsl:template match="project_admin_roles">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<xsl:if test="$project_access/@has_admin_rights = 1">
							<td class="left-column">
								<xsl:call-template name="draw_project_admin_taxonomy">
									<xsl:with-param name="base_url" select="concat($current_project/@url, 'admin/')"/>
								</xsl:call-template>
							</td>
						</xsl:if>
						<td class="center-column">
							<h2 class="admin">Рекомендация ролей</h2>
							<xsl:call-template name="draw_project_admin_roles_submenu"/>
							<div class="content">
								<xsl:call-template name="_draw_list"/>
							</div>
						</td>
						<xsl:if test="not($project_access/@has_admin_rights = 1)">
							<td class="right-column">

							</td>
						</xsl:if>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<xsl:choose>
			<xsl:when test="$current_project/@vector_competence_set_id &gt; 0">
				<div class="ico-links">
					<a class="add-" href="{$module_url}add/">
						<span>добавить роль</span>
					</a>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="vector_messages_no_competence_set">
					<xsl:with-param name="text" select="'Добавление ролей невозможно'"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="not(project_role)">
			<p>Ролей пока нет.</p>
		</xsl:if>
		<xsl:if test="project_role">
			<table class="tbl">
				<tr>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="'Роль'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_tbl_th">
						<xsl:with-param name="name" select="'competences'"/>
						<xsl:with-param name="title" select="'Кол-во&#160;компетенций'"/>
						<xsl:with-param name="center" select="true()"/>
					</xsl:call-template>
					<th/>
				</tr>
				<xsl:for-each select="project_role">
					<tr>
						<td class="col-title-">
							<xsl:if test="@enabled = 0">
								<xsl:attribute name="class">disabled</xsl:attribute>
							</xsl:if>
							<a href="{$module_url}{@id}/">
								<xsl:if test="@enabled = 0">
									<xsl:text>[Отключена] </xsl:text>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</a>
						</td>
						<td class="col-digits-">
							<xsl:value-of select="@competence_count_calc"/>
						</td>
						<td class="col-icons- icons">
							<a class="edit- gray-" href="{$module_url}{@id}/edit/" title="Редактировать"/>
							<span class="sep-"/>
							<a class="delete- gray-" href="{$module_url}{@id}/delete/" title="Удалить"/>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/tree.jquery.js"/>
		<script type="text/javascript" src="{$prefix}/js/project_admin_taxonomy.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Роли &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
