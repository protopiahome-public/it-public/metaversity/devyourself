<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_marks_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_pgroup_submenu.inc.xslt"/>
	<xsl:include href="../_core/include/delete.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'pgroups'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="pgroup_id" select="/root/project_admin_precedent_group_delete/@id"/>
	<xsl:variable name="pgroup_title" select="/root/project_admin_precedent_group_delete/@title"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/marks/pgroups/')"/>
	<xsl:template match="project_admin_precedent_group_delete">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_taxonomy">
								<xsl:with-param name="base_url" select="concat($current_project/@url, 'admin/')"/>
							</xsl:call-template>
						</td>
						<td class="center-column">
							<h2 class="admin">Прецеденты</h2>
							<xsl:call-template name="draw_project_admin_marks_submenu"/>
							<div class="content">
								<h3>
									<xsl:value-of select="$pgroup_title"/>
								</h3>
								<xsl:call-template name="draw_project_admin_pgroup_submenu"/>
								<xsl:call-template name="_draw_form"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/project_admin_precedent_group_delete/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$pgroups_url}"/>
			<input type="hidden" name="project_id" value="{$current_project/@id}"/>
			<xsl:if test="$pass_info/error[@name = 'CHILDREN_EXIST']">
				<div class="error">
					<span>Группа прецедентов не может быть удалена, так как в ней есть дочерние прецеденты.</span>
				</div>
			</xsl:if>
			<xsl:call-template name="draw_delete">
				<xsl:with-param name="title_akk" select="'группу прецедентов'"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/tree.jquery.js"/>
		<script type="text/javascript" src="{$prefix}/js/project_admin_taxonomy.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Удаление группы прецедентов &#8212; </xsl:text>
		<xsl:value-of select="$pgroup_title"/>
		<xsl:text> &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/projects/">Проекты</a>
		</div>
		<div class="level2-">
			<a href="{$current_project/@url}">
				<xsl:value-of select="$current_project/@title"/>
			</a>
		</div>
		<div class="level3-">
			<a href="{$current_project/@url}admin/">Админка</a>
		</div>
	</xsl:template>
</xsl:stylesheet>
