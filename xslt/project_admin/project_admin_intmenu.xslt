<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_integration_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'intmenu'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/integration/intmenu/')"/>
	<xsl:template match="project_admin_intmenu">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_integration_submenu"/>
						</td>
						<td class="center-column">
							<div class="content">
								<h3>Интеграционное меню</h3>
								<xsl:call-template name="_draw_list"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<div class="ico-links-small">
			<a class="add-" href="{$module_url}add/">
				<span>добавить пункт меню</span>
			</a>
		</div>
		<div id="jq-sort" class="sort-list">
			<xsl:choose>
				<xsl:when test="$pass_info/info[@name = 'action'] = 'dt_add' and $pass_info/info[@name = 'SAVED']">
					<div class="info">
						<span>Добавлен новый пункт меню</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/info[@name = 'DELETED']">
					<div class="info">
						<span>Пункт меню удален</span>
					</div>
				</xsl:when>
			</xsl:choose>
			<xsl:for-each select="intmenu_item">
				<div id="i-{@id}" class="item-">
					<span class="sort-handler"/>
					<span class="title-">
						<a target="_blank" href="{@url}">
							<xsl:value-of select="@title"/>
						</a>
					</span>
					<span class="icons- icons-small">
						<a class="edit- gray-" href="{$module_url}{@id}/" title="Редактировать"/>
						<a class="delete- gray-" href="{$module_url}{@id}/delete/" title="Удалить"/>
					</span>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery-ui.js"/>
		<script type="text/javascript" src="{$prefix}/js/sort.js"/>
		<script type="text/javascript">
			var post_vars = {
				project_id : '<xsl:value-of select="$current_project/@id"/>'
			};
			var sort_ajax_ctrl = 'project_admin_intmenu_sort';
		</script>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Интеграционное меню &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1-">
			<a href="{$prefix}/projects/">Проекты</a>
		</div>
		<div class="level2-">
			<a href="{$current_project/@url}">
				<xsl:value-of select="$current_project/@title"/>
			</a>
		</div>
		<div class="level3-">
			<a href="{$current_project/@url}admin/">Админка</a>
		</div>
		<div class="level4- selected-">Группы прецедентов</div>
	</xsl:template>
</xsl:stylesheet>
