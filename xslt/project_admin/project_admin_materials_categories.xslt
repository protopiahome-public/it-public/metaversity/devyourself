<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../project/head2/project_head2.inc.xslt"/>
	<xsl:include href="menu/project_admin_submenu.inc.xslt"/>
	<xsl:include href="menu/project_admin_materials_submenu.inc.xslt"/>
	<xsl:variable name="top_section" select="'projects'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="project_section" select="'admin'"/>
	<xsl:variable name="project_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'materials_categories'"/>
	<xsl:variable name="admin_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($current_project/@url, 'admin/materials/categories/')"/>
	<xsl:template match="project_admin_materials_categories">
		<div class="color-{$current_project/@color_scheme}">
			<xsl:call-template name="draw_project_head2"/>
			<xsl:call-template name="draw_project_admin_submenu"/>
			<div class="columns-wrap">
				<xsl:call-template name="draw_project_bg"/>
				<table cellspacing="0" class="columns">
					<tr>
						<td class="left-column">
							<xsl:call-template name="draw_project_admin_taxonomy">
								<xsl:with-param name="base_url" select="concat($current_project/@url, 'admin/')"/>
							</xsl:call-template>
						</td>
						<td class="center-column">
							<h2 class="admin">Материалы</h2>
							<xsl:call-template name="draw_project_admin_materials_submenu"/>
							<div class="content">
								<xsl:call-template name="_draw_tree"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div id="dialog-confirm-delete" class="dn">
				<p>Подтвердите: раздел будет удалён.</p>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_tree">
		<div class="ico-links-small">
			<strong id="tree_button_add_group" class="add-group- disabled-">
				<span>добавить раздел</span>
			</strong>
			<strong id="tree_button_edit" class="edit- disabled-">
				<span>редактировать</span>
			</strong>
			<strong id="tree_button_delete" class="delete- disabled-">
				<span>удалить</span>
			</strong>
		</div>
		<div id="jq-tree" class="jq-tree"/>
		<script type="text/javascript">
			var tree_ajax_url = global.ajax_prefix + "project_admin_materials_categories/";
			var project_id = <xsl:value-of select="@project_id"/>;
		</script>
	</xsl:template>
	<xsl:template mode="jquery" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery161.js"> </script>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$prefix}/js/jquery.hotkeys.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.jstree.js"/>
		<script type="text/javascript" src="{$prefix}/js/tree.jquery.js"/>
		<script type="text/javascript" src="{$prefix}/js/jquery.scrollTo.js"/>
		<script type="text/javascript" src="{$prefix}/js/project_admin_resources_categories.js"/>
		<script type="text/javascript" src="{$prefix}/js/project_admin_taxonomy.js"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Разделы материалов &#8212; </xsl:text>
		<xsl:value-of select="$current_project/@title"/>
	</xsl:template>
</xsl:stylesheet>
