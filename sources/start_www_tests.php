<?php

require_once PATH_SOURCES . "/init_www.php";

require_once PATH_CORE . "/user.php";
$user = new user(0);

ini_set("max_execution_time", 0);

if (!$error->is_debug_mode())
{
	die("Access denied");
}

define("TEST_RUN", 1);

require_once PATH_TEST_CORE . "/test_db.php";
require_once PATH_TEST_CORE . "/test_results.php";
require_once PATH_TEST_CORE . "/test_loader.php";
require_once PATH_TEST_CORE . "/base_test.php";
require_once PATH_TEST_CORE . "/base_save_ctrl_test.php";
require_once PATH_TEST_CORE . "/base_tester.php";
require_once PATH_TEST_CORE . "/save_ctrl_tester.php";
require_once PATH_TEST_CORE . "/ajax_ctrl_tester.php";
require_once PATH_TEST_CORE . "/xml_ctrl_tester.php";

/* Tests init */
$db->select_db($config["test_db_name"]);

$db->sql("set sql_mode='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'");

$test_results = new test_results();
if (!isset($_GET["nodb"]))
{
	test_db::init();
}

$test_loader = new test_loader(isset($_GET["file"]) ? $_GET["file"] : "");
$test_loader->run();

output_buffer::set_content($test_results->get_errors_html());

finalize();
?>