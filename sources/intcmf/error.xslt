<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" indent="yes" method="xml" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
	<xsl:decimal-format decimal-separator="." grouping-separator=" "/>
	<xsl:template match="/errors">
		<html>
			<xsl:comment>
				<xsl:text>&#10;</xsl:text> 
				
				<xsl:if test="exception">
					<xsl:for-each select="exception">
						<xsl:text>Uncaught exception: </xsl:text>
						<xsl:value-of select="desc"/>
						<xsl:text> (code = </xsl:text>
						<xsl:value-of select="code"/>
						<xsl:text>)&#10;</xsl:text>
						<xsl:call-template name="draw_text_trace">
							<xsl:with-param name="is_exception" select="true()"/>
						</xsl:call-template>
						<xsl:if test="position() != last() or ../error">
							<xsl:text>&#10;</xsl:text>
						</xsl:if>
					</xsl:for-each>
				</xsl:if>

				<xsl:if test="error">
					<xsl:for-each select="error">
						<xsl:value-of select="error_type"/>
						<xsl:text>: </xsl:text>
						<xsl:value-of select="desc"/>
						<xsl:text>&#10;</xsl:text>
						<xsl:call-template name="draw_text_trace"/>
						<xsl:if test="position() != last()">
							<xsl:text>&#10;</xsl:text>
						</xsl:if>
					</xsl:for-each>
				</xsl:if>

			</xsl:comment>
			<head>
				<title>Errors - IntCMS error handler</title>
				<meta http-equiv="Content-Type" content="text/html; charset={/errors/@output_encoding}"/>
				<script type="text/javascript">
					<xsl:comment>
					var backtrace_visible = true;
					var cache = new Array();
					var widths = new Array();
					function switch_backtrace(el) {
						(function fill_cache() {
							if (cache.length) return true;
							var els = document.getElementsByTagName("TABLE");
							for (var i = 0; i &lt; els.length; i++) {
								if (els[i].className == "error_trace") {
									widths[cache.length] = els[i].clientWidth;
									cache[cache.length] = els[i];
								}
							}
						})();
						backtrace_visible = !backtrace_visible;
						el.innerHTML = backtrace_visible ? "Hide backtraces" : "Show backtraces";
						for (var i = 0; i &lt; cache.length; i++) {
							cache[i].style.display = backtrace_visible ? "block" : "none";
							if (backtrace_visible) cache[i].style.width = widths[i] + "px";
						}
					}
					//</xsl:comment>
					<xsl:text>&#10;</xsl:text>
				</script>
				<style type="text/css">
					body {
						padding: 0;
						margin: 0;
						background: #FFF;
						color: #000;
					}
					body {
						overflow: auto;
					}
					body, td {
						font-size: 9pt;
						font-family: Verdana, Tahoma, Arial, sans-serif;
					}
					.help {
						/* border-bottom: 1px dotted #AAA; */
						cursor: help;
					}
					div#main {
						padding: 0 10px 10px;
					}
					h1 {
						padding: 0;
						margin: 10px 0;
						font-size: 16pt;
						font-weight: bold;
					}
					h2 {
						padding: 0;
						margin: 12px 0 6px;
						font-size: 13pt;
						font-weight: bold;
						cursor: help;
					}
					#url {
						margin-top: 10px;
						padding: 2px 4px;
						background: #EFE;
						border: 1px solid #999;
						line-height: 24px;
					}
					#url a {
						color: #000;
					}
					td.backtrace_hide {
						padding-bottom: 12px;
						vertical-align: bottom;
						padding-left: 50px;
					}
					span.backtrace_hide_link {
						cursor: pointer;
					}
					span.backtrace_hide_link:hover {
						color: #FF8000;
					}
					div.error {
						
					}
					div.error_desc_cont {
						margin: 12px 1px 6px;
					}
					span.error_type {
						color: #555;
					}
					span.error_desc {
					
					}
					span.exception_code {
					
					}
					table.error_trace, table.exception_trace {
						background: #999;
					}
					table.error_trace td, table.exception_trace td {
						padding: 2px 4px;
						background: #EFE;
						font-size: 10pt;
						font-family: 'Courier New', monospace;
					}
					table td.error_file_path_type {
						padding-right: 30px;
					}
					table td.error_file_line {
						padding-right: 30px;
					}
					table td.error_code {
						font-size: 10pt;
						font-family: 'Courier New', monospace;
					}
				</style>
			</head>
			<body>
				<div id="main">
					<div id="url">
						<xsl:text>URL: </xsl:text>
						<a href="{url}">
							<xsl:value-of select="url"/>
						</a>
						<xsl:if test="referer">
							<br/>
							<xsl:text>Referer: </xsl:text>
							<a href="{referer}">
								<xsl:value-of select="referer"/>
							</a>
						</xsl:if>
					</div>
					<xsl:if test="exception">
						<h1>Uncaught exception</h1>
						<!-- It will be only one exception here, but for compatibility... -->
						<xsl:for-each select="exception">
							<div class="error_desc_cont">
								<span class="error_desc">
									<strong>
										<xsl:value-of select="desc"/>
									</strong>
								</span>
								<span class="exception_code">
									<xsl:text> (code = </xsl:text>
									<xsl:value-of select="code"/>
									<xsl:text>)</xsl:text>
								</span>
							</div>
							<xsl:call-template name="draw_trace">
								<xsl:with-param name="is_exception" select="true()"/>
							</xsl:call-template>
						</xsl:for-each>
					</xsl:if>
					<xsl:if test="error">
						<table cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<h1>
										<xsl:text>Error</xsl:text>
										<xsl:if test="error[2]">s</xsl:if>
										<xsl:text> occured while running</xsl:text>
									</h1>
								</td>
								<td class="backtrace_hide">
									<script type="text/javascript">
										document.write('(<span class="backtrace_hide_link" onclick="switch_backtrace(this)">Hide backtraces</span>)');
									</script>
								</td>
							</tr>
						</table>
						<div class="error">
							<xsl:for-each select="error">
								<div class="error_desc_cont">
									<span title="{general_desc}" class="error_type help">
										<xsl:value-of select="error_type"/>
									</span>
									<xsl:text>: </xsl:text>
									<span class="error_desc">
										<strong>
											<xsl:value-of select="desc"/>
										</strong>
									</span>
								</div>
								<xsl:call-template name="draw_trace"/>
							</xsl:for-each>
						</div>
					</xsl:if>
				</div>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="draw_trace">
		<xsl:param name="is_exception" select="false()"/>
		<table cellspacing="1" cellpadding="2" class="error_trace">
			<xsl:if test="$is_exception">
				<xsl:attribute name="class">exception_trace</xsl:attribute>
			</xsl:if>
			<xsl:for-each select="backtrace/item">
				<tr>
					<td class="error_file_path_type">
						<span>
							<xsl:if test="/errors/paths/path[@type = current()/file_path_type]">
								<xsl:attribute name="title">
									<xsl:value-of select="/errors/paths/path[@type = current()/file_path_type]"/>
								</xsl:attribute>
								<xsl:attribute name="class">help</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="file_path_type"/>
						</span>
					</td>
					<td class="error_file_line">
						<span class="help" title="{file_path_full}">
							<xsl:value-of select="file_path_short"/>
						</span>
						<xsl:text> : </xsl:text>
						<span class="help" title="Line number">
							<xsl:value-of select="line"/>
						</span>
					</td>
					<!--td>
						<xsl:value-of select="call" />
					</td-->
					<td class="error_code">
						<xsl:value-of select="line_text" disable-output-escaping="yes"/>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template name="draw_text_trace">
		<xsl:param name="is_exception" select="false()"/>
		<xsl:for-each select="backtrace/item">
			<xsl:if test="/errors/paths/path[@type = current()/file_path_type]">
				<xsl:value-of select="file_path_type"/>
				<xsl:text> : </xsl:text>
			</xsl:if>
			<xsl:value-of select="file_path_short"/>
			<xsl:text> : </xsl:text>
			<xsl:value-of select="line"/>
			<xsl:text>&#10;</xsl:text>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
