<?php

class tree
{

	protected $parent_id_key;
	protected $id_key;
	protected $data;
	protected $data_index = array();
	protected $root;
	protected $do_build_ancestors;
	protected $root_id = "";
	protected $disable_parent_id_data = true;

	public function __construct($data, $do_build_ancestors = true, $parent_id_key = "parent_id", $id_key = "id")
	{
		$this->data = $data;
		$this->parent_id_key = $parent_id_key;
		$this->id_key = $id_key;
		$this->do_build_ancestors = $do_build_ancestors;
		$this->make_tree();
	}

	public function enable_parent_id_data()
	{
		$this->disable_parent_id_data = false;
	}

	public function set_root_id($root_id)
	{
		$this->root_id = $root_id;
	}

	public function make_tree()
	{
		$this->data_index = array();
		foreach ($this->data as $row)
		{
			$this->data_index[$row[$this->id_key]] = array("contents" => $row,
				"children" => array(),
				"id" => $row[$this->id_key],
				"parent_id" => $row[$this->parent_id_key]
			);
			if ($this->disable_parent_id_data)
			{
				unset($this->data_index[$row[$this->id_key]]["contents"][$this->parent_id_key]);
			}
		}

		$root = array("children" => array());

		foreach ($this->data as $row)
		{
			if ($row[$this->parent_id_key] != $this->root_id)
			{
				$this->data_index[$row[$this->parent_id_key]]["children"][] = &$this->data_index[$row[$this->id_key]];
				$this->data_index[$row[$this->id_key]]["parent"] = $this->data_index[$row[$this->parent_id_key]];
			}
			else
			{
				$root["children"][] = &$this->data_index[$row[$this->id_key]];
			}
		}

		$this->root = &$root;

		if ($this->do_build_ancestors)
		{
			$this->build_ancestors($this->root);
		}
	}

	protected function build_ancestors($parent_node, $ancestors = array())
	{
		foreach ($parent_node["children"] as &$node)
		{
			$node["ancestors"] = $ancestors;

			if (count($node["children"]))
			{
				$child_ancestors = $ancestors;
				array_unshift($child_ancestors, $node);
				$this->build_ancestors($node, $child_ancestors);
			}
		}
	}

	public function get_root()
	{
		return $this->root;
	}

	public function get_by_id($id)
	{
		return $this->data_index[$id];
	}

	public function is_with_ancestors()
	{
		return $this->do_build_ancestors;
	}

}

?>
