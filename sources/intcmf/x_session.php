<?php

class x_session_parsed_data
{

	public $session_id;
	public $signature;

}

class x_session
{

	const ERROR_MSG_SESSION_ID_MUST_BE_CREATED = "Session id must be created before this point";
	const ERROR_MSG_BAD_PRIVATE_KEY = "Bad private key: length must be >= 40 characters";
	const ERROR_MSG_FILE_OPERATION = "File operation error";
	const PRIVATE_KEY_REGEXP = "/^.{40}/";
	const SESSION_ID_REGEXP = "/^[0-9a-h]{40}$/";
	const SIGNATURE_REGEXP = "/^[0-9a-h]{40}$/";
	const CLASS_KEY = "0A1F8E2T7H4M3K6L1P3I6F4S5Z2Z3V46T6TY7Y1E8SD02BY36Y49SA1D2AD6E40W1RT2ERT3W6WRT";
	const COOKIE_EXPIRES_TIME = 1751371200; // year 2025

	protected $store_path = null;
	protected $private_key = null;
	protected $cookie_path = null;
	protected $cookie_domain = null;
	protected $cookie_http_only = null;
	protected $session_name = "X_SESSION_ID";
	protected $session_id = null;
	protected $file = null;
	protected $file_lock = null;

	public function __construct($store_path, $private_key, $cookie_path = null, $cookie_domain = null, $cookie_http_only = true, $session_name = null, $write_begin = false)
	{
		$this->store_path = $store_path;
		$this->private_key = $private_key;
		if (!preg_match(self::PRIVATE_KEY_REGEXP, $private_key))
		{
			trigger_error(self::ERROR_MSG_BAD_PRIVATE_KEY);
		}
		$this->cookie_path = $cookie_path;
		if (is_null($this->cookie_path))
		{
			$this->cookie_path = "/";
		}
		$this->cookie_domain = $cookie_domain;
		if (is_null($this->cookie_domain) and isset($_SERVER["HTTP_HOST"]))
		{
			$this->cookie_domain = $_SERVER["HTTP_HOST"];
		}
		$this->cookie_http_only = $cookie_http_only;
		if (!is_null($session_name))
		{
			$this->session_name = $session_name;
		}

		$this->try_auth($write_begin);
		if (!$this->session_id)
		{
			$this->create_new_session($write_begin);
		}
	}

	public function session_write_begin()
	{
		$this->file_load(true);
	}

	public function session_write_commit()
	{
		$this->file_store();
	}

	public function session_write_rollback()
	{
		$this->file_close();
		$this->file_load(false);
	}

	public function is_session_started()
	{
		return !is_null($this->session_id);
	}

	public function get_session_id()
	{
		return $this->session_id;
	}

	public function get_session_name()
	{
		return $this->session_name;
	}

	public function check_session_is_created($write_begin = false)
	{
		// compatibility with older versions
		// session exists at any time now
		if ($write_begin)
		{
			$this->file_load($write_begin);
		}
	}

	public function remove_old_cookie($session_name, $cookie_path = null, $cookie_domain = null)
	{
		if (isset($_COOKIE[$session_name]))
		{
			if (is_null($cookie_path))
			{
				$cookie_path = $this->cookie_path;
			}
			if (is_null($cookie_domain))
			{
				$cookie_domain = $this->cookie_domain;
			}
			setcookie($session_name, "", time() - 30000000, $cookie_path, $cookie_domain);
		}
	}

	protected function try_auth($write_begin)
	{
		$session_data = $this->get_session_data();
		if (!$session_data)
		{
			return;
		}
		$parsed_data = $this->get_parsed_data($session_data);
		if (!$parsed_data or !$this->check_signature($parsed_data))
		{
			return;
		}
		$this->session_id = $parsed_data->session_id;
		$this->file_load($write_begin);
	}

	protected function create_new_session($write_begin)
	{
		do
		{
			$this->session_id = $this->generate_new_session_id();
			$file_path = $this->get_file_path();
		}
		while (file_exists($file_path));

		$this->send_cookie();

		if ($write_begin)
		{
			$this->file_load($write_begin);
		}
	}

	protected function get_session_data()
	{
		if (isset($_POST[$this->session_name]) and is_string($_POST[$this->session_name]))
		{
			return $_POST[$this->session_name];
		}
		elseif (isset($_COOKIE[$this->session_name]) and is_string($_COOKIE[$this->session_name]))
		{
			return $_COOKIE[$this->session_name];
		}
		return null;
	}

	/**
	 * @param string $session_data
	 * @return x_session_parsed_data
	 */
	protected function get_parsed_data($session_data)
	{
		$parts = explode("$$", $session_data);
		if (sizeof($parts) !== 2)
		{
			return null;
		}
		if (!preg_match(self::SESSION_ID_REGEXP, $parts[0]))
		{
			return null;
		}
		if (!preg_match(self::SIGNATURE_REGEXP, $parts[1]))
		{
			return null;
		}
		$parsed_data = new x_session_parsed_data();
		$parsed_data->session_id = $parts[0];
		$parsed_data->signature = $parts[1];
		return $parsed_data;
	}

	protected function file_load($write_begin)
	{
		if ($this->file)
		{
			if ($this->file_lock === LOCK_EX xor $write_begin)
			{
				$this->file_close();
			}
			else
			{
				// Already opened
				return;
			}
		}
		$file_path = $this->get_file_path();
		if (file_exists($file_path) or $write_begin)
		{
			$this->get_file_path(true);
			$this->file = fopen($file_path, "c+b");
			if (!$this->file)
			{
				trigger_error(self::ERROR_MSG_FILE_OPERATION);
				$this->file_close();
				return;
			}
			$this->file_lock = $write_begin ? LOCK_EX : LOCK_SH;
			if (!flock($this->file, $this->file_lock))
			{
				trigger_error(self::ERROR_MSG_FILE_OPERATION);
				$this->file_close();
				return;
			}
			$size = filesize($file_path);
			$data = $size ? fread($this->file, $size) : "";
			if (!$write_begin)
			{
				$this->file_close();
			}
		}
		else
		{
			$data = "";
		}

		$this->fill_superglobal_from_serialized($data);
	}

	protected function file_close()
	{
		if ($this->file)
		{
			flock($this->file, LOCK_UN);
			fclose($this->file);
			$this->file = null;
			$this->file_lock = null;
		}
	}

	protected function file_store()
	{
		if (!$this->file || $this->file_lock !== LOCK_EX)
		{
			trigger_error(self::ERROR_MSG_FILE_OPERATION);
			$this->file_close();
			return;
		}
		$data = $this->get_super_global_serialized();
		ftruncate($this->file, 0);
		fseek($this->file, 0);
		if ($data and !fwrite($this->file, $data))
		{
			trigger_error(self::ERROR_MSG_FILE_OPERATION);
			$this->file_close();
			return;
		}
		$this->file_close();
		if (!$data)
		{
			unlink($this->get_file_path());
		}
	}

	protected function get_file_path($create_directories = false)
	{
		if (!$this->session_id)
		{
			trigger_error(self::ERROR_MSG_SESSION_ID_MUST_BE_CREATED);
			return $this->store_path . "/null";
		}
		$path = $this->store_path;
		$path .= "/" . substr($this->session_id, 0, 1);
		$path .= "/" . substr($this->session_id, 1, 1);
		$path .= "/" . substr($this->session_id, 2, 1);
		if ($create_directories and !file_exists($path))
		{
			mkdir($path, 0700, true);
		}
		$path .= "/" . $this->session_id;
		return $path;
	}

	protected function generate_new_session_id()
	{
		$ip = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "";
		return sha1(rand() . $ip . time() . uniqid("prefix", true));
	}

	protected function get_signature($session_id)
	{
		return sha1(sha1($this->private_key) . self::CLASS_KEY . sha1($session_id));
	}

	protected function check_signature(x_session_parsed_data $parsed_data)
	{
		return $this->get_signature($parsed_data->session_id) === $parsed_data->signature;
	}

	protected function get_session_value()
	{
		return $this->session_id . "$$" . $this->get_signature($this->session_id);
	}

	protected function fill_superglobal_from_serialized($data)
	{
		$_SESSION = $data ? unserialize($data) : array();
		if (!is_array($_SESSION))
		{
			$_SESSION = array();
		}
	}

	protected function get_super_global_serialized()
	{
		return is_array($_SESSION) && sizeof($_SESSION) ? serialize($_SESSION) : null;
	}

	protected function send_cookie()
	{
		$header = "Set-Cookie: ";
		$header .= "{$this->session_name}={$this->get_session_value()}";
		$header .= "; Expires=" . date("r", self::COOKIE_EXPIRES_TIME);
		if ($this->cookie_path)
		{
			$header .= "; path={$this->cookie_path}";
		}
		if ($this->cookie_domain)
		{
			$header .= "; domain={$this->cookie_domain}";
		}
		if ($this->cookie_http_only)
		{
			$header .= "; HttpOnly";
		}
		header($header);
	}

}

?>