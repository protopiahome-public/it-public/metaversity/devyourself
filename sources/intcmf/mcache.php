<?php

class mcache
{

	private $started = false;

	/**
	 * @var Memcache
	 */
	private $memcache;
	private $now;
	private $expire_default = 432000; // 5 days
	private $prefix = null;

	public function __construct($memcache_host, $memcache_port = "11211", $memcache_prefix = null, $expire_default = null)
	{
		if (!is_null($memcache_host))
		{
			$this->memcache = new Memcache;
			if (!$this->memcache->connect($memcache_host, $memcache_port))
			{
				trigger_error("Cannot connect to the memcache server ({$memcache_host}:{$memcache_port})");
			}
			$this->started = true;
		}
		$this->prefix = $memcache_prefix ? $memcache_prefix . "|" : null;
		if (!is_null($expire_default))
		{
			$this->expire_default = $expire_default;
		}
		$this->now = str_replace(",", ".", microtime(true));
	}

	public function add_raw($key, $value, $expire = null)
	{
		if (is_null($expire))
		{
			$expire = $this->expire_default;
		}
		$key_prefixed = $this->add_prefix($key);
		return $this->memcache->add($key_prefixed, $value, 0, $expire);
	}

	public function set_raw($key, $value, $expire = null)
	{
		if (is_null($expire))
		{
			$expire = $this->expire_default;
		}
		$key_prefixed = $this->add_prefix($key);
		if ($this->memcache->replace($key_prefixed, $value, 0, $expire) === false)
		{
			return $this->memcache->set($key_prefixed, $value, 0, $expire);
		}
		return true;
	}

	public function get_raw($key)
	{
		$key_prefixed = $this->add_prefix($key);
		$result = $this->memcache->get($key_prefixed);
		return $this->remove_prefix($key, $result);
	}

	public function delete_raw($key)
	{
		$key_prefixed = $this->add_prefix($key);
		return $this->memcache->delete($key_prefixed);
	}

	public function set_no_tags($key, $value, $expire = null)
	{
		$key = $this->normalize_key($key);
		return $this->set_raw($key, $value, $expire);
	}

	public function get_no_tags($key)
	{
		$key = $this->normalize_key($key);
		return $this->get_raw($key);
	}

	public function get_multi_no_tags($keys, $is_tag_keys = false)
	{
		$result = array();
		if (!empty($keys))
		{
			$result = $this->normalize_keys($keys, true, $is_tag_keys);
			$result_raw = $this->get_raw($keys);
			$result = array_merge($result, $result_raw);
		}
		return $result;
	}

	public function tag_update($tag_key)
	{
		$tag_key = $this->normalize_key($tag_key, true);
		return $this->set_raw($tag_key, $this->now);
	}

	public function tags_get($tag_keys)
	{
		return $this->get_multi_no_tags($tag_keys, true);
	}

	public function set($key, $value, $tag_keys = null, $expire = null)
	{
		$key = $this->normalize_key($key);
		$tags = $this->tags_get($tag_keys);
		foreach ($tags as $idx => &$val)
		{
			if (is_null($val))
			{
				$val = $this->now;
				$this->add_raw($idx, $val);
			}
		}
		$data = array("val" => $value, "tags" => $tags);
		return $this->set_raw($key, $data, $expire);
	}

	public function get($key)
	{
		$key = $this->normalize_key($key);
		$data = $this->get_raw($key);
		if (!$data or !isset($data["val"]) or !isset($data["tags"]))
		{
			return null;
		}
		$tags = $this->get_raw(array_keys($data["tags"]));
		foreach ($data["tags"] as $idx => $val)
		{
			if (!isset($tags[$idx]))
			{
				return null;
			}
			if ((float) $tags[$idx] > (float) $val)
			{
				return null;
			}
		}
		return $data["val"];
	}

	public function delete($key)
	{
		$key = $this->normalize_key($key);
		return $this->delete_raw($key);
	}

	public function get_multi($keys)
	{
		$result = array();
		$result = $this->normalize_keys($keys, true);
		$result_raw = $this->get_raw($keys);

		$tag_keys_normalized = array();
		foreach ($result as $result_key => &$result_val)
		{
			if (isset($result_raw[$result_key]["tags"]))
			{
				foreach ($result_raw[$result_key]["tags"] as $tag_key => $val)
				{
					$tag_keys_normalized[$tag_key] = true;
				}
			}
		}
		$tag_keys_normalized = array_keys($tag_keys_normalized);
		$tags = $this->get_raw($tag_keys_normalized);

		foreach ($result as $result_key => &$result_val)
		{
			if (!isset($result_raw[$result_key]) or !isset($result_raw[$result_key]["val"]) or !isset($result_raw[$result_key]["tags"]))
			{
				$result_val = null;
				continue;
			}
			foreach ($result_raw[$result_key]["tags"] as $idx => $val)
			{
				if (!isset($tags[$idx]))
				{
					$result_val = null;
					continue 2;
				}
				if ((float) $tags[$idx] > (float) $val)
				{
					$result_val = null;
					continue 2;
				}
			}
			$result_val = $result_raw[$result_key]["val"];
		}
		return $result;
	}

	public function flush()
	{
		return $this->memcache->flush();
	}

	private function normalize_key($key, $is_tag_key = false)
	{
		if (is_array($key))
		{
			$key = join("#", $key);
		}
		else
		{
			$key = (string) $key;
		}
		return $is_tag_key ? "%" . $key : $key;
	}

	private function normalize_keys(&$keys, $return_blank_array = false, $is_tag_keys = false)
	{
		if (!is_array($keys))
		{
			return false;
		}
		$result = array();
		foreach ($keys as &$key)
		{
			$key = $this->normalize_key($key, $is_tag_keys);
			if ($return_blank_array)
			{
				$result[$key] = null;
			}
		}
		return $return_blank_array ? $result : true;
	}

	private function add_prefix($data)
	{
		if ($this->prefix)
		{
			if (is_array($data))
			{
				foreach ($data as $key => $value)
				{
					$data[$key] = $this->prefix . $value;
				}
			}
			else
			{
				$data = $this->prefix . $data;
			}
		}
		return $data;
	}

	private function remove_prefix($key, $data)
	{
		if (is_array($key) and is_array($data) and $this->prefix)
		{
			$l = strlen($this->prefix);
			$data_new = array();
			foreach ($data as $key => $value)
			{
				$key_new = substr($key, $l);
				$data_new[$key_new] = $value;
			}
			$data = $data_new;
		}
		return $data;
	}

}

?>