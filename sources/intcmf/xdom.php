<?php

class xdom extends xnode
{

	/**
	 * @var SimpleXMLElement
	 */
	private $simple_xml;

	public function __construct(DOMNode $dom_node, DOMDocument $dom)
	{
		parent::__construct($dom_node, $dom);
	}

	/**
	 * Enter description here...
	 *
	 * @param DOMDocument $dom
	 * @return xdom
	 */
	public static function create_from_dom(DOMDocument $dom)
	{
		$dom_node = $dom->documentElement;
		$xdom = new xdom($dom_node, $dom);
		return $xdom;
	}

	/**
	 * Enter description here...
	 *
	 * @param string $file_name
	 * @return xdom
	 */
	public static function create_from_file($xml_file_name)
	{
		$dom = new DOMDocument("1.0", "UTF-8");
		$ret = $dom->load($xml_file_name);
		if (!$ret)
		{
			return false;
		}
		return self::create_from_dom($dom);
	}

	/**
	 * Enter description here...
	 *
	 * @param string $xml_string
	 * @return xdom
	 */
	public static function create_from_string($xml_string)
	{
		$dom = new DOMDocument("1.0", "UTF-8");
		$ret = $dom->loadXML($xml_string);
		if (!$ret)
		{
			return false;
		}
		return self::create_from_dom($dom);
	}

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $root_node_name
	 * @return xdom
	 */
	public static function create($root_node_name = "root")
	{
		$dom = new DOMDocument("1.0", "UTF-8");
		$dom_node = $dom->createElement($root_node_name);
		$dom->appendChild($dom_node);
		$xdom = new xdom($dom_node, $dom);
		return $xdom;
	}

	/**
	 * Enter description here...
	 *
	 * @return SimpleXMLElement
	 */
	public function get_simple_xml()
	{
		if (is_null($this->simple_xml))
		{
			$this->simple_xml = simplexml_import_dom($this->dom);
		}
		return $this->simple_xml;
	}

	/**
	 * Enter description here...
	 *
	 * @return xnode
	 */
	public function get_root_node()
	{
		return $this->dom_node;
	}

	public function get_xml($no_xml_declaration = false)
	{
		if ($no_xml_declaration)
		{
			return $this->dom->saveXML($this->dom->documentElement);
		}
		else
		{
			return $this->dom->saveXML();
		}
	}

	public function get_xml_as_html()
	{
		return $this->dom->saveHTML();
	}

	public function xslt_process($xslt, $xslt_functions = null)
	{
		$xslt_dom = new DOMDocument();
		$xslt_dom->loadXML($xslt);
		$xslt_proc = new XSLTProcessor();
		$xslt_proc->registerPHPFunctions();
		if ($xslt_functions)
		{
			$xslt_proc->registerPHPFunctions($xslt_functions);
		}
		$xslt_proc->importStyleSheet($xslt_dom);
		$xhtml = $xslt_proc->transformToXML($this->dom);
		return $xhtml;
	}

}

?>