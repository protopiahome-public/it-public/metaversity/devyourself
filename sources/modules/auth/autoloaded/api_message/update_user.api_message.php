<?php

class update_user_api_message extends base_api_message
{

	private $user_id_list;
	private $project_id;

	public function __construct($user_id_array, $project_id)
	{
		$this->project_id = (int) $project_id;
		$this->user_id_list = join(",", $user_id_array);
	}

	public function get_xml()
	{
		global $db;
		$use_game_name = $db->get_value("SELECT use_game_name FROM project WHERE id={$this->project_id}");
		$user_data_array = $db->fetch_all("
			SELECT user.*, upl.game_name, upl.status IN ('member', 'moderator', 'admin') AS is_member
			FROM user, user_project_link AS upl
			WHERE 
				user.id IN ({$this->user_id_list})
				AND user.id = upl.user_id
				AND project_id = {$this->project_id}
		");

		$xdom = xdom::create("message");
		$xdom->set_attr("type", "update_user");
		foreach ($user_data_array as $user_info)
		{
			$user_node = $xdom->create_child_node("user");
			$user_info = api_user_data_helper::clean_user_info($user_info, $use_game_name);
			foreach ($user_info as $key => $value)
			{
				$user_node->set_attr($key, $value);
			}
		}
		return $xdom->get_xml();
	}

}

?>