<?php

class login_api_message extends base_api_message
{

	private $user_id;
	private $user_info;

	public function __construct($user_id, $user_info, $use_game_name)
	{
		$this->user_id = $user_id;
		$this->user_info = api_user_data_helper::clean_user_info($user_info, $use_game_name);
	}

	public function get_xml()
	{
		$xdom = xdom::create("message");
		$xdom->set_attr("type", "login");
		$xdom->set_attr("user_id", $this->user_id);
		$user_node = $xdom->create_child_node("user");
		foreach ($this->user_info as $key => $value)
		{
			$user_node->set_attr($key, $value);
		}
		return $xdom->get_xml();
	}

}

?>