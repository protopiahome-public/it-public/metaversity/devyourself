<?php

class ping_api_message extends base_api_message
{

	private $test;

	public function __construct($test)
	{
		$this->test = $test;
	}

	public function get_xml()
	{
		$xdom = xdom::create("message");
		$xdom->set_attr("type", "ping");
		$xdom->set_attr("test", $this->test);
		return $xdom->get_xml();
	}

}

?>