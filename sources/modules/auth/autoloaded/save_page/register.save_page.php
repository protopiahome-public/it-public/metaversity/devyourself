<?php

class register_save_page extends base_dt_add_save_ctrl
{

	protected $dt_name = "user";
	protected $axis_name = "register";

	public function on_after_dt_init()
	{
		if (POST("cancel") !== null and $retpath = POST("retpath"))
		{
			$retpath .= strpos($retpath, "?") === false ? "?cancel=1" : "&cancel=1";
			$this->set_redirect_url($retpath);
			return true;
		}
		return true;
	}

	public function on_before_commit()
	{
		$this->update_array["register_ip"] = "'" . $this->request->get_remote_addr() . "'";
		return true;
	}

	public function on_after_commit()
	{
		$this->user->login_by_id($this->last_id);
		
		if (is_good_id(POST("project_id")))
		{
			$project_obj = project_obj::instance(POST("project_id"));
			if ($project_obj->exists())
			{
				$project_access_save = new project_access_save($project_obj, $this->last_id);
				$project_access_save->join();
				// @dm9 почему под это у нас нет метода?
				$this->db->sql("REPLACE INTO api_user_update SET project_id = {$project_obj->get_id()}, user_id = {$this->last_id}");
			}
		}
		
		if (!$retpath = POST("retpath"))
		{
			$retpath = $this->request->get_full_prefix() . "/";
		}

		$this->set_redirect_url($retpath);
	}

}

?>