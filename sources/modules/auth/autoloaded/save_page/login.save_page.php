<?php

class login_save_page extends base_save_ctrl
{

	public function set_up()
	{
		if (POST("cancel") !== null and $retpath = POST("retpath"))
		{
			$retpath .= strpos($retpath, "?") === false ? "?cancel=1" : "&cancel=1";
			$this->set_redirect_url($retpath);
			return true;
		}
		if (!POST("retpath"))
		{
			$this->set_redirect_url($this->request->get_prefix() . "/");
		}
		return true;
	}

	public function check()
	{
		if (!$this->user->login(POST("login"), POST("password")))
		{
			$this->pass_info->write_error("BAD_LOGIN_OR_PASSWORD");
			$this->pass_info->dump_vars();
			return false;
		}
		
		if ($this->domain_logic->is_on())
		{
			output_buffer::set_cookie("logged_in", 1, 1735689600, "/", "." . $this->domain_logic->get_main_host_name());
		}
		
		return true;
	}

}

?>