<?php

class settings_save_page extends base_dt_edit_save_ctrl
{

	protected $dt_name = "user";
	protected $axis_name = "settings";

	protected function fill_id()
	{
		$this->id = REQUEST("id");
		if (!is_good_id($this->id) or $this->id != $this->user->get_user_id())
		{
			$this->id = -1;
		}
	}

	public function clean_cache()
	{
		user_cache_tag::init($this->id)->update();
	}

	public function on_after_commit()
	{
		// @dm9 что это за ад? Почему все эти запросы не поместить в один класс?
		// нужно сделать поиск по api_user_update
		$this->db->sql("
			REPLACE INTO api_user_update
			SELECT upl.project_id,upl.user_id
			FROM user_project_link AS upl, project AS pr
			WHERE upl.user_id={$this->id}
				AND upl.status IN ('pretender', 'member', 'moderator', 'admin')
				AND pr.id=upl.project_id
				AND pr.endpoint_is_on>0
		");
	}

}

?>