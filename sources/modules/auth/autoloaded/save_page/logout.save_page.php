<?php

class logout_save_page extends base_save_ctrl
{

	public function commit()
	{
		$this->user->logout();
		global $session;
		/* @var $session x_session */
		output_buffer::set_cookie($session->get_session_name(), "", time() - 100000, $this->request->get_prefix() . "/");
		$this->set_redirect_url($this->request->get_full_prefix() . "/");
		
		if ($this->domain_logic->is_on())
		{
			output_buffer::set_cookie("logged_in", 0, time() - 100000, "/", "." . $this->domain_logic->get_main_host_name());
		}
		
		return true;
	}

}

?>