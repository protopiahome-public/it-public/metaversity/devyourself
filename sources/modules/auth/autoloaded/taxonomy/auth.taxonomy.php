<?php

final class auth_taxonomy extends base_taxonomy
{

	public function run()
	{
		$p = $this->get_parts_relative();

		if ($p[1] === "login" and $p[2] === null)
		{
			//login/
			if ($this->user->get_user_id())
			{
				$this->xml_loader->set_redirect_url($this->request->get_prefix() . "/");
			}
			else
			{
				$this->xml_loader->add_xml_with_xslt(new login_xml_page());
			}
		}
		elseif ($p[1] === "pass" and $p[2] === null)
		{
			//pass/
			$this->xml_loader->add_xml_with_xslt(new pass_xml_page());
		}
		elseif ($p[1] === "endpoint" and ($project_id = $this->get_project_id_by_url_name($p[2]) and $p[3] === null or $p[2] === null))
		{
			//endpoint/[<project_name>/]
			$this->xml_loader->add_xml(new endpoint_xml_page($project_id));
		}
		elseif ($p[1] === "join" and $project_id = $this->get_project_id_by_url_name($p[2]) and $p[3] === null)
		{
			//join/<project_name>/ - project join (for client auth)
			$this->xml_loader->add_xml_by_class_name("project_short_xml_ctrl", $project_id);
			$this->xml_loader->add_xml_with_xslt(new join_xml_page($project_id));
		}
		elseif ($p[1] === "reg" and $p[2] === null)
		{
			//reg/
			if ($this->user->get_user_id())
			{
				$this->xml_loader->set_redirect_url($this->request->get_prefix() . "/");
			}
			else
			{
				$this->xml_loader->add_xml_with_xslt(new register_xml_page());
			}
		}
		elseif ($p[1] === "settings" and $p[2] === null)
		{
			//settings/
			if (!$user_id = $this->user->get_user_id())
			{
				$this->xml_loader->set_redirect_url($this->request->get_prefix() . "/login/");
			}
			else
			{
				$this->xml_loader->add_xml_with_xslt(new settings_xml_page($user_id));
			}
		}
		elseif ($p[1] === "auth" and $p[2] === null)
		{
			//auth/
			$this->xml_loader->add_xml_with_xslt(new auth_xml_page());
		}
	}

	private function get_project_id_by_url_name($url_name)
	{
		return url_helper::get_project_id_by_name($url_name);
	}

}

?>