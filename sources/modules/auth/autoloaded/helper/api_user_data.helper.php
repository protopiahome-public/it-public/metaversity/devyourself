<?php

class api_user_data_helper
{

	public static function clean_user_info($user_info, $use_game_name)
	{
		global $config;
		
		$required_fields = array(
			"id", "login", "is_member", "is_admin", "last_name", "first_name", "mid_name", "phone", "email", "sex", /* "game_name", */
			/* "photo_big_url", */ "photo_big_width", "photo_big_height",
			/* "photo_mid_url", */ "photo_mid_width", "photo_mid_height",
			/* "photo_small_url", */ "photo_small_width", "photo_small_height",
			/* "profile_url", "profile_edit_url" */
		);
		$result = array();
		foreach ($required_fields as $field)
		{
			$dbg[$field] = serialize($user_info);
			if (array_key_exists ($field, $user_info))
			//if (isset($user_info[$field])) //is wrong and don't work
			{
				$result[$field] = $user_info[$field];
			}
			else
			{
				$result[$field] = "";
				trigger_error("Required field is not received: '{$field}'");
			}
		}
		
		// @ar нужно избавиться от $config["main_url"] и удалить его из конфигов
		$result["game_name"] = $use_game_name ? $user_info["game_name"] : "{$user_info["first_name"]} {$user_info["last_name"]}";
		$result["photo_big_url"] = $user_info["photo_big_width"] ? "{$config["main_url"]}data/user/big/{$user_info["id"]}.jpeg" : "";
		$result["photo_mid_url"] = $user_info["photo_mid_width"] ? "{$config["main_url"]}data/user/mid/{$user_info["id"]}.jpeg" : "";
		$result["photo_small_url"] = $user_info["photo_small_width"] ? "{$config["main_url"]}data/user/small/{$user_info["id"]}.jpeg" : "";
		$result["profile_url"] = "{$config["main_url"]}users/{$user_info["login"]}/";
		$result["profile_edit_url"] = "{$config["main_url"]}settings/";
		return $result;
	}

}

?>