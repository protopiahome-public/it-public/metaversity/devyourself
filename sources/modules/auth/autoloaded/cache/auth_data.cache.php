<?php

class auth_data_cache extends base_cache
{

	public static function init($key)
	{
		$tag_keys = array();
		$tag_keys[] = auth_data_cache_tag::init($key)->get_key();
		return parent::get_cache(__CLASS__, $key, $tag_keys);
	}

}

?>