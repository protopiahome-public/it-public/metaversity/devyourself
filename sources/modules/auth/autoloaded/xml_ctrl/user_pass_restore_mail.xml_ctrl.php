<?php

class user_pass_restore_mail_xml_ctrl extends base_xml_ctrl
{

	protected $user_id;
	protected $retpath_suffix;

	public function __construct($user_id, $retpath_suffix = false)
	{
		parent::__construct();
		$this->user_id = $user_id;
		$this->retpath_suffix = $retpath_suffix;
	}

	public function get_xml()
	{
		if (!$user_data = $this->db->get_row("SELECT * FROM user WHERE id={$this->user_id}"))
		{
			return "";
		}

		if ($this->retpath_suffix)
		{
			$user_data["retpath_suffix"] = $this->retpath_suffix;
		}

		$xml = $this->get_node_string(null, $user_data);
		return $xml;
	}

}