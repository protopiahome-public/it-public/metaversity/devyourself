<?php

class register_xml_page extends base_dt_add_xml_ctrl
{

	protected $dt_name = "user";
	protected $axis_name = "register";
	//protected $enable_blocks = true;
	
	protected function fill_xml(xdom $xdom)
	{
		if ($this->domain_logic->is_on() and ($project_id = GET("project")) and is_good_id($project_id))
		{
			$this->xml_loader->add_xml_by_class_name("project_short_xml_ctrl", $project_id);
			$xdom->set_attr("project_id", $project_id);
			
			/*$project_obj = project_obj::instance($project_id);
			if ($project_obj->use_game_name())
			{
				
			}*/
		}
		
		parent::fill_xml($xdom);
	}

}

?>