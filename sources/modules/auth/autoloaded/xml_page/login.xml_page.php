<?php

class login_xml_page extends base_xml_ctrl
{

	public function get_xml()
	{
		$attrs = array();

		if ($this->domain_logic->is_on() and ($project_id = GET("project")) and is_good_id($project_id))
		{
			$this->xml_loader->add_xml_by_class_name("project_short_xml_ctrl", $project_id);
			$attrs["project_id"] = $project_id;
		}

		return $this->get_node_string(null, $attrs);
	}

}

?>