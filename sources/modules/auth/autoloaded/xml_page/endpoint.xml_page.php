<?php

require_once PATH_CORE . "/api_client_interaction.php";

class endpoint_xml_page extends base_xml_ctrl
{

	const ENDPOINT_CONNECT_ATTEMPTS_LEFT_DEFAULT = 5;

	private $project_id;

	/**
	 * @var project_obj
	 */
	private $project_obj;

	/**
	 * @var project_access
	 */
	private $project_access;
	private $return_to_escaped;

	public function __construct($project_id = null)
	{
		$this->project_id = $project_id;

		if ($project_id)
		{
			$this->project_obj = project_obj::instance($this->project_id);
			$this->project_access = $this->project_obj->get_access();
		}

		$this->return_to_escaped = rawurlencode(GET("return_to"));

		parent::__construct();
	}

	public function get_xml()
	{
		$cancel = GET("cancel");
		$method = GET("method");

		if (!$this->project_id)
		{
			$this->do_print_default_message();
		}
		elseif (($key = GET("key")))
		{
			$this->do_key($key);
		}
		elseif (!$this->project_obj->endpoint_is_on() or !$this->project_obj->get_endpoint_url())
		{
			$this->do_print_error_message_no_setup();
		}
		elseif ($cancel)
		{
			$this->set_redirect_url($this->project_obj->get_endpoint_url() . "?method=login_return&cancel=1&return_to={$this->return_to_escaped}");
		}
		elseif ($method == "register")
		{
			$this->do_register();
		}
		elseif ($method == "login")
		{
			$this->do_login();
		}
		elseif ($method == "logout")
		{
			$this->do_logout();
		}
		elseif ($method == "head")
		{
			$this->get_head();
		}
		else
		{
			$this->do_print_default_message();
		}
		return true;
	}

	private function do_print_default_message()
	{
		response::set_content_text("Devyourself server endpoint");
	}

	private function do_print_error_message_no_setup()
	{
		response::set_content_html("В настройках проекта не указан адрес клиентского endpoint-а. <a href=\"{$this->project_obj->get_url()}admin/integration/endpoint/\">Указать</a>.");
	}

	private function do_key($key)
	{
		/* Запрос данных по ключу. Если такие есть - их надо отдать и стереть. Все. */
		$cache = endpoint_cache::init($key);
		if (!$object = $cache->get())
		{
			response::set_content_xml('<?xml version="1.0" encoding="UTF-8"?><message type="error" text="BAD_KEY"/>');
			return;
		}
		response::set_content_xml($object->get_xml());
	}

	private function do_register()
	{
		if (!$user_id = $this->user->get_user_id())
		{
			// Регистрация нового пользователя
			$retpath_escaped = urlencode($this->request->get_full_request_uri());
			$this->set_redirect_url($this->request->get_prefix() . "/reg/?retpath={$retpath_escaped}&project={$this->project_id}&can_cancel=1");
			return;
		}
		$this->join_and_return($user_id);
	}

	private function do_login()
	{
		if (!$user_id = $this->user->get_user_id())
		{
			// Вход не выполнен
			$retpath_escaped = urlencode($this->request->get_full_request_uri());
			$this->set_redirect_url($this->request->get_prefix() . "/login/?retpath={$retpath_escaped}&project={$this->project_id}&can_cancel=1");
			return;
		}
		$this->join_and_return($user_id);
	}

	private function do_logout()
	{
		$this->user->logout();
		$this->set_redirect_url($this->project_obj->get_endpoint_url() . "?method=logout_return&return_to={$this->return_to_escaped}");
	}

	private function join_and_return($user_id)
	{
		if (!($this->project_access->is_member() or $this->project_access->is_pretender_strict()))
		{
			// Пользователь не участвует в проекте
			$retpath_escaped = urlencode($this->request->get_full_request_uri());
			$this->set_redirect_url($this->request->get_prefix() . "/join/{$this->project_obj->get_name()}/?retpath={$retpath_escaped}");
			return;
		}

		$user_info = $this->user->get_user_data();
		$user_info2 = $this->db->get_row("
			SELECT
				game_name,
				status IN ('member', 'moderator', 'admin') AS is_member,
				status IN ('admin') AS is_admin
			FROM user_project_link
			WHERE user_id = '{$user_id}' AND project_id = '{$this->project_id}'
		");
		$user_info["game_name"] = $user_info2["game_name"];
		$user_info["is_member"] = $user_info2["is_member"] ? 1 : 0;
		$user_info["is_admin"] = $user_info["is_admin"] ? 1 : ($user_info2["is_admin"] ? 1 : 0);

		$api_client_interaction = new api_client_interaction(new login_api_message($user_id, $user_info, $this->project_obj->use_game_name()));
		$key = $api_client_interaction->get_key();
		$this->set_redirect_url($this->project_obj->get_endpoint_url() . "?method=login_return&key={$key}&return_to={$this->return_to_escaped}");
		$this->db->sql("UPDATE project SET endpoint_connect_attempts_left = " . self::ENDPOINT_CONNECT_ATTEMPTS_LEFT_DEFAULT . " WHERE id={$this->project_id}");
	}
	
	private function get_head()
	{
		$html = '<style type="text/css">' . "\n";
		$write = false;
		foreach (file(PATH_WWW . "/css/main.css") as $line)
		{
			if (trim($line) == "/* ENDPOINT_PROJECT_HEAD_BEGIN */")
			{
				$write = true;
			}
			if ($write)
			{
				$html .= $line;
			}
			if (trim($line) == "/* ENDPOINT_PROJECT_HEAD_END */")
			{
				break;
			}
		}
		$html .= '</style>' . "\n";
		$html .= '<script type="text/javascript" src="' . $this->domain_logic->get_main_prefix() . '/js/head.js"></script>' . "\n";
		$html .= $this->get_head_html();
		$html .= '<script type="text/javascript">head_init();</script>' . "\n";
		response::set_content_html($html);
	}
	
	private function get_head_html()
	{
		$xml_loader = new xml_loader();
		//$xml_loader->add_xml(new comments_xml_ctrl($this->type, $this->object_id, $this->project_id, $comment_id));
		$xml_loader->add_xml(new user_xml_ctrl());
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xml(new sys_params_xml_ctrl());
		$xml_loader->add_xml(new project_short_xml_ctrl($this->project_id));
		$xml_loader->add_xml(new project_menu_xml_ctrl($this->project_id));
		$xml_loader->add_xslt("ajax/endpoint_head.ajax", "auth");
		$xml_loader->run();
		return $xml_loader->get_content();
	}

}

?>