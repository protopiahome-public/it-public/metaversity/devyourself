<?php

class join_xml_page extends base_xml_ctrl
{

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct();
	}

	public function get_xml()
	{

		if (!$user_id = $this->user->get_user_id())
		{
			// guest --> to prefix . "/login/" + can_cancel
			$retpath_escaped = rawurlencode($this->request->get_full_request_uri());
			$this->set_redirect_url($this->request->get_prefix() . "/login/?retpath={$retpath_escaped}&can_cancel=1");
			return true;
		}

		// if no retpath, retpath = project page
		$retpath = ($maybe_retpath = GET("retpath")) ? $maybe_retpath : $this->project_obj->get_url();

		if (GET("cancel"))
		{
			// "cancel" pressed, may be @login
			$this->set_redirect_url($retpath);
			return true;
		}

		if ($this->project_access->is_member())
		{
			// already joined
			$this->set_redirect_url($retpath);
			return true;
		}

		return "<join/>";
	}

}

?>