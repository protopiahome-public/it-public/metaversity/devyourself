<?php

// @dm9 CR all
// @dm9 what if domains are off?
/*
 * auth endpoint for cross-domain login/logout/reg
 * ?do = login|logout|reg
 * &retpath = http://{project_domain}/
 */
class auth_xml_page extends base_xml_ctrl
{

	public function get_xml()
	{
		$do = GET('do');
		$user_id = $this->user->get_user_id();
		$retpath_escaped = urlencode($this->request->get_full_request_uri());

		// @dm9 bad code
		$project_id = 0;
		if ($this->domain_logic->is_on() and ($retpath = GET("retpath")))
		{
			$host_name = parse_url($retpath, PHP_URL_HOST);
			$project_id = $this->domain_logic->get_project_id_by_host_name($host_name);
			$project_id = (int) $project_id ? : 0;
		}

		// dispatch action
		if ($do == "login" and !$user_id and !GET("cancel"))
		{
			$this->set_redirect_url($this->request->get_prefix() . "/login/?retpath={$retpath_escaped}" . ($project_id ? "&project={$project_id}" : "") . "&can_cancel=1");
			return;
		}
		elseif ($do == "reg" and !$user_id and !GET("cancel"))
		{
			$this->set_redirect_url($this->request->get_prefix() . "/reg/?retpath={$retpath_escaped}" . ($project_id ? "&project={$project_id}" : "") . "&can_cancel=1");
			return;
		}
		elseif ($do == "logout" and $user_id)
		{
			$this->user->logout();
		}

		// check valid retpath
		if (!$project_id)
		{
			$this->set_redirect_url($this->request->get_prefix() . "/");
			return;
		}

		// @dm9 logic in controls: move to auth.php?
		// all done, transfer user_id
		// http://cmp5.lan/auth/?retpath=http://futuromicon.cmp5.lan/
		$key = md5(uniqid() . "-" . mt_rand());
		$auth_data = array("user_id" => $this->user->get_user_id());
		auth_data_cache::init($key)->set($auth_data);
		// @dm9 CR
		// @dm9 protocol
		$this->set_redirect_url("http://{$host_name}/auth/?key={$key}&retpath=" . urlencode($retpath));

		// Removing "logged_in" cookie, if no user is logged in
		// @dm9 move these cookies processing into some file
		if (!$this->user->get_user_id())
		{
			// Override incorrect "logged_in" cookie
			output_buffer::set_cookie("logged_in", 0, time() - 100000, "/", "." . $this->domain_logic->get_main_host_name());
		}
	}

}

?>