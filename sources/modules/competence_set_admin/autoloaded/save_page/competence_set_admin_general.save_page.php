<?php

class competence_set_admin_general_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"competence_set_edit_before_start",
	);
	// Settings
	protected $dt_name = "competence_set";
	protected $axis_name = "edit";
	// Internal
	protected $competence_set_id;

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;

	/**
	 * @var competence_set_access
	 */
	protected $competence_set_access;

	public function check_rights()
	{
		$this->competence_set_access = $this->competence_set_obj->get_access();
		return $this->competence_set_access->has_admin_rights();
	}

	public function clean_cache()
	{
		competence_set_cache_tag::init($this->id)->update();
	}

}

?>