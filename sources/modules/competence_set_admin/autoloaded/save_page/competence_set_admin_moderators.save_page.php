<?php

class competence_set_admin_moderators_save_page extends base_admin_moderators_save_ctrl
{

	protected $mixins = array(
		"competence_set_before_start",
	);
	// Settings
	// Internal
	protected $competence_set_id;

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;
	
	/**
	 * @var competence_set_access
	 */
	protected $competence_set_access;

	public function check_rights()
	{
		$this->competence_set_access = $this->competence_set_obj->get_access();
		return $this->competence_set_access->has_admin_rights();
	}

	protected function get_access_save($user_id)
	{
		return new competence_set_access_save($this->competence_set_obj, $user_id);
	}

	protected function get_rights_array()
	{
		return array("is_professiogram_moderator");
	}

}

?>