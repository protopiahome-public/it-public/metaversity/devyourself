<?php

final class competence_set_admin_taxonomy extends base_taxonomy
{

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, competence_set_obj $competence_set_obj)
	{
		$this->competence_set_obj = $competence_set_obj;
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		$p = $this->get_parts_relative();

		$competence_set_obj = $this->competence_set_obj;
		$competence_set_id = $competence_set_obj->get_id();
		$competence_set_access = $competence_set_obj->get_access();

		if (!$competence_set_access->has_admin_rights())
		{
			$this->xml_loader->set_error_403();
			$this->xml_loader->set_error_403_xslt("competence_set_admin_403", "competence_set_admin");
		}
		elseif ($p[1] === null)
		{
			//sets/set-<id>/admin/
			$this->xml_loader->add_xml_with_xslt(new competence_set_admin_general_xml_page($competence_set_id));
		}
		elseif ($p[1] == "tree" and $p[2] === null)
		{
			//sets/set-<id>/admin/tree/
			$this->xml_loader->add_xml_with_xslt(new competence_set_admin_tree_xml_page($competence_set_id));
		}
		elseif ($p[1] == "admins" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//sets/set-<id>/admin/admins/
			$this->xml_loader->add_xml_with_xslt(new competence_set_admin_admins_xml_page($competence_set_id, $page));
		}
		elseif ($p[1] == "moderators" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//sets/set-<id>/admin/moderators/
			$this->xml_loader->add_xml_with_xslt(new competence_set_admin_moderators_xml_page($competence_set_id, $page));
		}
	}

}

?>