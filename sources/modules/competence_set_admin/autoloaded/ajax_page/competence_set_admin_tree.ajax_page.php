<?php

class competence_set_admin_tree_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"competence_set_before_start",
	);
	protected $competence_set_id;

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;

	/**
	 *
	 * @var competence_set_access 
	 */
	protected $competence_set_access;
	protected $autostart_db_transaction = false;

	public function init()
	{
		$this->db->begin();
	}

	public function check_rights()
	{
		$this->competence_set_access = $this->competence_set_obj->get_access();
		return $this->competence_set_access->has_admin_rights();
	}

	public function get_data()
	{
		$multi_link_exists = $this->db->row_exists("
			SELECT c.id, c.competence_set_id, count(l.competence_group_id) as link_count
			FROM competence_full c
			LEFT JOIN competence_link l ON (c.id = l.competence_id)
			WHERE c.competence_set_id = {$this->competence_set_id}
			GROUP BY c.id
			HAVING link_count > 1
		");
		if ($multi_link_exists)
		{
			return false;
		}

		$operation = REQUEST("operation");
		$result = false;

		switch ($operation)
		{
			case "get_nodes":
				$result = $this->get_nodes();
				break;
			case "rename_item":
				$result = $this->rename_item();
				break;
			case "rename_group":
				$result = $this->rename_group();
				break;
			case "delete_item":
				$result = $this->delete_item();
				break;
			case "delete_group":
				$result = $this->delete_group();
				break;
			case "move_item":
				$result = $this->move_item();
				break;
			case "move_group":
				$result = $this->move_group();
				break;
			case "create_group":
				$result = $this->create_group();
				break;
			case "create_item":
				$result = $this->create_item();
				break;
		}

		if ($result)
		{
			if ($operation != "get_nodes")
			{
				$this->db->sql("
					UPDATE competence_set
					SET edit_time = NOW()
					WHERE id = {$this->competence_set_id}
				");
			}
			$this->db->commit();
			competence_set_competences_cache_tag::init($this->competence_set_id)->update();
		}
		else
		{
			$this->db->rollback();
		}

		if (!$result)
		{
			$result = array(
				"status" => "ERROR"
			);
		}

		return $result;
	}

	public function get_nodes()
	{
		$data_raw_plain = $this->db->fetch_all("
			SELECT id, parent_id, title, position
			FROM competence_group
			WHERE competence_set_id = {$this->competence_set_id}
			ORDER BY position, id
		");
		$data_raw = array();
		foreach ($data_raw_plain as $row)
		{
			$data_raw[$row["id"]] = array(
				"data" => $row["title"],
				"attr" => array(
					"id" => "group_" . $row["id"],
					"rel" => "group"
				)
			);
		}
		foreach ($data_raw_plain as $row)
		{
			if ($row["parent_id"])
			{
				if (!isset($data_raw[$row["parent_id"]]["children"]))
				{
					$data_raw[$row["parent_id"]]["children"] = array();
				}
				$data_raw[$row["parent_id"]]["children"][] = &$data_raw[$row["id"]];
			}
		}
		$data = array();
		foreach ($data_raw_plain as $row)
		{
			if (!$row["parent_id"])
			{
				$data[] = &$data_raw[$row["id"]];
			}
		}
		$competence_group_ids = array_keys($data_raw);
		if ($competence_group_ids)
		{
			$competences_data = $this->db->fetch_all("
				SELECT c.competence_id as id, c.title, l.competence_group_id as parent_id
				FROM competence_calc c
				INNER JOIN competence_link l ON l.competence_id = c.competence_id
				WHERE
					l.competence_group_id IN (" . join(",", $competence_group_ids) . ")
				ORDER BY l.position, c.competence_id
			");
			foreach ($competences_data as $competence_row)
			{
				if (isset($data_raw[$competence_row["parent_id"]]))
				{
					if (!isset($data_raw[$competence_row["parent_id"]]["children"]))
					{
						$data_raw[$competence_row["parent_id"]]["children"] = array();
					}
					$data_raw[$competence_row["parent_id"]]["children"][] = array(
						"data" => $competence_row["title"],
						"attr" => array(
							"id" => "competence_" . $competence_row["id"],
							"rel" => "competence"
						)
					);
				}
			}
		}
		return array(
			"data" => "Набор компетенций",
			"state" => "open",
			"attr" => array(
				"id" => "root_" . 0,
				"rel" => "root"
			),
			"children" => $data,
		);
	}

	protected function create_item()
	{
		$group_id = POST("group_id");
		$title = POST("title");
		$title = $this->process_title($title);
		$title_quoted = $this->db->escape($title);

		if (!is_good_id($group_id))
		{
			return false;
		}
		if (!$this->group_exists($group_id))
		{
			return false;
		}
		if ($this->child_groups_exist($group_id))
		{
			return false;
		}

		$this->db->sql("
			INSERT INTO competence_full (title, competence_set_id, add_time, edit_time)
			VALUES ('{$title_quoted}', {$this->competence_set_id}, NOW(), NOW())
		");
		$id = $this->db->get_last_id();

		$this->db->sql("
			INSERT INTO competence_calc (title, competence_id, competence_set_id)
			VALUES ('{$title_quoted}', $id, {$this->competence_set_id})
		");

		$position = $this->db->get_value("
			SELECT IF(count(position), max(position) + 1, 0)
			FROM competence_link
			WHERE competence_group_id = {$group_id}
		");
		$this->db->sql("
			INSERT INTO competence_link (competence_id, competence_group_id, position)
			VALUES ({$id}, {$group_id}, {$position})
		");

		$this->recalc_competence_count();

		return array(
			"status" => "OK",
			"id" => $id,
			"title" => $title
		);
	}

	protected function create_group()
	{
		$parent_id = POST("group_id");
		$title = $this->process_title(POST("title"));
		$title_quoted = $this->db->escape($title);

		if (!is_good_num($parent_id))
		{
			return false;
		}
		if ($parent_id)
		{
			if (!$this->group_exists($parent_id))
			{
				return false;
			}
		}

		if ($this->child_competences_exist($parent_id))
		{
			return false;
		}

		$eq = $parent_id ? "=" : "IS";
		if ($parent_id == 0)
		{
			$parent_id = "NULL";
		}

		$position = $this->db->get_value("
			SELECT IF(count(position), max(position)+1, 0)  
			FROM competence_group
			WHERE parent_id {$eq} {$parent_id} AND competence_set_id = {$this->competence_set_id}
		");
		$this->db->sql("
			INSERT INTO competence_group (title, competence_set_id, parent_id, add_time, edit_time, position)
			VALUES ('{$title_quoted}', {$this->competence_set_id}, {$parent_id}, NOW(), NOW(), {$position})
		");
		$id = $this->db->get_last_id();

		return array(
			"status" => "OK",
			"id" => $id,
			"title" => $title
		);
	}

	protected function move_item()
	{
		$id = POST("id");
		$group_id = POST("group_id");
		$old_group_id = POST("old_group_id");
		$position = POST("position");

		if (!is_good_id($id) or !is_good_id($group_id) or !is_good_id($old_group_id) or !is_good_num($position))
		{
			return false;
		}
		if (!$this->item_exists($id))
		{
			return false;
		}
		if (!$this->group_exists($old_group_id))
		{
			return false;
		}
		if (!$this->group_exists($group_id))
		{
			return false;
		}
		if ($this->child_groups_exist($group_id))
		{
			return false;
		}
		$this->db->sql("
			UPDATE competence_link l
			JOIN competence_calc c ON c.competence_id = l.competence_id
			SET l.competence_group_id = {$group_id}
			WHERE
				l.competence_id = {$id}
				AND l.competence_group_id = {$old_group_id}
				AND c.competence_set_id = {$this->competence_set_id}
		");

		$competence_positions = $this->db->fetch_all("
			SELECT l.competence_id, l.position
			FROM competence_link l
			INNER JOIN competence_calc c ON c.competence_id = l.competence_id
			WHERE
				l.competence_group_id = {$group_id}
				AND l.competence_id <> {$id}
				AND c.competence_set_id = {$this->competence_set_id}
			ORDER BY l.position, c.competence_id
		");
		array_splice($competence_positions, $position, 0, array(
			array(
				"competence_id" => $id
			)
		));
		foreach ($competence_positions as $item_position => $item)
		{
			$this->db->sql("
				UPDATE competence_link 
				SET position = {$item_position}
				WHERE competence_id = {$item["competence_id"]} 
			");
		}

		return array(
			"status" => "OK"
		);
	}

	protected function move_group()
	{
		$id = POST("id");
		$parent_id = POST("group_id");
		$position = POST("position");

		if (!is_good_id($id) or !is_good_num($parent_id) or !is_good_num($position))
		{
			return false;
		}
		if (!$this->group_exists($id))
		{
			return false;
		}
		if ($parent_id)
		{
			if (!$this->group_exists($parent_id))
			{
				return false;
			}

			$ancestor_id = $parent_id;
			do
			{
				if ($ancestor_id == $id)
				{
					return array("status" => "ERROR", "error" => "recursive_move");
				}
			}
			while ($ancestor_id = $this->db->get_value("SELECT parent_id FROM competence_group WHERE id = {$ancestor_id}"));
		}
		if ($this->child_competences_exist($parent_id))
		{
			return false;
		}

		$eq = $parent_id ? "=" : "IS";
		if ($parent_id == 0)
		{
			$parent_id = "NULL";
		}

		$this->db->sql("
			UPDATE competence_group
			SET parent_id = {$parent_id}
			WHERE id = {$id} AND competence_set_id = {$this->competence_set_id}
		");

		$group_positions = $this->db->fetch_all("
			SELECT id, position 
			FROM competence_group 
			WHERE parent_id {$eq} {$parent_id} AND id <> {$id} AND competence_set_id = {$this->competence_set_id}
			ORDER BY position, id
		");
		array_splice($group_positions, $position, 0, array(
			array(
				"id" => $id
			)
		));
		foreach ($group_positions as $item_position => $item)
		{
			$this->db->sql("
				UPDATE competence_group 
				SET position = {$item_position}
				WHERE id = {$item["id"]} 
			");
		}

		return array(
			"status" => "OK"
		);
	}

	protected function rename_item()
	{
		$id = POST("id");
		$title = POST("title");

		if (!is_good_id($id))
		{
			return false;
		}
		if (!$this->item_exists($id))
		{
			return false;
		}

		$title = $this->process_title($title);

		$title_quoted = $this->db->escape($title);

		$this->db->sql("
			UPDATE competence_full
			SET 
				title = '{$title_quoted}',
				edit_time = NOW()
			WHERE id = {$id}
				AND competence_set_id = {$this->competence_set_id}
		");

		$this->db->sql("
			UPDATE competence_calc
			SET 
				title = '{$title_quoted}'
			WHERE competence_id = {$id}
				AND competence_set_id = {$this->competence_set_id}
		");

		return array(
			"status" => "OK",
			"title" => $title
		);
	}

	protected function rename_group()
	{
		$id = POST("id");
		$title = POST("title");

		if (!is_good_id($id))
		{
			return false;
		}

		if (!$this->group_exists($id))
		{
			return false;
		}

		$title = $this->process_title($title);

		$title_quoted = $this->db->escape($title);

		$this->db->sql("
			UPDATE competence_group
			SET 
				title = '{$title_quoted}',
				edit_time = NOW()
			WHERE id = {$id}
		");
		return array(
			"status" => "OK",
			"title" => $title
		);
	}

	protected function delete_item()
	{
		$id = POST("id");

		if (!is_good_id($id))
		{
			return false;
		}
		if (!$this->item_exists($id))
		{
			return false;
		}

		$this->db->sql("
			UPDATE competence_full
			SET is_deleted = 1 
			WHERE id = {$id} AND competence_set_id = {$this->competence_set_id}
		");

		$this->db->sql("
			DELETE FROM competence_calc
			WHERE competence_id = {$id} AND competence_set_id = {$this->competence_set_id}
		");

		$this->delete_competences_from_rates();
		$this->recalc_competence_count();

		return array(
			"status" => "OK"
		);
	}

	protected function delete_group($id = 0, $recursive = false)
	{
		if (!$recursive)
		{
			$id = POST("id");
		}

		if (!is_good_id($id))
		{
			return false;
		}

		if (!$this->group_exists($id))
		{
			return false;
		}

		$group_dbres = $this->db->sql("
			SELECT id
			FROM competence_group
			WHERE parent_id = {$id}
			FOR UPDATE
		");

		while ($group_row = $this->db->get_row($group_dbres))
		{
			$this->delete_group($group_row["id"], true);
		}

		$this->db->sql("
			DELETE FROM competence_group
			WHERE id = {$id} AND competence_set_id = {$this->competence_set_id}
		");

		if (!$recursive)
		{
			$this->db->sql("
				UPDATE competence_full f
				LEFT JOIN competence_link l ON f.id = l.competence_id
				SET f.is_deleted = 1
				WHERE l.competence_group_id IS NULL
			");

			$this->db->sql("
				DELETE competence_calc.*
				FROM competence_calc
				LEFT JOIN competence_link l ON competence_calc.competence_id = l.competence_id
				WHERE l.competence_group_id IS NULL
			");

			$this->delete_competences_from_rates();
			$this->recalc_competence_count();
		}

		return array(
			"status" => "OK"
		);
	}

	protected function recalc_competence_count()
	{
		$this->db->sql("
			UPDATE competence_set
			SET competence_count_calc = (
				SELECT count(*)
				FROM competence_full
				WHERE is_deleted = 0 AND competence_set_id = {$this->competence_set_id}
			)
			WHERE id = {$this->competence_set_id}
		");

		$this->db->sql("
			UPDATE `project_role_rate_link` rl
			SET `competence_count_calc` = (
				SELECT COUNT(*) 
				FROM rate_competence_link rcl
				LEFT JOIN competence_full c ON (c.id = rcl.competence_id)
				WHERE rcl.rate_id = rl.rate_id AND c.is_deleted = 0 AND c.competence_set_id = {$this->competence_set_id}
			)
			WHERE rl.competence_set_id = {$this->competence_set_id}
		");
		$this->db->sql("
			UPDATE `event_rate_link` rl
			SET `competence_count_calc` = (
				SELECT COUNT(*) 
				FROM rate_competence_link rcl 
				LEFT JOIN competence_full c ON (c.id = rcl.competence_id)
				WHERE rcl.rate_id = rl.rate_id AND c.is_deleted = 0 AND c.competence_set_id = {$this->competence_set_id}
			)
			WHERE rl.competence_set_id = {$this->competence_set_id}
		");
		$this->db->sql("
			UPDATE `material_rate_link` rl
			SET `competence_count_calc` = (
				SELECT COUNT(*) 
				FROM rate_competence_link rcl 
				LEFT JOIN competence_full c ON (c.id = rcl.competence_id)
				WHERE rcl.rate_id = rl.rate_id AND c.is_deleted = 0 AND c.competence_set_id = {$this->competence_set_id}
			)
			WHERE rl.competence_set_id = {$this->competence_set_id}
		");
		$this->db->sql("
			UPDATE `professiogram` p
			SET `competence_count_calc` = (
				SELECT COUNT(*) 
				FROM rate_competence_link rcl 
				LEFT JOIN competence_full c ON (c.id = rcl.competence_id)
				WHERE rcl.rate_id = p.rate_id AND c.is_deleted = 0 AND c.competence_set_id = {$this->competence_set_id}
			)
			WHERE p.competence_set_id = {$this->competence_set_id}
		");
	}

	protected function delete_competences_from_rates()
	{
		$this->db->sql("
			DELETE l
			FROM rate_competence_link l
			LEFT JOIN competence_full c ON (c.id = l.competence_id)
			WHERE c.is_deleted = 1 AND c.competence_set_id = {$this->competence_set_id}
		");
	}

	protected function process_title($title)
	{
		$title = trim(preg_replace("/[\\x00-\\x20]+/", " ", $title));
		if ($title === "")
		{
			$title = "Unnamed";
		}
		return $title;
	}

	protected function group_exists($id)
	{
		return $this->db->row_exists("
			SELECT *
			FROM competence_group
			WHERE id = {$id} AND competence_set_id = {$this->competence_set_id}
		");
	}

	protected function item_exists($id)
	{
		return $this->db->row_exists("
			SELECT *
			FROM competence_full
			WHERE id = {$id} AND competence_set_id = {$this->competence_set_id}
				AND is_deleted = 0
		");
	}

	protected function child_competences_exist($parent_id)
	{
		if ($parent_id)
		{
			$competences_exist = $this->db->row_exists("
				SELECT f.id
				FROM competence_full f
				LEFT JOIN competence_link l ON (f.id = l.competence_id)
				WHERE f.is_deleted = 0 AND l.competence_group_id = {$parent_id}
				LIMIT 1
			");

			if ($competences_exist)
			{
				return true;
			}
		}

		return false;
	}

	protected function child_groups_exist($parent_id)
	{
		return $this->db->row_exists("
			SELECT id 
			FROM competence_group 
			WHERE parent_id = {$parent_id}
			LIMIT 1
		");
	}

}

?>