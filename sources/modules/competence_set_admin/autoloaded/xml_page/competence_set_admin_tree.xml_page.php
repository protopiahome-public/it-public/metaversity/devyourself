<?php

class competence_set_admin_tree_xml_page extends base_xml_ctrl
{

	private $competence_set_id;

	public function __construct($competence_set_id)
	{
		$this->competence_set_id = $competence_set_id;
		parent::__construct();
	}

	public function get_xml()
	{
		$node_data = array(
			"competence_set_id" => $this->competence_set_id
		);

		$multi_link_exists = $this->db->row_exists("
			SELECT c.id, c.competence_set_id, count(l.competence_group_id) as link_count
			FROM competence_full c
			LEFT JOIN competence_link l ON (c.id = l.competence_id)
			WHERE c.competence_set_id = {$this->competence_set_id}
			GROUP BY c.id
			HAVING link_count > 1
			ORDER BY link_count DESC
		");
		if ($multi_link_exists)
		{
			$node_data["multi_link"] = "1";
		}

		return $this->get_node_string($this->name, $node_data);
	}

}

?>