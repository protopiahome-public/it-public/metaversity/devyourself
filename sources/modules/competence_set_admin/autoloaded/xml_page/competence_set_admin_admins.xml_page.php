<?php

class competence_set_admin_admins_xml_page extends base_easy_xml_ctrl
{

	protected $dependencies_settings = array(
		array(
			"column" => "user_id",
			"ctrl" => "user_short",
		),
	);
	protected $competence_set_id;
	protected $xml_row_name = "admin";
	protected $page;

	public function __construct($competence_set_id, $page)
	{
		$this->competence_set_id = $competence_set_id;
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 25));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("csa.*");
		$select_sql->add_from("competence_set_admin csa");
		$select_sql->add_where("competence_set_id = {$this->competence_set_id} AND is_admin = 1");
		$select_sql->add_order("add_time DESC");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}