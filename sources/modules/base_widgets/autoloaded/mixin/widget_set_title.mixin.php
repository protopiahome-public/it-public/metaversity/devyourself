<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class widget_set_title_mixin extends base_mixin
{

	protected $widget_container_id;
	protected $widget_container_type;
	protected $widget_id;
	protected $title;

	public function start()
	{
		if (!isset($_POST["title"]))
		{
			return false;
		}

		return true;
	}

	public function commit()
	{
		$this->title = $_POST["title"];
		$this->title = text_processor::clean_string($this->title);
		$title_quoted = $this->db->escape($this->title);

		$this->db->sql("
			UPDATE {$this->widget_container_type}_widget
			SET title = '{$title_quoted}'
			WHERE id = {$this->widget_id} AND {$this->widget_container_type}_id = {$this->widget_container_id}
		");

		return true;
	}

	public function get_data()
	{
		return array("status" => "OK", "title" => $this->title);
	}

}

?>