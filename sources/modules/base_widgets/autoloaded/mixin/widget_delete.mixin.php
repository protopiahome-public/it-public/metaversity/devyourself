<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class widget_delete_mixin extends base_mixin
{

	protected $widget_container_id;
	protected $widget_container_type;
	protected $widget_id;
	protected $widget_row;

	public function commit()
	{
		if ($this->widget_row)
		{
			$this->db->sql("
				DELETE FROM {$this->widget_container_type}_widget
				WHERE id = {$this->widget_id} AND {$this->widget_container_type}_id = {$this->widget_container_id}
			");
		}

		return true;
	}

	public function get_data()
	{
		return array("status" => "OK");
	}

}

?>