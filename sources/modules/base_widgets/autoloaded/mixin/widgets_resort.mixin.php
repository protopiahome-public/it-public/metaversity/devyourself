<?php

class widgets_resort_mixin extends base_mixin
{

	protected $widget_container_id;
	protected $widget_container_type;
	protected $new_widgets_for_load;
	protected $widgets;
	protected $new_widgets;
	protected $column;

	public function start()
	{
		if (!isset($_POST["column"]) || !in_array($this->column = $_POST["column"], array("1", "2", "3")))
		{
			return false;
		}
		if (!isset($_POST["widgets"]) || !is_string($this->widgets = $_POST["widgets"]))
		{
			return false;
		}
		if (!isset($_POST["new_widgets"]) || !is_string($this->new_widgets = $_POST["new_widgets"]))
		{
			return false;
		}
		$this->widgets = explode(",", $this->widgets);
		$this->new_widgets = explode(",", $this->new_widgets);
		return true;
	}

	public function commit()
	{
		$this->new_widgets_for_load = array();

		$old_widgets = $this->db->fetch_all("
			SELECT id, `column`, position
			FROM {$this->widget_container_type}_widget
			WHERE {$this->widget_container_type}_id = {$this->widget_container_id}
			ORDER BY position
			FOR UPDATE
		", "id");
		$position = 1;
		foreach ($this->widgets as $widget_id)
		{
			if (isset($old_widgets[$widget_id]))
			{
				$this->db->sql("
					UPDATE {$this->widget_container_type}_widget
					SET 
						`column` = {$this->column},
						position = {$position}
					WHERE id = {$widget_id} AND {$this->widget_container_type}_id = {$this->widget_container_id}
				");
				$old_widgets[$widget_id]["processed"] = true;
				$position++;
				if (in_array($widget_id, $this->new_widgets))
				{
					$this->new_widgets_for_load[] = $widget_id;
				}
			}
		}
		foreach ($old_widgets as $widget_id => $widget_data)
		{
			if ($widget_data["column"] == $this->column and !isset($widget_data["processed"]))
			{
				$this->db->sql("
					UPDATE {$this->widget_container_type}_widget
					SET 
						position = {$position}
					WHERE id = {$widget_id} AND {$this->widget_container_type}_id = {$this->widget_container_id}
				");
				$position++;
			}
		}

		return true;
	}

}

?>