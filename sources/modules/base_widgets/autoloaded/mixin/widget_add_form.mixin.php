<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class widget_add_form_mixin extends base_mixin
{

	protected $widget_container_id;
	protected $widget_container_type;
	protected $column;

	public function start()
	{
		$this->column = isset($_POST["column"]) ? $_POST["column"] : 0;
		if (!in_array($this->column, array("1", "2", "3")))
		{
			return false;
		}

		return true;
	}

}

?>