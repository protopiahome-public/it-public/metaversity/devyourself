<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class widget_add_mixin extends base_mixin
{

	protected $widget_container_id;
	protected $widget_container_type;
	protected $type_id;
	protected $column;
	protected $last_id;
	protected $widget_type_row;

	public function start()
	{
		if (!isset($_POST["type_id"]))
		{
			return false;
		}
		if (!isset($_POST["column"]))
		{
			return false;
		}

		$this->type_id = $_POST["type_id"];
		$type_id_quoted = $this->db->escape($this->type_id);
		$this->column = $_POST["column"];
		if (!in_array($this->column, array("1", "2", "3")))
		{
			return false;
		}

		$this->widget_type_row = $this->db->get_row("
			SELECT *
			FROM {$this->widget_container_type}_widget_type
			WHERE type_id = '{$type_id_quoted}'
			LOCK IN SHARE MODE
		");

		if (!$this->widget_type_row)
		{
			return false;
		}

		if (!$this->widget_type_row["multiple_instances"])
		{
			$widget_type_exists = $this->db->row_exists("
				SELECT id 
				FROM {$this->widget_container_type}_widget
				WHERE {$this->widget_container_type}_id = {$this->widget_container_id} AND type_id = '{$type_id_quoted}'
			");

			if ($widget_type_exists)
			{
				$this->db->sql("
					DELETE
					FROM {$this->widget_container_type}_widget
					WHERE {$this->widget_container_type}_id = {$this->widget_container_id} AND type_id = '{$type_id_quoted}'
				");
			}
		}

		return true;
	}

	public function commit()
	{
		$this->db->sql("
			UPDATE {$this->widget_container_type}_widget
			SET position = position + 1
			WHERE
				{$this->widget_container_type}_id = {$this->widget_container_id}
				AND `column` = {$this->column}
		");
		$title_quoted = $this->db->escape($this->widget_type_row["title"]);
		$this->db->sql("
			INSERT INTO {$this->widget_container_type}_widget
			SET
				{$this->widget_container_type}_id = {$this->widget_container_id},
				`column` = {$this->column},
				position = 1,
				title = '{$title_quoted}',
				type_id = '{$this->type_id}',
				lister_item_count = {$this->widget_type_row["lister_item_count"]}
		");
		$this->last_id = $this->db->get_last_id();

		return true;
	}

}

?>