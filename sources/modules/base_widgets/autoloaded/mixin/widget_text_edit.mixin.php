<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class widget_text_edit_mixin extends base_widget_edit_mixin
{

	protected $html;

	public function start()
	{
		if (!isset($_POST["html"]))
		{
			return false;
		}

		return true;
	}

	public function commit()
	{

		$this->html = "";
		$html_quoted = "";

		if (trim($_POST["html"]))
		{
			$this->html = text_processor::tidy($_POST["html"]);
			$html_quoted = $this->db->escape($this->html);
		}
		$this->db->sql("
			INSERT INTO {$this->widget_container_type}_widget_text (widget_id, html)
			VALUES ({$this->widget_id}, '{$html_quoted}')
			ON DUPLICATE KEY UPDATE
				html = '{$html_quoted}'
		");

		return true;
	}

}

?>