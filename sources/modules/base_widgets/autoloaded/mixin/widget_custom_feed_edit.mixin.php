<?php

class widget_custom_feed_edit_mixin extends base_widget_edit_mixin
{

	const MAX_ITEM_COUNT = 50;

	protected $selected_feed_id;
	protected $count;

	public function start()
	{
		$this->count = POST("count");
		if (!is_good_id($this->count))
		{
			return false;
		}
		if ($this->count > self::MAX_ITEM_COUNT)
		{
			return false;
		}
		if (!is_good_id($this->selected_feed_id = POST("selected_feed_id")))
		{
			return false;
		}
		$feed_exists = $this->db->row_exists("
			SELECT id
			FROM {$this->widget_container_type}_custom_feed
			WHERE {$this->widget_container_type}_id = {$this->widget_container_id} AND id = {$this->selected_feed_id}
			LOCK IN SHARE MODE
		");
		if (!$feed_exists)
		{
			return false;
		}

		return true;
	}

	public function commit()
	{
		$this->db->sql("
			INSERT INTO {$this->widget_container_type}_widget_custom_feed (widget_id, {$this->widget_container_type}_custom_feed_id)
			VALUES ({$this->widget_id}, '{$this->selected_feed_id}')
			ON DUPLICATE KEY UPDATE
				{$this->widget_container_type}_custom_feed_id = '{$this->selected_feed_id}'
		");

		$count = $this->count;
		$this->db->sql("
			UPDATE {$this->widget_container_type}_widget
			SET lister_item_count = {$count}
			WHERE id = {$this->widget_id} AND {$this->widget_container_type}_id = {$this->widget_container_id}
		");
		return true;
	}

}

?>