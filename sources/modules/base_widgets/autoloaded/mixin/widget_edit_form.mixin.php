<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class widget_edit_form_mixin extends base_mixin
{

	protected $widget_container_id;
	protected $widget_container_type;
	protected $widget_id;
	protected $widget_row;
	protected $widget_type_row;

	public function start()
	{
		$this->widget_type_row = $this->db->get_row("
			SELECT *
			FROM {$this->widget_container_type}_widget_type
			WHERE type_id = '{$this->widget_row["type_id"]}'
		");
		if (!$this->widget_type_row["edit_ctrl"])
		{
			return false;
		}

		return true;
	}

}

?>