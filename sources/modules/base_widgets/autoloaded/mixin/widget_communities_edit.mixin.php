<?php

class widget_communities_edit_mixin extends base_widget_edit_mixin
{

	protected $communities_selected;
	protected $project_id;

	public function start()
	{
		$this->communities_selected = POST_AS_ARRAY("communities_selected");

		foreach ($this->communities_selected as $key => $community_id)
		{
			if (!is_good_id($community_id))
			{
				return false;
			}

			$community_exists = $this->db->row_exists("
				SELECT id
				FROM community
				WHERE id = {$community_id} AND project_id = {$this->project_id} AND is_deleted = 0
			");
			if (!$community_exists)
			{
				unset($this->communities_selected[$key]);
			}
		}

		return true;
	}

	public function commit()
	{
		$this->db->sql("
			DELETE FROM {$this->widget_container_type}_widget_communities
			WHERE widget_id = {$this->widget_id}
		");

		$position = 0;
		foreach ($this->communities_selected as $community_id)
		{
			$position++;
			$this->db->sql("
				INSERT INTO {$this->widget_container_type}_widget_communities (widget_id, community_id, position)
				VALUES ({$this->widget_id}, {$community_id}, {$position})
			");
		}

		return true;
	}

}

?>