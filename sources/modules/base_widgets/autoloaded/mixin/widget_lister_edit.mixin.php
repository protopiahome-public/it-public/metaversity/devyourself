<?php

class widget_lister_edit_mixin extends base_widget_edit_mixin
{

	const MAX_ITEM_COUNT = 50;

	protected $count;

	public function start()
	{
		$this->count = POST("count");

		if (!is_good_id($this->count))
		{
			return false;
		}

		if ($this->count > self::MAX_ITEM_COUNT)
		{
			return false;
		}

		return true;
	}

	public function commit()
	{
		$count = $this->count;

		$this->db->sql("
			UPDATE {$this->widget_container_type}_widget
			SET lister_item_count = {$count}
			WHERE id = {$this->widget_id} AND {$this->widget_container_type}_id = {$this->widget_container_id}
		");

		return true;
	}

}

?>