<?php

class competence_set_access_test extends base_test
{

	public function set_up()
	{
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user')");
		}
		$this->db->sql("REPLACE INTO competence_set (id, title) VALUES (1, 'test')");
		$this->db->sql("DELETE FROM competence_set_admin WHERE competence_set_id = 1 AND user_id = 1");
	}

	public function moderator_test()
	{
		$this->db->sql("REPLACE INTO competence_set_admin (competence_set_id, user_id, is_admin, is_professiogram_moderator) VALUES (1, 1, 0, 1)");
		$this->mcache->flush();

		$access = new competence_set_access(new competence_set_obj(1), 1);
		$this->assert_false($access->has_admin_rights());
		$this->assert_true($access->has_moderator_rights());
		$this->assert_true($access->can_moderate_professiograms());
	}

	public function admin_test()
	{
		$this->db->sql("REPLACE INTO competence_set_admin (competence_set_id, user_id, is_admin, is_professiogram_moderator) VALUES (1, 1, 1, 0 /* !!! */)");
		$this->mcache->flush();

		$access = new competence_set_access(new competence_set_obj(1), 1);
		$this->assert_true($access->has_admin_rights());
		$this->assert_true($access->has_moderator_rights());
		$this->assert_true($access->can_moderate_professiograms());
	}

	public function admin_cache_test()
	{
		$this->db->sql("REPLACE INTO competence_set_admin (competence_set_id, user_id, is_admin, is_professiogram_moderator) VALUES (1, 1, 1, 1)");
		$this->mcache->flush();
		
		$competence_set_obj = new competence_set_obj(1);

		$access = new competence_set_access($competence_set_obj, 1);
		$this->assert_true($access->has_admin_rights());
		$this->assert_true($access->can_moderate_professiograms());

		$query_count = $this->db->debug_get_query_count();
		$access = new competence_set_access($competence_set_obj, 1);
		$this->assert_true($access->has_admin_rights());
		$this->assert_true($access->can_moderate_professiograms());
		$this->assert_identical($this->db->debug_get_query_count() - $query_count, 0);
	}

	public function super_admin_test()
	{
		$this->db->sql("DELETE FROM competence_set_admin WHERE competence_set_id = 1 AND user_id = 1");
		$this->mcache->flush();

		$this->user->set_user_data(array("is_admin" => "1"));
		$access = new competence_set_access(new competence_set_obj(1), 1);
		$this->assert_true($access->has_admin_rights());
		$this->assert_true($access->has_moderator_rights());
		$this->assert_true($access->can_moderate_professiograms());
		$this->user->set_user_data(null);
	}
	
	public function unexisted_comptence_set_test()
	{
		$this->db->sql("DELETE FROM competence_set WHERE id = 99999");
		$this->mcache->flush();

		$access = new competence_set_access(new competence_set_obj(99999), 1);
		$this->assert_identical($access->get_level(), 0);
		$this->assert_identical($access->get_status(), "error");
		$this->assert_false($access->has_member_rights());
		$this->assert_false($access->has_admin_rights());
		$this->assert_false($access->has_moderator_rights());
		$this->assert_false($access->can_moderate_professiograms());
		$this->user->set_user_data(null);
	}

}

?>