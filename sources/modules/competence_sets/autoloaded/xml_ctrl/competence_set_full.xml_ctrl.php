<?php

class competence_set_full_xml_ctrl extends base_simple_xml_ctrl
{

	protected $competence_set_id;
	protected $data_one_row = true;
	protected $row_name = "competence_set";
	protected $allow_error_404_if_no_data = true;
	protected $cache = true;
	protected $cache_key_property_name = "competence_set_id";

	public function __construct($competence_set_id)
	{
		$this->competence_set_id = $competence_set_id;

		parent::__construct();
	}

	protected function set_result()
	{
		$this->result = "
			SELECT
				id, title,
				professiogram_count_calc,
				personal_mark_count_calc, group_mark_count_calc, group_mark_raw_count_calc, total_mark_count_calc, project_count_calc,
				personal_mark_count_with_trans_calc, group_mark_count_with_trans_calc, group_mark_raw_count_with_trans_calc, total_mark_count_with_trans_calc, project_count_with_trans_calc
			FROM competence_set
			WHERE id = {$this->competence_set_id}
		";
	}

}

?>