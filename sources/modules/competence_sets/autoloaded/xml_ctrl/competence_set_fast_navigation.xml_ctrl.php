<?php

/**
 * 'Fast navigation'
 * Appears at /ratings/ and <project_url>/precedents/precedent-xxx/edit/competences/
 * @todo Remove this module after the number of competence sets exceeds ~50
 */
class competence_set_fast_navigation_xml_ctrl extends base_simple_xml_ctrl
{

	protected $data_one_row = false;
	protected $row_name = "competence_set";
	protected $allow_error_404_if_no_data = false;
	protected $cache = false;
	protected $documents_per_page = 0;
	protected $dependencies_ctrl = array();

	protected function set_result()
	{
		$this->result = $this->db->sql("
			SELECT id, title
			FROM competence_set
			ORDER BY title
			LIMIT 100
		");
	}

	protected function set_auxil_data()
	{
		
	}

}

?>