<?php

class competence_short_xml_ctrl extends base_simple_xml_ctrl
{

	protected $competence_id;

	protected $data_one_row = true;

	protected $row_name = "competence";

	protected $allow_error_404_if_no_data = true;

	protected $cache = true;

	protected $cache_key_property_name = "competence_id";

	public function __construct($competence_id)
	{
		$this->competence_id = $competence_id;
		
		parent::__construct();
	}

	protected function set_result()
	{
		$this->result = "
			SELECT
				id, title
			FROM competence_full
			WHERE id = {$this->competence_id}
		";
	}

	protected function set_auxil_data()
	{
		
	}

}

?>