<?php

class competence_set_competences_xml_ctrl extends base_easy_xml_ctrl
{

	protected $competence_set_id;
	protected $show_stat;
	protected $xml_attrs = array("competence_set_id");

	public function __construct($competence_set_id, $show_stat = false)
	{
		$this->competence_set_id = $competence_set_id;
		$this->show_stat = $show_stat;

		parent::__construct();
	}

	public function load_data(select_sql $select_sql = null)
	{
		// Groups
		$data_raw_plain = $this->db->fetch_all("
			SELECT
				id, title, parent_id
			FROM competence_group
			WHERE competence_set_id = {$this->competence_set_id}
			ORDER BY position, id
		");
		$data_index = array();
		foreach ($data_raw_plain as $row)
		{
			$data_index[$row["id"]] = array(
				"id" => $row["id"],
				"title" => $row["title"],
				"competences" => array(),
				"children" => array(),
			);
		}
		$data = array();
		foreach ($data_raw_plain as $row)
		{
			if ($row["parent_id"])
			{
				$data_index[$row["parent_id"]]["children"][] = &$data_index[$row["id"]];
			}
			else
			{
				$data[] = &$data_index[$row["id"]];
			}
		}

		// Competences
		if (sizeof($data_index))
		{
			$stat_sql = $this->show_stat ? ", c.personal_mark_count_calc, c.group_mark_count_calc, c.group_mark_raw_count_calc, c.total_mark_count_calc" : "";
			$db_result = $this->db->sql("
				SELECT
					l.competence_group_id,
					c.competence_id id, c.number, c.title
					{$stat_sql}
				FROM competence_link l
				JOIN competence_calc c ON c.competence_id = l.competence_id
				WHERE
					l.competence_group_id IN (" . join(",", array_keys($data_index)) . ")
				ORDER BY l.position, c.competence_id
			");
			while ($row = $this->db->fetch_array($db_result))
			{
				$data_index[$row["competence_group_id"]]["competences"][] = $row;
			}
		}

		$this->data = $data;
	}

	public function fill_xml(xdom $xdom)
	{
		$this->group_export($xdom, $this->data);

		return $xdom;
	}

	private function group_export(xnode $xnode, $data)
	{
		foreach ($data as $competence_group_data)
		{
			$group_node = $xnode->create_child_node("group")
				->set_attr("id", $competence_group_data["id"])
				->set_attr("title", $competence_group_data["title"]);
			foreach ($competence_group_data["competences"] as $competence_data)
			{
				$competence_node = $group_node->create_child_node("competence")
					->set_attr("id", $competence_data["id"])
					->set_attr("number", $competence_data["number"])
					->set_attr("title", $competence_data["title"]);
				if ($this->show_stat)
				{
					$competence_node->set_attr("personal_mark_count_calc", $competence_data["personal_mark_count_calc"])
						->set_attr("group_mark_count_calc", $competence_data["group_mark_count_calc"])
						->set_attr("group_mark_raw_count_calc", $competence_data["group_mark_raw_count_calc"])
						->set_attr("total_mark_count_calc", $competence_data["total_mark_count_calc"]);
				}
			}
			if (sizeof($competence_group_data["children"]))
			{
				$this->group_export($group_node, $competence_group_data["children"]);
			}
		}
	}

	public function get_cache()
	{
		if (!$this->show_stat)
		{
			return competence_set_competences_cache::init($this->competence_set_id);
		}
	}

}

?>