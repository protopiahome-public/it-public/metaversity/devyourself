<?php

class competence_set_access_xml_ctrl extends base_xml_ctrl
{

	protected $competence_set_id;

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;

	/**
	 * @var competence_set_access
	 */
	protected $competence_set_access;

	public function __construct($competence_set_id)
	{
		$this->competence_set_id = $competence_set_id;

		$this->competence_set_obj = competence_set_obj::instance($this->competence_set_id);
		$this->competence_set_access = $this->competence_set_obj->get_access();

		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		$attrs = array(
			"competence_set_id" => $this->competence_set_access->get_competence_set_id(),
			"has_moderator_rights" => $this->competence_set_access->has_moderator_rights(),
			"has_admin_rights" => $this->competence_set_access->has_admin_rights(),
			"can_moderate_professiograms" => $this->competence_set_access->can_moderate_professiograms(),
			"status" => $this->competence_set_access->get_status(),
		);

		return $this->get_node_string(null, $attrs);
	}

}

?>