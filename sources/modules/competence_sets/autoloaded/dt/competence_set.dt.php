<?php

class competence_set_dt extends base_dt
{

	protected function init()
	{

		$dtf = new string_dtf("title", "Название набора компетенций");
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$this->add_field($dtf);

		$dtf = new text_html_dtf("descr", "Описание набора компетенций");
		$dtf->set_editor_height(240);
		$this->add_field($dtf);

		$dtf = new int_dtf("competence_count_calc", "Количество компетенций");
		$dtf->set_read_only();
		$this->add_field($dtf);

		$dtf = new int_dtf("professiogram_count_calc", "Количество профессиограмм");
		$dtf->set_read_only();
		$this->add_field($dtf);

		$dtf = new int_dtf("personal_mark_count_calc", "Количество оценок (личных)");
		$dtf->set_read_only();
		$this->add_field($dtf);

		$dtf = new int_dtf("group_mark_count_calc", "Количество оценок (групповых)");
		$dtf->set_read_only();
		$this->add_field($dtf);

		$dtf = new int_dtf("group_mark_raw_count_calc", "Количество оценок (групповых, чистых)");
		$dtf->set_read_only();
		$this->add_field($dtf);

		$dtf = new int_dtf("total_mark_count_calc", "Количество оценок (суммарное)");
		$dtf->set_read_only();
		$this->add_field($dtf);

		$dtf = new int_dtf("project_count_calc", "Количество проектов с оценками");
		$dtf->set_read_only();
		$this->add_field($dtf);

		$dtf = new int_dtf("personal_mark_count_with_trans_calc", "Количество оценок (личных) (с учетом переводчиков)");
		$dtf->set_read_only();
		$this->add_field($dtf);

		$dtf = new int_dtf("group_mark_count_with_trans_calc", "Количество оценок (групповых) (с учетом переводчиков)");
		$dtf->set_read_only();
		$this->add_field($dtf);

		$dtf = new int_dtf("group_mark_raw_count_with_trans_calc", "Количество оценок (групповых, чистых) (с учетом переводчиков)");
		$dtf->set_read_only();
		$this->add_field($dtf);

		$dtf = new int_dtf("total_mark_count_with_trans_calc", "Количество оценок (суммарное) (с учетом переводчиков)");
		$dtf->set_read_only();
		$this->add_field($dtf);

		$dtf = new int_dtf("project_count_with_trans_calc", "Количество проектов с оценками (с учетом переводчиков)");
		$dtf->set_read_only();
		$this->add_field($dtf);

		$dtf = new datetime_dtf("add_time", "Дата создания");
		$dtf->set_read_only();
		$this->add_field($dtf, "main");

		$dtf = new datetime_dtf("edit_time", "Дата редактирования");
		$dtf->set_read_only();
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("short", array("title"));
		$this->add_fields_in_axis("create", array("title", "descr"));
		$this->add_fields_in_axis("edit", array("title", "descr"));
		$this->add_fields_in_axis("profile", array("add_time", "edit_time", "title", "descr", "professiogram_count_calc", "personal_mark_count_calc", "group_mark_count_calc", "group_mark_raw_count_calc", "total_mark_count_calc", "project_count_calc", "personal_mark_count_with_trans_calc", "group_mark_count_with_trans_calc", "group_mark_raw_count_with_trans_calc", "total_mark_count_with_trans_calc", "project_count_with_trans_calc", "competence_count_calc"));
	}

}

?>