<?php

class rate_save_ctrl extends base_save_ctrl
{
	/* Settings */

	/**
	 * DB table
	 * Ex.: "professigram", "project_role"
	 */
	protected $type;

	/**
	 * Options' keys
	 * Ex.: 1, 2, 3
	 */
	protected $options = array();

	/**
	 * DB table PK value
	 */
	protected $object_id;
	protected $rate_id;
	protected $competence_set_id;

	/* Internal */
	protected $rates = array();
	protected $competences = array();
	protected $real_rate_id;

	public function __construct($type, $options)
	{
		$this->type = $type;
		$this->options = $options;
		parent::__construct();
	}

	public function start()
	{
		if (!is_good_num($this->rate_id))
		{
			trigger_error("rate_id is not set");
			return false;
		}

		if (!is_good_id($this->competence_set_id))
		{
			trigger_error("competence_set_id is not set");
			return false;
		}

		$this->competences = $this->db->fetch_all("
			SELECT competence_id
			FROM competence_calc
			WHERE competence_set_id = {$this->competence_set_id}
		", "competence_id");

		$rates_input = POST_AS_ARRAY("rates");

		foreach ($this->competences as $competence_id => $competence_data)
		{
			if (isset($rates_input[$competence_id]))
			{
				if (!is_string($rates_input[$competence_id]) || !in_array($rates_input[$competence_id], $this->options))
				{
					return false;
				}
				$this->rates[$competence_id] = (int) $rates_input[$competence_id];
			}
		}

		return true;
	}

	public function commit()
	{
		if (!is_good_id($this->object_id))
		{
			trigger_error("object_id is not set");
		}

		$type_id = $this->db->get_value("SELECT id FROM rate_type WHERE main_table = '{$this->type}'");
		if (!$type_id)
		{
			trigger_error("Incorrect main_table for the rate_type: '{$this->type}'");
			return false;
		}

		if (!$this->rate_id)
		{
			$this->db->sql("
				INSERT INTO rate (rate_type_id, competence_set_id, add_time, edit_time)
				VALUES ({$type_id}, {$this->competence_set_id}, NOW(), NOW())
			");
			$this->real_rate_id = $this->db->get_last_id();

			$this->update_object_table();
		}
		else
		{
			$this->db->sql("
				UPDATE rate
				SET edit_time = NOW()
				WHERE id = {$this->rate_id}
			");
			$this->real_rate_id = $this->rate_id;
		}

		foreach ($this->rates as $competence_id => $mark)
		{
			if ($mark == 0)
			{
				$this->db->sql("
					DELETE FROM	rate_competence_link
					WHERE rate_id = {$this->real_rate_id}
						AND competence_id = {$competence_id}
				");
			}
			else
			{
				$this->db->sql("
					INSERT INTO rate_competence_link 
						(rate_id, competence_id, mark)
					VALUES
						({$this->real_rate_id}, {$competence_id}, {$mark})
					ON DUPLICATE KEY UPDATE
						mark = {$mark}
				");
			}
		}

		$this->update_object_table_count();

		return true;
	}

	protected function update_object_table()
	{
		$this->db->sql("
			UPDATE `{$this->type}`
			SET
				rate_id = {$this->real_rate_id}
			WHERE id = {$this->object_id}
		");
	}

	protected function update_object_table_count()
	{
		$this->db->sql("
			UPDATE `{$this->type}`
			SET
				competence_count_calc = (
					SELECT COUNT(*)
					FROM rate_competence_link l
					JOIN competence_calc c ON c.competence_id = l.competence_id
					WHERE l.rate_id = {$this->real_rate_id}
				)
			WHERE id = {$this->object_id}
		");
	}

	public function clean_cache()
	{
		if ($this->rate_id)
		{
			rate_cache_tag::init($this->rate_id)->update();
		}
	}

	public function set_rate_id($id)
	{
		$this->rate_id = $id;
	}

	public function set_object_id($id)
	{
		$this->object_id = $id;
	}

	public function set_competence_set_id($id)
	{
		$this->competence_set_id = $id;
	}

}

?>