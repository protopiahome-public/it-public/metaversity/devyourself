<?php

class competence_set_competences_xml_page extends base_xml_ctrl
{
	private $competence_set_id;
	
	public function __construct($competence_set_id)
	{
		$this->competence_set_id = $competence_set_id;
		parent::__construct();
	}
	
	public function get_xml()
	{
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->competence_set_id, true));
		return "";
	}

}

?>