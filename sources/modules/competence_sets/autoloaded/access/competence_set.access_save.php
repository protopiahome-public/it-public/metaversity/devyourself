<?php

class competence_set_access_save extends base_access_save
{

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;

	/**
	 * @var competence_set_access_maper
	 */
	protected $access_maper;
	/**
	 * @var competence_set_access_fetcher
	 */
	protected $access_fetcher;
	protected $admin_row;

	public function __construct($competence_set_obj, $user_id)
	{
		$this->competence_set_obj = $competence_set_obj;
		parent::__construct($user_id);
	}

	protected function fill_data()
	{
		$this->access_maper = new competence_set_access_maper();
		$this->access_fetcher = new competence_set_access_fetcher($this->competence_set_obj, $this->access_maper, $this->user_id, $lock = true);
		$this->admin_row = $this->access_fetcher->get_admin_row();
		$this->level = $this->access_fetcher->get_level();
		return !$this->access_fetcher->error_occured();
	}

	public function add_member()
	{
		return false;
	}

	public function add_moderator()
	{
		if ($this->level > ACCESS_LEVEL_MODERATOR)
		{
			return false;
		}
		if ($this->level == ACCESS_LEVEL_MODERATOR)
		{
			return true;
		}
		$adder_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		$this->db->sql("
			INSERT INTO competence_set_admin (competence_set_id, user_id, add_time, adder_admin_user_id, edit_time, editor_admin_user_id, is_admin)
			VALUES ('{$this->competence_set_obj->get_id()}', '{$this->user_id}', NOW(), '{$adder_user_id}', NOW(), '{$adder_user_id}', 0)
			ON DUPLICATE KEY UPDATE
				edit_time = NOW(),
				editor_admin_user_id = {$adder_user_id},
				is_admin = 0
		");
		return true;
	}

	public function add_admin()
	{
		if ($this->level > ACCESS_LEVEL_ADMIN)
		{
			return false;
		}
		if ($this->level == ACCESS_LEVEL_ADMIN)
		{
			return true;
		}
		$adder_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		$this->db->sql("
			INSERT INTO competence_set_admin (competence_set_id, user_id, add_time, adder_admin_user_id, edit_time, editor_admin_user_id, is_admin)
			VALUES ('{$this->competence_set_obj->get_id()}', '{$this->user_id}', NOW(), '{$adder_user_id}', NOW(), '{$adder_user_id}', 1)
			ON DUPLICATE KEY UPDATE
				edit_time = NOW(),
				editor_admin_user_id = {$adder_user_id},
				is_admin = 1
		");
		return true;
	}

	public function delete_member()
	{
		return false;
	}

	public function delete_moderator()
	{
		if ($this->level < ACCESS_LEVEL_MODERATOR)
		{
			return false;
		}
		$this->db->sql("
			DELETE
			FROM competence_set_admin
			WHERE
				competence_set_id = '{$this->competence_set_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		return true;
	}

	public function delete_admin()
	{
		if ($this->level < ACCESS_LEVEL_ADMIN)
		{
			return false;
		}
		$this->db->sql("
			DELETE
			FROM competence_set_admin
			WHERE
				competence_set_id = '{$this->competence_set_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		return true;
	}

	public function set_moderator_rights($rights_array)
	{
		$editor_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		foreach ($rights_array as $idx => $val)
		{
			if ($idx == "is_admin")
			{
				trigger_error("Incorrect set_moderator_rights() function usage ('is_admin' check)");
				return false;
			}
			if (!preg_match("/^is_[a-z0-9_]+_moderator$/i", $idx))
			{
				trigger_error("Incorrect set_moderator_rights() function usage (regexp check)");
				return false;
			}
			if ((string) $val !== "0" and (string) $val !== "1")
			{
				trigger_error("Incorrect set_moderator_rights() function usage (value check)");
				return false;
			}
			$rights_array["edit_time"] = "NOW()";
			$rights_array["editor_admin_user_id"] = $editor_user_id;
		}
		if (!$this->is_moderator_strict())
		{
			return false;
		}
		$this->db->update_by_array("competence_set_admin", $rights_array, "competence_set_id = {$this->competence_set_obj->get_id()} AND user_id = {$this->user_id}");
		return true;
	}
	
	public function clean_cache()
	{
		competence_set_access_cache_tag::init($this->competence_set_obj->get_id(), $this->user_id)->update();
	}

}

?>