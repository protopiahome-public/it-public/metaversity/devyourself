<?php

class competence_set_access extends base_access
{

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;

	/**
	 * @var competence_set_access_fetcher
	 */
	protected $access_fetcher;

	/**
	 * @var competence_set_access_maper
	 */
	protected $access_maper;
	protected $admin_row = false;

	public function __construct(competence_set_obj $competence_set_obj, $user_id = null)
	{
		$this->competence_set_obj = $competence_set_obj;
		parent::__construct($user_id);
	}
	
	protected function get_new_access_maper()
	{
		return new competence_set_access_maper();
	}

	protected function fill_data()
	{
		if (!$this->user_id)
		{
			$this->level = ACCESS_LEVEL_GUEST;
			return;
		}
		$cache = competence_set_access_cache::init($this->competence_set_obj->get_id(), $this->user_id);
		$data = $cache->get();
		if ($data)
		{
			$this->level = $data["level"];
			$this->admin_row = $data["admin_row"];
			return;
		}
		$this->access_fetcher = new competence_set_access_fetcher($this->competence_set_obj, $this->access_maper, $this->user_id);
		$this->level = $this->access_fetcher->get_level();
		if (!$this->access_fetcher->error_occured())
		{
			$this->admin_row = $this->access_fetcher->get_admin_row();
			$data = array(
				"level" => $this->level,
				"admin_row" => $this->admin_row,
			);
			$cache->set($data);
		}
	}

	public function can_moderate_professiograms()
	{
		return $this->user->is_admin() || $this->has_admin_rights() || $this->has_moderator_rights() && $this->admin_row["is_professiogram_moderator"];
	}

	public function get_competence_set_id()
	{
		return $this->competence_set_obj->get_id();
	}

}

?>