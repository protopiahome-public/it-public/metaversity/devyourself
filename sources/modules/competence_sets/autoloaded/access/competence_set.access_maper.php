<?php

class competence_set_access_maper extends base_access_maper
{

	protected function fill_status_to_level()
	{
		$this->status_to_level = array(
			ACCESS_STATUS_ERROR /*             */ => ACCESS_LEVEL_ERROR,
			ACCESS_STATUS_GUEST /*             */ => ACCESS_LEVEL_GUEST,
			ACCESS_STATUS_USER /*              */ => ACCESS_LEVEL_USER,
			ACCESS_STATUS_DELETED /*           */ => ACCESS_LEVEL_DELETED,
			ACCESS_STATUS_MODERATOR /*         */ => ACCESS_LEVEL_MODERATOR,
			ACCESS_STATUS_ADMIN /*             */ => ACCESS_LEVEL_ADMIN,
		);
	}

}

?>