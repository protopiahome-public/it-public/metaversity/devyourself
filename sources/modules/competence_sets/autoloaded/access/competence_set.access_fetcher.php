<?php

class competence_set_access_fetcher extends base_access_fetcher
{

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;
	protected $admin_row = false;

	public function __construct(competence_set_obj $competence_set_obj, competence_set_access_maper $access_maper, $user_id = null, $lock = false)
	{
		$this->competence_set_obj = $competence_set_obj;
		parent::__construct($access_maper, $user_id, $lock);
	}

	protected function fetch_data()
	{
		$for_update_sql = $this->lock ? "FOR UPDATE" : "";

		if (!$this->competence_set_obj->exists())
		{
			$this->level = ACCESS_LEVEL_ERROR;
			$this->error("Competence set does not exist: '{$this->competence_set_obj->get_id()}'");
			return;
		}
		
		$this->admin_row = $this->db->get_row("
			SELECT *
			FROM competence_set_admin
			WHERE competence_set_id = {$this->competence_set_obj->get_id()} AND user_id = {$this->user_id}
			{$for_update_sql}
		");
		if (!$this->admin_row)
		{
			$this->level = ACCESS_LEVEL_USER;
		}
		elseif ($this->admin_row["is_admin"] == "1")
		{
			$this->level = ACCESS_LEVEL_ADMIN;
		}
		else
		{
			$this->level = ACCESS_LEVEL_MODERATOR;
		}
	}

	protected function error($error_msg)
	{
		$log_message = $error_msg . " (competence_set_id = {$this->competence_set_obj->get_id()}, user_id = {$this->user_id})";
		file_put_contents(PATH_LOG . "/competence_set_access.log", $log_message . "\n", FILE_APPEND);
		parent::error($error_msg);
	}

	public function get_admin_row()
	{
		return $this->admin_row;
	}

}

?>