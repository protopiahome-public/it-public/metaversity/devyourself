<?php

class competence_set_competences_cache extends base_cache
{

	public static function init($competence_set_id)
	{
		$tag_keys = array();
		$tag_keys[] = competence_set_competences_cache_tag::init($competence_set_id)->get_key();
		return parent::get_cache(__CLASS__, $competence_set_id, $tag_keys);
	}

}

?>