<?php

class competence_set_add_save_page extends base_dt_add_save_ctrl
{

	protected $dt_name = "competence_set";
	protected $axis_name = "create";
	
	public function check_rights()
	{
		return $this->user->get_user_id() > 0;
	}

	public function on_before_commit()
	{
		$user_id = $this->user->get_user_id();
		if (!$user_id)
		{
			return false;
		}
		$this->update_array["adder_user_id"] = $user_id;
		return true;
	}
	
	public function on_after_commit()
	{
		$competence_set_obj = new competence_set_obj($this->last_id, $lock = true);
		$access_save = new competence_set_access_save($competence_set_obj, $this->user->get_user_id());
		$access_save->add_admin();
	}

}

?>