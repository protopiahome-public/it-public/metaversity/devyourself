<?php

class competence_set_access_cache_tag extends base_cache_tag
{

	public static function init($competence_set_id, $user_id)
	{
		return parent::get_tag(__CLASS__, $competence_set_id, $user_id);
	}

}

?>