<?php

final class competences_taxonomy extends base_taxonomy
{

	public function run()
	{
		$p = $this->get_parts_relative();

		if (($page = $this->is_page_folder($p[1])))
		{
			if ($p[2] === null)
			{
				//sets/
				$this->xml_loader->add_xml_with_xslt(new competence_sets_xml_page($page));
			}
		}
		elseif ($p[1] === "add" and $p[2] === null)
		{
			//sets/add/
			$this->xml_loader->add_xml_with_xslt(new competence_set_add_xml_page());
		}
		elseif (($competence_set_id = $this->is_set_folder($p[1])))
		{
			//sets/set-<id>/
			$this->xml_loader->add_xml_by_class_name("competence_set_short_xml_ctrl", $competence_set_id);

			$competence_set_obj = new competence_set_obj($competence_set_id);
			if (!$competence_set_obj->exists())
			{
				return;
			}
			$competence_set_access = $competence_set_obj->get_access();

			$this->xml_loader->add_xml(new competence_set_access_xml_ctrl($competence_set_id));

			if ($p[2] === null)
			{
				//sets/set-<id>/
				$this->xml_loader->add_xml_with_xslt(new competence_set_profile_xml_page($competence_set_id));
			}
			elseif ($p[2] == "competences" and $p[3] === null)
			{
				//sets/set-<id>/competences/
				$this->xml_loader->add_xml_with_xslt(new competence_set_competences_xml_page($competence_set_id));
			}
			elseif ($p[2] === "professiograms")
			{
				$professiograms_taxonomy = new professiograms_taxonomy($this->xml_loader, $this, 2, $competence_set_obj);
				$professiograms_taxonomy->run();
			}
			elseif ($p[2] == "admin")
			{
				$competence_set_admin = new competence_set_admin_taxonomy($this->xml_loader, $this, 2, $competence_set_obj);
				$competence_set_admin->run();
			}
		}
	}

	private function is_set_folder($folder_param)
	{
		return $this->is_type_folder($folder_param, "set", $is_good_id = true);
	}

}

?>