<?php

class community_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"community_dt_init",
		"community_add_widgets_after_commit",
		"project_stat_clean_cache",
		"communities_tree_clean_cache",
		"communities_access_clean_cache",
	);
	protected $dt_name = "community";
	protected $axis_name = "add";
	protected $project_id;

	/**
	 * @var community_dt
	 */
	protected $dt;
	
	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->project_access->can_add_communities();
	}

	public function on_before_commit()
	{
		$this->update_array["project_id"] = "'{$this->project_id}'";
		return true;
	}

	public function on_after_commit()
	{
		$community_obj = new community_obj($this->last_id, $this->project_obj, $lock = true);
		$access_save = new community_access_save($community_obj, $this->project_obj, $this->user->get_user_id());
		$access_save->add_admin();
	}

	public function clean_cache()
	{
		
	}

}

?>