<?php

class communities_taxonomy extends base_taxonomy
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, project_obj $project_obj)
	{
		$this->project_obj = $project_obj;
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		$p = $this->get_parts_relative();

		$project_obj = $this->project_obj;
		$project_id = $project_obj->get_id();
		$project_access = $project_obj->get_access();

		if ($p[1] === "co" and !$project_obj->communities_are_on())
		{
			$this->xml_loader->set_error_404();
			$this->xml_loader->set_error_404_xslt("project_module_404", "project");
		}
		elseif ($p[1] === "co" and !$project_access->can_read_communities())
		{
			$this->xml_loader->set_error_403();
			$this->xml_loader->set_error_403_xslt("project_communities_403", "communities");
		}
		elseif ($p[1] === "co")
		{
			$this->xml_loader->add_xml(new communities_menu_xml_ctrl($project_id));
			if (!$project_obj->communities_are_on())
			{
				$this->xml_loader->add_xml_with_xslt(new error_404_xml_page());
			}
			elseif ($p[2] === null)
			{
				//p/<project_name>/co/[page-<page>]/
				$this->xml_loader->add_xml_with_xslt(new communities_xml_page($project_id));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//p/<project_name>/co/add/
				if ($project_access->can_add_communities())
				{
					$this->xml_loader->add_xml_with_xslt(new community_add_xml_page($project_id));
				}
				else
				{
					$this->xml_loader->set_error_403();
					$this->xml_loader->set_error_403_xslt("community_add_403", "communities");
				}
			}
			elseif ($p[2] === "feed" and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				//p/<project_name>/co/feed/[page-<page>]/
				$this->xml_loader->add_xml_with_xslt(new project_feed_xml_page($project_id, $page));
			}
			elseif ($p[2] === "feeds" and $p[3] === null)
			{
				//p/<project_name>/co/feeds/
				$this->xml_loader->add_xml_with_xslt(new project_custom_feeds_xml_page($project_id));
			}
			elseif ($p[2] === "feeds" and $project_custom_feed_id = $this->get_project_custom_feed_id_by_url_name($p[3], $project_id) and $page = $this->is_page_folder($p[4]) and $p[5] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new project_custom_feed_xml_page($project_id, $project_custom_feed_id, $page));
				$this->xml_loader->add_xml(new project_custom_feed_full_xml_ctrl($project_custom_feed_id, $project_id));
			}
			elseif ($community_id = $this->get_community_id_by_url_name($p[2], $project_id))
			{
				$community_taxonomy = new community_taxonomy($this->xml_loader, $this, 2, $project_obj, $community_id);
				$community_taxonomy->run();
			}
		}
	}

	private function get_community_id_by_url_name($url_name, $project_id)
	{
		return url_helper::get_community_id_by_name($url_name, $project_id);
	}

	private function get_project_custom_feed_id_by_url_name($url_name, $project_id)
	{
		return url_helper::get_project_custom_feed_id_by_name($url_name, $project_id);
	}

}

?>