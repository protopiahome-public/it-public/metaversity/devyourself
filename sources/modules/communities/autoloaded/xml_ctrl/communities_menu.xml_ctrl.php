<?php

class communities_menu_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "item";
	protected $dependencies_settings = array(
		array(
			"column" => "community_id",
			"ctrl" => "community_short",
			"param2" => "project_id",
		)
	);
	// Module specific
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		
		$this->project_obj = project_obj::instance($this->project_id);
		
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("communities_menu_item");
		$select_sql->add_select_fields("id, community_id");
		$select_sql->add_where("project_id = {$this->project_id}");
		$select_sql->add_order("position");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	protected function get_cache()
	{
		return communities_menu_cache::init($this->project_id);
	}

}

?>