<?php

class project_custom_feed_xml_page extends base_feed_xml_ctrl
{

//Settings
	protected $xml_attrs = array("project_id", "feed_id", "output_type");
	protected $xml_row_name = "post";
	// Internal
	protected $project_id;

	/**
	 *
	 * @var project_obj
	 */
	protected $project_obj;
	protected $feed_id;
	protected $page;
	protected $limit;

	public function __construct($project_id, $feed_id, $page, $limit = null)
	{
		$this->project_id = $project_id;
		$this->feed_id = $feed_id;
		$this->page = $page;
		$this->limit = $limit;

		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct();
	}

	public function init()
	{
		if (!$this->limit)
		{
			$this->add_easy_processor(new pager_db_fast_easy_processor($this->page, 25));
		}
	}

	public function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("id");
		$select_sql->add_from("post_calc");
		$select_sql->add_where("project_id = {$this->project_id}");
		$this->feed_sources_modify_sql($select_sql);
		$select_sql->add_order("id DESC");
		if ($this->limit)
		{
			$select_sql->set_limit($this->limit);
		}
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	protected function feed_sources_modify_sql(select_sql $select_sql)
	{
		$custom_feed_sources = project_custom_feed_data_cache::init($this->feed_id)->get();
		if (!is_array($custom_feed_sources))
		{
			$custom_feed_sources = $this->db->fetch_column_values("
				SELECT section_id, community_id
				FROM project_custom_feed_source
				WHERE custom_feed_id = {$this->feed_id}
			", "section_id", "community_id");

			project_custom_feed_data_cache::init($this->feed_id)->set($custom_feed_sources);
		}

		$communities_read_access_helper = $this->project_obj->get_communities_read_access_helper();
		$communities_read_access_helper->user_communities_in_custom_feed_modify_sql($custom_feed_sources, $select_sql);
		return $custom_feed_sources;
	}

	public function process_data()
	{
		foreach ($this->data as $post_data)
		{
			$this->xml_loader->add_xml(new post_full_xml_ctrl($post_data["id"], $this->project_id, $this->output_type == FEED_TYPE_EXPANDED));
		}
	}

}

?>