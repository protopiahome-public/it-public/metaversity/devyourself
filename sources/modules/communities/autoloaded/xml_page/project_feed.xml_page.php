<?php

class project_feed_xml_page extends base_feed_xml_ctrl
{

	//Settings
	protected $xml_attrs = array("project_id", "output_type");
	protected $xml_row_name = "post";
	// Internal
	/**
	 *
	 * @var project_obj
	 */
	protected $project_obj;
	protected $project_id;
	protected $page;

	public function __construct($project_id, $page)
	{
		$this->project_id = $project_id;
		$this->page = $page;
		
		$this->project_obj = project_obj::instance($this->project_id);
		
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_fast_easy_processor($this->page, 25));
	}

	public function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("id");
		$select_sql->add_from("post_calc");
		$select_sql->add_where("project_id = {$this->project_id}");
		$select_sql->add_where("hide_in_project_feed = 0");
		$select_sql->add_order("id DESC");

		$communities_read_access_helper = $this->project_obj->get_communities_read_access_helper();
		$communities_read_access_helper->user_communities_modify_sql($select_sql);

		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	public function process_data()
	{
		foreach ($this->data as $post_data)
		{
			$this->xml_loader->add_xml(new post_full_xml_ctrl($post_data["id"], $this->project_id, $this->output_type == FEED_TYPE_EXPANDED));
		}
	}

}

?>