<?php

class community_add_xml_page extends base_dt_add_xml_ctrl
{

	// Settings
	protected $dt_name = "community";
	protected $axis_name = "add";
	protected $enable_blocks = false;
	// Internal
	protected $project_id;

	/**
	 * @var community_dt
	 */
	protected $dt;

	/**
	 * @var project_access
	 */
	protected $project_access;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();
		
		parent::__construct();
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_project_obj($this->project_obj);
		return true;
	}

	public function check_rights()
	{
		return true;
		//return $this->project_access->can_moderate_communities();
	}

}

?>