<?php

class project_custom_feeds_xml_page extends base_easy_xml_ctrl
{

	protected $xml_row_name = "feed";
	protected $project_id;
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "project_custom_feed_full",
			"param2" => "project_id",
		)
	);

	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("project_custom_feed");
		$select_sql->add_select_fields("id");
		$select_sql->add_where("project_id = {$this->project_id}");
		$select_sql->add_order("title ASC");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>
