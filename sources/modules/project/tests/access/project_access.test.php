<?php

class project_access_test extends base_test
{

	private $log_path;

	public function set_up()
	{
		$this->log_path = PATH_LOG . "/project_access.log";
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user')");
		}
		$this->db->sql("REPLACE INTO project (id, name, title, communities_are_on, events_are_on, materials_are_on, role_recommendation_is_on) VALUES (1, 'test', 'test', 1, 1, 1, 1)");
		$this->db->sql("REPLACE INTO user_project_link (project_id, user_id, status) VALUES (1, 1, 'member')");
		$this->db->sql("DELETE FROM project_admin WHERE project_id = 1 AND user_id = 1");
	}

	public function basic_test()
	{
		$this->mcache->flush();
		$access = new project_access(project_obj::instance(1), 1);
		$this->assert_true($access->has_member_rights());
	}

	public function basic_cache_test()
	{
		$this->mcache->flush();
		$query_count = $this->db->debug_get_query_count();

		project_obj::_test_clean_cache();
		$access = new project_access(project_obj::instance(1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_identical($this->db->debug_get_query_count() - $query_count, 2);

		$access = new project_access(project_obj::instance(1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_identical($this->db->debug_get_query_count() - $query_count, 2);
	}

	public function moderator_test()
	{
		$this->db->sql("REPLACE INTO user_project_link (project_id, user_id, status) VALUES (1, 1, 'moderator')");
		$this->db->sql("REPLACE INTO project_admin (project_id, user_id, is_event_moderator) VALUES (1, 1, 1)");
		$this->mcache->flush();

		$access = new project_access(project_obj::instance(1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_false($access->has_admin_rights());
		$this->assert_true($access->has_moderator_rights());
		$this->assert_true($access->can_moderate_events());
		$this->assert_false($access->can_moderate_roles());
	}

	public function admin_test()
	{
		$this->db->sql("REPLACE INTO user_project_link (project_id, user_id, status) VALUES (1, 1, 'admin')");
		$this->db->sql("REPLACE INTO project_admin (project_id, user_id, is_admin, is_event_moderator, is_role_moderator) VALUES (1, 1, 1, 0 /* !!! */, 1)");
		$this->mcache->flush();

		$access = new project_access(project_obj::instance(1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_true($access->has_admin_rights());
		$this->assert_true($access->has_moderator_rights());
		$this->assert_true($access->can_moderate_events());
		$this->assert_true($access->can_moderate_roles());
	}

	public function admin_cache_test()
	{
		$this->db->sql("REPLACE INTO user_project_link (project_id, user_id, status) VALUES (1, 1, 'admin')");
		$this->db->sql("REPLACE INTO project_admin (project_id, user_id, is_admin, is_event_moderator, is_role_moderator) VALUES (1, 1, 1, 0 /* !!! */, 1)");
		$this->mcache->flush();

		$access = new project_access(project_obj::instance(1), 1);
		$this->assert_true($access->has_admin_rights());
		$this->assert_true($access->can_moderate_events());

		$query_count = $this->db->debug_get_query_count();
		$access = new project_access(project_obj::instance(1), 1);
		$this->assert_true($access->has_admin_rights());
		$this->assert_true($access->can_moderate_events());
		$this->assert_identical($this->db->debug_get_query_count() - $query_count, 0);
	}

	public function super_admin_test()
	{
		$this->db->sql("DELETE FROM user_project_link WHERE project_id = 1 AND user_id = 1");
		$this->db->sql("DELETE FROM project_admin WHERE project_id = 1 AND user_id = 1");
		$this->mcache->flush();

		$this->user->set_user_data(array("is_admin" => "1"));
		$access = new project_access(project_obj::instance(1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_true($access->has_admin_rights());
		$this->assert_true($access->has_moderator_rights());
		$this->assert_true($access->can_moderate_events());
		$this->assert_true($access->can_moderate_roles());
		$this->user->set_user_data(null);
	}

	public function not_admin_test()
	{
		$this->db->sql("REPLACE INTO user_project_link (project_id, user_id, status) VALUES (1, 1, 'moderator')");
		$this->db->sql("REPLACE INTO project_admin (project_id, user_id, is_admin, is_event_moderator) VALUES (1, 1, 1 /* !!! */, 1)");
		$this->mcache->flush();

		$access = new project_access(project_obj::instance(1), 1);
		$this->assert_false($access->has_admin_rights());
	}

	public function log_test()
	{
		if (file_exists($this->log_path))
		{
			$this->assert_fail("Log already exists. We fail here to save the log");
			return;
		}
		$this->db->sql("REPLACE INTO user_project_link (project_id, user_id, status) VALUES (1, 1, 'moderator')");
		$this->db->sql("DELETE FROM project_admin WHERE project_id = 1 AND user_id = 1");
		$this->mcache->flush();

		$access = new project_access(project_obj::instance(1), 1);
		$this->assert_true(file_exists($this->log_path));
		unlink_safe($this->log_path);
	}
	
//	public function unexisted_project_test()
//	{
//		$this->db->sql("DELETE FROM project WHERE id = 99999");
//		$this->mcache->flush();
//
//		$project_obj = project_obj::instance(99999);
//		$this->assert_null($project_obj->get_access());
//	}

}

?>