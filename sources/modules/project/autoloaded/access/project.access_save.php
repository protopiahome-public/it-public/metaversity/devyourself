<?php

require_once PATH_CORE . "/loader/xml_loader.php";

class project_access_save extends base_access_save
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access_maper
	 */
	protected $access_maper;

	/**
	 * @var project_access_fetcher
	 */
	protected $access_fetcher;
	protected $link_row;
	protected $admin_row;
	protected $clean_project_cache = false;

	public function __construct($project_obj, $user_id)
	{
		$this->project_obj = $project_obj;
		parent::__construct($user_id);
	}

	protected function fill_data()
	{
		$this->access_maper = new project_access_maper();
		$this->access_fetcher = new project_access_fetcher($this->project_obj, $this->access_maper, $this->user_id, $lock = true);
		$this->link_row = $this->access_fetcher->get_link_row();
		$this->admin_row = $this->access_fetcher->get_admin_row();
		$this->level = $this->access_fetcher->get_level();
		return!$this->access_fetcher->error_occured();
	}

	public function join()
	{
		if ($this->project_obj->join_premoderation())
		{
			$this->add_pretender();
		}
		else
		{
			$this->add_member();
		}
		game_name_save_helper::update_game_name($this->project_obj, POST("game_name"));
	}

	public function unjoin()
	{
		// @dm9 check email here
		$this->delete_member();
	}

	public function add_pretender()
	{
		if ($this->is_pretender_strict() or $this->is_member())
		{
			return true;
		}
		$new_status = ACCESS_STATUS_PRETENDER;
		$this->db->sql("
			INSERT INTO user_project_link (project_id, user_id, add_time, edit_time, status)
			VALUES ('{$this->project_obj->get_id()}', '{$this->user_id}', NOW(), NOW(), '{$new_status}')
			ON DUPLICATE KEY UPDATE
				status = '{$new_status}',
				edit_time = NOW()
		");
		$this->update_number();
		$this->send_emails_to_admins_about_pretender();
		return true;
	}

	public function add_member()
	{
		if ($this->is_member())
		{
			return true;
		}
		$new_status = ACCESS_STATUS_MEMBER;
		$this->db->sql("
			INSERT INTO user_project_link (project_id, user_id, add_time, edit_time, status)
			VALUES ('{$this->project_obj->get_id()}', '{$this->user_id}', NOW(), NOW(), '{$new_status}')
			ON DUPLICATE KEY UPDATE
				status = '{$new_status}',
				edit_time = NOW()
		");
		$this->update_number();
		$this->update_user_count();
		$this->send_email_to_pretender(true);
		return true;
	}

	public function add_moderator()
	{
		if ($this->level > ACCESS_LEVEL_MODERATOR)
		{
			return false;
		}
		if ($this->level == ACCESS_LEVEL_MODERATOR)
		{
			return true;
		}
		$new_status = ACCESS_STATUS_MODERATOR;
		$adder_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		$this->db->sql("
			INSERT INTO user_project_link (project_id, user_id, add_time, edit_time, status)
			VALUES ('{$this->project_obj->get_id()}', '{$this->user_id}', NOW(), NOW(), '{$new_status}')
			ON DUPLICATE KEY UPDATE
				status = '{$new_status}',
				edit_time = NOW()
		");
		$this->db->sql("
			INSERT INTO project_admin (project_id, user_id, add_time, adder_admin_user_id, edit_time, editor_admin_user_id, is_admin)
			VALUES ('{$this->project_obj->get_id()}', '{$this->user_id}', NOW(), '{$adder_user_id}', NOW(), '{$adder_user_id}', 0)
			ON DUPLICATE KEY UPDATE
				edit_time = NOW(),
				editor_admin_user_id = {$adder_user_id},
				is_admin = 0
		");
		$this->update_number();
		$this->update_user_count();
		$this->send_email_to_pretender(true);
		return true;
	}

	public function add_admin()
	{
		if ($this->level > ACCESS_LEVEL_ADMIN)
		{
			return false;
		}
		if ($this->level == ACCESS_LEVEL_ADMIN)
		{
			return true;
		}
		$new_status = ACCESS_STATUS_ADMIN;
		$adder_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		$this->db->sql("
			INSERT INTO user_project_link (project_id, user_id, add_time, edit_time, status)
			VALUES ('{$this->project_obj->get_id()}', '{$this->user_id}', NOW(), NOW(), '{$new_status}')
			ON DUPLICATE KEY UPDATE
				status = '{$new_status}',
				edit_time = NOW()
		");
		$this->db->sql("
			INSERT INTO project_admin (project_id, user_id, add_time, adder_admin_user_id, edit_time, editor_admin_user_id, is_admin)
			VALUES ('{$this->project_obj->get_id()}', '{$this->user_id}', NOW(), '{$adder_user_id}', NOW(), '{$adder_user_id}', 1)
			ON DUPLICATE KEY UPDATE
				edit_time = NOW(),
				editor_admin_user_id = {$adder_user_id},
				is_admin = 1
		");
		$this->update_number();
		$this->update_user_count();
		$this->send_email_to_pretender(true);
		return true;
	}

	public function delete_member()
	{
		$new_status = ACCESS_STATUS_DELETED;
		$this->db->sql("
			UPDATE user_project_link
			SET
				status = '{$new_status}',
				edit_time = NOW()
			WHERE
				project_id = '{$this->project_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$this->update_user_count();
		$this->send_email_to_pretender(false);
		return true;
	}

	public function delete_moderator()
	{
		if ($this->level < ACCESS_LEVEL_MODERATOR)
		{
			return false;
		}
		$new_status = ACCESS_STATUS_MEMBER;
		$this->db->sql("
			UPDATE user_project_link
			SET
				status = '{$new_status}',
				edit_time = NOW()
			WHERE
				project_id = '{$this->project_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$editor_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		$this->db->sql("
			UPDATE project_admin
			SET
				is_admin = 0,
				edit_time = NOW(),
				editor_admin_user_id = {$editor_user_id}
			WHERE
				project_id = '{$this->project_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$this->update_user_count();
		return true;
	}

	public function delete_admin()
	{
		if ($this->level < ACCESS_LEVEL_ADMIN)
		{
			return false;
		}
		$new_status = ACCESS_STATUS_MEMBER;
		$this->db->sql("
			UPDATE user_project_link
			SET
				status = '{$new_status}',
				edit_time = NOW()
			WHERE
				project_id = '{$this->project_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$editor_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		$this->db->sql("
			UPDATE project_admin
			SET
				is_admin = 0,
				edit_time = NOW(),
				editor_admin_user_id = {$editor_user_id}
			WHERE
				project_id = '{$this->project_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$this->update_user_count();
		return true;
	}

	public function set_moderator_rights($rights_array)
	{
		$editor_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		foreach ($rights_array as $idx => $val)
		{
			if ($idx == "is_admin")
			{
				trigger_error("Incorrect set_moderator_rights() function usage ('is_admin' check)");
				return false;
			}
			if (!preg_match("/^is_[a-z0-9_]+_moderator$/i", $idx))
			{
				trigger_error("Incorrect set_moderator_rights() function usage (regexp check)");
				return false;
			}
			if ((string) $val !== "0" and (string) $val !== "1")
			{
				trigger_error("Incorrect set_moderator_rights() function usage (value check)");
				return false;
			}
			$rights_array["edit_time"] = "NOW()";
			$rights_array["editor_admin_user_id"] = $editor_user_id;
		}
		if (!$this->is_moderator_strict())
		{
			return false;
		}
		$this->db->update_by_array("project_admin", $rights_array, "project_id = {$this->project_obj->get_id()} AND user_id = {$this->user_id}");
		return true;
	}

	private function update_number()
	{
		$number = $this->db->get_value("
			SELECT `number`
			FROM user_project_link
			WHERE project_id = '{$this->project_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		if (!$number)
		{
			$new_number = $this->db->get_value("SELECT IFNULL(MAX(number), 0) + 1 FROM user_project_link WHERE project_id = '{$this->project_obj->get_id()}'");
			$this->db->sql("
				UPDATE user_project_link
				SET number = '{$new_number}'
				WHERE project_id = '{$this->project_obj->get_id()}' AND user_id = '{$this->user_id}'
			");
			$double_exists = $this->db->row_exists("
				SELECT * 
				FROM user_project_link 
				WHERE 
					project_id = '{$this->project_obj->get_id()}' 
					AND user_id != '{$this->user_id}'
					AND number = {$new_number}
			");
			if ($double_exists)
			{
				// OK, I could try to make update one more time, 
				// but I don't believe that it's necessary...
				trigger_error("Impossible is possible, user should rejoin");
			}
		}
	}

	private function update_user_count()
	{
		$this->db->sql("
			UPDATE project
			SET user_count_calc = (
				SELECT COUNT(*)
				FROM user_project_link l
				WHERE
					l.project_id = {$this->project_obj->get_id()}
					AND l.status <> 'pretender' AND l.status <> 'deleted'
			)
			WHERE id = {$this->project_obj->get_id()}
		");
		$this->clean_project_cache = true;
	}

	public function clean_cache()
	{
		project_user_cache_tag::init($this->project_obj->get_id(), $this->user_id)->update();
		if ($this->clean_project_cache)
		{
			project_cache_tag::init($this->project_obj->get_id())->update();
		}
	}

	private function send_emails_to_admins_about_pretender()
	{
		$admin_emails = $this->db->fetch_column_values("
			SELECT u.id, u.email
			FROM user_project_link l
			JOIN user u ON u.id = l.user_id
			WHERE
				l.project_id = {$this->project_obj->get_id()}
				AND l.status = 'admin'
				AND u.allow_email_on_premoderation = 1
		", "email", "id");

		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new user_short_xml_ctrl($this->user_id));
		$xml_loader->add_xml(new project_short_xml_ctrl($this->project_obj->get_id()));
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xslt("mail/project_admin_new_member", "project_admin");
		$xml_loader->run();
		$body = $xml_loader->get_content();

		foreach ($admin_emails as $admin_user_id => $admin_email)
		{
			mail_helper::add_letter(":AUTO:", $body, $admin_email, $admin_user_id);
		}
	}

	private function send_email_to_pretender($is_now_member)
	{
		if (!$this->is_pretender_strict() or $this->user_id == $this->user->get_user_id())
		{
			return;
		}

		$is_now_member = $is_now_member ? "1" : "0";

		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new user_short_xml_ctrl($this->user_id));
		$xml_loader->add_xml(new user_short_xml_ctrl($this->user->get_user_id()));
		$xml_loader->add_xml(new project_short_xml_ctrl($this->project_obj->get_id()));
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xml(new sys_params_xml_ctrl());
		$xml_loader->add_xml(new string_to_xml_xml_ctrl("<is_now_member>{$is_now_member}</is_now_member>"));
		$xml_loader->add_xslt("mail/project_pretender", "project");
		$xml_loader->run();
		$body = $xml_loader->get_content();

		$pretender_user = new user($this->user_id);
		mail_helper::add_letter(":AUTO:", $body, $pretender_user->get_email(), $this->user_id);
	}

}

?>