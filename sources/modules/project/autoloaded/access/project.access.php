<?php

class project_access extends base_access
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access_fetcher
	 */
	protected $access_fetcher;

	/**
	 * @var project_access_maper
	 */
	protected $access_maper;
	protected $link_row = false;
	protected $admin_row = false;

	public function __construct(project_obj $project_obj, $user_id = null)
	{
		$this->project_obj = $project_obj;
		parent::__construct($user_id);
	}
	
	protected function get_new_access_maper()
	{
		return new project_access_maper();
	}

	protected function fill_required_levels()
	{
		$this->required_statuses["read"] = $this->project_obj->get_param("access_read");
		$this->required_statuses["communities_read"] = $this->project_obj->get_param("access_communities_read");
		$this->required_statuses["communities_add"] = $this->project_obj->get_param("access_communities_add");
		$this->required_statuses["events_read"] = $this->project_obj->get_param("access_events_read");
		$this->required_statuses["events_comment"] = $this->project_obj->get_param("access_events_comment");
		$this->required_statuses["materials_read"] = $this->project_obj->get_param("access_materials_read");
		$this->required_statuses["materials_comment"] = $this->project_obj->get_param("access_materials_comment");
		$this->required_statuses["precedents_read"] = $this->project_obj->get_param("access_precedents_read");
		$this->required_statuses["precedents_comment"] = $this->project_obj->get_param("access_precedents_comment");

		$this->required_levels = project_access_settings_cache::init($this->project_obj->get_id())->get();
		if (!$this->required_levels)
		{
			$this->required_levels = array();
			foreach ($this->required_statuses as $name => $status)
			{
				$this->required_levels[$name] = $this->access_maper->get_level_from_status($status);
			}
			project_access_settings_cache::init($this->project_obj->get_id())->set($this->required_levels);
		}
	}

	protected function fill_data()
	{
		if (!$this->user_id)
		{
			$this->level = ACCESS_LEVEL_GUEST;
			return;
		}
		$cache = project_access_cache::init($this->project_obj->get_id(), $this->user_id);
		$data = $cache->get();
		if ($data)
		{
			$this->level = $data["level"];
			$this->admin_row = $data["admin_row"];
			return;
		}
		$this->access_fetcher = new project_access_fetcher($this->project_obj, $this->access_maper, $this->user_id);
		$this->level = $this->access_fetcher->get_level();
		if (!$this->access_fetcher->error_occured())
		{
			$this->link_row = $this->access_fetcher->get_link_row();
			$this->admin_row = $this->access_fetcher->get_admin_row();
			$data = array(
				"level" => $this->level,
				"admin_row" => $this->admin_row,
			);
			$cache->set($data);
		}
	}

	public function join_premoderation()
	{
		return $this->project_obj->join_premoderation();
	}

	public function can_read_project()
	{
		return $this->user->is_admin() || $this->level >= $this->required_levels["read"];
	}

	public function can_moderate_widgets()
	{
		return $this->can_read_project() && ($this->user->is_admin() || $this->has_admin_rights() || $this->is_moderator_strict() && $this->admin_row["is_widget_moderator"]);
	}

	public function can_moderate_roles()
	{
		return $this->can_read_project() && $this->project_obj->role_recommendation_is_on() && ($this->user->is_admin() || $this->has_admin_rights() || $this->is_moderator_strict() && $this->admin_row["is_role_moderator"]);
	}

	public function can_read_communities()
	{
		$required_level = $this->required_levels["communities_read"];
		return $this->can_read_project() && $this->project_obj->communities_are_on() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_SPECIAL_MODERATOR && $this->is_moderator_strict() && $this->admin_row["is_community_moderator"]);
	}

	public function can_add_communities()
	{
		$required_level = $this->required_levels["communities_add"];
		return $this->can_read_communities() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_SPECIAL_MODERATOR && $this->is_moderator_strict() && $this->admin_row["is_community_moderator"]);
	}

	public function can_moderate_communities()
	{
		return $this->can_read_communities() && ($this->user->is_admin() || $this->has_admin_rights() || $this->is_moderator_strict() && $this->admin_row["is_community_moderator"]);
	}

	public function can_read_events()
	{
		$required_level = $this->required_levels["events_read"];
		return $this->can_read_project() && $this->project_obj->events_are_on() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_SPECIAL_MODERATOR && $this->is_moderator_strict() && $this->admin_row["is_event_moderator"]);
	}

	public function can_comment_events()
	{
		$required_level = $this->required_levels["events_comment"];
		return $this->can_read_events() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_SPECIAL_MODERATOR && $this->is_moderator_strict() && $this->admin_row["is_event_moderator"]);
	}

	public function can_moderate_events()
	{
		return $this->can_read_events() && ($this->user->is_admin() || $this->has_admin_rights() || $this->is_moderator_strict() && $this->admin_row["is_event_moderator"]);
	}

	public function can_read_materials()
	{
		$required_level = $this->required_levels["materials_read"];
		return $this->can_read_project() && $this->project_obj->materials_are_on() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_SPECIAL_MODERATOR && $this->is_moderator_strict() && $this->admin_row["is_material_moderator"]);
	}

	public function can_comment_materials()
	{
		$required_level = $this->required_levels["materials_comment"];
		return $this->can_read_materials() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_SPECIAL_MODERATOR && $this->is_moderator_strict() && $this->admin_row["is_material_moderator"]);
	}

	public function can_moderate_materials()
	{
		return $this->can_read_materials() && ($this->user->is_admin() || $this->has_admin_rights() || $this->is_moderator_strict() && $this->admin_row["is_material_moderator"]);
	}

	public function can_read_precedents()
	{
		$required_level = $this->required_levels["precedents_read"];
		return $this->can_read_project() && $this->project_obj->marks_are_on() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_SPECIAL_MODERATOR && $this->is_moderator_strict() && $this->admin_row["is_precedent_moderator"]);
	}

	public function can_comment_precedents()
	{
		$required_level = $this->required_levels["precedents_comment"];
		return $this->can_read_precedents() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_SPECIAL_MODERATOR && $this->is_moderator_strict() && $this->admin_row["is_precedent_moderator"]);
	}

	public function can_moderate_precedents()
	{
		return $this->can_read_precedents() && ($this->user->is_admin() || $this->has_admin_rights() || $this->is_moderator_strict() && $this->admin_row["is_precedent_moderator"]);
	}

	public function can_moderate_marks()
	{
		return $this->can_moderate_precedents() && ($this->user->is_admin() || $this->has_admin_rights() || $this->is_moderator_strict() && $this->admin_row["is_mark_moderator"]);
	}
	
	public function has_member_rights()
	{
		return $this->can_read_project() && ($this->user->is_admin() || $this->level >= ACCESS_LEVEL_MEMBER);
	}

	public function has_moderator_rights()
	{
		return $this->can_read_project() && ($this->user->is_admin() || $this->level >= ACCESS_LEVEL_MODERATOR);
	}

	public function get_project_id()
	{
		return $this->project_obj->get_id();
	}

}

?>