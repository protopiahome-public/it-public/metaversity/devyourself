<?php

class project_access_fetcher extends base_access_fetcher
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $link_row = false;
	protected $admin_row = false;

	public function __construct(project_obj $project_obj, project_access_maper $access_maper, $user_id = null, $lock = false)
	{
		$this->project_obj = $project_obj;
		parent::__construct($access_maper, $user_id, $lock);
	}

	protected function fetch_data()
	{
		$for_update_sql = $this->lock ? "FOR UPDATE" : "";

		if (!$this->project_obj->exists())
		{
			$this->level = ACCESS_LEVEL_ERROR;
			$this->error("Project does not exist: '{$this->project_obj->get_id()}'");
			return;
		}

		$this->link_row = $this->db->get_row("
			SELECT *
			FROM user_project_link
			WHERE project_id = {$this->project_obj->get_id()} AND user_id = {$this->user_id}
			{$for_update_sql}
		");
		if (!$this->link_row)
		{
			$this->level = ACCESS_LEVEL_USER;
		}
		else
		{
			$status = $this->link_row["status"];
			if (!$this->access_maper->status_exists($status))
			{
				$this->error("Unknown status is used in DB: '{$status}'");
				$this->level = ACCESS_LEVEL_USER;
			}
			else
			{
				$this->level = $this->access_maper->get_level_from_status($status);
				if ($this->level > ACCESS_LEVEL_MEMBER)
				{
					$this->admin_row = $this->db->get_row("
						SELECT *
						FROM project_admin
						WHERE project_id = {$this->project_obj->get_id()} AND user_id = {$this->user_id}
						{$for_update_sql}
					");
					if (!$this->admin_row)
					{
						$this->error("DB inconsistency: level = '{$this->level}' but admin_row was not found");
						$this->level = ACCESS_LEVEL_MEMBER;
					}
					elseif ($this->level >= ACCESS_LEVEL_ADMIN and $this->admin_row["is_admin"] != "1")
					{
						$this->error("DB inconsistency: level = '{$this->level}' but admin_row[is_admin] != 1");
						$this->level = ACCESS_LEVEL_MEMBER;
					}
				}
			}
		}
	}

	protected function error($error_msg)
	{
		$log_message = $error_msg . " (project_id = {$this->project_obj->get_id()}, user_id = {$this->user_id})";
		file_put_contents(PATH_LOG . "/project_access.log", $log_message . "\n", FILE_APPEND);
		parent::error($error_msg);
	}

	public function get_link_row()
	{
		return $this->link_row;
	}

	public function get_admin_row()
	{
		return $this->admin_row;
	}

}

?>