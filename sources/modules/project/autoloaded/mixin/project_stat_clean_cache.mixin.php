<?php

class project_stat_clean_cache_mixin extends base_mixin
{

	protected $project_id;

	public function clean_cache()
	{
		project_stat_cache_tag::init($this->project_id)->update();
	}

}

?>