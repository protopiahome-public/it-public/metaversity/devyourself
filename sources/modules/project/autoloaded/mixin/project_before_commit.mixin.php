<?php

class project_before_commit_mixin extends base_mixin
{

	protected $project_id;
	protected $update_array;

	public function on_before_commit()
	{
		$this->update_array["project_id"] = $this->project_id;
		return true;
	}

}

?>