<?php

class project_before_start_mixin extends base_mixin
{

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function on_before_start()
	{
		$this->project_id = $this->get_project_id();
		if (!is_good_id($this->project_id))
		{
			return false;
		}
		$this->project_obj = project_obj::instance($this->project_id, $lock = true);
		if (!$this->project_obj->exists())
		{
			return false;
		}

		return true;
	}

	protected function get_project_id()
	{
		return REQUEST("project_id");
	}

}

?>