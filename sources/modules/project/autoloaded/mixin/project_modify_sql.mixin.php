<?php

class project_modify_sql_mixin extends base_mixin
{

	protected $project_id;

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>