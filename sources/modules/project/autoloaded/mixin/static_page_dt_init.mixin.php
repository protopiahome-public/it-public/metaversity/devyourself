<?php

class static_page_dt_init_mixin extends base_mixin
{

	/**
	 * @var static_page_dt
	 */
	protected $dt;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	protected function on_after_dt_init()
	{
		$this->dt->set_project_obj($this->project_obj);
		return true;
	}

}

?>