<?php

class taxonomy_folder_modify_sql_mixin extends base_mixin
{

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("type = 'folder'");
	}

}

?>