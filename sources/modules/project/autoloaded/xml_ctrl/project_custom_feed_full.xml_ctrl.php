<?php

class project_custom_feed_full_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "project_custom_feed";
	protected $axis_name = "full";

	/**
	 * @var community_dt
	 */
	protected $dt;
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($feed_id, $project_id)
	{
		$this->project_id = $project_id;
		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct($feed_id);
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_project_obj($this->project_obj, $use_project_url_placeholder = true);
		return true;
	}

	protected function get_cache()
	{
		return project_custom_feed_full_cache::init($this->id, $this->project_obj->get_id());
	}

	protected function postprocess()
	{
		$this->xml = preg_replace("/%PROJECT_URL%/", $this->project_obj->get_url(), $this->xml, 1);
	}

}

?>