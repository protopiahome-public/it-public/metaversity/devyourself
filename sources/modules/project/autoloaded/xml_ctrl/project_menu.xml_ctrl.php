<?php

require_once PATH_INTCMF . "/tree.php";
require_once PATH_INTCMF . "/xml_builder.php";

class project_menu_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_attrs = array("project_id");
	// Internal
	protected $project_id;

	/**
	 *
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 *
	 * @var project_access
	 */
	protected $project_access;

	/**
	 * @var tree
	 */
	protected $tree;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct();
	}

	protected function get_cache()
	{
		return project_menu_cache::init($this->project_obj->get_id());
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$data_raw_plain = $this->db->fetch_all("
			SELECT *
			FROM project_taxonomy_item
			WHERE project_id = {$this->project_obj->get_id()}
			ORDER BY position
		");
		$this->data = $data_raw_plain;

		array_unshift($data_raw_plain, array(
			"id" => "0",
			"title" => "Главная",
			"type" => "module",
			"module_name" => "main",
			"show_in_menu" => "1",
			"parent_id" => "",
		));
		foreach ($data_raw_plain as &$item)
		{
			if ($item["module_name"] == "marks")
			{
				$item["module_front_url"] = "precedents/";
				$item["module_front_section"] = "precedents";
			}
			elseif ($item["module_name"] == "communities")
			{
				$item["module_front_url"] = "co/";
				$item["module_front_section"] = $item["module_name"];
			}
			elseif ($item["module_name"] == "members")
			{
				$item["module_front_url"] = "users/";
				$item["module_front_section"] = "users";
			}
			elseif ($item["module_name"] == "main")
			{
				$item["module_front_url"] = "";
				$item["module_front_section"] = $item["module_name"];
			}
			else
			{
				$item["module_front_url"] = $item["module_name"] . "/";
				$item["module_front_section"] = $item["module_name"];
			}
		}

		$this->tree = new tree($data_raw_plain);
		$this->tree->enable_parent_id_data();
		$this->tree->make_tree();
	}

	protected function fill_xml(xdom $xdom)
	{
		xml_builder::tree($xdom, $this->tree, "item");
	}

}

?>