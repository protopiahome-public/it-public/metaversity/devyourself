<?php

class project_intmenu_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "link";
	protected $xml_attrs = array("project_id", "project_title");
	// Module specific
	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $project_id;
	protected $project_title;
	protected $xml_output;

	public function __construct($project_id, $xml_output = false)
	{
		$this->project_id = $project_id;
		$this->xml_output = $xml_output;
		
		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct();
	}

	protected function get_cache()
	{
		return project_intmenu_cache::init($this->project_id);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->project_title = $this->project_obj->get_title();
		$select_sql->add_from("project_intmenu_item dt");
		$select_sql->add_select_fields("dt.url, dt.title");
		$select_sql->add_where("dt.project_id = {$this->project_id}");
		$select_sql->add_order("dt.position");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	public function get_xml()
	{
		$xml = parent::get_xml();
		if ($this->xml_output)
		{
			response::set_content_type_xml();
			response::set_content($xml);
		}
		return $xml;
	}

}

?>