<?php

class static_page_full_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "static_page";
	protected $axis_name = "full";
	// Internal
	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $load_block_set = false;

	/**
	 * @var static_page_dt
	 */
	protected $dt;

	// @dm9 CR params and constructor
	public function __construct($static_page_id, $project_id, $load_block_set = true)
	{
		parent::__construct($static_page_id);
		$this->load_block_set = $load_block_set;
		$this->project_obj = project_obj::instance($project_id);
	}

	protected function get_cache()
	{
		return static_page_full_cache::init($this->id);
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_project_obj($this->project_obj, $use_project_url_placeholder = true);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("project_id");
		$select_sql->add_select_fields("block_set_id");
		$select_sql->add_where("type = 'static_page' AND project_id = {$this->project_obj->get_id()}");
	}

	protected function process_data()
	{
		if ($this->data)
		{
			$this->store_data = array(
				"block_set_id" => $this->data[0]["block_set_id"],
				"project_id" => $this->data[0]["project_id"],
			);
		}
	}

	protected function modify_xml(xdom $xdom)
	{
		$xdom->set_attr("project_id", $this->data[0]["project_id"]);
		$xdom->set_attr("block_set_id", $this->data[0]["block_set_id"]);
	}

	protected function postprocess()
	{
		if ($this->store_data)
		{
			if ($this->load_block_set)
			{
				$this->xml_loader->add_xml_by_class_name("block_set_xml_ctrl", $this->store_data["block_set_id"]);
			}
		}

		// @todo escape 2nd param?
		$this->xml = preg_replace("/%PROJECT_URL%/", $this->project_obj->get_url(), $this->xml, 1);
	}

}

?>