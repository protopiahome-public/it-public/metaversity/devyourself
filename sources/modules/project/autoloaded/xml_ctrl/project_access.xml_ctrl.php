<?php

class project_access_xml_ctrl extends base_xml_ctrl
{

	/**
	 * @var project_access
	 */
	private $project_access;

	public function __construct(project_access $project_access)
	{
		$this->project_access = $project_access;
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		$attrs = array(
			"project_id" => $this->project_access->get_project_id(),
			"status" => $this->project_access->get_status(),
			"is_member" => $this->project_access->is_member(),
			"is_pretender" => $this->project_access->is_pretender_strict(),
			"has_member_rights" => $this->project_access->has_member_rights(),
			"has_moderator_rights" => $this->project_access->has_moderator_rights(),
			"has_admin_rights" => $this->project_access->has_admin_rights(),
			"join_premoderation" => $this->project_access->join_premoderation(),
			"can_add_communities" => $this->project_access->can_add_communities(),
			"can_moderate_widgets" => $this->project_access->can_moderate_widgets(),
			"can_moderate_events" => $this->project_access->can_moderate_events(),
			"can_moderate_materials" => $this->project_access->can_moderate_materials(),
			"can_moderate_precedents" => $this->project_access->can_moderate_precedents(),
			"can_comment_events" => $this->project_access->can_comment_events(),
			"can_comment_materials" => $this->project_access->can_comment_materials(),
		);
		foreach ($this->project_access->get_required_statuses() as $access_name => $status)
		{
			$attrs["access_" . $access_name] = $status;
		}

		return $this->get_node_string("project_access", $attrs);
	}

}

?>