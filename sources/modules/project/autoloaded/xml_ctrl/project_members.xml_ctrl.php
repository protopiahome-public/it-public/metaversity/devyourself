<?php
class project_members_xml_ctrl extends base_xml_ctrl
{
	protected $project_id;
	
	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}
	
	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		$xdom = xdom::create("project_members");
		
		$project_members = $this->db->fetch_all("
			SELECT
				user.id,
				user.login,
				CONCAT(user.first_name, ' ', last_name) AS full_name,
				upl.game_name,
				upl.number,
				user.photo_small_width,
				user.photo_small_height
			FROM
				user_project_link AS upl,
				user
			WHERE
				upl.project_id = {$this->project_id}
				AND user.id = upl.user_id
			ORDER BY user.id
		", "id");
		
		foreach($project_members as $user_id => $user_data)
		{
			if ($user_data["photo_small_width"])
			{
				$user_data["photo_small_url"] = $this->request->get_prefix() . "/data/user/small/{$user_id}.jpeg";
			}
			else
			{
				$user_data["photo_small_width"] = 24;
				$user_data["photo_small_height"] = 24;
				$user_data["photo_small_url"] = $this->request->get_prefix() . "/img/defaults/user-small-default.jpg";
			}
			$xdom->create_child_node("user")
				->set_attr("id", $user_id)
				->set_attr("login", $user_data["login"])
				->set_attr("full_name", strlen($user_data["full_name"]) > 1 ? $user_data["full_name"] : "<без имени>")
				->set_attr("game_name", $user_data["game_name"])
				->set_attr("number", $user_data["number"])
				->create_child_node("photo_small")
					->set_attr("width", $user_data["photo_small_width"])
					->set_attr("height", $user_data["photo_small_height"])
					->set_attr("url", $user_data["photo_small_url"]);
		}
		
		return $xdom->get_xml(true);
	}

}

?>
