<?php

abstract class base_project_user_stat_choice_xml_ctrl extends base_xml_ctrl
{

	// Settings
	protected $resource_names = array(
		"event" => array(
			"title" => "События",
			"not_finished_title" => "Запланированные события",
			"finished_title" => "Посещённые события",
			"empty_note" => "",
			"check_on" => "events_are_on",
		),
		"material" => array(
			"title" => "Материалы",
			"not_finished_title" => "Запланированные к изучению",
			"finished_title" => "Изученные",
			"empty_note" => "",
			"check_on" => "materials_are_on",
		),
	);
	protected $total_width = 400;
	protected $total_height = 400;
	protected $min_block_width = 20;
	protected $max_block_width = 40;
	protected $min_block_height = 2;
	protected $max_block_height = 18;
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $page_type; // "focus" | "all" | null
	protected $user_id;
	protected $competence_set_id;

	public function __construct($project_id, $page_type, $user_id = null)
	{
		$this->project_id = $project_id;
		$this->page_type = $page_type;
		$this->user_id = $user_id;

		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		/* Check that vector is on */
		$vector_config = vector_info_math::get_project_vector_config($this->project_id);
		if (!$vector_config)
		{
			$this->set_error_404();
			return false;
		}
		$this->competence_set_id = $vector_config["vector_competence_set_id"];

		/* Check user rights */
		if (is_null($this->user_id))
		{
			$this->user_id = $this->user->get_user_id();
		}
		if (!$this->user_id)
		{
			$this->set_error_403();
			return;
		}

		if ($this->page_type != "all")
		{
			/* Init vector */
			$vector_data = null;
			$focus_data = array();
			$vector_info_struct = vector_info_math::get_project_vector_info($this->user_id, $this->project_id);
			if ($vector_info_struct)
			{
				$vector_data = new vector_data_math($vector_info_struct["id"], $this->user_id, $vector_info_struct["competence_set_id"]);
				$focus_data = $vector_data->get_focus_data();
			}

			if (!$this->page_type)
			{
				$this->set_redirect_url($this->request->get_full_prefix() . $this->request->get_folders_string() . (sizeof($focus_data) ? "/focus/" : "/all/"));
				return "";
			}
		}

		$xdom = xdom::create($this->name);
		$xdom->set_attr("page_type", $this->page_type);
		$xdom->set_attr("project_id", $this->project_id);
		$xdom->set_attr("user_id", $this->user_id);

		if ($this->page_type == "focus" and !sizeof($focus_data))
		{
			$xdom->set_attr("no_focus", "1");
			return $xdom->get_xml(true);
		}

		foreach ($this->resource_names as $resource_name => $resource_info)
		{
			if ($this->project_obj->get_param($resource_info["check_on"]) == 1)
			{
				$this->fetch_data($xdom, $resource_name, $resource_info, $this->page_type == "focus" ? array_keys($focus_data) : null);
			}
		}

		$this->xml_loader->add_xml_by_class_name("competence_set_competences_xml_ctrl", $this->competence_set_id);
		//$this->xml_loader->add_xml(new vector_config_xml_ctrl($vector_config));

		return $xdom->get_xml(true);
	}

	private function fetch_data(xnode $parent_node, $resource_name, $resource_info, $competences = null)
	{

		if (!is_null($competences) and !sizeof($competences))
		{
			$raw_data = array();
		}
		else
		{
			$competences_restriction = is_null($competences) ? "" : "AND rcl.competence_id IN (" . join(",", $competences) . ")";
			$raw_data = $this->db->fetch_all("
				SELECT 
					ul.{$resource_name}_id as resource_id, rcl.competence_id,
					IF(ul.after_status = 1, 1, 0) as finished
				FROM {$resource_name} r
				LEFT JOIN {$resource_name} rr ON rr.id = r.reflex_id
				JOIN {$resource_name}_user_link ul ON ul.{$resource_name}_id = IFNULL(rr.id, r.id)
				JOIN {$resource_name}_rate_link rl ON rl.{$resource_name}_id = ul.{$resource_name}_id AND rl.competence_set_id = {$this->competence_set_id}
				JOIN rate_competence_link rcl ON rcl.rate_id = rl.rate_id
				WHERE
					r.project_id = {$this->project_id}
					AND ul.user_id = {$this->user_id}
					AND (ul.before_status > 0 OR ul.after_status = 1)
					{$competences_restriction}
				ORDER BY finished, ul.last_status_time DESC
			");
		}
		$competences_data = array();
		foreach ($raw_data as $row)
		{
			if (!isset($competences_data[$row["competence_id"]]))
			{
				$competences_data[$row["competence_id"]] = array();
			}
			$competences_data[$row["competence_id"]][] = array(
				"resource_id" => $row["resource_id"],
				"finished" => $row["finished"],
			);
		}
		if (!is_null($competences))
		{
			foreach ($competences as $competence_id)
			{
				if (!isset($competences_data[$competence_id]))
				{
					$competences_data[$competence_id] = array();
				}
			}
		}
		$stat_node = $parent_node->create_child_node("stat");
		$stat_node->set_attr("name", $resource_name);
		foreach ($resource_info as $info_key => $info_value)
		{
			$stat_node->set_attr($info_key, $info_value);
		}
		$competence_count = sizeof($competences_data);
		$max_resource_count = 0;
		foreach ($competences_data as $competence_id => $resources_data)
		{
			$competence_node = $stat_node->create_child_node("competence");
			$competence_node->set_attr("id", $competence_id);
			$resource_count = sizeof($resources_data);
			if ($resource_count > $max_resource_count)
			{
				$max_resource_count = $resource_count;
			}
			foreach ($resources_data as $resource_data)
			{
				$resource_node = $competence_node->create_child_node("resource");
				$resource_node->set_attr("id", $resource_data["resource_id"]);
				$resource_node->set_attr("finished", $resource_data["finished"]);
				$this->xml_loader->add_xml_by_class_name("{$resource_name}_short_xml_ctrl", $resource_data["resource_id"], $this->project_id);
			}
		}
		if ($competence_count and $max_resource_count)
		{
			$block_width = round($this->total_width / $competence_count);
			$block_height = round($this->total_height / $max_resource_count);
			$stat_node->set_attr("block_width", value_between($block_width, $this->min_block_width, $this->max_block_width));
			$stat_node->set_attr("block_height", value_between($block_height, $this->min_block_height, $this->max_block_height));
		}
	}

}

?>