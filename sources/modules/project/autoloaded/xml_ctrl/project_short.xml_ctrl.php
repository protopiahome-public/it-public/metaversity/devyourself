<?php

class project_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "vector_competence_set_id",
			"ctrl" => "competence_set_short",
		),
		array(
			"column" => "marks_competence_set_id",
			"ctrl" => "competence_set_short",
		),
	);
	protected $dt_name = "project";
	protected $axis_name = "short";
	// Internal
	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($project_id)
	{
		$this->project_obj = project_obj::instance($project_id);

		parent::__construct($project_id);
	}

	protected function get_cache()
	{
		return project_short_cache::init($this->id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("dt.domain_is_on");
		$select_sql->add_select_fields("dt.domain_name");
	}

	protected function load_data(select_sql $select_sql = null)
	{
		parent::load_data($select_sql);
		$this->store_data = array(
			"start_time" => $this->db->parse_datetime($this->data[0]["start_time"]),
			"finish_time" => substr($this->data[0]["finish_time"], 0, 5) === "0000-" ? 0 : $this->db->parse_datetime($this->data[0]["finish_time"]),
			"full_url" => $this->project_obj->get_url(),
		);
	}

	protected function modify_xml(xdom $xdom)
	{
		$xdom->set_attr("url", "%PROJECT_URL%");
	}

	protected function get_xdom()
	{
		$xdom = parent::get_xdom();
		$xdom->set_attr("state_calc", "%STATE%");
		return $xdom;
	}

	protected function postprocess()
	{
		$now = time();
		$start = $this->store_data["start_time"];
		$finish = $this->store_data["finish_time"];
		$state = $now < $start ? "NOT_ST" : (!$finish || $now < $finish ? "RUNNING" : "FINISHED");
		$this->xml = preg_replace("/%STATE%/", $state, $this->xml, 1);

		// @todo escape 2nd param?
		$this->xml = preg_replace("/%PROJECT_URL%/", $this->project_obj->get_url(), $this->xml, 1);
	}

}

?>