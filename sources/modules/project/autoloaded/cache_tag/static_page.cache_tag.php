<?php

class static_page_cache_tag extends base_cache_tag
{

	public static function init($static_page_id)
	{
		return parent::get_tag(__CLASS__, $static_page_id);
	}

}
?>