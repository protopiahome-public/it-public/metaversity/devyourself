<?php

class project_settings_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	
	public function check_rights()
	{
		return $this->project_obj->get_access()->is_member() || $this->project_obj->get_access()->is_pretender_strict();
	}

	public function commit()
	{
		game_name_save_helper::update_game_name($this->project_obj, POST("game_name"));
		return true;
	}
	
	public function on_after_commit()
	{
		// @dm9 почему под это у нас нет метода?
		$this->db->sql("REPLACE INTO api_user_update SET project_id={$this->project_id}, user_id={$this->user->get_user_id()}");
	}

	public function clean_cache()
	{
		project_user_cache_tag::init($this->project_id, $this->user->get_user_id())->update();
	}

}

?>