<?php

class project_join_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_stat_clean_cache",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	private $join;

	/**
	 * @var project_access_save
	 */
	private $project_access_save;

	public function set_up()
	{
		/* cancel button */
		if (POST("cancel") !== null)
		{
			if (($retpath = POST("retpath")))
			{
				$retpath .= strpos($retpath, "?") === false ? "?cancel=1" : "&cancel=1";
			}
			else
			{
				$retpath = $this->request->get_prefix() . "/";
			}
			$this->set_redirect_url($retpath);
			$this->save_loader->stop_load();
		}
		return true;
	}

	public function check_rights()
	{
		return $this->user->get_user_id() > 0;
	}

	public function start()
	{
		/* join param check */
		$this->join = POST("join");
		if ($this->join !== "1" and $this->join !== "0")
		{
			return false;
		}

		return true;
	}

	public function commit()
	{
		/* access_save class initialization */
		$this->project_access_save = new project_access_save($this->project_obj, $this->user->get_user_id());

		if ($this->join)
		{
			$this->project_access_save->join();
		}
		else
		{
			$this->project_access_save->unjoin();
		}
		return true;
	}

	public function on_after_commit()
	{
		// @dm9 почему под это у нас нет метода?
		$this->db->sql("REPLACE INTO api_user_update SET project_id={$this->project_id}, user_id={$this->user->get_user_id()}");
	}

	public function clean_cache()
	{
		$this->project_access_save->clean_cache();
	}

}

?>