<?php

final class project_taxonomy extends base_taxonomy
{

	protected $project_id;
	protected $is_direct;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, $project_id, $is_direct)
	{
		$this->project_id = $project_id;
		$this->is_direct = $is_direct;
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		$p = $this->get_parts_relative();

		//@xxx project_id, project_obj --> $this->...
		//@xxx see also other taxonomy files

		$project_id = $this->project_id;
		$project_obj = project_obj::instance($project_id);
		$project_access = $project_obj->get_access();

		if ($this->domain_logic->is_on() and !$this->is_direct)
		{
			$folders = substr($this->request->get_all_folders(), strlen("p/{$project_obj->get_name()}/"));
			$url = $project_obj->get_url() . $folders . $this->request->get_params_string();
			$this->xml_loader->set_redirect_url($url);
			$this->xml_loader->set_redirect_type_permanent();
			return;
		}

		if ($this->is_direct)
		{
			if ($p[1] === "auth" and $p[2] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new project_auth_xml_page());
				return;
			}
			elseif (!$project_obj->domain_is_on() and isset($_COOKIE["logged_in"]) and $_COOKIE["logged_in"] and !$this->user->get_user_id()) // project on subdomain
			{
				$this->xml_loader->set_redirect_url("{$this->request->get_protocol()}://{$this->domain_logic->get_main_host_name()}/auth/?retpath=" . urlencode($this->request->get_full_request_uri()));
				return;
			}
		}

		$this->xml_loader->add_xml(new project_access_xml_ctrl($project_access));
		$this->xml_loader->add_xml_by_class_name("project_short_xml_ctrl", $project_id);

		$this->xml_loader->add_xml(new project_menu_xml_ctrl($project_id));

		if (!$project_access->can_read_project())
		{
			$this->xml_loader->set_error_403();
			$this->xml_loader->set_error_403_xslt("project_403", "project");
		}
		elseif ($p[1] === null)
		{
			//p/<project_name>/
			$this->xml_loader->add_xml_with_xslt(new project_widgets_xml_page($project_id));
		}
		elseif (($p[1] === "settings" or $p[1] === "unjoin") and $p[2] === null)
		{
			if ($p[1] === "settings")
			{
				$this->xml_loader->add_xml_with_xslt(new project_settings_xml_page($project_id), null, "project_settings_403");
			}
			else
			{
				$this->xml_loader->add_xml_with_xslt(new project_unjoin_xml_page($project_id), null, "project_settings_403");
			}
			if ($this->user->get_user_id())
			{
				$this->xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $this->user->get_user_id(), $project_id);
			}
		}
		elseif ($p[1] === "profile" and $p[2] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new project_profile_xml_page($project_id));
		}
		elseif ($p[1] === "roles")
		{
			$roles_taxonomy = new roles_taxonomy($this->xml_loader, $this, 1, $project_obj);
			$roles_taxonomy->run();
		}
		elseif ($p[1] === "precedents")
		{
			$precedents_taxonomy = new precedents_taxonomy($this->xml_loader, $this, 1, $project_obj);
			$precedents_taxonomy->run();
		}
		elseif ($p[1] === "vector")
		{
			$vectors_taxonomy = new vectors_taxonomy($this->xml_loader, $this, 1, $project_obj);
			$vectors_taxonomy->run();
		}
		elseif ($p[1] === "users" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//p/<project_name>/users/[page-<page>/]
			$this->xml_loader->add_xml_with_xslt(new project_users_xml_page($page, $project_id));
		}
		elseif ($p[1] === "users" and $p[2] !== false and $user_id = $this->get_user_id_by_login($p[2]))
		{
			//p/<project_name>/users/<login>/...
			$this->xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $user_id, $project_id);
			$current_user_project_access = $project_obj->get_access($user_id);

			if (!$current_user_project_access->is_member())
			{
				$this->xml_loader->set_error_404();
				$this->xml_loader->set_error_404_xslt("project_user_404", "project");
			}
			elseif ($p[3] === null)
			{
				//p/<project_name>/users/<login>/
				$this->xml_loader->add_xml_with_xslt(new project_user_profile_xml_page($project_id, $user_id));
				$this->xml_loader->add_xml(new user_profile_xml_page($user_id));
			}
			elseif ($p[3] === "vector" and $p[4] === null)
			{
				//p/<project_name>/users/<login>/vector/
				$this->xml_loader->add_xml(new project_user_vector_xml_page($project_id, $user_id, "rose"));
				$this->xml_loader->add_xslt("project_user_vector_rose", "vectors");
				$this->xml_loader->set_error_404_xslt("project_user_vector_404", "vectors");
				$this->xml_loader->add_xml(new project_user_profile_xml_page($project_id, $user_id));
			}
			elseif ($p[3] === "vector" and $p[4] === "detailed" and $p[5] === null)
			{
				//p/<project_name>/users/<login>/vector/detailed/
				$this->xml_loader->add_xml(new project_user_vector_xml_page($project_id, $user_id, "detailed"));
				$this->xml_loader->add_xslt("project_user_vector_detailed", "vectors");
				$this->xml_loader->set_error_404_xslt("project_user_vector_404", "vectors");
				$this->xml_loader->add_xml(new project_user_profile_xml_page($project_id, $user_id));
			}
			elseif ($p[3] === "stat-choice" and ($p[4] === null or $p[4] === "focus" or $p[4] === "all") and $p[5] === null)
			{
				//p/<project_name>/users/<login>/stat-choice/
				$this->xml_loader->add_xml_with_xslt(new project_user_stat_choice_xml_page($project_id, $p[4], $user_id), "project_vector_404");
			}
		}
		elseif ($p[1] === "events" or $p[1] === "materials")
		{
			$resources_taxonomy = new resources_taxonomy($this->xml_loader, $this, 0, $project_obj);
			$resources_taxonomy->run();
		}
		elseif ($p[1] === "co")
		{
			// @xxx split and rename
			$communities_taxonomy = new communities_taxonomy($this->xml_loader, $this, 0, $project_obj);
			$communities_taxonomy->run();
		}
		elseif ($p[1] === "admin")
		{
			$project_admin_taxonomy = new project_admin_taxonomy($this->xml_loader, $this, 1, $project_obj);
			$project_admin_taxonomy->run();
		}
		elseif ($p[1] === "intmenu" and $p[2] === null)
		{
			//p/<project_name>/intmenu/
			$this->xml_loader->add_xml(new project_intmenu_xml_page($project_id, true));
		}
		elseif ($p[1] === "robots.txt" and $p[2] === null)
		{
			//p/<project_name>/robots.txt
			$this->xml_loader->add_xml(new project_robots_txt_xml_page($project_id));
		}
		elseif (!$this->xml_loader->is_xml_page_loaded() and $static_page_id = $this->get_static_page_id_by_name($p[1]))
		{
			if ($p[2] === null)
			{
				//p/<project_name>/<name>/
				$this->xml_loader->add_xml_with_xslt(new project_static_page_xml_page($static_page_id, $project_id));
			}
			elseif ($file_id = $this->is_type_folder($p[2], "file", true) and $p[3] === null)
			{
				//p/<project_name>/<name>/file-<id>/
				$this->xml_loader->add_xml_with_xslt(new project_static_page_file_download_xml_page($file_id, $project_id, $static_page_id));
			}
		}
	}

	private function get_user_id_by_login($login)
	{
		return url_helper::get_user_id_by_login($login);
	}

	private function get_static_page_id_by_name($static_page_name)
	{
		return url_helper::get_static_page_id_by_name($static_page_name, $this->project_id);
	}

}

?>