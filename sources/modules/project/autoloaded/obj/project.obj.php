<?php

class project_obj extends base_obj
{

	// Settings
	protected $fake_delete = false;
	// Internal
	protected $access_array = array();

	/**
	 * @var request
	 */
	protected $request;

	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	/**
	 * @var communities_read_access_helper
	 */
	protected $communities_read_access_helper;

	public function __construct($id, $lock = false)
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		global $request;
		$this->request = $request;

		parent::__construct($id, $lock);
	}

	private static $cache = array();

	/**
	 * @return project_obj
	 */
	public static function instance($id, $lock = false)
	{
		if (!$lock && isset(self::$cache[$id]))
		{
			return self::$cache[$id];
		}
		$class = __CLASS__;
		$instance = new $class($id, $lock);
		if (!$lock)
		{
			self::$cache[$id] = $instance;
		}
		return $instance;
	}
	
	public static function _test_clean_cache()
	{
		self::$cache = array();
	}

	/**
	 * @return project_access
	 */
	public function get_access($user_id = null)
	{
		if (!$this->exists())
		{
			trigger_error("Can't return access for unexisted project (id={$this->get_id()})");
			return null;
		}
		$key = $user_id ? $user_id : 0;
		if (!isset($this->access_array[$key]))
		{
			$this->access_array[$key] = new project_access($this, $user_id);
		}
		return $this->access_array[$key];
	}

	/**
	 *
	 * @return communities_read_access_helper
	 */
	public function get_communities_read_access_helper()
	{
		if (!$this->communities_read_access_helper)
		{
			$this->communities_read_access_helper = new communities_read_access_helper($this);
		}
		return $this->communities_read_access_helper;
	}

	public function get_name()
	{
		return $this->get_param("name");
	}
	
	private $x = 8;
	public function get_x() {return $this->x;}
	public function set_x($x) {$this->x = $x;}

	public function get_title()
	{
		return $this->get_param("title");
	}

	public function get_url()
	{
		if ($this->domain_logic->is_on())
		{
			if ($this->domain_logic->is_current_domain_project_domain($this->get_id()))
			{
				return "/";
			}
			else
			{
				$host_name = $this->get_param("domain_is_on") ? $this->get_param("domain_name") : $this->get_param("name") . "." . $this->domain_logic->get_main_host_name();
				return $this->request->get_protocol() . "://" . $host_name . "/";
			}
		}
		else
		{
			return $this->request->get_prefix() . "/p/" . $this->get_param("name") . "/";
		}
	}

	public function join_premoderation()
	{
		return $this->get_param("join_premoderation");
	}

	public function communities_are_on()
	{
		return $this->get_param("communities_are_on");
	}

	public function vector_is_on()
	{
		return $this->get_param("vector_is_on");
	}

	public function get_vector_competence_set_id()
	{
		return $this->get_param("vector_competence_set_id");
	}

	public function events_are_on()
	{
		return $this->get_param("events_are_on");
	}

	public function materials_are_on()
	{
		return $this->get_param("materials_are_on");
	}

	public function role_recommendation_is_on()
	{
		return $this->get_param("role_recommendation_is_on");
	}

	public function use_game_name()
	{
		return $this->get_param("use_game_name");
	}

	public function marks_are_on()
	{
		return $this->get_param("marks_are_on");
	}

	public function get_marks_competence_set_id()
	{
		return $this->get_param("marks_competence_set_id");
	}

	public function show_user_numbers()
	{
		return $this->get_param("show_user_numbers");
	}

	public function endpoint_is_on()
	{
		return $this->get_param("endpoint_is_on");
	}
	
	public function get_endpoint_url()
	{
		return $this->get_param("endpoint_url");
	}

	public function domain_is_on()
	{
		return $this->get_param("domain_is_on");
	}

	public function get_domain_name()
	{
		if ($this->domain_logic->is_on() and $this->get_param("domain_is_on"))
		{
			return $this->get_param("domain_name");
		}
		else
		{
			return false;
		}
	}

	protected function fill_data($lock)
	{
		$cache = project_cache::init($this->id);
		if ($lock)
		{
			$this->data = $this->fetch_data(true);
		}
		else
		{
			$this->data = $cache->get();
			if (empty($this->data))
			{
				$this->data = $this->fetch_data();
				if (!$this->data)
				{
					$this->data = array();
				}
				else
				{
					$cache->set($this->data);
				}
			}
		}
	}

	private function fetch_data($lock = false)
	{
		$lock_sql = $lock ? "LOCK IN SHARE MODE" : "";
		$row = $this->db->get_row("
			SELECT *
			FROM project
			WHERE id = {$this->id}
			{$lock_sql}
		");
		return $row ? $row : null;
	}

}

?>