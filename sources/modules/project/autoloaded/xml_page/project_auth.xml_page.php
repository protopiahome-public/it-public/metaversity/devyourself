<?php

class project_auth_xml_page extends base_xml_ctrl
{

	public function get_xml()
	{
		if (($key = GET("key"))
			and ($auth_data = auth_data_cache::init($key)->get())
			and isset($auth_data["user_id"]))
		{
			$this->user->login_by_id($auth_data["user_id"]);
		}

		$this->set_redirect_url(($retpath = GET("retpath")) ? $retpath : "/");
	}

}

?>