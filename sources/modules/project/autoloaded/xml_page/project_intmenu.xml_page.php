<?php

class project_intmenu_xml_page extends base_xml_ctrl
{

	protected $project_id;
	protected $xml_output;

	public function __construct($project_id, $xml_output = false)
	{
		$this->project_id = $project_id;
		$this->xml_output = $xml_output;
		parent::__construct();
	}

	public function init()
	{
		$this->xml_loader->add_xml(new project_intmenu_xml_ctrl($this->project_id, $this->xml_output));
	}

	public function get_xml()
	{
		return;
	}

}

?>