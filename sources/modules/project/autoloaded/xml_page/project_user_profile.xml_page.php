<?php

class project_user_profile_xml_page extends base_simple_xml_ctrl
{

	protected $project_id;
	protected $user_id;
	protected $data_one_row = true;
	protected $row_name = "user";
	protected $allow_error_404_if_no_data = true;
	protected $cache = false;
	protected $cache_key_property_name = "user_id";
	protected $dependencies_ctrl = array();

	public function __construct($project_id, $user_id)
	{
		$this->project_id = $project_id;
		$this->user_id = $user_id;
		parent::__construct();
	}

	protected function set_result()
	{
		$this->result = $this->db->fetch_all("
			SELECT
				is_analyst,
				l.number,
				game_name,
				personal_mark_count_calc, group_mark_count_calc, total_mark_count_calc
			FROM user_project_link l
			WHERE l.user_id = {$this->user_id} AND l.project_id = {$this->project_id}
		");
	}

	protected function append_xml()
	{
		/**
		 * Vector
		 */
		$vector_info_struct = vector_info_math::get_project_vector_info($this->user_id, $this->project_id);
		if ($vector_info_struct)
		{
			$vector_data = new vector_data_math($vector_info_struct["id"], $this->user_id, $vector_info_struct["competence_set_id"], false);
			$this->xml_loader->add_xml(new vector_xml_ctrl($vector_info_struct, $vector_data, VECTOR_DISPLAY_FUTURE | VECTOR_DISPLAY_SELF_STAT));
		}

		/**
		 * Competence sets
		 */
		$xdom = xdom::create("project_user_profile_competence_sets");
		$xdom->set_attr("project_id", $this->project_id)->set_attr("user_id", $this->user_id);
		$db_result = $this->db->sql("
			SELECT
				l.project_id, l.competence_set_id, cs.title as competence_set_title,
				l.personal_mark_count_calc, l.group_mark_count_calc, l.total_mark_count_calc
			FROM user_competence_set_project_link l
			LEFT JOIN competence_set cs ON cs.id = l.competence_set_id
			WHERE l.user_id = {$this->user_id} and l.project_id = {$this->project_id}
			ORDER BY l.total_mark_count_calc DESC
		");
		$data = array();
		while ($row = $this->db->fetch_array($db_result))
		{
			$data[$row["competence_set_id"]] = $row;
		}
		foreach ($data as $competence_set_id => $competence_set_data)
		{
			$xdom->create_child_node("competence_set")->set_attr("id", $competence_set_data["competence_set_id"])->set_attr("title", $competence_set_data["competence_set_title"])->set_attr("personal_mark_count_calc", $competence_set_data["personal_mark_count_calc"])->set_attr("group_mark_count_calc", $competence_set_data["group_mark_count_calc"])->set_attr("total_mark_count_calc", $competence_set_data["total_mark_count_calc"]);
		}
		return $xdom->get_xml(true);
	}

	protected function set_auxil_data()
	{
		
	}

}

?>