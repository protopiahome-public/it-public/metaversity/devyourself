<?php

class project_robots_txt_xml_page extends base_xml_ctrl
{

	protected $project_id;

	/**
	 *
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct();
	}

	public function get_xml()
	{
		$text = "";
		if (($domain_name = $this->project_obj->get_domain_name()))
		{
			$text .= "User-agent: *\n";
			$text .= "Host: {$domain_name}\n";
		}
		response::set_content_type_text();
		response::set_content($text);
		return true;
	}

}

?>