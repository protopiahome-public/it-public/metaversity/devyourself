<?php

class project_settings_xml_page extends base_xml_ctrl
{

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct();
	}

	public function get_xml()
	{
		if (!$this->project_obj->get_access()->is_member() and !$this->project_obj->get_access()->is_pretender_strict())
		{
			$this->set_error_403();
			return;
		}
		
		$user_id = $this->user->get_user_id();
		
		$attrs = array(
			"user_id" => $user_id,
			"use_game_name" => $this->project_obj->use_game_name(),
		);

		if ($user_id and $this->project_obj->use_game_name())
		{
			$game_name = $this->db->get_value("
				SELECT game_name 
				FROM user_project_link
				WHERE user_id = {$user_id}
					AND project_id = {$this->project_id}
			");
			$attrs["game_name"] = $game_name ?: $this->user->get_visible_name();
		}

		return $this->get_node_string(null, $attrs);
	}

}

?>