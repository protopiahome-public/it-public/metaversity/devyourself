<?php

class project_static_page_xml_page extends base_xml_ctrl
{

	protected $static_page_id;

	/**
	 *
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($static_page_id, $project_id)
	{
		$this->static_page_id = $static_page_id;
		$this->project_obj = project_obj::instance($project_id);
		parent::__construct();
	}

	public function init()
	{
		$this->xml_loader->add_xml(new static_page_full_xml_ctrl($this->static_page_id, $this->project_obj->get_id()));
	}

	public function get_xml()
	{
		return;
	}

}

?>