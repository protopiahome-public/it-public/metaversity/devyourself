<?php

class project_users_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "user";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "user_short",
			"param2" => "project_id"
		),
	);
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($page, $project_id)
	{
		$this->page = $page;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

		$processor = new sort_easy_processor();
		if ($this->project_obj->marks_are_on())
		{
			$processor->add_order("mark-count", "l.total_mark_count_calc DESC, l.number ASC", "u.total_mark_count_calc ASC, l.number DESC", true);
		}
		$processor->add_order("join-time", "l.add_time DESC", "l.add_time ASC");
		if ($this->project_obj->show_user_numbers())
		{
			$processor->add_order("number", "l.number ASC", "l.number DESC", false);
		}
		$this->add_easy_processor($processor);

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new text_sql_filter("search", "имя/логин", array("u.login", "u.first_name", "u.last_name", "l.game_name")));
		$processor->add_sql_filter(new multi_link_sql_filter("group", "группа", "u.id", "user_user_group_link", "user_id", "user_group_id", "user_group"));
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("user u");
		$select_sql->add_select_fields("u.id, l.number, l.status, l.game_name");
		$select_sql->add_select_fields("l.personal_mark_count_calc, l.group_mark_count_calc, l.total_mark_count_calc");
		$select_sql->add_select_fields("l.add_time");
		$select_sql->add_join("JOIN user_project_link l ON l.user_id = u.id");
		$select_sql->add_where("l.project_id = {$this->project_id}");
		$select_sql->add_where("l.status <> 'pretender' AND l.status <> 'deleted'");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	protected function append_xml()
	{
		/**
		 * Vectors
		 */
		$xdom_vectors = xdom::create("vectors");
		$user_id_array = $this->get_selected_ids();
		$vector_config = vector_info_math::get_project_vector_config($this->project_id);
		if ($vector_config)
		{
			$vector_competence_set_id = $vector_config["vector_competence_set_id"];
			$vectors = vector_info_math::get_project_vectors_for_project($this->project_id, $vector_competence_set_id, $user_id_array);
			foreach ($vectors as $vector_data)
			{
				$xdom_vectors->create_child_node("vector")->set_attr("id", $vector_data["id"])->set_attr("user_id", $vector_data["user_id"])->set_attr("project_id", $vector_data["project_id"])->set_attr("competence_set_id", $vector_data["competence_set_id"]);
			}
		}

		/**
		 * Output
		 */
		return $xdom_vectors->get_xml(true);
	}

}

?>