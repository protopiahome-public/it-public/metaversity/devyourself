<?php

class project_profile_xml_page extends base_simple_xml_ctrl
{
	
	protected $project_id;
	protected $data_one_row = true;
	protected $row_name = "project";
	protected $allow_error_404_if_no_data = true;
	protected $cache = false;
	protected $cache_key_property_name = "project_id";
	protected $dependencies_ctrl = array ();
	
	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}
	
	protected function set_result()
	{
		$this->result = $this->db->fetch_all("
			SELECT
				id, descr
			FROM project
			WHERE id = {$this->project_id}
		");
	}
	
	protected function append_xml()
	{
		$xdom = xdom::create("project_competence_set_links");
		$project_node = $xdom->create_child_node("project")->set_attr("id", $this->project_id);
		$db_result = $this->db->sql("
				SELECT
					l.competence_set_id, cs.title as competence_set_title,
					l.personal_mark_count_calc, l.group_mark_count_calc, l.group_mark_raw_count_calc, l.total_mark_count_calc
				FROM project_competence_set_link_calc l
				LEFT JOIN competence_set cs ON cs.id = l.competence_set_id
				WHERE project_id = {$this->project_id}
				ORDER BY l.total_mark_count_calc DESC
			");
		$data = array ();
		while ($row = $this->db->fetch_array($db_result))
		{
			$project_node->create_child_node("competence_set")->set_attr("id", $row["competence_set_id"])->set_attr("title", $row["competence_set_title"])->set_attr("personal_mark_count_calc", $row["personal_mark_count_calc"])->set_attr("group_mark_count_calc", $row["group_mark_count_calc"])->set_attr("group_mark_raw_count_calc", $row["group_mark_raw_count_calc"])->set_attr("total_mark_count_calc", $row["total_mark_count_calc"]);
		}
		return $xdom->get_xml(true);
	}
	
	protected function set_auxil_data()
	{
		
	}

}

?>