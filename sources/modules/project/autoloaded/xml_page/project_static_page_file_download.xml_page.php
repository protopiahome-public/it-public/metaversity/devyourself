<?php

class project_static_page_file_download_xml_page extends base_block_set_file_download_xml_ctrl
{

	protected $static_page_id;

	/**
	 *
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 *
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($block_file_id, $project_id, $static_page_id = 0)
	{
		$this->project_id = $project_id;
		$this->static_page_id = $static_page_id;
		
		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct($block_file_id);
	}

	protected function check_file_rights()
	{
		$static_page_data = $this->db->get_row("
			SELECT *
			FROM project_taxonomy_item
			WHERE id = {$this->static_page_id} AND type = 'static_page'
		");
		if (!$static_page_data)
		{
			return false;
		}
		if ($static_page_data["project_id"] != $this->project_id)
		{
			return false;
		}
		if ($static_page_data["block_set_id"] != $this->data["block_set_id"])
		{
			return false;
		}
		return true;
	}

}

?>
