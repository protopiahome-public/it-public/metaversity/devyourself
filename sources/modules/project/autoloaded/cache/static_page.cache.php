<?php

class static_page_cache extends base_cache
{

	public static function init($static_page_id)
	{
		$tag_keys = array();
		$tag_keys[] = static_page_cache_tag::init($static_page_id)->get_key();
		return parent::get_cache(__CLASS__, $static_page_id, $tag_keys);
	}

}

?>