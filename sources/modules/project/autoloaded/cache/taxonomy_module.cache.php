<?php

class taxonomy_module_cache extends base_cache
{

	public static function init($module_name, $project_id, $module_id = null)
	{
		$tag_keys = array();
		if ($module_id)
		{
			$tag_keys[] = taxonomy_module_cache_tag::init($module_id)->get_key();
		}
		return parent::get_cache(__CLASS__, $module_name, $project_id, $tag_keys);
	}

}

?>