<?php

class taxonomy_project_cache extends base_cache
{

	public static function init($name, $project_id = null)
	{
		$tag_keys = array();
		if ($project_id)
		{
			$tag_keys[] = project_cache_tag::init($project_id)->get_key();
		}
		return parent::get_cache(__CLASS__, $name, $tag_keys);
	}

}

?>