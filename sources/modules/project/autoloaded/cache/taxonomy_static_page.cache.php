<?php

class taxonomy_static_page_cache extends base_cache
{

	public static function init($name, $project_id, $static_page_id = null)
	{
		$tag_keys = array();
		if ($static_page_id)
		{
			$tag_keys[] = static_page_cache_tag::init($static_page_id)->get_key();
		}
		return parent::get_cache(__CLASS__, $name, $project_id, $tag_keys);
	}

}

?>