<?php

class project_menu_cache extends base_cache
{

	public static function init($project_id)
	{
		$tag_keys = array();
		$tag_keys[] = project_taxonomy_cache_tag::init($project_id)->get_key();
		return parent::get_cache(__CLASS__, $project_id, $tag_keys);
	}

}

?>