<?php

class project_access_cache extends base_cache
{

	public static function init($project_id, $user_id)
	{
		$tag_keys = array();
		$tag_keys[] = project_user_cache_tag::init($project_id, $user_id)->get_key();
		return parent::get_cache(__CLASS__, $project_id, $user_id, $tag_keys);
	}

}

?>