<?php

class taxonomy_module_dt extends base_dt
{

	protected function init()
	{
		$this->add_block("main", "Основное");

		$this->set_db_table("project_taxonomy_item");
		$dtf = new string_dtf("title", "Название ссылки на модуль");
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("show_in_menu", "Показывать в меню");
		$dtf->set_default_value(true);
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("add", array(
			"title", 
		));

		$this->add_fields_in_axis("edit", array(
			"show_in_menu",
		));
	}

}

?>