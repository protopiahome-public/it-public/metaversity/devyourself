<?php

class project_intmenu_item_dt extends base_dt
{

	protected function init()
	{
		$this->add_block("main", "Основное");

		$dtf = new string_dtf("title", "Название");
		$dtf->set_max_length(60);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new url_string_dtf("url", "Адрес");
		$dtf->set_max_length(255);
		$dtf->set_importance(true);
		$dtf->set_comment("Должен начинаться с http:// или https://");
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("edit", array("title", "url"));
		$this->add_fields_in_axis("frontend", array("title", "url"));
	}

}

?>