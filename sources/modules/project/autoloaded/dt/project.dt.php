<?php

require_once PATH_MODULES . "/project/autoloaded/access/project.access_maper.php";

class project_dt extends base_dt
{

	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	public function __construct()
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", "Названия");
		$this->add_block("main_dates", "Даты");
		$this->add_block("main_logo", "Логотип");
		$this->add_block("main_descr", "Описание");
		$this->add_block("main_domain", "Свой домен");
		$this->add_block("main_other", "Разное");
		$this->add_block("design", "Оформление");
		$this->add_block("communities", "Сообщества");
		$this->add_block("vector", "Вектор");
		$this->add_block("events", "События");
		$this->add_block("materials", "Материалы");
		$this->add_block("rpg", "Рекомендация ролей");
		$this->add_block("marks", "Оценивание участников");
		$this->add_block("other", "Прочее");
		$this->add_block("access_project", "Доступ к проекту");
		$this->add_block("access_communities", "Доступ к сообществам");
		$this->add_block("access_events", "Доступ к событиям");
		$this->add_block("access_materials", "Доступ к материалам");
		$this->add_block("access_precedents", "Доступ к прецедентам и оценкам");

		$dtf = new string_dtf("title", "Название проекта");
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		/* $dtf = new select_dtf("title", "Название проекта");
		  $dtf->set_cases(array(
		  "a" => "Alpha",
		  "b" => "Betta",
		  "c" => "Gamma",
		  ));
		  $dtf->set_drop_down_view(true);
		  $this->add_field($dtf, "main"); */

		$dtf = new url_name_dtf("name", "URL проекта");
		$dtf->set_max_length(40);
		$dtf->set_url_attribute("url");
		$dtf->set_url_prefix($this->domain_logic->get_main_prefix() . "/p/");
		$dtf->set_deprecated_values(array(
			"www", "mail", "ns", "ns1", "ns2", "ns3", "ns4", // std. subdomains
			"admin", "god", "moderator", "editor", "user", "all", // roles
			"cp", "add", "create", "edit", "delete", "remove", // actions
			"widget", "members", "users", "admins", "moderators", "settings", "search", "company", "join", // std. URLs
			"devyourself", "events", "materials", "precedents", "vector", "roles", // URLs
		));
		$dtf->set_comment("Разрешены английские буквы, цифры и дефис. Рекомендуется не менять.");
		$this->add_field($dtf, "main");

		$dtf = new datetime_dtf("start_time", "Начало проекта");
		$dtf->set_date_only(true);
		$dtf->set_edit_mode_years_count_for_select(3, 7);
		$this->add_field($dtf, "main_dates");

		$dtf = new datetime_dtf("finish_time", "Окончание проекта");
		$dtf->set_date_only(true);
		$dtf->set_edit_mode_years_count_for_select(3, 7);
		$dtf->set_must_be_later("start_time", true, true);
		$this->add_field($dtf, "main_dates");

		// @dm9 remove from here and from DB
		/* $dtf = new url_string_dtf("main_url", "Основной URL-адрес проекта");
		  $dtf->set_human_view_column();
		  $dtf->set_comment("Должен начинаться с http:// или https://");
		  $this->add_field($dtf, "main"); */

		$dtf = new image_pack_dtf("logo", "Логотип проекта", PATH_TMP);
		$dtf->add_image("big", "Big", 100, 75, "/data/project/big/", "/img/defaults/logo-big-default.jpg", PATH_PUBLIC_DATA . "/project/big");
		$dtf->add_image("small", "Small", 50, 38, "/data/project/small/", "/img/defaults/logo-small-default.jpg", PATH_PUBLIC_DATA . "/project/small");
		$dtf->set_comment("Отображается в «шапке» сообщества. Изображение будет сжато до 100x75 пикселей.");
		$dtf->set_add_prefix_to_url($this->domain_logic->get_main_prefix());
		$this->add_field($dtf, "main_logo");

		$dtf = new text_html_dtf("descr", "Краткое описание проекта");
		$dtf->set_editor_height(100);
		$this->add_field($dtf, "main_descr");

		$dtf = new bool_dtf("domain_is_on", "Проект будет доступен по своему доменному имени");
		$this->add_field($dtf, "main_domain");

		// @ar забыл проверить, что здесь что-то адекватное
		// не забудь про абв.рф
		$dtf = new string_dtf("domain_name", "Имя домена");
		$dtf->add_dependency("domain_is_on", "1", INTCMF_DTF_DEP_ACTION_IMPORTANT | INTCMF_DTF_DEP_ACTION_ENABLE);
		$dtf->add_dependency("domain_is_on", "0", INTCMF_DTF_DEP_ACTION_DO_NOT_STORE);
		$dtf->set_comment("Домен должен быть вами самостоятельно зарегистрирован и настроен.");
		$this->add_field($dtf, "main_domain");

		$dtf = new bool_dtf("use_game_name", "Использовать в проекте игровые имена");
		$this->add_field($dtf, "main_other");

		$dtf = new bool_dtf("show_user_numbers", "Показывать уникальные номера участников");
		$this->add_field($dtf, "main_other");

		/* $dtf = new calc_dtf("state_calc", "Состояние проекта", "IF(NOW() < start_time, 'NOT_ST', IF(NOW() < finish_time, 'RUNNING', 'FINISHED'))");
		  $this->add_field($dtf); */

		$dtf = new bool_dtf("communities_are_on", "Включить сообщества");
		$this->add_field($dtf, "communities");

		$dtf = new bool_dtf("vector_is_on", "Включить Вектор");
		$dtf->set_default_value(true);
		$this->add_field($dtf, "vector");

		$dtf = new foreign_key_dtf("vector_competence_set_id", "Набор компетенций для Вектора", "competence_set");
		$dtf->set_restrict_items_sql("
			SELECT id, title
			FROM competence_set
			WHERE professiogram_count_calc > 0
		", "
			SELECT id, title
			FROM competence_set
			WHERE professiogram_count_calc > 0 AND id = %s
		");
		$dtf->set_importance(true);
		$dtf->set_comment("Отображаются наборы компетенций, в которых есть профессиограммы (т. к. они необходимы для работы Вектора)");
		$this->add_field($dtf, "vector");

		$dtf = new bool_dtf("events_are_on", "Включить события");
		$this->add_field($dtf, "events");

		$dtf = new bool_dtf("materials_are_on", "Включить материалы");
		$this->add_field($dtf, "materials");

		$dtf = new bool_dtf("role_recommendation_is_on", "Включить рекомендацию ролей");
		$this->add_field($dtf, "rpg");

		$dtf = new bool_dtf("marks_are_on", "Включить оценивание участников");
		$this->add_field($dtf, "marks");

		$dtf = new foreign_key_dtf("marks_competence_set_id", "Набор компетенций для оценивания", "competence_set");
		$dtf->set_importance(true);
		$this->add_field($dtf, "marks");

		$dtf = new bool_dtf("join_premoderation", "Включить премодерацию на присоединение участников к проекту");
		$dtf->set_comment("Если галочка стоит, участники будут попадать в проект только после вашего подтверждения");
		$this->add_field($dtf, "access_project");

		$project_access_maper = new project_access_maper();

		$dtf = new access_level_dtf("access_read", "Доступ к проекту на чтение", $project_access_maper);
		$dtf->add_level(ACCESS_LEVEL_GUEST, "Все");
		$dtf->add_level(ACCESS_LEVEL_USER, "Зарегистрированные пользователи");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники проекта");
		$dtf->add_level(ACCESS_LEVEL_MODERATOR, "Модераторы проекта");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы проекта");
		$this->add_field($dtf, "access_project");

		$dtf = new access_level_dtf("access_communities_read", "Доступ к сообществам на чтение", $project_access_maper);
		$dtf->add_level(ACCESS_LEVEL_GUEST, "Все");
		$dtf->add_level(ACCESS_LEVEL_USER, "Зарегистрированные пользователи");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники проекта");
		$dtf->add_level(ACCESS_LEVEL_MODERATOR, "Модераторы проекта (любые)");
		$dtf->add_level(ACCESS_LEVEL_SPECIAL_MODERATOR, "Модераторы сообществ");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы проекта");
		$dtf->set_comment("Права могут быть ужесточены в отдельных сообществах");
		$this->add_field($dtf, "access_communities");

		$dtf = new access_level_dtf("access_communities_add", "Создание нового сообщества", $project_access_maper);
		$dtf->add_level(ACCESS_LEVEL_USER, "Все");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники проекта");
		$dtf->add_level(ACCESS_LEVEL_MODERATOR, "Модераторы проекта (любые)");
		$dtf->add_level(ACCESS_LEVEL_SPECIAL_MODERATOR, "Модераторы сообществ");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы проекта");
		$dtf->set_required_level_field_name("access_communities_read");
		$this->add_field($dtf, "access_communities");

		$dtf = new access_level_dtf("access_events_read", "Доступ к событиям на чтение", $project_access_maper);
		$dtf->add_level(ACCESS_LEVEL_GUEST, "Все");
		$dtf->add_level(ACCESS_LEVEL_USER, "Зарегистрированные пользователи");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники проекта");
		$dtf->add_level(ACCESS_LEVEL_MODERATOR, "Модераторы проекта (любые)");
		$dtf->add_level(ACCESS_LEVEL_SPECIAL_MODERATOR, "Модераторы событий");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы проекта");
		$this->add_field($dtf, "access_events");

		$dtf = new access_level_dtf("access_events_comment", "Комментирование событий", $project_access_maper);
		$dtf->add_level(ACCESS_LEVEL_USER, "Все");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники проекта");
		$dtf->add_level(ACCESS_LEVEL_MODERATOR, "Модераторы проекта (любые)");
		$dtf->add_level(ACCESS_LEVEL_SPECIAL_MODERATOR, "Модераторы событий");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы проекта");
		$dtf->set_required_level_field_name("access_events_read");
		$this->add_field($dtf, "access_events");

		$dtf = new access_level_dtf("access_materials_read", "Доступ к материалам на чтение", $project_access_maper);
		$dtf->add_level(ACCESS_LEVEL_GUEST, "Все");
		$dtf->add_level(ACCESS_LEVEL_USER, "Зарегистрированные пользователи");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники проекта");
		$dtf->add_level(ACCESS_LEVEL_MODERATOR, "Модераторы проекта (любые)");
		$dtf->add_level(ACCESS_LEVEL_SPECIAL_MODERATOR, "Модераторы материалов");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы проекта");
		$this->add_field($dtf, "access_materials");

		$dtf = new access_level_dtf("access_materials_comment", "Комментирование материалов", $project_access_maper);
		$dtf->add_level(ACCESS_LEVEL_USER, "Все");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники проекта");
		$dtf->add_level(ACCESS_LEVEL_MODERATOR, "Модераторы проекта (любые)");
		$dtf->add_level(ACCESS_LEVEL_SPECIAL_MODERATOR, "Модераторы материалов");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы проекта");
		$dtf->set_required_level_field_name("access_materials_read");
		$this->add_field($dtf, "access_materials");

		$dtf = new access_level_dtf("access_precedents_read", "Доступ к прецедентам на чтение", $project_access_maper);
		$dtf->add_level(ACCESS_LEVEL_GUEST, "Все");
		$dtf->add_level(ACCESS_LEVEL_USER, "Зарегистрированные пользователи");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники проекта");
		$dtf->add_level(ACCESS_LEVEL_MODERATOR, "Модераторы проекта (любые)");
		$dtf->add_level(ACCESS_LEVEL_SPECIAL_MODERATOR, "Модераторы прецедентов");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы проекта");
		$this->add_field($dtf, "access_precedents");

		$dtf = new access_level_dtf("access_precedents_comment", "Комментирование прецедентов", $project_access_maper);
		$dtf->add_level(ACCESS_LEVEL_USER, "Все");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники проекта");
		$dtf->add_level(ACCESS_LEVEL_MODERATOR, "Модераторы проекта (любые)");
		$dtf->add_level(ACCESS_LEVEL_SPECIAL_MODERATOR, "Модераторы прецедентов");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы проекта");
		$dtf->set_required_level_field_name("access_precedents_read");
		$this->add_field($dtf, "access_precedents");

		$dtf = new int_dtf("user_count_calc", "Количество пользователей");
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$dtf = new int_dtf("precedent_count_calc", "Количество событий");
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$dtf = new int_dtf("personal_mark_count_calc", "Количество личных оценок");
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$dtf = new int_dtf("group_mark_count_calc", "Количество групповых оценок");
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$dtf = new int_dtf("group_mark_raw_count_calc", "Количество изначальных групповых оценок");
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$dtf = new int_dtf("total_mark_count_calc", "Суммарное количество оценок");
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$dtf = new bool_dtf("endpoint_is_on", "Включить аутентификацию через {$this->domain_logic->get_main_host_name()} на сайте проекта");
		$this->add_field($dtf);

		$dtf = new bool_dtf("endpoint_resend", "Обновить информацию обо всех участниках проекта");
		$dtf->add_dependency("endpoint_is_on", "1", INTCMF_DTF_DEP_ACTION_IMPORTANT | INTCMF_DTF_DEP_ACTION_ENABLE);
		$dtf->add_dependency("endpoint_is_on", "0", INTCMF_DTF_DEP_ACTION_DO_NOT_STORE);
		$this->add_field($dtf);

		$dtf = new url_string_dtf("endpoint_url", "Endpoint URL сайта проекта");
		$dtf->add_dependency("endpoint_is_on", "1", INTCMF_DTF_DEP_ACTION_IMPORTANT | INTCMF_DTF_DEP_ACTION_ENABLE);
		$dtf->add_dependency("endpoint_is_on", "0", INTCMF_DTF_DEP_ACTION_DO_NOT_STORE);
		$this->add_field($dtf);
		
		$dtf = new select_dtf("color_scheme", "Цветовая схема сайта");
		$dtf->set_cases(array(
			"green" => "Зелёная",
			"blue" => "Голубая",
			"yellow" => "Оливковая",
			/*"red" => "Розовая",*/
		));
		$dtf->set_default_key("green");
		$this->add_field($dtf);

		$dtf = new image_pack_dtf("bg_head", "Фон шапки", PATH_TMP);
		$dtf->add_image("main", "Main", 0, 0, "/data/project/bg_head/", null, PATH_PUBLIC_DATA . "/project/bg_head");
		$dtf->get_image_by_name("main")->add_processor(new image_pack_crop_processor(0, 87));
		$dtf->set_add_prefix_to_url($this->domain_logic->get_main_prefix());
		$dtf->set_comment("Фон для шапки проекта. Будет обрезан по вертикали до высоты шапки (87 пикселя).");
		$this->add_field($dtf);

		$dtf = new image_pack_dtf("bg_body", "Фон", PATH_TMP);
		$dtf->add_image("main", "Main", 0, 0, "/data/project/bg_body/", null, PATH_PUBLIC_DATA . "/project/bg_body");
		$dtf->set_add_prefix_to_url($this->domain_logic->get_main_prefix());
		$dtf->set_comment("Фон проекта. Постарайтесь заливать не очень большие (по размеру файлов) изображения.");
		$this->add_field($dtf);

		$dtf = new image_pack_dtf("favicon", "Иконка сайта", PATH_TMP);
		$dtf->add_image("main", "Main", 16, 16, "/data/project/favicon/", null, PATH_PUBLIC_DATA . "/project/favicon");
		$dtf->set_image_type_png();
		$dtf->set_add_prefix_to_url($this->domain_logic->get_main_prefix());
		$dtf->set_comment("Иконка сайта. Отображается в браузере на закладках, в которых открыт сайт. Изображение будет сжато до 16x16 пикселей. Загружайте квадратные изображения, иначе они будут непропорционально отмасштабированы.");
		$this->add_field($dtf);

		$dtf = new int_dtf("custom_feed_count_calc", "Количество лент");
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		// @todo remove _calc from here! аnd see other mark_count_calc in shorts

		$this->add_fields_in_axis("short", array(
			"name", "title", "start_time", "finish_time",
			"logo",
			"join_premoderation",
			"communities_are_on",
			"vector_is_on", "vector_competence_set_id",
			"events_are_on",
			"materials_are_on",
			"role_recommendation_is_on", "use_game_name",
			"marks_are_on", "marks_competence_set_id",
			"show_user_numbers",
			"user_count_calc", "precedent_count_calc",
			"personal_mark_count_calc", "group_mark_count_calc", "group_mark_raw_count_calc", "total_mark_count_calc",
			"color_scheme", "bg_head", "bg_body", "custom_feed_count_calc",
			"domain_is_on", "domain_name",
			"favicon"
		));

		$this->add_fields_in_axis("create", array(
			"title", "name",
			"start_time", "finish_time",
			"logo",
			"descr",
		));

		$this->add_fields_in_axis("edit_general", array(
			"title", "name",
			"start_time", "finish_time",
			"logo",
			"descr",
			"show_user_numbers", "use_game_name",
		));

		$this->add_fields_in_axis("edit_access", array(
			"join_premoderation", "access_read"
		));

		$this->add_fields_in_axis("edit_domain", array(
			"domain_is_on", "domain_name",
		));

		$this->add_fields_in_axis("edit_endpoint", array(
			"endpoint_is_on",
			"endpoint_url",
			"endpoint_resend",
		));

		$this->add_fields_in_axis("edit_design", array(
			"color_scheme", "bg_head", "bg_body", "favicon"
		));

		$this->add_fields_in_axis("edit_communities", array(
			"access_communities_read", "access_communities_add"
		));

		$this->add_fields_in_axis("add_vector", array(
			"vector_competence_set_id"
		));

		$this->add_fields_in_axis("edit_vector", array(
			"vector_competence_set_id"
		));

		$this->add_fields_in_axis("edit_events", array(
			"access_events_read", "access_events_comment"
		));

		$this->add_fields_in_axis("edit_materials", array(
			"access_materials_read", "access_materials_comment"
		));

		$this->add_fields_in_axis("edit_roles", array(
			"access_materials_read", "access_materials_comment"
		));

		$this->add_fields_in_axis("add_marks", array(
			"marks_competence_set_id"
		));

		$this->add_fields_in_axis("edit_marks", array(
			"marks_competence_set_id", "access_precedents_read", "access_precedents_comment"
		));
	}

}

?>