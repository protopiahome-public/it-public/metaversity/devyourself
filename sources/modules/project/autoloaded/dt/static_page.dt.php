<?php

// @dm9 CR
class static_page_dt extends base_dt
{

	/**
	 * @var url_name_dtf
	 */
	protected $name_dtf;

	protected function init()
	{
		$this->add_block("main", "Основное");

		$this->set_db_table("project_taxonomy_item");
		$dtf = new string_dtf("title", "Название страницы");
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$this->name_dtf = $dtf = new url_name_dtf("static_page_name", "URL");
		$dtf->set_max_length(40);
		$dtf->set_url_attribute("url");
		$dtf->set_unique_restriction("MUST BE SET BELOW");
		$dtf->set_deprecated_values(array(
			"www", "mail", "ns", "ns1", "ns2", "ns3", "ns4", // std. subdomains
			"admin", "god", "moderator", "editor", "user", "all", // roles
			"cp", "add", "create", "edit", "delete", "remove", // actions
			"widget", "members", "users", "admins", "moderators", "settings", "search", "company", "join", // std. URLs
			"co", "post", "feed", "events", "materials", "vector", "roles", "precedents", // URLs
		));
		$dtf->set_comment("Разрешены английские буквы, цифры и дефис. Рекомендуется не менять.");
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("show_in_menu", "Показывать в меню");
		$dtf->set_default_value(true);
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("add", array(
			"title", "static_page_name",
			"show_in_menu",
		));

		$this->add_fields_in_axis("edit", array(
			"title", "static_page_name",
			"show_in_menu",
		));

		$this->add_fields_in_axis("full", array(
			"title", "static_page_name",
			"show_in_menu",
		));
	}

	public function set_project_obj(project_obj $project_obj, $use_project_url_placeholder = false)
	{
		$project_url = $use_project_url_placeholder ? "%PROJECT_URL%" : $project_obj->get_url();
		$this->name_dtf->set_url_prefix($project_url);

		$this->name_dtf->set_unique_restriction("type = 'static_page' AND project_id = " . $project_obj->get_id());
	}

}

?>