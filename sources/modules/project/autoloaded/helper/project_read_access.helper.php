<?php

class project_read_access_helper extends base_db_helper
{

	/**
	 * @var project_obj
	 */
	private $project_obj;

	public function __construct(project_obj $project_obj)
	{
		parent::__construct();

		$this->project_obj = $project_obj;
	}

	public function get_project_users_access_read($users_ids)
	{
		$project_access_maper = new project_access_maper();
		$project_access_status = $this->project_obj->get_param("access_read");
		$project_access_level = $project_access_maper->get_level_from_status($project_access_status);

		$allowed_users_ids = $users_ids;
		$allowed_users_ids_keys = array_flip($allowed_users_ids);
		foreach ($allowed_users_ids_keys as &$allowed_users_ids_value)
		{
			$allowed_users_ids_value = true;
		}

		if ($project_access_level > ACCESS_LEVEL_USER)
		{
			$allowed_users_ids_sql = implode(",", $allowed_users_ids);
			$users_access_levels = $this->db->fetch_all("
				SELECT u.id, pl.status as project_status
				FROM user u
				LEFT JOIN user_project_link pl ON (pl.user_id = u.id AND pl.project_id = {$this->project_obj->get_id()})
				WHERE u.id IN ({$allowed_users_ids_sql}) AND u.is_admin = 0
			", "id");
			foreach ($users_access_levels as $user_id => $user_access_levels)
			{
				// @todo может, не project_status, а user_status?
				if (!$user_access_levels["project_status"])
				{
					unset($allowed_users_ids_keys[$user_id]);
					continue;
				}

				$project_user_access_level = $project_access_maper->get_level_from_status($user_access_levels["project_status"]);

				if ($project_user_access_level < $project_access_level)
				{
					unset($allowed_users_ids_keys[$user_id]);
				}
			}
		}
		return $allowed_users_ids_keys;
	}

	// @todo плохой паттерн: project_obj передаётся через конструктор,
	// а community_obj через параметр.
	public function get_community_users_access_read($users_ids, community_obj $community_obj)
	{
		$project_access_maper = new project_access_maper();
		$project_access_status = $this->project_obj->get_param("access_read");
		$project_access_level = $project_access_maper->get_level_from_status($project_access_status);

		$community_access_maper = new community_access_maper();
		$community_access_status = $community_obj->get_param("access_read");
		$community_access_level = $community_access_maper->get_level_from_status($community_access_status);

		$allowed_users_ids = $users_ids;
		$allowed_users_ids_keys = array_flip($allowed_users_ids);
		foreach ($allowed_users_ids_keys as &$allowed_users_ids_value)
		{
			$allowed_users_ids_value = true;
		}

		if ($project_access_level > ACCESS_LEVEL_USER || $community_access_level > ACCESS_LEVEL_USER)
		{
			$allowed_users_ids_sql = implode(",", $allowed_users_ids);
			$users_access_levels = $this->db->fetch_all("
				SELECT u.id, pl.status as project_status, cl.status as community_status
				FROM user u
				LEFT JOIN user_project_link pl ON (pl.user_id = u.id AND pl.project_id = {$this->project_obj->get_id()})
				LEFT JOIN community_user_link cl ON (cl.user_id = u.id AND cl.community_id = {$community_obj->get_id()})
				WHERE u.id IN ({$allowed_users_ids_sql}) AND u.is_admin = 0
			", "id");
			foreach ($users_access_levels as $user_id => $user_access_levels)
			{
				if (!$user_access_levels["project_status"])
				{
					if ($project_access_level > ACCESS_LEVEL_USER)
					{
						unset($allowed_users_ids_keys[$user_id]);
						continue;
					}
					else
					{
						$user_access_levels["project_status"] = ACCESS_STATUS_DELETED;
					}
				}
				if (!$user_access_levels["community_status"])
				{
					if ($community_access_level > ACCESS_LEVEL_USER)
					{
						unset($allowed_users_ids_keys[$user_id]);
						continue;
					}
					else
					{
						$user_access_levels["community_status"] = ACCESS_STATUS_DELETED;
					}
				}

				$project_user_access_level = $project_access_maper->get_level_from_status($user_access_levels["project_status"]);
				$community_user_access_level = $community_access_maper->get_level_from_status($user_access_levels["community_status"]);

				if ($project_user_access_level < $project_access_level)
				{
					unset($allowed_users_ids_keys[$user_id]);
				}
				elseif ($community_access_level == ACCESS_LEVEL_PROJECT_MEMBER)
				{
					if ($project_user_access_level < ACCESS_LEVEL_MEMBER)
					{
						unset($allowed_users_ids_keys[$user_id]);
					}
				}
				elseif ($community_user_access_level < $community_access_level)
				{
					unset($allowed_users_ids_keys[$user_id]);
				}
			}
		}
		return $allowed_users_ids_keys;
	}

}

?>