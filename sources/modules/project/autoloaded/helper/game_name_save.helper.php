<?php

class game_name_save_helper extends base_static_db_helper
{

	public static function update_game_name(project_obj $project_obj, $game_name)
	{
		if (!$project_obj->use_game_name())
		{
			return;
		}
		
		global $user;

		$game_name = trim($game_name);
		$game_name = mb_substr($game_name, 0, 100);
		$game_name = trim($game_name);
		if ($user->get_visible_name() == $game_name)
		{
			$game_name = "";
		}
		$game_name_escaped = self::db()->escape($game_name);
		self::db()->sql("
			UPDATE user_project_link
			SET game_name = '{$game_name_escaped}'
			WHERE project_id = {$project_obj->get_id()}
				AND user_id = {$user->get_user_id()}
		");
	}

}

?>