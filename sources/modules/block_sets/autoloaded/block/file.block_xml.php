<?php

require_once PATH_INTCMF . "/xml_builder.php";

class file_block_xml extends base_block_xml
{

	protected function fill_xml(xdom $block_xdom)
	{
		$data = $this->db->get_row("SELECT * FROM block_file WHERE id = {$this->block_id}");

		$block_xdom->set_attr("title", $data["title"]);
		$block_xdom->set_attr("file_name", $data["file_name"]);
		$block_xdom->set_attr("ext", substr($data["ext"], 1));
		$block_xdom->set_attr("file_type", $data["type"]);
		xml_builder::file_size($block_xdom, $data["size"]);
	}

	public function get_type()
	{
		return "file";
	}

}

?>