<?php

define("BLOCK_SET_ERROR_BLOCK_NOT_EXISTS", 201);

abstract class base_block_save
{

	protected $block_id;
	protected $block_set_id;
	protected $block_fake_id;
	protected $data;
	protected $error_code;
	protected $type;
	protected $block_fake_for_id;
	protected $is_fake;
	protected $response_aux = array();

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var user
	 */
	protected $user;

	public function __construct($block_id, $block_set_id)
	{
		global $db;
		$this->db = $db;
		global $user;
		$this->user = $user;

		$this->type = $this->get_type();

		$this->block_id = $block_id;
		$this->block_set_id = $block_set_id;
	}

	public function get_id()
	{
		return $this->get_id();
	}

	public function get_fake_id()
	{
		return $this->block_fake_id;
	}

	public function get_error_code()
	{
		return $this->error_code;
	}

	public function get_response_aux()
	{
		return $this->response_aux;
	}

	public function save_fake($data)
	{
		$this->data = $data;

		if ($this->block_id)
		{
			$row = $this->db->get_row("
				SELECT id, user_id, fake_for_id, is_fake, block_set_id
				FROM block_{$this->type}
				WHERE id = {$this->block_id}
			");
			if (!$row)
			{
				$this->error_code = BLOCK_SET_ERROR_BLOCK_NOT_EXISTS;
				return false;
			}
			elseif (intval($this->block_set_id) != intval($data["block_set_id"]))
			{
				$this->error_code = BLOCK_SET_ERROR_BLOCK_NOT_EXISTS;
				return false;
			}
			elseif ($row["is_fake"] && $row["user_id"] != $this->user->get_user_id())
			{
				$this->error_code = BLOCK_SET_ERROR_BLOCK_NOT_EXISTS;
				return false;
			}
			else
			{
				$this->block_fake_for_id = $row["fake_for_id"];
				$this->is_fake = $row["is_fake"];
			}
		}
		if (!$this->check())
		{
			return false;
		}
		else
		{
			return $this->save_fake_to_db();
		}
	}

	abstract protected function check();

	protected function save_fake_to_db()
	{
		if ($this->block_id)
		{
			if ($this->is_fake)
			{
				return $this->save_fake_for_fake_to_db();
			}
			else
			{
				return $this->save_fake_for_real_to_db();
			}
		}
		else
		{
			return $this->save_new_fake_to_db();
		}
	}

	abstract protected function save_fake_for_fake_to_db();

	abstract protected function save_fake_for_real_to_db();

	abstract protected function save_new_fake_to_db();

	abstract public function fake_to_real();

	abstract public function delete();

	abstract public function get_type();
}

?>
