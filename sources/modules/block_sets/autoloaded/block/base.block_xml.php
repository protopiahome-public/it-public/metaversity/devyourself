<?php

abstract class base_block_xml
{

	protected $block_id;
	protected $type;

	/**
	 * @var db
	 */
	protected $db;

	public function __construct($block_id)
	{
		global $db;
		$this->db = $db;

		$this->block_id = $block_id;
		$this->type = $this->get_type();
	}

	public function get_block_id()
	{
		return $this->block_id;
	}

	public function get_xml()
	{
		$block_xdom = xdom::create("block");
		$block_xdom->set_attr("type", $this->type);
		$block_xdom->set_attr("id", $this->block_id);

		$this->fill_xml($block_xdom);

		return $block_xdom->get_xml(true);
	}

	abstract public function get_type();

	abstract protected function fill_xml(xdom $block_xdom);
}

?>