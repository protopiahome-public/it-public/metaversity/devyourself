<?php

require_once PATH_INTCMF . "/xml_builder.php";

class video_block_xml extends base_block_xml
{

	protected function fill_xml(xdom $block_xdom)
	{
		$data = $this->db->get_row("SELECT * FROM block_video WHERE id = {$this->block_id}");

		$block_xdom->set_attr("title", $data["title"]);
		$block_xdom->set_attr("url", $data["url"]);
		$block_xdom->set_attr("html", $data["html"]);
		$content_xdom = xdom::create_from_string("<html><div class=\"xhtml\">{$data["html"]}</div></html>");
		$block_xdom->import_xdom($content_xdom);
	}

	public function get_type()
	{
		return "video";
	}

}

?>