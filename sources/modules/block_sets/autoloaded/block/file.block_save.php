<?php

define("BLOCK_SET_ERROR_FILE__OFFSET", 1000);
define("BLOCK_SET_ERROR_FILE_SIZE", 1101);
define("BLOCK_SET_ERROR_FILE_NO_FILE", 1102);
define("BLOCK_SET_ERROR_FILE_COPY", 1103);

class file_block_save extends base_block_save
{

	protected $allowed_extensions_types = array(
		"pdf" => array("pdf"),
		"psd" => array("psd"),
		"flash" => array("swf"),
		"ssheet" => array("xls", "xlsx", "ods"),
		"doc" => array("doc", "docx", "odt"),
		"richtxt" => array("rtf", "ps", "djvu"),
		"txt" => array("txt"),
		"img" => array("png", "jpg", "jpeg", "gif", "bmp", "ico", "tiff", "tif", "svg"),
		"slides" => array("ppt", "pptx", "odp"),
		"archive" => array("zip", "rar", "7z", "tar", "bzip", "bz", "bz2", "gzip", "gz")
	);
	protected $file_data;
	protected $target_ext;
	protected $target_type;
	protected $target_dir;
	protected $title;

	protected function check()
	{
		// File upload test
		$max_file_size = 3 * 1024 * 1024; // DTF config???

		$upload_exists = isset($_FILES["file"]);
		if ($upload_exists)
		{
			$upload_error = $_FILES["file"]["error"];
		}

		if ($upload_exists and !$upload_error)
		{
			// Success (may be)

			$this->file_data = $_FILES["file"];
			if ($this->file_data["size"] > $max_file_size)
			{
				$this->error_code = BLOCK_SET_ERROR_FILE_SIZE;
				return false;
			}
			else
			{
				return true;
			}
		}
		elseif (!$upload_exists)
		{
			// Edit mode

			if ($this->block_id > 0)
			{
				return true;
			}
			else
			{
				$this->error_code = BLOCK_SET_ERROR_FILE_NO_FILE;
				return false;
			}
		}
		else
		{
			// Fail

			$file_error_code = $upload_exists ? $upload_error : BLOCK_SET_ERROR_FILE_NO_FILE;
			$this->error_code = BLOCK_SET_ERROR_FILE__OFFSET + $file_error_code;
			return false;
		}
	}

	protected function save_fake_for_fake_to_db()
	{
		if (isset($_FILES["file"]))
		{
			$this->prepare_uploaded_file();

			$old_file_path = $this->get_file_path($this->block_id, null, true);
			unlink_safe($old_file_path);

			$data = array(
				"title" => trim($this->data["title"]) !== "" ? preg_replace("#[\x{00}-\x{1F}]#", "", trim($this->data["title"])) : "NONAME",
				"ext" => $this->target_ext,
				"type" => $this->target_type,
				"file_name" => preg_replace("#[\x{00}-\x{1F}]#", "", $this->file_data["name"]),
				"size" => $this->file_data["size"],
				"is_fake" => 1,
			);

			foreach ($data as &$val)
			{
				$val = "'" . $this->db->escape($val) . "'";
			}

			$data["edit_time"] = "NOW()";

			$this->db->update_by_array("block_file", $data, "id = {$this->block_id}");

			$this->block_fake_id = $this->block_id;

			return $this->save_uploaded_file();
		}
		else
		{
			$data = array(
				"title" => trim($this->data["title"]) !== "" ? preg_replace("#[\x{00}-\x{1F}]#", "", trim($this->data["title"])) : "NONAME",
			);

			foreach ($data as &$val)
			{
				$val = "'" . $this->db->escape($val) . "'";
			}

			$data["edit_time"] = "NOW()";

			$this->db->update_by_array("block_file", $data, "id = {$this->block_id}");

			$this->block_fake_id = $this->block_id;

			return true;
		}
	}

	protected function save_fake_for_real_to_db()
	{
		if (isset($_FILES["file"]))
		{
			$this->prepare_uploaded_file();

			$data = array(
				"user_id" => $this->user->get_user_id(),
				"title" => trim($this->data["title"]) !== "" ? preg_replace("#[\x{00}-\x{1F}]#", "", trim($this->data["title"])) : "NONAME",
				"ext" => $this->target_ext,
				"type" => $this->target_type,
				"file_name" => preg_replace("#[\x{00}-\x{1F}]#", "", $this->file_data["name"]),
				"size" => $this->file_data["size"],
				"is_fake" => 1,
			);

			$data["block_set_id"] = $this->block_set_id;
			$data["fake_for_id"] = $this->block_id;

			foreach ($data as &$val)
			{
				$val = "'" . $this->db->escape($val) . "'";
			}

			$data["add_time"] = "NOW()";
			$data["edit_time"] = "NOW()";

			$this->db->insert_by_array("block_file", $data);

			$this->block_fake_id = $this->db->get_last_id();

			return $this->save_uploaded_file();
		}
		else
		{
			$data = $this->db->get_row("SELECT * FROM block_file WHERE id = {$this->block_id}");
			unset($data["id"]);

			$data["title"] = trim($this->data["title"]) !== "" ? preg_replace("#[\x{00}-\x{1F}]#", "", trim($this->data["title"])) : "NONAME";
			$data["is_fake"] = 1;

			$data["block_set_id"] = $this->block_set_id;
			$data["fake_for_id"] = $this->block_id;

			foreach ($data as &$val)
			{
				$val = "'" . $this->db->escape($val) . "'";
			}

			$data["add_time"] = "NOW()";
			$data["edit_time"] = "NOW()";

			$this->db->insert_by_array("block_file", $data);

			$this->block_fake_id = $this->db->get_last_id();

			return true;
		}
	}

	protected function save_new_fake_to_db()
	{
		$this->prepare_uploaded_file();

		$data = array(
			"user_id" => $this->user->get_user_id(),
			"title" => trim($this->data["title"]) !== "" ? preg_replace("#[\x{00}-\x{1F}]#", "", trim($this->data["title"])) : "NONAME",
			"ext" => $this->target_ext,
			"type" => $this->target_type,
			"file_name" => preg_replace("#[\x{00}-\x{1F}]#", "", $this->file_data["name"]),
			"size" => $this->file_data["size"],
			"is_fake" => 1,
		);

		if ($this->block_set_id)
		{
			$data["block_set_id"] = $this->block_set_id;
		}

		foreach ($data as &$val)
		{
			$val = "'" . $this->db->escape($val) . "'";
		}

		$data["add_time"] = "NOW()";
		$data["edit_time"] = "NOW()";

		$this->db->insert_by_array("block_file", $data);

		$this->block_fake_id = $this->db->get_last_id();

		return $this->save_uploaded_file();
	}

	protected function prepare_uploaded_file()
	{
		$this->target_ext = preg_match("/(\.[a-zA-Z0-9_]{1,4})$/", $this->file_data["name"], $matches) ? $matches[1] : ".bin";
		$this->target_ext = strtolower($this->target_ext);
		$this->data["title"] = isset($this->data["title"]) ? $this->data["title"] : "";

		$this->target_type = "";
		foreach ($this->allowed_extensions_types as $type => $extensions)
		{
			if (in_array(substr($this->target_ext, 1), $extensions))
			{
				$this->target_type = $type;
			}
		}
		if (!$this->target_type)
		{
			$this->target_ext = ".bin";
		}
	}

	protected function save_uploaded_file()
	{
		$target_path = $this->get_file_path($this->block_fake_id, $this->target_ext, true);
		/* Filesize check is definitely required on Windows - because
		 * if we have lack of target disk space, php copies only a part 
		 * of the file and says 'OK' (i. e. move_uploaded_file() 
		 * returns true). I did not test it on Linux.
		 */
		if (move_uploaded_file($this->file_data["tmp_name"], $target_path)
			&& filesize($target_path) == $this->file_data["size"])
		{
			return true;
		}
		else
		{
			unlink_safe($target_path); //@todo original fake?

			$this->error_code = BLOCK_SET_ERROR_FILE_COPY;
			return false;
		}
	}

	public function fake_to_real()
	{
		$data = $this->db->get_row("SELECT * FROM block_file WHERE id = {$this->block_id}");

		$fake_file_path = $this->get_file_path($data["id"], $data["ext"], true);

		if (file_exists($fake_file_path))
		{
			if ($data["fake_for_id"])
			{
				$old_file_path = $this->get_file_path($data["fake_for_id"]);
				unlink_safe($old_file_path);
			}

			$real_file_path = $this->get_file_path($data["fake_for_id"] ? $data["fake_for_id"] : $data["id"], $data["ext"]);
			rename_safe($fake_file_path, $real_file_path);
		}
	}

	public function delete()
	{
		$file_path = $this->get_file_path($this->block_id);
		unlink_safe($file_path);
	}

	protected function get_file_path($id, $ext = null, $is_fake = false)
	{
		$target_dir = PATH_PRIVATE_DATA . "/block_set/file/";

		if (!$ext)
		{
			$ext = $this->db->get_row("SELECT ext FROM block_file WHERE id = {$id}");
		}

		return $target_dir . ($is_fake ? "fake_" : "") . $id . $ext;
	}

	public function get_type()
	{
		return "file";
	}

}

?>