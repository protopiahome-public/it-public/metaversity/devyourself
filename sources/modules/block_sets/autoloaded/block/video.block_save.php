<?php

require_once (PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php");

define("BLOCK_SET_ERROR_VIDEO__OFFSET", 2000);
define("BLOCK_SET_ERROR_VIDEO_BAD_CODE", 2001);
define("BLOCK_SET_ERROR_VIDEO_UNSUPPORTED_SERVICE", 2002);

class video_block_save extends base_block_save
{

	protected $services = array(
		"youtube.com" => array(
			"regexp" => "/v=([a-zA-Z0-9-\_]{4,40})/",
			"code" => '<object width="480" height="360"><param name="movie" value="http://www.youtube.com/v/%KEY%?version=3&amp;hl=ru_RU"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/%KEY%?version=3&amp;hl=ru_RU" type="application/x-shockwave-flash" width="480" height="360" allowscriptaccess="always" allowfullscreen="true"></embed></object>',
		),
		"youtu.be" => array(
			"regexp" => "/^([a-zA-Z0-9-]{4,40})/",
			"code" => '<object width="480" height="360"><param name="movie" value="http://www.youtube.com/v/%KEY%?version=3&amp;hl=ru_RU"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/%KEY%?version=3&amp;hl=ru_RU" type="application/x-shockwave-flash" width="480" height="360" allowscriptaccess="always" allowfullscreen="true"></embed></object>',
		),
		"smotri.com" => array(
			"regexp" => "/id=(v[a-zA-Z0-9-]{4,40})/",
			"code" => '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="480" height="360"><param name="movie" value="http://pics.smotri.com/player.swf?file=%KEY%&amp;bufferTime=3&amp;autoStart=false&amp;str_lang=rus&amp;xmlsource=http%3A%2F%2Fpics.smotri.com%2Fcskins%2Fblue%2Fskin_color.xml&amp;xmldatasource=http%3A%2F%2Fpics.smotri.com%2Fskin_ng.xml" /><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="true" /><param name="bgcolor" value="#ffffff" /><embed src="http://pics.smotri.com/player.swf?file=%KEY%&amp;bufferTime=3&amp;autoStart=false&amp;str_lang=rus&amp;xmlsource=http%3A%2F%2Fpics.smotri.com%2Fcskins%2Fblue%2Fskin_color.xml&amp;xmldatasource=http%3A%2F%2Fpics.smotri.com%2Fskin_ng.xml" quality="high" allowscriptaccess="always" allowfullscreen="true" wmode="opaque" width="480" height="360" type="application/x-shockwave-flash"></embed></object>',
		),
		"video.google.com" => array(
			"regexp" => "/docid=([a-zA-Z0-9-]{4,40})/",
			"code" => '<embed src="http://video.google.com/googleplayer.swf?docid=%KEY%&amp;hl=ru&amp;fs=true" width="480" height="360" allowFullScreen="true" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>',
		),
	);
	protected $title;
	protected $url = "";
	protected $code;

	protected function check()
	{
		$this->title = isset($this->data["title"]) ? $this->data["title"] : "";
		$this->title = mb_substr($this->title, 0, 255);

		$this->code = isset($this->data["code"]) ? trim($this->data["code"]) : "";

		$url = preg_replace("#^(https?://)?(www\.)?#i", "", $this->code);
		$is_url = strlen($url) != strlen($this->code);

		foreach ($this->services as $service_domain => $data)
		{
			if (str_begins($url, $service_domain . "/"))
			{
				$url2 = substr($url, strlen($service_domain) + 1);
				if (preg_match($data["regexp"], $url2, $matches))
				{
					$this->url = $this->code;
					$this->code = str_replace("%KEY%", $matches[1], $data["code"]);
					break;
				}
			}
		}

		if (!$this->url && $is_url)
		{
			$this->error_code = BLOCK_SET_ERROR_VIDEO_UNSUPPORTED_SERVICE;
			preg_match("#([^/]+)#", $url, $matches);
			$this->response_aux["service_domain"] = $matches[1];
			return false;
		}

		$this->code = text_processor::tidy($this->code, true);
		if (!preg_match("/<(object|embed|iframe)/i", $this->code))
		{
			$this->error_code = BLOCK_SET_ERROR_VIDEO_BAD_CODE;
			return false;
		}

		return true;
	}

	protected function save_fake_for_fake_to_db()
	{
		$data = $this->get_prepared_data();

		$data["edit_time"] = "NOW()";

		$this->db->update_by_array("block_video", $data, "id = {$this->block_id}");

		$this->block_fake_id = $this->block_id;
		
		return true;
	}

	protected function save_fake_for_real_to_db()
	{
		$data = $this->get_prepared_data();

		$data["block_set_id"] = $this->block_set_id;
		$data["fake_for_id"] = $this->block_id;

		$data["add_time"] = "NOW()";
		$data["edit_time"] = "NOW()";

		$this->db->insert_by_array("block_video", $data);

		$this->block_fake_id = $this->db->get_last_id();
		
		return true;
	}

	protected function save_new_fake_to_db()
	{
		$data = $this->get_prepared_data();

		if ($this->block_set_id)
		{
			$data["block_set_id"] = $this->block_set_id;
		}

		$data["add_time"] = "NOW()";
		$data["edit_time"] = "NOW()";

		$this->db->insert_by_array("block_video", $data);

		$this->block_fake_id = $this->db->get_last_id();
		
		return true;
	}

	protected function get_prepared_data()
	{
		$data = array(
			"title" => $this->title,
			"url" => $this->url,
			"html" => $this->code,
			"is_fake" => 1,
			"user_id" => $this->user->get_user_id(),
		);

		foreach ($data as &$val)
		{
			$val = "'" . $this->db->escape($val) . "'";
		}
		return $data;
	}

	public function fake_to_real()
	{
		// Nothing to do
	}

	public function delete()
	{
		// Nothing to do
	}
	
	public function get_type()
	{
		return "video";
	}

}

?>
