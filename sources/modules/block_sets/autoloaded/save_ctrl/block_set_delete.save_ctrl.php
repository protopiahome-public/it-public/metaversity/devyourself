<?php

class block_set_delete_save_ctrl extends base_save_ctrl
{

	protected $id;
	protected $allowed_block_types = array("file", "video");

	/**
	 *
	 * @var base_block_save
	 */
	protected $block_save;

	public function __construct($id)
	{
		$this->id = $id;
		parent::__construct();
	}

	public function commit()
	{
		foreach ($this->allowed_block_types as $type)
		{
			$block_dbres = $this->db->sql("
					SELECT * 
					FROM block_{$type}
					WHERE block_set_id = {$this->id} AND is_fake = false
				");

			while ($block_dbrow = $this->db->get_row($block_dbres))
			{
				$save_class = "{$type}_block_save";
				$this->block_save = new $save_class($block_dbrow["id"], $this->id);
				$this->block_save->delete();
			}

			$this->db->sql("
				DELETE FROM block_{$type}
				WHERE block_set_id = {$this->id} AND is_fake = false
			");
		}

		$this->db->sql("
			DELETE FROM block_set
			WHERE id = {$this->id}
		");
		
		return true;
	}
	
	public function clean_cache()
	{
		block_set_cache_tag::init($this->id)->update();
	}

}

?>
