<?php

require_once (PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php");

class block_set_save_ctrl extends base_save_ctrl
{

	protected $id;
	protected $data;
	protected $allowed_block_types = array("file", "video");
	protected $xml;
	protected $object_table;
	protected $object_id;
	protected $javascript_error = false;
	protected $block_type_ids;
	protected $with_cut;

	public function __construct($data, $with_cut = false)
	{
		$this->data = json_decode($data, true);
		$this->with_cut = $with_cut;
		parent::__construct();
	}

	public function set_block_set_id($block_set_id)
	{
		$this->id = $block_set_id;
	}

	public function set_object_data($table, $id)
	{
		$this->object_table = $table;
		$this->object_id = $id;
	}

	public function start()
	{
		if ($this->id)
		{
			$block_set_exists = $this->db->row_exists("
				SELECT * 
				FROM block_set 
				WHERE id = {$this->id}
				FOR UPDATE
			");
			if (!$block_set_exists)
			{
				return false;
			}
		}

		if (!is_array($this->data))
		{
			$this->javascript_error = true;
			return true;
		}

		foreach ($this->allowed_block_types as $type)
		{
			$this->db->sql("
				SELECT * 
				FROM block_{$type}
				WHERE block_set_id = {$this->id}
				FOR UPDATE
			");
		}

		$already_has_cut = false;
		foreach ($this->data as $block_data)
		{
			$user_id = $this->user->get_user_id();

			if (!isset($block_data["type"]))
			{
				return false;
			}

			if (!in_array($block_data["type"], $this->allowed_block_types) and $block_data["type"] != 'text' and $block_data["type"] != 'cut')
			{
				return false;
			}

			if ($block_data["type"] == "text" and (!isset($block_data["html"]) or is_array($block_data["html"])))
			{
				return false;
			}

			if ($block_data["type"] == "cut")
			{
				if (!$this->with_cut)
				{
					return false;
				}
				if ($already_has_cut)
				{
					return false;
				}
				if (!isset($block_data["title"]) or trim($block_data["title"]) == '' or is_array($block_data["title"]))
				{
					return false;
				}

				$already_has_cut = true;
			}

			if ($block_data["type"] != "text" and $block_data["type"] != "cut")
			{
				if (!isset($block_data["id"]) or !is_good_id($block_data["id"]))
				{
					return false;
				}

				$block_set_sql_eq = $this->id ? "= {$this->id}" : "IS NULL";
				$block_exists = $this->db->row_exists("
					SELECT * 
					FROM block_{$block_data["type"]} 
					WHERE id = {$block_data["id"]} AND (user_id = {$user_id} OR is_fake = 0) AND block_set_id {$block_set_sql_eq}
					FOR UPDATE
				");
				if (!$block_exists)
				{
					return false;
				}
			}
		}

		return true;
	}

	public function check()
	{
		if ($this->javascript_error)
		{
			$this->pass_info->write_error("javascript");
			return false;
		}

		return true;
	}

	public function rollback()
	{
		if ($this->javascript_error)
		{
			return false;
		}

		$this->xml = "";

		foreach ($this->data as $block_data)
		{
			if ($block_data["type"] == "text")
			{
				$html = $block_data["html"];
				$html = text_processor::tidy($html, true);

				$this->xml .= <<<EOF
<block type="text">
	<html>
		{$html}
	</html>
</block>
EOF;
			}
			elseif ($block_data["type"] == "cut")
			{
				$title = $block_data["title"];
				$title = htmlspecialchars($title);

				$this->xml .= <<<EOF
<block type="cut" title="{$title}"/>
EOF;
			}
			else
			{
				$block_xml = $this->get_block_xml($block_data["type"], $block_data["id"]);
				$this->xml .= $block_xml->get_xml();
			}
		}
		$with_cut_attr = intval($this->with_cut);
		$this->xml = <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<block_set with_cut="{$with_cut_attr}" id="{$this->id}">
{$this->xml}
</block_set>
EOF;

		$pass_info_node = $this->pass_info->get_xdom();
		$block_set_node = xdom::create_from_string($this->xml);
		$pass_info_node->import_xdom($block_set_node);
	}

	public function commit()
	{
		$this->xml = "";
		$this->block_type_ids = array();
		foreach ($this->allowed_block_types as $type)
		{
			$this->block_type_ids[$type] = array();
		}

		$this->save_blocks();
		$this->delete_blocks();
		if (!$this->id)
		{
			$this->create_block_set();
		}

		$this->xml = <<<EOF
<block_set with_cut="x" id="{$this->id}">
{$this->xml}
</block_set>
EOF;
		$xml_escaped = $this->db->escape($this->xml);
		$this->db->sql("
			UPDATE block_set
			SET xml = '{$xml_escaped}',
				edit_time = NOW()
			WHERE id = {$this->id}
		");

		if ($this->object_table)
		{
			$this->db->sql("
				UPDATE `{$this->object_table}`
				SET block_set_id = {$this->id}
				WHERE id = {$this->object_id}
			");
		}
		return true;
	}

	protected function save_blocks()
	{
		foreach ($this->data as $block_data)
		{
			if ($block_data["type"] == "text")
			{
				$html = $block_data["html"];
				$html = text_processor::tidy($html, true);

				$this->xml .= <<<EOF
<block type="text">
	<html>
		{$html}
	</html>
</block>
EOF;
			}
			elseif ($block_data["type"] == "cut")
			{
				$title = $block_data["title"];
				$title = htmlspecialchars($title);

				$this->xml .= <<<EOF
<block type="cut" title="{$title}"/>
EOF;
			}
			else
			{
				$block_save = $this->get_block_save($block_data["type"], $block_data["id"]);

				$block_dbrow = $this->db->get_row("
					SELECT * 
					FROM block_{$block_data["type"]} 
					WHERE id = {$block_data["id"]}
				");

				if ($block_dbrow["is_fake"])
				{
					// New/changed block
					$block_save->fake_to_real();

					if ($block_dbrow["fake_for_id"])
					{
						// Changed block. Move data from fake to original and delete fake
						$data = $block_dbrow;

						unset($data["id"]);
						unset($data["fake_for_id"]);
						unset($data["is_fake"]);
						unset($data["add_time"]);
						unset($data["edit_time"]);

						foreach ($data as &$val)
						{
							$val = "'" . $this->db->escape($val) . "'";
						}

						$data["edit_time"] = 'NOW()';

						$this->db->update_by_array("block_{$block_data["type"]}", $data, "id = {$block_dbrow["fake_for_id"]}");
						$this->db->sql("DELETE FROM block_{$block_data["type"]} WHERE id = {$block_dbrow["id"]}");

						$real_block_id = $block_dbrow["fake_for_id"];
					}
					else
					{
						// New block. Transform fake to new block
						$data = array(
							"fake_for_id" => "NULL",
							"is_fake" => "0",
							"add_time" => "NOW()",
							"edit_time" => "NOW()"
						);

						$this->db->update_by_array("block_{$block_data["type"]}", $data, "id = {$block_dbrow["id"]}");

						$real_block_id = $block_data["id"];
					}
				}
				else
				{
					// Not fake block (not changed), save nothing
					$real_block_id = $block_data["id"];
				}

				$block_xml = $this->get_block_xml($block_data["type"], $real_block_id);
				$this->xml .= $block_xml->get_xml();
				$this->block_type_ids[$block_data["type"]][] = $real_block_id;
			}
		}
	}

	protected function delete_blocks()
	{
		if ($this->id)
		{
			foreach ($this->block_type_ids as $type => $block_ids)
			{
				$block_ids_sql = count($block_ids) ? implode(", ", $block_ids) : "-1";
				$block_dbres = $this->db->sql("
					SELECT * 
					FROM block_{$type}
					WHERE id NOT IN ({$block_ids_sql})
						AND block_set_id = {$this->id}
						AND is_fake = 0
				");

				while ($block_dbrow = $this->db->get_row($block_dbres))
				{
					$block_save = $this->get_block_save($type, $block_dbrow["id"]);
					$block_save->delete();
				}

				$this->db->sql("
					DELETE FROM block_{$type}
					WHERE id NOT IN ({$block_ids_sql})
						AND block_set_id = {$this->id}
						AND is_fake = 0
				");
			}
		}
	}

	public function create_block_set()
	{

		$this->db->sql("INSERT INTO block_set(xml, add_time) VALUES('', NOW())");
		$this->id = $this->db->get_last_id();

		foreach ($this->block_type_ids as $type => $block_ids)
		{
			$block_ids_sql = count($block_ids) ? implode(", ", $block_ids) : "-1";
			$this->db->sql("
				UPDATE block_{$type}
				SET block_set_id = {$this->id}
				WHERE id IN ({$block_ids_sql})
			");
		}
	}

	/**
	 * @return base_block_xml
	 */
	protected function get_block_xml($type, $id)
	{
		$xml_class = "{$type}_block_xml";
		return new $xml_class($id);
	}

	/**
	 * @return base_block_save
	 */
	protected function get_block_save($type, $id)
	{
		$save_class = "{$type}_block_save";
		return new $save_class($id, $this->id);
	}

	public function get_id()
	{
		return $this->id;
	}

	public function clean_cache()
	{
		block_set_cache_tag::init($this->id)->update();
	}

}

?>
