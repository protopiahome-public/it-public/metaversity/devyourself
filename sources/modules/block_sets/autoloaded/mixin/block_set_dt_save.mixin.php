<?php

class block_set_dt_save_mixin extends base_mixin
{

	/**
	 *
	 * @var save_loader
	 */
	protected $save_loader;

	/**
	 *
	 * @var base_dt
	 */
	protected $dt;

	/**
	 * @var block_set_save_ctrl
	 */
	protected $block_set_save_ctrl;
	protected $old_db_row;
	protected $last_id;

	public function init()
	{
		$this->block_set_save_ctrl = new block_set_save_ctrl(POST("block_set_data"));
		$this->save_loader->add_ctrl($this->block_set_save_ctrl);
	}

	public function on_after_start()
	{
		$this->block_set_save_ctrl->set_block_set_id($this->old_db_row ? intval($this->old_db_row["block_set_id"]) : 0);
		return true;
	}

	public function on_after_commit()
	{
		$this->block_set_save_ctrl->set_object_data($this->dt->get_db_table(), $this->last_id);
	}

}

?>
