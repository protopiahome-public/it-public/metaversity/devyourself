<?php

class block_set_cut_dt_save_mixin extends block_set_dt_save_mixin
{

	/**
	 * @var block_set_cut_save_ctrl
	 */
	protected $block_set_save_ctrl;

	public function init()
	{
		$this->block_set_save_ctrl = new block_set_cut_save_ctrl(POST("block_set_data"));
		$this->save_loader->add_ctrl($this->block_set_save_ctrl);
	}

}

?>
