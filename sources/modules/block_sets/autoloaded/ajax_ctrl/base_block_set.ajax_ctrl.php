<?php

require_once PATH_CORE . "/loader/xml_loader.php";

define("BLOCK_SET_ERROR_UNKNOWN_BLOCK_TYPE", 100);
define("BLOCK_SET_ERROR_NO_RIGHTS", 101);
define("BLOCK_SET_ERROR_INCORRECT_BLOCK_ID", 102);
define("BLOCK_SET_ERROR_INCORRECT_BLOCK_SET_ID", 103);

abstract class base_block_set_ajax_ctrl extends base_ajax_ctrl
{

	protected $block_id;
	protected $block_type;
	protected $allowed_block_types = array("file", "video");
	protected $is_block_add;
	protected $block_set_id;

	/**
	 *
	 * @var base_block_save
	 */
	protected $block_save = null;

	/**
	 *
	 * @var base_block_xml
	 */
	protected $block_xml;
	protected $dialog_key;
	protected $error_code;

	public function get_data()
	{
		$this->dialog_key = isset($_POST["dialog_key"]) ? $_POST["dialog_key"] : "NOT_SENT";

		if (!$this->block_set_check())
		{
			return $this->get_fail_response();
		}

		$this->db->begin();
		if (!$fake_id = $this->save())
		{
			$this->db->rollback();
			return $this->get_fail_response();
		}
		$this->db->commit();


		$response_base = array(
			"status" => "OK",
			"data" => array("html" => $this->get_xml($fake_id)),
			"dialog_key" => $this->dialog_key,
		);
		$response_aux = $this->block_save->get_response_aux();
		return array_merge($response_base, $response_aux);
	}

	protected function block_set_check()
	{
		if (!is_good_num($this->block_set_id = POST("block_set_id")))
		{
			$this->set_error_code(BLOCK_SET_ERROR_INCORRECT_BLOCK_SET_ID);
			return false;
		}

		if (!is_good_num($this->block_id = POST("block_id")))
		{
			$this->set_error_code(BLOCK_SET_ERROR_INCORRECT_BLOCK_ID);
			return false;
		}
		$this->is_block_add = $this->block_id == 0;

		if (!in_array($this->block_type = POST("block_type"), $this->allowed_block_types))
		{
			$this->set_error_code(BLOCK_SET_ERROR_UNKNOWN_BLOCK_TYPE);
			return false;
		}

		return true;
	}

	protected function save()
	{
		$save_class = "{$this->block_type}_block_save";
		$this->block_save = new $save_class($this->block_id, $this->block_set_id);

		if (!$this->block_save->save_fake($_POST))
		{
			$this->set_error_code($this->block_save->get_error_code());
			return false;
		}

		return $this->block_save->get_fake_id();
	}

	protected function get_xml($id)
	{
		$xml_class = "{$this->block_type}_block_xml";
		$this->block_xml = new $xml_class($id);

		$xml_loader = new xml_loader();

		$xml_loader->add_xml(new string_to_xml_xml_ctrl($this->block_xml->get_xml()));

		$xml_loader->add_xslt("ajax/block_set.ajax", "block_sets");
		$xml_loader->add_xslt("block/{$this->block_type}.block", "block_sets");

		$object_url = $this->get_object_url();
		$xml_loader->add_xml(new string_to_xml_xml_ctrl("<object_url>{$object_url}</object_url>"));

		$this->modify_xml($xml_loader);

		$xml_loader->run();

		return $xml_loader->get_content();
	}

	protected function modify_xml(xml_loader $xml_loader)
	{
		return;
	}

	abstract protected function get_object_url();

	private function set_error_code($error_code)
	{
		$this->error_code = $error_code;
	}

	private function get_fail_response()
	{
		$response_base = array(
			"status" => "ERROR",
			"error_code" => $this->error_code,
			"dialog_key" => $this->dialog_key,
		);
		$response_aux = $this->block_save ? $this->block_save->get_response_aux() : array();
		return array_merge($response_base, $response_aux);
	}

}

?>