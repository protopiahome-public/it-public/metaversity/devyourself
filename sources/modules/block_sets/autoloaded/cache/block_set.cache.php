<?php

class block_set_cache extends base_cache
{

	public static function init($block_set_id)
	{
		$tag_keys = array();
		$tag_keys[] = block_set_cache_tag::init($block_set_id)->get_key();
		return parent::get_cache(__CLASS__, $block_set_id, $tag_keys);
	}

}

?>