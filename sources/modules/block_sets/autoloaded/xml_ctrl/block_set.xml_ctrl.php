<?php

class block_set_xml_ctrl extends base_xml_ctrl
{

	protected $id;
	protected $with_cut;

	const CUT_ATTR_OFFSET = 21;

	public function __construct($id = null, $with_cut = false)
	{
		$this->id = $id;
		$this->with_cut = $with_cut;
		parent::__construct();
	}

	public function get_xml()
	{
		if ($this->id)
		{
			$xml = block_set_cache::init($this->id)->get();
			if (is_null($xml))
			{
				$this->cache_state = XML_CTRL_CACHE_STATE_MISS;
				$block_set_row = $this->db->get_row("
					SELECT xml
					FROM block_set
					WHERE id = {$this->id}
				");
				if (!$block_set_row)
				{
					$this->set_error_404();
					return;
				}
				$xml = $block_set_row["xml"];
				block_set_cache::init($this->id)->set($xml);
			}
			else
			{
				$this->cache_state = XML_CTRL_CACHE_STATE_CACHED;
			}
			$xml[self::CUT_ATTR_OFFSET] = intval($this->with_cut);
			return $xml;
		}
		else
		{
			return '<block_set with_cut="' . intval($this->with_cut) . '" id="0"/>';
		}
	}

}

?>