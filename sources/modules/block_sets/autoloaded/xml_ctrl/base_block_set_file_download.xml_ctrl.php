<?php

abstract class base_block_set_file_download_xml_ctrl extends base_xml_ctrl
{

	protected $id;
	protected $data;
	protected $is_add;
	protected $allowed_extensions_types = array(
		"pdf" => array("pdf"),
		"psd" => array("psd"),
		"flash" => array("swf"),
		"ssheet" => array("xls", "xlsx", "ods"),
		"doc" => array("doc", "docx", "odt"),
		"richtxt" => array("rtf", "ps", "djvu"),
		"txt" => array("txt"),
		"img" => array("png", "jpg", "jpeg", "gif", "bmp", "ico", "tiff", "tif", "svg"),
		"slides" => array("ppt", "pptx", "odp"),
		"archive" => array("zip", "rar", "7z", "tar", "bzip", "bz", "gzip", "gz")
	);

	public function __construct($block_file_id)
	{
		$this->id = $block_file_id;

		parent::__construct();
	}

	public function get_xml()
	{
		if (!is_good_id($this->id))
		{
			$this->set_error_404();
			return false;
		}

		if (!$this->data = $this->db->get_row("SELECT * FROM block_file WHERE id = {$this->id}"))
		{
			$this->set_error_404();
			return false;
		}

		$this->is_add = !$this->data["block_set_id"];
		if (!$this->is_add && !$this->check_file_rights())
		{
			$this->set_error_404();
			return false;
		}

		$storage_dir = PATH_PRIVATE_DATA . "/block_set/file/";

		if ($this->data["is_fake"])
		{
			$storage_file_name = "fake_" . $this->data["id"] . $this->data["ext"];
			if (!file_exists($storage_dir . $storage_file_name) && $this->data["fake_for_id"])
			{
				$storage_file_name = $this->data["fake_for_id"] . $this->data["ext"];
			}
		}
		else
		{
			$storage_file_name = $this->data["id"] . $this->data["ext"];
		}

		$file_path = $storage_dir . $storage_file_name;

		$file_name = $this->data["file_name"];

		if (file_exists($file_path))
		{
			$content_disposition = "Inline";
			if ($this->data["ext"] == ".jpg")
			{
				response::set_content_type("image/jpeg");
			}
			elseif ($this->data["ext"] == ".jpeg")
			{
				response::set_content_type("image/jpeg");
			}
			elseif ($this->data["ext"] == ".gif")
			{
				response::set_content_type("image/gif");
			}
			elseif ($this->data["ext"] == ".png")
			{
				response::set_content_type("image/png");
			}
			elseif ($this->data["ext"] == ".txt")
			{
				response::set_content_type("text/plain");
			}
			else
			{
				response::set_content_type_attach();
				$content_disposition = "Attachment";
			}
			output_buffer::set_header("Content-Length", filesize($file_path));
			$browser = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "";
			if (preg_match("/MSIE/", $browser) and !preg_match("/Opera/i", $browser))
			{
				$file_name = iconv("UTF-8", "WINDOWS-1251", $file_name);
			}
			output_buffer::set_header("Content-Disposition", "{$content_disposition}; filename=\"{$file_name}\"");
			output_buffer::set_header("Cache-Control", "must-revalidate");
			response::set_content(file_get_contents_safe($file_path));
			return true;
		}
		else
		{
			response::set_content_text("Файл не найден на сервере");
			return true;
		}
		return true;
	}

	/**
	 * @return bool
	 */
	abstract protected function check_file_rights();
}

?>
