<?php

abstract class base_taxonomy
{

	const MAX_DEPTH = 12;

	/**
	 * @var error
	 */
	protected $error;

	/**
	 * @var request
	 */
	protected $request;
	
	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var user
	 */
	protected $user;

	/**
	 * @var xml_loader
	 */
	protected $xml_loader;

	/**
	 *
	 * @var base_taxonomy
	 */
	protected $parent_taxonomy;
	protected $parts = null;
	protected $parts_relative = null;
	protected $parts_offset = 0;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy = null, $parts_offset = 0)
	{
		global $error;
		$this->error = $error;

		global $request;
		$this->request = $request;
		
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		global $db;
		$this->db = $db;

		global $user;
		$this->user = $user;

		$this->xml_loader = $xml_loader;

		$this->parent_taxonomy = $parent_taxonomy;
		if ($this->parent_taxonomy)
		{
			$this->parts = $parent_taxonomy->get_parts();
			$this->parts_offset = $parent_taxonomy->get_parts_offset() + $parts_offset;
		}
	}

	abstract public function run();

	public function get_parts()
	{
		if (is_array($this->parts))
		{
			return $this->parts;
		}

		$this->parts = $this->request->get_all_folders_array();
		array_unshift($this->parts, null);
		for ($i = sizeof($this->parts); $i <= self::MAX_DEPTH; ++$i)
		{
			array_push($this->parts, null);
		}

		return $this->parts;
	}

	public function get_parts_relative()
	{
		if (is_array($this->parts_relative))
		{
			return $this->parts_relative;
		}
		if (!$this->parts_offset)
		{
			return $this->parts_relative = $this->get_parts();
		}

		$parts = $this->get_parts();
		$this->parts_relative = array_slice($parts, $this->parts_offset);
		$this->parts_relative[0] = null;
		return $this->parts_relative;
	}

	public function get_parts_offset()
	{
		return $this->parts_offset;
	}

	protected function set_redirect_url($url)
	{
		$redirect_url = $this->request->get_prefix() . "/";
		for ($i = 1; $i <= $this->parts_offset; $i++)
		{
			$redirect_url .= $this->parts[$i] . "/";
		}
		$redirect_url .= $url;
		$this->xml_loader->set_redirect_url($redirect_url);
	}

	protected function is_not_empty_get_param($key)
	{
		if (isset($_GET[$key]) and strlen($str = trim($_GET[$key])))
		{
			return $str;
		}
		return false;
	}

	protected function is_type_folder($folder_param, $type, $is_good_id = false)
	{
		if ($folder_param === null)
		{
			return false;
		}
		else
		{
			$len = strlen($type) + 1;
			if (substr($folder_param, 0, $len) == "{$type}-")
			{
				$data = substr($folder_param, $len);
				if ($is_good_id && !is_good_id($data))
				{
					return false;
				}
				return $data;
			}
			else
			{
				return false;
			}
		}
	}

	protected function is_page_folder($folder_param)
	{
		if ($folder_param === null)
		{
			return 1;
		}
		elseif (substr($folder_param, 0, 5) == "page-" && is_good_id($page = substr($folder_param, 5)))
		{
			return $page;
		}
		else
		{
			return false;
		}
	}

}

?>