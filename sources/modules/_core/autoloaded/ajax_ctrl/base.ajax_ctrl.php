<?php

abstract class base_ajax_ctrl extends base_ctrl
{

	/**
	 * @var ajax_loader
	 */
	protected $ajax_loader;

	protected $autostart_db_transaction = true;

	public function __construct()
	{
		global $ajax_loader;
		$this->ajax_loader = $ajax_loader;

		parent::__construct();
	}

	public function set_up()
	{
		return true;
	}

	public function on_before_start()
	{
		return true;
	}

	public function start()
	{
		return true;
	}

	public function on_after_start()
	{
		return true;
	}

	public function check_rights()
	{
		return true;
	}

	public function on_before_check()
	{
		return true;
	}

	public function check()
	{
		return true;
	}

	public function on_after_check()
	{
		return;
	}

	public function on_check_fail()
	{
		return;
	}

	public function on_before_commit()
	{
		return true;
	}

	public function commit()
	{
		return true;
	}

	public function on_after_commit()
	{
		return;
	}

	public function clean_cache()
	{
		return;
	}

	public function rollback()
	{
		return;
	}

	public function get_data()
	{
		return array();
	}

	public function autostart_db_transaction()
	{
		return $this->autostart_db_transaction;
	}

}

?>