<?php

abstract class base_access
{

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var user
	 */
	protected $user;

	/**
	 * @var base_access_maper
	 */
	protected $access_maper;

	/**
	 * @var base_access_fetcher
	 */
	protected $access_fetcher;
	protected $level = 0;
	protected $user_id;
	protected $required_statuses = array();
	protected $required_levels = array();

	protected function __construct($user_id = null)
	{
		global $db;
		$this->db = $db;

		global $user;
		$this->user = $user;

		$this->user_id = !is_null($user_id) ? $user_id : $this->user->get_user_id();

		$this->access_maper = $this->get_new_access_maper();
		$this->fill_required_levels();
		$this->fill_data();
	}

	/**
	 * @return base_access_maper
	 */
	abstract protected function get_new_access_maper();

	protected function fill_required_levels()
	{
		return;
	}
	
	abstract protected function fill_data();
	
	/**
	 * @return base_access_maper
	 */
	public function get_access_maper()
	{
		return $this->access_maper;
	}

	public function get_level()
	{
		return $this->level;
	}

	public function get_status()
	{
		return $this->access_maper->get_status_from_level($this->level);
	}

	public function get_required_statuses()
	{
		return $this->required_statuses;
	}

	public function get_required_levels()
	{
		return $this->required_levels;
	}

	public function get_required_status($name)
	{
		return $this->required_statuses[$name];
	}

	public function get_required_level($name)
	{
		return $this->required_levels[$name];
	}

	public function is_pretender_strict()
	{
		return $this->level == ACCESS_LEVEL_PRETENDER;
	}

	public function is_moderator_strict()
	{
		return $this->level == ACCESS_LEVEL_MODERATOR;
	}

	public function is_member()
	{
		return $this->level >= ACCESS_LEVEL_MEMBER;
	}

	public function has_member_rights()
	{
		return $this->user->is_admin() || $this->level >= ACCESS_LEVEL_MEMBER;
	}

	public function has_moderator_rights()
	{
		return $this->user->is_admin() || $this->level >= ACCESS_LEVEL_MODERATOR;
	}

	public function has_admin_rights()
	{
		return $this->user->is_admin() || $this->level >= ACCESS_LEVEL_ADMIN;
	}

	public function get_user_id()
	{
		return $this->user_id;
	}

}

?>