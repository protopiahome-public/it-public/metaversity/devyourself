<?php

abstract class base_obj
{

	/**
	 * @var db
	 */
	protected $db;
	protected $id;
	protected $data = array();
	protected $fake_delete = false;

	public function __construct($id, $lock = false)
	{
		global $db;
		$this->db = $db;

		$this->id = $id;
		$this->fill_data($lock);
	}

	public function exists()
	{
		return sizeof($this->data) > 0 && (!$this->fake_delete || !$this->get_param("is_deleted"));
	}

	public function get_id()
	{
		return $this->id;
	}

	abstract protected function fill_data($lock);

	public function get_param($key)
	{
		return isset($this->data[$key]) ? $this->data[$key] : null;
	}

}

?>