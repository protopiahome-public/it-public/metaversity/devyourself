<?php

require_once PATH_INTCMF . "/select_sql.php";

abstract class base_dt_edit_save_ctrl extends base_save_ctrl
{

	// Settings
	protected $dt_name;
	protected $axis_name;
	// Internal
	protected $id;
	protected $action;
	protected $last_id;
	protected $old_db_row;
	protected $updated_db_row;
	protected $update_array = array();

	/**
	 * @var base_dt
	 */
	protected $dt;
	protected $dtfs = array();
	protected $save_dtfs = array();

	final public function set_up()
	{
		$this->fill_id();
		$this->action = $this->id ? "dt_edit" : "dt_add";
		return true;
	}

	protected function fill_id()
	{
		$this->id = REQUEST("id");
		if (!is_good_id($this->id))
		{
			$this->id = -1;
		}
	}

	protected function dt_init()
	{
		$class_reflection = new ReflectionClass($this->dt_name . "_dt");
		$this->dt = $class_reflection->newInstance();
	}

	protected function on_after_dt_init()
	{
		return true;
	}

	final public function start()
	{
		if ($this->id < 0)
		{
			return false;
		}
		$this->dt_init();
		if (!$this->mixin_call_method_with_mixins("on_after_dt_init", array(), "chain"))
		{
			return false;
		}
		if ($this->id)
		{
			$this->old_db_row = $this->get_db_row();
			if (!$this->old_db_row)
			{
				return false;
			}
			$this->updated_db_row = $this->old_db_row;
		}
		else
		{
			$this->old_db_row = null;
			$this->updated_db_row = array();
		}
		return true;
	}

	final public function check()
	{
		$this->dtfs = $this->dt->get_fields_by_axis($this->axis_name);
		$this->fill_save_dtfs();
		$ok = true;
		foreach ($this->save_dtfs as $save_dtf_idx => $save_dtf)
		{
			/* @var $save_dtf base_save_dtf */
			$deps = $save_dtf->get_dtf()->get_dependencies();
			foreach ($deps as $dependency)
			{
				/* @var $dependency dtf_dependency */
				if ($this->updated_db_row[$dependency->get_inspected_column()] == $dependency->get_activation_value())
				{
					if ($dependency->get_actions() & INTCMF_DTF_DEP_ACTION_IMPORTANT and method_exists($save_dtf->get_dtf(), "set_importance"))
					{
						$save_dtf->get_dtf()->set_importance(true);
					}
					if ($dependency->get_actions() & INTCMF_DTF_DEP_ACTION_DO_NOT_STORE)
					{
						unset($this->save_dtfs[$save_dtf_idx]);
						continue 2;
					}
				}
			}
			$check_ok = $save_dtf->check();
			if ($check_ok)
			{
				$save_dtf->update_db_row($this->updated_db_row);
			}
			$ok = $check_ok && $ok;
		}
		return $ok;
	}

	final public function commit()
	{
		foreach ($this->save_dtfs as $save_dtf)
		{
			/* @var $save_dtf base_save_dtf */
			$save_dtf->get_fields_to_write($this->update_array);
		}
		if ($this->id)
		{
			$this->update_array["id"] = $this->id;
		}
		else
		{
			if ($this->dt->get_add_timestamp_column())
			{
				$this->update_array[$this->dt->get_add_timestamp_column()] = $this->db->get_now(true);
			}
		}
		if ($this->dt->get_edit_timestamp_column())
		{
			$this->update_array[$this->dt->get_edit_timestamp_column()] = $this->db->get_now(true);
		}
		if (sizeof($this->update_array))
		{
			if (!$this->id)
			{
				$this->db->insert_by_array($this->dt->get_db_table(), $this->update_array);
				$this->last_id = $this->db->get_last_id();
			}
			else
			{
				$this->db->update_by_array($this->dt->get_db_table(), $this->update_array, "id = {$this->id}");
				$this->last_id = $this->id;
			}
		}
		foreach ($this->save_dtfs as $save_dtf)
		{
			/* @var $save_dtf base_save_dtf */
			$save_dtf->commit($this->last_id);
		}
		$this->pass_info->write_info('SAVED', $this->last_id);
		$this->pass_info->write_info('action', $this->action);
		$this->set_success_redirect_url();
		return true;
	}

	protected function set_success_redirect_url()
	{
		if (isset($_REQUEST["retpath"]))
		{
			/* Here we process %ID% and %NAME% in retpath */
			$retpath = $_REQUEST["retpath"];
			$retpath = str_replace("%ID%", $this->last_id, $retpath);
			if (preg_match("/%[A-Z0-9_]+%/", $retpath))
			{
				$column = false;
				foreach ($this->dt->get_fields() as $dtf)
				{
					/* @var $dtf base_dtf */
					if ($dtf instanceof url_name_dtf)
					{
						$column = $dtf->get_field_name();
						break;
					}
				}
				if ($column)
				{
					$new_db_row = $this->get_db_row(false);
					$column_upper = strtoupper($column);
					$retpath = str_replace("%{$column_upper}%", $new_db_row[$column], $retpath);
				}
			}
			$this->set_redirect_url($retpath);
			return $retpath;
		}
		else
		{
			/* Here we process invalidation of the referrer url */
			$column = "";
			$url_prefix = "";
			foreach ($this->dt->get_fields() as $dtf)
			{
				/* @var $dtf base_dtf */
				if ($dtf instanceof url_name_dtf)
				{
					$column = $dtf->get_field_name();
					$url_prefix = $dtf->get_url_prefix();
					break;
				}
			}
			if ($column)
			{
				$new_db_row = $this->get_db_row(false);
				if ($new_db_row[$column] != $this->old_db_row[$column] and ($referrer = $this->request->get_referrer()))
				{
					$old_url = $url_prefix . $this->old_db_row[$column] . "/";
					$new_url = $url_prefix . $new_db_row[$column] . "/";
					require_once PATH_INTCMF . "/url.php";
					$referrer_url = new url($referrer);
					$referrer_folders = $referrer_url->get_folders_string();
					if (str_begins($referrer_folders, $old_url))
					{
						$redirect_url = $new_url . substr($referrer_folders, strlen($old_url)) . $referrer_url->get_params_string();
						$this->set_redirect_url($redirect_url);
					}
				}
			}
		}
		return parent::get_redirect_url();
	}

	final public function rollback()
	{
		foreach ($this->save_dtfs as $save_dtf)
		{
			/* @var $save_dtf base_save_dtf */
			$save_dtf->rollback();
		}
		$this->pass_info->dump_vars();
	}

	private function fill_save_dtfs()
	{
		foreach ($this->dtfs as $dtf)
		{
			/* @var $dtf base_dtf */
			$field_name = $dtf->get_field_name();
			$field_type = $dtf->get_field_type();
			$class_reflection = new ReflectionClass($field_type . "_save_dtf");
			$this->save_dtfs[$field_name] = $class_reflection->newInstance($dtf, $this->pass_info, $this->dt->get_db_table(), $this->id, $this->old_db_row);
		}
	}

	private function get_db_row($lock = true)
	{
		$id = $this->id ? $this->id : $this->last_id;
		if (!$id)
		{
			trigger_error("Incorrect call: \$this->id must be > 0");
		}
		$select_sql = new select_sql();
		$select_sql->add_from($this->dt->get_db_table() . " dt");
		$select_sql->add_select_fields("dt.*");
		$select_sql->add_where("dt.id = {$id}");
		if ($lock)
		{
			$select_sql->set_FOR_UPDATE_modifier();
		}
		$this->mixin_call_method_with_mixins("modify_sql", array($select_sql));
		return $this->db->get_row($select_sql->get_sql());
	}

	protected function modify_sql(select_sql $select_sql)
	{
		return;
	}

}

?>