<?php

require_once PATH_INTCMF . "/select_sql.php";

abstract class base_delete_save_ctrl extends base_save_ctrl
{

	// Settings
	protected $db_table;
	protected $pk_column = "id";
	// Internal
	protected $id;
	protected $old_db_row;

	final public function set_up()
	{
		if (!is_null(REQUEST("cancel")))
		{
			$url = REQUEST("cancelpath");
			if ($url)
			{
				$this->set_redirect_url($url);
			}
			else
			{
				$url = REQUEST("retpath");
				if ($url)
				{
					$this->set_redirect_url($url);
				}
			}
			$this->save_loader->stop_load();
		}
		return true;
	}

	final public function start()
	{
		$this->id = REQUEST("id");
		if (!is_good_id($this->id))
		{
			return false;
		}
		$this->old_db_row = $this->get_db_row();
		if (!$this->old_db_row)
		{
			return false;
		}
		return true;
	}

	protected function do_delete()
	{
		$this->db->sql("
			DELETE FROM 
			{$this->db_table}
			WHERE `{$this->pk_column}` = {$this->id}
		");
	}

	final public function commit()
	{
		$this->do_delete();
		$this->pass_info->write_info('DELETED', $this->id);
		return true;
	}

	private function get_db_row()
	{
		$select_sql = new select_sql();
		$select_sql->add_from($this->db_table);
		$select_sql->add_select_fields("*");
		$select_sql->add_where("id = {$this->id}");
		$select_sql->set_FOR_UPDATE_modifier();
		$this->mixin_call_method_with_mixins("modify_sql", array($select_sql));
		return $this->db->get_row($select_sql->get_sql());
	}

	protected function modify_sql(select_sql $select_sql)
	{
		return;
	}

}

?>