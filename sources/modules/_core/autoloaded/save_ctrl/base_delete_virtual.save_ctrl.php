<?php

abstract class base_delete_virtual_save_ctrl extends base_delete_save_ctrl
{

	protected function do_delete()
	{
		$this->db->sql("
			UPDATE {$this->db_table}
			SET is_deleted = 1
			WHERE `{$this->pk_column}` = {$this->id}
		");
	}

}

?>
