<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class url_string_save_dtf extends base_save_dtf
{
	const URL_REGEXP = '/^(https?):\/{2}[a-zA-Z0-9;\/?:@&=+$,#_.!~*\'()%,{}|\^\[\]-]+$/';

	/**
	 * @var url_dtf
	 */
	protected $dtf;
	protected $value;
	protected $value_human = null;

	public function check_inner()
	{
		if (!isset($_POST[$this->field_name]))
		{
			return false;
		}
		$this->value = $_POST[$this->field_name];
		$this->value = trim($this->value);
		if ($this->dtf->is_important())
		{
			if ($this->value === "")
			{
				$this->write_error("UNFILLED");
				return false;
			}
		}
		if ($max_length = $this->dtf->get_max_length())
		{
			$len = mb_strlen($this->value);
			if ($len > $max_length)
			{
				$this->write_error("TOO_LONG", $len);
				return false;
			}
		}
		if ($this->value)
		{
			// @todo This is NOT an international way of check...
			$tmp = mb_ereg_replace("[а-яА-ЯёЁ]", "_", $this->value);
			if (!preg_match(self::URL_REGEXP, $tmp))
			{
				$this->write_error("INCORRECT_URL");
				return false;
			}
		}
		if ($human_view_column = $this->dtf->get_human_view_column())
		{
			$this->value_human = text_processor::get_url_human($this->value);
		}
		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		$update_array[$this->field_name] = "'" . $this->db->escape($this->value) . "'";
		if (!is_null($this->value_human))
		{
			$update_array[$this->dtf->get_human_view_column()] = "'" . $this->db->escape($this->value_human) . "'";
		}
	}

}

?>