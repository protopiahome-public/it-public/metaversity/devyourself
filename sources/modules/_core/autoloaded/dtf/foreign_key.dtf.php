<?php

class foreign_key_dtf extends base_dtf
{

	protected $foreign_table = "";
	protected $foreign_table_title_column = "";
	protected $foreign_table_pk_column = "";
	protected $is_important = false;
	protected $where = "";
	protected $restrict_items_read_sql = "";
	protected $restrict_items_check_sql = "";

	public function __construct($field_name, $field_title, $foreign_table, $foreign_table_title_column = "title", $foreign_table_pk_column = "id")
	{
		$this->foreign_table = $foreign_table;
		$this->foreign_table_title_column = $foreign_table_title_column;
		$this->foreign_table_pk_column = $foreign_table_pk_column;
		parent::__construct($field_name, $field_title);
	}
	
	public function set_importance($is_important)
	{
		$this->is_important = $is_important ? true : false;
	}

	public function set_where($where)
	{
		$this->where = $where;
	}
	
	public function set_restrict_items_sql($restrict_items_read_sql, $restrict_items_check_sql)
	{
		$this->restrict_items_read_sql = $restrict_items_read_sql;
		$this->restrict_items_check_sql = $restrict_items_check_sql;
	}

	public function get_foreign_table()
	{
		return $this->foreign_table;
	}

	public function get_foreign_table_pk_column()
	{
		return $this->foreign_table_pk_column;
	}

	public function get_foreign_table_title_column()
	{
		return $this->foreign_table_title_column;
	}
	
	public function is_important()
	{
		return $this->is_important;
	}

	public function get_where()
	{
		return $this->where;
	}
	
	public function get_restrict_items_read_sql()
	{
		return $this->restrict_items_read_sql;
	}
	
	public function get_restrict_items_check_sql()
	{
		return $this->restrict_items_check_sql;
	}

}

?>