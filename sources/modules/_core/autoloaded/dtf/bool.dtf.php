<?php

class bool_dtf extends base_dtf
{

	private $default_value = 0;
	
	public function set_default_value($default_value)
	{
		$this->default_value = $default_value ? true : false;
	}
	
	public function get_default_value()
	{
		return $this->default_value;
	}

}

?>