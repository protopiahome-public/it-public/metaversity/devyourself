<?php

class datetime_xml_dtf extends base_datetime_xml_dtf
{

	/**
	 * @var datetime_dtf
	 */
	protected $dtf;

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);

		$datetime = time();
		$this->export_years_for_select($dtf_node, $datetime);

		// Months for select
		$months_for_select_node = $dtf_node->create_child_node("months_for_select");
		for ($i = 1; $i <= 12; $i++)
		{
			$months_for_select_node->create_child_node("month")
				->set_attr("key", $i <= 9 ? "0" . $i : $i)
				->set_attr("title", $i);
		}

		// Days for select
		$days_for_select_node = $dtf_node->create_child_node("days_for_select");
		for ($i = 1; $i <= 31; $i++)
		{
			$days_for_select_node->create_child_node("day")
				->set_attr("key", $i <= 9 ? "0" . $i : $i)
				->set_attr("title", $i);
		}

		// Hours for select
		$hours_for_select_node = $dtf_node->create_child_node("hours_for_select");
		for ($i = 0; $i <= 23; $i++)
		{
			$val = $i <= 9 ? "0" . $i : $i;
			$hours_for_select_node->create_child_node("hour")
				->set_attr("key", $val)
				->set_attr("title", $val);
		}

		// Minutes for select
		$minutes_for_select_node = $dtf_node->create_child_node("minutes_for_select");
		for ($i = 0; $i < 60; $i += 5)
		{
			$val = $i <= 9 ? "0" . $i : $i;
			$minutes_for_select_node->create_child_node("minute")
				->set_attr("key", $val)
				->set_attr("title", $val);
		}

		// Seconds for select
		//$seconds_for_select_node = $dtf_node->create_child_node("seconds_for_select");
		//for ($i = 0; $i <= 59; $i++)
		//{
		//	$seconds_for_select_node->create_child_node("second", $i <= 9 ? "0" . $i : $i);
		//}
		$dtf_node->set_attr("date_only", $this->dtf->is_date_only());

		if ($must_be_later_than_field_name = $this->dtf->get_must_be_later_than_field_name())
		{
			$dtf_node->set_attr("must_be_later_than_field_name", $must_be_later_than_field_name);
			$dtf_node->set_attr("can_be_equal", $this->dtf->get_can_be_equal());
			$dtf_node->set_attr("hide_by_default", $this->dtf->get_hide_by_default());
		}

		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_edit_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = $parent_node->create_child_node("field");
		$dtf_node->set_attr("name", $this->field_name);

		if (substr($db_row[$this->field_name], 0, 5) === "0000-")
		{
			$datetime = mktime(0, 0, 0, 1, 1, 1972);
			$dtf_node->set_attr("unset", "1");
		}
		else
		{
			$datetime = $this->db->parse_datetime($db_row[$this->field_name]);
		}

		$datetime_for_years_export = $datetime;
		if ($earlier_field_name = $this->dtf->get_must_be_later_than_field_name())
		{
			if (isset($db_row[$earlier_field_name]))
			{
				$datetime_for_years_export = $this->db->parse_datetime($db_row[$earlier_field_name]);
			}
		}

		$this->export_years_for_select($dtf_node, $datetime_for_years_export);
		$dtf_node->set_attr("year", date("Y", $datetime));
		$dtf_node->set_attr("month", date("m", $datetime));
		$dtf_node->set_attr("day", date("d", $datetime));
		$dtf_node->set_attr("hours", date("H", $datetime));
		$minutes = str_pad((int) (date("i", $datetime) / 5) * 5, 2, "0", STR_PAD_LEFT);
		$dtf_node->set_attr("minutes", $minutes);
		return $dtf_node;
	}

	protected function export_years_for_select(xnode $parent_node, $datetime)
	{
		$current_year = date("Y");

		// Years for select
		$min_year = min(date("Y", $datetime) - 1, $current_year - $this->dtf->get_edit_mode_past_years_count_for_select());
		$max_year = max(date("Y", $datetime) + 1, $current_year + $this->dtf->get_edit_mode_future_years_count_for_select());
		$min_year = max($min_year, datetime_dtf::min_allowed_year); // UNIX epoch timestamp restrictions
		$max_year = min($max_year, datetime_dtf::max_allowed_year);
		$years_for_select_node = $parent_node->create_child_node("years_for_select");
		for ($i = $min_year; $i <= $max_year; $i++)
		{
			$years_for_select_node->create_child_node("year")
				->set_attr("key", $i)
				->set_attr("title", $i);
		}
	}

}

?>