<?php

class image_pack_xml_dtf extends base_xml_dtf
{

	/**
	 * @var image_pack_dtf
	 */
	protected $dtf;

	public function modify_sql(select_sql $select_sql, $is_edit_mode = false)
	{
		$field_name = $this->field_name;
		foreach ($this->dtf->get_images() as $image_name => $image_pack_image)
		{
			/* @var $image_pack_image image_pack_image */
			$src_image = $image_pack_image->get_image_to_scale_from() ? $image_pack_image->get_image_to_scale_from() : $image_name;
			$select_sql->add_select_fields("dt.{$field_name}_{$src_image}_width as {$field_name}_{$image_name}_width");
			$select_sql->add_select_fields("dt.{$field_name}_{$src_image}_height as {$field_name}_{$image_name}_height");
			$select_sql->add_select_fields("dt.{$field_name}_version as {$field_name}_version");
			if ($image_pack_image->get_url_prefix() === false)
			{
				$select_sql->add_select_fields("dt.{$field_name}_{$src_image}_url as {$field_name}_{$image_name}_url");
			}
			if ($image_pack_image->get_save_ext())
			{
				$select_sql->add_select_fields("dt.{$field_name}_{$src_image}_ext as {$field_name}_{$image_name}_ext");
			}
		}
	}

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);
		if ($max_file_size = $this->dtf->get_max_file_size())
		{
			$dtf_node->set_attr("max_file_size", $max_file_size);
			$dtf_node->set_attr("max_file_size_formatted", format_file_size($max_file_size));
		}
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_edit_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = $parent_node->create_child_node("field");
		$dtf_node->set_attr("name", $this->field_name);
//		$dtf_node->set_attr("rand", uniqid());
		$this->fill_xml_helper($dtf_node, $db_row, true);
	}

	/**
	 * @return xnode
	 */
	public function fill_xml(xnode $parent_node, $db_row)
	{
		$this->fill_xml_helper($parent_node, $db_row, false);
	}

	public function fill_xml_helper(xnode $parent_node, $db_row, $is_edit_mode)
	{
		$field_name = $this->field_name;
		foreach ($this->dtf->get_images() as $image_name => $image_pack_image)
		{
			/* @var $image_pack_image image_pack_image */
			/* if (isset($db_row["id"]) and isset($db_row["{$field_name}_{$image_name}_width"]) and isset($db_row["{$field_name}_{$image_name}_height"])) */
			$width = $db_row["{$field_name}_{$image_name}_width"];
			$height = $db_row["{$field_name}_{$image_name}_height"];
			$is_uploaded = $width && $height;
			if ($image_pack_image->get_max_width() > 0 && $image_pack_image->get_max_height() > 0)
			{
				if ($image_pack_image->get_max_width() < $width or $image_pack_image->get_max_height() < $height)
				{
					//For directly imported to db
					$scale_factor = false;
					if ($image_pack_image->get_max_width() < $width)
					{
						$scale_factor = $image_pack_image->get_max_width() / $width;
					}
					if ($image_pack_image->get_max_height() < $height)
					{
						$scale_factor_2 = $image_pack_image->get_max_height() / $height;
						if ($scale_factor === false or $scale_factor_2 < $scale_factor)
						{
							$scale_factor = $scale_factor_2;
						}
					}
					if ($scale_factor !== false)
					{
						$width = round($width * $scale_factor);
						$height = round($height * $scale_factor);
					}
				}
			}
			$ext = $image_pack_image->get_save_ext() ? $db_row["{$field_name}_{$image_name}_ext"] : $this->dtf->get_image_type();
			$url = "";
			if ($is_uploaded && $image_pack_image->get_url_prefix() === false)
			{
				$url = $db_row["{$field_name}_{$image_name}_url"];
			}
			elseif ($is_uploaded)
			{
				$url = $this->dtf->get_add_prefix_to_url() . $image_pack_image->get_url_prefix() . $db_row["id"] . "." . $ext;
			}
			elseif (($stub_url = $image_pack_image->get_stub_url()))
			{
				$url = $this->dtf->get_add_prefix_to_url() . $stub_url;
				$width = $image_pack_image->get_max_width();
				$height = $image_pack_image->get_max_height();
			}
			$url .= "?v{$db_row["{$field_name}_version"]}";

			if ($is_edit_mode)
			{
				$image_node = $parent_node->create_child_node("image");
				$image_node->set_attr("name", $image_pack_image->get_name());
				$image_node->set_attr("title", $image_pack_image->get_title());
			}
			else
			{
				$image_node = $parent_node->create_child_node($this->field_name . "_" . $image_pack_image->get_name());
			}
			$image_node->set_attr("is_uploaded", $is_uploaded);
			$image_node->set_attr("width", $width);
			$image_node->set_attr("height", $height);
			$image_node->set_attr("url", $url);
		}
		return $parent_node;
	}

}

?>