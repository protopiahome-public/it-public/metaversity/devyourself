<?php

require_once PATH_INTCMF . "/ximage.php";

class image_pack_save_dtf extends base_save_dtf
{

	/**
	 * @var image_pack_dtf
	 */
	protected $dtf;
	protected $images = array();
	protected $delete = false;

	public function check_inner()
	{
		foreach ($this->dtf->get_images() as $image_name => $image_pack_image)
		{
			/* @var $image_pack_image image_pack_image */
			if ($image_pack_image->get_image_to_scale_from())
			{
				continue;
			}
			if (!$image_pack_image->get_image_directory())
			{
				trigger_error("Saving of these images ({$this->field_name}) is unallowed: image_directory is not set");
				return false;
			}
		}
		if (!isset($_FILES[$this->field_name]))
		{
			return false;
		}
		$file_data = $_FILES[$this->field_name];
		if ($file_data["error"] != UPLOAD_ERR_NO_FILE)
		{
			if ($file_data["error"] != UPLOAD_ERR_OK)
			{
				switch ($file_data["error"])
				{
					case UPLOAD_ERR_INI_SIZE:
						/* @todo Пользователю надо писать, что это системная ошибка (настройки сервера),
						  но перед этим надо поднять проверку $max_file_size = $this->dtf->get_max_file_size(),
						  чтобы она шла выше этой - иначе систмная ошибка будет выдаваться, даже если можно просто
						  по-человечески объяснить, что размер файла превышен */
						$this->write_error("TOO_MUCH_SIZE_INI", format_file_size($file_data["size"]));
						break;
					case UPLOAD_ERR_FORM_SIZE:
						$this->write_error("UPLOAD_ERR_FORM_SIZE", format_file_size($file_data["size"]));
						break;
					case UPLOAD_ERR_PARTIAL:
						$this->write_error("UPLOAD_ERR_PARTIAL");
						break;
					case UPLOAD_ERR_NO_TMP_DIR:
						$this->write_error("UPLOAD_ERR_NO_TMP_DIR");
						break;
					case UPLOAD_ERR_CANT_WRITE:
						$this->write_error("UPLOAD_ERR_CANT_WRITE");
						break;
					case UPLOAD_ERR_EXTENSION:
						$this->write_error("UPLOAD_ERR_EXTENSION");
						break;
					default:
						$this->write_error("UNKNOWN_UPLOAD_ERROR");
					/* @todo logging for some of these errors */
				}
				return false;
			}
			if ($max_file_size = $this->dtf->get_max_file_size())
			{
				if ($file_data["size"] > $max_file_size)
				{
					$this->write_error("TOO_MUCH_SIZE", format_file_size($file_data["size"]));
					return false;
				}
			}
			$ximage_src = new ximage($file_data["tmp_name"]);
			$ximage_for_resize = $ximage_src;
			$ximage_for_resize_reassigned = false;
			if (!$ximage_src->image_type_test())
			{
				$this->write_error("UNKNOWN_TYPE");
				return false;
			}
			$tmp_name = uniqid($this->field_name);
			foreach ($this->dtf->get_images() as $image_name => $image_pack_image)
			{
				/* @var $image_pack_image image_pack_image */
				if ($image_pack_image->get_image_to_scale_from())
				{
					continue;
				}
				$this->images[$image_pack_image->get_name()] = array();
				$image_desc = &$this->images[$image_pack_image->get_name()];
				$image_desc["ext"] = $image_pack_image->get_save_ext();
				$image_desc["file_ext"] = $image_pack_image->get_save_ext();
				if (!$image_desc["file_ext"])
				{
					$image_desc["file_ext"] = $this->dtf->get_image_type();
				}
				$image_desc["tmp_path"] = $this->dtf->get_tmp_dir() . "/" . $this->field_name . "_" . $image_pack_image->get_name() . "_" . $tmp_name . "." . $image_desc["file_ext"];
				$image_desc["target_dir"] = $image_pack_image->get_image_directory() . "/";

				if ($image_pack_image->get_max_width() > 0 || $image_pack_image->get_max_height() > 0)
				{
					$ximage_new = $ximage_for_resize->resize($image_pack_image->get_max_width(), $image_pack_image->get_max_height(), $image_desc["tmp_path"]);
				}
				else
				{
					$ximage_new = $ximage_for_resize->optimize($image_desc["tmp_path"]);
				}

				foreach ($image_pack_image->get_processors() as $processor)
				{
					$ximage_new = $processor->process($ximage_new, $image_desc["tmp_path"]);
				}

				$image_desc["width"] = $ximage_new->get_width();
				$image_desc["height"] = $ximage_new->get_height();
				if ($ximage_new->image_type_test() and $image_desc["width"] and $image_desc["height"])
				{
					if ($this->dtf->get_use_first_image_to_resize_others() and !$ximage_for_resize_reassigned)
					{
						$ximage_for_resize = $ximage_new;
						$ximage_for_resize_reassigned = true;
					}
				}
				else
				{
					$this->write_error("CANT_RESIZE", $image_pack_image->get_name());
					return false;
				}
			}
		}
		else
		{
			if (isset($_POST[$this->field_name . "_delete"]))
			{
				if ($this->document_id)
				{
					$this->delete = true;
				}
			}
		}
		return true;
	}

	public function commit($last_id)
	{
		if ($this->delete)
		{
			if ($this->document_id)
			{
				foreach ($this->dtf->get_images() as $image_name => $image_pack_image)
				{
					/* @var $image_pack_image image_pack_image */
					if ($image_pack_image->get_image_to_scale_from())
					{
						continue;
					}
					$file_ext = $image_pack_image->get_save_ext();
					if (!$file_ext)
					{
						$file_ext = $this->dtf->get_image_type();
					}
					$target_path = $image_pack_image->get_image_directory() . "/" . $this->document_id . "." . $file_ext;
					if (file_exists($target_path))
					{
						unlink($target_path);
					}
				}
			}
			return true;
		}
		else
		{
			$ok = true;
			foreach ($this->images as $image_name => $image_desc)
			{
				if (file_exists($image_desc["tmp_path"]))
				{
					$target_path = $image_desc["target_dir"] . $last_id . "." . $image_desc["file_ext"];
					if (file_exists($target_path))
					{
						unlink($target_path);
					}
					$ret = rename($image_desc["tmp_path"], $target_path);
					$ok = $ok && $ret;
				}
			}
			return $ok;
		}
	}

	public function rollback()
	{
		foreach ($this->images as $image_name => $image_desc)
		{
			if (file_exists($image_desc["tmp_path"]))
			{
				unlink($image_desc["tmp_path"]);
			}
		}
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		$field_name = $this->dtf->get_field_name();
		if ($this->delete)
		{
			foreach ($this->dtf->get_images() as $image_name => $image_pack_image)
			{
				/* @var $image_pack_image image_pack_image */
				if ($image_pack_image->get_image_to_scale_from())
				{
					continue;
				}
				$update_array["{$field_name}_{$image_name}_width"] = 0;
				$update_array["{$field_name}_{$image_name}_height"] = 0;
				if ($image_pack_image->get_save_ext())
				{
					$update_array["{$field_name}_{$image_name}_ext"] = "''";
				}
			}
		}
		else
		{
			foreach ($this->images as $image_name => $image_desc)
			{
				$update_array["{$field_name}_{$image_name}_width"] = $image_desc["width"];
				$update_array["{$field_name}_{$image_name}_height"] = $image_desc["height"];
				if ($image_desc["ext"])
				{
					$update_array["{$field_name}_{$image_name}_ext"] = "'" . $image_desc["ext"] . "'";
				}
			}
			if (sizeof($this->images))
			{
				$update_array["{$field_name}_version"] = "{$field_name}_version + 1";
			}
		}
	}

}

?>