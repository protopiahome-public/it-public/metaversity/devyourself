<?php

class datetime_save_dtf extends base_save_dtf
{

	/**
	 * @var datetime_dtf
	 */
	protected $dtf;
	protected $hide;
	protected $year;
	protected $month;
	protected $day;
	protected $hours = 0;
	protected $minutes = 0;
	protected static $times = array();

	public function check_inner()
	{
		$this->hide = $this->dtf->get_hide_by_default() && isset($_POST[$this->field_name . "_hide"]) && $_POST[$this->field_name . "_hide"] == "1";
		if (!$this->hide)
		{
			/* Year, month, day */
			if (!isset($_POST[$this->field_name . "_year"]) or !isset($_POST[$this->field_name . "_month"]) or !isset($_POST[$this->field_name . "_day"]))
			{
				return false;
			}
			$this->year = (int) $_POST[$this->field_name . "_year"];
			$this->month = (int) $_POST[$this->field_name . "_month"];
			$this->day = (int) $_POST[$this->field_name . "_day"];
			if (!is_good_id($this->year) or !is_good_id($this->month) or !is_good_id($this->day))
			{
				$this->write_error("INCORRECT_VALUE");
				return false;
			}
			if ($this->year < datetime_dtf::min_allowed_year)
			{
				$this->write_error("UNALLOWED_YEAR");
				return false;
			}
			if ($this->year > datetime_dtf::max_allowed_year)
			{
				$this->write_error("UNALLOWED_YEAR");
				return false;
			}
			if ($this->month < 1 or $this->month > 12)
			{
				$this->write_error("UNALLOWED_MONTH");
				return false;
			}
			if ($this->day < 1 or $this->day > 31)
			{
				$this->write_error("UNALLOWED_DAY");
				return false;
			}
			$max_allowed_day = (int) date("t", mktime(1, 0, 0, $this->month, 1, $this->year));
			if ($this->day > $max_allowed_day)
			{
				$this->day = $max_allowed_day;
			}

			/* Hours, minutes */
			if ($this->dtf->is_date_only())
			{
				$this->hours = 12;
				$this->minutes = 0;
			}
			else
			{
				if (!isset($_POST[$this->field_name . "_hours"]) or !isset($_POST[$this->field_name . "_minutes"]))
				{
					return false;
				}
				$this->hours = (int) $_POST[$this->field_name . "_hours"];
				$this->minutes = (int) $_POST[$this->field_name . "_minutes"];
				if (!is_good_num($this->hours) or !is_good_num($this->minutes))
				{
					$this->write_error("INCORRECT_VALUE");
					return false;
				}
				if ($this->hours > 23)
				{
					$this->write_error("UNALLOWED_HOURS");
					return false;
				}
				if ($this->minutes > 59)
				{
					$this->write_error("UNALLOWED_MINUTES");
					return false;
				}
			}

			/*  */
			$result_time = mktime($this->hours, $this->minutes, 0, $this->month, $this->day, $this->year);
			self::$times[$this->field_name] = $result_time;
			if ($check_field_name = $this->dtf->get_must_be_later_than_field_name())
			{
				$can_be_equal = $this->dtf->get_can_be_equal();
				if ($result_time < self::$times[$check_field_name] or !$can_be_equal and $result_time == self::$times[$check_field_name])
				{
					$this->write_error("FAILED_DEPENDENCY", $check_field_name);
					return false;
				}
			}
		}

		// OK
		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		if ($this->hide)
		{
			$update_array[$this->field_name] = "'0000-00-00 00:00:00'";
		}
		else
		{
			$year = $this->year;
			$month = str_pad($this->month, 2, "0", STR_PAD_LEFT);
			$day = str_pad($this->day, 2, "0", STR_PAD_LEFT);
			$hours = str_pad($this->hours, 2, "0", STR_PAD_LEFT);
			$minutes = str_pad($this->minutes, 2, "0", STR_PAD_LEFT);
			$update_array[$this->field_name] = "'{$year}-{$month}-{$day} {$hours}:{$minutes}:00'";
		}
	}

}

?>