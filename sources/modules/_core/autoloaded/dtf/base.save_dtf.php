<?php

abstract class base_save_dtf
{

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var error
	 */
	protected $error;

	/**
	 * @var request
	 */
	protected $request;

	/**
	 * @var user
	 */
	protected $user;

	/**
	 * @var base_dtf
	 */
	protected $dtf;

	/**
	 * @var pass_info
	 */
	protected $pass_info;
	protected $db_table;
	protected $document_id = 0;
	protected $old_db_row = null;
	protected $field_name;
	protected $check_is_ok = false;
	protected $errors = array();

	public function __construct(base_dtf $dtf, pass_info $pass_info, $db_table, $document_id = 0, $old_db_row = null)
	{
		global $db;
		$this->db = $db;

		global $error;
		$this->error = $error;

		global $request;
		$this->request = $request;

		global $user;
		$this->user = $user;

		$this->dtf = $dtf;
		$this->pass_info = $pass_info;
		$this->db_table = $db_table;
		$this->document_id = $document_id;
		$this->old_db_row = $old_db_row;

		$this->field_name = $this->dtf->get_field_name();
	}

	abstract protected function check_inner();

	abstract protected function get_fields_to_write_inner(&$update_array);

	public function check()
	{
		if ($this->dtf->is_read_only())
		{
			trigger_error("Writing of read only field: '{$this->field_name}'");
			$this->check_is_ok = false;
			return false;
		}

		$this->check_is_ok = $this->check_inner();
		return $this->check_is_ok;
	}

	public function update_db_row(&$db_row)
	{
		if (property_exists($this, "value"))
		{
			$db_row[$this->field_name] = $this->value;
		}
	}

	public function commit($last_id)
	{
		return true;
	}

	public function rollback()
	{
		return true;
	}

	public function get_fields_to_write(&$update_array)
	{
		if ($this->check_is_ok)
		{
			$this->get_fields_to_write_inner($update_array);
		}
	}

	protected function write_error($error_name, $error_descr = "")
	{
		$this->pass_info->write_field_error($this->field_name, $error_name, $error_descr);
		$this->errors[] = array($error_name => $error_descr);
	}

	/**
	 * @return base_dtf
	 */
	public function get_dtf()
	{
		return $this->dtf;
	}

	public function get_errors()
	{
		return $this->errors;
	}

}

?>