<?php

class foreign_key_save_dtf extends base_save_dtf
{

	/**
	 * @var foreign_key_dtf
	 */
	protected $dtf;

	protected $value;

	public function check_inner()
	{
		$foreign_table_name = $this->dtf->get_foreign_table();
		$foreign_table_alias = $this->dtf->get_field_name() . "_foreign_table";
		$foreign_table_pk_column = $this->dtf->get_foreign_table_pk_column();
		$foreign_table_title_column = $this->dtf->get_foreign_table_title_column();

		if (!isset($_POST[$this->field_name]))
		{
			return false;
		}
		$this->value = $_POST[$this->field_name];
		if ($this->value === "")
		{
			if ($this->dtf->is_important())
			{
				$this->write_error("UNFILLED");
				return false;
			}
			else
			{
				$this->value = null;
				return true;
			}
		}
		else
		{
			$value_escaped = $this->db->escape($this->value);
			if ($restrict_items_check_sql = $this->dtf->get_restrict_items_check_sql())
			{
				$allowed_value = $this->db->row_exists(str_replace("%s", "'" . $value_escaped . "'", $restrict_items_check_sql));
			}
			else 
			{
				$where = $this->dtf->get_where();
				if ($where)
				{
					$where = "AND ({$where})";
				}
				$allowed_value = $this->db->row_exists("SELECT * FROM {$foreign_table_name} WHERE {$foreign_table_pk_column} = '{$value_escaped}' {$where}");
			}
			if (!$allowed_value)
			{
				$this->write_error("UNEXISTED_VALUE");
				return false;
			}
			return true;
		}
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		if (is_null($this->value))
		{
			$update_array[$this->field_name] = "NULL";
		}
		else
		{
			$update_array[$this->field_name] = "'" . $this->db->escape($this->value) . "'";
		}
	}

}

?>