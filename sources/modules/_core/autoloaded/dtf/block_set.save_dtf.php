<?php

class block_set_save_dtf extends base_save_dtf
{

	/**
	 * @var block_set_dtf
	 */
	protected $dtf;

	/**
	 * @var save_loader
	 */
	protected $save_loader;

	/**
	 *
	 * @var block_set_save_ctrl
	 */
	protected $block_set_save_ctrl;

	public function check_inner()
	{
		global $save_loader;
		$this->save_loader = $save_loader;

		$this->block_set_save_ctrl = new block_set_save_ctrl(POST("block_set_data"));
		$this->block_set_save_ctrl->set_block_set_id(intval($this->old_db_row ? $this->old_db_row[$this->field_name] : 0));
		$this->block_set_save_ctrl->set_pass_info($this->pass_info);
		$this->save_loader->add_ctrl($this->block_set_save_ctrl);

		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		$this->block_set_save_ctrl->set_object_data($this->db_table, $this->document_id);
//		$update_array[$this->field_name] = $this->block_set_save_ctrl->get_id();
	}

}

?>