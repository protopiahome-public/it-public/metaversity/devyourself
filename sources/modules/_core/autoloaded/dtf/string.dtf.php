<?php

class string_dtf extends base_dtf
{

	private $trim_before_save = true;
	private $is_important = false;
	private $unique = false;
	private $unique_error_phrase = "";
	private $min_length = 0;
	private $max_length = 255;
	private $regexp = "";
	private $regexp_error_phrase = "";

	public function set_trim_before_save($trim_before_save)
	{
		$this->trim_before_save = $trim_before_save ? true : false;
	}

	public function set_importance($is_important)
	{
		$this->is_important = $is_important ? true : false;
	}

	public function set_unique($unique, $unique_error_phrase)
	{
		$this->unique = $unique;
		$this->unique_error_phrase = $unique_error_phrase;
	}

	public function set_min_length($min_length)
	{
		if (is_good_num($min_length))
		{
			$this->min_length = $min_length;
		}
	}

	public function set_max_length($max_length)
	{
		if (is_good_num($max_length))
		{
			$this->max_length = $max_length;
		}
	}

	public function set_regexp($regexp, $regexp_error_phrase)
	{
		$this->regexp = $regexp;
		$this->regexp_error_phrase = $regexp_error_phrase;
	}

	public function get_trim_before_save()
	{
		return $this->trim_before_save;
	}

	public function is_important()
	{
		return $this->is_important;
	}

	public function get_unique()
	{
		return $this->unique;
	}

	public function get_unique_error_phrase()
	{
		return $this->unique_error_phrase;
	}

	public function get_min_length()
	{
		return $this->min_length;
	}

	public function get_max_length()
	{
		return $this->max_length;
	}

	public function get_regexp()
	{
		return $this->regexp;
	}

	public function get_regexp_error_phrase()
	{
		return $this->regexp_error_phrase;
	}

}

?>