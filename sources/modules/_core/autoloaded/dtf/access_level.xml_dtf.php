<?php

class access_level_xml_dtf extends base_xml_dtf
{

	/**
	 * @var access_level_dtf
	 */
	protected $dtf;

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);
		$default_level = $this->dtf->get_default_level();
		foreach ($this->dtf->get_levels() as $level => $level_data)
		{
			if ($level < $this->dtf->get_min_level())
			{
				continue;
			}
			$item_node = $dtf_node->create_child_node("item")
				->set_attr("level", $level)
				->set_attr("status", $level_data["status"])
				->set_attr("title", $level_data["title"]);
			if ($level == $default_level)
			{
				$item_node->set_attr("is_default", "1");
			}
		}
		if ($required_level_field_name = $this->dtf->get_required_level_field_name())
		{
			$dtf_node->set_attr("required_level_field_name", $required_level_field_name);
		}
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_edit_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = parent::fill_edit_xml($parent_node, $db_row);
		if ($this->dtf->get_default_checkbox_column_name() and $db_row[$this->dtf->get_default_checkbox_column_name()] and $this->dtf->get_default_level())
		{
			$level = $this->dtf->get_default_level();
			$status = $this->dtf->get_status_from_level($level);
		}
		else
		{
			$status = $db_row[$this->field_name];
			$level = $this->dtf->get_level_from_status($status);
		}
		$dtf_node->set_attr("status", $status);
		$dtf_node->set_attr("level", $level);
		$dtf_node->set_text($this->dtf->get_level_title($level));
		return $dtf_node;
	}

}

?>