<?php

define("INTCMF_INT_DTF_TYPE_BYTE", 10);
define("INTCMF_INT_DTF_TYPE_SMALLINT", 11);
define("INTCMF_INT_DTF_TYPE_WORD", 20);
define("INTCMF_INT_DTF_TYPE_INT16", 21);
define("INTCMF_INT_DTF_TYPE_DWORD", 40);
define("INTCMF_INT_DTF_TYPE_INT32", 41);

class int_dtf extends base_dtf
{

	private $is_important = false;
	private $default_value = null;
	private $min_value; // Must be set in constructor
	private $max_value; // Must be set in constructor

	public function __construct($field_name, $field_title, $int_type = INTCMF_INT_DTF_TYPE_DWORD)
	{
		parent::__construct($field_name, $field_title);
		$this->set_type($int_type);
	}

	public function set_default_value($default_value)
	{
		if (is_good_num_with_negative($default_value))
		{
			$this->default_value = $default_value;
		}
	}

	public function set_importance($is_important)
	{
		$this->is_important = $is_important ? true : false;
		if ($this->is_important && is_null($this->default_value) && $this->max_value >= 0 && $this->min_value <= 0)
		{
			$this->default_value = 0;
		}
	}

	public function set_min_value($min_value)
	{
		if (is_good_num_with_negative($min_value))
		{
			$this->min_value = $min_value;
		}
	}

	public function set_max_value($max_value)
	{
		if (is_good_num_with_negative($max_value))
		{
			$this->max_value = $max_value;
		}
	}

	public function set_type($int_type)
	{
		switch ($int_type)
		{
			case INTCMF_INT_DTF_TYPE_BYTE:
				$this->min_value = 0;
				$this->max_value = 255;
				break;
			case INTCMF_INT_DTF_TYPE_SMALLINT:
				$this->min_value = -128;
				$this->max_value = 127;
				break;
			case INTCMF_INT_DTF_TYPE_WORD:
				$this->min_value = 0;
				$this->max_value = 65535;
				break;
			case INTCMF_INT_DTF_TYPE_INT16:
				$this->min_value = -32768;
				$this->max_value = 32767;
				break;
			case INTCMF_INT_DTF_TYPE_DWORD:
				$this->min_value = 0;
				//$this->max_value = 4294967295;
				$this->max_value = 2147483646;
				break;
			case INTCMF_INT_DTF_TYPE_INT32:
				//$this->min_value = -2147483648;
				$this->min_value = -2147483647;
				//$this->max_value = 2147483647;
				$this->max_value = 2147483646;
				break;
		}
	}

	public function is_important()
	{
		return $this->is_important;
	}

	public function get_default_value()
	{
		return $this->default_value;
	}

	public function get_min_value()
	{
		return $this->min_value;
	}

	public function get_max_value()
	{
		return $this->max_value;
	}

}

?>