<?php

class image_pack_image
{

	protected $allowed_exts = array("jpeg", "jpg", "png", "gif");
	protected $name;
	protected $title;
	protected $max_width;
	protected $max_height;
	protected $url_prefix;
	protected $stub_url;
	protected $image_directory;
	protected $save_ext;
	protected $image_to_scale_from;
	protected $processors = array();

	public function __construct($name, $title, $max_width, $max_height, $url_prefix, $stub_url = null, $image_directory = null, $save_ext = null, $image_to_scale_from = null)
	{
		$this->name = $name;
		$this->title = $title;
		$this->max_width = $max_width;
		$this->max_height = $max_height;
		$this->url_prefix = $url_prefix;
		$this->stub_url = $stub_url;
		$this->image_directory = $image_directory;
		$this->save_ext = $save_ext;
		$this->image_to_scale_from = $image_to_scale_from;
		if (!$save_ext or !in_array($save_ext, $this->allowed_exts))
		{
			$save_ext = "";
		}
	}

	public function get_name()
	{
		return $this->name;
	}

	public function get_title()
	{
		return $this->title;
	}

	public function get_max_width()
	{
		return $this->max_width;
	}

	public function get_max_height()
	{
		return $this->max_height;
	}

	public function get_url_prefix()
	{
		return $this->url_prefix;
	}

	public function get_stub_url()
	{
		return $this->stub_url;
	}

	public function get_image_directory()
	{
		return $this->image_directory;
	}

	public function get_save_ext()
	{
		return $this->save_ext;
	}

	public function get_image_to_scale_from()
	{
		return $this->image_to_scale_from;
	}

	public function add_processor(base_image_pack_processor $processor)
	{
		$this->processors[] = $processor;
	}

	public function get_processors()
	{
		return $this->processors;
	}

}

?>