<?php

class image_pack_crop_processor extends base_image_pack_processor
{

	protected $width;
	protected $height;

	public function __construct($width, $height)
	{
		$this->width = $width;
		$this->height = $height;
	}

	public function process(ximage $ximage, $tmp_path)
	{
		return $ximage->crop($this->width, $this->height, $tmp_path);
	}

}

?>
