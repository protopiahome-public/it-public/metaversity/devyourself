<?php

abstract class base_image_pack_processor
{

	abstract public function process(ximage $ximage, $tmp_path);
}

?>
