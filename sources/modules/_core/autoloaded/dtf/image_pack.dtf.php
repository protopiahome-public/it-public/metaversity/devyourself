<?php

require_once dirname(__FILE__) . "/image_pack_image.php";
require_once dirname(__FILE__) . "/base_image_pack_processor.php";
require_once dirname(__FILE__) . "/image_pack_crop_processor.php";

class image_pack_dtf extends base_dtf
{

	protected $tmp_dir; // Without trailing slash
	protected $images = array(); // Array of objects with image_pack_image class
	protected $max_file_size = 2097152; // 2 MB default - max upload file size
	protected $use_first_image_to_resize_others = false;
	protected $add_prefix_to_url = "";
	protected $image_type = "jpeg";

	public function __construct($field_name, $field_title, $tmp_dir)
	{
		$this->tmp_dir = $tmp_dir;
		parent::__construct($field_name, $field_title);
	}

	public function add_image($name, $title, $max_width, $max_height, $url_prefix, $stub_url = null, $image_directory = null, $save_ext = null, $image_to_scale_from = null)
	{
		$this->images[$name] = new image_pack_image($name, $title, $max_width, $max_height, $url_prefix, $stub_url, $image_directory, $save_ext, $image_to_scale_from);
	}

	public function set_max_file_size($max_file_size)
	{
		$this->max_file_size = $max_file_size;
	}

	public function set_use_first_image_to_resize_others($use_first_image_to_resize_others)
	{
		$this->use_first_image_to_resize_others = $use_first_image_to_resize_others ? true : false;
	}

	public function set_add_prefix_to_url($add_prefix_to_url)
	{
		$this->add_prefix_to_url = $add_prefix_to_url;
	}

	public function set_image_type_jpeg()
	{
		$this->image_type = "jpeg";
	}

	public function set_image_type_png()
	{
		$this->image_type = "png";
	}

	public function get_tmp_dir()
	{
		return $this->tmp_dir;
	}

	public function get_images()
	{
		return $this->images;
	}

	/**
	 * @return image_pack_image
	 */
	public function get_image_by_name($name) /* @todo add _by_... everywhere */
	{
		if (isset($this->images[$name]))
		{
			return $this->images[$name];
		}
		return null;
	}

	public function get_max_file_size()
	{
		return $this->max_file_size;
	}

	public function get_use_first_image_to_resize_others()
	{
		return $this->use_first_image_to_resize_others;
	}

	public function get_add_prefix_to_url()
	{
		return $this->add_prefix_to_url;
	}

	public function get_image_type()
	{
		return $this->image_type;
	}

}

?>