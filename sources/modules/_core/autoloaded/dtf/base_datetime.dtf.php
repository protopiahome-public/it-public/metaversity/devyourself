<?php

abstract class base_datetime_dtf extends base_dtf
{

	// Ex.: "d.m.Y H:i"
	protected $format = null;

	public function set_format($format)
	{
		$this->format = $format;
	}

	public function get_format()
	{
		return $this->format;
	}

}

?>