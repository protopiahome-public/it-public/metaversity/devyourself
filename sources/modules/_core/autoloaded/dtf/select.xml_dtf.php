<?php

class select_xml_dtf extends base_xml_dtf
{

	/**
	 * @var select_dtf
	 */
	protected $dtf;

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);
		$default_key = $this->dtf->get_default_key();
		foreach ($this->dtf->get_cases() as $idx => $val)
		{
			$item_node = $dtf_node->create_child_node("item")->set_attr("key", $idx)->set_attr("title", $val);
			if ($idx == $default_key)
			{
				$item_node->set_attr("is_default", "1");
			}
		}
		if ($this->dtf->is_important())
		{
			$dtf_node->set_attr("is_important", "1");
		}
		if ($this->dtf->get_drop_down_view())
		{
			$dtf_node->set_attr("drop_down_view", "1");
		}
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_edit_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = parent::fill_edit_xml($parent_node, $db_row);
		/* @var $dtf_node xnode */
		$cases = $this->dtf->get_cases();
		if (isset($cases[$db_row[$this->field_name]]))
		{
			$dtf_node->set_text($cases[$db_row[$this->field_name]]);
			$dtf_node->set_attr("key", $db_row[$this->field_name]);
		}
		return $dtf_node;
	}

}

?>