<?php

class foreign_key_xml_dtf extends base_xml_dtf
{

	/**
	 * @var foreign_key_dtf
	 */
	protected $dtf;

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$foreign_table_name = $this->dtf->get_foreign_table();
		$foreign_table_alias = $this->dtf->get_field_name() . "_foreign_table";
		$foreign_table_pk_column = $this->dtf->get_foreign_table_pk_column();
		$foreign_table_title_column = $this->dtf->get_foreign_table_title_column();

		$dtf_node = parent::fill_doctype_xml($parent_node);
		/* @var $dtf_node xnode */
		if ($this->dtf->is_important())
		{
			$dtf_node->set_attr("is_important", "1");
		}
		if ($restrict_items_read_sql = $this->dtf->get_restrict_items_read_sql())
		{
			$db_result = $this->db->sql($restrict_items_read_sql);
		}
		else 
		{
			$where = $this->dtf->get_where();
			if ($where)
			{
				$where = "WHERE " . $where;
			}
			$db_result = $this->db->sql("
				SELECT {$foreign_table_pk_column} as `id`, {$foreign_table_title_column} AS `title`
				FROM {$foreign_table_name}
				{$where}
				ORDER BY `title`
			");
		}
		while ($db_row = $this->db->fetch_array($db_result))
		{
			$dtf_node->create_child_node("item")->set_attr("value", $db_row["id"])->set_attr("title", $db_row["title"]);
		}
		return $dtf_node;
	}

	public function set_joins(&$joins)
	{
		$foreign_table_name = $this->dtf->get_foreign_table();
		$foreign_table_alias = $this->dtf->get_field_name() . "_foreign_table";
		$foreign_table_pk_column = $this->dtf->get_foreign_table_pk_column();
		$field_name = $this->dtf->get_field_name();

		$joins[] =
		"LEFT JOIN {$foreign_table_name} {$foreign_table_alias}
		ON {$foreign_table_alias}.{$foreign_table_pk_column} = dt.{$field_name}";
	}

	public function set_fields_for_selection(&$fields_array)
	{
		$key_column_name = $this->field_name . "_key";
		$title_column_name = $this->field_name . "_title";
		$foreign_table_alias = $this->dtf->get_field_name() . "_foreign_table";
		$foreign_table_pk_column = $this->dtf->get_foreign_table_pk_column();
		$foreign_table_title_column = $this->dtf->get_foreign_table_title_column();

		$fields_array[$key_column_name] = "{$foreign_table_alias}.{$foreign_table_pk_column}";
		$fields_array[$title_column_name] = "{$foreign_table_alias}.{$foreign_table_title_column}";
	}

	/**
	 * @return xnode
	 */
	public function fill_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = parent::fill_xml($parent_node, $db_row);
		/* @var $dtf_node xnode */
		$key_column_name = $this->field_name . "_key";
		$title_column_name = $this->field_name . "_title";
		if (isset($db_row[$key_column_name]) and isset($db_row[$title_column_name]))
		{
			$dtf_node->set_text($db_row[$title_column_name]);
			$dtf_node->set_attr("value_key", $db_row[$key_column_name]);
		}
		return $dtf_node;
	}

}

?>