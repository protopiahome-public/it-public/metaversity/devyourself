<?php

class string_xml_dtf extends base_xml_dtf
{

	/**
	 * @var string_dtf
	 */
	protected $dtf;

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);
		if ($min_length = $this->dtf->get_min_length())
		{
			$dtf_node->set_attr("min_length", $min_length);
		}
		if ($max_length = $this->dtf->get_max_length())
		{
			$dtf_node->set_attr("max_length", $max_length);
		}
		if ($this->dtf->is_important())
		{
			$dtf_node->set_attr("is_important", "1");
		}
		if ($this->dtf->get_unique())
		{
			$dtf_node->set_attr("unique", "1");
			$dtf_node->set_attr("unique_error_phrase", $this->dtf->get_unique_error_phrase());
		}
		if ($regexp = $this->dtf->get_regexp())
		{
			$dtf_node->set_attr("regexp", $regexp);
			$dtf_node->set_attr("regexp_error_phrase", $this->dtf->get_regexp_error_phrase());
		}
		return $dtf_node;
	}

}

?>