<?php

class text_br_xml_dtf extends base_xml_dtf
{

	/**
	 * @var text_br_dtf
	 */
	protected $dtf;

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);
		if ($max_length = $this->dtf->get_max_length())
		{
			$dtf_node->set_attr("max_length", $max_length);
		}
		if ($this->dtf->is_important())
		{
			$dtf_node->set_attr("is_important", "1");
		}
		$dtf_node->set_attr("editor_height", $this->dtf->get_editor_height());
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_edit_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = $parent_node->create_child_node("field");
		$dtf_node->set_attr("name", $this->field_name);
		$dtf_node->set_text(unhtmlspecialchars(preg_replace("/<br\s*\/>/", "", $db_row[$this->field_name])));
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = $parent_node->create_child_node($this->field_name);
		$content_xdom = xdom::create_from_string("<root><div class=\"text_br\">{$db_row[$this->field_name]}</div></root>");
		$dtf_node->import_xdom($content_xdom, true);
		return $dtf_node;
	}

}

?>