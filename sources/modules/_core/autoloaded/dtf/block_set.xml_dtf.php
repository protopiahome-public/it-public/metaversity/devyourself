<?php

class block_set_xml_dtf extends base_xml_dtf
{

	/**
	 * @var block_set_dtf
	 */
	protected $dtf;

	public function load_additional_data($db_row = null, $is_edit_mode = false)
	{
		$id = isset($db_row[$this->field_name]) ? $db_row[$this->field_name] : null;
		$this->draw_ctrl($id);
		parent::load_additional_data($db_row, $is_edit_mode);
	}

	protected function draw_ctrl($id = null)
	{
		$this->xml_loader = $xml_loader;
		$this->xml_loader->add_xml(new block_set_xml_ctrl($id));
	}

}

?>