<?php

class access_level_dtf extends base_dtf
{

	/**
	 * @var base_access_maper
	 */
	private $access_maper;
	private $levels = array();
	private $statuses = array();
	private $min_level = -1;
	private $default_level = null;
	private $default_checkbox_column_name = null;
	private $required_level_field_name = null;
	
	public function __construct($field_name, $field_title, base_access_maper $access_maper)
	{
		parent::__construct($field_name, $field_title);
		$this->access_maper = $access_maper;
	}

	public function add_level($level, $title)
	{
		$status = $this->access_maper->get_status_from_level($level);
		$this->levels[$level] = array(
			"status" => $status,
			"title" => $title,
		);
		$this->statuses[$status] = $level;
	}
	
	public function set_min_level($min_level)
	{
		$this->min_level = $min_level;
	}

	public function set_default_level($default_level, $default_checkbox_column_name = "access_default")
	{
		$this->default_level = $default_level;
		$this->default_checkbox_column_name = $default_checkbox_column_name;
	}
	
	public function status_exists($status)
	{
		return isset($this->statuses[$status]);
	}
	
	public function level_exists($level)
	{
		return isset($this->levels[$level]);
	}
	
	public function set_required_level_field_name($field_name)
	{
		$this->required_level_field_name = $field_name;
	}
	
	public function get_status_from_level($level)
	{
		return $this->levels[$level]["status"];
	}
	
	public function get_level_from_status($status)
	{
		return $this->statuses[$status];
	}
	
	public function get_level_title($level)
	{
		return $this->levels[$level]["title"];
	}

	public function get_levels()
	{
		return $this->levels;
	}
	
	public function get_statuses()
	{
		return $this->statuses;
	}
	
	public function get_min_level()
	{
		return $this->min_level;
	}

	public function get_default_level()
	{
		return $this->default_level;
	}
	
	public function get_default_checkbox_column_name()
	{
		return $this->default_checkbox_column_name;
	}
	
	public function get_required_level_field_name()
	{
		return $this->required_level_field_name;
	}

}

?>