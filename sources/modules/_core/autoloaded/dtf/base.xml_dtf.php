<?php

require_once PATH_INTCMF . "/select_sql.php";

abstract class base_xml_dtf
{

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var error
	 */
	protected $error;

	/**
	 * @var request
	 */
	protected $request;

	/**
	 * @var base_dtf
	 */
	protected $dtf;

	/**
	 * @var xml_loader
	 */
	protected $xml_loader;
	protected $field_name;
	protected $db_table;

	public function __construct(base_dtf $dtf, $db_table, xml_loader $xml_loader)
	{
		global $db;
		$this->db = $db;

		global $error;
		$this->error = $error;

		global $request;
		$this->request = $request;

		$this->dtf = $dtf;
		$this->db_table = $db_table;

		$this->field_name = $this->dtf->get_field_name();

		$this->xml_loader = $xml_loader;
	}

	public function modify_sql(select_sql $select_sql, $is_edit_mode = false)
	{
		$select_sql->add_select_fields("dt." . $this->field_name);
	}

	/**
	 *
	 * @param type $db_row (selected data or null if add)
	 * @param type $is_edit_mode
	 */
	public function load_additional_data($db_row = null, $is_edit_mode = false)
	{
		return;
	}

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = $parent_node->create_child_node("field");
		$dtf_node->set_attr("name", $this->field_name);
		$dtf_node->set_attr("type", $this->dtf->get_field_type());
		$dtf_node->set_attr("title", $this->dtf->get_field_title());
		$dtf_node->set_attr("comment", $this->dtf->get_comment());
		if (sizeof($deps = $this->dtf->get_dependencies()))
		{
			foreach ($deps as $dependency)
			{
				/* @var $dependency dtf_dependency */
				$dtf_node->create_child_node("dependency")
					->set_attr("inspected_column", $dependency->get_inspected_column())
					->set_attr("activation_value", $dependency->get_activation_value())
					->set_attr("actions", $dependency->get_actions());
			}
		}
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_edit_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = $parent_node->create_child_node("field", $db_row[$this->field_name]);
		$dtf_node->set_attr("name", $this->field_name);
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_xml(xnode $parent_node, $db_row)
	{
		$parent_node->set_attr($this->field_name, $db_row[$this->field_name]);
		return $parent_node;
	}

	public function has_edit_xslt()
	{
		return true;
	}

}

?>