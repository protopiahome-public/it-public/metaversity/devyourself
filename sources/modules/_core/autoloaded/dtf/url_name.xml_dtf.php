<?php

class url_name_xml_dtf extends base_xml_dtf
{

	/**
	 * @var url_name_dtf
	 */
	protected $dtf;

	public function modify_sql(select_sql $select_sql, $is_edit_mode = false)
	{
		$select_sql->add_select_fields("dt." . $this->field_name);
	}

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);
		if ($this->dtf->get_url_attribute())
		{
			$url_prefix = $this->dtf->get_url_prefix() ? : $this->request->get_prefix();
			$dtf_node->set_attr("url_prefix", $url_prefix);
		}
		if ($min_length = $this->dtf->get_min_length())
		{
			$dtf_node->set_attr("min_length", $min_length);
		}
		if ($max_length = $this->dtf->get_max_length())
		{
			$dtf_node->set_attr("max_length", $max_length);
		}
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_xml(xnode $parent_node, $db_row)
	{
		parent::fill_xml($parent_node, $db_row);
		if ($this->dtf->get_url_attribute())
		{
			$url_prefix = $this->dtf->get_url_prefix() ? : $this->request->get_prefix();
			$parent_node->set_attr($this->dtf->get_url_attribute(), $url_prefix . $db_row[$this->field_name] . "/");
		}
		return $parent_node;
	}

}

?>