<?php

class int_xml_dtf extends base_xml_dtf
{

	/**
	 * @var int_dtf
	 */
	protected $dtf;

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);
		/* @var $dtf_node xnode */
		$dtf_node->set_attr("min_value", $this->hyphen_to_minus($this->dtf->get_min_value()));
		$dtf_node->set_attr("max_value", $this->hyphen_to_minus($this->dtf->get_max_value()));
		$dtf_node->set_attr("max_length", max(strlen($this->dtf->get_min_value()), strlen($this->dtf->get_max_value())));
		if ($this->dtf->is_important())
		{
			$dtf_node->set_attr("is_important", "1");
		}
		if (!is_null($this->dtf->get_default_value()))
		{
			$dtf_node->set_attr("default_value", $this->dtf->get_default_value());
		}
		return $dtf_node;
	}

	protected function hyphen_to_minus($str)
	{
		return str_replace("-", "−", $str); // first - hyphen, second - minus (&minus; or &#8722;)
	}

}

?>