<?php

abstract class base_simple_xml_ctrl extends base_xml_ctrl
{
	/* MUST be redefined is descendants */

	protected $data_one_row = false; // One-row db query or multiple rows result
	protected $row_name = "row"; // Row node name
	protected $allow_error_404_if_no_data = false;
	protected $cache = false;
	protected $cache_key_property_name = null;

	/* MIGHT be redefinesd is descendants */
	protected $documents_per_page = 0;
	protected $dependencies_ctrl = array();
	protected $dependencies_second_param = null; // hack :-(

	/* MUST be redefined in the corresponding methods */
	/**
	 * @var mixed (string | resource | array)
	 */
	protected $result; // set_result()

	/* MIGHT be redefined in the corresponding methods */
	protected $auxil_data; // set_auxil_data()
	protected $column_type_array; // set_column_type_array()
	protected $current_page = 1; // __construct()
	protected $page_count = 0;
	protected $row_count = 0;
	protected $filter = array();
	protected $sort_fields = array();
	protected $sort_default = null;
	protected $sort = null;
	protected $sort_back = false;

	/* Internal properties */
	protected $data = null;
	protected $user_filter = false;

	public function get_xml()
	{
		$this->before_load();
		if ($this->cache && false)
		{
			$this->cache_state = XML_CTRL_CACHE_STATE_CACHED; // Might be changed in the xml_fill_function()
			if (is_array($this->cache_key_property_name))
			{
				$xml = cache_instances::get_instance_k2($this->name . "_xml_k2")->get($this->{$this->cache_key_property_name[0]}, $this->{$this->cache_key_property_name[1]}, array($this, "xml_fill_function"));
			}
			else
			{
				$xml = cache_instances::get_instance_k1($this->name . "_xml_k1")->get($this->{$this->cache_key_property_name}, array($this, "xml_fill_function"));
			}
			if ($this->dependencies_ctrl && !$this->is_error_404())
			{
				if (is_null($this->data))
				{
					/* if (is_array($this->cache_key_property_name))
					  {
					  $this->data = cache_instances::get_instance_k2($this->name . "_data_k2")->get($this->{$this->cache_key_property_name[0]}, $this->{$this->cache_key_property_name[1]});
					  }
					  else
					  {
					  $this->data = cache_instances::get_instance_k1($this->name . "_data_k1")->get($this->{$this->cache_key_property_name});
					  } */
				}
				if (is_null($this->data))
				{
					$xml = $this->xml_fill_function();
					$this->cache_state = XML_CTRL_CACHE_STATE_CACHED_PARTIALLY;
				}
			}
		}
		else
		{
			$xml = $this->xml_fill_function();
			$this->cache_state = XML_CTRL_CACHE_STATE_NO_CACHE;
		}
		if (($ret = $this->after_load()) !== true)
		{
			return is_string($ret) ? $ret : "";
		}
		if (!$this->is_error_404())
		{
			$this->process_dependencies();
		}
		return is_null($xml) ? "<{$this->name}/>" : $xml;
	}

	public function xml_fill_function()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_MISS;

		// defining data
		$this->process_filter_and_sort();
		$this->set_result();
		$this->set_auxil_data();
		$this->set_column_type_array();

		// pages
		$this->set_page_count();

		// analyzing result
		$db_result = null;
		$no_rows = false;
		if (!$this->result)
		{
			$no_rows = true;
		}
		elseif (is_string($this->result))
		{
			if ($this->dependencies_ctrl)
			{
				$this->result = $db_result = $this->db->fetch_all($this->result);
				$no_rows = $db_result ? false : true;
			}
			else
			{
				$db_result = $this->db->sql($this->result);
				$no_rows = $this->db->get_selected_row_count($db_result) ? false : true;
			}
		}
		elseif (is_resource($this->result))
		{
			$db_result = $this->result;
			$no_rows = $this->db->get_selected_row_count($db_result) ? false : true;
		}
		elseif (is_array($this->result))
		{
			$db_result = $this->result;
			$no_rows = $this->result ? false : true;
		}
		else
		{
			trigger_error("Unknown type of the result: " . gettype($this->result));
		}

		// error 404
		if ($no_rows && $this->allow_error_404_if_no_data)
		{
			$this->set_error_404();
			return null; // no cache
		}
		if ($no_rows && $this->data_one_row)
		{
			return null; // no cache
		}
		/* if ($no_rows && $this->dependencies_ctrl)
		  {
		  return null; // no cache
		  } */

		// dependencies
		$this->fill_dependencies();

		//data
		$this->modify_data($db_result, $no_rows);
		$this->save_data();

		// filter and sort info -> XML
		$xdom = xdom::create($this->name);
		if (sizeof($this->sort_fields) or $this->sort_default)
		{
			$sort_node = $xdom->create_child_node("sort");
			$sort_node->set_attr("sort", $this->sort);
			$sort_node->set_attr("sort_back", $this->sort_back ? "1" : "0");
			$sort_node->set_attr("sort_default", $this->sort_default);
		}
		if (sizeof($this->filter))
		{
			$filter_node = $xdom->create_child_node("filter");
			$filter_node->set_attr("user_filter", $this->user_filter ? "1" : "0");
			foreach ($this->filter as $field_name => $field_value)
			{
				$filter_node->create_child_node("field", $field_value)->set_attr("name", $field_name);
			}
		}

		// data -> XML
		if (!$this->data_one_row)
		{
			if (is_array($this->cache_key_property_name))
			{
				$xdom->set_attr($this->cache_key_property_name[0], $this->{$this->cache_key_property_name[0]});
				$xdom->set_attr($this->cache_key_property_name[1], $this->{$this->cache_key_property_name[1]});
			}
			elseif ($this->cache_key_property_name)
			{
				$xdom->set_attr($this->cache_key_property_name, $this->{$this->cache_key_property_name});
			}
		}
		foreach ($this->get_additional_node_params() as $idx => $val)
		{
			$xdom->set_attr($idx, $val);
		}
		if (!$no_rows)
		{
			$this->db_xml_converter->build_xml($xdom, $db_result, $this->data_one_row, $this->row_name, $this->auxil_data, $this->column_type_array);
			$this->process_pages($xdom);
		}
		$this->modify_xml($xdom);
		return $xdom->get_xml(true) . $this->append_xml();
	}

	protected function get_additional_node_params()
	{
		return array();
	}

	protected function before_load()
	{
		// $this->cache = true|false;
		return;
	}

	protected function after_load()
	{
		return true;
	}

	protected function modify_xml(xdom $xdom)
	{
		return;
	}

	protected function append_xml()
	{
		return "";
	}

	protected function process_filter_and_sort()
	{
		foreach ($this->filter as $param_name => &$value)
		{
			if (isset($_GET[$param_name]))
			{
				$value = $_GET[$param_name];
				$this->user_filter = true;
			}
		}
		if (isset($_GET["sort"]) and in_array($_GET["sort"], $this->sort_fields))
		{
			$this->sort = $_GET["sort"];
		}
		else
		{
			$this->sort = $this->sort_default;
		}
		$this->sort_back = isset($_GET["sort_back"]) && $_GET["sort_back"] === "1";
	}

	abstract protected function set_result();

	// $this->result = "SELECT ..."; // string
	// $this->result = $this->db->sql("SELECT ..."); // resource
	// $this->result = $this->db->fetch_all("SELECT ..."); // array

	protected function set_auxil_data()
	{
		// $this->auxil_data = array(...
		return;
	}

	protected function set_column_type_array()
	{
		// $this->column_type_array = array(...
		return;
	}

	protected function get_limit_statement()
	{
		$limit = "";
		if ($this->documents_per_page)
		{
			$limit = ($this->current_page - 1) * $this->documents_per_page;
			$limit .= ", ";
			$limit .= $this->documents_per_page;
		}
		if ($limit)
		{
			$limit = "LIMIT " . $limit;
		}
		return $limit;
	}

	protected function fill_dependencies()
	{
		if ($this->dependencies_ctrl)
		{
			$this->data = array();
			$this->data["dependencies"] = array();
			foreach ($this->dependencies_ctrl as $ctrl => $idx)
			{
				$this->data["dependencies"][$ctrl] = array();
				if (is_array($this->result))
				{
					foreach ($this->result as $row)
					{
						if (is_array($idx))
						{
							foreach ($idx as $idx2)
							{
								$this->data["dependencies"][$ctrl][$row[$idx2]] = true;
							}
						}
						else
						{
							$this->data["dependencies"][$ctrl][$row[$idx]] = true;
						}
					}
				}
			}
		}
	}

	protected function modify_data($db_result, $no_rows)
	{
		// $this->data[...] = ...
	}

	private function save_data()
	{
		if ($this->cache && !is_null($this->data) && false)
		{
			if (is_array($this->cache_key_property_name))
			{
				cache_instances::get_instance_k2($this->name . "_data_k2")->set($this->{$this->cache_key_property_name[0]}, $this->{$this->cache_key_property_name[1]}, $this->data);
			}
			else
			{
				cache_instances::get_instance_k1($this->name . "_data_k1")->set($this->{$this->cache_key_property_name}, $this->data);
			}
		}
	}

	protected function process_dependencies()
	{
		if ($this->dependencies_ctrl and isset($this->data["dependencies"]))
		{
			foreach ($this->data["dependencies"] as $ctrl => $params)
			{
				$class = $ctrl . "_xml_ctrl";
				foreach ($params as $param => $tmp)
				{
					if (!$param)
					{
						continue;
					}
					if ($this->dependencies_second_param)
					{
						$this->xml_loader->add_xml_by_class_name($class, $param, $this->{$this->dependencies_second_param});
					}
					else
					{
						$this->xml_loader->add_xml_by_class_name($class, $param);
					}
				}
			}
		}
	}

	private function set_page_count()
	{
		if ($this->documents_per_page)
		{
			if ($this->row_count == 0)
			{
				$this->row_count = $this->db->get_value("SELECT FOUND_ROWS()");
			}
			$this->page_count = ceil($this->row_count / $this->documents_per_page);
		}
	}

	private function process_pages(xnode $parent_node)
	{
		if ($this->documents_per_page)
		{
			require_once PATH_INTCMF . "/paging.php";
			$paging = new paging($parent_node);
			$pages_node = $paging->process($this->page_count, $this->current_page, 2);
			$pages_node->set_attr("row_count", $this->row_count);
			$pages_node->set_attr("from_row", $this->documents_per_page * ($this->current_page - 1) + 1);
			$to_row = $this->documents_per_page * $this->current_page;
			$pages_node->set_attr("to_row", $to_row <= $this->row_count ? $to_row : $this->row_count);
		}
	}

}

?>