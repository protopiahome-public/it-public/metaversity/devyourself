<?php

class string_to_xml_xml_ctrl extends base_xml_ctrl
{

	protected $xml;

	public function __construct($xml)
	{
		$this->xml = $xml;
		parent::__construct();
	}

	public function get_xml()
	{
		return $this->xml;
	}

}

?>
