<?php

abstract class base_dt_show_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $dt_name;
	protected $axis_name;
	// Internal
	protected $id;

	/**
	 * @var base_dt
	 */
	protected $dt;
	protected $dtfs = array();
	protected $xml_dtfs = array();

	public function __construct($id)
	{
		$this->id = $id;
		parent::__construct();
	}

	protected function dt_init()
	{
		$class_reflection = new ReflectionClass($this->dt_name . "_dt");
		$this->dt = $class_reflection->newInstance();
		$this->dtfs = $this->dt->get_fields_by_axis($this->axis_name);
		$this->fill_dtf_reads();
	}

	protected function on_after_dt_init()
	{
		return;
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->dt_init();
		$this->on_after_dt_init();
		$select_sql = new select_sql();
		$select_sql->add_select_fields("dt.id");
		if ($this->dt->get_add_timestamp_column())
		{
			$select_sql->add_select_fields("dt." . $this->dt->get_add_timestamp_column());
		}
		if ($this->dt->get_edit_timestamp_column())
		{
			$select_sql->add_select_fields("dt." . $this->dt->get_edit_timestamp_column());
		}
		foreach ($this->xml_dtfs as $xml_dtf)
		{
			/* @var $xml_dtf base_xml_dtf */
			$xml_dtf->modify_sql($select_sql, false);
		}
		$select_sql->add_from($this->dt->get_db_table(), "dt");
		$select_sql->add_where("dt.id = " . $this->id);
		$this->modify_sql($select_sql);
		$this->data = $this->db->fetch_all($select_sql->get_sql());
		if (!empty($this->data))
		{
			foreach ($this->xml_dtfs as $xml_dtf)
			{
				/* @var $xml_dtf base_xml_dtf */
				$xml_dtf->load_additional_data($this->data[0], false);
			}
		}
		else
		{
			$this->set_error_404();
		}
	}

	protected function modify_sql(select_sql $select_sql)
	{
		return;
	}

	protected function fill_xml(xdom $xdom)
	{
		$xdom->set_attr("id", $this->id);
		foreach ($this->xml_dtfs as $xml_dtf)
		{
			/* @var $xml_dtf base_xml_dtf */
			$xml_dtf->fill_xml($xdom, $this->data[0]);
		}
		if ($column = $this->dt->get_add_timestamp_column())
		{
			$xdom->set_attr($column, $this->data[0][$column]);
		}
		if ($column = $this->dt->get_edit_timestamp_column())
		{
			$xdom->set_attr($column, $this->data[0][$column]);
		}
	}

	private function fill_dtf_reads()
	{
		foreach ($this->dtfs as $dtf)
		{
			/* @var $dtf base_dtf */
			$field_name = $dtf->get_field_name();
			$field_type = $dtf->get_field_type();
			$class_reflection = new ReflectionClass($field_type . "_xml_dtf");
			$this->xml_dtfs[$field_name] = $class_reflection->newInstance($dtf, $this->dt->get_db_table(), $this->xml_loader);
		}
	}

}

?>