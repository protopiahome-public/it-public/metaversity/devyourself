<?php

require_once PATH_INTCMF . "/select_sql.php";

abstract class base_easy_xml_ctrl extends base_xml_ctrl
{

	// Settings
	// array(
	//   array(
	//     "column" => ...,
	//     "ctrl" => ...,
	//     "param2" => ...,
	//     "param3" => ...,
	//     "param4" => ...,
	//   ),
	//   ...
	// );
	protected $dependencies_settings = array();
	protected $xml_attrs = array();
	protected $xml_row_name = null;
	// Internal
	protected $easy_processors = array();
	protected $data = null;
	protected $dependencies = array();
	protected $xml = "";
	protected $xslt_names_as_keys = array();
	protected $store_data = null;
	
	public function get_xml()
	{
		$disable_cache = false;
		foreach ($this->easy_processors as $easy_processor)
		{
			/* @var $easy_processor base_easy_processor */
			$easy_processor->set_easy_xml_ctrl($this);
			if ($easy_processor->disable_cache())
			{
				$disable_cache = true;
			}
		}
		$cached_data = null;
		$cache = $this->get_cache();

		if ($cache and !$disable_cache)
		{
			$cached_data = $cache->get();
			if (is_null($cached_data))
			{
				$this->cache_state = XML_CTRL_CACHE_STATE_MISS;
			}
			elseif (sizeof($cached_data) == 4)
			{
				$this->cache_state = XML_CTRL_CACHE_STATE_CACHED;
			}
			else
			{
				trigger_error("Broken cache: '" . var_export($cached_data, true) . "'");
				$this->cache_state = XML_CTRL_CACHE_STATE_CACHED_PARTIALLY;
				$cached_data = null;
			}
		}
		else
		{
			$this->cache_state = XML_CTRL_CACHE_STATE_NO_CACHE;
		}

		if (!is_null($cached_data))
		{
			$this->xml = $cached_data["xml"];
			$this->store_data = $cached_data["data"];
			$this->dependencies = $cached_data["deps"];
			$this->xslt_names_as_keys = $cached_data["xslt"];
		}
		else
		{
			if (is_null($this->data))
			{
				$select_sql = $this->get_basic_select_sql();
				$this->load_data($select_sql);
			}
			$this->process_data();
			foreach ($this->easy_processors as $easy_processor)
			{
				/* @var $easy_processor base_easy_processor */
				$easy_processor->process_data($this->data);
			}
			if (!$this->is_error_404())
			{
				$xdom = $this->get_xdom();
				if (!is_null($this->data))
				{
					$this->fill_xml($xdom);
					foreach ($this->easy_processors as $easy_processor)
					{
						/* @var $easy_processor base_easy_processor */
						$easy_processor->modify_xml($xdom);
					}
				}
				$this->modify_xml($xdom);
				$this->xml = $xdom->get_xml(true) . $this->append_xml();
				$this->fill_dependencies();
				$cache = $this->get_cache();
				if ($cache and !$disable_cache)
				{
					$cached_data = array(
						"xml" => $this->xml,
						"data" => $this->store_data,
						"deps" => $this->dependencies,
						"xslt" => $this->xslt_names_as_keys,
					);
					$cache->set($cached_data);
				}
			}
		}
		if (!$this->is_error_404())
		{
			$this->postprocess();
			$this->process_dependencies();
			$this->process_xslt();
		}
		return $this->xml;
	}

	public function set_data($data)
	{
		$this->data = $data;
	}

	public function add_easy_processor(base_easy_processor $easy_processor)
	{
		$this->easy_processors[] = $easy_processor;
	}

	public function add_xslt($xslt_name, $module_name)
	{
		//@dm9 Module and xslt must be both in key and value
		$this->xslt_names_as_keys[$xslt_name] = $module_name;
	}

	/**
	 * @return cache
	 */
	protected function get_cache()
	{
		return false;
	}

	abstract protected function load_data(select_sql $select_sql = null);

	protected function fill_xml(xdom $xdom)
	{
		$this->db_xml_converter->build_xml($xdom, $this->data, is_null($this->xml_row_name), $this->xml_row_name, $this->get_auxil_data(), $this->get_column_type_array());
	}

	protected function get_auxil_data()
	{
		return array();
	}

	protected function get_column_type_array()
	{
		return array();
	}

	protected function process_data()
	{
		return;
	}

	protected function modify_xml(xdom $xdom)
	{
		return;
	}

	protected function append_xml()
	{
		return "";
	}

	protected function postprocess()
	{
		return;
	}

	protected function fill_dependencies()
	{
		if (is_array($this->data) && sizeof($this->dependencies_settings))
		{
			foreach ($this->data as $row)
			{
				if (!is_array($row))
				{
					return;
				}
				foreach ($this->dependencies_settings as $dep_settings_data)
				{
					if (isset($row[$dep_settings_data["column"]])) // for NULL columns
					{
						$dep_data = array();
						$dep_data["ctrl"] = $dep_settings_data["ctrl"];
						$dep_data["params"] = array($row[$dep_settings_data["column"]]);
						$k = 2;
						$key = "param" . $k;
						while (isset($dep_settings_data[$key]) and $dep_settings_data[$key])
						{
							$dep_data["params"][] = is_string($dep_settings_data[$key]) ? $this->{$dep_settings_data[$key]} : $dep_settings_data[$key];
							++$k;
							$key = "param" . $k;
						}
						$this->dependencies[] = $dep_data;
					}
				}
			}
		}
	}

	protected function get_basic_select_sql()
	{
		$select_sql = new select_sql();
		foreach ($this->easy_processors as $easy_processor)
		{
			/* @var $easy_processor base_easy_processor */
			$easy_processor->modify_sql($select_sql);
		}
		return $select_sql;
	}

	protected function get_selected_ids($id_column = "id")
	{
		$ids = array();
		foreach ($this->data as $row)
		{
			$ids[$row[$id_column]] = true;
		}
		return array_keys($ids);
	}

	private function process_dependencies()
	{
		foreach ($this->dependencies as $dep_data)
		{
			$ctrl = $dep_data["ctrl"];
			$params = $dep_data["params"];
			$class = $ctrl . "_xml_ctrl";
			array_unshift($params, $class);
			call_user_func_array(array($this->xml_loader, "add_xml_by_class_name"), $params);
		}
	}

	private function process_xslt()
	{
		foreach ($this->xslt_names_as_keys as $xslt_name => $module_name)
		{
			$this->xml_loader->add_xslt($xslt_name, $module_name);
		}
	}

	/**
	 * @return xdom
	 */
	protected function get_xdom()
	{
		$xdom = xdom::create($this->name);
		foreach ($this->xml_attrs as $attr_name)
		{
			if (!isset($this->$attr_name))
			{
				continue;
			}
			$xdom->set_attr($attr_name, $this->$attr_name);
		}
		return $xdom;
	}

}

?>