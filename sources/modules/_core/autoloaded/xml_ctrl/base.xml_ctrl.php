<?php

define("XML_CTRL_CACHE_STATE_NO_INFO", 0);
define("XML_CTRL_CACHE_STATE_CACHE_NO_NEED", 1);
define("XML_CTRL_CACHE_STATE_NO_CACHE", 2);
define("XML_CTRL_CACHE_STATE_MISS", 3);
define("XML_CTRL_CACHE_STATE_CACHED_PARTIALLY", 4);
define("XML_CTRL_CACHE_STATE_CACHED", 5);
define("XML_CTRL_CACHE_STATE_CACHED_DIRECTLY", 6);

abstract class base_xml_ctrl extends base_ctrl
{

	protected $name;

	/**
	 * @var xml_loader
	 */
	protected $xml_loader;

	/**
	 * @var db_xml_converter
	 */
	protected $db_xml_converter;
	protected $cache_state = XML_CTRL_CACHE_STATE_NO_INFO;
	protected $module;

	public function __construct()
	{
		global $db_xml_converter;
		$this->db_xml_converter = $db_xml_converter;

		$this->name = substr(get_class($this), 0, -9);

		parent::__construct();
	}

	public function start()
	{
		return true;
	}

	public function check_rights()
	{
		return true;
	}

	abstract public function get_xml();

	public function get_cache_state()
	{
		return $this->cache_state;
	}

	public function set_xml_loader(xml_loader $xml_loader)
	{
		$this->xml_loader = $xml_loader;
	}

	protected function get_node_string($node_name = null, $attrs = array(), $content = null)
	{
		if (!$node_name)
		{
			$node_name = $this->name;
		}

		$node = '<' . $node_name;
		foreach ($attrs as $idx => $val)
		{
			if (is_bool($val))
			{
				$val = $val ? "1" : "0";
			}
			$node .= ' ' . $idx . '="' . htmlspecialchars($val, ENT_COMPAT) . '"';
		}
		$node .= $content === null ? '/>' : ('>' . $content . '</' . $node_name . '>');
		return $node;
	}

	public function get_module_name()
	{
		if (!$this->module)
		{
			$reflector = new ReflectionClass(get_class($this));
			$file = $reflector->getFileName();
			$path = dirname($file);
			$path_array = explode("/", univers_filename($path));
			foreach ($path_array as $key => $dir)
			{
				if ($dir == "modules")
				{
					$this->module = $path_array[$key + 1];
					break;
				}
			}
		}
		return $this->module;
	}

}

?>