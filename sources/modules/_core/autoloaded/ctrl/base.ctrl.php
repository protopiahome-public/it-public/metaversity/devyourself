<?php

abstract class base_ctrl extends base_mixin
{

	/**
	 * @var error
	 */
	protected $error;

	/**
	 * @var request
	 */
	protected $request;

	/**
	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var x_session_base
	 */
	protected $session;

	/**
	 * @var user
	 */
	protected $user;

	public function __construct()
	{
		global $error;
		$this->error = $error;

		global $request;
		$this->request = $request;
		
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		global $db;
		$this->db = $db;

		global $session;
		$this->session = $session;

		global $user;
		$this->user = $user;

		parent::__construct();
	}

	public function init()
	{
		return;
	}

}

?>