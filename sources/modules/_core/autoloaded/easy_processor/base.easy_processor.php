<?php

abstract class base_easy_processor
{

	/**
	 * @var db
	 */
	protected $db;
	/**
	 * @var request
	 */
	protected $request;
	/**
	 * @var base_easy_xml_ctrl
	 */
	protected $easy_xml_ctrl;
	protected $disable_cache = false;
	protected $xslt_names = array();

	public function __construct()
	{
		global $db;
		$this->db = $db;

		global $request;
		$this->request = $request;
	}

	public function set_easy_xml_ctrl(base_easy_xml_ctrl $easy_xml_ctrl)
	{
		$this->easy_xml_ctrl = $easy_xml_ctrl;
	}

	public function modify_sql(select_sql $select_sql)
	{

	}

	public function process_data(&$data)
	{

	}

	public function modify_xml(xdom $xdom)
	{

	}

	public function disable_cache()
	{
		return false;
	}

	public function get_xslt_names()
	{
		return $this->xslt_names;
	}

}

?>