<?php

class pager_db_easy_processor extends base_pager_easy_processor
{
	
	public function modify_sql(select_sql $select_sql)
	{
		$limit = ($this->page - 1) * $this->rows_per_page;
		$limit .= ", ";
		$limit .= $this->rows_per_page;
		$select_sql->set_SQL_CALC_FOUND_ROWS_modifier();
		$select_sql->set_limit($limit);
	}
	
	public function process_data(&$data)
	{
		if ($this->row_count == 0)
		{
			$this->row_count = $this->db->get_value("SELECT FOUND_ROWS()");
		}
		if (empty($data) and $this->page != 1)
		{
			$this->easy_xml_ctrl->set_error_404();
		}
		$this->calculate();
	}

}

?>