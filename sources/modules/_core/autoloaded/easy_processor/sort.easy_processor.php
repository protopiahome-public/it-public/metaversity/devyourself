<?php

class sort_easy_processor extends base_easy_processor
{
	const SORT_PARAM = "sort";
	const BACK_PARAM = "sort_back";

	protected $orders = array();
	protected $default_order = "NOT_INITIALIZED";
	protected $order;
	protected $back;

	public function add_order($name, $main_order, $back_order, $is_default = false)
	{
		$this->orders[$name] = array(
			"main" => $main_order,
			"back" => $back_order,
		);
		if ($this->default_order == "NOT_INITIALIZED" or $is_default)
		{
			$this->default_order = $name;
		}
	}
	
	public function load_order()
	{
		$input_order = $this->input_get_order();
		$this->order = $input_order && $this->order_exists($input_order) ? $input_order
				: $this->default_order;
		$this->back = $this->input_is_back();
	}

	public function modify_sql(select_sql $select_sql)
	{
		$this->load_order();
		$select_sql->add_order($this->orders[$this->order][$this->back ? "back" : "main"]);
	}

	public function modify_xml(xdom $xdom)
	{
		$sort_node = $xdom->create_child_node("sort");
		$sort_node->set_attr("order", $this->order);
		$sort_node->set_attr("back", $this->back);
		$sort_node->set_attr("default_order", $this->default_order);
	}
	
	public function get_order()
	{
		return $this->order;
	}
	
	public function get_back()
	{
		return $this->back;
	}

	public function disable_cache()
	{
		return $this->input_get_order() ? true : false;
	}

	protected function input_get_order()
	{
		return isset($_GET[self::SORT_PARAM]) ? $_GET[self::SORT_PARAM] : false;
	}

	protected function input_is_back()
	{
		return $this->input_get_order() && isset($_GET[self::BACK_PARAM]) && $_GET[self::BACK_PARAM];
	}

	protected function order_exists($order)
	{
		return isset($this->orders[$order]);
	}
	
}

?>