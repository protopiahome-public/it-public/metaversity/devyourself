<?php

/**
 * @author DileSoft 
 */
abstract class base_mixin extends base_http_issues
{

	/**
	 * @var db
	 */
	protected $db;
	protected $mixin_start_sync = false;
	protected $mixins = array();
	private $mixin_objects = array();

	public function __construct()
	{
		global $db;
		$this->db = $db;

		$this->mixin_init();
	}

	public function mixin_init()
	{
		foreach ($this->mixins as $key => $mixin_name)
		{
			$mixin_class_name = "{$mixin_name}_mixin";
			$this->mixin_objects[$key] = $mixin = new $mixin_class_name();
			if ($mixin->mixin_start_sync)
			{
				$mixin->sync_to($this);
			}
		}
	}

	public function mixin_call_method_with_mixins($name, $args = array(), $agregate_type = "call")
	{
		$results = array();
		if (count($this->mixins))
		{
			foreach ($this->mixins as $key => $mixin_name)
			{
				if (!method_exists($this->mixin_objects[$key], $name))
				{
					continue;
				}
				$results[] = $result = $this->mixin_call_method_from_mixin($this->mixin_objects[$key], $name, $args);

				if ($agregate_type == "chain" && !$result)
				{
					return false;
				}
			}
		}

		//Call method from this object at end
		$results[] = $result = call_user_func_array(array($this, $name), $args);
		if ($agregate_type == "chain" && !$result)
		{
			return false;
		}

		return $this->mixin_agregate_results($results, $agregate_type);
	}

	private function mixin_agregate_results($results, $agregate_type)
	{
		if ($agregate_type == "call")
		{
			return;
		}
		elseif ($agregate_type == "chain")
		{
			return true;
		}
		elseif ($agregate_type == "and")
		{
			$result = true;
			foreach ($results as $result_item)
			{
				$result = $result && $result_item;
			}
			return $result;
		}
		elseif ($agregate_type == "array_merge")
		{
			$result = array();
			foreach ($results as $result_item)
			{
				$result = array_merge_recursive($result, $result_item);
			}
			return $result;
		}
	}

	private function mixin_call_method_from_mixin(base_mixin $mixin, $name, $args)
	{
		return $mixin->mixin_call($this, $name, $args);
	}

	private function mixin_call(base_mixin $mixin_from, $name, $args)
	{
		if (method_exists($this, $name))
		{
			$this->sync_from($mixin_from);
			$result = call_user_func_array(array($this, $name), $args);
			$this->sync_to($mixin_from);

			return $result;
		}
	}

	private function mixin_get($name)
	{
		return array_key_exists($name, get_class_vars(get_class($this))) ? $this->$name : null;
	}

	private function mixin_set($name, $value)
	{
		if (array_key_exists($name, get_class_vars(get_class($this))))
		{
			$this->$name = $value;
		}
	}

	private function sync_from(base_mixin $mixin)
	{
		$class_vars = get_object_vars($this);

		foreach ($class_vars as $name => $default)
		{
			if (array_key_exists($name, get_class_vars("base_mixin")))
			{
				continue;
			}
			if (!array_key_exists($name, get_object_vars($mixin)))
			{
				continue;
			}
			$this->$name = $mixin->mixin_get($name);
		}
	}

	private function sync_to(base_mixin $mixin)
	{
		$class_vars = get_object_vars($this);

		foreach ($class_vars as $name => $default)
		{
			if (array_key_exists($name, get_class_vars("base_mixin")))
			{
				continue;
			}
			if (!array_key_exists($name, get_object_vars($mixin)))
			{
				continue;
			}
			$mixin->mixin_set($name, $this->$name);
		}
	}

}

?>