<?php

class multi_link_sql_filter extends base_sql_filter
{

	protected $pk_column;
	protected $link_table;
	protected $link_table_fk1_column;
	protected $link_table_fk2_column;
	protected $table2;
	protected $table2_title_column;
	protected $table2_pk_column;
	protected $table2_where;
	protected $value = null;

	public function __construct($name, $title, $pk_column, $link_table, $link_table_fk1_column, $link_table_fk2_column, $table2, $table2_title_column = "title", $table2_pk_column = "id", $table2_where = "")
	{
		$this->pk_column = $pk_column;
		$this->link_table = $link_table;
		$this->link_table_fk1_column = $link_table_fk1_column;
		$this->link_table_fk2_column = $link_table_fk2_column;
		$this->table2 = $table2;
		$this->table2_title_column = $table2_title_column;
		$this->table2_pk_column = $table2_pk_column;
		$this->table2_where = $table2_where;
		parent::__construct($name, $title);
	}

	public function fill_is_active()
	{
		$value = $this->get_filter_param();
		$this->value = is_good_id($value) ? $value : null;
		$this->is_active = !is_null($this->value);
	}

	public function modify_sql(select_sql $select_sql)
	{
		if ($this->value)
		{
			$link_table_alias = "filter_multi_link_{$this->table2}";
			$select_sql->add_join("INNER JOIN {$this->link_table} as {$link_table_alias} ON {$link_table_alias}.{$this->link_table_fk1_column} = {$this->pk_column}");
			$select_sql->add_where("{$link_table_alias}.{$this->link_table_fk2_column} = '{$this->value}'");
		}
	}

	public function modify_xml(xnode $parent_node)
	{
		$filter_node = $this->get_filter_node($parent_node);
		$filter_node->set_attr("value", $this->value);
		$select_sql = new select_sql();
		$select_sql->add_from($this->table2);
		$select_sql->add_select_fields($this->table2_pk_column);
		$select_sql->add_select_fields($this->table2_title_column);
		$select_sql->add_where($this->table2_where);
		$select_sql->add_order($this->table2_title_column);
		$options = $this->db->fetch_column_values($select_sql->get_sql(), $this->table2_title_column, $this->table2_pk_column);
		$options_node = $filter_node->create_child_node("options");
		foreach ($options as $id => $title)
		{
			$options_node->create_child_node("option")
				->set_attr("id", $id)
				->set_attr("title", $title);
		}
	}

}

?>