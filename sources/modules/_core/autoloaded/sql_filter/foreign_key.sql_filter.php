<?php

class foreign_key_sql_filter extends base_sql_filter
{

	protected $fk_column;
	protected $table;
	protected $table_pk_column;
	protected $table_title_column;
	protected $table_where;
	protected $value = null;

	public function __construct($name, $title, $fk_column, $table, $table_title_column = "title", $table_pk_column = "id", $table_where = "")
	{
		$this->fk_column = $fk_column;
		$this->table = $table;
		$this->table_title_column = $table_title_column;
		$this->table_pk_column = $table_pk_column;
		$this->table_where = $table_where;
		parent::__construct($name, $title);
	}

	public function fill_is_active()
	{
		$value = $this->get_filter_param();
		$this->value = is_good_id($value) ? $value : null;
		$this->is_active = !is_null($this->value);
	}

	public function modify_sql(select_sql $select_sql)
	{
		if ($this->value)
		{
			$select_sql->add_where("{$this->fk_column} = '{$this->value}'");
		}
	}

	public function modify_xml(xnode $parent_node)
	{
		$filter_node = $this->get_filter_node($parent_node);
		$filter_node->set_attr("value", $this->value);
		$select_sql = new select_sql();
		$select_sql->add_from($this->table);
		$select_sql->add_select_fields($this->table_pk_column);
		$select_sql->add_select_fields($this->table_title_column);
		$select_sql->add_where($this->table_where);
		$options = $this->db->fetch_column_values($select_sql->get_sql(), $this->table_title_column, $this->table_pk_column);
		$options_node = $filter_node->create_child_node("options");
		foreach ($options as $id => $title)
		{
			$options_node->create_child_node("option")
				->set_attr("id", $id)
				->set_attr("title", $title);
		}
	}

}

?>