<?php

class mcache_test extends base_test
{

	public function set_up()
	{
		global $mcache;
		$this->mcache = $mcache;
	}

	public function set_raw_AND_get_raw_test()
	{
		$this->mcache->set_raw("k1", "v");
		$this->assert_equal($this->mcache->get_raw("k1"), "v");

		$this->mcache->set_raw("k1", "v1");
		$this->mcache->set_raw("k1", "v2");
		$this->assert_equal($this->mcache->get_raw("k1"), "v2");
	}

	public function delete_raw_test()
	{
		$this->mcache->set_raw("test_key", "v");
		$this->mcache->delete_raw("test_key");
		$this->assert_false($this->mcache->get_raw("test_key"));
	}

	public function complex_data_test()
	{
		$this->mcache->set_no_tags("a", array(1, "2"));
		$data = $this->mcache->get_no_tags("a");
		$this->assert_identical($data, array(1, "2"));
	}

	public function set_no_tags_test()
	{
		$this->mcache->set_no_tags("k2", 112);
		$this->assert_identical($this->mcache->get_raw("k2"), "112");

		$this->mcache->set_no_tags(array("post", 14), 1122);
		$this->assert_identical($this->mcache->get_raw("post#14"), "1122");
	}

	public function get_no_tags_test()
	{
		$this->mcache->set_raw("p1", "v");
		$data = $this->mcache->get_no_tags("p1");
		$this->assert_equal($data, "v");

		$this->mcache->set_raw("post#18", "vvv");
		$data = $this->mcache->get_no_tags(array("post", 18));
		$this->assert_equal($data, "vvv");
	}

	public function get_multi_no_tags_test()
	{
		$this->mcache->set_raw("abc", 1);
		$this->mcache->set_raw("def", array(2));
		$data = $this->mcache->get_multi_no_tags(array("abc", "def"));
		$this->assert_identical($data, array("abc" => "1", "def" => array(2)));

		$data = $this->mcache->get_multi_no_tags(array("unexisted_key", "abc", "def", "unexisted_key2"));
		$this->assert_identical($data, array("unexisted_key" => null, "abc" => "1", "def" => array(2), "unexisted_key2" => null));
	}

	public function tag_update_test()
	{
		$this->mcache->tag_update("xxx");
		$this->assert_true(preg_match("#[0-9]+.[0-9]+#", $this->mcache->get_raw("%xxx")) == 1);

		$this->mcache->tag_update(array("post", 125));
		$this->assert_true(preg_match("#[0-9]+.[0-9]+#", $this->mcache->get_raw("%post#125")) == 1);

		$this->mcache->tag_update("a1t");
		usleep(2);
		$this->mcache->tag_update("a2t");
		$a1t_v = $this->mcache->get_raw("%a1t");
		$a2t_v = $this->mcache->get_raw("%a2t");
		$this->assert_equal($a1t_v, $a2t_v);
	}

	public function tags_get_test()
	{
		$this->mcache->tag_update("xxx");
		$this->mcache->tag_update(array("yyy", 12));
		$v1 = $this->mcache->get_raw("%xxx");
		$v2 = $this->mcache->get_raw("%yyy#12");
		$tags = $this->mcache->tags_get(array("xxx", array("yyy", 12)));
		$this->assert_identical($tags, array("%xxx" => $v1, "%yyy#12" => $v2));
	}

	public function set_test()
	{
		$this->mcache->tag_update("xtag");
		$xtag_v = $this->mcache->get_raw("%xtag");
		$this->mcache->set("g", array(1, 3), array("xtag", "ytag"));
		$data = $this->mcache->get_raw("g");
		$ytag_v = $this->mcache->get_raw("%ytag");
		$this->assert_true(preg_match("#[0-9]+.[0-9]+#", $ytag_v) == 1);
		$this->assert_identical($data, array("val" => array(1,3), "tags" => array("%xtag" => $xtag_v, "%ytag" => $ytag_v)));
	}

	public function set_without_tags_test()
	{
		$this->mcache->set("g", array(1, 3));
		$data = $this->mcache->get("g");
		$this->assert_identical($data, array(1, 3));
	}

	public function get_test()
	{
		$this->mcache->flush();

		$this->mcache->set("k0", "v0", array("xtag0", "ytag0"));
		$this->assert_equal($this->mcache->get("k0"), "v0");

		$this->mcache->set("k1", "v1", array("xtag1", "ytag1"));
		$this->mcache->delete_raw("%xtag1");
		$this->assert_null($this->mcache->get("k1"));

		$future = microtime(true) + 10;
		$this->mcache->set("k2", "v2", array("xtag2", "ytag2"));
		$this->mcache->set_raw("%xtag2", $future);
		$this->assert_null($this->mcache->get("k2"));
	}

	public function delete_test()
	{
		$this->mcache->flush();

		$this->mcache->set("k0", "v0", array("xtag0", "ytag0"));
		$this->mcache->delete("k0");
		$data = $this->mcache->get("k0");
		$this->assert_null($data);
	}

	public function get_multi_test()
	{
		$this->mcache->flush();
		$this->mcache->set("k1", "v1", array("tag1", "tag2"));
		$this->mcache->set("k2", "v2", array("tag1", "tag3"));
		$this->assert_equal($this->mcache->get_multi(array("k1", "k2")), array("k1" => "v1", "k2" => "v2"));

		$this->mcache->flush();
		$this->mcache->set("k1", "v1", array("tag1", "tag2"));
		$this->mcache->set("k2", "v2", array("tag1", "tag3"));
		$this->mcache->delete_raw("%tag1");
		$this->assert_equal($this->mcache->get_multi(array("k1", "k2")), array("k1" => null, "k2" => null));

		$this->mcache->flush();
		$this->mcache->set("k1", "v1", array("tag1", "tag2"));
		$this->mcache->set("k2", "v2", array("tag1", "tag3"));
		$this->mcache->delete_raw("%tag2");
		$this->assert_equal($this->mcache->get_multi(array("k1", "k2")), array("k1" => null, "k2" => "v2"));

		$this->mcache->flush();
		$future = microtime(true) + 10;
		$this->mcache->set("k1", "v1", array("tag1", "tag2"));
		$this->mcache->set("k2", "v2", array("tag1", "tag3"));
		$this->mcache->set_raw("%tag1", $future);
		$this->assert_equal($this->mcache->get_multi(array("k1", "k2")), array("k1" => null, "k2" => null));
	}

	public function flush_test()
	{
		$this->mcache->set_raw("test_key", "v");
		$this->mcache->flush();
		$this->assert_false($this->mcache->get_raw("test_key"));
	}

	public function tear_down()
	{
		$this->mcache->flush();
	}

}

?>