<?php

class vector_data_math_test extends base_test
{

	public function set_up()
	{
		$this->select_db("math");
	}

	public function tear_down()
	{
		unset($_GET);
	}

	public function get_future_data_test()
	{
		$vd = new vector_data_math($vector_id = 100, $user_id = 100, $vector_competence_set_id = 1);
		$this->assert_identical($vd->get_future_data($checked_only = true, $calculate_match_using_self = false), array(
			4 => array(
				"checked" => true,
				"title" => "ABC - Prof 4",
			),
			1 => array(
				"checked" => true,
				"title" => "Prof 1",
			),
		));
		$this->assert_identical($vd->get_future_data($checked_only = true, $calculate_match_using_self = true), array(
			4 => array(
				"checked" => true,
				"title" => "ABC - Prof 4",
				"match" => 67,
			),
			1 => array(
				"checked" => true,
				"title" => "Prof 1",
				"match" => 17,
			),
		));
		$this->assert_identical($vd->get_future_data($checked_only = false, $calculate_match_using_self = false), array(
			4 => array(
				"checked" => true,
				"title" => "ABC - Prof 4",
			),
			1 => array(
				"checked" => true,
				"title" => "Prof 1",
			),
			2 => array(
				"checked" => false,
				"title" => "Prof 2",
			),
			3 => array(
				"checked" => false,
				"title" => "Prof 3",
			),
		));
		$this->assert_identical($vd->get_future_data($checked_only = false, $calculate_match_using_self = true), array(
			4 => array(
				"checked" => true,
				"title" => "ABC - Prof 4",
				"match" => 67,
			),
			1 => array(
				"checked" => true,
				"title" => "Prof 1",
				"match" => 17,
			),
			2 => array(
				"checked" => false,
				"title" => "Prof 2",
				"match" => 0,
			),
			3 => array(
				"checked" => false,
				"title" => "Prof 3",
				"match" => 100,
			),
		));
	}

	public function get_future_data_no_vector_test()
	{
		$vd = new vector_data_math($vector_id = 0, $user_id = 0, $vector_competence_set_id = 1);
		$this->assert_identical($vd->get_future_data($checked_only = false, $calculate_match_using_self = false), array(
			4 => array(
				"checked" => false,
				"title" => "ABC - Prof 4",
			),
			1 => array(
				"checked" => false,
				"title" => "Prof 1",
			),
			2 => array(
				"checked" => false,
				"title" => "Prof 2",
			),
			3 => array(
				"checked" => false,
				"title" => "Prof 3",
			),
		));
		
		$this->assert_false($vd->is_self_complete());
	}

	public function get_future_competences_data_test()
	{
		$vd = new vector_data_math($vector_id = 100, $user_id = 100, $vector_competence_set_id = 1);
		$this->assert_identical($vd->get_future_competences_data(), array(
			// Removed competence (id = 106) must not be here
			101 => 3, 102 => 3, 105 => 1
		));

		$vd = new vector_data_math($vector_id = 100, $user_id = 100, $vector_competence_set_id = 1, $focus_competences_only = true);
		$this->assert_identical($vd->get_future_competences_data(), array(
			102 => 3
		));

		$vd = new vector_data_math($vector_id = 102, $user_id = 100, $vector_competence_set_id = 1);
		$this->assert_identical($vd->get_future_competences_data(), array(
			101 => 1, 102 => 1, 104 => 2, 105 => 2
		));
	}

	public function get_future_competences_details_test()
	{
		$vd = new vector_data_math($vector_id = 100, $user_id = 100, $vector_competence_set_id = 1);
		$this->assert_identical($vd->get_future_competences_details(), array(
			// Removed competence (id = 106) must not be here
			101 => array(4 => 1, 1 => 3),
			102 => array(4 => 1, 1 => 3),
			105 => array(4 => 1),
		));

		// Sorts correct order of competences (competence_id ASC)
		$vd = new vector_data_math($vector_id = 102, $user_id = 100, $vector_competence_set_id = 1);
		$this->assert_identical($vd->get_future_competences_details(), array(
			101 => array(4 => 1),
			102 => array(4 => 1),
			104 => array(3 => 2),
			105 => array(4 => 1, 3 => 2),
		));
	}

	public function get_self_stat_test()
	{
		$vd = new vector_data_math($vector_id = 100, $user_id = 100, $vector_competence_set_id = 1);
		$this->assert_identical($vd->get_self_stat(), array(
			"mark_count" => 18,
			"edit_time" => "2010-01-02 02:00:00",
		));

		$vd = new vector_data_math($vector_id = 101, $user_id = 101, $vector_competence_set_id = 1, $focus_competences_only = true);
		$this->assert_identical($vd->get_self_stat(), array(
			"mark_count" => 0,
			"edit_time" => "0000-00-00 00:00:00",
		));
	}

	public function get_self_data_test()
	{
		$vd = new vector_data_math($vector_id = 100, $user_id = 100, $vector_competence_set_id = 1);
		$this->assert_identical($vd->get_self_data(), array(
			// Removed competence (id = 106) must not be here
			101 => 1, 104 => 3, 105 => 3
		));

		$vd = new vector_data_math($vector_id = 100, $user_id = 100, $vector_competence_set_id = 1, $focus_competences_only = true);
		$this->assert_identical($vd->get_self_data(), array(
			
		));
	}

	public function is_self_complete_test()
	{
		$vd = new vector_data_math($vector_id = 100, $user_id = 100, $vector_competence_set_id = 1);
		$this->assert_false($vd->is_self_complete());
		
		$vd = new vector_data_math($vector_id = 100, $user_id = 100, $vector_competence_set_id = 1);
		$this->db->begin();
		$this->db->sql("INSERT INTO vector_self (user_id, competence_id, competence_set_id, mark) VALUES (100, 102, 1, 3)");
		$this->assert_true($vd->is_self_complete());
		$this->db->rollback();
	}
	
	public function get_focus_data_test()
	{
		$vd = new vector_data_math($vector_id = 100, $user_id = 100, $vector_competence_set_id = 1);
		$this->assert_identical($vd->get_focus_data(), array(
			// Removed competence (id = 106) must not be here
			102 => true, 103 => true,
		));
	}

	public function blank_vector_test()
	{
		$vd = new vector_data_math(101, 100, 1);
		$this->assert_identical($vd->get_future_data($checked_only = true), array());
		$this->assert_identical($vd->get_future_competences_data(), array());
		$this->assert_identical($vd->get_focus_data(), array());
	}

	public function cache_test()
	{
		$vd = new vector_data_math(100, 100, 1);
		$a = $vd->get_future_data(true, true);
		$c = $vd->get_future_data(false, true);
		$h = $vd->get_future_data(false, false);
		$b = $vd->get_future_data(true, true);
		$e = $vd->get_future_data(true, false);
		$g = $vd->get_future_data(false, false);
		$f = $vd->get_future_data(true, false);
		$d = $vd->get_future_data(false, true);
		$this->assert_identical($a, $b);
		$this->assert_identical($c, $d);
		$this->assert_identical($e, $f);
		$this->assert_identical($g, $h);

		$a = $vd->get_possible_professiograms_data();
		$b = $vd->get_possible_professiograms_data();
		$this->assert_identical($a, $b);

		$a = $vd->get_future_competences_data();
		$b = $vd->get_future_competences_data();
		$this->assert_identical($a, $b);

		$a = $vd->get_future_competences_details();
		$b = $vd->get_future_competences_details();
		$this->assert_identical($a, $b);

		$a = $vd->get_self_stat();
		$b = $vd->get_self_stat();
		$this->assert_identical($a, $b);

		$a = $vd->get_self_data();
		$b = $vd->get_self_data();
		$this->assert_identical($a, $b);

		$a = $vd->get_focus_data();
		$b = $vd->get_focus_data();
		$this->assert_identical($a, $b);
	}

}

?>