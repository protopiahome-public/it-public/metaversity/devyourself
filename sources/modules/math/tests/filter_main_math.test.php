<?php

class filter_main_math_test extends base_test
{

	public function set_up()
	{
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (1, 'x 1')");
		}
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 2"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (2, 'x 2')");
		}
		$this->db->sql("DELETE FROM user WHERE id = 99999"); // 'Unexisted' test

		$this->db->sql("REPLACE INTO competence_set (id, title) VALUES (1, 'CS TEST 1')");
		$this->db->sql("REPLACE INTO competence_set (id, title) VALUES (2, 'CS TEST 2')");
		$this->db->sql("REPLACE INTO competence_set (id, title) VALUES (3, 'CS TEST 3')");
		$this->db->sql("REPLACE INTO competence_set (id, title) VALUES (10, 'CS TEST 3')");

		$this->db->sql("REPLACE INTO project (id, title, name) VALUES (1, 'TEST 1', 'x 1')");
		$this->db->sql("REPLACE INTO project (id, title, name) VALUES (2, 'TEST 2', 'x 2')");
		$this->db->sql("REPLACE INTO project (id, title, name) VALUES (3, 'TEST 3', 'x 3')");
		$this->db->sql("REPLACE INTO project (id, title, name) VALUES (4, 'TEST 4', 'x 4')");

		$this->db->sql("REPLACE INTO competence_translator (id, from_competence_set_id, to_competence_set_id, title) VALUES (1, 1, 2, '1->2')");
		$this->db->sql("REPLACE INTO competence_translator (id, from_competence_set_id, to_competence_set_id, title) VALUES (2, 2, 1, '2->1')");
		$this->db->sql("REPLACE INTO competence_translator (id, from_competence_set_id, to_competence_set_id, title) VALUES (3, 3, 1, '3->1/1')");
		$this->db->sql("REPLACE INTO competence_translator (id, from_competence_set_id, to_competence_set_id, title) VALUES (4, 3, 1, '3->1/2')");
		$this->db->sql("REPLACE INTO competence_translator (id, from_competence_set_id, to_competence_set_id, title) VALUES (10, 1, 10, '1->10')");

		$this->db->sql("TRUNCATE project_competence_set_link_calc");
		$this->db->sql("TRUNCATE user_competence_set_project_link");

		$this->db->sql("INSERT INTO project_competence_set_link_calc (project_id, competence_set_id, total_mark_count_calc) VALUES (1, 1, 1000)");
		$this->db->sql("INSERT INTO project_competence_set_link_calc (project_id, competence_set_id, total_mark_count_calc) VALUES (1, 2, 500)");
		$this->db->sql("INSERT INTO project_competence_set_link_calc (project_id, competence_set_id, total_mark_count_calc) VALUES (2, 1, 200)");
		$this->db->sql("INSERT INTO project_competence_set_link_calc (project_id, competence_set_id, total_mark_count_calc) VALUES (3, 2, 400)");
		$this->db->sql("INSERT INTO project_competence_set_link_calc (project_id, competence_set_id, total_mark_count_calc) VALUES (4, 1, 300)");
		$this->db->sql("INSERT INTO project_competence_set_link_calc (project_id, competence_set_id, total_mark_count_calc) VALUES (4, 3, 777)");

		$this->db->sql("INSERT INTO user_competence_set_project_link (user_id, project_id, competence_set_id, total_mark_count_calc) VALUES (1, 1, 1, 1 + 1000)");
		$this->db->sql("INSERT INTO user_competence_set_project_link (user_id, project_id, competence_set_id, total_mark_count_calc) VALUES (1, 1, 2, 1 + 500)");
		$this->db->sql("INSERT INTO user_competence_set_project_link (user_id, project_id, competence_set_id, total_mark_count_calc) VALUES (1, 2, 1, 1 + 200)");
		$this->db->sql("INSERT INTO user_competence_set_project_link (user_id, project_id, competence_set_id, total_mark_count_calc) VALUES (1, 3, 2, 1 + 400)");
		$this->db->sql("INSERT INTO user_competence_set_project_link (user_id, project_id, competence_set_id, total_mark_count_calc) VALUES (1, 4, 1, 1 + 300)");
		$this->db->sql("INSERT INTO user_competence_set_project_link (user_id, project_id, competence_set_id, total_mark_count_calc) VALUES (1, 4, 3, 1 + 777)");

		$this->db->sql("INSERT INTO user_competence_set_project_link (user_id, project_id, competence_set_id, total_mark_count_calc) VALUES (2, 1, 1, 1000)");
		$this->db->sql("INSERT INTO user_competence_set_project_link (user_id, project_id, competence_set_id, total_mark_count_calc) VALUES (2, 1, 2, 500)");
		$this->db->sql("INSERT INTO user_competence_set_project_link (user_id, project_id, competence_set_id, total_mark_count_calc) VALUES (2, 2, 1, 200)");
		$this->db->sql("INSERT INTO user_competence_set_project_link (user_id, project_id, competence_set_id, total_mark_count_calc) VALUES (2, 3, 2, 400)");
		$this->db->sql("INSERT INTO user_competence_set_project_link (user_id, project_id, competence_set_id, total_mark_count_calc) VALUES (2, 4, 1, 300)");
		$this->db->sql("INSERT INTO user_competence_set_project_link (user_id, project_id, competence_set_id, total_mark_count_calc) VALUES (2, 4, 3, 777)");
	}

	public function tear_down()
	{
		unset($_GET);
	}

	public function construct_complex_test()
	{
		unset($_GET);

		$correct_filter = array(
			1 => array(
				"projects" => array(
					1 => array("title" => "TEST 1", "total_mark_count" => 1000, "is_selected" => true),
					4 => array("title" => "TEST 4", "total_mark_count" => 300, "is_selected" => true),
					2 => array("title" => "TEST 2", "total_mark_count" => 200, "is_selected" => true),
				)
			),
			3 => array(
				"translators" => array(
					3 => array(
						"translator_id" => 3,
						"translator_title" => "3->1/1",
						"from_competence_set_id" => 3,
						"from_competence_set_title" => "CS TEST 3",
						"is_default" => 0,
					),
					4 => array(
						"translator_id" => 4,
						"translator_title" => "3->1/2",
						"from_competence_set_id" => 3,
						"from_competence_set_title" => "CS TEST 3",
						"is_default" => 0,
					),
				),
				"projects" => array(
					4 => array("title" => "TEST 4", "total_mark_count" => 777),
				)
			),
			2 => array(
				"translators" => array(
					2 => array(
						"translator_id" => 2,
						"translator_title" => "2->1",
						"from_competence_set_id" => 2,
						"from_competence_set_title" => "CS TEST 2",
						"is_default" => 0,
					),
				),
				"projects" => array(
					1 => array("title" => "TEST 1", "total_mark_count" => 500),
					3 => array("title" => "TEST 3", "total_mark_count" => 400),
				),
			),
		);

		$correct_selected_projects = array(
			"project_1_1" => array(
				"project_id" => 1,
				"competence_set_id" => 1,
				"translator_id" => 0,
			),
			"project_1_4" => array(
				"project_id" => 4,
				"competence_set_id" => 1,
				"translator_id" => 0,
			),
			"project_1_2" => array(
				"project_id" => 2,
				"competence_set_id" => 1,
				"translator_id" => 0,
			),
		);

		// without user - basic test
		$filter_main = new filter_main_math(1);
		$real_filter = $filter_main->get_filter();
		$real_selected_projects = $filter_main->get_selected_projects();
		$this->assert_identical($real_filter, $correct_filter);
		$this->assert_identical($real_selected_projects, $correct_selected_projects);

		// with user - basic test
		$filter_main = new filter_main_math(1, array(2));
		$real_filter = $filter_main->get_filter();
		$real_selected_projects = $filter_main->get_selected_projects();
		$this->assert_identical($real_filter, $correct_filter);
		$this->assert_identical($real_selected_projects, $correct_selected_projects);

		// default selection of the translator test
		// @todo refactor this method; $correct_filter can be global variable (or $correct_filter = get_std_filter())
		$this->db->sql("UPDATE competence_translator SET is_default = 1 WHERE id = 2");
		$correct_filter[2]["translators"][2]["is_default"] = 1;
		$correct_filter[2]["translators"][2]["is_selected"] = true;
		$correct_filter[2]["projects"][1]["is_selected"] = true;
		$correct_filter[2]["projects"][3]["is_selected"] = true;
		$correct_selected_projects["project_2_1"] = array(
			"project_id" => 1,
			"competence_set_id" => 2,
			"translator_id" => 2,
		);
		$correct_selected_projects["project_2_3"] = array(
			"project_id" => 3,
			"competence_set_id" => 2,
			"translator_id" => 2,
		);
		$filter_main = new filter_main_math(1);
		$real_filter = $filter_main->get_filter();
		$real_selected_projects = $filter_main->get_selected_projects();
		$this->assert_identical($real_filter, $correct_filter);
		$this->assert_identical($real_selected_projects, $correct_selected_projects);
		$this->db->sql("UPDATE competence_translator SET is_default = 0 WHERE id = 2");
		$correct_filter[2]["translators"][2]["is_default"] = 0;
		unset($correct_filter[2]["translators"][2]["is_selected"]);
		unset($correct_filter[2]["projects"][1]["is_selected"]);
		unset($correct_filter[2]["projects"][3]["is_selected"]);
		unset($correct_selected_projects["project_2_1"]);
		unset($correct_selected_projects["project_2_3"]);

		$correct_selected_projects = array(
			"project_1_4" => array(
				"project_id" => 4,
				"competence_set_id" => 1,
				"translator_id" => 0,
			),
			"project_3_4" => array(
				"project_id" => 4,
				"competence_set_id" => 3,
				"translator_id" => 3,
			),
		);

		$_GET["project_1_4"] = "3";
		$_GET["translator_3"] = "3";
		$_GET["translator_2"] = "2";
		$_GET["project_3_4"] = "1";

		// $ignore_user_selection test
		$filter_main = new filter_main_math(1, null, true);
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter, $correct_filter);

		unset($correct_filter[1]["projects"][1]["is_selected"]);
		unset($correct_filter[1]["projects"][2]["is_selected"]);
		$correct_filter[1]["projects"][4]["is_selected"] = true;
		$correct_filter[3]["projects"][4]["is_selected"] = true;
		$correct_filter[2]["translators"][2]["is_selected"] = true;
		$correct_filter[3]["translators"][3]["is_selected"] = true;

		// selection check
		$filter_main = new filter_main_math(1);
		$real_filter = $filter_main->get_filter();
		$real_selected_projects = $filter_main->get_selected_projects();
		$this->assert_identical($real_filter, $correct_filter);
		$this->assert_identical($real_selected_projects, $correct_selected_projects);

		$_GET["translator_3"] = "4";
		$correct_selected_projects["project_3_4"]["translator_id"] = 4;
		unset($correct_filter[3]["translators"][3]["is_selected"]);
		$correct_filter[3]["translators"][4]["is_selected"] = true;

		// we have changed translator_id
		$filter_main = new filter_main_math(1);
		$real_filter = $filter_main->get_filter();
		$real_selected_projects = $filter_main->get_selected_projects();
		$this->assert_identical($real_filter, $correct_filter);
		$this->assert_identical($real_selected_projects, $correct_selected_projects);

		unset($_GET["translator_3"]);
		unset($correct_filter[3]["projects"][4]["is_selected"]);
		unset($correct_filter[3]["translators"][4]["is_selected"]);

		// unset field with translator's id
		$filter_main = new filter_main_math(1);
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter, $correct_filter);

		$_GET["translator_3"] = "66";

		// incorrect translator's id
		$filter_main = new filter_main_math(1);
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter, $correct_filter);
	}

	public function mark_count_test()
	{
		unset($_GET);

		$filter_main = new filter_main_math(1);
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter[1]["projects"][1]["total_mark_count"], 1000);

		$filter_main = new filter_main_math(1, array(1));
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter[1]["projects"][1]["total_mark_count"], 1001);

		$filter_main = new filter_main_math(1, array(1, 99999)); // 99999 - unexisted user
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter[1]["projects"][1]["total_mark_count"], 1001);

		$filter_main = new filter_main_math(1, array(2));
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter[1]["projects"][1]["total_mark_count"], 1000);

		$filter_main = new filter_main_math(1, array(1, 2));
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter[1]["projects"][1]["total_mark_count"], 2001);
	}

	public function construst_no_users_test()
	{
		unset($_GET);

		$filter_main = new filter_main_math(1, array());
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter, array());

		$filter_main = new filter_main_math(1, array(99999)); // 99999 - unexisted user
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter, array());
	}

	public function construct_only_main_test()
	{
		unset($_GET);

		$correct_filter = array(
			3 => array(
				"projects" => array(
					4 => array("title" => "TEST 4", "total_mark_count" => 777, "is_selected" => true),
				)
			),
		);

		$filter_main = new filter_main_math(3);
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter, $correct_filter);

		$filter_main = new filter_main_math(3, array(2));
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter, $correct_filter);
	}

	public function construct_unexisted_competence_set_test()
	{
		unset($_GET);

		$correct_filter = array(
		);

		$filter_main = new filter_main_math(10000);
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter, $correct_filter);

		$filter_main = new filter_main_math(10000, array(2));
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter, $correct_filter);
	}

	public function construct_no_main_test()
	{
		unset($_GET);

		$correct_filter = array(
			1 => array(
				"translators" => array(
					10 => array(
						"translator_id" => 10,
						"translator_title" => "1->10",
						"from_competence_set_id" => 1,
						"from_competence_set_title" => "CS TEST 1",
						"is_default" => 0,
					),
				),
				"projects" => array(
					1 => array("title" => "TEST 1", "total_mark_count" => 1000),
					4 => array("title" => "TEST 4", "total_mark_count" => 300),
					2 => array("title" => "TEST 2", "total_mark_count" => 200),
				)
			),
		);

		// without user - basic test
		$filter_main = new filter_main_math(10);
		$real_filter = $filter_main->get_filter();
		$this->assert_identical($real_filter, $correct_filter);
	}

	public function get_competence_set_id_test()
	{
		unset($_GET);

		$filter_main = new filter_main_math(10);
		$this->assert_identical($filter_main->get_competence_set_id(), 10);
	}

	public function project_restriction_test()
	{
		unset($_GET);

		$this->db->sql("UPDATE competence_translator SET is_default = 1 WHERE id = 2");

		$correct_filter = array(
			1 => array(
				"projects" => array(
					1 => array("title" => "TEST 1", "total_mark_count" => 1000, "is_selected" => true),
					4 => array("title" => "TEST 4", "total_mark_count" => 300),
					2 => array("title" => "TEST 2", "total_mark_count" => 200),
				)
			),
			3 => array(
				"translators" => array(
					3 => array(
						"translator_id" => 3,
						"translator_title" => "3->1/1",
						"from_competence_set_id" => 3,
						"from_competence_set_title" => "CS TEST 3",
						"is_default" => 0,
					),
					4 => array(
						"translator_id" => 4,
						"translator_title" => "3->1/2",
						"from_competence_set_id" => 3,
						"from_competence_set_title" => "CS TEST 3",
						"is_default" => 0,
					),
				),
				"projects" => array(
					4 => array("title" => "TEST 4", "total_mark_count" => 777),
				)
			),
			2 => array(
				"translators" => array(
					2 => array(
						"translator_id" => 2,
						"translator_title" => "2->1",
						"from_competence_set_id" => 2,
						"from_competence_set_title" => "CS TEST 2",
						"is_default" => 1,
						"is_selected" => true,
					),
				),
				"projects" => array(
					1 => array("title" => "TEST 1", "total_mark_count" => 500, "is_selected" => true),
					3 => array("title" => "TEST 3", "total_mark_count" => 400),
				),
			),
		);

		$correct_selected_projects = array(
			"project_1_1" => array(
				"project_id" => 1,
				"competence_set_id" => 1,
				"translator_id" => 0,
			),
			"project_2_1" => array(
				"project_id" => 1,
				"competence_set_id" => 2,
				"translator_id" => 2,
			),
		);

		$filter_main = new filter_main_math(1, array(2), false, 1);
		$real_filter = $filter_main->get_filter();
		$real_selected_projects = $filter_main->get_selected_projects();
		$this->assert_identical($real_filter, $correct_filter);
		$this->assert_identical($real_selected_projects, $correct_selected_projects);

		$this->db->sql("UPDATE competence_translator SET is_default = 0 WHERE id = 2");
	}

}

?>