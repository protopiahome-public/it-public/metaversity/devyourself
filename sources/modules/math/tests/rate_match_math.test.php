<?php

class rate_match_math_test extends base_test
{

	private $results;

	private $rate_data;

	public function set_up()
	{
		$this->select_db("math");

		$this->results = array(
			1 => array(
				"sum" => array(
					1 => 3,
					3 => 2,
				),
			),
			2 => array(
				"sum" => array(
					1 => 3,
					2 => 1,
					3 => 2,
				),
			),
			3 => array(
				"sum" => array(
					999 => 3,
				),
			),
		);

		$this->rate_data = array(
			array(
				"id" => 1,
				"competences" => array(2 => 2, 3 => 2,),
			),
			array(
				"id" => 2,
				"competences" => array(1 => 1, 2 => 3,),
			),
			array(
				"id" => 3,
				"competences" => array(1 => 1, 2 => 2, 3 => 2,),
			),
			array(
				"id" => 4,
				"competences" => array(1000 => 1),
			),
			array(
				"id" => 5,
				"competences" => array(),
			),
		);
	}

	public function get_user_rate_match_test()
	{
		$rate_match = new rate_match_math($this->results, $this->rate_data);
		$this->assert_identical($rate_match->get_user_rate_match(2, 0), 75);
	}

	public function get_user_rating_for_rate_test()
	{
		$rate_match = new rate_match_math($this->results, $this->rate_data);
		$this->assert_identical($rate_match->get_user_rating_for_rate(2), array(
			2 => 80,
			1 => 60,
			//3 => 0, // no zero results!
		));
	}

	public function get_rate_rating_for_user_test()
	{
		$rate_match = new rate_match_math($this->results, $this->rate_data);
		$this->assert_identical($rate_match->get_rate_rating_for_user(1), array(
			3 => 60,
			1 => 50,
			2 => 25,
			5 => 0, // zero results are allowed!
			4 => 0, // zero results are allowed!
		));
	}

	public function integration_test()
	{
		unset($_GET);
		
		$results = new results_math(1, array(1));
		$results = $results->get_results();

		$professiogram_data = new professiogram_data_math();
		$professiogram_data = $professiogram_data->get_professiograms_data_by_competence_set(1, 0, false);

		$rate_match = new rate_match_math($results, $professiogram_data);
		$this->assert_identical($rate_match->get_rate_rating_for_user(1), array(
			1 => 100,
			2 => 100,
			4 => 67,
			3 => 25,
		));
	}

}

?>