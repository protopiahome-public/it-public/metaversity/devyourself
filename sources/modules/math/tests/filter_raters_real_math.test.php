<?php

class filter_raters_real_math_test extends base_test
{

	public function set_up()
	{
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (1, 'x 1')");
		}
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 2"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (2, 'x 2')");
		}
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 3"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (3, 'x 3')");
		}
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 4"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (4, 'x 4')");
		}

		$this->db->sql("REPLACE INTO user_group (id, title) VALUES (1, 'TEST 1')");
		$this->db->sql("REPLACE INTO user_group (id, title) VALUES (2, 'TEST 2')");
		$this->db->sql("REPLACE INTO user_group (id, title) VALUES (3, 'TEST 3')");
		$this->db->sql("REPLACE INTO user_group (id, title) VALUES (4, 'TEST 4')");

		$this->db->sql("REPLACE INTO user_user_group_link (user_id, user_group_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO user_user_group_link (user_id, user_group_id) VALUES (1, 2)");
		$this->db->sql("REPLACE INTO user_user_group_link (user_id, user_group_id) VALUES (2, 2)");
		$this->db->sql("REPLACE INTO user_user_group_link (user_id, user_group_id) VALUES (3, 3)");
	}

	public function main_test()
	{
		$filter_raters_real = new filter_raters_real_math(array(1 => true), array(1 => true, 2 => true, 3 => true));
		$user_groups = $filter_raters_real->get_user_groups();
		$total_rater_count = $filter_raters_real->get_total_rater_count();
		$not_grouped_rater_count = $filter_raters_real->get_not_grouped_rater_count();

		$this->assert_identical($user_groups, array(
			1 => array("title" => "TEST 1", "user_count" => 1, "is_selected" => true),
			2 => array("title" => "TEST 2", "user_count" => 2),
			3 => array("title" => "TEST 3", "user_count" => 1),
		));
		$this->assert_identical($total_rater_count, 3);
		$this->assert_identical($not_grouped_rater_count, 0);
		$this->assert_identical($filter_raters_real->is_select_all_checked(), false);
	}

	public function zero_selection_test()
	{
		$filter_raters_real = new filter_raters_real_math(array(), array(1 => true, 2 => true, 3 => true));
		$user_groups = $filter_raters_real->get_user_groups();
		$total_rater_count = $filter_raters_real->get_total_rater_count();
		$not_grouped_rater_count = $filter_raters_real->get_not_grouped_rater_count();

		$this->assert_identical($user_groups, array(
			1 => array("title" => "TEST 1", "user_count" => 1, "is_selected" => true),
			2 => array("title" => "TEST 2", "user_count" => 2, "is_selected" => true),
			3 => array("title" => "TEST 3", "user_count" => 1, "is_selected" => true),
		));
		$this->assert_identical($total_rater_count, 3);
		$this->assert_identical($not_grouped_rater_count, 0);
		$this->assert_identical($filter_raters_real->is_select_all_checked(), true);
	}

	public function zero_selection_in_fact_test()
	{
		$filter_raters_real = new filter_raters_real_math(array(10000 => true), array(1 => true, 2 => true, 3 => true));
		$user_groups = $filter_raters_real->get_user_groups();
		$total_rater_count = $filter_raters_real->get_total_rater_count();
		$not_grouped_rater_count = $filter_raters_real->get_not_grouped_rater_count();

		$this->assert_identical($user_groups, array(
			1 => array("title" => "TEST 1", "user_count" => 1),
			2 => array("title" => "TEST 2", "user_count" => 2),
			3 => array("title" => "TEST 3", "user_count" => 1),
		));
		$this->assert_identical($total_rater_count, 3);
		$this->assert_identical($not_grouped_rater_count, 0);
		$this->assert_identical($filter_raters_real->is_select_all_checked(), false);
	}

	public function no_raters_test()
	{
		$filter_raters_real = new filter_raters_real_math(array(1 => true), array());
		$user_groups = $filter_raters_real->get_user_groups();
		$total_rater_count = $filter_raters_real->get_total_rater_count();
		$not_grouped_rater_count = $filter_raters_real->get_not_grouped_rater_count();

		$this->assert_identical($user_groups, array());
		$this->assert_identical($total_rater_count, 0);
		$this->assert_identical($not_grouped_rater_count, 0);
		$this->assert_identical($filter_raters_real->is_select_all_checked(), true);
	}

	public function not_grouped_test()
	{
		$filter_raters_real = new filter_raters_real_math(array(1 => true), array(1 => true, 3 => true, 4 => true));
		$user_groups = $filter_raters_real->get_user_groups();
		$total_rater_count = $filter_raters_real->get_total_rater_count();
		$not_grouped_rater_count = $filter_raters_real->get_not_grouped_rater_count();

		$this->assert_identical($user_groups, array(
			1 => array("title" => "TEST 1", "user_count" => 1, "is_selected" => true),
			2 => array("title" => "TEST 2", "user_count" => 1),
			3 => array("title" => "TEST 3", "user_count" => 1),
		));
		$this->assert_identical($total_rater_count, 3);
		$this->assert_identical($not_grouped_rater_count, 1);
		$this->assert_identical($filter_raters_real->is_select_all_checked(), false);
	}

	public function not_grouped_2_test()
	{
		$filter_raters_real = new filter_raters_real_math(array(1 => true), array(4 => true));
		$user_groups = $filter_raters_real->get_user_groups();
		$total_rater_count = $filter_raters_real->get_total_rater_count();
		$not_grouped_rater_count = $filter_raters_real->get_not_grouped_rater_count();

		$this->assert_identical($user_groups, array());
		$this->assert_identical($total_rater_count, 1);
		$this->assert_identical($not_grouped_rater_count, 1);
		$this->assert_identical($filter_raters_real->is_select_all_checked(), false);
	}

}

?>