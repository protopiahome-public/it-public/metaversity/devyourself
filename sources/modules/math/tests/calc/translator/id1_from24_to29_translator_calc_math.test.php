<?php

class id1_from24_to29_translator_calc_math_test extends base_test
{

	public function set_up()
	{
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (1, 'user1')");
		}
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 2"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (2, 'user2')");
		}
		$this->db->sql("REPLACE INTO project (id, name, title) VALUES (1, 'pr1', 'TEST PROJECT 1')");
		$this->db->sql("REPLACE INTO precedent_group (id, project_id, title) VALUES (1, 1, 'TEST PRECEDENT_GROUP 1')");
		$this->db->sql("REPLACE INTO precedent (id, project_id, title, precedent_group_id, descr, last_editor_user_id, adder_user_id) VALUES (1, 1, 'EV', 1, '', 1, 1)");
		$this->db->sql("REPLACE INTO competence_set (id, title) VALUES (24, 'TEST SET 24')");
		$this->db->sql("REPLACE INTO competence_set (id, title) VALUES (29, 'TEST SET 29')");
		$this->db->sql("
			REPLACE INTO competence_full (id, title, competence_set_id) VALUES
			(808, '', 24), (809, '', 24), (810, '', 24), (811, '', 24), (812, '', 24),
			(813, '', 24), (814, '', 24), (818, '', 24), (820, '', 24), (823, '', 24),
			(833, '', 24), (836, '', 24), (840, '', 24), (841, '', 24), (842, '', 24),
			(851, '', 24), (853, '', 24), (856, '', 24), (943, '', 24), (944, '', 24),
			(945, '', 24), (946, '', 24),
			(958, '', 29), (959, '', 29), (960, '', 29), (961, '', 29), (962, '', 29),
			(963, '', 29), (964, '', 29), (965, '', 29), (966, '', 29)
		");
		$this->db->sql("
			REPLACE INTO competence_calc (competence_id, title, competence_set_id) VALUES
			(808, '', 24), (809, '', 24), (810, '', 24), (811, '', 24), (812, '', 24),
			(813, '', 24), (814, '', 24), (818, '', 24), (820, '', 24), (823, '', 24),
			(833, '', 24), (836, '', 24), (840, '', 24), (841, '', 24), (842, '', 24),
			(851, '', 24), (853, '', 24), (856, '', 24), (943, '', 24), (944, '', 24),
			(945, '', 24), (946, '', 24),
			(958, '', 29), (959, '', 29), (960, '', 29), (961, '', 29), (962, '', 29),
			(963, '', 29), (964, '', 29), (965, '', 29), (966, '', 29)
		");
		// ratee_user_id, rater_user_id, competence_id, value
		$marks = array(
			array(1, 1, 809, 1),
			array(1, 2, 944, 3),
			array(1, 1, 841, 2),
			array(2, 1, 841, 1),
		);
		foreach ($marks as $data)
		{
			$this->db->sql("
				INSERT INTO precedent_flash_group (precedent_id)
				VALUES (1)
			");
			$precedent_flash_group_id = $this->db->get_last_id();
			$this->db->sql("
				INSERT INTO precedent_flash_group_user_link (user_id, precedent_flash_group_id)
				VALUES ({$data[0]}, {$precedent_flash_group_id})
			");
			$precedent_flash_group_user_link_id = $this->db->get_last_id();
			$this->db->sql("
				INSERT INTO mark_calc (project_id, precedent_group_id, precedent_flash_group_id, precedent_flash_group_user_link_id, ratee_user_id, rater_user_id, adder_user_id, competence_id, competence_set_id, type, value, comment, precedent_id)
				VALUES (1, 1, {$precedent_flash_group_id}, {$precedent_flash_group_user_link_id}, {$data[0]}, {$data[1]}, {$data[1]}, {$data[2]}, 24, 'personal', {$data[3]}, '', 1)
			");
		}
	}

	public function one_user_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					960 => 1,
					961 => 2,
				)
			)
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id1_from24_to29_translator_calc_math($results, $used_mark_count, $stat, true /* do not care */, 29, array(1), "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($used_mark_count, 3);
		$this->assert_identical($stat, array());
	}

	public function all_users_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					960 => 1,
					961 => 2,
				)
			),
			2 => array(
				"key" => array(
					961 => 1,
				)
			)
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id1_from24_to29_translator_calc_math($results, $used_mark_count, $stat, true /* do not care */, 29, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($used_rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($used_mark_count, 4);
		$this->assert_identical($stat, array());
	}

	public function all_users_invalid_competence_restriction_test()
	{
		unset($_GET);

		$results_correct = array(
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id1_from24_to29_translator_calc_math($results, $used_mark_count, $stat, true /* do not care */, 29, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(9999999999 => true));
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array());
		$this->assert_identical($used_mark_count, 0);
		$this->assert_identical($stat, array());
	}

	public function all_users_with_rater_restriction_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					961 => 2,
				)
			),
			2 => array(
				"key" => array(
					961 => 1,
				)
			)
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id1_from24_to29_translator_calc_math($results, $used_mark_count, $stat, true /* do not care */, 29, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(), array(1 => true));
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($used_rater_user_id_array_as_keys, array(1 => true));
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($used_mark_count, 3);
		$this->assert_identical($stat, array());
	}

	public function all_users_competence_restriction_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					961 => 2,
				)
			),
			2 => array(
				"key" => array(
					961 => 1,
				)
			)
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id1_from24_to29_translator_calc_math($results, $used_mark_count, $stat, true /* do not care */, 29, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(961), array());
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($used_rater_user_id_array_as_keys, array(1 => true));
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true));
		$this->assert_identical($used_mark_count, 2);
		$this->assert_identical($stat, array());
	}

	public function check_test()
	{
		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id1_from24_to29_translator_calc_math($results, $used_mark_count, $stat, true /* do not care */, 29, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(961), array());
		$this->assert_identical($calc->check_trans_data(), 0);

		$this->db->sql("DELETE FROM mark_calc WHERE competence_id = 944");
		$this->db->sql("DELETE FROM competence_calc WHERE competence_id = 944");
		$calc = new id1_from24_to29_translator_calc_math($results, $used_mark_count, $stat, true /* do not care */, 29, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(961), array());
		$this->assert_identical($calc->check_trans_data(), -3);
	}

}

?>