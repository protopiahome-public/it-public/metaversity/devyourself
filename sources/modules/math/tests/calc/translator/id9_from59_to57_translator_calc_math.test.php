<?php

class id9_from59_to57_translator_calc_math_test extends base_test
{

	public function set_up()
	{
		
	}

	public function check_test()
	{
		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id9_from59_to57_translator_calc_math($results, $used_mark_count, $stat, false, 57, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(), array());
		$this->assert_identical($calc->check_trans_data(), 0);
	}

}

?>