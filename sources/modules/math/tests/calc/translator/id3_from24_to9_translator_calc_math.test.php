<?php

class id3_from24_to9_translator_calc_math_test extends base_test
{

	public function set_up()
	{
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (1, 'user1')");
		}
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 2"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (2, 'user2')");
		}
		$this->db->sql("REPLACE INTO project (id, name, title) VALUES (1, 'pr1', 'TEST PROJECT 1')");
		$this->db->sql("REPLACE INTO precedent_group (id, project_id, title) VALUES (1, 1, 'TEST PRECEDENT_GROUP 1')");
		$this->db->sql("REPLACE INTO precedent (id, project_id, title, precedent_group_id, descr, last_editor_user_id, adder_user_id) VALUES (1, 1, 'EV', 1, '', 1, 1)");
		$this->db->sql("REPLACE INTO competence_set (id, title) VALUES (24, 'TEST SET 24')");
		$this->db->sql("REPLACE INTO competence_set (id, title) VALUES (9, 'TEST SET 9')");
		$this->db->sql("
			REPLACE INTO competence_full (id, title, competence_set_id) VALUES
			(808, '', 24), (809, '', 24), (816, '', 24), (826, '', 24), (827, '', 24),
			(828, '', 24), (829, '', 24), (830, '', 24), (833, '', 24), (834, '', 24),
			(839, '', 24), (841, '', 24), (843, '', 24),
			(299, '', 9), (300, '', 9), (301, '', 9), (316, '', 9), (367, '', 9),
			(369, '', 9), (370, '', 9), (371, '', 9)
		");
		$this->db->sql("
			REPLACE INTO competence_calc (competence_id, title, competence_set_id) VALUES
			(808, '', 24), (809, '', 24), (816, '', 24), (826, '', 24), (827, '', 24),
			(828, '', 24), (829, '', 24), (830, '', 24), (833, '', 24), (834, '', 24),
			(839, '', 24), (841, '', 24), (843, '', 24),
			(299, '', 9), (300, '', 9), (301, '', 9), (316, '', 9), (367, '', 9),
			(369, '', 9), (370, '', 9), (371, '', 9)
		");
		// ratee_user_id, rater_user_id, competence_id, value
		$marks = array(
			array(1, 1, 808, 1),
			array(1, 2, 808, 3),
			array(1, 1, 809, 2),
			array(1, 1, 816, 3),
			array(1, 1, 830, 2),
			array(2, 1, 830, 1),
		);
		foreach ($marks as $data)
		{
			$this->db->sql("
				INSERT INTO precedent_flash_group (precedent_id)
				VALUES (1)
			");
			$precedent_flash_group_id = $this->db->get_last_id();
			$this->db->sql("
				INSERT INTO precedent_flash_group_user_link (user_id, precedent_flash_group_id)
				VALUES ({$data[0]}, {$precedent_flash_group_id})
			");
			$precedent_flash_group_user_link_id = $this->db->get_last_id();
			$this->db->sql("
				INSERT INTO mark_calc (project_id, precedent_group_id, precedent_flash_group_id, precedent_flash_group_user_link_id, ratee_user_id, rater_user_id, adder_user_id, competence_id, competence_set_id, type, value, comment, precedent_id)
				VALUES (1, 1, {$precedent_flash_group_id}, {$precedent_flash_group_user_link_id}, {$data[0]}, {$data[1]}, {$data[1]}, {$data[2]}, 24, 'personal', {$data[3]}, 'x', 1)
			");
		}
	}

	public function all_users_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					369 => 3,
					367 => 1,
				)
			),
			2 => array(
				"key" => array(
					369 => 0,
					367 => 1,
				)
			)
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id3_from24_to9_translator_calc_math($results, $used_mark_count, $stat, false, 9, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($used_rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($used_mark_count, 6);
		$this->assert_identical($stat, array());
	}

	public function one_user_stat_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					369 => 3,
					367 => 1,
				)
			),
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$marks_log = array("previous_project_key" => array("some_data"));
		$calc = new id3_from24_to9_translator_calc_math($results, $used_mark_count, $stat, true, 9, array(1), "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(), array(), true, $marks_log);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($used_rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($used_mark_count, 5);
		$this->assert_identical($stat, array(
			369 => array(
				"marks_p1" => 1,
				"marks_p2" => 1,
				"marks_p3" => 2,
				"marks_g1" => 0,
				"marks_g2" => 0,
				"marks_g3" => 0,
				"raters" => array(1 => true, 2 => true,),
				"precedents" => array(1 => true,),
				"stat_tags" => array("translator" => true),
			),
			367 => array(
				"marks_p1" => 0,
				"marks_p2" => 1,
				"marks_p3" => 0,
				"marks_g1" => 0,
				"marks_g2" => 0,
				"marks_g3" => 0,
				"raters" => array(1 => true),
				"precedents" => array(1 => true,),
				"stat_tags" => array("translator" => true),
			),
		));
		$this->assert_identical($marks_log, array(
			"previous_project_key" => array("some_data"),
			"key" => array(
				"project_id" => 1,
				"competence_set_id" => 9,
				"src_competence_set_id" => 24,
				"translator_id" => 3,
				// ratee_user_id = 1 is not included in $marks_log
				"competences" => array(
					369 => array(
						"translation_type" => "AVG",
						"src_competences" => array(
							816 => array("mark" => 3, "weight" => 3),
							808 => array("mark" => 3, "weight" => 3),
							830 => array("mark" => 2, "weight" => 3),
						),
						"marks" => array(
							array(
								"precedent_id" => 1,
								"competence_id" => 808,
								"rater_user_id" => 1,
								"type" => "personal",
								"value" => 1,
								"comment" => "x",
							),
							array(
								"precedent_id" => 1,
								"competence_id" => 808,
								"rater_user_id" => 2,
								"type" => "personal",
								"value" => 3,
								"comment" => "x",
							),
							array(
								"precedent_id" => 1,
								"competence_id" => 816,
								"rater_user_id" => 1,
								"type" => "personal",
								"value" => 3,
								"comment" => "x",
							),
							array(
								"precedent_id" => 1,
								"competence_id" => 830,
								"rater_user_id" => 1,
								"type" => "personal",
								"value" => 2,
								"comment" => "x",
							),
						),
					),
					367 => array(
						"translation_type" => "AVG",
						"src_competences" => array(
							830 => array("mark" => 2, "weight" => 3),
							829 => array("mark" => 0, "weight" => 3),
						),
						"marks" => array(
							array(
								"precedent_id" => 1,
								"competence_id" => 830,
								"rater_user_id" => 1,
								"type" => "personal",
								"value" => 2,
								"comment" => "x",
							),
						),
					),
				),
			),
		));
	}

	public function one_user_no_stat_test()
	{
		unset($_GET);
		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$marks_log = array("previous_project_key" => array("some_data"));
		$calc = new id3_from24_to9_translator_calc_math($results, $used_mark_count, $stat, true, 9, array(1), "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(), array(), false, $marks_log);
		$calc->run();
		$this->assert_identical($marks_log, array("previous_project_key" => array("some_data")));
	}

	public function user_restriction_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					369 => 3,
					367 => 1,
				)
			)
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id3_from24_to9_translator_calc_math($results, $used_mark_count, $stat, false, 9, array(1), "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($used_mark_count, 5);
		$this->assert_identical($stat, array());
	}

	public function user_restriction_incorrect_user_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					369 => 3,
					367 => 1,
				)
			)
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id3_from24_to9_translator_calc_math($results, $used_mark_count, $stat, false, 9, array(1, 99999), "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($used_mark_count, 5);
		$this->assert_identical($stat, array());
	}

	public function user_restriction_no_users_test()
	{
		unset($_GET);

		$results_correct = array(
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id3_from24_to9_translator_calc_math($results, $used_mark_count, $stat, false, 9, array(), "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array());
		$this->assert_identical($used_mark_count, 0);
		$this->assert_identical($stat, array());
	}

	public function user_restriction_no_users_incoorect_user_test()
	{
		unset($_GET);

		$results_correct = array(
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id3_from24_to9_translator_calc_math($results, $used_mark_count, $stat, false, 9, array(99999), "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array());
		$this->assert_identical($used_mark_count, 0);
		$this->assert_identical($stat, array());
	}

	public function all_users_invalid_competence_restriction_test()
	{
		unset($_GET);

		$results_correct = array(
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id3_from24_to9_translator_calc_math($results, $used_mark_count, $stat, false, 9, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(9999999999 => true));
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array());
		$this->assert_identical($used_mark_count, 0);
		$this->assert_identical($stat, array());
	}

	public function all_users_with_rater_restriction_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					369 => 2,
					367 => 1,
				)
			),
			2 => array(
				"key" => array(
					369 => 0,
					367 => 1,
				)
			)
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id3_from24_to9_translator_calc_math($results, $used_mark_count, $stat, false, 9, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(), array(1 => true));
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($used_rater_user_id_array_as_keys, array(1 => true));
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($used_mark_count, 5);
		$this->assert_identical($stat, array());
	}

	public function all_users_competence_restriction_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					369 => 3,
				)
			),
			2 => array(
				"key" => array(
					369 => 0,
				)
			)
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id3_from24_to9_translator_calc_math($results, $used_mark_count, $stat, false, 9, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(369), array());
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($used_rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($used_mark_count, 5);
		$this->assert_identical($stat, array());
	}

	public function check_test()
	{
		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new id3_from24_to9_translator_calc_math($results, $used_mark_count, $stat, false, 9, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(816), array());
		$this->assert_identical($calc->check_trans_data(), 0);

		$this->db->sql("DELETE FROM mark_calc WHERE competence_id = 816");
		$this->db->sql("DELETE FROM competence_calc WHERE competence_id = 816");
		$calc = new id3_from24_to9_translator_calc_math($results, $used_mark_count, $stat, false, 9, null, "key", 1, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(816), array());
		$this->assert_identical($calc->check_trans_data(), -3);
	}

}

?>