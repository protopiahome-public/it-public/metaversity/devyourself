<?php

class calc_helper_math_test extends base_test
{

	private $helper1_log = array();

	public function set_up()
	{
		$this->select_db("math");
	}

	public function call_for_every_user_and_competence_test()
	{
		$this->helper1_log = array();
		$db_result = $this->db->sql("SELECT * FROM mark_calc WHERE project_id = 2 ORDER BY ratee_user_id, competence_id");
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$marks_log = array();
		calc_helper_math::call_for_every_user_and_competence($db_result, array($this, "_callback1"), $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, $used_mark_count, $stat, true, "own", array(), true, $marks_log);
		$this->assert_identical($this->helper1_log, array(
			array(1, 101, array(
					array("type" => "personal", "value" => 2),
					array("type" => "personal", "value" => 1),
			)),
			array(1, 102, array(
					array("type" => "personal", "value" => 3),
			)),
			array(2, 102, array(
					array("type" => "personal", "value" => 3),
			)),
			array(2, 103, array(
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 2),
			)),
		));
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($used_mark_count, 6);
		$this->assert_identical($stat, array(
			101 => array(
				"marks_p1" => 1,
				"marks_p2" => 1,
				"marks_p3" => 0,
				"marks_g1" => 0,
				"marks_g2" => 0,
				"marks_g3" => 0,
				"raters" => array(1 => true, 2 => true,),
				"precedents" => array(201 => true,),
				"stat_tags" => array("own" => true),
			),
			102 => array(
				"marks_p1" => 0,
				"marks_p2" => 0,
				"marks_p3" => 2,
				"marks_g1" => 0,
				"marks_g2" => 0,
				"marks_g3" => 0,
				"raters" => array(2 => true, 1 => true,),
				"precedents" => array(201 => true,),
				"stat_tags" => array("own" => true),
			),
			103 => array(
				"marks_p1" => 1,
				"marks_p2" => 1,
				"marks_p3" => 0,
				"marks_g1" => 0,
				"marks_g2" => 0,
				"marks_g3" => 0,
				"raters" => array(1 => true,),
				"precedents" => array(201 => true,),
				"stat_tags" => array("own" => true),
			),
		));
		$this->assert_identical($marks_log, array(
			array(
				"precedent_id" => 201,
				"competence_id" => 101,
				"rater_user_id" => 1,
				"type" => "personal",
				"value" => 2,
				"comment" => "x",
			),
			array(
				"precedent_id" => 201,
				"competence_id" => 101,
				"rater_user_id" => 2,
				"type" => "personal",
				"value" => 1,
				"comment" => "x",
			),
			array(
				"precedent_id" => 201,
				"competence_id" => 102,
				"rater_user_id" => 2,
				"type" => "personal",
				"value" => 3,
				"comment" => "x",
			),
			array(
				"precedent_id" => 201,
				"competence_id" => 102,
				"rater_user_id" => 1,
				"type" => "personal",
				"value" => 3,
				"comment" => "x",
			),
			array(
				"precedent_id" => 201,
				"competence_id" => 103,
				"rater_user_id" => 1,
				"type" => "personal",
				"value" => 1,
				"comment" => "x",
			),
			array(
				"precedent_id" => 201,
				"competence_id" => 103,
				"rater_user_id" => 1,
				"type" => "personal",
				"value" => 2,
				"comment" => "x",
			),
		));
	}

	public function call_for_every_user_and_competence_NOMARKS_test()
	{
		$this->helper1_log = array();
		$db_result = $this->db->sql("SELECT * FROM mark_calc WHERE project_id = 2 ORDER BY ratee_user_id, competence_id");
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$marks_log = array();
		calc_helper_math::call_for_every_user_and_competence($db_result, array($this, "_callback1"), $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, $used_mark_count, $stat, true, "own", array(), false, $marks_log);
		$this->assert_identical($marks_log, array());
	}

	public function call_for_every_user_and_competence_STAT_UPDATE_test()
	{
		$this->helper1_log = array();
		$db_result = $this->db->sql("SELECT * FROM mark_calc WHERE project_id = 2 AND competence_id = 101 ORDER BY ratee_user_id, competence_id");
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array(
			101 => array(
				"marks_p1" => 0,
				"marks_p2" => 2,
				"marks_p3" => 5,
				"marks_g1" => 0,
				"marks_g2" => 2,
				"marks_g3" => 0,
				"raters" => array(1 => true, 3 => true,),
				"precedents" => array(999 => true,),
				"stat_tags" => array("translator" => true),
			)
		);
		calc_helper_math::call_for_every_user_and_competence($db_result, array($this, "_callback1"), $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, $used_mark_count, $stat, true, "own");
		$this->assert_identical($stat, array(
			101 => array(
				"marks_p1" => 1,
				"marks_p2" => 3,
				"marks_p3" => 5,
				"marks_g1" => 0,
				"marks_g2" => 2,
				"marks_g3" => 0,
				"raters" => array(1 => true, 3 => true, 2 => true,),
				"precedents" => array(999 => true, 201 => true,),
				"stat_tags" => array("translator" => true, "own" => true),
			),
		));
	}

	public function call_for_every_user_and_competence_rater_RESTRICTION_test()
	{
		$this->helper1_log = array();
		$db_result = $this->db->sql("SELECT * FROM mark_calc WHERE project_id = 2 ORDER BY ratee_user_id, competence_id");
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		calc_helper_math::call_for_every_user_and_competence($db_result, array($this, "_callback1"), $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, $used_mark_count, $stat, false, "", array(2 => true));
		$this->assert_identical($this->helper1_log, array(
			array(1, 101, array(
					array("type" => "personal", "value" => 1),
			)),
			array(1, 102, array(
					array("type" => "personal", "value" => 3),
			)),
		));
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($used_rater_user_id_array_as_keys, array(2 => true));
		$this->assert_identical($used_mark_count, 2);
		$this->assert_identical($stat, array());
	}

	public function call_for_every_user_and_competence_LIMIT_1_test()
	{
		$this->helper1_log = array();
		$db_result = $this->db->sql("SELECT * FROM mark_calc WHERE project_id = 2 ORDER BY ratee_user_id, competence_id LIMIT 1");
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		calc_helper_math::call_for_every_user_and_competence($db_result, array($this, "_callback1"), $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, $used_mark_count, $stat, false, "");
		$this->assert_identical($this->helper1_log, array(
			array(1, 101, array(
					array("type" => "personal", "value" => 2),
			)),
		));
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true));
	}

	public function call_for_every_user_and_competence_LIMIT_0_test()
	{
		$this->helper1_log = array();
		$db_result = $this->db->sql("SELECT * FROM mark_calc WHERE project_id = 2 ORDER BY ratee_user_id, competence_id LIMIT 0");
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		calc_helper_math::call_for_every_user_and_competence($db_result, array($this, "_callback1"), $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, $used_mark_count, $stat, false, "");
		$this->assert_identical($this->helper1_log, array());
		$this->assert_identical($rater_user_id_array_as_keys, array());
	}

	public function _callback1($ratee_user_id, $competence_id, $marks)
	{
		$this->helper1_log[] = array($ratee_user_id, $competence_id, $marks);
	}

	public function get_max_weighted_mark_test()
	{
		$result = calc_helper_math::get_max_weighted_mark(array(
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 2),
				));
		$this->assert_identical($result, 2);

		$result = calc_helper_math::get_max_weighted_mark(array(
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 3),
				));
		$this->assert_identical($result, 3);

		$result = calc_helper_math::get_max_weighted_mark(array(
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 3),
				));
		$this->assert_identical($result, 1);

		$result = calc_helper_math::get_max_weighted_mark(array(
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 2),
					array("type" => "personal", "value" => 2),
					array("type" => "personal", "value" => 3),
				));
		$this->assert_identical($result, 2);

		$result = calc_helper_math::get_max_weighted_mark(array(
					array("type" => "group", "value" => 1),
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 1),
					array("type" => "personal", "value" => 2),
					array("type" => "personal", "value" => 2),
					array("type" => "personal", "value" => 3),
				));
		$this->assert_identical($result, 1);

		$result = calc_helper_math::get_max_weighted_mark(array(
					array("type" => "group", "value" => 1),
					array("type" => "group", "value" => 1),
					array("type" => "group", "value" => 2),
				));
		$this->assert_identical($result, 2);

		$result = calc_helper_math::get_max_weighted_mark(array(
					array("type" => "group", "value" => 1),
					array("type" => "group", "value" => 1),
					array("type" => "group", "value" => 1),
					array("type" => "group", "value" => 1),
					array("type" => "group", "value" => 1),
					array("type" => "personal", "value" => 3),
				));
		$this->assert_identical($result, 3);
	}

}

?>