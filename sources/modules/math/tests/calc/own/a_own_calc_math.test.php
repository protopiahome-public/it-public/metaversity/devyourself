<?php

class a_own_calc_math_test extends base_test
{

	public function set_up()
	{
		$this->select_db("math");
	}

	public function one_user_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					101 => 2,
					102 => 3,
				)
			)
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$marks_log = array("previous_project_key" => array("some_data"));
		$calc = new a_own_calc_math($results, $used_mark_count, $stat, true, 1, array(1), "key", 2, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(), array(), true, $marks_log);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
		$this->assert_identical($used_mark_count, 3);
		$this->assert_identical($stat, array(
			101 => array(
				"marks_p1" => 1,
				"marks_p2" => 1,
				"marks_p3" => 0,
				"marks_g1" => 0,
				"marks_g2" => 0,
				"marks_g3" => 0,
				"raters" => array(1 => true, 2 => true,),
				"precedents" => array(201 => true,),
				"stat_tags" => array("own" => true),
			),
			102 => array(
				"marks_p1" => 0,
				"marks_p2" => 0,
				"marks_p3" => 1,
				"marks_g1" => 0,
				"marks_g2" => 0,
				"marks_g3" => 0,
				"raters" => array(2 => true,),
				"precedents" => array(201 => true,),
				"stat_tags" => array("own" => true),
			),
		));
		$this->assert_identical($marks_log, array(
			"previous_project_key" => array("some_data"),
			"key" => array(
				"project_id" => 2,
				"competence_set_id" => 1,
				"competences" => array(// ratee_user_id = 1 is not included in $marks_log
					101 => array(
						"marks" => array(
							array(
								"precedent_id" => 201,
								"competence_id" => 101,
								"rater_user_id" => 1,
								"type" => "personal",
								"value" => 2,
								"comment" => "x",
							),
							array(
								"precedent_id" => 201,
								"competence_id" => 101,
								"rater_user_id" => 2,
								"type" => "personal",
								"value" => 1,
								"comment" => "x",
							),
						),
					),
					102 => array(
						"marks" => array(
							array(
								"precedent_id" => 201,
								"competence_id" => 102,
								"rater_user_id" => 2,
								"type" => "personal",
								"value" => 3,
								"comment" => "x",
							),
						),
					),
				),
			),
		));
	}

	public function one_user_no_stat_test()
	{
		unset($_GET);
		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$marks_log = array("previous_project_key" => array("some_data"));
		$calc = new a_own_calc_math($results, $used_mark_count, $stat, true, 1, array(1), "key", 2, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(), array(), false, $marks_log);
		$calc->run();
		$this->assert_identical($marks_log, array("previous_project_key" => array("some_data")));
	}

	public function all_users_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					101 => 2,
					102 => 3,
				)
			),
			2 => array(
				"key" => array(
					102 => 3,
					103 => 2,
				)
			),
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new a_own_calc_math($results, $used_mark_count, $stat, false, 1, null, "key", 2, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
	}

	public function user_restriction_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					101 => 2,
					102 => 3,
				)
			),
			2 => array(
				"key" => array(
					102 => 3,
					103 => 2,
				)
			),
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new a_own_calc_math($results, $used_mark_count, $stat, false, 1, array(1, 2), "key", 2, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
	}

	public function user_restriction_incorrect_user_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					101 => 2,
					102 => 3,
				)
			),
			2 => array(
				"key" => array(
					102 => 3,
					103 => 2,
				)
			),
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new a_own_calc_math($results, $used_mark_count, $stat, false, 1, array(1, 2, 99999), "key", 2, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
	}

	public function user_restriction_no_users_test()
	{
		unset($_GET);

		$results_correct = array(
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new a_own_calc_math($results, $used_mark_count, $stat, false, 1, array(), "key", 2, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array());
	}

	public function user_restriction_no_users_incorrect_user_test()
	{
		unset($_GET);

		$results_correct = array(
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new a_own_calc_math($results, $used_mark_count, $stat, false, 1, array(99999), "key", 2, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys);
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array());
	}

	public function all_users_with_rater_restriction_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					101 => 1,
					102 => 3,
				)
			),
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new a_own_calc_math($results, $used_mark_count, $stat, false, 1, null, "key", 2, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(), array(2 => true));
		$calc->run();
		$this->assert_identical($results, $results_correct);
	}

	public function all_users_competence_restriction_1_test()
	{
		unset($_GET);

		$results_correct = array(
			1 => array(
				"key" => array(
					101 => 2,
					102 => 3,
				)
			),
			2 => array(
				"key" => array(
					102 => 3,
				)
			),
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new a_own_calc_math($results, $used_mark_count, $stat, false, 1, null, "key", 2, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(101, 102));
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true, 2 => true));
	}

	public function all_users_competence_restriction_2_test()
	{
		unset($_GET);

		$results_correct = array(
			2 => array(
				"key" => array(
					103 => 2,
				)
			),
		);

		$results = array();
		$rater_user_id_array_as_keys = array();
		$used_rater_user_id_array_as_keys = array();
		$used_mark_count = 0;
		$stat = array();
		$calc = new a_own_calc_math($results, $used_mark_count, $stat, false, 1, null, "key", 2, $rater_user_id_array_as_keys, $used_rater_user_id_array_as_keys, array(103));
		$calc->run();
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($rater_user_id_array_as_keys, array(1 => true));
	}

}

?>