<?php

class vector_info_math_test extends base_test
{

	public function set_up()
	{
		$this->select_db("math");
	}

	public function tear_down()
	{
		unset($_GET);
	}

	public function get_project_vector_config_test()
	{
		global $config;

		$vector_config_correct = array(
			"project_id" => 1,
			"vector_competence_set_id" => 1,
			"vector_self_must_skip" => false,
			"vector_self_can_skip" => false,
			"vector_self_can_skip_if_results" => true,
			"vector_focus_can_skip" => false,
			"vector_focus_min_competence_count" => 1,
			"vector_focus_max_competence_count" => 4,
		);

		// Existed project
		$vector_config = vector_info_math::get_project_vector_config(1);
		$this->assert_identical($vector_config, $vector_config_correct);

		// Unset competence_set_id
		$this->db->test_begin();
		$this->db->begin();
		$this->db->sql("CREATE TEMPORARY TABLE x SELECT * FROM project WHERE id = 1");
		$this->db->sql("UPDATE x SET id = 100000000, vector_competence_set_id = NULL WHERE id = 1");
		$this->db->sql("INSERT INTO project SELECT * FROM x");
		$config["default_vector_competence_set_id"] = 999123;
		$vector_config = vector_info_math::get_project_vector_config(100000000);
		$vector_config_correct["project_id"] = 100000000;
		$vector_config_correct["vector_competence_set_id"] = 999123;
		$this->assert_identical($vector_config, $vector_config_correct);
		$this->db->test_end();

		// Unexisted project
		$vector_config = vector_info_math::get_project_vector_config(999);
		$this->assert_identical($vector_config, false);

		// Cache test
		$a = vector_info_math::get_project_vector_config(2);
		$b = vector_info_math::get_project_vector_config(2);
		$this->assert_identical($a, $b);
	}

	public function get_project_vector_info_test()
	{
		global $config;
		$config["default_vector_competence_set_id"] = 1;
		vector_info_math::$project_vector_config_cache = array(); // cleaning cache

		$vector_info_struct_correct = array(
			"id" => 1,
			"foreign" => false,
			"user_id" => 1,
			"project_id" => 1,
			"competence_set_id" => 1,
			"future_professiogram_count" => 0,
			"focus_competence_count" => 0,
			"calculations_base" => "results",
			"add_time" => "2010-00-00 00:00:00",
			"edit_time" => "2010-00-00 00:00:00",
			"future_edit_time" => "2010-00-00 00:00:01",
			"self_edit_time" => "0000-00-00 00:00:00",
			"focus_edit_time" => "0000-00-00 00:00:00",
		);

		$this->db->test_begin();
		$this->db->test_set_allow_queries_beyond_transaction();

		// Existed vector
		$vector_info_struct = vector_info_math::get_project_vector_info(1, 1);
		$this->assert_identical($vector_info_struct, $vector_info_struct_correct);

		// There is no vector for the default project competence set (id = 2)
		$vector_info_struct = vector_info_math::get_project_vector_info(2, 2);
		$this->assert_identical($vector_info_struct, false);

		// Foreign vector is allowed
		$vector_info_struct = vector_info_math::get_project_vector_info(2, 2, false, true);
		$this->assert_identical($vector_info_struct["id"], 3);

		// Competenfce set is not set for a project - the default one is taken
		// (the 3rd parameter)
		$vector_info_struct = vector_info_math::get_project_vector_info(1, 3);
		$this->assert_false($vector_info_struct);

		// Unexisted vector
		$vector_info_struct = vector_info_math::get_project_vector_info(999, 999);
		$this->assert_identical($vector_info_struct, false);

		// Previous requests shouldn't lead to commits
		$this->assert_identical($this->db->test_is_commit_performed(), false);
		$this->assert_identical($this->db->test_is_transaction_finished(), true);
		$this->db->test_end();

		// Vector creation
		$sql = "SELECT * FROM vector WHERE project_id = 2 AND user_id = 3 AND competence_set_id = 2";
		$this->db->test_begin();
		$this->db->test_set_allow_queries_beyond_transaction();
		$this->assert_db_row_not_exist($sql);
		$vector_info_struct = vector_info_math::get_project_vector_info(3, 2, true);
		$this->assert_identical($this->db->test_is_commit_performed(), true);
		$this->assert_db_row_exists($sql);
		$this->db->test_end();
	}

	public function get_vectors_for_user_test()
	{
		$vectors_real = vector_info_math::get_project_vectors_for_user($user_id = 200, $project_id_array = array(2 => 2, 3 => 1, 4 => 1));
		$this->assert_identical($vectors_real, array(
			0 => array(
				"id" => 202,
				"user_id" => 200,
				"project_id" => 3,
				"competence_set_id" => 1,
			),
			1 => array(
				"id" => 203,
				"user_id" => 200,
				"project_id" => 2,
				"competence_set_id" => 2,
			),
		));
	}

	public function get_project_vectors_for_project_test()
	{
		$vectors_real = vector_info_math::get_project_vectors_for_project($project_id = 2, $vector_competence_set_id = 2, $user_id_array = array(200));
		$this->assert_identical($vectors_real, array(
			0 => array(
				"id" => 203,
				"user_id" => 200,
				"project_id" => 2,
				"competence_set_id" => 2,
			),
		));
	}

}

?>