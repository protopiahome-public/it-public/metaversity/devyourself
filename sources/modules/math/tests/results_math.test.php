<?php

class results_math_test extends base_test
{

	public function set_up()
	{
		$this->select_db("math");
		$this->db->sql("TRUNCATE competence_translator");
		$this->db->sql("INSERT INTO competence_translator (id, from_competence_set_id, to_competence_set_id, title) VALUES (1000000001, 2, 1, 'TRANS')");
	}

	public function basic_test()
	{
		$results_data_correct = array(
			1 => array(
				"project_1_1" => array(
					101 => 3,
					102 => 3,
					103 => 2,
					104 => 1,
				),
				"project_1_2" => array(
					101 => 2,
					102 => 3,
				),
				"sum" => array(
					101 => 3,
					102 => 3,
					103 => 2,
					104 => 1,
				),
			),
			2 => array(
				"project_1_1" => array(
					101 => 2,
					102 => 2,
					103 => 1,
					104 => 1,
					105 => 3,
				),
				"project_1_2" => array(
					102 => 3,
					103 => 2,
				),
				"sum" => array(
					101 => 2,
					102 => 3,
					103 => 2,
					104 => 1,
					105 => 3,
				),
			),
		);

		$results = new results_math(1);
		$results_data_real = $results->get_results();
		$this->assert_identical($results_data_real, $results_data_correct);
	}

	public function basic_competence_restricion_test()
	{
		$results_data_correct = array(
			1 => array(
				"project_1_1" => array(
					101 => 3,
					103 => 2,
				),
				"project_1_2" => array(
					101 => 2,
				),
				"sum" => array(
					101 => 3,
					103 => 2,
				),
			),
			2 => array(
				"project_1_1" => array(
					101 => 2,
					103 => 1,
				),
				"project_1_2" => array(
					103 => 2,
				),
				"sum" => array(
					101 => 2,
					103 => 2,
				),
			),
		);

		$results = new results_math(1, null, array(101, 103, 999999));
		$results_data_real = $results->get_results();
		$this->assert_identical($results_data_real, $results_data_correct);
	}

	public function one_user_test()
	{
		$results_data_correct = array(
			2 => array(
				"project_1_1" => array(
					101 => 2,
					102 => 2,
					103 => 1,
					104 => 1,
					105 => 3,
				),
				"project_1_2" => array(
					102 => 3,
					103 => 2,
				),
				"sum" => array(
					101 => 2,
					102 => 3,
					103 => 2,
					104 => 1,
					105 => 3,
				),
			),
		);

		// one user
		$results = new results_math(1, array(2));
		$results_data_real = $results->get_results();
		$this->assert_identical($results_data_real, $results_data_correct);

		// one user + incorrect user
		$results = new results_math(1, array(2, 99999));
		$results_data_real = $results->get_results();
		$this->assert_identical($results_data_real, $results_data_correct);
	}

	public function user_restriction_test()
	{
		$results_data_correct = array(
			1 => array(
				"project_1_1" => array(
					101 => 3,
					102 => 3,
					103 => 2,
					104 => 1,
				),
				"project_1_2" => array(
					101 => 2,
					102 => 3,
				),
				"sum" => array(
					101 => 3,
					102 => 3,
					103 => 2,
					104 => 1,
				),
			),
			2 => array(
				"project_1_1" => array(
					101 => 2,
					102 => 2,
					103 => 1,
					104 => 1,
					105 => 3,
				),
				"project_1_2" => array(
					102 => 3,
					103 => 2,
				),
				"sum" => array(
					101 => 2,
					102 => 3,
					103 => 2,
					104 => 1,
					105 => 3,
				),
			),
		);

		$results = new results_math(1, array(1, 2));
		$results_data_real = $results->get_results();
		$this->assert_identical($results_data_real, $results_data_correct);
	}

	public function user_restriction_no_users_test()
	{
		$results_data_correct = array(
		);

		// one user
		$results = new results_math(1, array());
		$results_data_real = $results->get_results();
		$this->assert_identical($results_data_real, $results_data_correct);

		// one user + incorrect user
		$results = new results_math(1, array(99999));
		$results_data_real = $results->get_results();
		$this->assert_identical($results_data_real, $results_data_correct);
	}

	public function one_user_competence_restricion_test()
	{
		$results_data_correct = array(
			2 => array(
				"project_1_1" => array(
					104 => 1,
				),
				"sum" => array(
					104 => 1,
				),
			),
		);

		$results = new results_math(1, array(2), array(104));
		$results_data_real = $results->get_results();
		$this->assert_identical($results_data_real, $results_data_correct);
	}

	public function basic_GET_test()
	{
		unset($_GET);
		$_GET["project_1_1"] = "1";

		$results_data_correct = array(
			1 => array(
				"project_1_1" => array(
					101 => 3,
					102 => 3,
					103 => 2,
					104 => 1,
				),
				"sum" => array(
					101 => 3,
					102 => 3,
					103 => 2,
					104 => 1,
				),
			),
			2 => array(
				"project_1_1" => array(
					101 => 2,
					102 => 2,
					103 => 1,
					104 => 1,
					105 => 3,
				),
				"sum" => array(
					101 => 2,
					102 => 2,
					103 => 1,
					104 => 1,
					105 => 3,
				),
			),
		);

		$results = new results_math(1);
		$results_data_real = $results->get_results();
		$this->assert_identical($results_data_real, $results_data_correct);
	}

	public function one_user_GET_test()
	{
		unset($_GET);
		$_GET["project_1_1"] = "1";

		$results_data_correct = array(
			2 => array(
				"project_1_1" => array(
					101 => 2,
					102 => 2,
					103 => 1,
					104 => 1,
					105 => 3,
				),
				"sum" => array(
					101 => 2,
					102 => 2,
					103 => 1,
					104 => 1,
					105 => 3,
				),
			),
		);

		$results = new results_math(1, array(2));
		$results_data_real = $results->get_results();
		$this->assert_identical($results_data_real, $results_data_correct);
	}

	public function one_user_raters_check_1_test()
	{
		unset($_GET);

		$results_data_correct = array(
			2 => array(
				"project_1_1" => array(
					101 => 2,
					102 => 2,
					103 => 1,
					104 => 1,
					105 => 3,
				),
				"project_1_2" => array(
					102 => 3,
					103 => 2,
				),
				"sum" => array(
					101 => 2,
					102 => 3,
					103 => 2,
					104 => 1,
					105 => 3,
				),
			),
		);

		$results = new results_math(1, array(2), array(), false, true);
		$results_data_real = $results->get_results();
		$user_groups_real = $results->get_filter_raters_real()->get_user_groups();
		$total_rater_count_real = $results->get_filter_raters_real()->get_total_rater_count();
		$not_grouped_rater_count_real = $results->get_filter_raters_real()->get_not_grouped_rater_count();
		$this->assert_identical($results_data_real, $results_data_correct);
		$this->assert_identical($user_groups_real, array(
			1 => array("title" => "G1", "user_count" => 3, "is_selected" => true),
			2 => array("title" => "G2", "user_count" => 2, "is_selected" => true),
			3 => array("title" => "G3", "user_count" => 2, "is_selected" => true),
			4 => array("title" => "G4", "user_count" => 1, "is_selected" => true),
		));
		$this->assert_identical($total_rater_count_real, 4);
		$this->assert_identical($not_grouped_rater_count_real, 1);
		$this->assert_identical($results->get_used_mark_count(), 18);
		$stat = $results->get_stat();
		$this->assert_identical(isset($stat[101]) ? $stat[101] : array(), array(
			"marks_p1" => 0,
			"marks_p2" => 3,
			"marks_p3" => 0,
			"marks_g1" => 0,
			"marks_g2" => 0,
			"marks_g3" => 3,
			"raters" => 4,
			"precedents" => 3,
			"stat_type_own" => true,
		));
	}

	public function basic_raters_check_2_test()
	{
		unset($_GET);
		$_GET["project_1_2"] = "1";
		$_GET["rater_user_groups_all"] = "1";

		$results_data_correct = array(
			1 => array(
				"project_1_2" => array(
					101 => 2,
					102 => 3,
				),
				"sum" => array(
					101 => 2,
					102 => 3,
				),
			),
			2 => array(
				"project_1_2" => array(
					102 => 3,
					103 => 2,
				),
				"sum" => array(
					102 => 3,
					103 => 2,
				),
			),
		);

		$results = new results_math(1, null, array());
		$results_data_real = $results->get_results();
		$user_groups_real = $results->get_filter_raters_real()->get_user_groups();
		$total_rater_count_real = $results->get_filter_raters_real()->get_total_rater_count();
		$not_grouped_rater_count_real = $results->get_filter_raters_real()->get_not_grouped_rater_count();
		$this->assert_identical($results_data_real, $results_data_correct);
		$this->assert_identical($user_groups_real, array(
			1 => array("title" => "G1", "user_count" => 2, "is_selected" => true),
			2 => array("title" => "G2", "user_count" => 2, "is_selected" => true),
			3 => array("title" => "G3", "user_count" => 1, "is_selected" => true),
		));
		$this->assert_identical($total_rater_count_real, 2);
		$this->assert_identical($not_grouped_rater_count_real, 0);
	}

	public function basic_raters_restriction_test()
	{
		unset($_GET);
		$_GET["project_1_2"] = "1";
		$_GET["rater_user_groups"] = array("3", "4");

		$results_data_correct = array(
			1 => array(
				"project_1_2" => array(
					101 => 1,
					102 => 3,
				),
				"sum" => array(
					101 => 1,
					102 => 3,
				),
			),
		);

		$results = new results_math(1, null, array());
		$results_data_real = $results->get_results();
		$user_groups_real = $results->get_filter_raters_real()->get_user_groups();
		$total_rater_count_real = $results->get_filter_raters_real()->get_total_rater_count();
		$not_grouped_rater_count_real = $results->get_filter_raters_real()->get_not_grouped_rater_count();
		$this->assert_identical($results_data_real, $results_data_correct);
		$this->assert_identical($user_groups_real, array(
			1 => array("title" => "G1", "user_count" => 2),
			2 => array("title" => "G2", "user_count" => 2),
			3 => array("title" => "G3", "user_count" => 1, "is_selected" => true),
		));
		$this->assert_identical($total_rater_count_real, 2);
		$this->assert_identical($not_grouped_rater_count_real, 0);
	}

	public function translator_test()
	{
		unset($_GET);
		$_GET["translator_2"] = "1000000001";
		$_GET["project_1_1"] = "1";
		$_GET["project_1_2"] = "1";
		$_GET["project_2_1"] = "1";

		$results_data_correct = array(
			1 => array(
				"project_1_1" => array(
					101 => 3,
					102 => 3,
					103 => 2,
					104 => 1,
				),
				"project_1_2" => array(
					101 => 2,
					102 => 3,
				),
				"project_2_1" => array(
					101 => 3,
					102 => 2,
				),
				"sum" => array(
					101 => 3,
					102 => 3,
					103 => 2,
					104 => 1,
				),
			),
			2 => array(
				"project_1_1" => array(
					101 => 2,
					102 => 2,
					103 => 1,
					104 => 1,
					105 => 3,
				),
				"project_1_2" => array(
					102 => 3,
					103 => 2,
				),
				"project_2_1" => array(
					101 => 3,
				),
				"sum" => array(
					101 => 3,
					102 => 3,
					103 => 2,
					104 => 1,
					105 => 3,
				),
			),
		);

		$results = new results_math(1);
		$results_data_real = $results->get_results();
		$this->assert_identical($results_data_real, $results_data_correct);
		$this->assert_identical($results->get_used_mark_count(), 43);
		$this->assert_identical($results->get_stat(), array()); // no stat should be calculated
	}

	public function stat_test()
	{
		unset($_GET);
		$_GET["project_1_2"] = "1";

		$results_data_correct = array(
			2 => array(
				"project_1_2" => array(
					102 => 3,
					103 => 2,
				),
				"sum" => array(
					102 => 3,
					103 => 2,
				),
			),
		);

		$results = new results_math(1, array(2), array(), false, true, true);
		$results_data_real = $results->get_results();
		$user_groups_real = $results->get_filter_raters_real()->get_user_groups();
		$total_rater_count_real = $results->get_filter_raters_real()->get_total_rater_count();
		$not_grouped_rater_count_real = $results->get_filter_raters_real()->get_not_grouped_rater_count();
		$this->assert_identical($results_data_real, $results_data_correct);
		$this->assert_identical($results->get_marks_log(), array(
			"project_1_2" => array(
				"project_id" => 2,
				"competence_set_id" => 1,
				"competences" => array(// ratee_user_id = 1 is not included in $marks_log
					102 => array(
						"marks" => array(
							array(
								"precedent_id" => 201,
								"competence_id" => 102,
								"rater_user_id" => 1,
								"type" => "personal",
								"value" => 3,
								"comment" => "x",
							),
						),
					),
					103 => array(
						"marks" => array(
							array(
								"precedent_id" => 201,
								"competence_id" => 103,
								"rater_user_id" => 1,
								"type" => "personal",
								"value" => 1,
								"comment" => "x",
							),
							array(
								"precedent_id" => 201,
								"competence_id" => 103,
								"rater_user_id" => 1,
								"type" => "personal",
								"value" => 2,
								"comment" => "x",
							),
						),
					),
				),
			)
		));
	}

	public function no_stat_test()
	{
		unset($_GET);
		$_GET["project_1_2"] = "1";

		$results = new results_math(1, array(2), array(), false, true, true);
		$this->assert_true($results->get_stat() != array());
		$this->assert_true($results->get_marks_log() != array());

		$results = new results_math(1, array(2), array(), false, false, true);
		$this->assert_false($results->get_stat() != array());
		$this->assert_true($results->get_marks_log() != array());

		$results = new results_math(1, array(2), array(), false, true, false);
		$this->assert_true($results->get_stat() != array());
		$this->assert_false($results->get_marks_log() != array());

		$results = new results_math(1, array(2), array(), false, false, false);
		$this->assert_false($results->get_stat() != array());
		$this->assert_false($results->get_marks_log() != array());
	}

}

?>