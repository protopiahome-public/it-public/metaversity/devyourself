<?php

class vectors_data_math_test extends base_test
{

	public function set_up()
	{
		$this->select_db("math");
	}

	public function tear_down()
	{
		unset($_GET);
	}

	public function main_test()
	{
		// All '106' competences are commented because the competence with
		// id = 106 is a removed one.
		$v = new vectors_data_math(1, 1);
		$this->assert_identical($v->get_vectors_future_data(), array(
			0 => array(
				"vector_id" => 4,
				"future_edit_time" => "2013-00-00 00:00:01",
				"type" => "project",
				"project_id" => 3,
				"competences" => array(101 => 3, 102 => 3, 105 => 1/*, 106 => 1*/),
			),
			1 => array(
				"vector_id" => 1,
				"future_edit_time" => "2010-00-00 00:00:01",
				"type" => "project",
				"project_id" => 1,
				"competences" => array(101 => 3, 102 => 3, 103 => 2/*, 106 => 1*/),
			),
		));
		$this->assert_identical($v->get_vectors_self_data(), array(
			101 => 2, 102 => 3, 104 => 1/*, 106 => 3*/
		));
		$this->assert_identical($v->get_vectors_focus_data(), array(
			101 => array(
				0 => array(
					"vector_id" => 1,
					"type" => "project",
					"project_id" => 1,
					"add_time" => "2011-00-00 00:00:01",
				),
			),
			102 => array(
				0 => array(
					"vector_id" => 1,
					"type" => "project",
					"project_id" => 1,
					"add_time" => "2011-00-00 00:00:02",
				),
			),
			105 => array(
				0 => array(
					"vector_id" => 4,
					"type" => "project",
					"project_id" => 3,
					"add_time" => "2010-00-00 00:00:05",
				),
				1 => array(
					"vector_id" => 1,
					"type" => "project",
					"project_id" => 1,
					"add_time" => "2009-00-00 00:00:03",
				),
			),
			/*106 => array(
				0 => array(
					"vector_id" => 1,
					"type" => "project",
					"project_id" => 1,
					"add_time" => "2010-00-00 00:00:06",
				),
			),*/
		));
	}

	public function cache_test()
	{
		$v = new vectors_data_math(1, 1);

		$a = $v->get_vectors_future_data();
		$b = $v->get_vectors_future_data();
		$this->assert_identical($a, $b);

		$c = $v->get_vectors_self_data();
		$d = $v->get_vectors_self_data();
		$this->assert_identical($a, $b);

		$e = $v->get_vectors_focus_data();
		$e = $v->get_vectors_focus_data();
		$this->assert_identical($a, $b);
	}

	public function project_restriction_test()
	{
		$v = new vectors_data_math(1, 1, array(3));
		$this->assert_identical($v->get_vectors_future_data(), array(
			0 => array(
				"vector_id" => 4,
				"future_edit_time" => "2013-00-00 00:00:01",
				"type" => "project",
				"project_id" => 3,
				"competences" => array(101 => 3, 102 => 3, 105 => 1/*, 106 => 1*/),
			),
		));

		$v = new vectors_data_math(1, 1, array(1));
		$this->assert_identical($v->get_vectors_focus_data(), array(
			101 => array(
				0 => array(
					"vector_id" => 1,
					"type" => "project",
					"project_id" => 1,
					"add_time" => "2011-00-00 00:00:01",
				),
			),
			102 => array(
				0 => array(
					"vector_id" => 1,
					"type" => "project",
					"project_id" => 1,
					"add_time" => "2011-00-00 00:00:02",
				),
			),
			105 => array(
				0 => array(
					"vector_id" => 1,
					"type" => "project",
					"project_id" => 1,
					"add_time" => "2009-00-00 00:00:03",
				),
			),
			/*106 => array(
				0 => array(
					"vector_id" => 1,
					"type" => "project",
					"project_id" => 1,
					"add_time" => "2010-00-00 00:00:06",
				),
			),*/
		));
	}

	public function competence_restriction_test()
	{
		$v = new vectors_data_math(1, 1, null, array(101, 102));
		$this->assert_identical($v->get_vectors_future_data(), array(
			0 => array(
				"vector_id" => 4,
				"future_edit_time" => "2013-00-00 00:00:01",
				"type" => "project",
				"project_id" => 3,
				"competences" => array(101 => 3, 102 => 3),
			),
			1 => array(
				"vector_id" => 1,
				"future_edit_time" => "2010-00-00 00:00:01",
				"type" => "project",
				"project_id" => 1,
				"competences" => array(101 => 3, 102 => 3),
			),
		));
		$this->assert_identical($v->get_vectors_self_data(), array(
			101 => 2, 102 => 3
		));
		$this->assert_identical($v->get_vectors_focus_data(), array(
			101 => array(
				0 => array(
					"vector_id" => 1,
					"type" => "project",
					"project_id" => 1,
					"add_time" => "2011-00-00 00:00:01",
				),
			),
			102 => array(
				0 => array(
					"vector_id" => 1,
					"type" => "project",
					"project_id" => 1,
					"add_time" => "2011-00-00 00:00:02",
				),
			),
		));
	}

	public function no_data_test()
	{
		$v = new vectors_data_math(999, 999);
		$this->assert_identical($v->get_vectors_future_data(), array());
		$this->assert_identical($v->get_vectors_self_data(), array());
		$this->assert_identical($v->get_vectors_focus_data(), array());
	}

}

?>