<?php

class filter_raters_allowed_math_test extends base_test
{

	public function set_up()
	{
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (1, 'x 1')");
		}
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 2"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (2, 'x 2')");
		}
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 3"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (3, 'x 3')");
		}
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 4"))
		{
			$this->db->sql("REPLACE INTO user (id, login) VALUES (4, 'x 4')");
		}

		$this->db->sql("REPLACE INTO user_group (id, title) VALUES (1, 'TEST')");
		$this->db->sql("REPLACE INTO user_group (id, title) VALUES (2, 'TEST')");
		$this->db->sql("REPLACE INTO user_group (id, title) VALUES (3, 'TEST')");
		$this->db->sql("REPLACE INTO user_group (id, title) VALUES (4, 'TEST')");
		$this->db->sql("DELETE FROM user_group WHERE id BETWEEN 5 AND 10");

		$this->db->sql("REPLACE INTO user_user_group_link (user_id, user_group_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO user_user_group_link (user_id, user_group_id) VALUES (1, 2)");
		$this->db->sql("REPLACE INTO user_user_group_link (user_id, user_group_id) VALUES (2, 2)");
		$this->db->sql("REPLACE INTO user_user_group_link (user_id, user_group_id) VALUES (3, 3)");
	}

	public function main_1_test()
	{
		unset($_GET);

		$_GET["rater_user_groups"] = array("1", "3", "4");
		$allowed_user_group_id_array_as_keys_correct = array(1 => true, 3 => true, 4 => true);
		$allowed_rater_user_id_array_correct = array(1, 3);
		$allowed_rater_user_id_array_as_keys_correct = array(1 => true, 3 => true);
		
		$filter_raters_allowed = new filter_raters_allowed_math();
		$allowed_user_group_id_array_as_keys_real = $filter_raters_allowed->get_allowed_user_group_id_array_as_keys();
		$allowed_rater_user_id_array_real = $filter_raters_allowed->get_allowed_rater_user_id_array();
		$allowed_rater_user_id_array_as_keys_real = $filter_raters_allowed->get_allowed_rater_user_id_array_as_keys();
		$this->assert_identical($allowed_user_group_id_array_as_keys_real, $allowed_user_group_id_array_as_keys_correct);
		$this->assert_identical($allowed_rater_user_id_array_real, $allowed_rater_user_id_array_correct);
		$this->assert_identical($allowed_rater_user_id_array_as_keys_real, $allowed_rater_user_id_array_as_keys_correct);
	}

	public function main_2_test()
	{
		unset($_GET);

		$_GET["rater_user_groups_all"] = "1";
		$_GET["rater_user_groups"] = array("1", "3", "4");
		$allowed_user_group_id_array_as_keys_correct = array();
		$allowed_rater_user_id_array_correct = array();

		$filter_raters_allowed = new filter_raters_allowed_math();
		$allowed_user_group_id_array_as_keys_real = $filter_raters_allowed->get_allowed_user_group_id_array_as_keys();
		$allowed_rater_user_id_array_real = $filter_raters_allowed->get_allowed_rater_user_id_array();
		$this->assert_identical($allowed_user_group_id_array_as_keys_real, $allowed_user_group_id_array_as_keys_correct);
		$this->assert_identical($allowed_rater_user_id_array_real, $allowed_rater_user_id_array_correct);
	}

	public function duplicate_items_test()
	{
		unset($_GET);

		$_GET["rater_user_groups"] = array("1", "3", "4", "3");
		$allowed_user_group_id_array_as_keys_correct = array(1 => true, 3 => true, 4 => true);
		$allowed_rater_user_id_array_correct = array(1, 3);

		$filter_raters_allowed = new filter_raters_allowed_math();
		$allowed_user_group_id_array_as_keys_real = $filter_raters_allowed->get_allowed_user_group_id_array_as_keys();
		$allowed_rater_user_id_array_real = $filter_raters_allowed->get_allowed_rater_user_id_array();
		$this->assert_identical($allowed_user_group_id_array_as_keys_real, $allowed_user_group_id_array_as_keys_correct);
		$this->assert_identical($allowed_rater_user_id_array_real, $allowed_rater_user_id_array_correct);
	}

	public function empty_GET_test()
	{
		unset($_GET);

		$filter_raters_allowed = new filter_raters_allowed_math();

		$allowed_user_group_id_array_as_keys = $filter_raters_allowed->get_allowed_user_group_id_array_as_keys();
		$allowed_rater_user_id_array = $filter_raters_allowed->get_allowed_rater_user_id_array();
		$this->assert_identical($allowed_user_group_id_array_as_keys, array());
		$this->assert_identical($allowed_rater_user_id_array, array());
	}

	public function only_50_groups_are_allowed_test()
	{
		unset($_GET);
		
		$_GET["rater_user_groups"] = array(
			"9", "1", "1", "1", "1", "1", "1", "1", "1", "1",
			"2", "1", "1", "1", "1", "1", "1", "1", "1", "1",
			"1", "1", "1", "1", "1", "1", "1", "1", "1", "1",
			"5", "1", "1", "1", "1", "1", "1", "1", "1", "1",
			"6", "1", "1", "1", "1", "1", "1", "1", "1", "1",
			"7", "1", "1", "1", "1", "1", "1", "1", "1", "1",
		);
		$allowed_user_group_id_array_as_keys_correct = array(9 => true, 1 => true, 2 => true, 5 => true, 6 => true); // no 7 - it is the 51'st group!
		$allowed_rater_user_id_array_correct = array(1, 2);

		$filter_raters_allowed = new filter_raters_allowed_math();
		$allowed_user_group_id_array_as_keys_real = $filter_raters_allowed->get_allowed_user_group_id_array_as_keys();
		$allowed_rater_user_id_array_real = $filter_raters_allowed->get_allowed_rater_user_id_array();
		$this->assert_identical($allowed_user_group_id_array_as_keys_real, $allowed_user_group_id_array_as_keys_correct);
		$this->assert_identical($allowed_rater_user_id_array_real, $allowed_rater_user_id_array_correct);
	}

	public function tear_down()
	{
		unset($_GET);
	}

}

?>