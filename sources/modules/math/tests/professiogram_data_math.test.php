<?php

class professiogram_data_math_test extends base_test
{

	/**
	 * @var professiogram_data
	 */
	private $professiogram_data;

	public function set_up()
	{
		$this->select_db("math");
		$this->professiogram_data = new professiogram_data_math();
	}

	public function get_professiogram_data_test()
	{
		$result = $this->professiogram_data->get_professiogram_data(1);
		$this->assert_identical($result, array(
			'id' => 1,
			'competence_set_id' => 1,
			'rate_id' => 101,
			'title' => 'Prof 1',
			'competences' => array(101 => 3, 102 => 3,)
		));

		$result = $this->professiogram_data->get_professiogram_data(10);
		$this->assert_identical($result, false);
	}

	public function get_professiograms_data_test()
	{
		$correct_result = array(
			array(
				'id' => 1,
				'competence_set_id' => 1,
				'rate_id' => 101,
				'title' => 'Prof 1',
				'competences' => array (101 => 3, 102 => 3,)
			),
			array(
				'id' => 2,
				'competence_set_id' => 1,
				'rate_id' => 102,
				'title' => 'Prof 2',
				'competences' => array (102 => 3, 103 => 2,)
			),
		);

		$result = $this->professiogram_data->get_professiograms_data(array(1, 2));
		$this->assert_identical($result, $correct_result);

		$result = $this->professiogram_data->get_professiograms_data(array(1, 2, 10));
		$this->assert_identical($result, $correct_result);

		$result = $this->professiogram_data->get_professiograms_data(array(10, 20));
		$this->assert_identical($result, array());
	}

	public function get_professiograms_data_by_competence_set_test()
	{
		$correct_result = array(
			array(
				'id' => 4,
				'competence_set_id' => 1,
				'rate_id' => 104,
				'title' => 'ABC - Prof 4',
				'competences' => array (101 => 1, 102 => 1, 105 => 1,)
			),
			array(
				'id' => 1,
				'competence_set_id' => 1,
				'rate_id' => 101,
				'title' => 'Prof 1',
				'competences' => array (101 => 3, 102 => 3,)
			),
			array(
				'id' => 2,
				'competence_set_id' => 1,
				'rate_id' => 102,
				'title' => 'Prof 2',
				'competences' => array (102 => 3, 103 => 2,)
			),
			array(
				'id' => 3,
				'competence_set_id' => 1,
				'rate_id' => 103,
				'title' => 'Prof 3',
				'competences' => array (104 => 2, 105 => 2,)
			),
		);

		$result = $this->professiogram_data->get_professiograms_data_by_competence_set(1);
		$this->assert_identical($result, $correct_result);

		$correct_result_2 = array(0 => $correct_result[0], 1 => $correct_result[1]);
		$result = $this->professiogram_data->get_professiograms_data_by_competence_set(1, 2);
		$this->assert_identical($result, $correct_result_2);

		$correct_result_3 = array(0 => $correct_result[1], 1 => $correct_result[2]);
		$result = $this->professiogram_data->get_professiograms_data_by_competence_set(1, 2, false);
		$this->assert_identical($result, $correct_result_3);
	}

}

?>