<?php

class rate_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_attrs = array("competence_set_id");
	// Internal
	protected $rate_id; // Zero for rate addition
	protected $competence_set_id;

	/**
	 * Options keys and values
	 * Ex.: 1 => "one", 2 => "two", 3 => "three"
	 */
	protected $options = array();
	protected $focus_data;
	protected $future_data;

	public function __construct($rate_id, $competence_set_id, $options, $data = null, $focus_data = array(), $future_data = array())
	{
		$this->rate_id = $rate_id;
		$this->competence_set_id = $competence_set_id;
		$this->options = $options;
		$this->data = $data;
		$this->focus_data = $focus_data;
		$this->future_data = $future_data;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		if (!$this->rate_id)
		{
			$this->data = array();
			return;
		}

		if (!$this->data) // Not set in constructor
		{
			$rate_data_math = rate_data_math::get_instance($this->rate_id, $this->competence_set_id);
			$this->data = $rate_data_math->get_data();
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		$xdom->set_attr("id", $this->rate_id);
		$options_node = $xdom->create_child_node("options");
		foreach ($this->options as $mark => $title)
		{
			$options_node->create_child_node("option")
				->set_attr("mark", $mark)
				->set_attr("mark_title", $title);
		}

		foreach ($this->data as $competence_id => $data)
		{
			$competence_node = $xdom->create_child_node("competence")
				->set_attr("id", $competence_id)
				->set_attr("mark", $data["mark"])
				->set_attr("title", $data["title"]);

			if ($this->focus_data or $this->future_data)
			{
				if (isset($this->focus_data[$competence_id]))
				{
					$competence_node->set_attr("focus", true);
				}
				if (isset($this->future_data[$competence_id]))
				{
					$competence_node->set_attr("future", true);
				}
			}
		}
	}

}

?>