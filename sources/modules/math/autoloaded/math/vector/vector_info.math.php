<?php

/**
 * Provides information about vector. Creates vector record when needed.
 *
 * vector_info_struct structure is defined as follows:
 * array(
 *   "id" => <vector_id>,
 *   "foreign" => true|false, // true for 'foreign' vectors (the same competence 
 *                            // set but another project). Leads to a warning in 
 *                            // the interface; the user is asked to pass the 
 *                            // 'native' vector.
 *   "user_id" => <user_id>,
 *   "project_id" => <project_id>,
 *   "competence_set_id" => <competence_set_id>,
 *   "future_professiogram_count" => <number>,
 *   "focus_competence_count" => <number>,
 *   "calculations_base" => "results"|"self",
 *   "add_time" => <date_time>,
 *   "edit_time" => <date_time>,
 *   "future_edit_time" => <date_time>,
 *   "self_edit_time" => <date_time>,
 *   "focus_edit_time" => <date_time>,
 * )
 */
class vector_info_math
{

	/**
	 * @var db
	 */
	static $db;
	static $project_vector_config_cache = array();

	/**
	 * Provides information about project vector settings.
	 *
	 * @param id $project_id
	 * @param bool $lock
	 * @return array (
	 *   "project_id" => <project_id>,
	 *   "vector_is_on" => true|false,
	 *   "vector_competence_set_id" => <competence_set_id>,
	 *   "vector_self_must_skip" => true|false,
	 *   "vector_self_can_skip" => true|false,
	 *   "vector_self_can_skip_if_results" => true|false,
	 *   "vector_focus_can_skip" => true|false,
	 *   "vector_focus_min_competence_count" => <int>,
	 *   "vector_focus_max_competence_count" => <int>,
	 * )
	 */
	public static function get_project_vector_config($project_id, $lock = false)
	{
		if (isset(self::$project_vector_config_cache[$project_id]) and !$lock)
		{
			return self::$project_vector_config_cache[$project_id];
		}
		global $db, $config;
		self::$db = $db;
		$lock_sql = $lock ? "LOCK IN SHARE MODE" : "";
		$row = self::$db->get_row("
			SELECT
				id,
				vector_is_on,
				vector_competence_set_id,
				vector_self_must_skip, vector_self_can_skip, vector_self_can_skip_if_results,
				vector_focus_can_skip, vector_focus_min_competence_count, vector_focus_max_competence_count
			FROM project
			WHERE id = {$project_id}
			{$lock_sql}
		");
		if ($row && $row["vector_is_on"])
		{
			self::$project_vector_config_cache[$project_id] = array(
				"project_id" => (int) $row["id"],
				"vector_competence_set_id" => (int) $row["vector_competence_set_id"] ? (int) $row["vector_competence_set_id"] : (int) $config["default_vector_competence_set_id"],
				"vector_self_must_skip" => (bool) $row["vector_self_must_skip"],
				"vector_self_can_skip" => (bool) $row["vector_self_can_skip"],
				"vector_self_can_skip_if_results" => (bool) $row["vector_self_can_skip_if_results"],
				"vector_focus_can_skip" => (bool) $row["vector_focus_can_skip"],
				"vector_focus_min_competence_count" => (int) $row["vector_focus_min_competence_count"],
				"vector_focus_max_competence_count" => (int) $row["vector_focus_max_competence_count"],
			);
		}
		else
		{
			self::$project_vector_config_cache[$project_id] = false;
		}
		return self::$project_vector_config_cache[$project_id];
	}

	/**
	 * Provides general information (id, statistics etc.) about the project
	 * vector. Creates the vector if needed. Finds unassistedly the competence
	 * set required for the project.
	 *
	 * @param id $user_id
	 * @param id $project_id
	 *   is used if it is not specified in the project's db row (column
	 *   'vector_competence_set_id').
	 * @param bool $create_if_not_exist
	 * @param bool $look_for_foreign_vector - look for another vector for the
	 *   required competence set if the 'native' vector does not exist. This
	 *   check is performed only if $create_if_not_exist == false.
	 *
	 * @return false - if vector was not found, another suitable ('foreign')
	 * vector was not found and vector creation is not allowed.
	 * @return vector_info_struct
	 */
	public static function get_project_vector_info($user_id, $project_id, $create_if_not_exist = false, $look_for_foreign_vector = false)
	{
		global $db;
		self::$db = $db;
		$create_if_not_exist ? self::$db->begin() : null;
		$vector_config = self::get_project_vector_config($project_id, $create_if_not_exist);
		if ($vector_config === false)
		{
			$create_if_not_exist ? self::$db->rollback() : null;
			return false;
		}
		$competence_set_id = $vector_config["vector_competence_set_id"];
		if (!$competence_set_id)
		{
			$create_if_not_exist ? self::$db->rollback() : null;
			return false;
		}
		$sql = "SELECT * FROM vector WHERE project_id = {$project_id} AND user_id = {$user_id} AND competence_set_id = {$competence_set_id}";
		$sql_lock = "";
		if ($create_if_not_exist)
		{
			$sql_lock = " FOR UPDATE";
		}
		$foreign = false;
		$data = self::$db->get_row($sql . $sql_lock);
		if (!$data)
		{
			if ($create_if_not_exist)
			{
				if (!self::$db->row_exists("SELECT id FROM user WHERE id = {$user_id} LOCK IN SHARE MODE"))
				{
					self::$db->rollback();
					return false;
				}
				self::$db->sql("
					INSERT INTO vector
						(project_id, user_id, competence_set_id)
					VALUES
						({$project_id}, {$user_id}, {$competence_set_id})
				");
				$data = self::$db->get_row($sql);
				self::$db->commit();
				if (!$data)
				{
					trigger_error("Cannot add project vector ({$project_id}, {$user_id}, {$competence_set_id})");
					return false;
				}
			}
			else
			{
				if ($look_for_foreign_vector)
				{
					$vector_id = self::$db->get_value("
						SELECT default_vector_id_calc
						FROM user_competence_set_link_calc
						WHERE user_id = {$user_id} AND competence_set_id = {$competence_set_id}
					");
					if ($vector_id)
					{
						$data = self::$db->get_row("SELECT * FROM vector WHERE id = {$vector_id}");
						$foreign = true;
					}
				}
				if (!$data)
				{
					return false;
				}
			}
		}
		else
		{
			$create_if_not_exist ? self::$db->rollback() : null;
		}
		$vector_info_struct = array();
		$vector_info_struct["id"] = (int) $data["id"];
		$vector_info_struct["foreign"] = $foreign;
		$vector_info_struct["user_id"] = $user_id;
		$vector_info_struct["project_id"] = (int) $data["project_id"];
		$vector_info_struct["competence_set_id"] = (int) $data["competence_set_id"];
		$vector_info_struct["future_professiogram_count"] = (int) $data["future_professiogram_count_calc"];
		$vector_info_struct["focus_competence_count"] = (int) $data["focus_competence_count_calc"];
		$vector_info_struct["calculations_base"] = $data["calculations_base"];
		$vector_info_struct["add_time"] = $data["add_time"];
		$vector_info_struct["edit_time"] = $data["edit_time"];
		$vector_info_struct["future_edit_time"] = $data["future_edit_time"];
		$vector_info_struct["self_edit_time"] = $data["self_edit_time"];
		$vector_info_struct["focus_edit_time"] = $data["focus_edit_time"];
		return $vector_info_struct;
	}

	/**
	 * Provides information about existing project vectors for a user.
	 * 
	 * @global db $db
	 * @param id $user_id
	 * @param array $project_id_array - array(
	 *   <project_id> => <vector_competence_set_id>,
	 *   ...
	 * ); if there are no entries (array()), the output will be blank.
	 * 
	 * @return array(
	 *   <index> => array(
	 *     "id" => <vector_id>,
	 *     "user_id" => <user_id>,
	 *     "project_id" => <project_id>,
	 *     "competence_set_id" => <competence_set_id>,
	 *   ),
	 *   ...
	 * )
	 */
	public static function get_project_vectors_for_user($user_id, $project_id_array)
	{
		if (!sizeof($project_id_array))
		{
			return array();
		}
		global $db;
		self::$db = $db;
		$where_parts = array();
		foreach ($project_id_array as $project_id => $vector_competence_set_id)
		{
			$where_parts[] = " project_id = {$project_id} AND competence_set_id = {$vector_competence_set_id} ";
		}
		$where = "user_id = {$user_id} AND (" . join(" OR ", $where_parts) . ")";
		$db_result = self::$db->sql("
			SELECT id, project_id, user_id, competence_set_id
			FROM vector
			WHERE {$where}
			ORDER BY id
		");
		$result = array();
		while ($row = self::$db->fetch_array($db_result))
		{
			$result[] = array(
				"id" => (int) $row["id"],
				"user_id" => (int) $row["user_id"],
				"project_id" => (int) $row["project_id"],
				"competence_set_id" => (int) $row["competence_set_id"],
			);
		}
		return $result;
	}

	/**
	 * Provides information about existing vectors for a project.
	 *
	 * @global db $db
	 * @param id $project_id
	 * @param id $vector_competence_set_id
	 * @param array $user_id_array - array(<user_id>, ...) - if there are
	 * no entries (array()), the output will be blank.
	 *
	 * @return array(
	 *   <index> => array(
	 *     "id" => <vector_id>,
	 *     "user_id" => <user_id>,
	 *     "project_id" => <project_id>,
	 *     "competence_set_id" => <competence_set_id>,
	 *   ),
	 *   ...
	 * )
	 */
	public static function get_project_vectors_for_project($project_id, $vector_competence_set_id, $user_id_array)
	{
		if (!sizeof($user_id_array))
		{
			return array();
		}
		global $db;
		self::$db = $db;
		$db_result = self::$db->sql("
			SELECT id, project_id, user_id, competence_set_id
			FROM vector
			WHERE
				project_id = {$project_id}
				AND competence_set_id = {$vector_competence_set_id}
				AND user_id IN (" . join(",", $user_id_array) . ")
			ORDER BY id
		");
		$result = array();
		while ($row = self::$db->fetch_array($db_result))
		{
			$result[] = array(
				"id" => (int) $row["id"],
				"user_id" => (int) $row["user_id"],
				"project_id" => (int) $row["project_id"],
				"competence_set_id" => (int) $row["competence_set_id"],
			);
		}
		return $result;
	}

}

?>