<?php

/**
 * Provides information about all vectors of one competence_set for a user.
 * Used in pages with general results (where results from all projects are
 * summarized).
 */
class vectors_data_math
{

	/**
	 * @var db
	 */
	protected $db;
	protected $competence_set_id;
	protected $user_id;
	protected $project_id_array = array();
	protected $competence_id_array = array();
	protected $vector_id_array = null;
	protected $future_data = null;
	protected $self_data = null;
	protected $focus_data = null;

	/**
	 * @param id $competence_set_id
	 * @param id $user_id
	 * @param array $project_id_array = null - show only projects from this list
	 * @param array $competence_id_array = null - show only data for these competences
	 */
	public function __construct($competence_set_id, $user_id, $project_id_array = null, $competence_id_array = null)
	{
		$this->competence_set_id = $competence_set_id;
		$this->user_id = $user_id;
		$this->project_id_array = $project_id_array;
		$this->competence_id_array = $competence_id_array;

		global $db;
		$this->db = $db;
	}

	/**
	 * Provides information about all 'future' profiles of the vectors.
	 *
	 * @return array (
	 *   <index> => array(
	 *     "vector_id" => <vector_id>,
	 *     "future_edit_time" => <date_time>,
	 *     "project_id" => <project_id>,
	 *     "competences" => array (
	 *       <competence_id> => <mark>,
	 *       ...
	 *     ),
	 *   ),
	 *   ...
	 * ) // ordering by vector.edit_time DESC
	 */
	public function get_vectors_future_data()
	{
		if (!is_null($this->future_data))
		{
			return $this->future_data;
		}
		$this->fill_vector_id_array();
		if (!sizeof($this->vector_id_array) or is_array($this->competence_id_array) and !sizeof($this->competence_id_array))
		{
			$this->future_data = array();
			return $this->future_data;
		}
		$competence_id_restriction = is_array($this->competence_id_array) ? " AND vc.competence_id IN (" . join(",", $this->competence_id_array) . ")" : "";
		$result = array();
		$result_index = array();
		$next_result_key = 0;
		foreach ($this->vector_id_array as $vector_id => $vector_data)
		{
			$result[$next_result_key] = array(
				"vector_id" => (int) $vector_id,
				"future_edit_time" => $vector_data["future_edit_time"],
				"type" => "project",
				"project_id" => (int) $vector_data["project_id"],
				"competences" => array()
			);
			$result_index[(int) $vector_id] = $next_result_key;
			++$next_result_key;
		}
		$db_result = $this->db->sql("
			SELECT vc.vector_id, vc.competence_id, vc.mark
			FROM vector_future_competences_calc vc
			INNER JOIN competence_calc c ON c.competence_id = vc.competence_id
			WHERE
				vc.vector_id IN (" . join(",", array_keys($this->vector_id_array)) . ")
				{$competence_id_restriction}
			ORDER BY vc.competence_id
		");
		while ($row = $this->db->fetch_array($db_result))
		{
			$result[$result_index[(int) $row["vector_id"]]]["competences"][(int) $row["competence_id"]] = (int) $row["mark"];
		}
		$this->future_data = $result;
		return $this->future_data;
	}

	/**
	 * Provides information about all self marks (remember: self marks are
	 * connected directly to a competence set, not to a vector).
	 *
	 * @return array (
	 *   <competence_id> => <mark>,
	 *   ...
	 * ) // all competences are always outputed
	 */
	public function get_vectors_self_data()
	{
		if (!is_null($this->self_data))
		{
			return $this->self_data;
		}
		if (is_array($this->competence_id_array) and !sizeof($this->competence_id_array))
		{
			$this->self_data = array();
			return $this->self_data;
		}
		$competence_id_restriction = is_array($this->competence_id_array) ? " AND vs.competence_id IN (" . join(",", $this->competence_id_array) . ")" : "";
		$result_raw = $this->db->fetch_column_values("
			SELECT vs.competence_id, vs.mark
			FROM vector_self vs
			INNER JOIN competence_calc c ON c.competence_id = vs.competence_id
			WHERE
				vs.user_id = {$this->user_id}
				AND vs.competence_set_id = {$this->competence_set_id}
				{$competence_id_restriction}
		", "mark", "competence_id");
		$result = array();
		foreach ($result_raw as $key => $val)
		{
			$result[(int) $key] = (int) $val;
		}
		$this->self_data = $result;
		return $this->self_data;
	}

	/**
	 * Provides information about focus competences, including the information
	 * where (in which projects) the competence was selected as a focus one.
	 *
	 * @return array (
	 *   <competence_id> => array(
	 *     <index> => array (
	 *       "vector_id" => <vector_id>,
	 *       "project_id" => <project_id>,
	 *       "add_time" => <date_time>,
	 *     ),
	 *     ...
	 *   ), // ordering by vector.edit_time DESC
	 *   ...
	 * )
	 */
	public function get_vectors_focus_data()
	{
		if (!is_null($this->focus_data))
		{
			return $this->focus_data;
		}
		$this->fill_vector_id_array();
		if (!sizeof($this->vector_id_array) or is_array($this->competence_id_array) and !sizeof($this->competence_id_array))
		{
			$this->focus_data = array();
			return $this->focus_data;
		}
		$competence_id_restriction = is_array($this->competence_id_array) ? " AND vf.competence_id IN (" . join(",", $this->competence_id_array) . ")" : "";
		$db_result = $this->db->sql("
			SELECT vf.vector_id, vf.competence_id, vf.add_time
			FROM vector_focus vf
			INNER JOIN competence_calc c ON c.competence_id = vf.competence_id
			WHERE 
				vf.vector_id IN (" . join(",", array_keys($this->vector_id_array)) . ")
				{$competence_id_restriction}
			ORDER BY vf.competence_id, vf.add_time DESC
		");
		$result = array();
		while ($row = $this->db->fetch_array($db_result))
		{
			$competence_id = (int) $row["competence_id"];
			if (!isset($result[$competence_id]))
			{
				$result[$competence_id] = array();
			}
			$result[$competence_id][] = array(
				"vector_id" => (int) $row["vector_id"],
				"type" => "project",
				"project_id" => (int) $this->vector_id_array[$row["vector_id"]]["project_id"],
				"add_time" => $row["add_time"],
			);
		}
		$this->focus_data = $result;
		return $this->focus_data;
	}

	public function get_user_id()
	{
		return $this->user_id;
	}

	public function get_competence_set_id()
	{
		return $this->competence_set_id;
	}

	private function fill_vector_id_array()
	{
		if (is_null($this->vector_id_array))
		{
			$sql = "
				SELECT id, project_id, future_edit_time
				FROM vector
				WHERE user_id = {$this->user_id} AND competence_set_id = {$this->competence_set_id}
			";
			if (is_array($this->project_id_array))
			{
				if (!sizeof($this->project_id_array))
				{
					$this->vector_id_array = array();
					return;
				}
				else
				{
					$sql .= " AND project_id IN (" . join(",", $this->project_id_array) . ")";
				}
			}
			$sql .= " ORDER BY edit_time DESC LIMIT 100";
			$this->vector_id_array = $this->db->fetch_all($sql, "id");
		}
	}

}

?>