<?php

/**
 * Provides information about vector. Used in vector passage.
 * 
 * @todo fix this table
 * Method \ Step                  Future  Self  Focus  Recommendations
 * get_future_data()                 +             +
 * get_future_competences_data()            +      +      +
 * get_self_data()                          +      +      +
 * get_focus_data()                                +      +
 */
class vector_data_math
{
	const MAX_FUTURE_PROFESSIOGRAMS = 150;

	/**
	 * @var db
	 */
	private $db;
	private $vector_id;
	private $user_id;
	private $competence_set_id;
	private $focus_competences_only = false;

	/**
	 * @var array(<professiogram_id> => true, ...)
	 */
	private $checked_professiograms = null;

	/**
	 * @var array(<index> => <professiogram_id>, ...)
	 */
	private $possible_professiograms_data = null;

	/**
	 * @var array(<professiogram_id> => <match>, ...)
	 */
	private $possible_professiograms_matches = null;

	/**
	 * Cache of get_future_data()
	 */
	private $future_data = array("00" => null, "01" => null, "10" => null, "11" => null);

	/**
	 * Cache of get_future_competences_data()
	 */
	private $future_competences_data = null;

	/**
	 * Cache of get_future_competences_details()
	 */
	private $future_competences_details = null;

	/**
	 * Cache of get_self_data()
	 */
	private $self_data = null;

	/**
	 * Cache of get_self_stat()
	 */
	private $self_stat = null;

	/**
	 * Cache of get_focus_data()
	 */
	private $focus_data = null;

	/**
	 * @param id $vector_id - vector id (it is not checked here that vector
	 * exists).
	 * @param id $user_id - vector's user_id (it is not checked here that it 
	 * is correct).
	 * @param id $competence_set_id - vector's competence_set_id (it is not
	 * checked here that it is correct).
	 * @param bool $focus_competences_only - we are interested in focus
	 * competences only.
	 */
	public function __construct($vector_id, $user_id, $competence_set_id, $focus_competences_only = false)
	{
		global $db;
		$this->db = $db;

		if ($vector_id xor $user_id)
		{
			trigger_error("\$vector_id and \$user_id must be both zero or non-zero");
		}

		$this->vector_id = $vector_id;
		$this->user_id = $user_id;
		$this->competence_set_id = $competence_set_id;
		$this->focus_competences_only = $focus_competences_only;
	}

	/**
	 * Returns data about professiograms which can be selected by a user.
	 *
	 * @return array (
	 *   <index> => array (
	 *     "id" => <professiogram_id>,
	 *     "competence_set_id" => <competence_set_id>,
	 *     "rate_id" => <rate_id>,
	 *     "title" => <title>,
	 *     "competences" => array(
	 *       <competence_id> => <mark>,
	 *       ...
	 *     ),
	 *   ),
	 *   ...
	 * )
	 */
	public function get_possible_professiograms_data()
	{
		if (!is_null($this->possible_professiograms_data))
		{
			return $this->possible_professiograms_data;
		}
		$professiogram_data_math = new professiogram_data_math();
		$this->possible_professiograms_data = $professiogram_data_math->get_professiograms_data_by_competence_set($this->competence_set_id, self::MAX_FUTURE_PROFESSIOGRAMS);
		return $this->possible_professiograms_data;
	}

	/**
	 * Returns professiograms for the vector - all which can be selected by
	 * a user or only those which are already selected.
	 * 
	 * @param bool $checked_only - export only professiograms checked by user in
	 *   _this_ vector.
	 * @param bool $calculate_match_using_self - calculate match using self
	 *   marks. Affects get_future_competences_data() and get_self_data().
	 * 
	 * @return array (
	 *   <professiogram_id> => array (
	 *     "checked" => true|false,
	 *     "title" => <professiogram_title>,
	 *     ["match" => <percent>,]    // Param exists if
	 *                                // $calculate_match_using_self is true
	 *   ),
	 *   ...
	 * )
	 */
	public function get_future_data($checked_only = false, $calculate_match_using_self = false)
	{
		$key = $checked_only ? "1" : "0";
		$key .= $calculate_match_using_self ? "1" : "0";
		if (!is_null($this->future_data[$key]))
		{
			return $this->future_data[$key];
		}
		$this->get_possible_professiograms_data();
		$this->fill_checked_professiograms();
		if ($calculate_match_using_self)
		{
			$this->fill_possible_professiograms_matches();
		}
		$this->future_data[$key] = array();
		foreach ($this->possible_professiograms_data as $professiogram_data)
		{
			$professiogram_id = $professiogram_data["id"];
			$checked = isset($this->checked_professiograms[$professiogram_id]);
			if (!$checked_only or $checked)
			{
				$data = array(
					"checked" => $checked,
					"title" => $professiogram_data["title"],
				);
				if ($calculate_match_using_self)
				{
					$data["match"] = $this->possible_professiograms_matches[$professiogram_id];
				}
				$this->future_data[$key][$professiogram_id] = $data;
			}
		}
		return $this->future_data[$key];
	}

	/**
	 * Returns all competences necessary for the professions selected by a user.
	 * If any competence exists in 2 or more professiograms, the max value is
	 * taken.
	 * 
	 * @return array (
	 *   <competence_id> => <mark>,
	 *   ...
	 * )
	 */
	public function get_future_competences_data()
	{
		if (!is_null($this->future_competences_data))
		{
			return $this->future_competences_data;
		}
		if ($this->focus_competences_only)
		{
			// Building cache
			$this->get_focus_data();
		}
		$db_result = $this->db->sql("
			SELECT vc.competence_id, vc.mark
			FROM vector_future_competences_calc vc
			INNER JOIN competence_calc c ON c.competence_id = vc.competence_id
			WHERE vc.vector_id = {$this->vector_id}
			ORDER BY vc.competence_id
		");
		$this->future_competences_data = array();
		while ($row = $this->db->fetch_array($db_result))
		{
			$competence_id = (int) $row["competence_id"];
			if (!$this->focus_competences_only or isset($this->focus_data[$competence_id]))
			{
				$this->future_competences_data[$competence_id] = (int) $row["mark"];
			}
		}
		return $this->future_competences_data;
	}

	/**
	 * Provides information, for which professiogram each competence
	 * is necessary.
	 *
	 * @return array (
	 *   <competence_id> => array (
	 *     <professiogram_id> => <mark>,
	 *     ...
	 *   ),
	 *   ...
	 * )
	 */
	public function get_future_competences_details()
	{
		if (!is_null($this->future_competences_details))
		{
			return $this->future_competences_details;
		}
		$this->get_possible_professiograms_data();
		$this->fill_checked_professiograms();
		$this->future_competences_details = array();
		foreach ($this->possible_professiograms_data as $professiogram_data)
		{
			$professiogram_id = $professiogram_data["id"];
			$checked = isset($this->checked_professiograms[$professiogram_id]);
			if ($checked)
			{
				foreach ($professiogram_data["competences"] as $competence_id => $mark)
				{
					if (!isset($this->future_competences_details[$competence_id]))
					{
						$this->future_competences_details[$competence_id] = array();
					}
					$this->future_competences_details[$competence_id][$professiogram_id] = $mark;
				}
			}
		}
		ksort($this->future_competences_details);
		return $this->future_competences_details;
	}

	/**
	 * Returns statistics about self marks: how many marks are filled and what
	 * is the last edit time.
	 *
	 * @return array (
	 *   "mark_count" => <mark_count>,
	 *   "edit_time" => <datetime>,
	 * )
	 */
	public function get_self_stat()
	{
		if (!is_null($this->self_stat))
		{
			return $this->self_stat;
		}
		$row = $this->db->get_row("SELECT self_competence_count_calc, edit_time FROM vector_self_stat WHERE user_id = {$this->user_id} AND competence_set_id = {$this->competence_set_id}");
		if ($row)
		{
			$this->self_stat = array(
				"mark_count" => (int) $row["self_competence_count_calc"],
				"edit_time" => $row["edit_time"],
			);
		}
		else
		{
			$this->self_stat = array(
				"mark_count" => 0,
				"edit_time" => "0000-00-00 00:00:00",
			);
		}
		return $this->self_stat;
	}

	/**
	 * Returns ALL self marks (including those which are NOT necessary for the
	 * "future" professions).
	 *
	 * @return array (
	 *   <competence_id> => <mark>,
	 * )
	 */
	public function get_self_data()
	{
		if (!is_null($this->self_data))
		{
			return $this->self_data;
		}
		if ($this->focus_competences_only)
		{
			// Building cache
			$this->get_focus_data();
		}
		$db_result = $this->db->sql("
			SELECT vs.competence_id, vs.mark
			FROM vector_self vs
			INNER JOIN competence_calc c ON c.competence_id = vs.competence_id
			WHERE vs.user_id = {$this->user_id} AND vs.competence_set_id = {$this->competence_set_id}
			ORDER BY vs.competence_id
		");
		$this->self_data = array();
		while ($row = $this->db->fetch_array($db_result))
		{
			$competence_id = (int) $row["competence_id"];
			if (!$this->focus_competences_only or isset($this->focus_data[$competence_id]))
			{
				$this->self_data[$competence_id] = (int) $row["mark"];
			}
		}
		return $this->self_data;
	}
	
	/**
	 * Returns whether there are self marks for all future competences.
	 * 
	 * @return bool
	 */
	public function is_self_complete()
	{
		if ($this->focus_competences_only)
		{
			trigger_error("Unexpected function usage. The call does not have sense.");
		}
		if (!$this->vector_id)
		{
			return false;
		}
		$self_data = $this->get_self_data();
		$future_competences_data = $this->get_future_competences_data();
		foreach ($self_data as $competence_id => $mark)
		{
			if (isset($future_competences_data[$competence_id]))
			{
				unset($future_competences_data[$competence_id]);
			}
		}
		return sizeof($future_competences_data) == 0;
	}

	/**
	 * Returns focus competences as array keys.
	 *
	 * @return array (
	 *   <competence_id> => true,
	 * )
	 */
	public function get_focus_data()
	{
		if (!is_null($this->focus_data))
		{
			return $this->focus_data;
		}
		$db_result = $this->db->sql("
			SELECT vf.competence_id
			FROM vector_focus vf
			INNER JOIN competence_calc c ON c.competence_id = vf.competence_id
			WHERE vf.vector_id = {$this->vector_id}
			ORDER BY vf.competence_id
		");
		$this->focus_data = array();
		while ($row = $this->db->fetch_array($db_result))
		{
			$this->focus_data[(int) $row["competence_id"]] = true;
		}
		return $this->focus_data;
	}

	public function get_vector_id()
	{
		return $this->vector_id;
	}

	public function get_user_id()
	{
		return $this->user_id;
	}

	public function get_competence_set_id()
	{
		return $this->competence_set_id;
	}

	private function fill_checked_professiograms()
	{
		if (is_null($this->checked_professiograms))
		{
			$db_result = $this->db->sql("
				SELECT professiogram_id
				FROM vector_future
				WHERE vector_id = {$this->vector_id}
			");
			$this->checked_professiograms = array();
			while ($row = $this->db->fetch_array($db_result))
			{
				$this->checked_professiograms[(int) $row["professiogram_id"]] = true;
			}
		}
	}

	private function fill_possible_professiograms_matches()
	{
		if (is_null($this->possible_professiograms_matches))
		{
			$this->get_possible_professiograms_data();
			$this->get_self_data();
			// Fake results - just to use existing mathematics from the
			// rate_match_math class.
			$results = array(1 => array("sum" => $this->self_data));
			$rm = new rate_match_math($results, $this->possible_professiograms_data);
			$this->possible_professiograms_matches = $rm->get_rate_rating_for_user(1);
		}
	}

}

?>