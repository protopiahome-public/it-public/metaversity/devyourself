<?php

class results_math
{

	/**
	 * @var db
	 */
	private $db;
	private $competence_set_id;
	private $user_id_array = null;
	private $competence_id_array = array();
	private $used_mark_count = 0;
	private $calc_stat = false;
	private $export_marks_log = false;
	private $stat = array();
	private $marks_log = array();
	private $results = array();
	/**
	 * @var filter_main_math
	 */
	private $filter_main;
	/**
	 * @var filter_raters_allowed_math
	 */
	private $filter_raters_allowed;
	/**
	 * @var filter_raters_real_math
	 */
	private $filter_raters_real;
	private $selected_projects;
	private $rater_user_id_array_as_keys = array();
	private $used_rater_user_id_array_as_keys = array();
	private $used_rater_count = 0;

	public function __construct($competence_set_id, $user_id_array = null, $competence_id_array = array(), $ignore_user_selection = false, $calc_stat = false, $export_marks_log = false, $project_id = null)
	{
		global $db;
		$this->db = $db;

		$this->competence_set_id = $competence_set_id;
		$this->user_id_array = $user_id_array;
		$this->competence_id_array = $competence_id_array;
		$this->calc_stat = $calc_stat;
		$this->export_marks_log = $export_marks_log;

		$this->filter_main = new filter_main_math($competence_set_id, $user_id_array, $ignore_user_selection, $project_id);
		$this->selected_projects = $this->filter_main->get_selected_projects();

		$this->filter_raters_allowed = new filter_raters_allowed_math();

		$this->calc_results();
		$this->calc_sum();

		$this->filter_raters_real = new filter_raters_real_math($this->filter_raters_allowed->get_allowed_user_group_id_array_as_keys(), $this->rater_user_id_array_as_keys);
	}

	/**
	 * @return filter_main_math
	 */
	public function get_filter_main()
	{
		return $this->filter_main;
	}

	/**
	 * @return filter_raters_real_math
	 */
	public function get_filter_raters_real()
	{
		return $this->filter_raters_real;
	}

	public function get_results()
	{
		return $this->results;
	}

	public function get_used_rater_count()
	{
		return $this->used_rater_count;
	}

	public function get_used_mark_count()
	{
		return $this->used_mark_count;
	}

	public function get_stat()
	{
		return $this->stat;
	}

	public function get_marks_log()
	{
		return $this->marks_log;
	}

	private function calc_results()
	{
		foreach ($this->selected_projects as $project_key => $data)
		{
			if ($data["translator_id"] == 0)
			{
				$class = "a_own_calc_math";
			}
			else
			{
				$class = "id{$data["translator_id"]}_from{$data["competence_set_id"]}_to{$this->competence_set_id}_translator_calc_math";
			}
			$calc = new $class(
							$this->results,
							$this->used_mark_count,
							$this->stat,
							$this->calc_stat,
							$this->competence_set_id,
							$this->user_id_array,
							$project_key,
							$data["project_id"],
							$this->rater_user_id_array_as_keys,
							$this->used_rater_user_id_array_as_keys,
							$this->competence_id_array,
							$this->filter_raters_allowed->get_allowed_rater_user_id_array_as_keys(),
							$this->export_marks_log,
							$this->marks_log
			);
			/* @var $calc base_calc_math */
			$calc->run();
		}
		if ($this->calc_stat)
		{
			foreach ($this->stat as $competence_id => &$data)
			{
				$data["raters"] = sizeof($data["raters"]);
				$data["precedents"] = sizeof($data["precedents"]);
				foreach ($data["stat_tags"] as $stat_type => $tmp)
				{
					$data["stat_type_" . $stat_type] = true;
				}
				unset($data["stat_tags"]);
			}
		}
		$this->used_rater_count = sizeof($this->used_rater_user_id_array_as_keys);
	}

	private function calc_sum()
	{
		foreach ($this->results as $user_id => &$user_data)
		{
			$sum = reset($user_data);
			if ($sum !== false)
			{
				while ($next = next($user_data))
				{
					foreach ($next as $competence_id => $mark)
					{
						if (!isset($sum[$competence_id]) or $sum[$competence_id] < $mark)
						{
							$sum[$competence_id] = $mark;
						}
					}
				}
				$user_data["sum"] = $sum;
			}
		}
	}

}

?>