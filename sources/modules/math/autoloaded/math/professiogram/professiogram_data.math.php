<?php

class professiogram_data_math
{

	/**
	 * @var db
	 */
	private $db;
	private $professiogram_count = null;

	public function __construct()
	{
		global $db;
		$this->db = $db;
	}

	public function get_professiogram_data($professiogram_id)
	{
		$result = $this->get_data("p.id = {$professiogram_id}");
		if (isset($result[0]))
		{
			return $result[0];
		}
		else
		{
			return false;
		}
	}

	public function get_professiograms_data($professiogram_id_array)
	{
		if (!sizeof($professiogram_id_array))
		{
			return array();
		}
		return $this->get_data("p.id IN (" . join(",", $professiogram_id_array) . ")");
	}

	public function get_professiograms_data_by_competence_set($competence_set_id, $limit_count = 0, $order_by_title = true)
	{
		$where = "p.competence_set_id = {$competence_set_id}";
		return $this->get_data($where, $limit_count, $order_by_title);
	}

	/**
	 * @return array (
	 *   <index> => array (
	 *     "id" => <professiogram_id>,
	 *     "competence_set_id" => <competence_set_id>,
	 *     "rate_id" => <rate_id>,
	 *     "title" => <title>,
	 *     "competences" => array(
	 *       <competence_id> => <mark>,
	 *       ...
	 *     ),
	 *   ),
	 *   ...
	 * )
	 */
	protected function get_data($where, $limit_count = 0, $order_by_title = true)
	{
		/* Selecting professiograms from DB */
		$order_sql = $order_by_title ? "ORDER BY p.title" : "";
		$limit_sql = $limit_count ? "LIMIT " . $limit_count : "";
		$db_result = $this->db->sql("
			SELECT SQL_CALC_FOUND_ROWS
				id, competence_set_id, rate_id, title
			FROM professiogram p
			WHERE ({$where}) AND p.enabled = 1
			{$order_sql}
			{$limit_sql}
		");
		$this->professiogram_count = $this->db->get_value("SELECT FOUND_ROWS()");

		/* Fetching professiograms */
		$result = array();
		$rate_id_array = array();
		$rate_data = array();
		while ($row = $this->db->fetch_array($db_result))
		{
			$result[] = array(
				"id" => (int) $row["id"],
				"competence_set_id" => (int) $row["competence_set_id"],
				"rate_id" => (int) $row["rate_id"],
				"title" => $row["title"],
				"competences" => array(),
			);
			$rate_id_array[] = $row["rate_id"];
			$rate_data[$row["rate_id"]] = array();
		}

		if (sizeof($rate_id_array))
		{
			/* Selecting competences and marks */
			$db_result_2 = $this->db->sql("
				SELECT l.rate_id, l.competence_id, l.mark
				FROM rate_competence_link l
				INNER JOIN competence_calc c ON l.competence_id = c.competence_id /* Removed competences must be removed from the output */
				WHERE l.rate_id IN (" . join(",", $rate_id_array) . ")
			");

			/* Fetching competences and marks */
			while ($row2 = $this->db->fetch_array($db_result_2))
			{
				$rate_data[(int) $row2["rate_id"]][(int) $row2["competence_id"]] = (int) $row2["mark"];
			}
			foreach ($result as &$result_item)
			{
				$result_item["competences"] = &$rate_data[$result_item["rate_id"]];
			}
		}

		return $result;
	}

	public function get_professiogram_count()
	{
		return $this->professiogram_count;
	}

}

?>