<?php

class rate_match_math
{
	/**
	 * @var db
	 */
	private $db;

	private $results;

	private $rate_data;

	public function __construct($results, $rate_data)
	{
		global $db;
		$this->db = $db;

		$this->results = $results;
		$this->rate_data = $rate_data;
	}

	public function get_rate_rating_for_user($user_id)
	{
		$rating = array();
		foreach ($this->rate_data as $rate_index => $rate_data)
		{
			$match = $this->get_user_rate_match($user_id, $rate_index);
			$rating[$rate_data["id"]] = $match; // zero matches are allowed
		}
		arsort($rating, SORT_NUMERIC);
		return $rating;
	}

	public function get_user_rating_for_rate($rate_index = 0)
	{
		$rating = array();
		foreach ($this->results as $user_id => $user_data)
		{
			$match = $this->get_user_rate_match($user_id, $rate_index);
			if ($match > 0) // zero matches are not allowed here
			{
				$rating[$user_id] = $match;
			}
		}
		arsort($rating, SORT_NUMERIC);
		return $rating;
	}

	public function get_user_rate_match($user_id, $rate_index = 0)
	{
		$numerator = 0;
		$denominator = 0;
		foreach ($this->rate_data[$rate_index]["competences"] as $competence_id => $rate_mark)
		{
			$denominator += $rate_mark;
			$result_mark = isset($this->results[$user_id]["sum"][$competence_id]) ? $this->results[$user_id]["sum"][$competence_id] : 0;
			$numerator += $result_mark >= $rate_mark ? $rate_mark : $result_mark;
		}
		return $denominator > 0 ? (int)round($numerator * 100 / $denominator) : 0;
	}

}

?>