<?php

class rate_data_math
{
	
	/**
	 * @var db
	 */
	private $db;
	private $rate_id;
	private $competence_set_id;
	private $data = null;
	
	private function __construct($rate_id, $competence_set_id)
	{
		global $db;
		$this->db = $db;
		
		$this->rate_id = $rate_id;
		$this->competence_set_id = $competence_set_id ? $competence_set_id : 0;
	}
	
	public static function get_instance($rate_id, $competence_set_id)
	{
		return new rate_data_math($rate_id, $competence_set_id);
	}
	
	public static function get_rate_id_multi_link($type, $object_id, $competence_set_id)
	{
		global $db;
		/* @var $db db */
		$rate_id = $db->get_value("
			SELECT rate_id 
			FROM {$type}_rate_link
			WHERE {$type}_id = {$object_id} AND competence_set_id = {$competence_set_id}
		");
		return $rate_id ? $rate_id : 0;
	}
	
	public static function get_instance_multi_link($type, $object_id, $competence_set_id)
	{
		$rate_id = $competence_set_id ? self::get_rate_id_multi_link($type, $object_id, $competence_set_id) : 0;
		return new rate_data_math($rate_id, $competence_set_id);
	}
	
	public function exists()
	{
		return $this->rate_id > 0;
	}
	
	public function get_id()
	{
		return $this->rate_id;
	}
	
	public function get_data()
	{
		if (!$this->rate_id or !$this->competence_set_id)
		{
			return array();
		}
		if (!is_null($this->data))
		{
			return $this->data;
		}
		$cache = rate_data_cache::init($this->rate_id, $this->competence_set_id);
		$this->data = $cache->get();
		if (!$this->data)
		{
			$this->data = $this->db->fetch_all("
				SELECT l.competence_id, c.title, l.mark
				FROM rate_competence_link l
				JOIN competence_calc c ON c.competence_id = l.competence_id
				WHERE rate_id = {$this->rate_id}
				ORDER BY mark DESC, competence_id ASC
			", "competence_id");
			$cache->set($this->data);
		}
		return $this->data;
	}
	
}

?>