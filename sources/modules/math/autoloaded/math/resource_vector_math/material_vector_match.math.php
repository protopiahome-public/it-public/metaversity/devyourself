<?php

class material_vector_match_math extends base_resource_vector_match_math
{

	protected $resource_name = "material";

	protected function get_resource_vector_match_cache($resource_id, $vector_id, $competence_set_id, $calc_base)
	{
		return material_vector_match_cache::init($resource_id, $vector_id, $competence_set_id, $calc_base);
	}

	protected function get_resources_vector_max_match_cache($project_id, $vector_id, $competence_set_id, $calc_base)
	{
		return materials_vector_max_match_cache::init($project_id, $vector_id, $competence_set_id, $calc_base);
	}

}

?>
