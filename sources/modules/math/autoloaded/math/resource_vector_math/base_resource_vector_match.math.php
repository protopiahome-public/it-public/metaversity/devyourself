<?php

abstract class base_resource_vector_match_math
{

	protected $resource_name;

	/**
	 * @var db
	 */
	private $db;
	protected $user_id;
	protected $project_id;
	private $vector_calc_base;
	private $vector_id;
	private $vector_competence_set_id;
	private $vector_data;
	private $vector_competences;
	private $resources_data_getter;
	private $max_raw_match;
	private $vector_exists = false;

	public function __construct($project_id, $user_id, $vector_calc_base)
	{
		global $db;
		$this->db = $db;

		$this->user_id = $user_id;
		$this->project_id = $project_id;
		$this->vector_calc_base = $vector_calc_base;

		if (!in_array($vector_calc_base, array("future", "focus")))
		{
			trigger_error("Incorrect vector_calc_base: '{$vector_calc_base}'");
		}

		$this->prepare();
	}

	private function prepare()
	{
		$vector_config = vector_info_math::get_project_vector_config($this->project_id);
		if (!$vector_config)
		{
			return;
		}

		$this->vector_competence_set_id = $vector_config["vector_competence_set_id"];
		$vector_info_struct = vector_info_math::get_project_vector_info($this->user_id, $this->project_id, false, true);
		if (!$vector_info_struct)
		{
			return;
		}

		$this->vector_exists = true;
		$this->vector_id = $vector_info_struct["id"];
		$this->vector_data = new vector_data_math($this->vector_id, $this->user_id, $vector_info_struct["competence_set_id"]);

		if ($this->vector_calc_base == "focus")
		{
			//1 - default (focus competences)
			$this->vector_competences = $this->get_focus_data();
		}
		elseif ($this->vector_calc_base == "future")
		{
			//2 - future
			$this->vector_competences = $this->get_future_data();
		}
	}

	public function get_competence_set_id()
	{
		return $this->vector_competence_set_id;
	}

	public function get_focus_data()
	{
		return $this->vector_data->get_focus_data();
	}

	public function get_future_data()
	{
		return $this->vector_data->get_future_competences_data();
	}

	public function vector_exists()
	{
		return $this->vector_exists;
	}

	public function set_resources_data_getter($resources_data_getter)
	{
		$this->resources_data_getter = $resources_data_getter;
	}

	public function get_match($id, $rate_data)
	{
		$max_raw_match = $this->get_max_raw_match();
		$match = 0;
		if ($max_raw_match > 0)
		{
			$raw_match = $this->get_raw_match($id, $rate_data);
			$match = round($raw_match / $max_raw_match * 10);
		}

		return $match;
	}

	private function get_max_raw_match()
	{
		if (isset($this->max_raw_match))
		{
			return $this->max_raw_match;
		}
		else
		{
			$max_match_cache = $this->get_resources_vector_max_match_cache($this->project_id, $this->vector_id, $this->vector_competence_set_id, $this->vector_calc_base);
			$max_raw_match = $max_match_cache->get();
			if (!is_good_num($max_raw_match))
			{
				$resources_data = call_user_func($this->resources_data_getter);

				$match_list = array(0);
				foreach ($resources_data as $row)
				{
					$match_list[] = $this->get_raw_match($row["id"], $row["rate_data"]);
				}
				$max_raw_match = max($match_list);

				$max_match_cache = $this->get_resources_vector_max_match_cache($this->project_id, $this->vector_id, $this->vector_competence_set_id, $this->vector_calc_base);
				$max_match_cache->set($max_raw_match);
			}
			$this->max_raw_match = $max_raw_match;
			return $this->max_raw_match;
		}
	}

	private function get_raw_match($id, $rate_data)
	{
		if (!sizeof($rate_data))
		{
			return 0;
		}

		$vector_match_cache = $this->get_resource_vector_match_cache($id, $this->vector_id, $this->vector_competence_set_id, $this->vector_calc_base);
		$raw_match = $vector_match_cache->get();
		if (!$raw_match)
		{
			$raw_match = 0;
			foreach ($rate_data as $competence => $rate_competence_data)
			{
				if (isset($this->vector_competences[$competence]))
				{
					$raw_match += $rate_competence_data["mark"];
				}
			}
			$vector_match_cache = $this->get_resource_vector_match_cache($id, $this->vector_id, $this->vector_competence_set_id, $this->vector_calc_base);
			$vector_match_cache->set($raw_match);
		}
		return $raw_match;
	}

	abstract protected function get_resources_vector_max_match_cache($project_id, $vector_id, $competence_set_id, $calc_base);

	abstract protected function get_resource_vector_match_cache($resource_id, $vector_id, $competence_set_id, $calc_base);
}

?>