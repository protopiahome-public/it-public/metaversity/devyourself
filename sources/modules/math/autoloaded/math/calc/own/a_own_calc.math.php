<?php


class a_own_calc_math extends base_own_calc_math
{

	public function run()
	{
		if (is_array($this->user_id_array) and !sizeof($this->user_id_array))
		{
			return; // $this->user_id_array == array() means that no results are required (no users were selected)
		}

		$sql_competence_id_where = sizeof($this->competence_id_array) ? "AND competence_id IN (" . join(",", $this->competence_id_array) . ")" : "";
		$sql_user_where = is_array($this->user_id_array) ? "AND ratee_user_id IN (" . join(",", $this->user_id_array) . ")" : "";
		$sql_stat_fields = ($this->calc_stat or $this->export_marks_log) ? ", rater_user_id, precedent_id" : "";
		$sql_marks_export_fields = $this->export_marks_log ? ", comment" : "";
		$db_result = $this->db->sql("
			SELECT
				ratee_user_id, rater_user_id, competence_id, type, value {$sql_stat_fields} {$sql_marks_export_fields}
			FROM mark_calc
			WHERE competence_set_id = {$this->competence_set_id} AND project_id = {$this->project_id}
			{$sql_competence_id_where}
			{$sql_user_where}
			ORDER BY ratee_user_id, competence_id
		");
		$small_marks_log = array();
		calc_helper_math::call_for_every_user_and_competence($db_result, array($this, "_callback"), $this->rater_user_id_array_as_keys, $this->used_rater_user_id_array_as_keys, $this->used_mark_count, $this->stat, $this->calc_stat, "own", $this->allowed_rater_user_id_array_as_keys, $this->export_marks_log, $small_marks_log);
		if ($this->export_marks_log)
		{
			$this->marks_log[$this->project_key] = array(
				"project_id" => $this->project_id,
				"competence_set_id" => $this->competence_set_id,
				"competences" => array(),
			);
			$p = &$this->marks_log[$this->project_key];
			foreach ($small_marks_log as $export_data)
			{
				$competence_id = $export_data["competence_id"];
				if (!isset($p["competences"][$competence_id]))
				{
					$p["competences"][$competence_id] = array("marks" => array());
				}
				$p["competences"][$competence_id]["marks"][] = $export_data;
			}
		}
	}

	public function _callback($ratee_user_id, $competence_id, $marks)
	{
		if (!isset($this->results[$ratee_user_id]))
		{
			$this->results[$ratee_user_id] = array();
		}
		if (!isset($this->results[$ratee_user_id][$this->project_key]))
		{
			$this->results[$ratee_user_id][$this->project_key] = array();
		}
		$this->results[$ratee_user_id][$this->project_key][$competence_id] = calc_helper_math::get_max_weighted_mark($marks);
	}

}

?>