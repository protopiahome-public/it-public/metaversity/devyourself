<?php

class calc_helper_math
{

	/**
	 * @var db
	 */
	private static $db;

	public static function call_for_every_user_and_competence(
	$marks_ordered_by_ratee_user_id_competence_id_db_result, // db_result, выборка select from mark_calc order by ratee_user_id, competence_id
			$callback, // функция, которую вызывать (ratee_user_id, competence_id, array(  array('type'=>, 'value'=>), array('type'=>, 'value'=>), ... ) )
			&$rater_user_id_array_as_keys, // массив вида ( 1=>true. 2=>true, 3=>true ) rater_user_id, оценки которых встретились в выборке
			&$used_rater_user_id_array_as_keys, // массив вида ( 1=>true. 2=>true, 3=>true ) rater_user_id, оценки которых были учтены
			&$used_mark_count, // количество оценок, которые были учтены
			&$stat, // посчитанная статистика
			$calc_stat, // bool-flag, надо-ли считать статистику
			$stat_tag = null, //
			$allowed_rater_user_id_array_as_keys = array(), // массив вида ( 1=>true. 2=>true, 3=>true ) rater_user_id, оценки которых учитывать
			$export_marks_log = false, //
			&$marks_log = null //
	)
	{
		global $db;
		self::$db = $db;
		$db_result = $marks_ordered_by_ratee_user_id_competence_id_db_result;

		$ratee_user_id = 0;
		$competence_id = 0;
		$marks = array();
		$filter_raters = sizeof($allowed_rater_user_id_array_as_keys) > 0;
		while ($row = self::$db->fetch_array($db_result))
		{
			if ((int) $row["value"] === 0)
			{
				continue;
			}
			$rater_user_id_array_as_keys[$row["rater_user_id"]] = true;
			if ($filter_raters and !isset($allowed_rater_user_id_array_as_keys[$row["rater_user_id"]]))
			{
				continue;
			}
			$used_rater_user_id_array_as_keys[$row["rater_user_id"]] = true;
			$used_mark_count++;
			if ($export_marks_log)
			{
				$marks_log[] = array(
					"precedent_id" => (int) $row["precedent_id"],
					"competence_id" => (int) $row["competence_id"],
					"rater_user_id" => (int) $row["rater_user_id"],
					"type" => $row["type"],
					"value" => (int) $row["value"],
					"comment" => $row["comment"],
				);
			}
			if ($calc_stat)
			{
				if (!isset($stat[(int) $row["competence_id"]]))
				{
					$stat[(int) $row["competence_id"]] = array(
						"marks_p1" => 0,
						"marks_p2" => 0,
						"marks_p3" => 0,
						"marks_g1" => 0,
						"marks_g2" => 0,
						"marks_g3" => 0,
						"raters" => array(),
						"precedents" => array(),
						"stat_tags" => array(),
					);
				}
				$stat[(int) $row["competence_id"]]["marks_" . substr($row["type"], 0, 1) . $row["value"]]++;
				$stat[(int) $row["competence_id"]]["raters"][(int) $row["rater_user_id"]] = true;
				$stat[(int) $row["competence_id"]]["precedents"][(int) $row["precedent_id"]] = true;
				if ($stat_tag !== null)
				{
					$stat[(int) $row["competence_id"]]["stat_tags"][$stat_tag] = true;
				}
			}
			if ($row["ratee_user_id"] != $ratee_user_id or $row["competence_id"] != $competence_id)
			{
				if ($ratee_user_id)
				{
					call_user_func($callback, (int) $ratee_user_id, (int) $competence_id, $marks);
				}
				$marks = array();
			}
			$marks[] = array("type" => $row["type"], "value" => (int) $row["value"]);
			$ratee_user_id = $row["ratee_user_id"];
			$competence_id = $row["competence_id"];
		}
		if ($ratee_user_id)
		{
			call_user_func($callback, (int) $ratee_user_id, (int) $competence_id, $marks);
		}
	}

	public static function get_max_weighted_mark($marks)
	{
		$s1 = $s2 = $s3 = 0;
		foreach ($marks as $mark_data)
		{
			switch ($mark_data["type"])
			{
				case "personal": $mul = 2;
					break;
				case "group": $mul = 1;
					break;
				default: trigger_error("Unexpected mark type: {$mark_data["type"]}");
			}
			if ($mark_data["value"] == 1)
			{
				$s1 += $mul;
			}
			elseif ($mark_data["value"] == 2)
			{
				$s2 += $mul * 2;
			}
			elseif ($mark_data["value"] == 3)
			{
				$s3 += $mul * 3;
			}
			else
			{
				trigger_error("Unexpected mark value: {$mark_data["value"]}");
			}
		}
		if ($s3 >= $s2 and $s3 >= $s1)
		{
			return 3;
		}
		elseif ($s2 >= $s1)
		{
			return 2;
		}
		else
		{
			return 1;
		}
	}

}

?>