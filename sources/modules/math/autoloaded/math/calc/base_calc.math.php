<?php

abstract class base_calc_math
{

	/**
	 * @var db
	 */
	protected $db;
	protected $results;
	protected $used_mark_count;
	protected $stat;
	protected $calc_stat;
	protected $competence_set_id;
	protected $user_id_array;
	protected $project_key;
	protected $project_id;
	protected $rater_user_id_array_as_keys;
	protected $used_rater_user_id_array_as_keys;
	protected $competence_id_array = array();
	protected $allowed_rater_user_id_array_as_keys = array();
	protected $export_marks_log = false;
	protected $marks_log;

	public function __construct(&$results, &$used_mark_count, &$stat, $calc_stat, $competence_set_id, $user_id_array, $project_key, $project_id, &$rater_user_id_array_as_keys, &$used_rater_user_id_array_as_keys, $competence_id_array = array(), $allowed_rater_user_id_array_as_keys = array(), $export_marks_log = false, &$marks_log = null)
	{
		global $db;
		$this->db = $db;

		$this->results = &$results;
		$this->used_mark_count = &$used_mark_count;
		$this->stat = &$stat;
		$this->calc_stat = $calc_stat;
		$this->competence_set_id = $competence_set_id;
		$this->user_id_array = $user_id_array;
		if (!is_array($this->user_id_array) and !is_null($this->user_id_array))
		{
			trigger_error("\$user_id_array must be array or null! (Despite in the earlier versions int was allowed.)");
		}
		$this->project_key = $project_key;
		$this->project_id = $project_id;
		$this->rater_user_id_array_as_keys = &$rater_user_id_array_as_keys;
		$this->used_rater_user_id_array_as_keys = &$used_rater_user_id_array_as_keys;
		$this->competence_id_array = $competence_id_array;
		$this->allowed_rater_user_id_array_as_keys = $allowed_rater_user_id_array_as_keys;
		$this->export_marks_log = $export_marks_log;
		$this->marks_log = &$marks_log;
	}

	abstract public function run();
}

?>