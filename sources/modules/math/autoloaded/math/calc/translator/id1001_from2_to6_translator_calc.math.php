<?php

/**
 * Переводчик для competence.rf.ru
 * Набор компетенций ТвГУ (формулировки Солинга) ->
 * Набор компетенций ТвГУ (формулировки ФГОС)
 * Written by TindDae on 24/12/2012.
 */
class id1001_from2_to6_translator_calc_math extends base_and_or_translator_calc_math
{

	protected $trans_data_and = array(
		327 => array(37 => 3, 41 => 3),
		328 => array(115 => 3),
		329 => array(42 => 3),
		330 => array(45 => 3),
		331 => array(15 => 3),
		332 => array(105 => 3, 3 => 3),
		333 => array(104 => 3),
		334 => array(106 => 3),
		335 => array(103 => 3),
		336 => array(3 => 3),
		337 => array(111 => 3),
		338 => array(6 => 3, 70 => 3),
		339 => array(71 => 3),
		340 => array(3 => 3, 18 => 3),
		341 => array(67 => 3),
		342 => array(18 => 3, 27 => 3),
		343 => array(113 => 3),
		344 => array(14 => 3),
		346 => array(89 => 3),
		347 => array(21 => 3),
		348 => array(6 => 3, 13 => 3),
		349 => array(70 => 3, 7 => 3),
		350 => array(70 => 3),
		351 => array(73 => 3),
		352 => array(8 => 3),
		353 => array(26 => 3),
		354 => array(11 => 3, 5 => 3),
		355 => array(96 => 3),
		356 => array(11 => 3),
		357 => array(7 => 3),
		358 => array(68 => 3, 67 => 3),
		359 => array(33 => 3),
		360 => array(79 => 3, 17 => 3),
		362 => array(68 => 3, 67 => 3),
		369 => array(33 => 3),
		370 => array(6 => 3),
	);
	protected $trans_data_or = array(
		345 => array(25 => 3, 26 => 3),
		361 => array(15 => 3, 14 => 3, 7 => 3),
		363 => array(25 => 3, 26 => 3),
	);
	protected $translator_id = 1001;
	protected $competence_set_id_src = 2;
	protected $competence_set_id_target = 6;

}

?>