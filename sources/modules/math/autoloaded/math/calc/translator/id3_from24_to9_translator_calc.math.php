<?php

/**
 * This translator is written by Lena Kozharinova 22/10/2010 for the 'Team lead'
 * professiogramm.
 * It tests 2 types of translations:
 * 1) "and" translations - those where a resulting competence consists of some
 *    competences from another competence set, and the result is the minimum
 *    value of the competences involved. Weights (1, 2, 3) are not used here
 *    (because it not clear how to use them).
 * 2) "averaging" translations - those where a resulting competence consists
 *    of some competences from another competence set, and the result
 *    is the weighted average of the competences involved.
 */

class id3_from24_to9_translator_calc_math extends base_and_or_translator_calc_math
{

	protected $trans_data_and = array(
		370 => array(808 => 3, 809 => 3, 839 => 3, 834 => 3, 828 => 3),
		301 => array(808 => 3, 827 => 3, 839 => 3, 841 => 3, 828 => 3),
		299 => array(841 => 3, 843 => 3),
	);

	protected $trans_data_or = array(
		369 => array(816 => 3, 808 => 3, 830 => 3),
		371 => array(833 => 3),
		367 => array(830 => 3, 829 => 3),
		316 => array(826 => 3),
	);

	protected $translator_id = 3;

	protected $competence_set_id_src = 24;

	protected $competence_set_id_target = 9;

}

?>