<?php

/**
 * This translator is written by Lena Kozharinova 07/08/2010.
 * It tests "averaging" translations - those where a resulting competence
 * consists of some competences from another competence set, and the result
 * is the weighted average of the competences involved.
 * Weights are now 1, 2, 3, but in fact there is no special resiriction here.
 */

class id2_from24_to28_translator_calc_math extends base_translator_calc_math
{

	protected $trans_data_or = array(
		947 => array(830 => 3, 832 => 2, 820 => 2),
		948 => array(817 => 3, 827 => 3, 823 => 2, 826 => 1),
		949 => array(816 => 3),
		950 => array(834 => 3, 839 => 2, 840 => 3, 843 => 2),
		951 => array(851 => 2, 852 => 2),
		952 => array(853 => 2, 854 => 3, 856 => 2, 855 => 2),
		953 => array(858 => 2, 859 => 2, 860 => 3, 861 => 3),
		954 => array(945 => 3),
		955 => array(946 => 3),
		956 => array(944 => 3),
		957 => array(943 => 3),
	);

	protected $translator_id = 2;

	protected $competence_set_id_src = 24;

	protected $competence_set_id_target = 28;

	protected $results_src = array();

	public function check_trans_data()
	{
		$comeptence_id_array_src = array();
		$comeptence_id_array_target = array();
		foreach ($this->trans_data_or as $competence_id_target => $data)
		{
			$comeptence_id_array_target[] = $competence_id_target;
			$comeptence_id_array_src = array_merge($comeptence_id_array_src, array_keys($data));
		}
		$comeptence_id_array_src = array_unique($comeptence_id_array_src);
		if (!sizeof($comeptence_id_array_src))
		{
			return -1;
		}
		if (!sizeof($comeptence_id_array_target))
		{
			return -2;
		}
		$db_result = $this->db->sql("
			SELECT competence_id
			FROM competence_calc
			WHERE
				competence_id IN (" . join(",", $comeptence_id_array_src) . ")
				AND competence_set_id = {$this->competence_set_id_src}
		");
		if ($this->db->get_selected_row_count($db_result) != sizeof($comeptence_id_array_src))
		{
			return -3;
		}
		$db_result = $this->db->sql("
			SELECT competence_id
			FROM competence_calc
			WHERE
				competence_id IN (" . join(",", $comeptence_id_array_target) . ")
				AND competence_set_id = {$this->competence_set_id_target}
		");
		if ($this->db->get_selected_row_count($db_result) != sizeof($comeptence_id_array_target))
		{
			return -4;
		}
		return 0;
	}

	public function run()
	{
		if (is_array($this->user_id_array) and !sizeof($this->user_id_array))
		{
			return; // array() means that no results are required (no users were selected)
		}
		
		// Finding competence_sets
		$competence_id_array_target = array();
		$competence_id_array_src = array();
		if (sizeof($this->competence_id_array))
		{
			foreach ($this->competence_id_array as $competence_id)
			{
				if (isset($this->trans_data_or[$competence_id]))
				{
					$competence_id_array_target[] = $competence_id;
					$competence_id_array_src = array_merge($competence_id_array_src, array_keys($this->trans_data_or[$competence_id]));
				}
			}
		}
		else
		{
			foreach ($this->trans_data_or as $competence_id_target => $data_src)
			{
				$competence_id_array_target[] = $competence_id_target;
				$competence_id_array_src = array_merge($competence_id_array_src, array_keys($data_src));
			}
		}
		if (!sizeof($competence_id_array_src))
		{
			return;
		}
		// Building SQL
		$sql_competence_id_where = sizeof($competence_id_array_src) ? "AND competence_id IN (" . join(",", $competence_id_array_src) . ")" : "";
		$sql_user_where = is_array($this->user_id_array) ? "AND ratee_user_id IN (" . join(",", $this->user_id_array) . ")" : "";
		$db_result = $this->db->sql("
			SELECT
				ratee_user_id, rater_user_id, competence_id, type, value
			FROM mark_calc
			WHERE competence_set_id = {$this->competence_set_id_src} AND project_id = {$this->project_id}
			{$sql_competence_id_where}
			{$sql_user_where}
			ORDER BY ratee_user_id, competence_id
		");
		// Fetching results of the source set
		calc_helper_math::call_for_every_user_and_competence($db_result, array($this, "_callback"), $this->rater_user_id_array_as_keys, $this->used_rater_user_id_array_as_keys, $this->used_mark_count, $this->stat, false, "", $this->allowed_rater_user_id_array_as_keys);
		// Building results in the new set
		foreach ($this->results_src as $ratee_user_id => $ratee_data)
		{
			foreach ($competence_id_array_target as $competence_id_target)
			{
				$numerator = 0;
				$denominator = 0;
				$mark_exist = false;
				$competence_id_array_src = $this->trans_data_or[$competence_id_target];
				foreach ($competence_id_array_src as $competence_id_src => $weight)
				{
					$denominator += $weight;
					if (isset($ratee_data[$this->project_key][(int)$competence_id_src]))
					{
						$numerator += $ratee_data[$this->project_key][(int)$competence_id_src] * $weight;
						$mark_exist = true;
					}
				}
				if ($mark_exist)
				{
					if (!isset($this->results_src[$ratee_user_id]))
					{
						$this->results_src[$ratee_user_id] = array();
					}
					if (!isset($this->results_src[$ratee_user_id][$this->project_key]))
					{
						$this->results_src[$ratee_user_id][$this->project_key] = array();
					}
					$this->results[$ratee_user_id][$this->project_key][(int)$competence_id_target] = $denominator > 0 ? (int)round($numerator / $denominator) : 0;
				}
			}
		}
	}

	public function _callback($ratee_user_id, $competence_id, $marks)
	{
		if (!isset($this->results_src[$ratee_user_id]))
		{
			$this->results_src[$ratee_user_id] = array();
		}
		if (!isset($this->results_src[$ratee_user_id][$this->project_key]))
		{
			$this->results_src[$ratee_user_id][$this->project_key] = array();
		}
		$this->results_src[$ratee_user_id][$this->project_key][$competence_id] = calc_helper_math::get_max_weighted_mark($marks);
	}

}

?>