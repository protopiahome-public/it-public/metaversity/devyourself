<?php

/**
 * The tranlstor hadles 2 types of translations:
 * 1) "and" translations - those where a resulting competence consists of some
 *    competences from another competence set, and the result is the minimum
 *    value of the competences involved. Weights (1, 2, 3) are not used here
 *    (because it not clear how to use them).
 * 2) "averaging" translations - those where a resulting competence consists
 *    of some competences from another competence set, and the result
 *    is the weighted average of the competences involved.
 */
class base_and_or_translator_calc_math extends base_translator_calc_math
{
	/* Variables to redefine in the ancestor */

	protected $trans_data_and = array();
	protected $trans_data_or = array();
	protected $translator_id = null;
	protected $competence_set_id_src = null;
	protected $competence_set_id_target = null;

	/* Internal variables */
	private $results_src = array(); // Results in terms of the sourse competence set

	public function check_trans_data()
	{
		$comeptence_id_array_src = array();
		$comeptence_id_array_target = array();
		foreach ($this->trans_data_and as $competence_id_target => $data)
		{
			$comeptence_id_array_target[] = $competence_id_target;
			$comeptence_id_array_src = array_merge($comeptence_id_array_src, array_keys($data));
		}
		foreach ($this->trans_data_or as $competence_id_target => $data)
		{
			$comeptence_id_array_target[] = $competence_id_target;
			$comeptence_id_array_src = array_merge($comeptence_id_array_src, array_keys($data));
		}
		$comeptence_id_array_src = array_unique($comeptence_id_array_src);
		if (!sizeof($comeptence_id_array_src))
		{
			return -1;
		}
		if (!sizeof($comeptence_id_array_target))
		{
			return -2;
		}
		$db_result = $this->db->sql("
			SELECT competence_id
			FROM competence_calc
			WHERE
				competence_id IN (" . join(",", $comeptence_id_array_src) . ")
				AND competence_set_id = {$this->competence_set_id_src}
		");
		if ($this->db->get_selected_row_count($db_result) != sizeof($comeptence_id_array_src))
		{
			return -3;
		}
		$db_result = $this->db->sql("
			SELECT competence_id
			FROM competence_calc
			WHERE
				competence_id IN (" . join(",", $comeptence_id_array_target) . ")
				AND competence_set_id = {$this->competence_set_id_target}
		");
		if ($this->db->get_selected_row_count($db_result) != sizeof($comeptence_id_array_target))
		{
			return -4;
		}
		return 0;
	}

	public function run()
	{
		if (is_array($this->user_id_array) and !sizeof($this->user_id_array))
		{
			return; // array() means that no results are required (no users were selected)
		}

		// Finding competence_sets
		$competence_id_array_target = array();
		$competence_id_array_src = array();
		if (sizeof($this->competence_id_array))
		{
			foreach ($this->competence_id_array as $competence_id)
			{
				if (isset($this->trans_data_and[$competence_id]))
				{
					$competence_id_array_target[] = $competence_id;
					$competence_id_array_src = array_merge($competence_id_array_src, array_keys($this->trans_data_and[$competence_id]));
				}
				elseif (isset($this->trans_data_or[$competence_id]))
				{
					$competence_id_array_target[] = $competence_id;
					$competence_id_array_src = array_merge($competence_id_array_src, array_keys($this->trans_data_or[$competence_id]));
				}
			}
		}
		else
		{
			foreach ($this->trans_data_and as $competence_id_target => $data_src)
			{
				$competence_id_array_target[] = $competence_id_target;
				$competence_id_array_src = array_merge($competence_id_array_src, array_keys($data_src));
			}
			foreach ($this->trans_data_or as $competence_id_target => $data_src)
			{
				$competence_id_array_target[] = $competence_id_target;
				$competence_id_array_src = array_merge($competence_id_array_src, array_keys($data_src));
			}
		}
		if (!sizeof($competence_id_array_src))
		{
			return;
		}
		// Building SQL
		$sql_competence_id_where = sizeof($competence_id_array_src) ? "AND competence_id IN (" . join(",", $competence_id_array_src) . ")" : "";
		$sql_user_where = is_array($this->user_id_array) ? "AND ratee_user_id IN (" . join(",", $this->user_id_array) . ")" : "";
		$sql_stat_fields = ($this->calc_stat or $this->export_marks_log) ? ", rater_user_id, precedent_id" : "";
		$sql_marks_export_fields = $this->export_marks_log ? ", comment" : "";
		$db_result = $this->db->sql("
			SELECT
				ratee_user_id, rater_user_id, competence_id, type, value {$sql_stat_fields} {$sql_marks_export_fields}
			FROM mark_calc
			WHERE competence_set_id = {$this->competence_set_id_src} AND project_id = {$this->project_id}
			{$sql_competence_id_where}
			{$sql_user_where}
			ORDER BY ratee_user_id, competence_id
		");
		// Fetching results of the source set
		$small_marks_log = array();
		calc_helper_math::call_for_every_user_and_competence($db_result, array($this, "_callback"), $this->rater_user_id_array_as_keys, $this->used_rater_user_id_array_as_keys, $this->used_mark_count, $this->stat, $this->calc_stat, "translator_tmp", $this->allowed_rater_user_id_array_as_keys, $this->export_marks_log, $small_marks_log);
		// Building results in the new set
		foreach ($this->results_src as $ratee_user_id => $ratee_data)
		{
			foreach ($competence_id_array_target as $competence_id_target)
			{
				$end_mark = null;
				$competence_id_array_src = null;
				if ($this->export_marks_log)
				{
					$export_src_competence_id_array_as_keys = array();
					$translation_type = null;
				}
				if (isset($this->trans_data_and[$competence_id_target]))
				{
					if ($this->export_marks_log)
					{
						$translation_type = "MIN";
					}
					$min = 3;
					$all_marks_exist = true;
					$competence_id_array_src = $this->trans_data_and[$competence_id_target];
					foreach ($competence_id_array_src as $competence_id_src => $weight)
					{
						if (isset($ratee_data[$this->project_key][(int) $competence_id_src]))
						{
							$new_mark = $ratee_data[$this->project_key][(int) $competence_id_src];
							if ($new_mark < $min)
							{
								$min = $new_mark;
							}
						}
						else
						{
							$all_marks_exist = false;
							break;
						}
					}
					if ($all_marks_exist)
					{
						if (!isset($this->results_src[$ratee_user_id]))
						{
							$this->results_src[$ratee_user_id] = array();
						}
						if (!isset($this->results_src[$ratee_user_id][$this->project_key]))
						{
							$this->results_src[$ratee_user_id][$this->project_key] = array();
						}
						$end_mark = $min;
						$this->results[$ratee_user_id][$this->project_key][(int) $competence_id_target] = $end_mark;
					}
				}
				else
				{
					if ($this->export_marks_log)
					{
						$translation_type = "AVG";
					}
					$numerator = 0;
					$denominator = 0;
					$mark_exists = false;
					$competence_id_array_src = $this->trans_data_or[$competence_id_target];
					foreach ($competence_id_array_src as $competence_id_src => $weight)
					{
						$denominator += $weight;
						if (isset($ratee_data[$this->project_key][(int) $competence_id_src]))
						{
							$numerator += $ratee_data[$this->project_key][(int) $competence_id_src] * $weight;
							$mark_exists = true;
						}
					}
					if ($mark_exists)
					{
						if (!isset($this->results_src[$ratee_user_id]))
						{
							$this->results_src[$ratee_user_id] = array();
						}
						if (!isset($this->results_src[$ratee_user_id][$this->project_key]))
						{
							$this->results_src[$ratee_user_id][$this->project_key] = array();
						}
						$end_mark = $denominator > 0 ? (int) round($numerator / $denominator) : 0;
						$this->results[$ratee_user_id][$this->project_key][(int) $competence_id_target] = $end_mark;
					}
				}
				if ($end_mark and $this->export_marks_log)
				{
					$export_src_competence_id_array_as_keys[$competence_id_target] = true;
				}
				if ($end_mark /* not null and > 0 (!) */ and $this->calc_stat)
				{
					if (!isset($this->stat[$competence_id_target]))
					{
						$this->stat[$competence_id_target] = array(
							"marks_p1" => 0,
							"marks_p2" => 0,
							"marks_p3" => 0,
							"marks_g1" => 0,
							"marks_g2" => 0,
							"marks_g3" => 0,
							"raters" => array(),
							"precedents" => array(),
							"stat_tags" => array(),
						);
					}
					$this->stat[$competence_id_target]["stat_tags"]["translator"] = true;
					foreach ($competence_id_array_src as $competence_id_src => $weight)
					{
						if (isset($this->stat[$competence_id_src]))
						{
							$this->stat[$competence_id_target]["marks_p1"] += $this->stat[$competence_id_src]["marks_p1"];
							$this->stat[$competence_id_target]["marks_p2"] += $this->stat[$competence_id_src]["marks_p2"];
							$this->stat[$competence_id_target]["marks_p3"] += $this->stat[$competence_id_src]["marks_p3"];
							$this->stat[$competence_id_target]["marks_g1"] += $this->stat[$competence_id_src]["marks_g1"];
							$this->stat[$competence_id_target]["marks_g2"] += $this->stat[$competence_id_src]["marks_g2"];
							$this->stat[$competence_id_target]["marks_g3"] += $this->stat[$competence_id_src]["marks_g3"];
							foreach ($this->stat[$competence_id_src]["raters"] as $idx => $val)
							{
								$this->stat[$competence_id_target]["raters"][$idx] = true;
							}
							foreach ($this->stat[$competence_id_src]["precedents"] as $idx => $val)
							{
								$this->stat[$competence_id_target]["precedents"][$idx] = true;
							}
						}
					}
				}
				if ($this->export_marks_log)
				{
					if (!isset($this->marks_log[$this->project_key]))
					{
						$this->marks_log[$this->project_key] = array(
							"project_id" => $this->project_id,
							"competence_set_id" => $this->competence_set_id,
							"src_competence_set_id" => $this->competence_set_id_src,
							"translator_id" => $this->translator_id,
							"competences" => array(),
						);
					}
					if ($end_mark)
					{
						$p = &$this->marks_log[$this->project_key];
						$p["competences"][$competence_id_target] = array(
							"translation_type" => $translation_type,
							"src_competences" => array(),
							"marks" => array(),
						);
						foreach ($competence_id_array_src as $competence_id_src => $weight)
						{
							$p["competences"][$competence_id_target]["src_competences"][$competence_id_src] = array(
								"mark" => isset($ratee_data[$this->project_key][(int) $competence_id_src]) ? $ratee_data[$this->project_key][(int) $competence_id_src] : 0,
								"weight" => $weight,
							);
						}
						foreach ($small_marks_log as $export_data)
						{
							if (isset($competence_id_array_src[$export_data["competence_id"]]))
							{
								$p["competences"][$competence_id_target]["marks"][] = $export_data;
							}
						}
					}
				}
			}
		}
		/* Cleaning temporary statistics records */
		if ($this->calc_stat)
		{
			foreach ($this->stat as $competence_id => $stat_data)
			{
				if (isset($stat_data["stat_tags"]["translator_tmp"]))
				{
					unset($this->stat[$competence_id]);
				}
			}
		}
	}

	public function _callback($ratee_user_id, $competence_id, $marks)
	{
		if (!isset($this->results_src[$ratee_user_id]))
		{
			$this->results_src[$ratee_user_id] = array();
		}
		if (!isset($this->results_src[$ratee_user_id][$this->project_key]))
		{
			$this->results_src[$ratee_user_id][$this->project_key] = array();
		}
		$this->results_src[$ratee_user_id][$this->project_key][$competence_id] = calc_helper_math::get_max_weighted_mark($marks);
	}

}

?>