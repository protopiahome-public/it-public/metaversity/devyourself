<?php

/**
 * This translator into the "World of professions - 2030" competence set
 * is written by TindDae on 18/02/2011.
 */
class id8_from30_to33_translator_calc_math extends base_and_or_translator_calc_math
{

	protected $trans_data_and = array(
		1374 => array(1024 => 3, 1014 => 3),
		1375 => array(981 => 3, 1057 => 3),
		1377 => array(1040 => 3, 1030 => 3),
		1378 => array(1030 => 3),
		1379 => array(1057 => 3, 974 => 3, 979 => 3),
		1380 => array(971 => 3, 1057 => 3, 979 => 3),
		1381 => array(1011 => 3, 1009 => 3),
		1382 => array(1057 => 3, 980 => 3),
		1383 => array(1057 => 3, 981 => 3),
		1384 => array(1037 => 3, 1053 => 3, 1057 => 3),
		1386 => array(1029 => 3, 981 => 3),
		1389 => array(974 => 3, 991 => 3, 1057 => 3),
		1390 => array(983 => 3, 1030 => 3),
		1392 => array(978 => 3, 1037 => 3),
		1394 => array(1037 => 3, 1025 => 3, 1035 => 3),
		1395 => array(980 => 3, 1014 => 3, 1024 => 3),
		1396 => array(1024 => 3, 1040 => 3, 991 => 3),
		1397 => array(969 => 3, 1042 => 3),
		1399 => array(1046 => 3, 1033 => 3),
		1402 => array(1034 => 3),
		1403 => array(1035 => 3),
		1404 => array(1045 => 3, 1029 => 3),
		1405 => array(1047 => 3),
		1406 => array(1025 => 3),
		1407 => array(1014 => 3),
		1409 => array(1042 => 3),
		1410 => array(1045 => 3),
		1413 => array(1044 => 3),
		1414 => array(1046 => 3),
		1415 => array(1033 => 3),
		1416 => array(1025 => 3, 980 => 3),
		1417 => array(1041 => 3),
		1420 => array(1048 => 3),
		1421 => array(1043 => 3),
		1423 => array(1031 => 3),
		1425 => array(977 => 3),
		1429 => array(1054 => 3),
		1431 => array(969 => 3,),
		1433 => array(979 => 3),
		1434 => array(1039 => 3),
		1437 => array(1043 => 3),
	);
	protected $trans_data_or = array(
		1373 => array(1057 => 3, 978 => 3, 979 => 3),
		1388 => array(974 => 3, 1037 => 3, 1040 => 3),
		1418 => array(978 => 3, 979 => 3),
		1426 => array(971 => 3, 972 => 3),
		1428 => array(971 => 3, 972 => 3),
	);
	protected $translator_id = 8;
	protected $competence_set_id_src = 30;
	protected $competence_set_id_target = 33;

}

?>