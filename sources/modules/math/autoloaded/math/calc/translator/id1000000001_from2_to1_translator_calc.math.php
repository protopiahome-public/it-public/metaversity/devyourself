<?php

/**
 * Just a test translator. Used in results_math.test.php
 */

class id1000000001_from2_to1_translator_calc_math extends base_translator_calc_math
{

	public function run()
	{
		if (is_array($this->user_id_array) and !sizeof($this->user_id_array))
		{
			return; // array() means that no results are required (no users were selected)
		}
		
		$sql_competence_id_where = sizeof($this->competence_id_array) ? "AND competence_id IN (" . join(",", $this->competence_id_array) . ")" : "";
		$sql_user_where = is_array($this->user_id_array) ? "AND ratee_user_id IN (" . join(",", $this->user_id_array) . ")" : "";
		$db_result = $this->db->sql("
			SELECT
				ratee_user_id, rater_user_id, competence_id - 100 as competence_id, type, value
			FROM mark_calc
			WHERE competence_set_id = 2 AND project_id = {$this->project_id}
			{$sql_competence_id_where}
			{$sql_user_where}
			ORDER BY ratee_user_id, competence_id
		");
		calc_helper_math::call_for_every_user_and_competence($db_result, array($this, "_callback"), $this->rater_user_id_array_as_keys, $this->used_rater_user_id_array_as_keys, $this->used_mark_count, $this->stat, $this->calc_stat, "translator", $this->allowed_rater_user_id_array_as_keys);
	}

	public function _callback($ratee_user_id, $competence_id, $marks)
	{
		if (!isset($this->results[$ratee_user_id]))
		{
			$this->results[$ratee_user_id] = array();
		}
		if (!isset($this->results[$ratee_user_id][$this->project_key]))
		{
			$this->results[$ratee_user_id][$this->project_key] = array();
		}
		$this->results[$ratee_user_id][$this->project_key][$competence_id] = calc_helper_math::get_max_weighted_mark($marks);
	}

}

?>