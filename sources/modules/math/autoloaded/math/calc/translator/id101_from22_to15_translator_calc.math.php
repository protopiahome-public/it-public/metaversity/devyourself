<?php

/**
 * Just a test translator.
 */

class id101_from22_to15_translator_calc_math extends base_translator_calc_math
{

	public function run()
	{
		$r = array();
		foreach ($this->results as $ratee_user_id => $ratee_data)
		{
			foreach ($ratee_data as $key => $competences)
			{
				foreach ($competences as $competence_id => $tmp)
				{
					if (!isset($r[$competence_id]))
					{
						$r[$competence_id] = 2;
					}
				}
			}
		}
		foreach ($this->results as $ratee_user_id => &$ratee_data)
		{
			$ratee_data[$this->project_key] = $r;
		}
	}

}

?>