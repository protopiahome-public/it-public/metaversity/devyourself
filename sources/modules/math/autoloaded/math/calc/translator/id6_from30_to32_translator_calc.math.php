<?php

/**
 * This translator is written by Tind Dae on 06/12/2010 for the metagame
 * http://nuclear-sprint.ru/.
 */
class id6_from30_to32_translator_calc_math extends base_and_or_translator_calc_math
{

	protected $trans_data_and = array(
		1293 => array(981 => 3, 983 => 3),
		1294 => array(1030 => 3),
		1295 => array(1036 => 3),
		1296 => array(1034 => 3, 1031 => 3),
		1297 => array(1049 => 3),
		1298 => array(1050 => 3),
		1299 => array(985 => 3),
		1300 => array(1051 => 3),
		1301 => array(1037 => 3),
		1302 => array(1052 => 3),
		1303 => array(1053 => 3),
		1304 => array(1054 => 3),
		1305 => array(1031 => 3),
		1306 => array(1037 => 3),
	);
	protected $trans_data_or = array(
	);
	protected $translator_id = 6;
	protected $competence_set_id_src = 30;
	protected $competence_set_id_target = 32;

}

?>