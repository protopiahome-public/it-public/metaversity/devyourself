<?php

class filter_raters_real_math
{

	/**
	 * @var db
	 */
	private $db;

	private $allowed_user_group_id_array_as_keys;

	private $rater_user_id_array_as_keys;

	private $user_groups = array();

	private $total_rater_count;

	private $not_grouped_rater_count = 0;

	private $select_all_checked = true;

	public function __construct($allowed_user_group_id_array_as_keys, $rater_user_id_array_as_keys)
	{
		global $db;
		$this->db = $db;

		$this->allowed_user_group_id_array_as_keys = $allowed_user_group_id_array_as_keys;
		$this->rater_user_id_array_as_keys = $rater_user_id_array_as_keys;

		$this->build_filter();
	}

	public function get_user_groups()
	{
		return $this->user_groups;
	}

	public function get_total_rater_count()
	{
		return $this->total_rater_count;
	}

	public function get_not_grouped_rater_count()
	{
		return $this->not_grouped_rater_count;
	}

	public function is_select_all_checked()
	{
		return $this->select_all_checked;
	}

	private function build_filter()
	{
		// raters count
		$this->total_rater_count = sizeof($this->rater_user_id_array_as_keys);
		if ($this->total_rater_count == 0)
		{
			return;
		}
		$rater_user_id_array = array_keys($this->rater_user_id_array_as_keys);

		// Selecting groups from DB
		$link_db_result = $this->db->sql("
			SELECT l.user_group_id, l.user_id, ug.title as user_group_title
			FROM user_user_group_link l
			LEFT JOIN user_group ug ON ug.id = l.user_group_id
			WHERE l.user_id IN (" . join(",", $rater_user_id_array) . ")
		");
		$raters_real = array();
		while ($link_row = $this->db->fetch_array($link_db_result))
		{
			$raters_real[(int)$link_row["user_id"]] = true;
			if (!isset($this->user_groups[(int)$link_row["user_group_id"]]))
			{
				$this->user_groups[(int)$link_row["user_group_id"]] = array(
					"title" => $link_row["user_group_title"],
					"user_count" => 1
				);
			}
			else
			{
				$this->user_groups[(int)$link_row["user_group_id"]]["user_count"]++;
			}
		}
		
		// User selection
		$this->select_all_checked = sizeof($this->allowed_user_group_id_array_as_keys) == 0;
		if ($this->select_all_checked)
		{
			foreach ($this->user_groups as $user_group_id => &$data)
			{
				$data["is_selected"] = true;
			}
		}
		else
		{
			foreach ($this->allowed_user_group_id_array_as_keys as $user_group_id => $tmp)
			{
				if (isset($this->user_groups[$user_group_id]))
				{
					$this->user_groups[$user_group_id]["is_selected"] = true;
				}
			}
		}

		// Calculating users which are not attached to any group
		$raters_not_grouped = array_diff($rater_user_id_array, array_keys($raters_real));
		$this->not_grouped_rater_count = sizeof($raters_not_grouped);
	}

}

?>