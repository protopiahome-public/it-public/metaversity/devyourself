<?php

class filter_main_math
{

	/**
	 * @var db
	 */
	private $db;
	private $competence_set_id;
	private $user_id_array = null;
	private $project_id = null;
	private $filter = array();
	private $selected_projects = array();

	/**
	 * @param id $competence_set_id - it's clear
	 * @param array $user_id_array - if specified, mark count for wach project
	 *   is calculated for these users.
	 * @param bool $ignore_user_selection - ignore $_GET
	 * @param bool $project_id - show results for this project only (by default).
	 */
	public function __construct($competence_set_id, $user_id_array = null, $ignore_user_selection = false, $project_id = null)
	{
		global $db;
		$this->db = $db;

		$this->competence_set_id = (int) $competence_set_id;
		$this->user_id_array = $user_id_array;
		$this->project_id = $project_id;
		if (!is_array($this->user_id_array) and !is_null($this->user_id_array))
		{
			trigger_error("\$user_id_array must be array or null! (Despite in the earlier versions int was allowed.)");
		}

		$this->build_filter();
		$this->analyse_selection($ignore_user_selection);
	}

	public function get_competence_set_id()
	{
		return $this->competence_set_id;
	}

	public function get_filter()
	{
		return $this->filter;
	}

	public function get_selected_projects()
	{
		return $this->selected_projects;
	}

	private function build_filter()
	{
		if (is_array($this->user_id_array) and !sizeof($this->user_id_array))
		{
			return; // array() means that no results are required (no users were selected)
		}

		$translators_db_result = $this->db->sql("
			SELECT
				ct.id as translator_id,
				ct.title as translator_title,
				ct.from_competence_set_id,
				cs.title as from_competence_set_title,
				ct.is_default
			FROM competence_translator ct
			LEFT JOIN competence_set cs ON cs.id = ct.from_competence_set_id
			WHERE ct.to_competence_set_id = {$this->competence_set_id}
		", "from_competence_set_id");
		$translators = array();
		while ($translators_row = $this->db->fetch_array($translators_db_result))
		{
			if (!isset($translators[(int) $translators_row["from_competence_set_id"]]))
			{
				$translators[(int) $translators_row["from_competence_set_id"]] = array();
			}
			$translators_row["translator_id"] = (int) $translators_row["translator_id"];
			$translators_row["from_competence_set_id"] = (int) $translators_row["from_competence_set_id"];
			$translators_row["is_default"] = (int) $translators_row["is_default"];
			$translators[(int) $translators_row["from_competence_set_id"]][$translators_row["translator_id"]] = $translators_row;
		}

		$competence_set_id_array = array_keys($translators);
		$competence_set_id_array[] = $this->competence_set_id;

		if (is_null($this->user_id_array))
		{
			$db_result = $this->db->sql("
				SELECT l.competence_set_id, l.project_id, p.title as project_title, l.total_mark_count_calc
				FROM project_competence_set_link_calc l
				LEFT JOIN project p ON p.id = l.project_id
				WHERE 
					l.competence_set_id IN (" . join(",", $competence_set_id_array) . ")
				ORDER BY l.total_mark_count_calc DESC
			");
		}
		else
		{
			$db_result = $this->db->sql("
				SELECT l.competence_set_id, l.project_id, p.title as project_title, SUM(l.total_mark_count_calc) as total_mark_count_calc
				FROM user_competence_set_project_link l
				LEFT JOIN project p ON p.id = l.project_id
				WHERE
					l.user_id IN (" . join(",", $this->user_id_array) . ") AND
					l.competence_set_id IN (" . join(",", $competence_set_id_array) . ")
				GROUP BY l.competence_set_id, l.project_id
				HAVING total_mark_count_calc > 0
				ORDER BY l.total_mark_count_calc DESC
			");
		}

		// Projects without translators should always be on top
		$this->filter = array($this->competence_set_id => false);

		while ($row = $this->db->fetch_array($db_result))
		{
			$competence_set_id = (int) $row["competence_set_id"];
			if ($competence_set_id == $this->competence_set_id)
			{
				if (!$this->filter[$competence_set_id])
				{
					$this->filter[$competence_set_id] = array(
						"projects" => array(),
					);
				}
			}
			else
			{
				if (!isset($this->filter[$competence_set_id]))
				{
					$this->filter[$competence_set_id] = array(
						"translators" => &$translators[(int) $row["competence_set_id"]],
						"projects" => array(),
					);
				}
			}

			$this->filter[$competence_set_id]["projects"][(int) $row["project_id"]] = array(
				"title" => $row["project_title"],
				"total_mark_count" => (int) $row["total_mark_count_calc"],
			);
		}

		if (!$this->filter[$this->competence_set_id])
		{
			unset($this->filter[$this->competence_set_id]);
		}
	}

	private function analyse_selection($ignore_user_selection)
	{
		$selection_exists = false;
		if (!$ignore_user_selection)
		{
			foreach ($this->filter as $competence_set_id => &$competence_set_data)
			{
				$translator_id = 0;
				if ($competence_set_id !== $this->competence_set_id)
				{
					$translator_id = isset($_GET["translator_" . $competence_set_id]) ? $_GET["translator_" . $competence_set_id] : "";
					if (!is_good_id($translator_id))
					{
						continue;
					}
					$translator_id = (int) $translator_id;
					if (!isset($competence_set_data["translators"][$translator_id]))
					{
						continue;
					}
					$competence_set_data["translators"][$translator_id]["is_selected"] = true;
				}
				foreach ($competence_set_data["projects"] as $project_id => &$project_data)
				{
					$key = "project_" . $competence_set_id . "_" . $project_id;
					if (isset($_GET[$key]))
					{
						$data = array();
						$data["project_id"] = $project_id;
						$data["competence_set_id"] = $competence_set_id == $this->competence_set_id ? $this->competence_set_id : $competence_set_data["translators"][$translator_id]["from_competence_set_id"];
						$data["translator_id"] = $competence_set_id == $this->competence_set_id ? 0 : $translator_id;
						$this->selected_projects[$key] = $data;
						$project_data["is_selected"] = true;
						$selection_exists = true;
					}
				}
			}
		}
		if (!$selection_exists)
		{
			foreach ($this->filter as $competence_set_id => &$competence_set_data)
			{
				if ($competence_set_id == $this->competence_set_id)
				{
					foreach ($competence_set_data["projects"] as $project_id => &$project_data)
					{
						if (is_null($this->project_id) or $this->project_id == $project_id)
						{
							$project_data["is_selected"] = true;
							$key = "project_" . $this->competence_set_id . "_" . $project_id;
							$data = array();
							$data["project_id"] = $project_id;
							$data["competence_set_id"] = $this->competence_set_id;
							$data["translator_id"] = 0;
							$this->selected_projects[$key] = $data;
						}
					}
				}
				else
				{
					$default_translator_id = null;
					foreach ($competence_set_data["translators"] as $translator_id => &$translator_data)
					{
						if ($translator_data["is_default"] === 1)
						{
							$translator_data["is_selected"] = true;
							$default_translator_id = $translator_id;
							break;
						}
					}
					if ($default_translator_id)
					{
						foreach ($competence_set_data["projects"] as $project_id => &$project_data)
						{
							if (is_null($this->project_id) or $this->project_id == $project_id)
							{
								$project_data["is_selected"] = true;
								$key = "project_" . $competence_set_id . "_" . $project_id;
								$data = array();
								$data["project_id"] = $project_id;
								$data["competence_set_id"] = $competence_set_id;
								$data["translator_id"] = $translator_id;
								$this->selected_projects[$key] = $data;
							}
						}
					}
				}
			}
		}
	}

}

?>