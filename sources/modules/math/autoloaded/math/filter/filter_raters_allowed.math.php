<?php
/*

на основе GET параметров rater_user_groups_all и rater_user_groups (! не более 50 групп)
добывает списки групп и наблюдателей, оценки которых требуются

->get_allowed_user_group_id_array_as_keys()
	возвращает список group_id наблюдателей в виде array( 1=>true. 2=>true, 3=>true )

->public function get_allowed_rater_user_id_array()
	возвращает список user_id наблюдателей в виде array( 1, 2, 3 )

->get_allowed_rater_user_id_array_as_keys()
	возвращает список user_id наблюдателей в виде array( 1=>true. 2=>true, 3=>true )

*/
class filter_raters_allowed_math
{
	/**
	 * @var db
	 */
	private $db;

	private $allowed_user_group_id_array_as_keys = array();

	private $allowed_rater_user_id_array = array();

	private $allowed_rater_user_id_array_as_keys = array();

	public function __construct()
	{
		global $db;
		$this->db = $db;
		
		if (!isset($_GET["rater_user_groups_all"]))
		{
			$i = 0;
			if (isset($_GET["rater_user_groups"]) and is_array($_GET["rater_user_groups"]))
			{
				foreach ($_GET["rater_user_groups"] as $user_group_id)
				{
					if (is_good_id($user_group_id))
					{
						$this->allowed_user_group_id_array_as_keys[(int)$user_group_id] = true;
						$i++;
						if ($i >= 50)
						{
							break;
						}
					}
				}
			}
		}
		if (sizeof($this->allowed_user_group_id_array_as_keys))
		{
			$this->allowed_rater_user_id_array = $this->db->fetch_column_values("SELECT DISTINCT user_id FROM user_user_group_link WHERE user_group_id IN (" . join(",", array_keys($this->allowed_user_group_id_array_as_keys)) . ")");
			foreach ($this->allowed_rater_user_id_array as $idx => &$val)
			{
				$val = (int)$val;
				$this->allowed_rater_user_id_array_as_keys[$val] = true;
			}
		}
	}

	public function get_allowed_user_group_id_array_as_keys()
	{
		return $this->allowed_user_group_id_array_as_keys;
	}

	public function get_allowed_rater_user_id_array()
	{
		return $this->allowed_rater_user_id_array;
	}

	public function get_allowed_rater_user_id_array_as_keys()
	{
		return $this->allowed_rater_user_id_array_as_keys;
	}

}

?>