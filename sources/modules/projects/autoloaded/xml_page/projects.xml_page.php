<?php

class projects_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "project_short"
		),
	);
	protected $xml_row_name = "project";
	// Module specific
	protected $page;

	/**
	 * @var sql_filters_easy_processor
	 */
	private $sql_filters_easy_processor;

	public function __construct($page)
	{
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 25));

		$processor = new sort_easy_processor();
		$processor->add_order("date", "start_time DESC", "start_time ASC");
		$processor->add_order("site", "main_url_human_calc ASC", "main_url_human_calc DESC");
		$processor->add_order("users", "user_count_calc DESC", "user_count_calc ASC");
		$processor->add_order("precedents", "precedent_count_calc DESC", "precedent_count_calc ASC");
		$processor->add_order("marks", "total_mark_count_calc DESC", "total_mark_count_calc ASC");
		$this->add_easy_processor($processor);

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new text_sql_filter("search", "проект", array("title", "main_url")));
		$processor->add_sql_filter(new checkbox_sql_filter("hide_no_marks", "Скрывать проекты без оценок", true, "total_mark_count_calc > 0"));
		$this->add_easy_processor($processor);
		$this->sql_filters_easy_processor = $processor;
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("project");
		$select_sql->add_select_fields("id, user_count_calc");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
		if (empty($this->data))
		{
			$sql_filter = $this->sql_filters_easy_processor->get_filter_by_name("hide_no_marks");
			if (!$sql_filter->is_active())
			{
				$this->sql_filters_easy_processor->remove_filter("hide_no_marks");
				$select_sql = $this->get_basic_select_sql();
				$select_sql->add_from("project");
				$select_sql->add_select_fields("id");
				$this->data = $this->db->fetch_all($select_sql->get_sql());
			}
		}
	}

	protected function append_xml()
	{
		$project_id_array = $this->get_selected_ids();
		$xdom = xdom::create("project_competence_set_links");
		if (sizeof($project_id_array))
		{
			$db_result = $this->db->sql("
				SELECT
					l.project_id, l.competence_set_id, cs.title as competence_set_title,
					l.personal_mark_count_calc, l.group_mark_count_calc, l.group_mark_raw_count_calc, l.total_mark_count_calc
				FROM project_competence_set_link_calc l
				LEFT JOIN competence_set cs ON cs.id = l.competence_set_id
				WHERE project_id IN (" . join(",", $project_id_array) . ")
				ORDER BY l.total_mark_count_calc DESC
			");
			$data = array();
			while ($row = $this->db->fetch_array($db_result))
			{
				if (!isset($data[$row["project_id"]]))
				{
					$data[$row["project_id"]] = array();
				}
				if (!isset($data[$row["project_id"]][$row["competence_set_id"]]))
				{
					$data[$row["project_id"]][$row["competence_set_id"]] = $row;
				}
			}
			foreach ($data as $project_id => $project_data)
			{
				$project_node = $xdom->create_child_node("project")->set_attr("id", $project_id);
				foreach ($project_data as $competence_set_id => $competence_set_data)
				{
					$project_node->create_child_node("competence_set")->set_attr("id", $competence_set_data["competence_set_id"])->set_attr("title", $competence_set_data["competence_set_title"])->set_attr("personal_mark_count_calc", $competence_set_data["personal_mark_count_calc"])->set_attr("group_mark_count_calc", $competence_set_data["group_mark_count_calc"])->set_attr("group_mark_raw_count_calc", $competence_set_data["group_mark_raw_count_calc"])->set_attr("total_mark_count_calc", $competence_set_data["total_mark_count_calc"]);
				}
			}
		}
		return $xdom->get_xml(true);
	}

}

?>