<?php

final class projects_taxonomy extends base_taxonomy
{

	public function run()
	{
		$p = $this->get_parts_relative();

		if ($p[1] === "projects" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//projects/[page-<page>/]
			$this->xml_loader->add_xml_with_xslt(new projects_xml_page($page));
		}
		elseif ($p[1] === "projects" and $p[2] === "add" and $p[3] === null)
		{
			//projects/new/
			$this->xml_loader->add_xml_with_xslt(new project_add_xml_page());
		}
		elseif ($p[1] === "p" and $p[2] === null)
		{
			//p/ --> /projects/
			$this->set_redirect_url("/projects/");
		}
		elseif ($p[1] === "p" and $p[2] !== false and $project_id = $this->get_project_id_by_url_name($p[2]))
		{
			$project_taxonomy = new project_taxonomy($this->xml_loader, $this, 2, $project_id, false);
			$project_taxonomy->run();
		}
	}

	private function get_project_id_by_url_name($url_name)
	{
		return url_helper::get_project_id_by_name($url_name);
	}

}

?>