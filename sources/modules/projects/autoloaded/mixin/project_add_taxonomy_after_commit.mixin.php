<?php

class project_add_taxonomy_after_commit_mixin extends base_mixin
{

	protected $last_id;

	public function on_after_commit()
	{
		$this->db->sql("
			INSERT INTO project_taxonomy_item (project_id, type, module_name, title, position, add_time, edit_time, show_in_menu)
			VALUES
			(
				{$this->last_id}, 'module', 'communities', 'Сообщества', 1, NOW(), NOW(), 1
			),
			(
				{$this->last_id}, 'module', 'members', 'Участники', 7, NOW(), NOW(), 1
			)
		");
	}

}

?>