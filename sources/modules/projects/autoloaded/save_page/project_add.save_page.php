<?php

class project_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"project_add_widgets_after_commit",
		"project_add_taxonomy_after_commit",
	);
	protected $dt_name = "project";
	protected $axis_name = "create";

	public function check_rights()
	{
		return $this->user->get_user_id() > 0;
	}

	public function on_before_commit()
	{
		$this->update_array["adder_user_id"] = $this->user->get_user_id();
		$this->update_array["communities_are_on"] = 1;
		return true;
	}

	public function on_after_commit()
	{
		$project_obj = project_obj::instance($this->last_id, $lock = true);
		$access_save = new project_access_save($project_obj, $this->user->get_user_id());
		$access_save->add_admin();
	}

}

?>