<?php

class community_widget_stat_xml_ctrl extends base_community_widget_xml_ctrl
{

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = $this->db->fetch_all("
			SELECT *
			FROM community
			WHERE id = {$this->community_id}
		");
	}

	protected function get_cache()
	{
		return community_widget_stat_cache::init($this->widget_id, $this->community_id);
	}

}

?>