<?php

class community_widget_custom_feed_xml_ctrl extends base_community_widget_xml_ctrl
{

	protected function load_data(select_sql $select_sql = null)
	{
		$this->store_data = $this->data = $this->db->fetch_all("
			SELECT community_custom_feed_id
			FROM community_widget_custom_feed
			WHERE widget_id = {$this->widget_id}
			LIMIT 1
		");
	}

	protected function postprocess()
	{
		if (count($this->store_data))
		{
			$this->xml_loader->add_xml(new community_custom_feed_xml_page($this->project_id, $this->community_id, $this->store_data[0]["community_custom_feed_id"], 1, $this->widget_data["lister_item_count"]));
			$this->xml_loader->add_xml(new community_custom_feed_full_xml_ctrl($this->store_data[0]["community_custom_feed_id"], $this->community_id, $this->project_id));
		}
	}

	protected function get_cache()
	{
		return community_widget_custom_feed_cache::init($this->widget_id, $this->community_id, count($this->data) ? $this->data[0]["community_custom_feed_id"] : null);
	}

}

?>