<?php

class community_widget_text_xml_ctrl extends base_community_widget_xml_ctrl
{

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = $this->db->fetch_all("
			SELECT html
			FROM community_widget_text
			WHERE widget_id = {$this->widget_id}
		");
		if (!$this->data)
		{
			$this->data = array(
				array(
					"widget_id" => $this->widget_id,
					"html" => "",
				)
			);
		}
	}

	protected function get_cache()
	{
		return community_widget_text_cache::init($this->widget_id);
	}

}

?>