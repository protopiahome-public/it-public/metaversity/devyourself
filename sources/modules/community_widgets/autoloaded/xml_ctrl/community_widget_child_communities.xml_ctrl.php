<?php

class community_widget_child_communities_xml_ctrl extends base_community_widget_xml_ctrl
{

	protected $xml_row_name = "community";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "community_short",
			"param2" => "project_id",
		),
	);

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("id");
		$select_sql->add_from("community");
		$select_sql->add_where("parent_id = {$this->community_id} AND parent_approved = 1 AND is_deleted = 0");
		$select_sql->add_order("title");

		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>