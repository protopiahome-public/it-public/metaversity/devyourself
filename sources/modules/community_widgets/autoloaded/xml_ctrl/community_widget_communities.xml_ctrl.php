<?php

class community_widget_communities_xml_ctrl extends base_community_widget_xml_ctrl
{

	protected $xml_row_name = "community";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "community_short",
			"param2" => "project_id",
		),
	);

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("community_id as id");
		$select_sql->add_from("community_widget_communities");
		$select_sql->add_where("widget_id = {$this->widget_id}");
		$select_sql->add_order("position");

		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	protected function get_cache()
	{
		return community_widget_communities_cache::init($this->widget_id, $this->community_id);
	}

}

?>