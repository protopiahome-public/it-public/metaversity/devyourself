<?php

class community_widget_top_users_xml_ctrl extends base_community_widget_lister_xml_ctrl
{

	protected $xml_row_name = "user";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "user_short",
			"param2" => "project_id",
		)
	);
	protected $by_comments = false;

	public function __construct($widget_id, $project_id, $community_id, $widget_data, $by_comments = false)
	{
		parent::__construct($widget_id, $project_id, $community_id, $widget_data);

		$this->by_comments = $by_comments;
		$this->xml_attrs[] = "by_comments";
	}

	public function init()
	{
		parent::init();
		if ($this->by_comments == 0)
		{
			$this->xml_loader->add_xml(new community_widget_top_users_xml_ctrl($this->widget_id, $this->project_id, $this->community_id, $this->widget_data, true));
		}
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("user_id as id, post_count_calc, comment_count_calc");
		$select_sql->add_from("community_user_link");
		$select_sql->add_where("community_id = {$this->community_id}");
		$select_sql->add_where($this->by_comments ? "comment_count_calc > 0" : "post_count_calc > 0");
		$select_sql->add_order($this->by_comments ? "comment_count_calc DESC" : "post_count_calc DESC");

		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>