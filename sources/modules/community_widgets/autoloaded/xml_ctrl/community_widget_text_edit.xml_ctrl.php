<?php

class community_widget_text_edit_xml_ctrl extends base_community_widget_edit_xml_ctrl
{

	protected $xml_row_name = "text";

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = $this->db->fetch_all("
			SELECT widget_id, html
			FROM community_widget_text
			WHERE widget_id = {$this->widget_id}
		");
		if (!$this->data)
		{
			$this->data = array(
				array(
					"widget_id" => $this->widget_id,
					"html" => "",
				)
			);
		}
	}

}

?>