<?php

abstract class base_community_widget_edit_xml_ctrl extends base_easy_xml_ctrl
{

	protected $widget_id;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $widget_data;
	protected $xml_attrs = array("widget_id");

	public function __construct($widget_id, $project_id, $community_id, $widget_data)
	{
		$this->widget_id = $widget_id;
		$this->project_id = $project_id;
		$this->community_id = $community_id;
		$this->widget_data = $widget_data;
		
		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);

		parent::__construct();
	}

}

?>
