<?php

class community_widget_last_comments_xml_ctrl extends base_community_widget_lister_xml_ctrl
{

	protected $xml_row_name = "post";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "post_full",
			"param2" => "project_id",
		),
		array(
			"column" => "last_comment_author_user_id",
			"ctrl" => "user_short",
			"param2" => "project_id",
		),
	);

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("id, comment_count_calc, last_comment_id, last_comment_author_user_id, last_comment_html last_comment_html");
		$select_sql->add_from("post_calc");
		$select_sql->add_where("community_id = {$this->community_id} AND last_comment_id > 0");
		$select_sql->add_order("last_comment_id DESC");

		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>