<?php

class community_widget_last_posts_xml_ctrl extends base_community_widget_lister_xml_ctrl
{

	protected $xml_attrs = array("widget_id", "filter");
	protected $xml_row_name = "post";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "post_full",
			"param2" => "project_id",
		)
	);
	protected $filter = "all";

	public function __construct($widget_id, $project_id, $community_id, $widget_data, $filter = "all")
	{
		parent::__construct($widget_id, $project_id, $community_id, $widget_data);

		if (!$this->community_obj->get_child_community_count())
		{
			$this->filter = "own";
		}
		else
		{
			$this->filter = $filter;
		}
	}

	public function init()
	{
		parent::init();
		if ($this->filter == "all")
		{
			$this->xml_loader->add_xml(new community_widget_last_posts_xml_ctrl($this->widget_id, $this->project_id, $this->community_id, $this->widget_data, "own"));
			$this->xml_loader->add_xml(new community_widget_last_posts_xml_ctrl($this->widget_id, $this->project_id, $this->community_id, $this->widget_data, "sub"));
		}
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("id, comment_count_calc");
		$select_sql->add_from("post_calc");
		if ($this->filter == "all" || $this->filter == "own")
		{
			$select_sql->add_where_in("community_id", array($this->community_id));
		}
		if ($this->filter == "all" || $this->filter == "sub")
		{
			$communities_read_access_helper = $this->project_obj->get_communities_read_access_helper();
			$communities_ids = subcommunities_helper::get_descendants_ids($this->project_id, $this->community_id);
			$communities_read_access_helper->user_communities_intersection_modify_sql($communities_ids, $select_sql);
		}
		$select_sql->add_order("id DESC");

		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>