<?php

class community_widget_custom_feed_cache extends base_cache
{

	public static function init($widget_id, $community_id, $feed_id = null)
	{
		$tag_keys = array();
		$tag_keys[] = community_widget_cache_tag::init($widget_id)->get_key();
		$tag_keys[] = community_widgets_cache_tag::init($community_id)->get_key();
		if ($feed_id)
		{
			$tag_keys[] = community_custom_feed_cache_tag::init($feed_id)->get_key();
		}
		return parent::get_cache(__CLASS__, $widget_id, $tag_keys);
	}

}

?>