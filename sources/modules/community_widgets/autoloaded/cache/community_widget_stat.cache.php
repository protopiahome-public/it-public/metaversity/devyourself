<?php

class community_widget_stat_cache extends base_cache
{

	public static function init($widget_id, $community_id)
	{
		$tag_keys = array();
		$tag_keys[] = community_widget_cache_tag::init($widget_id)->get_key();
		$tag_keys[] = community_stat_cache_tag::init($community_id)->get_key();
		return parent::get_cache(__CLASS__, $widget_id, $community_id, $tag_keys);
	}

}

?>