<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class community_widget_set_title_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_widget_before_start",
		"widget_set_title",
		"community_admin_check_rights",
		"community_widgets_clean_cache"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $widget_container_id;
	protected $widget_container_type;
	protected $widget_id;

}

?>