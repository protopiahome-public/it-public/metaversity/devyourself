<?php

class community_widgets_resort_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_widgets_before_start",
		"community_admin_check_rights",
		"widgets_resort",
		"community_widgets_clean_cache"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;
	protected $widget_container_id;
	protected $widget_container_type;
	protected $new_widgets_for_load;

	public function get_data()
	{
		$ret = array(
			"status" => "OK",
			"widgets" => array()
		);
		foreach ($this->new_widgets_for_load as $widget_id)
		{
			$ret["widgets"][$widget_id] = $this->get_widget_html($widget_id);
		}
		return $ret;
	}

	private function get_widget_html($widget_id)
	{
		$xml_loader_helper = new xml_loader_helper();
		$xml_loader_helper->get_xml_loader()->add_xml(new community_widgets_xml_page($this->project_id, $this->community_id, $widget_id));
		$xml_loader_helper->get_xml_loader()->add_xml_by_class_name("community_short_xml_ctrl", $this->community_id, $this->project_id);
		$xml_loader_helper->get_xml_loader()->add_xml(new community_access_xml_ctrl($this->community_access));
		$xml_loader_helper->get_xml_loader()->add_xslt("ajax/community_widget.ajax", "community_widgets");
		return $xml_loader_helper->get_html();
	}

}

?>