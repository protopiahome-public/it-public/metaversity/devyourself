<?php

class community_widget_add_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_widgets_before_start",
		"widget_add",
		"community_admin_check_rights",
		"community_widgets_clean_cache"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;
	protected $widget_container_id;
	protected $widget_container_type;
	protected $last_id;
	protected $column;

	public function get_data()
	{
		return array(
			"status" => "OK",
			"id" => $this->last_id,
			"column" => $this->column,
			"html" => $this->get_widget_html($this->last_id),
		);
	}

	private function get_widget_html($widget_id)
	{
		$xml_loader_helper = new xml_loader_helper();
		$xml_loader_helper->get_xml_loader()->add_xml(new community_widgets_xml_page($this->project_id, $this->community_id, $widget_id));
		$xml_loader_helper->get_xml_loader()->add_xml(new community_short_xml_ctrl($this->community_id, $this->project_id));
		$xml_loader_helper->get_xml_loader()->add_xml(new community_access_xml_ctrl($this->community_access));
		$xml_loader_helper->get_xml_loader()->add_xslt("ajax/community_widget_full.ajax", "community_widgets");
		return $xml_loader_helper->get_html();
	}

}

?>