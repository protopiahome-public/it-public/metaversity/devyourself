<?php

class community_widget_communities_edit_ajax_page extends base_community_widget_edit_ajax_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_widget_before_start",
		"community_admin_check_rights",
		"widget_communities_edit",
		"community_widget_get_data",
		"community_widget_clean_cache"
	);

}

?>