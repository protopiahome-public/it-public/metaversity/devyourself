<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class community_widget_delete_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_widget_delete_before_start",
		"widget_delete",
		"community_admin_check_rights",
		"community_widgets_clean_cache"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $widget_container_id;
	protected $widget_container_type;
	protected $widget_id;
	protected $widget_row;

}

?>