<?php

abstract class base_community_widget_edit_ajax_ctrl extends base_ajax_ctrl
{

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;
	protected $widget_id;
	protected $widget_row;
	protected $widget_container_id;
	protected $widget_container_type;

}

?>