<?php

class community_widgets_clean_cache_mixin extends base_mixin
{

	protected $community_id;

	public function clean_cache()
	{
		community_widgets_cache_tag::init($this->community_id)->update();
	}

}

?>