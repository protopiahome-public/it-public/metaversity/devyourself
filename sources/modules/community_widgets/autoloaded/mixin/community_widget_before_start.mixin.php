<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class community_widget_before_start_mixin extends base_mixin
{

	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $widget_id;
	protected $widget_row;
	protected $widget_container_id;
	protected $widget_container_type;

	public function on_before_start()
	{
		$this->widget_container_type = "community";
		$this->widget_container_id = $this->community_id;
		if (!isset($_POST["widget_id"]) || !is_good_id($this->widget_id = $_POST["widget_id"]))
		{
			return false;
		}

		$this->widget_row = $this->db->get_row("
			SELECT *
			FROM community_widget
			WHERE id = {$this->widget_id} AND community_id = {$this->community_id}
			FOR UPDATE
		");
		if (!$this->widget_row)
		{
			return false;
		}

		return true;
	}

}

?>