<?php

class community_widget_delete_before_start_mixin extends community_widget_before_start_mixin
{

	public function on_before_start()
	{
		parent::on_before_start();
		return is_good_id($this->widget_id);
	}

}

?>