<?php

class community_add_widgets_after_commit_mixin extends base_mixin
{

	protected $last_id;

	public function on_after_commit()
	{
		$this->db->sql("
			INSERT INTO community_widget (type_id, title, `column`, `position`, community_id)
			VALUES ('text', 'Описание сообщества', 1, 1, {$this->last_id})
		");
		$decription_widget_id = $this->db->get_last_id();
		$this->db->sql("
			INSERT INTO community_widget_text (widget_id, html)
			VALUES ({$decription_widget_id}, '<p>Вы можете разместить здесь краткое описание вашего сообщества.</p>')
		");

		$this->db->sql("
			INSERT INTO community_widget
				(SELECT NULL, {$this->last_id}, d.*, NULL FROM community_widget_default d);
		");
	}

}

?>