<?php

class community_widget_get_data_mixin extends base_mixin
{

	protected $project_id;

	/**
	 * @var community_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;
	protected $widget_id;

	public function get_data()
	{
		$xml_loader_helper = new xml_loader_helper();
		$xml_loader_helper->get_xml_loader()->add_xml(new community_widgets_xml_page($this->project_id, $this->community_id, $this->widget_id));
		$xml_loader_helper->get_xml_loader()->add_xml_by_class_name("project_short_xml_ctrl", $this->project_id);
		$xml_loader_helper->get_xml_loader()->add_xml_by_class_name("community_short_xml_ctrl", $this->community_id, $this->project_id);
		$xml_loader_helper->get_xml_loader()->add_xml(new community_access_xml_ctrl($this->community_access));
		$xml_loader_helper->get_xml_loader()->add_xslt("ajax/community_widget.ajax", "community_widgets");

		return array(
			"status" => "OK",
			"widget_id" => $this->widget_id,
			"html" => $xml_loader_helper->get_html()
		);
	}

}

?>