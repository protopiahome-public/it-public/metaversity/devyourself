<?php

class community_widgets_before_start_mixin extends base_mixin
{

	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $widget_container_id;
	protected $widget_container_type;

	public function on_before_start()
	{
		$this->widget_container_type = "community";
		$this->widget_container_id = $this->community_id;

		return true;
	}

}

?>