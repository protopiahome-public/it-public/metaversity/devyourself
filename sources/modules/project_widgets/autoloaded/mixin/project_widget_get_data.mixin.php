<?php

class project_widget_get_data_mixin extends base_mixin
{

	protected $widget_id;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function get_data()
	{
		$xml_loader_helper = new xml_loader_helper();
		$xml_loader_helper->get_xml_loader()->add_xml(new project_widgets_xml_page($this->project_id, $this->widget_id));
		$xml_loader_helper->get_xml_loader()->add_xml_by_class_name("project_short_xml_ctrl", $this->project_id);
		$xml_loader_helper->get_xml_loader()->add_xml(new project_access_xml_ctrl($this->project_access));
		$xml_loader_helper->get_xml_loader()->add_xslt("ajax/project_widget.ajax", "project_widgets");

		return array(
			"status" => "OK",
			"widget_id" => $this->widget_id,
			"html" => $xml_loader_helper->get_html()
		);
	}

}

?>