<?php

class project_widget_clean_cache_mixin extends base_mixin
{

	protected $widget_id;

	public function clean_cache()
	{
		project_widget_cache_tag::init($this->widget_id)->update();
	}

}

?>