<?php

class project_widgets_before_start_mixin extends base_mixin
{

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $widget_container_id;
	protected $widget_container_type;

	public function on_before_start()
	{
		$this->widget_container_type = "project";
		$this->widget_container_id = $this->project_id;

		return true;
	}

}

?>