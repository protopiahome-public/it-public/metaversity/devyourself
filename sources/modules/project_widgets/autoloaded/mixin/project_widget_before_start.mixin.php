<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class project_widget_before_start_mixin extends base_mixin
{

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $widget_id;
	protected $widget_row;
	protected $widget_container_id;
	protected $widget_container_type;

	public function on_before_start()
	{
		$this->widget_container_type = "project";
		$this->widget_container_id = $this->project_id;
		if (!isset($_POST["widget_id"]) || !is_good_id($this->widget_id = $_POST["widget_id"]))
		{
			return false;
		}

		$this->widget_row = $this->db->get_row("
			SELECT *
			FROM project_widget
			WHERE id = {$this->widget_id} AND project_id = {$this->project_id}
			FOR UPDATE
		");
		if (!$this->widget_row)
		{
			return false;
		}

		return true;
	}

}

?>