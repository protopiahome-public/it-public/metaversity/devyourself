<?php

class project_add_widgets_after_commit_mixin extends base_mixin
{

	protected $last_id;

	public function on_after_commit()
	{
		$this->db->sql("
			INSERT INTO project_widget (type_id, title, `column`, `position`, project_id)
			VALUES ('text', 'Описание проекта', 1, 1, {$this->last_id})
		");
		$decription_widget_id = $this->db->get_last_id();
		$this->db->sql("
			INSERT INTO project_widget_text (widget_id, html)
			VALUES ({$decription_widget_id}, '<p>Вы можете разместить здесь краткое описание вашего проекта.</p>')
		");

		$this->db->sql("
			INSERT INTO project_widget
				(SELECT NULL, {$this->last_id}, d.*, NULL FROM project_widget_default d);
		");
	}

}

?>