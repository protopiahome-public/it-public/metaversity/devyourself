<?php

class project_widget_add_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_widgets_before_start",
		"widget_add",
		"project_widget_moderator_check_rights",
		"project_widgets_clean_cache"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $widget_container_id;
	protected $widget_container_type;
	protected $last_id;
	protected $column;

	public function get_data()
	{
		return array(
			"status" => "OK",
			"id" => $this->last_id,
			"column" => $this->column,
			"html" => $this->get_widget_html($this->last_id),
		);
	}

	private function get_widget_html($widget_id)
	{
		$xml_loader_helper = new xml_loader_helper();
		$xml_loader_helper->get_xml_loader()->add_xml(new project_widgets_xml_page($this->project_id, $widget_id));
		$xml_loader_helper->get_xml_loader()->add_xml(new project_short_xml_ctrl($this->project_id));
		$xml_loader_helper->get_xml_loader()->add_xml(new project_access_xml_ctrl($this->project_access));
		$xml_loader_helper->get_xml_loader()->add_xslt("ajax/project_widget_full.ajax", "project_widgets");
		return $xml_loader_helper->get_html();
	}

}

?>