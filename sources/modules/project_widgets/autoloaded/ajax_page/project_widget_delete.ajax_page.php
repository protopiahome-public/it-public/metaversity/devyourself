<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class project_widget_delete_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_widget_delete_before_start",
		"widget_delete",
		"project_widget_moderator_check_rights",
		"project_widgets_clean_cache"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $widget_container_id;
	protected $widget_container_type;
	protected $widget_id;
	protected $widget_row;

}

?>