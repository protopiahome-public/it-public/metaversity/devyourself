<?php

class project_widgets_resort_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_widgets_before_start",
		"project_widget_moderator_check_rights",
		"widgets_resort",
		"project_widgets_clean_cache"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $widget_container_id;
	protected $widget_container_type;
	protected $new_widgets_for_load;

	public function get_data()
	{
		$ret = array(
			"status" => "OK",
			"widgets" => array()
		);
		foreach ($this->new_widgets_for_load as $widget_id)
		{
			$ret["widgets"][$widget_id] = $this->get_widget_html($widget_id);
		}
		return $ret;
	}

	private function get_widget_html($widget_id)
	{
		$xml_loader_helper = new xml_loader_helper();
		$xml_loader_helper->get_xml_loader()->add_xml(new project_widgets_xml_page($this->project_id, $widget_id));
		$xml_loader_helper->get_xml_loader()->add_xml_by_class_name("project_short_xml_ctrl", $this->project_id);
		$xml_loader_helper->get_xml_loader()->add_xml(new project_access_xml_ctrl($this->project_access));
		$xml_loader_helper->get_xml_loader()->add_xslt("ajax/project_widget.ajax", "project_widgets");
		return $xml_loader_helper->get_html();
	}

}

?>