<?php

class project_widget_edit_form_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_widget_before_start",
		"widget_edit_form",
		"project_widget_moderator_check_rights"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $widget_container_id;
	protected $widget_container_type;
	protected $widget_id;
	protected $widget_row;
	protected $widget_type_row;

	public function get_data()
	{
		return array(
			"status" => "OK",
			"html" => $this->get_form_html()
		);
	}

	protected function get_form_html()
	{
		$xml_loader_helper = new xml_loader_helper();
		$class = "project_widget_" . $this->widget_type_row["edit_ctrl"] . "_edit_xml_ctrl";
		$xml_loader_helper->get_xml_loader()->add_xml(new $class($this->widget_id, $this->project_id, $this->widget_row));
		$xml_loader_helper->get_xml_loader()->add_xslt("ajax/project_widget_edit_form.ajax", "project_widgets");
		$xml_loader_helper->get_xml_loader()->add_xslt("widget_edit/project_{$this->widget_type_row["edit_ctrl"]}.widget_edit", "project_widgets");
		return $xml_loader_helper->get_html();
	}

}

?>