<?php

class project_widget_add_form_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_widgets_before_start",
		"widget_add_form",
		"project_widget_moderator_check_rights",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $widget_container_id;
	protected $widget_container_type;
	protected $column;

	public function get_data()
	{
		return array(
			"status" => "OK",
			"html" => $this->get_form_html()
		);
	}

	protected function get_form_html()
	{
		$xml_loader_helper = new xml_loader_helper();
		$xml_loader_helper->get_xml_loader()->add_xml(new project_short_xml_ctrl($this->project_id));
		$xml_loader_helper->get_xml_loader()->add_xml(new project_widget_add_form_xml_ctrl($this->project_id, $this->column));
		$xml_loader_helper->get_xml_loader()->add_xslt("ajax/project_widget_add_form.ajax", "project_widgets");
		return $xml_loader_helper->get_html();
	}

}

?>