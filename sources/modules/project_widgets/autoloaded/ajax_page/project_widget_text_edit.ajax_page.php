<?php

class project_widget_text_edit_ajax_page extends base_project_widget_edit_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_widget_before_start",
		"project_widget_moderator_check_rights",
		"widget_text_edit",
		"project_widget_get_data",
		"project_widget_clean_cache"
	);

}

?>