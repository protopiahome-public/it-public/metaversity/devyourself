<?php

abstract class base_project_widget_edit_ajax_ctrl extends base_ajax_ctrl
{

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $widget_id;
	protected $widget_row;
	protected $widget_container_id;
	protected $widget_container_type;

}

?>