<?php

class project_widget_last_comments_xml_ctrl extends base_project_widget_lister_xml_ctrl
{

	protected $xml_row_name = "post";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "post_full",
			"param2" => "project_id",
		),
		array(
			"column" => "last_comment_author_user_id",
			"ctrl" => "user_short",
			"param2" => "project_id",
		),
	);

	public function init()
	{
		parent::init();
		if (!$this->project_obj->communities_are_on())
		{
			$this->module_disabled = true;
		}
		elseif (!$this->project_obj->get_access()->can_read_communities())
		{
			$this->module_no_access = true;
		}
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("id, comment_count_calc, last_comment_id, last_comment_author_user_id, last_comment_html");
		$select_sql->add_from("post_calc");
		$select_sql->add_where("project_id = {$this->project_id} AND last_comment_id > 0");
		$select_sql->add_order("last_comment_id DESC");

		$communities_read_access_helper = $this->project_obj->get_communities_read_access_helper();
		$communities_read_access_helper->user_communities_modify_sql($select_sql);
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>