<?php

class project_widget_calendar_xml_ctrl extends base_project_widget_xml_ctrl
{

	public function init()
	{
		parent::init();
		if (!$this->project_obj->events_are_on())
		{
			$this->module_disabled = true;
		}
		elseif (!$this->project_obj->get_access()->can_read_events())
		{
			$this->module_no_access = true;
		}
		else
		{
			$this->xml_loader->add_xml(new events_calendar_xml_ctrl($this->project_id));
		}
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = array();
	}

}

?>