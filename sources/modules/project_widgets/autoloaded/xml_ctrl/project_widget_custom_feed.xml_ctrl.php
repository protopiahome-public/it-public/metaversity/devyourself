<?php

class project_widget_custom_feed_xml_ctrl extends base_project_widget_xml_ctrl
{

	protected function load_data(select_sql $select_sql = null)
	{
		$this->store_data = $this->data = $this->db->fetch_all("
			SELECT project_custom_feed_id
			FROM project_widget_custom_feed
			WHERE widget_id = {$this->widget_id}
			LIMIT 1
		");
	}

	public function init()
	{
		parent::init();
		if (!$this->project_obj->communities_are_on())
		{
			$this->module_disabled = true;
		}
		elseif (!$this->project_obj->get_access()->can_read_communities())
		{
			$this->module_no_access = true;
		}
	}

	protected function postprocess()
	{
		if (count($this->store_data))
		{
			$this->xml_loader->add_xml(new project_custom_feed_xml_page($this->project_id, $this->store_data[0]["project_custom_feed_id"], 1, $this->widget_data["lister_item_count"]));
			$this->xml_loader->add_xml(new project_custom_feed_full_xml_ctrl($this->store_data[0]["project_custom_feed_id"], $this->project_id));
		}
	}

	protected function get_cache()
	{
		return project_widget_custom_feed_cache::init($this->widget_id, $this->project_id, count($this->data) ? $this->data[0]["project_custom_feed_id"] : null);
	}

}

?>