<?php

class project_widget_text_xml_ctrl extends base_project_widget_xml_ctrl
{

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = $this->db->fetch_all("
			SELECT html
			FROM project_widget_text
			WHERE widget_id = {$this->widget_id}
		");
		if (!$this->data)
		{
			$this->data = array(
				array(
					"widget_id" => $this->widget_id,
					"html" => "",
				)
			);
		}
	}

	protected function get_cache()
	{
		return project_widget_text_cache::init($this->widget_id);
	}

}

?>