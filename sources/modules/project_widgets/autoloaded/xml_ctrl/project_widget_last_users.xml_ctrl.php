<?php

class project_widget_last_users_xml_ctrl extends base_project_widget_lister_xml_ctrl
{

	protected $xml_row_name = "user";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "user_short",
			"param2" => "project_id",
		)
	);

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("user_id as id, post_count_calc, comment_count_calc");
		$select_sql->add_from("user_project_link");
		$select_sql->add_where("project_id = {$this->project_id}");
		$select_sql->add_where("status != 'deleted' AND status != 'pretender'");
		$select_sql->add_order("add_time DESC");

		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>