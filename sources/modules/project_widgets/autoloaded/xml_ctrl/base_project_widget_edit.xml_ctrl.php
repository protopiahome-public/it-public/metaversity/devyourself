<?php

abstract class base_project_widget_edit_xml_ctrl extends base_easy_xml_ctrl
{

	protected $widget_id;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $widget_data;
	protected $xml_attrs = array("widget_id");

	public function __construct($widget_id, $project_id, $widget_data)
	{
		$this->widget_id = $widget_id;
		$this->project_id = $project_id;
		$this->widget_data = $widget_data;
		
		$this->project_obj = project_obj::instance($this->project_id);
		
		parent::__construct();
	}

}

?>
