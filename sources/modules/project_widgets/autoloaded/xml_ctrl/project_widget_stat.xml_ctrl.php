<?php

class project_widget_stat_xml_ctrl extends base_project_widget_xml_ctrl
{

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = $this->db->fetch_all("
			SELECT *
			FROM project
			WHERE id = {$this->project_id}
		");
	}

	protected function get_cache()
	{
		return project_widget_stat_cache::init($this->widget_id, $this->project_id);
	}

}

?>