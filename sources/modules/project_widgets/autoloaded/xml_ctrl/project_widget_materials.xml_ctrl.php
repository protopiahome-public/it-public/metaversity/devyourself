<?php

class project_widget_materials_xml_ctrl extends base_project_widget_lister_xml_ctrl
{

	public function init()
	{
		parent::init();
		if (!$this->project_obj->materials_are_on())
		{
			$this->module_disabled = true;
		}
		elseif (!$this->project_obj->get_access()->can_read_materials())
		{
			$this->module_no_access = true;
		}
		else
		{
			$this->xml_loader->add_xml(new materials_xml_page($this->project_id, $page = 1, null, null, null, null, $count = $this->item_count, $force_sort_date = true));
		}
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = array();
	}

}

?>