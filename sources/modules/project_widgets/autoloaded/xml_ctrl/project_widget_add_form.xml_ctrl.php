<?php

class project_widget_add_form_xml_ctrl extends base_xml_ctrl
{

	private $project_id;
	private $column;

	public function __construct($project_id, $column)
	{
		$this->project_id = $project_id;
		$this->column = $column;
		parent::__construct();
	}

	public function get_xml()
	{
		$xdom = xdom::create($this->name);
		$xdom->set_attr("column", $this->column);
		$all = $this->db->fetch_all("
			SELECT type_id, title, multiple_instances
			FROM project_widget_type
			ORDER BY position
		", "type_id");
		$existing = $this->db->fetch_all("
			SELECT id, type_id
			FROM project_widget
			WHERE project_id = {$this->project_id}
		", "type_id");
		foreach ($all as $type_id => $data)
		{
			$widget_node = $xdom->create_child_node("widget")
				->set_attr("type_id", $type_id)
				->set_attr("title", $data["title"]);
			if (!$data["multiple_instances"] and isset($existing[$type_id]))
			{
				$widget_node->set_attr("disabled", "1");
			}
		}
		return $xdom->get_xml(true);
	}

}

?>