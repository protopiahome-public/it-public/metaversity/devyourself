<?php

abstract class base_project_widget_lister_xml_ctrl extends base_project_widget_xml_ctrl
{

	protected $item_count;

	public function init()
	{
		$this->item_count = $this->widget_data["lister_item_count"];
	}

	protected function get_basic_select_sql()
	{
		$select_sql = parent::get_basic_select_sql();
		$select_sql->set_limit($this->item_count);
		return $select_sql;
	}

}
?>