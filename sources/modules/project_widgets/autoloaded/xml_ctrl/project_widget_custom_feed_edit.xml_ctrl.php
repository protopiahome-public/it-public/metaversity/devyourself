<?php

class project_widget_custom_feed_edit_xml_ctrl extends base_project_widget_edit_xml_ctrl
{

	protected $xml_row_name = "feed";
	protected $xml_attrs = array("selected_feed_id", "widget_id", "lister_item_count");
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "project_custom_feed_full",
			"param2" => "project_id",
		)
	);
	protected $selected_feed_id;
	protected $lister_item_count;

	protected function load_data(select_sql $select_sql = null)
	{
		$this->lister_item_count = $this->widget_data["lister_item_count"];

		$this->selected_feed_id = $this->db->get_value("
			SELECT project_custom_feed_id
			FROM project_widget_custom_feed
			WHERE widget_id = {$this->widget_id}
		");

		$select_sql->add_from("project_custom_feed");
		$select_sql->add_select_fields("id");
		$select_sql->add_where("project_id = {$this->project_id}");
		$select_sql->add_order("title ASC");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>