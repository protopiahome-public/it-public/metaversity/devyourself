<?php

abstract class base_project_widget_xml_ctrl extends base_easy_xml_ctrl
{

	protected $widget_id;
	protected $module_disabled = false;
	protected $module_no_access = false;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $widget_data;
	protected $xml_attrs = array("widget_id");

	public function __construct($widget_id, $project_id, $widget_data)
	{
		$this->widget_id = $widget_id;
		$this->project_id = $project_id;
		$this->widget_data = $widget_data;

		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct();
	}

	public function get_xml()
	{
		if ($this->module_disabled || $this->module_no_access)
		{
			return $this->get_node_string(null, array(
					"widget_id" => $this->widget_id,
					"module_disabled" => $this->module_disabled,
					"module_no_access" => $this->module_no_access,
					)
			);
		}
		return parent::get_xml();
	}

}

?>