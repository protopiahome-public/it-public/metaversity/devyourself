<?php

class project_widget_communities_edit_xml_ctrl extends base_project_widget_edit_xml_ctrl
{

	protected $xml_row_name = "community";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "community_short",
			"param2" => "project_id",
		),
	);
	protected $selected_ids = array();

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = $this->db->fetch_all("
			SELECT id
			FROM community
			WHERE project_id = {$this->project_id} AND is_deleted = 0
			ORDER BY title
		");

		$this->selected_ids = $this->db->fetch_all("
			SELECT community_id
			FROM project_widget_communities
			WHERE widget_id = {$this->widget_id}
			ORDER BY position
		", "community_id");
	}

	protected function modify_xml(xdom $xdom)
	{
		$selected_ids_node = $xdom->create_child_node("selected");
		foreach ($this->selected_ids as $selected_id => $selected_row)
		{
			$selected_ids_node->create_child_node("community")->set_attr("id", $selected_id);
		}
	}

}

?>