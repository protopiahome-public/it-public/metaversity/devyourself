<?php

class project_widget_custom_feed_cache extends base_cache
{

	public static function init($widget_id, $project_id, $feed_id = null)
	{
		$tag_keys = array();
		$tag_keys[] = project_widget_cache_tag::init($widget_id)->get_key();
		$tag_keys[] = project_widgets_cache_tag::init($project_id)->get_key();
		if ($feed_id)
		{
			$tag_keys[] = project_custom_feed_cache_tag::init($feed_id)->get_key();
		}
		return parent::get_cache(__CLASS__, $widget_id, $tag_keys);
	}

}

?>