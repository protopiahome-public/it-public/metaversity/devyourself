<?php

class project_widget_text_cache extends base_cache
{

	public static function init($widget_id)
	{
		$tag_keys = array();
		$tag_keys[] = project_widget_cache_tag::init($widget_id)->get_key();
		return parent::get_cache(__CLASS__, $widget_id, $tag_keys);
	}

}

?>