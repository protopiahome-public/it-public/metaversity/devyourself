<?php

class project_widget_communities_cache extends base_cache
{

	public static function init($widget_id, $project_id)
	{
		$tag_keys = array();
		$tag_keys[] = project_widget_cache_tag::init($widget_id)->get_key();
		$tag_keys[] = communities_list_cache_tag::init($project_id)->get_key();
		return parent::get_cache(__CLASS__, $widget_id, $project_id, $tag_keys);
	}

}

?>