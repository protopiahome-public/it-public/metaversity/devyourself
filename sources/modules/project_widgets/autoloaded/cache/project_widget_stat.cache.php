<?php

class project_widget_stat_cache extends base_cache
{

	public static function init($widget_id, $project_id)
	{
		$tag_keys = array();
		$tag_keys[] = project_stat_cache_tag::init($project_id)->get_key();
		return parent::get_cache(__CLASS__, $widget_id, $project_id, $tag_keys);
	}

}

?>