<?php

class project_widgets_xml_page extends base_easy_xml_ctrl
{

	protected $project_id;
	protected $widget_id;
	protected $xml_row_name = "widget";
	protected $xml_attrs = array("project_id");

	public function __construct($project_id, $widget_id = null)
	{
		$this->project_id = $project_id;
		$this->widget_id = $widget_id;
		parent::__construct();
	}

	protected function get_cache()
	{
		if (!$this->widget_id)
		{
			return project_widgets_cache::init($this->project_id);
		}
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = $this->store_data = $this->db->fetch_all("
			SELECT 
				w.id, w.`column`, w.title, 
				wt.title as widget_type_title, wt.ctrl, wt.multiple_instances, IF(wt.edit_ctrl = '', 0, 1) as is_editable,
				w.type_id, w.lister_item_count
			FROM project_widget w
			LEFT JOIN project_widget_type wt ON wt.type_id = w.type_id
			WHERE w.project_id = {$this->project_id}
			" . ($this->widget_id ? " AND id = {$this->widget_id} " : "") . "
			ORDER BY w.`column`, w.`position`
		");
	}

	protected function postprocess()
	{
		foreach ($this->store_data as $widget_data)
		{
			$widget_class = "project_widget_" . $widget_data["ctrl"] . "_xml_ctrl";
			$this->xml_loader->add_xml(new $widget_class($widget_data["id"], $this->project_id, $widget_data));
			$this->xml_loader->add_xslt("widget/project_{$widget_data["type_id"]}.widget", "project_widgets");
		}
		parent::postprocess();
	}

}

?>