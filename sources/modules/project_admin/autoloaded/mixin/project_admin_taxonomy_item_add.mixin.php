<?php

class project_admin_taxonomy_item_add_mixin extends base_mixin
{

	protected $project_id;
	protected $dt_name;
	protected $parent_id;
	protected $update_array;

	public function on_before_start()
	{
		$this->parent_id = POST("parent_id");

		if (!is_good_num($this->parent_id))
		{
			return false;
		}

		if ($this->parent_id)
		{
			$good_parent_exists = $this->db->row_exists("
				SELECT id
				FROM project_taxonomy_item
				WHERE id = {$this->parent_id}
					AND project_id = {$this->project_id} AND parent_id IS NULL AND "
				. ($this->dt_name == "static_page" ? "type IN ('folder', 'static_page')" : "type = 'folder'")
			);
			if (!$good_parent_exists)
			{
				return false;
			}
		}

		return true;
	}

	public function on_before_commit()
	{
		if ($this->parent_id)
		{
			$this->update_array["parent_id"] = $this->parent_id;
		}

		return true;
	}

}

?>