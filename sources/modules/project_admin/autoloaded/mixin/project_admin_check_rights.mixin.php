<?php

class project_admin_check_rights_mixin extends base_mixin
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->project_access->has_admin_rights();
	}

}

?>