<?php

class project_admin_taxonomy_module_edit_mixin extends base_mixin
{

	protected $project_id;
	protected $taxonomy_module_id;
	protected $show_in_menu;

	public function on_before_start()
	{
		$this->taxonomy_module_id = POST("taxonomy_module_id");
		if (!is_good_id($this->taxonomy_module_id))
		{
			return false;
		}

		$taxonomy_module_exists = $this->db->row_exists("
			SELECT id
			FROM project_taxonomy_item
			WHERE id = {$this->taxonomy_module_id} AND project_id = {$this->project_id}
				AND type = 'module'
		");
		if (!$taxonomy_module_exists)
		{
			return false;
		}

		if (!isset($_POST["show_in_menu"]))
		{
			$this->show_in_menu = 0;
		}
		elseif ($_POST["show_in_menu"] === "1")
		{
			$this->show_in_menu = 1;
		}
		else
		{
			return false;
		}

		return true;
	}

	public function commit()
	{
		$this->db->sql("
			UPDATE project_taxonomy_item
			SET show_in_menu = {$this->show_in_menu}
			WHERE id = {$this->taxonomy_module_id}
		");
		return true;
	}

	public function clean_cache()
	{
		taxonomy_module_cache_tag::init($this->taxonomy_module_id)->update();
		project_taxonomy_cache_tag::init($this->project_id)->update();
	}

}

?>