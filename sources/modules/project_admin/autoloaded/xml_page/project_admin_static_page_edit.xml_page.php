<?php

class project_admin_static_page_edit_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	protected $dt_name = "static_page";
	protected $axis_name = "edit";
	// Internal
	protected $mixins = array(
		"static_page_dt_init",
	);
	protected $dependencies_settings = array(
		array(
			"column" => "block_set_id",
			"ctrl" => "block_set",
		),
	);

	/**
	 * @var static_page_dt
	 */
	protected $dt;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($id, $project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct($id);
	}

	public function check_rights()
	{
		return $this->project_access->has_admin_rights();
	}

	public function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("IF(block_set_id > 0, block_set_id, 0) as block_set_id");
	}

}

?>