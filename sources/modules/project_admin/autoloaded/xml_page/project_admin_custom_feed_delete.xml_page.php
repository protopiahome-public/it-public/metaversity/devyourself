<?php

class project_admin_custom_feed_delete_xml_page extends base_delete_xml_ctrl
{

	protected $db_table = "project_custom_feed";
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($project_id, $feed_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct($feed_id);
	}

	public function check_rights()
	{
		return $this->project_access->has_admin_rights();
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>