<?php

class project_admin_intmenu_item_edit_xml_page extends base_dt_edit_xml_ctrl
{

	protected $dt_name = "project_intmenu_item";
	protected $axis_name = "edit";
	protected $project_id;

	public function __construct($project_id, $item_id)
	{
		$this->project_id = $project_id;
		parent::__construct($item_id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>