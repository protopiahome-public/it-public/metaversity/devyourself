<?php

class project_admin_events_categories_xml_page extends base_xml_ctrl
{
	
	private $project_id;
	
	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}
	
	public function get_xml()
	{
		return $this->get_node_string($this->name, array (
				"project_id" => $this->project_id 
		));
	}

}

?>