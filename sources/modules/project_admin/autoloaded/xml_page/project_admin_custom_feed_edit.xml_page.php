<?php

class project_admin_custom_feed_edit_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	protected $dt_name = "project_custom_feed";
	protected $axis_name = "edit";
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($project_id, $feed_id)
	{
		$this->project_id = $project_id;
		
		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();
		
		parent::__construct($feed_id);
	}

	public function check_rights()
	{
		return $this->project_access->has_admin_rights();
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_project_obj($this->project_obj);
		return true;
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>