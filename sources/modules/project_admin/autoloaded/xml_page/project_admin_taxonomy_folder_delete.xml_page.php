<?php

class project_admin_taxonomy_folder_delete_xml_page extends base_delete_xml_ctrl
{

	protected $db_table = "project_taxonomy_item";
	protected $project_id;

	public function __construct($taxonomy_folder_id, $project_id)
	{
		$this->project_id = $project_id;
		parent::__construct($taxonomy_folder_id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
		$select_sql->add_where("type = 'folder'");
	}

}

?>