<?php

class project_admin_custom_feed_source_add_xml_page extends base_xml_ctrl
{

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $feed_id;

	public function __construct($project_id, $feed_id)
	{
		$this->project_id = $project_id;
		$this->feed_id = $feed_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct();
	}

	public function check_rights()
	{
		return $this->project_access->has_admin_rights();
	}

	public function get_xml()
	{
		return $this->get_node_string();
	}

}

?>