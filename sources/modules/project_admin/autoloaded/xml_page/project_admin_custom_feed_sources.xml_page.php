<?php

class project_admin_custom_feed_sources_xml_page extends base_easy_xml_ctrl
{

	protected $xml_row_name = "feed_source";
	protected $dependencies_settings = array(
		array(
			"column" => "community_id",
			"ctrl" => "community_short",
			"param2" => "project_id",
		),
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $feed_id;

	public function __construct($project_id, $feed_id)
	{
		$this->project_id = $project_id;
		$this->feed_id = $feed_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct();
	}

	public function check_rights()
	{
		return $this->project_access->has_admin_rights();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("project_custom_feed_source");
		$select_sql->add_select_fields("*");
		$select_sql->add_where("custom_feed_id = {$this->feed_id}");
		$select_sql->add_order("community_id, section_id");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}
	
	protected function postprocess()
	{
		foreach ($this->data as $data_row)
		{
			if ($data_row["section_id"])
			{
				$this->xml_loader->add_xml(new section_full_xml_ctrl($data_row["section_id"], $data_row["community_id"], $this->project_id));
			}
		}
	}

}

?>