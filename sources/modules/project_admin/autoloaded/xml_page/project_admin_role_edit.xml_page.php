<?php

class project_admin_role_edit_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings level 2
	protected $options = array(
		0 => "—",
		1 => "Склонность",
		2 => "Способность",
		3 => "Компетенция"
	);
	// Settings
	protected $dt_name = "project_role";
	protected $axis_name = "edit";
	protected $enable_blocks = true;
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $competence_set_id;

	public function __construct($project_id, $project_role_id)
	{
		$this->project_id = $project_id;
		
		$this->project_obj = project_obj::instance($this->project_id);
		
		parent::__construct($project_role_id);
	}

	public function init()
	{
		$this->competence_set_id = $this->project_obj->get_vector_competence_set_id();
		if ($this->competence_set_id)
		{
			$this->xml_loader->add_xml_by_class_name("competence_set_short_xml_ctrl", $this->competence_set_id);
			$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->competence_set_id));
			$this->xml_loader->add_xml(new rate_multi_link_xml_ctrl($this->dt_name, $this->id, $this->competence_set_id, $this->options));
		}
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>