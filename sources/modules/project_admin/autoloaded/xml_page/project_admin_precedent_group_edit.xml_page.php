<?php

class project_admin_precedent_group_edit_xml_page extends base_dt_edit_xml_ctrl
{

	protected $dt_name = "precedent_group";
	protected $axis_name = "edit";
	protected $project_id;

	public function __construct($project_id, $precedent_group_id)
	{
		$this->project_id = $project_id;
		parent::__construct($precedent_group_id);
	}
	
	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>