<?php

class project_admin_domain_xml_page extends base_dt_edit_xml_ctrl
{
	
	protected $dt_name = "project";
	protected $axis_name = "edit_domain";
	protected $enable_blocks = false;
	
	public function modify_xml(xdom $xdom)
	{
		if ($this->domain_logic->get_main_host_name())
		{
			$xdom->set_attr("main_host_name", $this->domain_logic->get_main_host_name());
		}
	}
	
}

?>