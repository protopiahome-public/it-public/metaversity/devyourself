<?php

class project_admin_communities_menu_xml_page extends base_xml_ctrl
{

	protected $project_id;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;
	}

	public function init()
	{
		$this->xml_loader->add_xml(new communities_menu_xml_ctrl($this->project_id));
	}

	public function get_xml()
	{
		return;
	}

}

?>
