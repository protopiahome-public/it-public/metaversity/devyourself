<?php

class project_admin_role_add_xml_page extends base_dt_add_xml_ctrl
{

	// Settings level 2
	protected $options = array(
		0 => "—",
		1 => "Склонность",
		2 => "Способность",
		3 => "Компетенция"
	);
	// Settings
	protected $dt_name = "project_role";
	protected $axis_name = "edit";
	protected $enable_blocks = true;
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $competence_set_id;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct();
	}

	public function init()
	{
		$this->competence_set_id = $this->project_obj->get_vector_competence_set_id();
		if ($this->competence_set_id)
		{
			$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->competence_set_id));
			$this->xml_loader->add_xml(new rate_xml_ctrl(0, $this->competence_set_id, $this->options));
		}
	}

}

?>