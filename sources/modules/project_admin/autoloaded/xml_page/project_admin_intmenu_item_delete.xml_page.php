<?php

class project_admin_intmenu_item_delete_xml_page extends base_delete_xml_ctrl
{

	protected $db_table = "project_intmenu_item";
	protected $project_id;

	public function __construct($project_id, $intmenu_item_id)
	{
		$this->project_id = $project_id;
		parent::__construct($intmenu_item_id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>