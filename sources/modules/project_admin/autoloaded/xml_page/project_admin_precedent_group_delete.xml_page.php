<?php

class project_admin_precedent_group_delete_xml_page extends base_delete_xml_ctrl
{

	protected $db_table = "precedent_group";
	protected $project_id;

	public function __construct($project_id, $precedent_group_id)
	{
		$this->project_id = $project_id;
		parent::__construct($precedent_group_id);
	}
	
	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>