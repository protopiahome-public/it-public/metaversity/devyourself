<?php

class project_admin_moderators_xml_page extends base_easy_xml_ctrl
{

	protected $dependencies_settings = array(
		array(
			"column" => "user_id",
			"ctrl" => "user_short",
			"param2" => "project_id",
		),
	);
	protected $project_id;
	protected $xml_row_name = "moderator";
	protected $page;

	public function __construct($project_id, $page)
	{
		$this->project_id = $project_id;
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 25));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("pa.*");
		$select_sql->add_from("project_admin pa");
		$select_sql->add_join("LEFT JOIN user_project_link l ON pa.user_id = l.user_id AND pa.project_id = l.project_id");
		$select_sql->add_where("pa.project_id = {$this->project_id} AND l.status = 'moderator'");
		$select_sql->add_order("pa.add_time DESC");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>