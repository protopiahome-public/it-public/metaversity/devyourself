<?php

class project_admin_role_delete_xml_page extends base_delete_xml_ctrl
{

	protected $db_table = "project_role";
	protected $project_id;

	public function __construct($project_id, $project_role_id)
	{
		$this->project_id = $project_id;
		parent::__construct($project_role_id);
	}
	
	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>