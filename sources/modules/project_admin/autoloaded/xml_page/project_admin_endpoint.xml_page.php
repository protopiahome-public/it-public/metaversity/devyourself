<?php

class project_admin_endpoint_xml_page extends base_dt_edit_xml_ctrl
{

	protected $dt_name = "project";
	protected $axis_name = "edit_endpoint";

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("dt.endpoint_connect_attempts_left");
		$select_sql->add_select_fields("dt.endpoint_last_connect_time");
		$select_sql->add_select_fields("dt.endpoint_last_connect_result");
	}

	protected function fill_xml(xdom $xdom)
	{
		parent::fill_xml($xdom);
		$row = reset($this->data);

		$xdom->set_attr("endpoint_last_connect_time", $row["endpoint_last_connect_time"]);
		$result_parsed = explode(":", $row["endpoint_last_connect_result"]);
		$last_connect_result = "NO_DATA";
		if ($result_parsed[0] == "OK")
		{
			$last_connect_result = "OK";
		}
		elseif (strlen($result_parsed[0]))
		{
			$last_connect_result = $result_parsed[0];
			if (isset($result_parsed[1]))
			{
				$xdom->set_attr("endpoint_last_connect_curl_error", $result_parsed[1]);
				if (isset($result_parsed[2]))
				{
					$xdom->set_attr("endpoint_last_connect_http_code", $result_parsed[2]);
				}
			}
		}
		$xdom->set_attr("endpoint_last_connect_result", $last_connect_result);
		if ($row["endpoint_connect_attempts_left"] <= 0)
		{
			$xdom->set_attr("api_is_last_attempt", "1");
		}
	}

}

?>