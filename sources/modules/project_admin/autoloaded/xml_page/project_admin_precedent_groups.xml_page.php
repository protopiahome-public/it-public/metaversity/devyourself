<?php

class project_admin_precedent_groups_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "precedent_group";
	// Module specific
	protected $project_id;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("precedent_group dt");
		$select_sql->add_select_fields("dt.id, dt.title, COUNT(p.id) as precedent_count");
		$select_sql->add_where("dt.project_id = {$this->project_id}");
		$select_sql->add_order("dt.position");
		$select_sql->add_join("LEFT JOIN precedent p ON p.precedent_group_id = dt.id");
		$select_sql->add_group_by("dt.id");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>