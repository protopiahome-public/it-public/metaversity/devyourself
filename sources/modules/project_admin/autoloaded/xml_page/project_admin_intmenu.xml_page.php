<?php

class project_admin_intmenu_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "intmenu_item";
	// Module specific
	protected $project_id;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("project_intmenu_item dt");
		$select_sql->add_select_fields("dt.id, dt.title, dt.url");
		$select_sql->add_where("dt.project_id = {$this->project_id}");
		$select_sql->add_order("dt.position");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>