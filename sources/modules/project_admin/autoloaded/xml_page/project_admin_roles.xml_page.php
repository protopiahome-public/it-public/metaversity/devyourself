<?php

class project_admin_roles_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "project_role";
	// Internal
	protected $project_id;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("competences", "competence_count_calc ASC", "competence_count_calc DESC");
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("project_role pr");
		$select_sql->add_select_fields("pr.id, pr.enabled, IF(rl.competence_count_calc > 0, rl.competence_count_calc, 0) as competence_count_calc, pr.title");
		$select_sql->add_where("project_id = {$this->project_id}");
		$select_sql->add_join("
			LEFT JOIN project p ON (p.id = pr.project_id)
			LEFT JOIN project_role_rate_link rl
			ON (rl.project_role_id = pr.id AND rl.competence_set_id = p.vector_competence_set_id)
		");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>