<?php

class project_admin_taxonomy_link_edit_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	protected $dt_name = "taxonomy_link";
	protected $axis_name = "edit";

	/**
	 * @var taxonomy_link_dt
	 */
	protected $dt;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($id, $project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct($id);
	}

	public function check_rights()
	{
		return $this->project_access->has_admin_rights();
	}

}

?>