<?php

class project_admin_taxonomy_add_form_xml_ctrl extends base_xml_ctrl
{

	/**
	 * @var project_obj
	 */
	private $project_obj;

	public function __construct($project_obj)
	{
		$this->project_obj = $project_obj;
		parent::__construct();
	}

	public function get_xml()
	{
		$page_types = array();
		$page_types["static_page"] = "Страница";
		$page_types["link"] = "Ссылка";
		$page_types["folder"] = "Папка";

		$page_types_xml = "";
		foreach ($page_types as $name => $title)
		{
			$page_types_xml .= "<page_type name=\"{$name}\" title=\"{$title}\"/>";
		}

		$page_types_xml .= "<page_type name=\"module\" title=\"Сообщества\" module_name=\"communities\" enabled=\"" . ( $this->project_obj->communities_are_on() ? "1" : "0" ) . "\"/>";
		$page_types_xml .= "<page_type name=\"module\" title=\"Вектор\" module_name=\"vector\" enabled=\"" . ( $this->project_obj->vector_is_on() ? "1" : "0" ) . "\"/>";
		$page_types_xml .= "<page_type name=\"module\" title=\"События\" module_name=\"events\" enabled=\"" . ( $this->project_obj->events_are_on() ? "1" : "0" ) . "\"/>";
		$page_types_xml .= "<page_type name=\"module\" title=\"Материалы\" module_name=\"materials\" enabled=\"" . ( $this->project_obj->materials_are_on() ? "1" : "0" ) . "\"/>";
		$page_types_xml .= "<page_type name=\"module\" title=\"Рекомедация ролей\" module_name=\"roles\" enabled=\"" . ( $this->project_obj->role_recommendation_is_on() ? "1" : "0" ) . "\"/>";
		$page_types_xml .= "<page_type name=\"module\" title=\"Прецеденты\" module_name=\"marks\" enabled=\"" . ( $this->project_obj->marks_are_on() ? "1" : "0" ) . "\"/>";

		return $this->get_node_string($this->name, array(
				"project_id" => $this->project_obj->get_id()
				), $page_types_xml);
	}

}

?>
