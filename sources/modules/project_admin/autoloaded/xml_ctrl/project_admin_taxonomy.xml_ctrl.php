<?php

class project_admin_taxonomy_xml_ctrl extends base_xml_ctrl
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $selected_item_id;

	public function __construct($project_obj, $selected_item_id)
	{
		$this->project_obj = $project_obj;
		$this->selected_item_id = $selected_item_id;
		parent::__construct();
	}

	public function get_xml()
	{
		$project_admin_taxonomy_helper = new project_admin_taxonomy_helper($this->project_obj);
		$tree_init_data = htmlspecialchars(json_encode($project_admin_taxonomy_helper->get_nodes_data($this->selected_item_id)));

		return $this->get_node_string($this->name, array(
				"project_id" => $this->project_obj->get_id(),
				"selected_item_id" => $this->selected_item_id,
				), "<tree_init_data>{$tree_init_data}</tree_init_data>");
	}

}

?>