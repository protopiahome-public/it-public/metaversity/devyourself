<?php

final class project_admin_taxonomy extends base_taxonomy
{

	/**
	 *
	 * @var project_obj
	 */
	protected $project_obj;
	protected $project_id;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, project_obj $project_obj)
	{
		$this->project_obj = $project_obj;
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		$p = $this->get_parts_relative();

		$project_obj = $this->project_obj;
		$this->project_id = $project_id = $project_obj->get_id();
		$project_access = $project_obj->get_access();

		$this->xml_loader->set_error_403_xslt("project_admin_403", "project_admin");
		$this->xml_loader->set_error_404_xslt("error_404", "_site");

		$taxonomy_selected_item_id = -1;
		if ($project_obj->role_recommendation_is_on() && !$project_access->has_admin_rights() and $project_access->can_moderate_roles() and ($p[1] === null or $p[1] === "roles" and $p[2] === null))
		{
			//admin/ --> /admin/roles/roles/
			//admin/roles/ --> /admin/roles/roles/
			$this->set_redirect_url("roles/roles/");
		}
		elseif ($p[1] === "roles" and $project_obj->role_recommendation_is_on() && $project_access->can_moderate_roles())
		{
			$taxonomy_selected_item_id = $this->get_taxonomy_module_id_by_module_name("roles");
			if ($p[2] !== "roles")
			{
				if ($project_access->has_admin_rights())
				{
					if ($p[2] === null)
					{
						//admin/roles/
						$this->xml_loader->add_xml_with_xslt(new project_admin_roles_settings_xml_page(), null, "project_admin_403");
					}
					elseif ($p[2] === "disable" and $p[3] === null)
					{
						//p/<project_name>/admin/roles/disable/
						$this->xml_loader->add_xml(new project_admin_taxonomy_module_disable_xml_page($taxonomy_selected_item_id, $project_id));
						$this->xml_loader->add_xslt("project_admin_{$p[1]}_disable", "project_admin");
					}
				}
				else
				{
					$this->xml_loader->set_error_403();
				}
			}
			else
			{
				if ($p[3] === null)
				{
					//p/<project_name>/admin/roles/roles/
					$this->xml_loader->add_xml_with_xslt(new project_admin_roles_xml_page($project_id), null, "project_admin_403");
				}
				elseif ($p[3] === "add" and $p[4] === null)
				{
					//p/<project_name>/admin/roles/roles/add/
					$this->xml_loader->add_xml_with_xslt(new project_admin_role_add_xml_page($project_id), null, "project_admin_403");
				}
				elseif (is_good_id($p[3]) and $p[4] === null)
				{
					//p/<project_name>/admin/roles/roles/<id>/
					$this->xml_loader->add_xml_with_xslt(new project_admin_role_show_xml_page($project_id, $p[3]), null, "project_admin_403");
				}
				elseif (is_good_id($p[3]) and $p[4] === "edit" and $p[5] === null)
				{
					//p/<project_name>/admin/roles/roles/<id>/edit/
					$this->xml_loader->add_xml_with_xslt(new project_admin_role_edit_xml_page($project_id, $p[3]), null, "project_admin_403");
				}
				elseif (is_good_id($p[3]) and $p[4] === "delete" and $p[5] === null)
				{
					//p/<project_name>/admin/roles/roles/<id>/delete/
					$this->xml_loader->add_xml_with_xslt(new project_admin_role_delete_xml_page($project_id, $p[3]), null, "project_admin_403");
				}
			}
		}
		elseif (!$project_access->has_admin_rights())
		{
			$this->xml_loader->set_error_403();
		}
		elseif ($p[1] === null)
		{
			//p/<project_name>/admin/
			$this->xml_loader->add_xml_with_xslt(new project_admin_taxonomy_root_xml_page(), null, "project_admin_403");
		}
		elseif ($p[1] === "settings" and $p[2] === null)
		{
			//p/<project_name>/admin/settings/
			$this->xml_loader->add_xml_with_xslt(new project_admin_general_xml_page($project_id), null, "project_admin_403");
		}
		elseif ($p[1] === "access" and $p[2] === null)
		{
			//p/<project_name>/admin/access/
			$this->xml_loader->add_xml_with_xslt(new project_admin_access_xml_page($project_id), null, "project_admin_403");
		}
		elseif ($p[1] === "domain" and $p[2] === null)
		{
			//p/<project_name>/admin/domain/
			$this->xml_loader->add_xml_with_xslt(new project_admin_domain_xml_page($project_id), null, "project_admin_403");
		}
		elseif ($p[1] === "join-form" and $p[2] === null)
		{
			//p/<project_name>/admin/join-form/
			$this->xml_loader->add_xml_with_xslt(new project_admin_join_form_xml_page($project_id), null, "project_admin_403");
		}
		elseif ($p[1] === "design" and $p[2] === null)
		{
			//p/<project_name>/admin/design/
			$this->xml_loader->add_xml_with_xslt(new project_admin_design_xml_page($project_id), null, "project_admin_403");
		}
		elseif ($project_obj->communities_are_on() and $p[1] === "communities")
		{
			$taxonomy_selected_item_id = $this->get_taxonomy_module_id_by_module_name("communities");
			//p/<project_name>/admin/communities/...
			if ($p[2] === null)
			{
				//p/<project_name>/admin/communities/
				$this->xml_loader->add_xml_with_xslt(new project_admin_communities_xml_page($project_id), null, "project_admin_403");
			}
			elseif ($p[2] === "disable" and $p[3] === null)
			{
				//p/<project_name>/admin/communities/disable/
				$this->xml_loader->add_xml(new project_admin_taxonomy_module_disable_xml_page($taxonomy_selected_item_id, $project_id));
				$this->xml_loader->add_xslt("project_admin_{$p[1]}_disable", "project_admin");
			}
			elseif ($p[2] == "menu" and $p[3] === null)
			{
				//p/<project_name>/admin/communities/menu/
				$this->xml_loader->add_xml_with_xslt(new project_admin_communities_menu_xml_page($project_id));
			}
			elseif ($p[2] === "menu" and $p[3] === "add" and $p[4] === null)
			{
				//p/<project_name>/admin/communities/menu/add/
				$this->xml_loader->add_xml_with_xslt(new project_admin_communities_menu_add_xml_page($project_id));
			}
			elseif ($p[2] === "feeds")
			{
				if ($p[3] === null)
				{
					//p/<project_name>/admin/communities/feeds/
					$this->xml_loader->add_xml_with_xslt(new project_admin_custom_feeds_xml_page($project_id), null, "project_admin_403");
				}
				elseif ($p[3] === "add" and $p[4] === null)
				{
					//p/<project_name>/admin/communities/feeds/add/
					$this->xml_loader->add_xml_with_xslt(new project_admin_custom_feed_add_xml_page($project_id), null, "project_admin_403");
				}
				elseif (is_good_id($p[3]))
				{
					$this->xml_loader->add_xml(new project_custom_feed_full_xml_ctrl($p[3], $project_id));
					if ($p[4] === null)
					{
						//p/<project_name>/admin/communities/feeds/<id>/
						$this->xml_loader->add_xml_with_xslt(new project_admin_custom_feed_edit_xml_page($project_id, $p[3]), null, "project_admin_403");
					}
					elseif ($p[4] === "sources" and $p[5] === null)
					{
						//p/<project_name>/admin/communities/feeds/<id>/sources/
						$this->xml_loader->add_xml_with_xslt(new project_admin_custom_feed_sources_xml_page($project_id, $p[3]), null, "project_admin_403");
					}
					elseif ($p[4] === "sources" and $p[5] === "add" and $p[6] === null)
					{
						//p/<project_name>/admin/communities/feeds/<id>/sources/add/
						$this->xml_loader->add_xml_with_xslt(new project_admin_custom_feed_source_add_xml_page($project_id, $p[3]), null, "project_admin_403");
					}
					elseif ($p[4] === "delete" and $p[5] === null)
					{
						//p/<project_name>/admin/communities/feeds/<id>/delete/
						$this->xml_loader->add_xml_with_xslt(new project_admin_custom_feed_delete_xml_page($project_id, $p[3]), null, "project_admin_403");
					}
				}
			}
		}
		elseif ($project_obj->vector_is_on() && $p[1] === "vector")
		{
			$taxonomy_selected_item_id = $this->get_taxonomy_module_id_by_module_name("vector");
			//p/<project_name>/admin/vector/...
			if ($p[2] === null)
			{
				//p/<project_name>/admin/vector/
				$this->xml_loader->add_xml_with_xslt(new project_admin_vector_xml_page($project_id), null, "project_admin_403");
			}
			elseif ($p[2] === "disable" and $p[3] === null)
			{
				//p/<project_name>/admin/vector/disable/
				$this->xml_loader->add_xml(new project_admin_taxonomy_module_disable_xml_page($taxonomy_selected_item_id, $project_id));
				$this->xml_loader->add_xslt("project_admin_{$p[1]}_disable", "project_admin");
			}
		}
		elseif ($project_obj->events_are_on() && $p[1] === "events")
		{
			$taxonomy_selected_item_id = $this->get_taxonomy_module_id_by_module_name("events");
			//p/<project_name>/admin/events/...
			if ($p[2] === null)
			{
				//p/<project_name>/admin/events/
				$this->xml_loader->add_xml_with_xslt(new project_admin_events_xml_page($project_id), null, "project_admin_403");
			}
			elseif ($p[2] === "disable" and $p[3] === null)
			{
				//p/<project_name>/admin/events/disable/
				$this->xml_loader->add_xml(new project_admin_taxonomy_module_disable_xml_page($taxonomy_selected_item_id, $project_id));
				$this->xml_loader->add_xslt("project_admin_{$p[1]}_disable", "project_admin");
			}
			elseif ($p[1] === "events" and $p[2] === "categories" and $p[3] === null)
			{
				//p/<project_name>/admin/events/categories/
				$this->xml_loader->add_xml_with_xslt(new project_admin_events_categories_xml_page($project_id));
			}
		}
		elseif ($project_obj->materials_are_on() && $p[1] === "materials")
		{
			$taxonomy_selected_item_id = $this->get_taxonomy_module_id_by_module_name("materials");
			//p/<project_name>/admin/materials/...
			if ($p[2] === null)
			{
				//p/<project_name>/admin/materials/
				$this->xml_loader->add_xml_with_xslt(new project_admin_materials_xml_page($project_id), null, "project_admin_403");
			}
			elseif ($p[2] === "disable" and $p[3] === null)
			{
				//p/<project_name>/admin/materials/disable/
				$this->xml_loader->add_xml(new project_admin_taxonomy_module_disable_xml_page($taxonomy_selected_item_id, $project_id));
				$this->xml_loader->add_xslt("project_admin_{$p[1]}_disable", "project_admin");
			}
			elseif ($p[1] === "materials" and $p[2] === "categories" and $p[3] === null)
			{
				//p/<project_name>/admin/materials/categories/
				$this->xml_loader->add_xml_with_xslt(new project_admin_materials_categories_xml_page($project_id));
			}
		}
		elseif ($project_obj->marks_are_on() && $p[1] === "marks")
		{
			$taxonomy_selected_item_id = $this->get_taxonomy_module_id_by_module_name("marks");
			//p/<project_name>/admin/marks/...
			if ($p[2] === null)
			{
				//p/<project_name>/admin/marks/
				$this->xml_loader->add_xml_with_xslt(new project_admin_marks_xml_page($project_id), null, "project_admin_403");
			}
			elseif ($p[2] === "disable" and $p[3] === null)
			{
				//p/<project_name>/admin/marks/disable/
				$this->xml_loader->add_xml(new project_admin_taxonomy_module_disable_xml_page($taxonomy_selected_item_id, $project_id));
				$this->xml_loader->add_xslt("project_admin_{$p[1]}_disable", "project_admin");
			}
			elseif ($p[2] === "pgroups")
			{
				//p/<project_name>/admin/marks/pgroups/...
				if ($p[3] === null)
				{
					//p/<project_name>/admin/marks/pgroups/
					$this->xml_loader->add_xml_with_xslt(new project_admin_precedent_groups_xml_page($project_id), null, "project_admin_403");
				}
				elseif ($p[3] === "add" and $p[4] === null)
				{
					//p/<project_name>/admin/marks/pgroups/add/
					$this->xml_loader->add_xml_with_xslt(new project_admin_precedent_group_add_xml_page($project_id), null, "project_admin_403");
				}
				elseif (is_good_id($p[3]) and $p[4] === null)
				{
					//p/<project_name>/admin/marks/pgroups/<id>/
					$this->xml_loader->add_xml_with_xslt(new project_admin_precedent_group_edit_xml_page($project_id, $p[3]), null, "project_admin_403");
				}
				elseif (is_good_id($p[3]) and $p[4] === "delete" and $p[5] === null)
				{
					//p/<project_name>/admin/marks/pgroups/<id>/delete/
					$this->xml_loader->add_xml_with_xslt(new project_admin_precedent_group_delete_xml_page($project_id, $p[3]), null, "project_admin_403");
				}
			}
		}
		elseif ($p[1] === "members" and $p[2] === null)
		{
			//p/<project_name>/admin/members/
			$taxonomy_selected_item_id = $this->get_taxonomy_module_id_by_module_name("members");
			$this->xml_loader->add_xml_with_xslt(new project_admin_taxonomy_members_xml_page(), null, "project_admin_403");
		}
		elseif ($p[1] === "modules" and $p[2] === null)
		{
			//p/<project_name>/admin/modules/
			$this->xml_loader->add_xml_with_xslt(new project_admin_modules_xml_page(), null, "project_admin_403");
			$this->xml_loader->add_xml(new project_admin_vector_add_xml_ctrl($project_id));
			$this->xml_loader->add_xml(new project_admin_marks_add_xml_ctrl($project_id));
		}
		elseif ($p[1] === "main" and $p[2] === null)
		{
			//p/<project_name>/admin/main/
			$taxonomy_selected_item_id = 0;
			$this->xml_loader->add_xml_with_xslt(new project_admin_taxonomy_main_xml_page(), null, "project_admin_403");
		}
		elseif ($p[1] === "users")
		{
			//p/<project_name>/admin/users/...
			if ($p[2] === null)
			{
				//p/<project_name>/admin/users/ --> //p/<project_name>/admin/users/members/
				$this->set_redirect_url("users/members/");
			}
			elseif ($p[2] == "members" and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				//p/<project_name>/admin/users/members/
				$this->xml_loader->add_xml_with_xslt(new project_admin_members_xml_page($project_id, $page), null, "project_admin_403");
			}
			elseif ($p[2] == "pretenders" and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				//p/<project_name>/admin/users/pretenders/
				// @dm9 унифицировать порядок. Иногда $project_id, $page, иногда наоборот.
				$this->xml_loader->add_xml_with_xslt(new project_admin_pretenders_xml_page($project_id, $page), null, "project_admin_403");
			}
			elseif ($p[2] == "moderators" and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				//p/<project_name>/admin/users/moderators/
				$this->xml_loader->add_xml_with_xslt(new project_admin_moderators_xml_page($project_id, $page), null, "project_admin_403");
			}
			elseif ($p[2] == "admins" and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				//p/<project_name>/admin/users/admins/
				$this->xml_loader->add_xml_with_xslt(new project_admin_admins_xml_page($project_id, $page), null, "project_admin_403");
			}
		}
		elseif ($p[1] === "integration")
		{
			//p/<project_name>/admin/integration/...
			if ($p[2] === null)
			{
				//p/<project_name>/admin/integration/ -> /p/<project_name>/admin/integration/intmenu/
				$this->set_redirect_url("integration/endpoint/");
			}
//			elseif ($p[2] == "intmenu")
//			{
//				//p/<project_name>/admin/integration/intmenu/...
//				if ($p[3] === null)
//				{
//					//p/<project_name>/admin/integration/intmenu/
//					$this->xml_loader->add_xml_with_xslt(new project_admin_intmenu_xml_page($project_id));
//				}
//				elseif ($p[3] === "add" and $p[4] === null)
//				{
//					//p/<project_name>/admin/integration/intmenu/add/
//					$this->xml_loader->add_xml_with_xslt(new project_admin_intmenu_item_add_xml_page($project_id));
//				}
//				elseif (is_good_id($p[3]) and $p[4] === null)
//				{
//					//p/<project_name>/admin/integration/intmenu/<id>/
//					$this->xml_loader->add_xml_with_xslt(new project_admin_intmenu_item_edit_xml_page($project_id, $p[3]));
//				}
//				elseif (is_good_id($p[3]) and $p[4] === "delete" and $p[5] === null)
//				{
//					//p/<project_name>/admin/integration/intmenu/<id>/delete/
//					$this->xml_loader->add_xml_with_xslt(new project_admin_intmenu_item_delete_xml_page($project_id, $p[3]));
//				}
//			}
			elseif ($p[2] == "endpoint" and $p[3] === null)
			{
				//p/<project_name>/admin/integration/endpoint/
				$this->xml_loader->add_xml_with_xslt(new project_admin_endpoint_xml_page($project_id));
			}
		}
		elseif ($p[1] == "static-pages")
		{
			if ($p[2] === null)
			{
				//p/<project_name>/admin/static-pages/
				$this->set_redirect_url("");
			}
			elseif ($static_page_id = $this->get_static_page_id_by_name($p[2]))
			{
				$taxonomy_selected_item_id = $static_page_id;
				if ($p[3] === null)
				{
					//p/<project_name>/admin/static-pages/<name>/
					$this->xml_loader->add_xml_with_xslt(new project_admin_static_page_edit_xml_page($static_page_id, $project_id));
				}
				elseif ($file_id = $this->is_type_folder($p[3], "file", true) and $p[4] === null)
				{
					//p/<project_name>/admin/static-pages/<name>/file-<id>/
					$this->xml_loader->add_xml_with_xslt(new project_static_page_file_download_xml_page($file_id, $project_id, $static_page_id));
				}
				elseif ($p[3] == "delete" and $p[4] === null)
				{
					//p/<project_name>/admin/static-pages/<name>/delete/
					$this->xml_loader->add_xml_with_xslt(new project_admin_static_page_delete_xml_page($static_page_id, $project_id));
				}
			}
		}
		elseif ($p[1] == "links")
		{
			if ($p[2] === null)
			{
				//p/<project_name>/admin/links/
				$this->set_redirect_url("");
			}
			elseif (is_good_id($taxonomy_link_id = $p[2]))
			{
				$taxonomy_selected_item_id = $taxonomy_link_id;
				if ($p[3] === null)
				{
					//p/<project_name>/admin/links/<id>/
					$this->xml_loader->add_xml_with_xslt(new project_admin_taxonomy_link_edit_xml_page($taxonomy_link_id, $project_id));
				}
				elseif ($p[3] == "delete" and $p[4] === null)
				{
					//p/<project_name>/admin/links/<id>/delete/
					$this->xml_loader->add_xml_with_xslt(new project_admin_taxonomy_link_delete_xml_page($taxonomy_link_id, $project_id));
				}
			}
		}
		elseif ($p[1] == "folders")
		{
			if ($p[2] === null)
			{
				//p/<project_name>/admin/folders/
				$this->set_redirect_url("");
			}
			elseif (is_good_id($taxonomy_folder_id = $p[2]))
			{
				$taxonomy_selected_item_id = $taxonomy_folder_id;
				if ($p[3] === null)
				{
					//p/<project_name>/admin/folders/<id>/
					$this->xml_loader->add_xml_with_xslt(new project_admin_taxonomy_folder_edit_xml_page($taxonomy_folder_id, $project_id));
				}
				elseif ($p[3] == "delete" and $p[4] === null)
				{
					//p/<project_name>/admin/folders/<id>/delete/
					$this->xml_loader->add_xml_with_xslt(new project_admin_taxonomy_folder_delete_xml_page($taxonomy_folder_id, $project_id));
				}
			}
		}
		$this->xml_loader->add_xml(new project_admin_taxonomy_xml_ctrl($project_obj, $taxonomy_selected_item_id));
		$this->xml_loader->add_xml(new project_admin_taxonomy_add_form_xml_ctrl($project_obj));
	}

	private function get_user_id_by_login($login)
	{
		return url_helper::get_user_id_by_login($login);
	}

	private function get_static_page_id_by_name($static_page_name)
	{
		return url_helper::get_static_page_id_by_name($static_page_name, $this->project_id);
	}

	private function get_taxonomy_module_id_by_module_name($module_name)
	{
		return url_helper::get_taxonomy_module_id_by_module_name($module_name, $this->project_id);
	}

}

?>