<?php

class project_admin_taxonomy_module_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
		"project_modify_sql",
		"taxonomy_module_modify_sql",
		"project_taxonomy_clean_cache",
	);
	// Settings
	protected $db_table = "project_taxonomy_item";
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function on_after_commit()
	{
		$modules = array(
			"communities" => "communities_are_on",
			"vector" => "vector_is_on",
			"events" => "events_are_on",
			"materials" => "materials_are_on",
			"roles" => "role_recommendation_is_on",
			"marks" => "marks_are_on",
		);

		$this->db->sql("
			UPDATE project
			SET {$modules[$this->old_db_row["module_name"]]} = 0
			WHERE id = {$this->project_id}
		");
	}

	public function clean_cache()
	{
		taxonomy_module_cache_tag::init($this->id)->update();
		project_cache_tag::init($this->project_id)->update();
	}

}

?>