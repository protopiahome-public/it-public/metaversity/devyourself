<?php

class project_admin_taxonomy_module_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
		"project_admin_taxonomy_item_add",
		"project_before_commit",
		"project_taxonomy_clean_cache",
		"project_admin_taxonomy_item_add",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $dt_name = "taxonomy_module";
	protected $axis_name = "add";
	protected $module_name;

	public function init()
	{
		$this->module_name = POST("module_name");
		if ($this->module_name == "vector")
		{
			$this->save_loader->add_ctrl(new project_admin_vector_add_save_ctrl());
		}
		elseif ($this->module_name == "marks")
		{
			$this->save_loader->add_ctrl(new project_admin_marks_add_save_ctrl());
		}
	}

	public function on_before_start()
	{
		$modules = array(
			"communities" => "Сообщества",
			"vector" => "Вектор",
			"roles" => "Рекомендация ролей",
			"events" => "События",
			"materials" => "Материалы",
			"marks" => "Прецеденты"
		);
		if (!array_key_exists($this->module_name, $modules))
		{
			return false;
		}
		$_POST["title"] = $modules[$this->module_name];

		$module_enabled = $this->db->row_exists("
			SELECT id 
			FROM project_taxonomy_item 
			WHERE project_id = {$this->project_id} AND type = 'module' 
				AND module_name = '{$this->module_name}'");
		if ($module_enabled)
		{
			return false;
		}

		return true;
	}

	public function on_before_commit()
	{
		$modules_positions = array(
			"communities" => true,
			"vector" => true,
			"roles" => true,
			"events" => true,
			"materials" => true,
			"marks" => true,
			"members" => true
		);
		$modules_added_positions = $this->db->fetch_column_values("
			SELECT id, module_name, position
			FROM project_taxonomy_item
			WHERE project_id = {$this->project_id} AND type = 'module' AND parent_id IS NULL
		", "position", "module_name");

		$end_position = $this->db->get_value("SELECT MAX(position)+1 FROM project_taxonomy_item WHERE parent_id IS NULL and project_id = {$this->project_id}");
		$this->update_array["position"] = $end_position;

		reset($modules_positions);
		if ($this->module_name == key($modules_positions))
		{
			$this->update_array["position"] = 0;
		}
		else
		{
			end($modules_positions);
			while (key($modules_positions) != $this->module_name)
			{
				prev($modules_positions);
			}
			do
			{
				prev($modules_positions);
				if (isset($modules_added_positions[key($modules_positions)]))
				{
					$this->update_array["position"] = $modules_added_positions[key($modules_positions)] + 1;
					break;
				}
			}
			while (current($modules_positions));
		}

		if ($this->update_array["position"] < $end_position)
		{
			$this->db->sql("
				UPDATE project_taxonomy_item
				SET position = position + 1
				WHERE project_id = {$this->project_id} AND position >= {$this->update_array["position"]}
			");
		}

		$this->update_array["type"] = "'module'";
		$this->update_array["show_in_menu"] = "1";
		$this->update_array["module_name"] = "'{$this->module_name}'";
		return true;
	}

	public function on_after_commit()
	{
		$modules_settings = array(
			"communities" => "communities_are_on",
			"vector" => "vector_is_on",
			"events" => "events_are_on",
			"materials" => "materials_are_on",
			"roles" => "role_recommendation_is_on",
			"marks" => "marks_are_on",
		);

		$this->db->sql("
			UPDATE project
			SET {$modules_settings[$this->module_name]} = 1
			WHERE id = {$this->project_id}
		");

		$modules_titles = array(
			"communities" => "Сообщества",
			"vector" => "Вектор",
			"roles" => "Рекомендация ролей",
			"events" => "События",
			"materials" => "Материалы",
			"marks" => "Прецеденты"
		);
		$this->pass_info->write_info("module_added");
		$this->pass_info->write_info("module_name", $this->module_name);
		$this->pass_info->write_info("module_title", $modules_titles[$this->module_name]);
	}

	public function clean_cache()
	{
		project_cache_tag::init($this->project_id)->update();
	}

}

?>