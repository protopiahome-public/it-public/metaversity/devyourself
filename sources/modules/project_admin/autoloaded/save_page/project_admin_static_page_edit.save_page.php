<?php

class project_admin_static_page_edit_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
		"static_page_dt_init",
		"project_modify_sql",
		"static_page_modify_sql",
		"project_taxonomy_clean_cache",
		"block_set_dt_save"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $dt_name = "static_page";
	protected $axis_name = "edit";

	public function clean_cache()
	{
		static_page_cache_tag::init($this->id)->update();
	}

}

?>