<?php

class project_admin_role_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
	);
	// Settings
	protected $dt_name = "project_role";
	protected $axis_name = "edit";
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	/**
	 * @var rate_save_ctrl
	 */
	protected $rate_save_ctrl;

	public function init()
	{
		$this->rate_save_ctrl = new rate_multi_link_save_ctrl($this->dt_name, array(0, 1, 2, 3));
		$this->save_loader->add_ctrl($this->rate_save_ctrl);
	}

	public function on_before_start()
	{
		if (!$this->project_obj->get_vector_competence_set_id())
		{
			return false;
		}

		return true;
	}

	public function on_after_start()
	{
		$this->rate_save_ctrl->set_competence_set_id($this->project_obj->get_vector_competence_set_id());
		return true;
	}

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->project_access->can_moderate_roles();
	}

	public function on_before_commit()
	{
		$this->update_array["project_id"] = $this->project_id;
		$this->update_array["adder_user_id"] = $this->user->get_user_id();
		return true;
	}

	public function on_after_commit()
	{
		$this->rate_save_ctrl->set_object_id($this->last_id);
	}

}

?>