<?php

class project_admin_intmenu_item_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $dt_name = "project_intmenu_item";
	protected $axis_name = "edit";

	public function on_before_commit()
	{
		$this->update_array["project_id"] = "'{$this->project_id}'";
		$this->update_array["position"] = "'999999'";
		return true;
	}

	public function clean_cache()
	{
		project_intmenu_cache_tag::init($this->project_id)->update();
	}

}

?>