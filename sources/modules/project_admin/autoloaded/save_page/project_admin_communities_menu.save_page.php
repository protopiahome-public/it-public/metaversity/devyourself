<?php

class project_admin_communities_menu_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $action;
	protected $add_community_url;
	protected $add_community_id;
	protected $delete_community_id;

	public function set_up()
	{
		$this->project_id = POST("project_id");
		$this->action = POST("action");

		$this->add_community_url = POST("add_community_url");
		$this->delete_community_id = POST("delete_community_id");

		return true;
	}

	public function start()
	{
		if (!($this->action == "add" or $this->action == "delete"))
		{
			return false;
		}

		if ($this->action == "delete")
		{
			if (!is_good_id($this->delete_community_id))
			{
				return false;
			}
		}

		$this->pass_info->write_info("action", $this->action);

		return true;
	}

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->project_access->has_admin_rights();
	}

	public function check()
	{
		if ($this->action == "add")
		{
			if (!$this->add_community_id = url_helper::extract_community_id($this->add_community_url, $this->project_id))
			{
				$this->pass_info->write_error("wrong_add_community_url");
				$this->pass_info->dump_vars();
				return false;
			}

			$item_exists = $this->db->row_exists("
				SELECT * 
				FROM communities_menu_item 
				WHERE project_id = {$this->project_id} AND community_id = {$this->add_community_id}
			");
			if ($item_exists)
			{
				$this->pass_info->write_error("add_community_already_exists");
				$this->pass_info->dump_vars();
				return false;
			}
		}

		if ($this->action == "delete")
		{
			$item_exists = $this->db->row_exists("
				SELECT * 
				FROM communities_menu_item 
				WHERE project_id = {$this->project_id} AND community_id = {$this->delete_community_id}
			");
			if (!$item_exists)
			{
				$this->pass_info->write_error("wrong_delete_community");
				return false;
			}
		}

		return true;
	}

	public function commit()
	{
		if ($this->action == "add")
		{
			$this->db->sql("
				INSERT INTO communities_menu_item (project_id, community_id)
				VALUES ({$this->project_id}, {$this->add_community_id})
			");
			$this->pass_info->write_info("SAVED");
		}

		if ($this->action == "delete")
		{
			$this->db->sql("
				DELETE FROM communities_menu_item
				WHERE project_id = {$this->project_id} AND community_id = {$this->delete_community_id}
			");
			$this->pass_info->write_info("DELETED");
		}

		return true;
	}

	public function clean_cache()
	{
		communities_menu_cache_tag::init($this->project_id)->update();
	}

}

?>