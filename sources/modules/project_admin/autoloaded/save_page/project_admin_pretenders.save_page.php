<?php

class project_admin_pretenders_save_page extends base_admin_pretenders_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	protected function get_access_save($user_id)
	{
		return new project_access_save($this->project_obj, $user_id);
	}

}

?>