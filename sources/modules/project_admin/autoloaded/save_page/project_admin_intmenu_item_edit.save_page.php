<?php

class project_admin_intmenu_item_edit_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
		"project_modify_sql"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $dt_name = "project_intmenu_item";
	protected $axis_name = "edit";

	public function clean_cache()
	{
		project_intmenu_cache_tag::init($this->project_id)->update();
	}

}

?>