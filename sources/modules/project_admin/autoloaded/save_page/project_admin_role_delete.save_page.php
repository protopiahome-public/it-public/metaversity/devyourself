<?php

class project_admin_role_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_modify_sql",
	);
	// Settings
	protected $db_table = "project_role";
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->project_access->can_moderate_roles();
	}

	public function on_before_commit()
	{
		$this->db->sql("
			DELETE rate r
			FROM rate r
			LEFT JOIN {$this->db_table}_rate_link rl ON (r.id = rl.rate_id)
			WHERE rl.{$this->db_table}_id = {$this->old_db_row["id"]};
		");

		return true;
	}

}

?>