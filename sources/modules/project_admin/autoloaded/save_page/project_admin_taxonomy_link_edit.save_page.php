<?php

class project_admin_taxonomy_link_edit_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
		"project_modify_sql",
		"taxonomy_link_modify_sql",
		"project_taxonomy_clean_cache",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $dt_name = "taxonomy_link";
	protected $axis_name = "edit";

}

?>