<?php

class project_admin_roles_settings_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
		"project_admin_taxonomy_module_edit",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

}

?>