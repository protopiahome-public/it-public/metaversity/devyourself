<?php

class project_admin_custom_feed_sources_save_page extends base_admin_custom_feed_sources_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
	);
	protected $container_name = "project";
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	protected function get_container_id()
	{
		return $this->project_id;
	}

	public function clean_cache()
	{
		project_custom_feed_sources_cache_tag::init($this->feed_id)->update();
	}

}

?>