<?php

class project_admin_custom_feed_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
		"project_before_commit",
		"project_custom_feed_dt_init",
		"project_clean_cache",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $dt_name = "project_custom_feed";
	protected $axis_name = "add";

}

?>