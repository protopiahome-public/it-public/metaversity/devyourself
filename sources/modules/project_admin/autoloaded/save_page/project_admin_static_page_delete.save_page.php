<?php

class project_admin_static_page_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
		"project_modify_sql",
		"static_page_modify_sql",
		"project_taxonomy_clean_cache",
	);
	// Settings
	protected $db_table = "project_taxonomy_item";
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function clean_cache()
	{
		static_page_cache_tag::init($this->id)->update();
	}

}

?>