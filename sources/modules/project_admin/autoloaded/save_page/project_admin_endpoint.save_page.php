<?php

require_once PATH_CORE . "/api_client_interaction.php";

class project_admin_endpoint_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"project_edit_before_start",
		"project_admin_check_rights",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $dt_name = "project";
	protected $axis_name = "edit_endpoint";

	public function clean_cache()
	{
		project_cache_tag::init($this->id)->update();
	}

	public function on_before_commit()
	{
		$endpoint_url = $this->updated_db_row["endpoint_url"];
		$ping_test = sprintf("%04X", mt_rand(0, 65535));
		$req = new api_client_interaction(new ping_api_message($ping_test));
		$result = $req->call($endpoint_url, "ping");
		if ($result === false)
		{
			$this->pass_info->write_field_error("endpoint_url", "PING_FAIL", "REQUEST_FAILED");
			$this->pass_info->write_info("CURL_ERROR", $req->get_curl_errno());
			$this->pass_info->write_info("HTTP_CODE", $req->get_http_code());
			$this->pass_info->dump_vars();
			return false;
		}
		if (!preg_match("/^API_VERSION=([0-9]{8});TEST=([0-9A-F]{4})$/", $result, $matches))
		{
			$this->pass_info->write_field_error("endpoint_url", "PING_FAIL", "INCORRECT_RESPONSE");
			$this->pass_info->dump_vars();
			return false;
		}
		if ($matches[1] != api_client_interaction::API_VERSION)
		{
			$this->pass_info->write_field_error("endpoint_url", "PING_FAIL", "INCORRECT_VERSION");
			$this->pass_info->write_info("API_VERSION", api_client_interaction::API_VERSION);
			$this->pass_info->write_info("YOUR_VERSION", $matches[1]);
			$this->pass_info->dump_vars();
			return false;
		}
		if ($matches[2] != $ping_test)
		{
			$this->pass_info->write_field_error("endpoint_url", "PING_FAIL", "INCORRECT_TEST_VALUE");
			$this->pass_info->dump_vars();
			return false;
		}
		return true;
	}

}

?>