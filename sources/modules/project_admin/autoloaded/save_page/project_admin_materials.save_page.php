<?php

class project_admin_materials_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"project_edit_before_start",
		"project_admin_check_rights",
		"project_admin_taxonomy_module_edit",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $dt_name = "project";
	protected $axis_name = "edit_materials";

	public function clean_cache()
	{
		project_cache_tag::init($this->id)->update();
	}

}

?>