<?php

class project_admin_precedent_group_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
		"project_modify_sql"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $db_table = "precedent_group";

	public function check()
	{
		if ($this->db->row_exists("SELECT * FROM precedent WHERE {$this->db_table}_id = {$this->id} LIMIT 1"))
		{
			$this->pass_info->write_error("CHILDREN_EXIST");
			return false;
		}
		return true;
	}

}

?>