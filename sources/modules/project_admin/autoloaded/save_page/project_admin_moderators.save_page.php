<?php

class project_admin_moderators_save_page extends base_admin_moderators_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
		"project_stat_clean_cache"
	);
	// Settings
	protected $do_member_check = true;
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	protected function get_access_save($user_id)
	{
		return new project_access_save($this->project_obj, $user_id);
	}

	protected function get_rights_array()
	{
		return array(
			"is_role_moderator",
			"is_event_moderator",
			"is_material_moderator",
			"is_precedent_moderator",
			"is_mark_moderator",
			"is_community_moderator",
			"is_widget_moderator",
		);
	}

}

?>