<?php

class project_admin_custom_feed_edit_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
		"project_modify_sql",
		"project_custom_feed_dt_init"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $dt_name = "project_custom_feed";
	protected $axis_name = "edit";

	public function clean_cache()
	{
		project_custom_feed_cache_tag::init($this->id)->update();
	}

}

?>