<?php

class project_admin_taxonomy_helper extends base_db_helper
{

	/**
	 *
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct(project_obj $project_obj)
	{
		$this->project_obj = $project_obj;
		parent::__construct();
	}

	public function get_nodes_data($selected_item_id = 0)
	{
		$data_raw_plain = $this->db->fetch_all("
			SELECT id, parent_id, title, position, type, module_name, link_url, static_page_name
			FROM project_taxonomy_item
			WHERE project_id = {$this->project_obj->get_id()}
			ORDER BY position, id
		");
		$data_raw = array();
		foreach ($data_raw_plain as $row)
		{
			$data_raw[$row["id"]] = array(
				"name" => $row["title"],
				"id" => "taxonomy_node_item_" . $row["id"],
				"jq-item-id" => $row["id"],
				"rel" => $row["type"],
				"module_name" => $row["module_name"],
				"link_url" => $row["link_url"],
				"static_page_name" => $row["static_page_name"],
			);

			if ($selected_item_id == $row["id"])
			{
				$data_raw[$row["id"]]["selected_item"] = 1;
			}
		}
		foreach ($data_raw_plain as $row)
		{
			if ($row["parent_id"])
			{
				if (!isset($data_raw[$row["parent_id"]]["children"]))
				{
					$data_raw[$row["parent_id"]]["children"] = array();
				}
				$data_raw[$row["parent_id"]]["children"][] = &$data_raw[$row["id"]];
			}
		}
		
		$data = array();
		
		$data[] = array(
			"name" => "Главная",
			"id" => "taxonomy_node_root_" . 0,
			"jq-item-id" => 0,
			"rel" => "root",
		);
		if ($selected_item_id == 0)
		{
			$data[0]["selected_item"] = 1;
		}
		
		foreach ($data_raw_plain as $row)
		{
			if (!$row["parent_id"])
			{
				$data[] = &$data_raw[$row["id"]];
			}
		}

		$result = $data;

		
		
		return $result;
	}

	public function get_avaliable_modules()
	{
		
	}

	public function get_module_title($module_name)
	{
		
	}

}

?>
