<?php

class project_admin_taxonomy_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
	);
	protected $autostart_db_transaction = false;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function init()
	{
		$this->db->begin();
	}

	public function get_data()
	{
		$operation = REQUEST("operation");

		switch ($operation)
		{
			case "get_nodes":
				$result = $this->get_nodes();
				break;
			case "move_item":
				$result = $this->move_item();
				break;
			default:
				$result = array("status" => "error", "error" => "wrong_operation");
		}

		if ($result["status"] == "OK")
		{
			if ($operation != "get_nodes")
			{
				project_taxonomy_cache_tag::init($this->project_id)->update();
			}

			$this->db->commit();
		}
		else
		{
			$this->db->rollback();
		}

		return $result;
	}

	public function get_nodes()
	{
		$selected_item_id = POST("selected_item_id");

		if (!is_good_num($selected_item_id))
		{
			return array("status" => "ERROR", "error" => "wrong_input");
		}

		$project_admin_taxonomy_helper = new project_admin_taxonomy_helper($this->project_obj);
		$data = $project_admin_taxonomy_helper->get_nodes_data($selected_item_id);
		$result["status"] = "OK";
		$result["data"] = $data;
		return $result;
	}

	protected function move_item()
	{
		$id = POST("id");
		$parent_id = POST("parent_id");
		$position = POST("position");

		if (!is_good_id($id) or !is_good_num($parent_id) or !is_good_num($position))
		{
			return array("status" => "ERROR", "error" => "wrong_input");
		}
		if (!$item_row = $this->item_exists($id))
		{
			return array("status" => "ERROR", "error" => "wrong_item");
		}
		if ($parent_id)
		{
			if (!$parent_row = $this->item_exists($parent_id))
			{
				return array("status" => "ERROR", "error" => "wrong_parent");
			}
			if (!($parent_row["type"] == "folder" || $parent_row["type"] == "static_page"))
			{
				return array("status" => "ERROR", "error" => "wrong_parent");
			}
			if ($parent_row["type"] == "folder" && $item_row["type"] == "folder")
			{
				return array("status" => "ERROR", "error" => "wrong_parent");
			}
			if ($parent_row["type"] == "static_page" && $item_row["type"] != "static_page")
			{
				return array("status" => "ERROR", "error" => "wrong_parent");
			}

			$grandparent_exists = $this->db->get_value("SELECT parent_id FROM project_taxonomy_item WHERE id = {$parent_id} AND project_id = {$this->project_id}");
			if ($grandparent_exists)
			{
				return array("status" => "ERROR", "error" => "third_level");
			}

			$child_exists = $this->db->row_exists("SELECT id FROM project_taxonomy_item WHERE parent_id = {$id} AND project_id = {$this->project_id}");
			if ($child_exists)
			{
				return array("status" => "ERROR", "error" => "third_level");
			}
		}

		$this->db->sql("
			UPDATE project_taxonomy_item
			SET parent_id = " . ($parent_id ? $parent_id : "NULL") . ",
			edit_time = NOW()
			WHERE
				id = {$id}
				AND project_id = {$this->project_id}
		");

		$positions = $this->db->fetch_all("
			SELECT id, position
			FROM project_taxonomy_item
			WHERE
				parent_id " . ($parent_id ? "= $parent_id" : "IS NULL") . "
				AND id <> {$id}
				AND project_id = {$this->project_id}
			ORDER BY position, id
		");
		array_splice($positions, $position, 0, array(
			array(
				"id" => $id
			)
		));
		foreach ($positions as $item_position => $item)
		{
			$this->db->sql("
				UPDATE project_taxonomy_item 
				SET position = {$item_position}
				WHERE id = {$item["id"]} 
			");
		}

		return array(
			"status" => "OK"
		);
	}

	protected function item_exists($id)
	{
		return $this->db->get_row("
			SELECT *
			FROM project_taxonomy_item
			WHERE id = {$id} AND project_id = {$this->project_id}
		");
	}

}

?>