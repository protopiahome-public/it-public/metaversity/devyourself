<?php

class project_admin_taxonomy_item_add_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $type;
	protected $errors = array();
	protected $title;
	protected $name;
	protected $url;
	protected $id;

	public function start()
	{
		$this->type = POST("type");
		$types = array("static_page", "folder", "link");
		if (!in_array($this->type, $types))
		{
			return false;
		}
		return true;
	}

	public function check()
	{
		$result = true;
		$this->title = POST("title");
		$this->title = trim($this->title);
		if (strlen($this->title) == 0)
		{
			$this->ajax_loader->write_error("title", "BLANK");
			$result = false;
		}

		if ($this->type == "static_page")
		{
			$this->name = POST("name");
			$this->name = trim($this->name);
			if (strlen($this->name) == 0)
			{
				$this->ajax_loader->write_error("name", "BLANK");
				$result = false;
			}
			elseif (in_array($this->name, array(
					"www", "mail", "ns", "ns1", "ns2", "ns3", "ns4", // std. subdomains
					"admin", "god", "moderator", "editor", "user", "all", // roles
					"cp", "add", "create", "edit", "delete", "remove", // actions
					"widget", "members", "users", "admins", "moderators", "settings", "search", "company", "join", // std. URLs
					"co", "post", "feed", "events", "materials", "vector", "roles", "precedents", // URLs
				)))
			{
				$this->ajax_loader->write_error("name", "WRONG");
				$result = false;
			}
			elseif (!preg_match("/^[a-zA-Z0-9\-]+$/", $this->name))
			{
				$this->ajax_loader->write_error("name", "WRONG");
				$result = false;
			}
			elseif (strpos($this->name, "--") !== false)
			{
				$this->ajax_loader->write_error("name", "WRONG");
				$result = false;
			}
			elseif (substr($this->name, 0, 1) == "-" or substr($this->name, -1, 1) == "-")
			{
				$this->ajax_loader->write_error("name", "WRONG");
				$result = false;
			}
			else
			{
				$name_escaped = $this->db->escape($this->name);

				$name_exists = $this->db->row_exists("
					SELECT *
					FROM project_taxonomy_item
					WHERE project_id = {$this->project_id} AND type = 'static_page'
						AND static_page_name = '$name_escaped'
				");
				if ($name_exists)
				{
					$this->ajax_loader->write_error("name", "USED");
					$result = false;
				}
			}
		}
		elseif ($this->type == "link")
		{
			$this->url = POST("url");
			$this->url = trim($this->url);
			if (strlen($this->url) == 0)
			{
				$this->ajax_loader->write_error("url", "BLANK");
				$result = false;
			}
			else
			{
				$tmp = mb_ereg_replace("[а-яА-ЯёЁ]", "_", $this->url);
				$url_regexp = '/^(https?):\/{2}[a-zA-Z0-9;\/?:@&=+$,#_.!~*\'()%,{}|\^\[\]-]+$/';
				if (!preg_match($url_regexp, $tmp))
				{
					$this->ajax_loader->write_error("url", "WRONG");
					$result = false;
				}
			}
		}
		elseif ($this->type == "folder")
		{
			
		}

		return $result;
	}

	public function commit()
	{
		$insert_array = array(
			"project_id" => $this->project_id,
			"type" => "'{$this->type}'",
			"position" => "99999",
			"add_time" => "NOW()",
			"edit_time" => "NOW()",
			"title" => "'" . $this->db->escape($this->title) . "'",
			"show_in_menu" => "1",
		);
		$this->update_array["position"] = $this->db->get_value("SELECT MAX(position)+1 FROM project_taxonomy_item WHERE parent_id IS NULL and project_id = {$this->project_id}");

		if ($this->type == "static_page")
		{
			$insert_array["static_page_name"] = "'" . $this->db->escape($this->name) . "'";
		}
		elseif ($this->type == "link")
		{
			$insert_array["link_url"] = "'" . $this->db->escape($this->url) . "'";
		}
		elseif ($this->type == "folder")
		{
			
		}
		$this->db->insert_by_array("project_taxonomy_item", $insert_array);
		$this->id = $this->db->get_last_id();
		return true;
	}

	public function clean_cache()
	{
		project_taxonomy_cache_tag::init($this->project_id)->update();
	}

	public function get_data()
	{
		return array(
			"status" => "OK",
			"id" => $this->id,
			"name" => $this->name,
		);
	}

}

?>