<?php

class project_admin_materials_categories_ajax_page extends base_resources_categories_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
	);
	protected $resource_name = "material";

	public function clean_cache()
	{
		return materials_categories_cache_tag::init($this->project_id)->update();
	}

}

?>