<?php

class project_admin_static_page_block_set_ajax_page extends base_block_set_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $static_page_id;
	protected $static_page_row;

	public function on_before_start()
	{
		$this->static_page_id = POST("static_page_id");

		if (!is_good_num($this->static_page_id))
		{
			return false;
		}

		if ($this->static_page_id > 0)
		{
			$this->static_page_row = $this->db->get_row("SELECT * FROM project_taxonomy_item WHERE id = {$this->static_page_id} AND type = 'static_page' AND project_id = {$this->project_id} LOCK IN SHARE MODE");
			if (!$this->static_page_row)
			{
				return false;
			}
		}

		return true;
	}

	protected function get_object_url()
	{
		$object_url = "{$this->project_obj->get_url()}admin/static-pages/";

		if ($this->static_page_id)
		{
			$object_url .= "{$this->static_page_row["static_page_name"]}/";
		}
		else
		{
			$object_url .= "add/";
		}

		return $object_url;
	}

}

?>