<?php

class rating_xml_page extends base_easy_xml_ctrl
{

	protected $xml_row_name = "user";
	protected $xml_attrs = array(
		"professiogram_id"
	);
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "user_short"
		)
	);
	protected $professiogram_id = 0;

	public function __construct($page, $professiogram_id)
	{
		$this->page = $page;
		$this->professiogram_id = $professiogram_id;

		parent::__construct();
	}

	private $professiogram_data;
	protected $competence_set_current_page = 1;
	protected $data_one_row = false;
	protected $row_name = "user";
	protected $allow_error_404_if_no_data = false;
	protected $cache = false;
	protected $cache_key_property_name = "professiogram_id";
	protected $documents_per_page = 50;
	protected $dependencies_ctrl = array();
	protected $filter = array(
		"search" => "",
		"group" => 0
	);
	protected $sort_default = "match";

	public function init()
	{
		$this->add_easy_processor(new pager_full_fetch_easy_processor($this->page, 50));

		$processor = new sort_easy_processor();
		$processor->add_order("match", "u.id", "u.id", true);
		$this->add_easy_processor($processor);

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new text_sql_filter("search", "имя/логин", array(
				"u.login",
				"u.first_name",
				"u.last_name"
			)));
		$processor->add_sql_filter(new multi_link_sql_filter("group", "группа", "u.id", "user_user_group_link", "user_id", "user_group_id", "user_group"));
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		// Professiogram data
		$professiogram_data_math = new professiogram_data_math();
		$this->professiogram_data = $professiogram_data = $professiogram_data_math->get_professiograms_data(array(
			$this->professiogram_id
			));
		if (sizeof($professiogram_data) != 1)
		{
			$this->set_error_404();
			return false;
		}
		$competence_set_id = $professiogram_data[0]["competence_set_id"];
		$this->xml_loader->add_xml_by_class_name("competence_set_short_xml_ctrl", $competence_set_id);
		$this->xml_loader->add_xml(new professiogram_show_xml_page($competence_set_id, $this->professiogram_id));

		// Users to process
		$user_id_array = null;
		if (base_sql_filter::is_filter_active())
		{
			// Fetching users
			$select_sql->add_from("user u");
			$select_sql->add_select_fields("u.id");
			$user_id_array = $this->db->fetch_column_values($select_sql->get_sql());
		}

		// Results
		$results_math = new results_math($competence_set_id, $user_id_array, array_keys($professiogram_data[0]["competences"]));
		$results = $results_math->get_results();

		// Filters' output
		$filter_main = $results_math->get_filter_main();
		$filter_raters_real = $results_math->get_filter_raters_real();
		$this->xml_loader->add_xml(new filter_xml_ctrl($filter_main, $filter_raters_real, $results_math->get_used_mark_count(), $results_math->get_used_rater_count()));

		// Match calculation
		$professiogram_match_math = new rate_match_math($results, $professiogram_data);
		$rating = $professiogram_match_math->get_user_rating_for_rate();

		// Building 'data'
		$this->data = array();
		foreach ($rating as $user_id => $match)
		{
			$this->data[] = array(
				"id" => $user_id,
				"match" => $match
			);
		}
	}

	protected function modify_xml(xdom $xdom)
	{
		if ($this->is_error_404())
		{
			return;
		}
		$xdom->set_attr("professiogram_title", $this->professiogram_data[0]["title"]);
		$xdom->set_attr("competence_set_id", $this->professiogram_data[0]["competence_set_id"]);
		if (isset($_COOKIE["user_to_highlight"]))
		{
			$xdom->set_attr("user_to_highlight", $_COOKIE["user_to_highlight"]);
		}
	}

}

?>