<?php

class ratings_xml_page extends base_simple_xml_ctrl
{
	
	protected $data_one_row = false;
	protected $row_name = "competence_set";
	protected $allow_error_404_if_no_data = false;
	protected $cache = false;
	protected $documents_per_page = 25;
	protected $dependencies_ctrl = array (
			"competence_set_full" => "id" 
	);
	protected $filter = array (
			"search" => "" 
	);
	protected $sort_fields = array (
			"title", 
			"marks_trans", 
			"marks", 
			"profs" 
	);
	protected $sort_default = "marks_trans";
	
	public function __construct($current_page)
	{
		$this->current_page = $current_page;
		
		parent::__construct();
	}
	
	protected function before_load()
	{
		if ($this->current_page != 1)
		{
			$this->allow_error_404_if_no_data = true;
		}
	}
	
	protected function set_result()
	{
		// Input filtering
		$where = array ();
		$this->filter["search"] = mb_substr(trim($this->filter["search"]), 0, 255);
		if ($this->filter["search"] !== "")
		{
			$search_quoted = $this->db->escape($this->filter["search"]);
			$where[] = "(cs.title LIKE '%{$search_quoted}%')";
		}
		$where = sizeof($where) ? "WHERE " . join(" AND ", $where) : "";
		
		// Sorting
		$order = "";
		switch ($this->sort)
		{
			case "title":
				$order = "ORDER BY cs.title " . (!$this->sort_back ? "ASC" : "DESC");
				break;
			case "profs":
				$order = "ORDER BY cs.professiogram_count_calc " . (!$this->sort_back ? "DESC" : "ASC");
				break;
			case "marks":
				$order = "ORDER BY total_mark_count_calc " . (!$this->sort_back ? "DESC" : "ASC");
				break;
			case "marks_trans":
				$order = "ORDER BY total_mark_count_with_trans_calc " . (!$this->sort_back ? "DESC" : "ASC");
				break;
		}
		
		// Query
		$this->result = $this->db->fetch_all("
			SELECT SQL_CALC_FOUND_ROWS id
			FROM competence_set cs
			{$where}
			{$order}
			{$this->get_limit_statement()}
		");
	}

}

?>