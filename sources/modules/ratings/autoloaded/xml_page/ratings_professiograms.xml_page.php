<?php

class ratings_professiograms_xml_page extends base_simple_xml_ctrl
{
	
	protected $competence_set_id = 0;
	protected $profession_search = false;
	protected $data_one_row = false;
	protected $row_name = "professiogram";
	protected $allow_error_404_if_no_data = false;
	protected $cache = false;
	protected $cache_key_property_name = "competence_set_id";
	protected $documents_per_page = 50;
	protected $dependencies_ctrl = array (
			"competence_set_full" => "competence_set_id" 
	);
	protected $filter = array (
			"search" => "" 
	);
	protected $competence_set_id_array_as_keys = array ();
	
	public function __construct($current_page, $competence_set_id = 0)
	{
		$this->current_page = $current_page;
		$this->competence_set_id = $competence_set_id;
		
		parent::__construct();
	}
	
	protected function before_load()
	{
		if ($this->current_page != 1)
		{
			$this->allow_error_404_if_no_data = true;
		}
	}
	
	protected function set_result()
	{
		// Where
		$where = array ();
		$where[] = "p.enabled = 1";
		if ($this->competence_set_id)
		{
			$where[] = "p.competence_set_id = {$this->competence_set_id}";
		}
		$this->filter["search"] = mb_substr(trim($this->filter["search"]), 0, 255);
		if ($this->filter["search"] !== "")
		{
			$search_quoted = $this->db->escape($this->filter["search"]);
			$where[] = "(p.title LIKE '%{$search_quoted}%')";
		}
		$where = sizeof($where) ? "WHERE " . join(" AND ", $where) : "";
		
		// Join
		$join = "";
		if (!$this->competence_set_id)
		{
			$join = "LEFT JOIN competence_set cs ON cs.id = p.competence_set_id";
		}
		
		// Order
		if ($this->competence_set_id)
		{
			$order = "ORDER BY p.title";
		}
		else
		{
			$order = "ORDER BY cs.total_mark_count_with_trans_calc DESC, p.title";
		}
		
		// Query
		$this->result = $this->db->fetch_all("
			SELECT SQL_CALC_FOUND_ROWS
				p.id, p.title, p.competence_set_id
			FROM professiogram p
			{$join}
			{$where}
			{$order}
			{$this->get_limit_statement()}
		");
		
		// Extraction of competence sets' IDs
		foreach ($this->result as $data)
		{
			$this->competence_set_id_array_as_keys[$data["competence_set_id"]] = true;
		}
		
		// Zero output
		if ($this->competence_set_id and !sizeof($this->result))
		{
			$this->competence_set_id_array_as_keys[$this->competence_set_id] = true;
			$this->xml_loader->add_xml_by_class_name("competence_set_full_xml_ctrl", $this->competence_set_id);
		}
	}
	
	protected function modify_xml(xdom $xdom)
	{
		$competence_sets_node = $xdom->create_child_node("competence_sets");
		foreach ($this->competence_set_id_array_as_keys as $competence_set_id => $true)
		{
			$competence_sets_node->create_child_node("competence_set")->set_attr("id", $competence_set_id);
		}
	}

}

?>