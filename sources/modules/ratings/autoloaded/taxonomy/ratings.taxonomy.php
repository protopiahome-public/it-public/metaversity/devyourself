<?php

final class ratings_taxonomy extends base_taxonomy
{

	public function run()
	{
		$p = $this->get_parts_relative();

		if ($page = $this->is_page_folder($p[1]) and $p[2] === null)
		{
			//ratings/[page-<page>/]
			$this->xml_loader->add_xml_with_xslt(new ratings_xml_page($page));
			$this->xml_loader->add_xml(new competence_set_fast_navigation_xml_ctrl());
		}
		elseif ($p[1] === "profs" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//ratings/profs/[page-<page>/]
			$this->xml_loader->add_xml_with_xslt(new ratings_professiograms_xml_page($page));
			$this->xml_loader->add_xml(new competence_set_fast_navigation_xml_ctrl());
		}
		elseif ($competence_set_id = $this->is_set_folder($p[1]) and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//ratings/set-<competence_set_id>/[page-<page>/]
			$this->xml_loader->add_xml_with_xslt(new ratings_professiograms_xml_page($page, $competence_set_id));
			$this->xml_loader->add_xml(new competence_set_fast_navigation_xml_ctrl());
		}
		elseif ($professiogram_id = $this->is_prof_folder($p[1]) and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//ratings/prof-<professiogram_id>/[page-<page>/]
			$this->xml_loader->add_xml_with_xslt(new rating_xml_page($page, $professiogram_id));
			$this->xml_loader->add_xml(new competence_set_fast_navigation_xml_ctrl());
		}
	}

	private function is_set_folder($folder_param)
	{
		return $this->is_type_folder($folder_param, "set", $is_good_id = true);
	}

	private function is_prof_folder($folder_param)
	{
		return $this->is_type_folder($folder_param, "prof", $is_good_id = true);
	}

}

?>