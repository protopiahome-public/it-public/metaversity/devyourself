<?php

class community_admin_check_rights_mixin extends base_mixin
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	
	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		$this->community_access = $this->community_obj->get_access();
		return $this->community_access->has_admin_rights();
	}

}

?>