<?php

class community_admin_taxonomy extends base_taxonomy
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, project_obj $project_obj, community_obj $community_obj)
	{
		$this->project_obj = $project_obj;
		$this->community_obj = $community_obj;
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		$p = $this->get_parts_relative();

		$project_obj = $this->project_obj;
		$project_id = $project_obj->get_id();
		$project_access = $project_obj->get_access();

		$community_obj = $this->community_obj;
		$community_id = $community_obj->get_id();
		$community_access = $community_obj->get_access();

		//p/<project_name>/co/<community_name>/admin/...
		if (!$community_access->has_admin_rights())
		{
			$this->xml_loader->set_error_403();
			$this->xml_loader->set_error_403_xslt("community_admin_403", "community");
		}
		elseif ($p[1] === null)
		{
			//p/<project_name>/co/<community_name>/admin/
			$this->xml_loader->add_xml_with_xslt(new community_admin_general_xml_page($community_id, $project_id));
		}
		elseif ($p[1] === "access" and $p[2] === null)
		{
			//p/<project_name>/co/<community_name>/admin/access/
			$this->xml_loader->add_xml_with_xslt(new community_admin_access_xml_page($community_id, $project_id));
		}
		elseif ($p[1] === "design" and $p[2] === null)
		{
			//p/<project_name>/co/<community_name>/admin/design/
			$this->xml_loader->add_xml_with_xslt(new community_admin_design_xml_page($community_id, $project_id));
		}
		elseif ($p[1] === "sections")
		{
			//p/<project_name>/co/<community_name>/admin/sections/
			if ($p[2] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new community_admin_sections_xml_page($community_id, $project_id));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//p/<project_name>/co/<community_name>/admin/sections/add/
				$this->xml_loader->add_xml_with_xslt(new community_admin_section_add_xml_page($community_id, $project_id));
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//p/<project_name>/co/<community_name>/admin/sections/<id>/
				$this->xml_loader->add_xml_with_xslt(new community_admin_section_edit_xml_page($p[2], $community_id, $project_id));
			}
			elseif (is_good_id($p[2]) and $p[3] === "delete" and $p[4] === null)
			{
				//p/<project_name>/co/<community_name>/admin/sections/delete/
				$this->xml_loader->add_xml_with_xslt(new community_admin_section_delete_xml_page($p[2], $community_id, $project_id));
			}
		}
		elseif ($p[1] === "feeds")
		{
			if ($p[2] === null)
			{
				//p/<project_name>/co/<community_name>/admin/feeds/
				$this->xml_loader->add_xml_with_xslt(new community_admin_custom_feeds_xml_page($community_id, $project_id));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//p/<project_name>/co/<community_name>/admin/feeds/add/
				$this->xml_loader->add_xml_with_xslt(new community_admin_custom_feed_add_xml_page($community_id, $project_id), null, "community_admin_403");
			}
			elseif (is_good_id($p[2]))
			{
				$this->xml_loader->add_xml(new community_custom_feed_full_xml_ctrl($p[2], $community_id, $project_id));
				if ($p[3] === null)
				{
					//p/<project_name>/co/<community_name>/admin/feeds/<id>/
					$this->xml_loader->add_xml_with_xslt(new community_admin_custom_feed_edit_xml_page($p[2], $community_id, $project_id), null, "community_admin_403");
				}
				elseif ($p[3] === "sources" and $p[4] === null)
				{
					//p/<project_name>/co/<community_name>/admin/feeds/<id>/sources/
					$this->xml_loader->add_xml_with_xslt(new community_admin_custom_feed_sources_xml_page($p[2], $community_id, $project_id), null, "community_admin_403");
				}
				elseif ($p[3] === "sources" and $p[4] === "add" and $p[5] === null)
				{
					//p/<project_name>/co/<community_name>/admin/feeds/<id>/sources/add/
					$this->xml_loader->add_xml_with_xslt(new community_admin_custom_feed_source_add_xml_page($p[2], $community_id, $project_id), null, "community_admin_403");
				}
				elseif ($p[3] === "delete" and $p[4] === null)
				{
					//p/<project_name>/co/<community_name>/admin/feeds/<id>/delete/
					$this->xml_loader->add_xml_with_xslt(new community_admin_custom_feed_delete_xml_page($p[2], $community_id, $project_id), null, "community_admin_403");
				}
			}
		}
		elseif ($p[1] == "pretenders" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//p/<project_name>/co/<community_name>/admin/pretenders/
			$this->xml_loader->add_xml_with_xslt(new community_admin_pretenders_xml_page($community_id, $project_id, $page));
		}
		elseif ($p[1] == "members" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//p/<project_name>/co/<community_name>/admin/members/
			$this->xml_loader->add_xml_with_xslt(new community_admin_members_xml_page($community_id, $project_id, $page));
		}
		elseif ($p[1] == "admins" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//p/<project_name>/co/<community_name>/admin/admins/
			$this->xml_loader->add_xml_with_xslt(new community_admin_admins_xml_page($community_id, $project_id, $page));
		}
		elseif ($p[1] == "child-communities-pretenders" and $p[2] === null)
		{
			//p/<project_name>/co/<community_name>/admin/child-communities-pretenders/
			$this->xml_loader->add_xml_with_xslt(new community_admin_child_communities_pretenders_xml_page($community_id, $project_id));
		}
		elseif ($p[1] == "child-communities" and $p[2] === null)
		{
			//p/<project_name>/co/<community_name>/admin/child-communities/
			$this->xml_loader->add_xml_with_xslt(new community_admin_child_communities_xml_page($community_id, $project_id));
		}
		elseif ($p[1] == "parent-community" and $p[2] === null)
		{
			//p/<project_name>/co/<community_name>/admin/parent-community/
			$this->xml_loader->add_xml_with_xslt(new community_admin_parent_community_xml_page($community_id, $project_id));
		}
		elseif ($p[1] == "delete" and $p[2] === null)
		{
			//p/<project_name>/co/<community_name>/admin/delete/
			$this->xml_loader->add_xml_with_xslt(new community_admin_delete_xml_page($community_id, $project_id));
		}
	}

}

?>