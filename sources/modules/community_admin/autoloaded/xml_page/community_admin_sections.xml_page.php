<?php

class community_admin_sections_xml_page extends base_easy_xml_ctrl
{

	protected $xml_row_name = "section";
	protected $project_id;
	protected $community_id;

	public function __construct($community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("section dt");
		$select_sql->add_select_fields("dt.id, dt.title, dt.name");
		$select_sql->add_where("dt.community_id = {$this->community_id}");
		$select_sql->add_order("dt.position");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>