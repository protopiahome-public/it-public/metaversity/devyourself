<?php

class community_admin_custom_feed_delete_xml_page extends base_delete_xml_ctrl
{

	// Settings
	protected $db_table = "community_custom_feed";
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	public function __construct($feed_id, $community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);

		parent::__construct($feed_id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("community_id = {$this->community_id}");
	}

}

?>