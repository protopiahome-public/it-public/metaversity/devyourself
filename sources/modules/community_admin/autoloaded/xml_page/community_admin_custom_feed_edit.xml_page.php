<?php

class community_admin_custom_feed_edit_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	protected $dt_name = "community_custom_feed";
	protected $axis_name = "edit";
	// Internal
	protected $project_id;
	protected $community_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	public function __construct($feed_id, $community_id, $project_id)
	{
		$this->project_id = $project_id;
		$this->community_id = $community_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);

		parent::__construct($feed_id);
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_objects($this->project_obj, $this->community_obj);
		return true;
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("community_id = {$this->community_id}");
	}

}

?>