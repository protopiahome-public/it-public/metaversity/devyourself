<?php

class community_admin_section_edit_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	protected $dt_name = "section";
	protected $axis_name = "edit";
	protected $enable_blocks = true;

	/**
	 * @var section_dt
	 */
	protected $dt;
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	public function __construct($section_id, $community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);

		parent::__construct($section_id);
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_objects($this->project_obj, $this->community_obj);
		return true;
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("community_id = " . $this->community_id);
	}

}

?>