<?php

class community_admin_parent_community_xml_page extends base_xml_ctrl
{

	protected $project_id;
	protected $community_id;

	public function __construct($community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		parent::__construct();
	}

	public function get_xml()
	{
		return $this->get_node_string();
	}

}

?>