<?php

class community_admin_delete_xml_page extends base_delete_xml_ctrl
{

	// Settings
	protected $db_table = "community";
	// Internal
	protected $project_id;

	public function __construct($community_id, $project_id)
	{
		$this->project_id = $project_id;

		parent::__construct($community_id);
	}
	
	protected function modify_sql(select_sql $select_sql)
	{
		// In fact, it is not necessary here because there are corresponding checks in taxonomy.
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>