<?php

class community_admin_access_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	protected $dt_name = "community";
	protected $axis_name = "edit_access";
	protected $enable_blocks = true;
	// Internal

	/**
	 * @var community_dt
	 */
	protected $dt;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($id, $project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct($id);
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_project_obj($this->project_obj);
		return true;
	}

}

?>