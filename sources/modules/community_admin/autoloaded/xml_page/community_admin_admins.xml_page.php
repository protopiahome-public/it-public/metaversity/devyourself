<?php

class community_admin_admins_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "user_id",
			"ctrl" => "user_short",
			"param2" => "project_id",
		),
	);
	protected $xml_row_name = "admin";
	protected $page;
	// Internal
	protected $project_id;
	protected $community_id;

	public function __construct($community_id, $project_id, $page)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;
		$this->page = $page;

		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 25));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("ca.*");
		$select_sql->add_from("community_admin ca");
		$select_sql->add_join("LEFT JOIN community_user_link l ON ca.user_id = l.user_id AND ca.community_id = l.community_id");
		$select_sql->add_where("ca.community_id = {$this->community_id} AND ca.is_admin = 1 AND l.status = 'admin'");
		$select_sql->add_order("ca.add_time DESC");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>