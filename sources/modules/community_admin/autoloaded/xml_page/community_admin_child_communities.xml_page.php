<?php

class community_admin_child_communities_xml_page extends base_easy_xml_ctrl
{

	//Settings
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "community_short",
			"param2" => "project_id",
		),
	);
	protected $xml_row_name = "community";
	// Internal
	protected $project_id;
	protected $community_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("date", "add_time DESC", "add_time ASC");
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("posts", "post_count_calc DESC", "post_count_calc ASC");
		$processor->add_order("comments", "comment_count_calc DESC", "comment_count_calc ASC");
		$this->add_easy_processor($processor);
	}

	public function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("id, parent_id, post_count_calc, comment_count_calc, member_count_calc");
		$select_sql->add_from("community");
		$select_sql->add_where("is_deleted = 0");
		$select_sql->add_where("project_id = {$this->project_id} AND parent_id = {$this->community_id} AND parent_approved = 1");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>