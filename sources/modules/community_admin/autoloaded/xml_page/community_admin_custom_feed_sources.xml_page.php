<?php

class community_admin_custom_feed_sources_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "feed_source";
	protected $dependencies_settings = array(
		array(
			"column" => "community_id",
			"ctrl" => "community_short",
			"param2" => "project_id",
		),
	);
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $feed_id;

	public function __construct($feed_id, $community_id, $project_id)
	{
		$this->feed_id = $feed_id;
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);
		$this->community_access = $this->community_obj->get_access();

		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("community_custom_feed_source");
		$select_sql->add_select_fields("*");
		$select_sql->add_where("custom_feed_id = {$this->feed_id}");
		$select_sql->add_order("community_id, section_id");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	protected function postprocess()
	{
		foreach ($this->data as $data_row)
		{
			if ($data_row["section_id"])
			{
				$this->xml_loader->add_xml(new section_full_xml_ctrl($data_row["section_id"], $data_row["community_id"], $this->project_id));
			}
		}
	}

}

?>