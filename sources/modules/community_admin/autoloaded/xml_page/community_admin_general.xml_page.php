<?php

class community_admin_general_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	protected $dt_name = "community";
	protected $axis_name = "edit_general";
	protected $enable_blocks = false;
	// Internal

	/**
	 * @var community_dt
	 */
	protected $dt;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($project_id)
	{
		$this->project_obj = project_obj::instance($project_id);

		parent::__construct($project_id);
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_project_obj($this->project_obj);
		return true;
	}

}

?>