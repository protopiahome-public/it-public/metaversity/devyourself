<?php

class community_admin_members_xml_page extends base_easy_xml_ctrl
{

	protected $xml_row_name = "user";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "user_short",
			"param2" => "project_id",
		),
	);
	protected $project_id;
	protected $community_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($community_id, $project_id, $page)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;
		$this->page = $page;

		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

		$processor = new sort_easy_processor();
		$processor->add_order("time", "l.add_time DESC", "l.add_time ASC");
		if ($this->project_obj->show_user_numbers())
		{
			$processor->add_order("number", "lp.number ASC", "lp.number DESC", false);
		}
		$this->add_easy_processor($processor);

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new text_sql_filter("search", "логин/имя", array("CONCAT('#', u.id)", "u.login", "CONCAT(u.first_name, ' ', u.last_name)", "lp.game_name")));
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("user u");
		$select_sql->add_select_fields("u.id, lp.number, lp.game_name");
		$select_sql->add_select_fields("l.add_time");
		$select_sql->add_join("JOIN community_user_link l ON l.user_id = u.id");
		$select_sql->add_join("LEFT JOIN user_project_link lp ON lp.user_id = u.id AND lp.project_id = {$this->project_id}");
		$select_sql->add_where("l.community_id = {$this->community_id}");
		$select_sql->add_where("l.status <> 'pretender' AND l.status <> 'deleted'");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>