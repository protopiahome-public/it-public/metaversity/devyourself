<?php

class community_admin_custom_feed_source_add_xml_page extends base_xml_ctrl
{

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $feed_id;

	public function __construct($feed_id, $community_id, $project_id)
	{
		$this->feed_id = $feed_id;
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);

		parent::__construct();
	}

	public function get_xml()
	{
		return $this->get_node_string();
	}

}

?>