<?php

abstract class base_admin_custom_feed_sources_save_ctrl extends base_save_ctrl
{

	//Settings
	protected $container_name;
	//Internals
	protected $project_id;
	protected $container_id;
	protected $action;
	protected $feed_id;
	protected $add_item_url;
	protected $add_community_id;
	protected $add_section_id;
	protected $delete_item_id;

	public function set_up()
	{
		$this->action = POST("action");

		$this->feed_id = POST("feed_id");

		$this->add_item_url = POST("add_item_url");
		$this->delete_item_id = POST("delete_item_id");

		return true;
	}

	public function start()
	{
		$this->container_id = $this->get_container_id();
		if (!($this->action == "add" or $this->action == "delete"))
		{
			return false;
		}

		if (!is_good_id($this->feed_id))
		{
			return false;
		}
		$feed_exists = $this->db->row_exists("
			SELECT id
			FROM {$this->container_name}_custom_feed
			WHERE id = {$this->feed_id} AND {$this->container_name}_id = {$this->container_id}
			LOCK IN SHARE MODE
		");
		if (!$feed_exists)
		{
			return false;
		}

		if ($this->action == "delete")
		{
			if (!is_good_id($this->delete_item_id))
			{
				return false;
			}
		}

		$this->pass_info->write_info("action", $this->action);

		return true;
	}

	abstract protected function get_container_id();

	public function check()
	{
		if ($this->action == "add")
		{
			if (!$this->add_community_id = url_helper::extract_community_id($this->add_item_url, $this->project_id))
			{
				$this->pass_info->write_error("wrong_add_item_url");
				$this->pass_info->dump_vars();
				return false;
			}

			$this->add_section_id = url_helper::extract_section_id($this->add_item_url, $this->add_community_id, $this->project_id);

			$item_exists_in_feed = $this->db->row_exists("
				SELECT * 
				FROM {$this->container_name}_custom_feed_source
				WHERE custom_feed_id = {$this->feed_id} AND community_id = {$this->add_community_id}
				AND section_id " . ($this->add_section_id ? "= {$this->add_section_id}" : "IS NULL")
			);
			if ($item_exists_in_feed)
			{
				$this->pass_info->write_error("add_item_already_exists");
				$this->pass_info->dump_vars();
				return false;
			}
		}

		if ($this->action == "delete")
		{
			$item_exists_in_feed = $this->db->row_exists("
				SELECT * 
				FROM {$this->container_name}_custom_feed_source
				WHERE custom_feed_id = {$this->feed_id} AND id = {$this->delete_item_id}
			");
			if (!$item_exists_in_feed)
			{
				$this->pass_info->write_error("wrong_delete_item");
				return false;
			}
		}

		return true;
	}

	public function commit()
	{
		if ($this->action == "add")
		{
			$this->db->sql("
				INSERT INTO {$this->container_name}_custom_feed_source (custom_feed_id, community_id, section_id)
				VALUES ({$this->feed_id}, {$this->add_community_id}, 
					" . ($this->add_section_id ? $this->add_section_id : "NULL" ) . ")
			");
			$this->pass_info->write_info("SAVED");
		}

		if ($this->action == "delete")
		{
			$this->db->sql("
				DELETE FROM {$this->container_name}_custom_feed_source
				WHERE custom_feed_id = {$this->feed_id} AND id = {$this->delete_item_id}
			");
			$this->pass_info->write_info("DELETED");
		}

		return true;
	}

}

?>