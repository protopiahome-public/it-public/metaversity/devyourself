<?php

class community_admin_custom_feed_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_admin_check_rights",
		"community_before_commit",
		"community_custom_feed_dt_init",
		"community_clean_cache",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $dt_name = "community_custom_feed";
	protected $axis_name = "add";

}

?>