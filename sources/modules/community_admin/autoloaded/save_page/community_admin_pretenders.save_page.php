<?php

class community_admin_pretenders_save_page extends base_admin_pretenders_save_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_admin_check_rights",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	protected function get_access_save($user_id)
	{
		return new community_access_save($this->community_obj, $this->project_obj, $user_id);
	}

}

?>