<?php

class community_admin_delete_save_page extends base_delete_virtual_save_ctrl
{

	protected $mixins = array(
		"community_edit_before_start",
		"community_admin_check_rights",
		"project_modify_sql",
		"project_stat_clean_cache",
		"communities_tree_clean_cache",
		"communities_access_clean_cache",
	);
	protected $db_table = "community";
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	public function on_before_commit()
	{
		if ($this->community_obj->get_parent_id())
		{
			$community_child_helper = new community_child_save_helper(new community_obj($this->community_obj->get_parent_id(), $this->project_obj), $this->community_obj);
			$community_child_helper->delete_child();
		}
		return true;
	}

	public function clean_cache()
	{
		community_cache_tag::init($this->id)->update();
		if ($this->old_db_row["parent_id"])
		{
			community_cache_tag::init($this->old_db_row["parent_id"])->update();
		}

		$children_ids_data = $this->db->fetch_all("
			SELECT id
			FROM community
			WHERE parent_id = {$this->id}
		", "id");
		$this->db->sql("
			UPDATE community
			SET parent_id = NULL, parent_approved = 0
			WHERE parent_id = {$this->id}
		");
		foreach ($children_ids_data as $child_id => $child_row)
		{
			community_cache_tag::init($child_id)->update();
		}

		communities_list_cache_tag::init($this->project_id)->update();
	}

}

?>