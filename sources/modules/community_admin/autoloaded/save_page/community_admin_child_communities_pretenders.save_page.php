<?php

class community_admin_child_communities_pretenders_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_admin_check_rights",
		"communities_tree_clean_cache",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $pretender_community_id;

	/**
	 * @var community_obj
	 */
	protected $pretender_community_obj;
	protected $action;

	public function start()
	{
		$communities_accept = array_keys(POST_STARTING_WITH("accept-community-"));
		$communities_decline = array_keys(POST_STARTING_WITH("decline-community-"));
		if (sizeof($communities_accept))
		{
			$this->action = "accept";
			$this->pretender_community_id = $communities_accept[0];
		}
		elseif (sizeof($communities_decline))
		{
			$this->action = "decline";
			$this->pretender_community_id = $communities_decline[0];
		}
		else
		{
			return false;
		}

		if (!is_good_id($this->pretender_community_id))
		{
			return false;
		}

		$this->pretender_community_obj = new community_obj($this->pretender_community_id, $this->project_obj);
		if (!$this->pretender_community_obj->exists())
		{
			return false;
		}
		if (!($this->pretender_community_obj->get_parent_id() == $this->community_id && $this->pretender_community_obj->get_parent_approved() == 0))
		{
			return false;
		}
		return true;
	}

	public function commit()
	{
		if ($this->action == "accept")
		{
			$community_child_helper = new community_child_save_helper($this->community_obj, $this->pretender_community_obj);
			$community_child_helper->add_child();
			subcommunities_helper::send_emails_to_pretender_admins($this->project_obj, $this->community_obj, $this->pretender_community_obj, true);
		}
		else
		{
			$community_child_helper = new community_child_save_helper($this->community_obj, $this->pretender_community_obj);
			$community_child_helper->delete_child();
			subcommunities_helper::send_emails_to_pretender_admins($this->project_obj, $this->community_obj, $this->pretender_community_obj, false);
		}
		return true;
	}

	public function clean_cache()
	{
		community_cache_tag::init($this->community_id)->update();
		community_cache_tag::init($this->pretender_community_id)->update();
	}

}

?>