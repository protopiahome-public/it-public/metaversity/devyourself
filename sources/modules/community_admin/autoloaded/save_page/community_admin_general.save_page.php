<?php

class community_admin_general_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"community_edit_before_start",
		"community_admin_check_rights",
		"community_dt_init",
		"project_modify_sql",
	);
	protected $dt_name = "community";
	protected $axis_name = "edit_general";
	protected $project_id;

	/**
	 * @var community_dt
	 */
	protected $dt;

	/**
	 * @var project_access
	 */
	protected $project_access;

	/**
	 * @var community_access
	 */
	protected $community_access;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	public function clean_cache()
	{
		community_cache_tag::init($this->id)->update();
	}

}

?>