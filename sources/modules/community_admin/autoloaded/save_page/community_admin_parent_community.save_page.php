<?php

class community_admin_parent_community_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_admin_check_rights",
		"communities_tree_clean_cache",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;
	protected $disconnect;
	protected $parent_community_url;
	protected $parent_community_id;

	/**
	 * @var community_obj
	 */
	protected $parent_community_obj;

	public function start()
	{
		$this->disconnect = POST("disconnect");
		if ($this->disconnect != "1")
		{
			// Change/set parent community
			$this->parent_community_url = POST("parent_community_url");
		}
		return true;
	}

	public function check()
	{
		if ($this->disconnect)
		{
			if (!$this->community_obj->get_parent_id())
			{
				return false;
			}
			$this->parent_community_obj = new community_obj($this->community_obj->get_parent_id(), $this->project_obj);
		}
		else
		{
			if (!$this->parent_community_id = url_helper::extract_community_id($this->parent_community_url, $this->project_id))
			{
				$this->pass_info->write_error("WRONG_PARENT_COMMUNITY_URL");
				$this->pass_info->dump_vars();
				return false;
			}
			$this->parent_community_obj = new community_obj($this->parent_community_id, $this->project_obj);
			if (!$this->parent_community_obj->exists())
			{
				$this->pass_info->write_error("WRONG_PARENT_COMMUNITY_URL");
				$this->pass_info->dump_vars();
				return false;
			}
			if ($this->community_obj->get_parent_id() == $this->parent_community_id)
			{
				$this->pass_info->write_error("ALREADY_PARENT");
				$this->pass_info->dump_vars();
				return false;
			}
			if ($this->community_id == $this->parent_community_id)
			{
				$this->pass_info->write_error("PARENT_SELF");
				$this->pass_info->dump_vars();
				return false;
			}

			$ancestor_row["parent_id"] = $this->parent_community_id;
			while ($ancestor_row["parent_id"])
			{
				$ancestor_row = $this->db->get_row("SELECT * FROM community WHERE id = {$ancestor_row["parent_id"]}");
				if ($ancestor_row["id"] == $this->community_id)
				{
					$this->pass_info->write_error("PARENT_SELF");
					$this->pass_info->dump_vars();
					return false;
				}
			}

			if ($this->parent_community_obj->get_allow_child_communities() == "nobody")
			{
				$this->pass_info->write_error("SUBCOMMUNITIES_DISABLED");
				$this->pass_info->dump_vars();
				return false;
			}
		}
		return true;
	}

	public function commit()
	{
		if ($this->disconnect)
		{
			$community_child_helper = new community_child_save_helper($this->parent_community_obj, $this->community_obj);
			$community_child_helper->delete_child();
		}
		else
		{
			if ($this->community_obj->get_parent_id())
			{
				$community_child_helper = new community_child_save_helper(new community_obj($this->community_obj->get_parent_id(), $this->project_obj), $this->community_obj);
				$community_child_helper->delete_child();
			}

			$community_child_helper = new community_child_save_helper($this->parent_community_obj, $this->community_obj);
			$parent_community_access = $this->parent_community_obj->get_access();
			if ($this->parent_community_obj->get_allow_child_communities() == "user_premoderation" and !$parent_community_access->has_admin_rights())
			{
				$community_child_helper->add_pretender();
				subcommunities_helper::send_emails_to_admins_about_pretender($this->project_obj, $this->parent_community_obj, $this->community_obj);
			}
			else
			{
				$community_child_helper->add_child();
			}
		}
		return true;
	}

	public function clean_cache()
	{
		community_cache_tag::init($this->community_id)->update();
		community_cache_tag::init($this->parent_community_id)->update();
	}

}

?>