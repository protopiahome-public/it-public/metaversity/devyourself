<?php

class community_admin_section_edit_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_admin_check_rights",
		"community_admin_sections_dt_init",
		"community_modify_sql",
	);
	protected $dt_name = "section";
	protected $axis_name = "edit";

	/**
	 * @var section_dt
	 */
	protected $dt;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	public function clean_cache()
	{
		section_cache_tag::init($this->id)->update();
		community_sections_cache_tag::init($this->community_id)->update();
		community_posts_cache_tag::init($this->community_id)->update();
	}

}

?>