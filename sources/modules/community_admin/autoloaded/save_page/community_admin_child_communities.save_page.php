<?php

class community_admin_child_communities_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_admin_check_rights",
		"communities_tree_clean_cache",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $delete_community_id;

	/**
	 * @var community_obj
	 */
	protected $delete_community_obj;

	public function start()
	{
		$this->delete_community_id = POST("delete_community_id");
		if (!is_good_id($this->delete_community_id))
		{
			return false;
		}
		$this->delete_community_obj = new community_obj($this->delete_community_id, $this->project_obj);
		if (!$this->delete_community_obj->exists())
		{
			return false;
		}
		if ($this->delete_community_obj->get_parent_id() != $this->community_id)
		{
			return false;
		}
		if (!$this->delete_community_obj->get_parent_approved())
		{
			return false;
		}
		return true;
	}

	public function commit()
	{
		$community_child_helper = new community_child_save_helper($this->community_obj, $this->delete_community_obj);
		$community_child_helper->delete_child();
		return true;
	}

	public function clean_cache()
	{
		community_cache_tag::init($this->community_id)->update();
		community_cache_tag::init($this->delete_community_id)->update();
	}

}

?>