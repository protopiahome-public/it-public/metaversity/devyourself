<?php

class community_admin_custom_feed_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_admin_check_rights",
		"community_modify_sql",
		"community_clean_cache",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $db_table = "community_custom_feed";

	public function clean_cache()
	{
		community_custom_feed_cache_tag::init($this->id)->update();
	}

}

?>