<?php

class community_admin_section_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_admin_check_rights",
		"community_admin_sections_dt_init",
		"community_modify_sql"
	);
	protected $dt_name = "section";
	protected $axis_name = "add";

	/**
	 * @var section_dt
	 */
	protected $dt;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	public function on_before_commit()
	{
		$this->update_array["community_id"] = $this->community_id;
		$this->update_array["position"] = 999999;
		return true;
	}

	public function clean_cache()
	{
		community_sections_cache_tag::init($this->community_id)->update();
	}

}

?>