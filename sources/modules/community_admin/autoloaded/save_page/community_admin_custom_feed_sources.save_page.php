<?php

class community_admin_custom_feed_sources_save_page extends base_admin_custom_feed_sources_save_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_admin_check_rights",
	);
	protected $container_name = "community";
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	protected function get_container_id()
	{
		return $this->community_id;
	}

	public function clean_cache()
	{
		community_custom_feed_sources_cache_tag::init($this->feed_id)->update();
	}

}

?>