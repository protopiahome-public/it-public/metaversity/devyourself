<?php

class community_admin_sections_sort_ajax_page extends base_sort_ajax_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_admin_check_rights",
		"community_modify_sql",
	);

	/* Settings */
	protected $table_name = "section";
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	public function clean_cache()
	{
		community_sections_cache_tag::init($this->community_id)->update();
	}

}

?>