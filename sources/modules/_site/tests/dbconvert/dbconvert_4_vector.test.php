<?php

class dbconvert_4_vector_test_ extends base_test
{

	public function set_up()
	{
		global $config;

		/* dbconvert clean call */
		require_once PATH_DBCONVERT . "/dbconvert_1_clean.php";
		$cv = new dbconvert_1_clean($this->db);
		$cv->run();

		/* dbconvert move data call */
		require_once PATH_DBCONVERT . "/dbconvert_2_load.php";
		$cv = new dbconvert_2_load($this->db, $config["test_db_name"] . "_old_db_2", $config["test_db_name"] . "_old_db_2");
		$cv->run();

		/* dbconvert calc data call */
		require_once PATH_DBCONVERT . "/dbconvert_3_calc.php";
		$cv = new dbconvert_3_calc($this->db);
		$cv->run();

		/* dbconvert vector call */
		require_once PATH_DBCONVERT . "/dbconvert_4_vector.php";
		$cv = new dbconvert_4_vector($this->db, $config["test_db_name"] . "_old_db_2", $config["test_db_name"] . "_old_db_2");
		$cv->run();
	}

	public function table_project_role_test()
	{
		$this->assert_db_row_exists("SELECT * FROM project_role WHERE id = 44 AND add_time = '2009-11-10 14:44:51' AND competence_set_id = 15 AND based_on_professiogram_id = 44");
		$this->assert_db_row_count("project_role", 63); // is_role = 1, projects: 9, 12, 15, 16, 31, 34
		$this->assert_db_row_count("project_role", 63, "WHERE project_id IN (14, 16, 26, 31) AND competence_set_id IN (9, 12, 15, 16, 31, 34)");
		$this->assert_db_row_exists("SELECT * FROM project_role WHERE id = 62 AND title = 'Рисковый инвестор' AND add_time = '2009-11-11 02:57:22' AND competence_set_id = 15 AND project_id = 16");
		$this->assert_db_value("project_role", "competence_count_calc", 63, 8);
	}

	public function table_rate_test()
	{
		// professiogram (id = 97) -> project_role (id = 97)
		$rate_id = $this->db->get_value("SELECT rate_id FROM project_role WHERE id = 97");
		$this->assert_true(is_good_id($rate_id));
		if (is_good_id($rate_id))
		{
			$this->assert_db_row_exists("SELECT * FROM rate WHERE id = {$rate_id} AND rate_type_id = 2 AND add_time = '2010-05-24 15:15:02'");
		}
	}

	public function table_rate_competence_link_test()
	{
		// professiogram (id = 97) -> project_role (id = 97)
		// This test was necessary because the code moved only competences of professiograms, not roles
		$rate_id = $this->db->get_value("SELECT rate_id FROM project_role WHERE id = 97 AND project_id = 16");
		$this->assert_true(is_good_id($rate_id));
		if (is_good_id($rate_id))
		{
			$this->assert_db_row_exists("SELECT * FROM rate_competence_link WHERE rate_id = {$rate_id} AND competence_id = 600 AND mark = 2"); // project_role 97
		}
	}

	public function table_vector_test()
	{
		$this->assert_db_row_exists("
			SELECT *
			FROM vector
			WHERE
				user_id = 158 AND
				competence_set_id = 31 AND
				project_id = 26 AND
				future_professiogram_count_calc = 2 AND
				/*self_competence_count_calc = 0 AND*/
				focus_competence_count_calc = 0 AND
				calculations_base = 'results' AND
				add_time = '2010-10-16 00:00:01' AND
				edit_time = '2010-10-16 00:00:01' AND
				future_edit_time = '2010-10-16 00:00:01' AND
				self_edit_time = '0000-00-00 00:00:00' AND
				focus_edit_time = '0000-00-00 00:00:00'
		");
		$this->assert_db_row_exists("
			SELECT *
			FROM vector
			WHERE
				user_id = 175 AND
				competence_set_id = 31 AND
				project_id = 26 AND
				future_professiogram_count_calc = 2 AND
				/*self_competence_count_calc = 24 AND*/
				focus_competence_count_calc = 0 AND
				calculations_base = 'self' AND
				add_time = '2010-10-16 00:00:01' AND
				edit_time = '2010-10-16 00:00:02' AND
				future_edit_time = '2010-10-16 00:00:01' AND
				self_edit_time = '2010-10-16 00:00:02' AND
				focus_edit_time = '0000-00-00 00:00:00'
		");
		$this->assert_db_row_exists("
			SELECT *
			FROM vector
			WHERE
				user_id = 1122 AND
				competence_set_id = 31 AND
				project_id = 26 AND
				future_professiogram_count_calc = 4 AND
				/*self_competence_count_calc = 159 AND*/
				focus_competence_count_calc = 8 AND
				calculations_base = 'self' AND
				add_time = '2010-10-16 00:00:01' AND
				edit_time = '2010-10-16 00:00:03' AND
				future_edit_time = '2010-10-16 00:00:01' AND
				self_edit_time = '2010-10-16 00:00:02' AND
				focus_edit_time = '2010-10-16 00:00:03'
		");
	}

	public function table_vector_focus_test()
	{
		$vector_id = $this->db->get_value("SELECT id FROM vector WHERE user_id = 1122 AND competence_set_id = 31 AND project_id = 26");
		$this->assert_true(is_good_id($vector_id));
		if (is_good_id($vector_id))
		{
			$this->assert_db_row_exists("SELECT * FROM vector_focus WHERE vector_id = {$vector_id} AND competence_id = 1132 AND add_time = '2010-10-16 00:00:03'");
			$this->assert_db_row_count("vector_focus", 8, "WHERE vector_id = {$vector_id}");
		}
	}

	public function table_vector_focus_history_test()
	{
		$vector_id = $this->db->get_value("SELECT id FROM vector WHERE user_id = 1121 AND competence_set_id = 31 AND project_id = 26");
		$this->assert_true(is_good_id($vector_id));
		if (is_good_id($vector_id))
		{
			$log = $this->db->get_value("
				SELECT log
				FROM vector_focus_history
				WHERE
					vector_id = {$vector_id} AND
					add_time = '2010-10-16 00:00:03'
			");
			$this->assert_equal($log, "+|1200|Календарное планирование многодисциплинарных операций \n+|1201|Планирование и организация многофункциональных программ \n+|1202|способен составлять прогноз основных социально экономических показателей деятельности предприятия, отрасли, региона и экономики в целом\n+|1204|Обладать способностью использовать знание методов и теорий гуманитарных, социальных и экономических наук при осуществлении экспертных и аналитических работ\n+|1207|Выдвигать инновационные идеи и нестандартные подходы к их реализации\n");
		}
	}

	public function table_vector_future_test()
	{
		$vector_id = $this->db->get_value("SELECT id FROM vector WHERE user_id = 1122 AND competence_set_id = 31 AND project_id = 26");
		$this->assert_true(is_good_id($vector_id));
		if (is_good_id($vector_id))
		{
			$this->assert_db_row_exists("SELECT * FROM vector_future WHERE vector_id = {$vector_id} AND professiogram_id = 127 AND add_time = '2010-10-16 00:00:01'");
			$this->assert_db_row_count("vector_future", 4, "WHERE vector_id = {$vector_id}");
		}
	}

	public function table_vector_future_competences_calc_test()
	{
		$vector_id = $this->db->get_value("SELECT id FROM vector WHERE user_id = 28 AND competence_set_id = 15");
		$this->assert_true(is_good_id($vector_id));
		if (is_good_id($vector_id))
		{
			$this->assert_db_row_exists("SELECT * FROM vector_future_competences_calc WHERE vector_id = {$vector_id} AND competence_id = 495 AND mark = 2");
			// 51, not 52 (!) because removed competences shouldn't be in this stat
			$this->assert_db_row_count("vector_future_competences_calc", 51, "WHERE vector_id = {$vector_id}");
		}
	}

	public function table_vector_future_history_test()
	{
		$vector_id = $this->db->get_value("SELECT id FROM vector WHERE user_id = 158 AND competence_set_id = 31 AND project_id = 26");
		$this->assert_true(is_good_id($vector_id));
		if (is_good_id($vector_id))
		{
			$log = $this->db->get_value("
				SELECT log
				FROM vector_future_history
				WHERE
					vector_id = {$vector_id} AND
					add_time = '2010-10-16 00:00:01'
			");
			$this->assert_equal($log, "+|139|Менеджер - управленец\n+|141|Менеджер по персоналу\n");
		}
	}

	public function table_vector_self_test()
	{
		$this->assert_db_row_exists("SELECT * FROM vector_self WHERE user_id = 140 AND competence_id = 487 AND competence_set_id = 15 AND mark = 2");
		$this->assert_db_row_exists("SELECT * FROM vector_self WHERE user_id = 1122 AND competence_id = 1115 AND competence_set_id = 31 AND mark = 3");
	}

	public function table_vector_self_history_test()
	{
		$log = $this->db->get_value("
			SELECT log
			FROM vector_self_history
			WHERE
				user_id = 1122
				AND competence_set_id = 31
				AND add_time = '2010-10-16 00:00:02'
		");
		$parts = explode("\n", $log);
		$this->assert_equal(sizeof($parts), 159 + 1);
		if (isset($parts[6]))
		{
			// "+" - added, "-" - removed, "=" - changed
			// +/-/=|competence_id|competence_set_id|from_mark|to_mark|competence_title
			$this->assert_equal($parts[0], "+|1109|-|2|Знание общих закономерностей физико-химических процессов в объектах автоматизации различной физической природы");
			$this->assert_equal($parts[6], "+|1115|-|3|Умение ориентироваться в современных программно-технических комплексах автоматизации и управления");
		}
	}

	public function table_vector_self_stat_test()
	{
		$this->assert_equal(61, $this->db->get_value("SELECT self_competence_count_calc FROM vector_self_stat WHERE user_id = 28 AND competence_set_id = 15 AND edit_time = '2010-02-15 00:00:03'"));
	}

	public function autojoin_test()
	{
		$this->assert_equal($this->db->get_value("SELECT status FROM user_project_link WHERE user_id = 1152 AND project_id = 16"), "member");
	}

}

?>