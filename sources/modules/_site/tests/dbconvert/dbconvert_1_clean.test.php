<?php

class dbconvert_1_clean_test_ extends base_test
{

	public function set_up()
	{
		/* Undeletable data */
		$this->db->sql("REPLACE INTO user_group (id, title) VALUES (1, 'test')");
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user_JAHNSUW8')");
		}
		$this->db->sql("REPLACE INTO user_user_group_link (user_id, user_group_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO competence_set (id, title) VALUES (1, 'test')");
		$this->db->sql("REPLACE INTO competence_translator (id, from_competence_set_id, to_competence_set_id, title) VALUES (1, 1, 1, 'test')");
		/* Data which MUST be deleted */
		$this->db->sql("REPLACE INTO competence_set_admin (competence_set_id, user_id, is_admin) VALUES (1, 1, 1)");
		$this->db->sql("REPLACE INTO competence_full (id, title, competence_set_id) VALUES (1, 'test', 1)");
		$this->db->sql("REPLACE INTO competence_group (id, title, competence_set_id) VALUES (1, 'test', 1)");
		$this->db->sql("REPLACE INTO competence_link (competence_id, competence_group_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO project (id, name, title) VALUES (1, 'test', 'test')");
		$this->db->sql("REPLACE INTO project_admin (project_id, user_id, is_admin) VALUES (1, 1, 1)");
		$this->db->sql("REPLACE INTO precedent_group (id, title, project_id) VALUES (1, 'test', 1)");
		$this->db->sql("REPLACE INTO precedent (id, project_id, title, descr, adder_user_id, last_editor_user_id) VALUES (1, 1, 'test', '', 1, 1)");
		$this->db->sql("REPLACE INTO precedent_competence_group_link (precedent_id, competence_group_id, competence_set_id) VALUES (1, 1, 1)");
		$this->db->sql("REPLACE INTO precedent_flash_group (id, precedent_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO precedent_flash_group_user_link (id, user_id, precedent_flash_group_id) VALUES (1, 1, 1)");
		$this->db->sql("REPLACE INTO precedent_rater_link (precedent_id, rater_user_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO mark_calc (id, project_id, precedent_group_id, precedent_flash_group_id, precedent_flash_group_user_link_id, ratee_user_id, rater_user_id, competence_id, competence_set_id, type, value, comment, precedent_id) VALUES (1, 1, 1, 1, 1, 1, 1, 1, 1, 'personal', 2, '', 1)");
		$this->db->sql("REPLACE INTO mark_group (precedent_flash_group_id, competence_id, rater_user_id, adder_user_id, value) VALUES (1, 1, 1, 1, 3)");
		$this->db->sql("REPLACE INTO mark_personal (precedent_flash_group_user_link_id, competence_id, rater_user_id, adder_user_id, value) VALUES (1, 1, 1, 1, 3)");
		$this->db->sql("REPLACE INTO rate (id, rate_type_id, title, competence_set_id) VALUES (1, 1, 'test', 1)");
		$this->db->sql("REPLACE INTO project_role (id, title, rate_id, competence_set_id, project_id) VALUES (1, 'Test', 1, 1, 1)");
		$this->db->sql("REPLACE INTO professiogram (id, title, rate_id, competence_set_id, descr) VALUES (1, 'test', 1, 1, '')");
		$this->db->sql("REPLACE INTO rate_competence_link (rate_id, competence_id, mark) VALUES (1, 1, 3)");
		$this->db->sql("REPLACE INTO user_project_link (project_id, user_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO vector (id, project_id, user_id, competence_set_id) VALUES (1, 1, 1, 1)");
		$this->db->sql("REPLACE INTO vector_focus (vector_id, competence_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO vector_focus_history (id, vector_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO vector_future (vector_id, professiogram_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO vector_future_history (id, vector_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO vector_self (user_id, competence_id, competence_set_id, mark) VALUES (1, 1, 1, 1)");
		$this->db->sql("REPLACE INTO vector_self_history (id, competence_set_id, user_id) VALUES (1, 1, 1)");
		$this->db->sql("REPLACE INTO precedent_comment (id, precedent_id, commenter_user_id, position, parent_id, level) VALUES (1, 1, 1, 1, NULL, 1)");
		$this->db->sql("REPLACE INTO event (id, project_id, competence_set_id, title) VALUES (1, 1, 1, 'Test')");
		$this->db->sql("REPLACE INTO event_category (id, project_id, title) VALUES (1, 1, 'Test')");
		$this->db->sql("REPLACE INTO event_category_link (event_id, event_category_id) VALUES (1, 1)");
		/* Data which MUST be deleted - calc tables */
		$this->db->sql("REPLACE INTO competence_calc (competence_id, title, competence_set_id) VALUES (1, 'test', 1)");
		$this->db->sql("REPLACE INTO project_competence_set_link_calc (project_id, competence_set_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO user_competence_set_link_calc (user_id, competence_set_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO user_competence_set_project_link (user_id, competence_set_id, project_id) VALUES (1, 1, 1)");
		$this->db->sql("REPLACE INTO vector_future_competences_calc (vector_id, competence_id, mark) VALUES (1, 1, 1)");
		$this->db->sql("REPLACE INTO vector_self_stat (user_id, competence_set_id) VALUES (1, 1)");

		/* dbconvert clean call */
		require_once PATH_DBCONVERT . "/dbconvert_1_clean.php";
		global $db;
		$cv = new dbconvert_1_clean($this->db);
		$cv->run();
	}

	public function table_competence_calc_test()
	{
		$this->assert_db_row_count("competence_calc", 0);
	}

	public function table_competence_full_test()
	{
		$this->assert_db_row_count("competence_full", 0);
	}

	public function table_competence_group_test()
	{
		$this->assert_db_row_count("competence_group", 0);
	}

	public function table_competence_link_test()
	{
		$this->assert_db_row_count("competence_link", 0);
	}

	public function table_competence_set_test()
	{
		$this->assert_db_row_count_more_or_equal_than("competence_set", 1);
	}

	public function table_competence_set_admin_test()
	{
		$this->assert_db_row_count("competence_set_admin", 0);
	}

	public function table_competence_translator_test()
	{
		$this->assert_db_row_count_more_or_equal_than("competence_translator", 1);
	}

	public function table_event_test()
	{
		$this->assert_db_row_count_more_or_equal_than("event", 0);
	}

	public function table_event_category_test()
	{
		$this->assert_db_row_count_more_or_equal_than("event_category", 0);
	}

	public function table_event_category_link_test()
	{
		$this->assert_db_row_count_more_or_equal_than("event_category_link", 0);
	}

	public function table_mark_calc_test()
	{
		$this->assert_db_row_count("mark_calc", 0);
	}

	public function table_mark_group_test()
	{
		$this->assert_db_row_count("mark_group", 0);
	}

	public function table_mark_personal_test()
	{
		$this->assert_db_row_count("mark_personal", 0);
	}

	public function table_precedent_test()
	{
		$this->assert_db_row_count("precedent", 0);
	}

	public function table_precedent_comment_test()
	{
		$this->assert_db_row_count("precedent_comment", 0);
	}

	public function table_precedent_competence_group_link_test()
	{
		$this->assert_db_row_count("precedent_competence_group_link", 0);
	}

	public function table_precedent_flash_group_test()
	{
		$this->assert_db_row_count("precedent_flash_group", 0);
	}

	public function table_precedent_flash_group_user_link_test()
	{
		$this->assert_db_row_count("precedent_flash_group_user_link", 0);
	}

	public function table_precedent_group_test()
	{
		$this->assert_db_row_count("precedent_group", 0);
	}

	public function table_precedent_rater_link_test()
	{
		$this->assert_db_row_count("precedent_rater_link", 0);
	}

	public function table_professiogram_test()
	{
		$this->assert_db_row_count("professiogram", 0);
	}

	public function table_project_test()
	{
		$this->assert_db_row_count("project", 0);
	}

	public function table_project_admin_test()
	{
		$this->assert_db_row_count("project_admin", 0);
	}

	public function table_project_competence_set_link_calc_test()
	{
		$this->assert_db_row_count("project_competence_set_link_calc", 0);
	}

	public function table_project_intmenu_item_test()
	{
		$this->assert_db_row_count("project_intmenu_item", 0);
	}

	public function table_project_role_test()
	{
		$this->assert_db_row_count("project_role", 0);
	}

	public function table_rate_test()
	{
		$this->assert_db_row_count("rate", 0);
	}

	public function table_rate_competence_link_test()
	{
		$this->assert_db_row_count("rate_competence_link", 0);
	}
	
	public function table_rate_type_test()
	{
		$this->assert_db_row_count("rate_type", 3);
	}

	public function table_university_test()
	{
		$this->assert_db_row_count_more_or_equal_than("university", 8);
	}

	public function table_user_test()
	{
		$this->assert_db_row_count_more_or_equal_than("user", 138, "WHERE id <= 200");
	}

	public function table_user_competence_set_link_calc_test()
	{
		$this->assert_db_row_count("user_competence_set_link_calc", 0);
	}

	public function table_user_competence_set_project_link_test()
	{
		$this->assert_db_row_count("user_competence_set_project_link", 0);
	}

	public function table_user_group_test()
	{
		$this->assert_db_row_count_more_or_equal_than("user_group", 1);
	}

	public function table_user_project_link_test()
	{
		$this->assert_db_row_count("user_project_link", 0);
	}

	public function table_user_user_group_link_test()
	{
		$this->assert_db_row_count_more_or_equal_than("user_user_group_link", 1);
	}

	public function table_vector_test()
	{
		$this->assert_db_row_count("vector", 0);
	}

	public function table_vector_focus_test()
	{
		$this->assert_db_row_count("vector_focus", 0);
	}

	public function table_vector_focus_history_test()
	{
		$this->assert_db_row_count("vector_focus_history", 0);
	}

	public function table_vector_future_test()
	{
		$this->assert_db_row_count("vector_future", 0);
	}

	public function table_vector_future_competences_calc_test()
	{
		$this->assert_db_row_count("vector_future_competences_calc", 0);
	}

	public function table_vector_future_history_test()
	{
		$this->assert_db_row_count("vector_future_history", 0);
	}

	public function table_vector_self_test()
	{
		$this->assert_db_row_count("vector_self", 0);
	}

	public function table_vector_self_history_test()
	{
		$this->assert_db_row_count("vector_self_history", 0);
	}

	public function table_vector_self_stat_test()
	{
		$this->assert_db_row_count("vector_self_stat", 0);
	}

}

?>