<?php

class dbconvert_0_tables_check_test extends base_test
{

	public function set_up()
	{

	}

	public function table_competence_calc_test()
	{
		$this->assert_db_table_not_exist("user_university");
		$this->assert_db_table_not_exist("profile");
		$this->assert_db_table_not_exist("account");
		$this->assert_db_table_not_exist("vector_self_mark");
		$this->assert_db_table_not_exist("vector_self_stat_calc");
		$this->assert_db_table_not_exist("vector_professiograms");
		$this->assert_db_table_not_exist("vector_professiograms_wish_calc");
		$this->assert_db_table_not_exist("vector_history");
		$this->assert_db_table_not_exist("zzz_vector_focus");
		$this->assert_db_table_not_exist("zzz_vector_professiograms");
		$this->assert_db_table_not_exist("zzz_vector_professiograms_wish_calc");
		$this->assert_db_table_not_exist("org");
		$this->assert_db_table_not_exist("precedent_witness_link");
		
		// Please check all other tables which were renamed from '*event*' to '*precedent*'
		$this->assert_db_table_not_exist("event_flash_group");
	}

}

?>