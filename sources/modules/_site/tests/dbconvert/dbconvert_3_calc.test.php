<?php

class dbconvert_3_calc_test_ extends base_test
{

	public function set_up()
	{
		global $config;

		/* dbconvert clean call */
		require_once PATH_DBCONVERT . "/dbconvert_1_clean.php";
		$cv = new dbconvert_1_clean($this->db);
		$cv->run();

		/* dbconvert move data call */
		require_once PATH_DBCONVERT . "/dbconvert_2_load.php";
		$cv = new dbconvert_2_load($this->db, $config["test_db_name"] . "_old_db", $config["test_db_name"] . "_old_db");
		$cv->run();

		$this->db->sql("REPLACE INTO user_group (id, title) VALUES (1, 'test')");
		$this->db->sql("DELETE FROM user_user_group_link WHERE user_group_id = 1");
		$this->db->sql("REPLACE INTO user_user_group_link (user_id, user_group_id) VALUES (1, 1)");
		$this->db->sql("REPLACE INTO user_user_group_link (user_id, user_group_id) VALUES (140, 1)");
		$this->db->sql("REPLACE INTO user_user_group_link (user_id, user_group_id) VALUES (114, 1)");
		if ($this->db->row_exists("SELECT * FROM precedent WHERE id = 942"))
		{
			$this->db->sql("DELETE FROM precedent_comment WHERE precedent_id = 942");
			$this->db->sql("INSERT INTO precedent_comment (precedent_id, commenter_user_id, position, is_deleted, parent_id, level) VALUES (942, 1, 1, 0, NULL, 1)");
			$comment_id = $this->db->get_last_id();
			$this->db->sql("INSERT INTO precedent_comment (precedent_id, commenter_user_id, position, is_deleted, parent_id, level) VALUES (942, 1, 1, 1, {$comment_id}, 1)");
			$this->db->sql("INSERT INTO precedent_comment (precedent_id, commenter_user_id, position, is_deleted, parent_id, level) VALUES (942, 1, 1, 0, {$comment_id}, 1)");
		}
		$this->db->sql("UPDATE competence_translator SET is_default = 0");
		$this->db->sql("INSERT INTO competence_translator (from_competence_set_id, to_competence_set_id, title, is_default) VALUES (12, 9, 'Test', 1)");

		/* dbconvert calc call */
		require_once PATH_DBCONVERT . "/dbconvert_3_calc.php";
		$cv = new dbconvert_3_calc($this->db);
		$cv->run();
	}

	public function table_competence_calc_test()
	{
		$this->assert_db_row_exists("SELECT * FROM competence_calc WHERE competence_id = 299");
		$this->assert_db_row_exists("SELECT * FROM competence_calc WHERE competence_id = 771");
		$this->assert_db_row_not_exist("SELECT * FROM competence_calc WHERE competence_id = 1"); // broken competence_tree_id
		$this->assert_db_row_not_exist("SELECT * FROM competence_calc WHERE competence_id = 1000"); // link_count_calc = 0
		$this->assert_db_row_not_exist("SELECT * FROM competence_calc WHERE competence_id = 1001"); // is_deleted = 1
		$this->assert_db_row_not_exist("SELECT * FROM competence_full WHERE total_mark_count_calc != personal_mark_count_calc + group_mark_count_calc");
	}

	public function table_competence_full_test()
	{
		$this->assert_db_row_exists("SELECT * FROM competence_full WHERE id = 1000 AND link_count_calc = 0");
		$this->assert_db_row_exists("SELECT * FROM competence_full WHERE id = 771 AND link_count_calc = 2");
		$this->assert_db_row_not_exist("SELECT * FROM competence_full WHERE total_mark_count_calc != personal_mark_count_calc + group_mark_count_calc");
		$this->assert_db_row_exists("SELECT * FROM competence_full WHERE id = 439 AND personal_mark_count_calc = 3 AND group_mark_count_calc = 4 AND group_mark_raw_count_calc = 2 AND total_mark_count_calc = 7");
	}

	public function table_competence_group_test()
	{
		// no tests required
	}

	public function table_competence_link_test()
	{
		// no tests required
	}

	public function table_competence_set_test()
	{
		$this->assert_db_row_not_exist("SELECT * FROM competence_set WHERE total_mark_count_calc != personal_mark_count_calc + group_mark_count_calc");
		$this->assert_db_row_exists("SELECT * FROM competence_set WHERE id = 15 AND personal_mark_count_calc = 3516 AND group_mark_count_calc = 3795 AND group_mark_raw_count_calc = 1298 AND total_mark_count_calc = 3795 + 3516");
		$this->assert_db_row_exists("SELECT * FROM competence_set WHERE id = 15 AND professiogram_count_calc = 32");
		$this->assert_db_value("competence_set", "project_count_calc", 9, 2);
		// *with_trans
		$this->assert_db_value("competence_set", "project_count_with_trans_calc", 9, 3);
		$this->assert_db_value("competence_set", "project_count_with_trans_calc", 12, 1);
		$this->assert_db_row_exists("SELECT * FROM competence_set WHERE id = 9 AND personal_mark_count_with_trans_calc = 29 + 140 + 1785 AND group_mark_count_with_trans_calc = 4 + 0 + 2148 AND group_mark_raw_count_with_trans_calc = 2 + 0 + 729 AND total_mark_count_with_trans_calc = 33 + 140 + 3933");
		$this->assert_db_row_exists("SELECT * FROM competence_set WHERE id = 12 AND personal_mark_count_with_trans_calc = 1785 AND group_mark_count_with_trans_calc = 2148 AND group_mark_raw_count_with_trans_calc = 729 AND total_mark_count_with_trans_calc = 3933");
	}
	
	public function table_competence_set_admin_test()
	{
		// no tests required
	}

	public function table_competence_translator_test()
	{
		// no tests required
	}
	
	public function table_event_test()
	{
		// no tests required
	}

	public function table_event_category_test()
	{
		// no tests required
	}
	
	public function table_mark_calc_test()
	{
		$this->assert_db_row_count("mark_calc", 12489 /* personal */ + 10401 /* group */);
	}

	public function table_mark_group_test()
	{
		// no tests required
	}

	public function table_mark_personal_test()
	{
		// no tests required
	}

	public function table_precedent_test()
	{
		$this->assert_db_row_exists("SELECT * FROM precedent WHERE id = 942 AND personal_mark_count_calc = 4");
		$this->assert_db_row_exists("SELECT * FROM precedent WHERE id = 951 AND group_mark_count_calc = 22 AND group_mark_raw_count_calc = 11");
		$this->assert_db_row_exists("SELECT * FROM precedent WHERE id = 941 AND total_mark_count_calc = 1 /* personal */ + 10 /* group */ ");
		$this->assert_db_row_exists("SELECT * FROM precedent WHERE id = 942 AND comment_count_calc = 2"); // WHERE is_deleted = 0
	}

	public function table_precedent_comment_test()
	{
		// no tests required
	}

	public function table_precedent_competence_group_link_test()
	{
		// no tests required
	}

	public function table_precedent_flash_group_test()
	{
		// no tests required
	}

	public function table_precedent_flash_group_user_link_test()
	{
		// no tests required
	}
	
	public function table_precedent_group_test()
	{
		// no tests required
	}

	public function table_precedent_rater_link_test()
	{
		// no tests required
	}
	
	public function table_professiogram_test()
	{
		$this->assert_db_value("professiogram", "competence_count_calc", 97, 15);
	}

	public function table_project_test()
	{
		$this->assert_db_row_not_exist("SELECT * FROM project WHERE total_mark_count_calc != personal_mark_count_calc + group_mark_count_calc");
		$this->assert_db_row_exists("SELECT * FROM project WHERE id = 11 AND personal_mark_count_calc = 5304 AND group_mark_count_calc = 5911 AND group_mark_raw_count_calc = 2018 AND total_mark_count_calc = 5911 + 5304");
		$this->assert_db_value("project", "precedent_count_calc", 14, 279);
		$this->assert_db_value("project", "user_count_calc", 14, 169);
		$this->assert_db_value("project", "main_url_human_calc", 17, "arktika2030.ru");
		$this->assert_db_value("project", "main_url_human_calc", 22, "metagame2010.ru/ignore...");
	}
	
	public function table_project_admin_test()
	{
		// no tests required
	}

	public function table_project_competence_set_link_calc_test()
	{
		$this->assert_db_row_not_exist("SELECT * FROM project_competence_set_link_calc WHERE total_mark_count_calc != personal_mark_count_calc + group_mark_count_calc");
		$this->assert_db_row_exists("SELECT * FROM project_competence_set_link_calc WHERE project_id = 11 AND competence_set_id = 15 AND personal_mark_count_calc = 3486 AND group_mark_count_calc = 3759 AND group_mark_raw_count_calc = 1287 AND total_mark_count_calc = 3759 + 3486");
		// *with_trans
		$this->assert_db_row_exists("SELECT * FROM project_competence_set_link_calc WHERE competence_set_id = 9 AND project_id = 11 AND personal_mark_count_with_trans_calc = 29 + 1785 AND group_mark_count_with_trans_calc = 4 + 2148 AND group_mark_raw_count_with_trans_calc = 2 + 729 AND total_mark_count_with_trans_calc = 33 + 3933");
		$this->assert_db_row_exists("SELECT * FROM project_competence_set_link_calc WHERE competence_set_id = 12 AND project_id = 11 AND personal_mark_count_with_trans_calc = 1785 AND group_mark_count_with_trans_calc = 2148 AND group_mark_raw_count_with_trans_calc = 729 AND total_mark_count_with_trans_calc = 3933");
	}

	public function table_project_intmenu_item_test()
	{
		// no tests required
	}

	public function table_project_role_test()
	{
		// no tests required
	}
	
	public function table_rate_test()
	{
		// no tests required
	}

	public function table_rate_competence_link_test()
	{
		// no tests required
	}

	public function table_rate_type_test()
	{
		// no tests required
	}

	public function table_university_test()
	{
		// no tests required
	}

	public function table_user_test()
	{
		$this->assert_db_row_not_exist("SELECT * FROM user WHERE total_mark_count_calc != personal_mark_count_calc + group_mark_count_calc");
		$this->assert_db_row_exists("SELECT * FROM user WHERE id = 87 AND personal_mark_count_calc = 383 AND group_mark_count_calc = 258");
		$this->assert_db_value("user", "www", 1, "http://test.ru/");
		$this->assert_db_value("user", "www_human_calc", 1, "test.ru");
	}

	public function table_user_competence_set_link_calc_test()
	{
		$this->assert_db_row_not_exist("SELECT * FROM user_competence_set_link_calc WHERE total_mark_count_calc != personal_mark_count_calc + group_mark_count_calc");
		$this->assert_db_row_exists("SELECT * FROM user_competence_set_link_calc WHERE user_id = 87 AND competence_set_id = 15 AND personal_mark_count_calc = 168 AND group_mark_count_calc = 142");
		$this->assert_equal(2, $this->db->get_value("SELECT project_count_calc FROM user_competence_set_link_calc WHERE competence_set_id = 15 AND user_id = 205"));
		$this->assert_db_row_exists("SELECT * FROM user_competence_set_link_calc WHERE competence_set_id = 9 AND user_id = 218 AND personal_mark_count_with_trans_calc = 1 + 44 AND group_mark_count_with_trans_calc = 0 + 58 AND total_mark_count_with_trans_calc = 1 + 102");
		$this->assert_db_row_exists("SELECT * FROM user_competence_set_link_calc WHERE competence_set_id = 12 AND user_id = 218 AND personal_mark_count_with_trans_calc = 44 AND group_mark_count_with_trans_calc = 58 AND total_mark_count_with_trans_calc = 102");
	}

	public function table_user_competence_set_project_link_test()
	{
		$this->assert_db_row_not_exist("SELECT * FROM user_competence_set_project_link WHERE total_mark_count_calc != personal_mark_count_calc + group_mark_count_calc");
		$this->assert_db_row_exists("SELECT * FROM user_competence_set_project_link WHERE user_id = 87 AND competence_set_id = 15 AND project_id = 11 AND personal_mark_count_calc = 168 AND group_mark_count_calc = 142");
		$this->assert_db_row_exists("SELECT * FROM user_competence_set_project_link WHERE user_id = 87 AND competence_set_id = 22 AND project_id = 17 AND personal_mark_count_calc = 22 AND group_mark_count_calc = 0");
	}

	public function table_user_group_test()
	{
		$this->assert_db_row_exists("SELECT * FROM user_group WHERE id = 1 AND user_count_calc = 3");
	}

	public function table_user_project_link_test()
	{
		$this->assert_db_row_not_exist("SELECT * FROM user_project_link WHERE total_mark_count_calc != personal_mark_count_calc + group_mark_count_calc");
		$this->assert_db_row_exists("SELECT * FROM user_project_link WHERE user_id = 87 AND project_id = 11 AND personal_mark_count_calc = 292 AND group_mark_count_calc = 237");
	}

	public function table_user_user_group_link_test()
	{
		// no tests required
	}

	/*public function table_vector_test()
	{
		// no tests required
	}

	public function table_vector_focus_test()
	{
		// no tests required
	}

	public function table_vector_focus_history_test()
	{
		// no tests required
	}

	public function table_vector_future_test()
	{
		// no tests required
	}

	public function table_vector_future_competences_calc_test()
	{
		// no tests required
	}

	public function table_vector_future_history_test()
	{
		// no tests required
	}

	public function table_vector_self_test()
	{
		// no tests required
	}

	public function table_vector_self_history_test()
	{
		// no tests required
	}

	public function table_vector_self_stat_test()
	{
		// no tests required
	}*/

}

?>