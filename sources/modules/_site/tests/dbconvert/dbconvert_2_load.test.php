<?php

/*
  SELECT m.competence_id, eml.member_id, c.competence_tree_id, emg.precedent_id, e.project_id
  FROM mark m
  LEFT JOIN competence c ON c.id = m.competence_id
  LEFT JOIN precedent_member_link eml ON eml.id = m.precedent_member_link_id
  LEFT JOIN precedent_member_group emg ON emg.id = eml.precedent_member_group_id
  LEFT JOIN precedent e ON e.id = emg.precedent_id
  WHERE m.src_competence_tree_id = 0 AND c.is_deleted = 0;
  SELECT gm.competence_id, eml.member_id, c.competence_tree_id, emg.precedent_id, e.project_id
  FROM group_mark gm
  LEFT JOIN competence c ON c.id = gm.competence_id
  LEFT JOIN precedent_member_group emg ON emg.id = gm.precedent_member_group_id
  LEFT JOIN precedent_member_link eml ON eml.precedent_member_group_id = emg.id
  LEFT JOIN precedent e ON e.id = emg.precedent_id
  WHERE gm.src_competence_tree_id = 0 AND c.is_deleted = 0;
  SELECT gm.competence_id, c.competence_tree_id, emg.precedent_id, e.project_id
  FROM group_mark gm
  LEFT JOIN competence c ON c.id = gm.competence_id
  LEFT JOIN precedent_member_group emg ON emg.id = gm.precedent_member_group_id
  LEFT JOIN precedent e ON e.id = emg.precedent_id
  WHERE gm.src_competence_tree_id = 0 AND c.is_deleted = 0;
 */

class dbconvert_2_load_test_ extends base_test
{

	public function set_up()
	{
		global $config;

		/* dbconvert clean call */
		require_once PATH_DBCONVERT . "/dbconvert_1_clean.php";
		$cv = new dbconvert_1_clean($this->db);
		$cv->run();

		/* dbconvert move data call */
		require_once PATH_DBCONVERT . "/dbconvert_2_load.php";
		$cv = new dbconvert_2_load($this->db, $config["test_db_name"] . "_old_db", $config["test_db_name"] . "_old_db");
		$cv->run();
	}

	public function table_competence_calc_test()
	{
		$this->assert_db_row_count("competence_calc", 0);
	}

	public function table_competence_full_test()
	{
		$this->assert_db_row_exists("SELECT * FROM competence_full WHERE id = 299 AND add_time = '2009-10-06 15:35:58'");
		$this->assert_db_row_not_exist("SELECT * FROM competence_full WHERE id = 1"); // broken competence_tree_id
	}

	public function table_competence_group_test()
	{
		$this->assert_db_row_not_exist("SELECT * FROM competence_group WHERE id = 1"); // group with id = 0 becomes a group with id = 1; it shouldn't be moved here
		$this->assert_db_row_exists("SELECT * FROM competence_group WHERE id = 365 AND add_time = '2009-10-06 15:35:28'");
		$this->assert_db_row_not_exist("SELECT * FROM competence_group WHERE id = 367"); // is_deleted = 1
		$this->assert_db_row_exists("SELECT * FROM competence_group WHERE id = 408 AND add_time = '2009-11-10 10:04:07' /*AND position = 3*/ AND level = 0");
		$this->assert_db_row_exists("SELECT * FROM competence_group WHERE id = 424 AND add_time = '2009-11-10 10:14:46' /*AND position = 12*/ AND level = 1");
	}

	public function table_competence_link_test()
	{
		$this->assert_db_row_exists("SELECT * FROM competence_link WHERE competence_id = 299 AND competence_group_id = 365 /*AND position = 1*/"); // в оригинале position везде 0
		$this->assert_db_row_exists("SELECT * FROM competence_link WHERE competence_id = 303 AND competence_group_id = 365 /*AND position = 5*/"); // в оригинале position везде 0
	}

	public function table_competence_set_test()
	{
		$this->assert_db_row_exists("SELECT * FROM competence_set WHERE id = 22 AND add_time = '2010-03-07 15:03:44'");
	}
	
	public function table_competence_set_admin_test()
	{
		$this->assert_db_row_exists("SELECT * FROM competence_set_admin WHERE competence_set_id = 21 AND user_id = 413 AND is_admin = 1 AND is_professiogram_moderator = 0 AND add_time = '2010-01-25 12:15:41'");
	}

	public function table_competence_translator_test()
	{
		// no tests required
	}
	
	public function table_event_test()
	{
		// no tests required
	}

	public function table_event_category_test()
	{
		// no tests required
	}

	public function table_event_category_link_test()
	{
		// no tests required
	}
	
	public function table_mark_calc_test()
	{
		// Data calculation is moved from here to the stage 3
		$this->assert_db_row_count("mark_calc", 0);
	}

	public function table_mark_group_test()
	{
		$this->assert_db_row_count("mark_group", 3395);
	}

	public function table_mark_personal_test()
	{
		$this->assert_db_row_count("mark_personal", 12489);
	}

	public function table_precedent_test()
	{
		$this->assert_db_row_exists("SELECT * FROM precedent WHERE id = 117 AND add_time = '2009-11-29 18:38:30' AND project_id = 11 AND precedent_group_id = 115");
	}

	public function table_precedent_comment_test()
	{
		// no tests required
	}
	
	public function table_precedent_competence_group_link_test()
	{
		$this->assert_db_row_exists("SELECT * FROM precedent_competence_group_link WHERE precedent_id = 87 AND competence_group_id = 365");
	}

	public function table_precedent_flash_group_test()
	{
		$this->assert_db_row_exists("SELECT * FROM precedent_flash_group WHERE id = 282 AND title = 'Основная' AND precedent_id = 245 AND add_time = '2009-12-06 00:52:35'");
	}

	public function table_precedent_flash_group_user_link_test()
	{
		$this->assert_db_row_exists("SELECT * FROM precedent_flash_group_user_link WHERE id = 3023 AND user_id = 1000 AND precedent_flash_group_id = 1407");
	}
	
	public function table_precedent_group_test()
	{
		$this->assert_db_row_exists("SELECT * FROM precedent_group WHERE id = 119 AND add_time = '2009-11-28 01:17:55'");
	}

	public function table_precedent_rater_link_test()
	{
		$this->assert_db_row_exists("SELECT * FROM precedent_rater_link WHERE precedent_id = 74 AND rater_user_id = 148");
		$this->assert_db_row_exists("SELECT * FROM precedent_rater_link WHERE precedent_id = 82 AND rater_user_id = 146");
		$this->assert_db_row_exists("SELECT * FROM precedent_rater_link WHERE precedent_id = 82 AND rater_user_id = 147");
	}

	public function table_professiogram_test()
	{
		$this->assert_db_row_exists("SELECT * FROM professiogram WHERE id = 97 AND add_time = '2010-05-24 15:15:02' AND competence_set_id = 15");
		$this->assert_db_row_count("professiogram", 72); // is_profession = 1
	}

	public function table_project_test()
	{
		$this->assert_db_row_exists("SELECT * FROM project WHERE id = 14 AND name = 'aibolit2010'");
		$this->assert_db_row_not_exist("SELECT * FROM project WHERE id = 1");
	}
	
	public function table_project_admin_test()
	{
		$this->assert_db_row_exists("SELECT * FROM project_admin WHERE project_id = 19 AND user_id = 948 AND is_admin = 1 AND is_role_moderator = 0 AND add_time = '2010-03-24 14:14:22'");
		$this->assert_db_row_exists("SELECT * FROM user_project_link WHERE project_id = 19 AND user_id = 948 AND status = 'admin'");
		
		$this->assert_db_row_exists("SELECT * FROM project_admin WHERE project_id = 16 AND user_id = 133 AND is_admin = 1 AND is_role_moderator = 0 AND add_time = '2009-11-12 18:45:33' AND edit_time = '2009-11-12 18:45:33'");
		$this->assert_db_row_exists("SELECT * FROM user_project_link WHERE project_id = 16 AND user_id = 133 AND status = 'admin'");
		
		$this->assert_db_row_exists("SELECT * FROM project_admin WHERE project_id = 16 AND user_id = 137 AND is_admin = 1 AND is_role_moderator = 0 AND add_time = '2009-11-22 14:11:28' AND edit_time = '2009-11-22 14:11:28'");
		$this->assert_db_row_exists("SELECT * FROM user_project_link WHERE project_id = 16 AND user_id = 137 AND status = 'admin'");
		
		$this->assert_db_row_exists("SELECT * FROM user_project_link WHERE project_id = 17 AND user_id = 1 AND status = 'member'");
	}

	public function table_project_competence_set_link_calc_test()
	{
		// no tests required
	}
	
	public function table_project_intmenu_item_test()
	{
		$this->assert_db_row_exists("SELECT * FROM project_intmenu_item WHERE project_id = 17 AND url = 'http://cmp4.ru/projects/17/' AND add_time = '2010-02-19 02:22:35'");
	}
	
	public function table_project_role_test()
	{
		// no test here - see dbconvert_4
	}

	public function table_rate_test()
	{
		// professiogram (id = 97) -> professiogram (id = 97)
		$rate_id = $this->db->get_value("SELECT rate_id FROM professiogram WHERE id = 97");
		$this->assert_true(is_good_id($rate_id));
		if (is_good_id($rate_id))
		{
			$this->assert_db_row_exists("SELECT * FROM rate WHERE id = {$rate_id} AND rate_type_id = 1 AND add_time = '2010-05-24 15:15:02'");
		}
	}

	public function table_rate_competence_link_test()
	{
		// professiogram (id = 97) -> professiogram (id = 97)
		$rate_id = $this->db->get_value("SELECT rate_id FROM professiogram WHERE id = 97");
		$this->assert_true(is_good_id($rate_id));
		if (is_good_id($rate_id))
		{
			$this->assert_db_row_exists("SELECT * FROM rate_competence_link WHERE rate_id = {$rate_id} AND competence_id = 601 AND mark = 2"); // professiogram 97
		}
	}

	public function table_rate_type_test()
	{
		$this->assert_db_row_count("rate_type", 3);
	}

	public function table_university_test()
	{
		// no tests required
	}

	public function table_user_test()
	{
		$this->assert_db_row_exists("SELECT * FROM user WHERE id = 1 AND login = 'dvs' AND profile_url = 'http://users.idsrv.ru/people/1/'");
		$this->assert_db_row_exists("SELECT * FROM user WHERE id = 14 AND login = 'master' AND www = 'http://vkontakte.ru/id4853301' AND LENGTH(about) > 100 AND profile_id_bak = 3 AND password = '555' AND register_ip = '10.11.12.13' AND edit_time = '2010-02-08 16:53:41'");
	}

	public function table_user_competence_set_link_calc_test()
	{
		// no tests required
	}

	public function table_user_competence_set_project_link_test()
	{
		// no tests required
	}

	public function table_user_group_test()
	{
		// no tests required
	}

	public function table_user_project_link_test()
	{
		$this->assert_db_row_exists("SELECT * FROM user_project_link WHERE project_id = 11 AND user_id = '140'"); // project 11, was 1 but not exists
	}

	public function table_user_user_group_link_test()
	{
		// no tests required
	}
	
	// see all vector tests in dbconvert_4

}

?>