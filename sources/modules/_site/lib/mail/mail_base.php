<?php

define("MAIL_OK", 0);
define("MAIL_ERROR_SYSTEM", 101);

abstract class mail_base
{

	// Служебное 
	protected $line_separator = "\n";
	protected $encoding = "";
	
	// Заполняемое пользователем (классом-наследником)
	protected $to = "";
	protected $from = "";
	protected $subject = "";
	protected $body = "";
	protected $params = array();
	protected $headers = array();
	protected $files = array();
	
	// Подготовленные к отсылке данные
	protected $subject_prepared = "";
	protected $headers_prepared = "";
	protected $body_prepared = "";

	public function __construct($encoding)
	{
		if (strtoupper(substr(PHP_OS, 0, 3)) === "WIN")
		{
			$this->line_separator = "\r\n";
		}
		
		$this->encoding = $encoding;
	}

	protected function prepare()
	{
		$this->headers_prepared = join($this->line_separator, $this->headers);
		$this->subject_prepared = trim($this->subject);
		foreach ($this->params as $idx => $val) {
			$this->subject_prepared = str_replace($idx, $val, $this->subject_prepared);
		}
		$this->subject_prepared = $this->encode_string($this->subject_prepared);
		$this->body_prepared = str_replace("\r\n", "\n", $this->body_prepared);
		$this->body_prepared = str_replace("\n", $this->line_separator, $this->body_prepared);
		return MAIL_OK;
	}
	
	protected function do_send()
	{
		$ret = send_mail($this->to, $this->subject_prepared, $this->body_prepared, $this->headers_prepared);
		/*if (!$ret)
		{
			if (!file_exists("mail_errors"))
			{
				mkdir("mail_errors", 0777, true);
			}
			//file_put_contents("mail_errors/" . substr(md5(rand()), 0, 8) . ".txt", $this->to . "\n\n===\n\n" . $this->subject_prepared . "\n\n===\n\n" . $this->body_prepared . "\n\n===\n\n" . $this->headers_prepared);
			file_put_contents("mail_errors/to", serialize($this->to));
			file_put_contents("mail_errors/subject_prepared", serialize($this->subject_prepared));
			file_put_contents("mail_errors/body_prepared", serialize($this->body_prepared));
			file_put_contents("mail_errors/headers_prepared", serialize($this->headers_prepared));
			die();
		}*/
		return $ret ? MAIL_OK : MAIL_ERROR_SYSTEM;
	}
	
	protected function log()
	{
		return MAIL_OK;
	}

	final public function send()
	{
		$ret = $this->prepare();
		if ($ret != MAIL_OK) 
		{
			return $ret;
		}
		$ret = $this->do_send();
		if ($ret != MAIL_OK)
		{
			return $ret;
		}
		$ret = $this->log();
		if ($ret != MAIL_OK)
		{
			return $ret;
		}
		
	}
	
	protected function encode_string($str)
	{
		$encoding = strtolower($this->encoding);
		return "=?{$encoding}?B?" . base64_encode($str) . "?=";
	}

}

?>