<?php

require_once PATH_MODULE_SITE_LIB . "/mail/mail_base.php";
require_once PATH_MODULE_SITE_LIB . "/mail/mail_text.php";
require_once PATH_MODULE_SITE_LIB . "/mail/mail.int.php";

class mail_html extends mail_text implements mail_interface 
{
	protected $body;

	public function __construct($to, $from, $subject, $body, $params = array(), $files = array(), $encoding = "UTF-8")
	{
		parent::__construct($to, $from, $subject, $body, $params, $files, $encoding);
	}
	
	protected function set_content_type_header()
	{
		$this->headers[] = "Content-Type: text/html; charset={$this->encoding}";
	}

}

?>