<?php

require_once PATH_MODULE_SITE_LIB . "/mail/mail_base.php";

class mail_xslt extends mail_base
{

	/**
	 * @var xdom
	 */
	protected $mail_dom;
	/**
	 * @var xnode
	 */
	protected $user_node;
	/**
	 * @var xnode
	 */
	protected $request_node;
	/**
	 * @var string
	 */
	protected $xslt_path;

	/**
	 * @param string $to
	 * @param string $from
	 * @param string $subject
	 * @param string $xslt_path
	 * @param xdom $mail_dom
	 * @param xnode $request_node
	 * @param xnode $user_node
	 * @param string $encoding
	 */
	public function __construct($to, $from, $xslt_path, xdom $mail_dom, xnode $request_node = null, xnode $user_node = null, $encoding = "UTF-8")
	{
		$this->to = $to;
		$this->from = $from;
		$this->mail_dom = $mail_dom;
		if ($request_node)
		{
			$this->request_node = $this->mail_dom->import_xnode($request_node);
		}

		if ($user_node)
		{
			$this->user_node = $this->mail_dom->import_xnode($user_node);
		}

		$this->headers[] = "From: {$from}";
		$this->headers[] = "Reply-To: {$from}";
		$this->headers[] = "Content-Type: text/html; charset={$this->encoding}";

		$this->line_separator = "\n";
		$this->xslt_path = $xslt_path;

		parent::__construct($encoding);
	}

	/**
	 * @return xnode
	 */
	public function get_mail_node()
	{
		return $this->mail_dom;
	}

	protected function prepare()
	{
		$html = $this->mail_dom->xslt_process(file_get_contents($this->xslt_path));

		$this->headers[] = "Content-Type: text/html; charset=\"{$this->encoding}\"";

		$dom = new DOMDocument("1.0", "UTF-8");
		$dom->loadHTML($html);
		$html_sxml = simplexml_import_dom($dom);
		$this->subject = $this->subject_prepared = $html_sxml->head->title;

		$this->body = $this->body_prepared = $html;

		parent::prepare();
	}

}

?>
