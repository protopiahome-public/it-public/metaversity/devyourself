<?php

require_once PATH_MODULE_SITE_LIB . "/mail/mail_base.php";
require_once PATH_MODULE_SITE_LIB . "/mail/mail.int.php";

class mail_text extends mail_base implements mail_interface 
{
	protected $body;

	public function __construct($to, $from, $subject, $body, $params = array(), $files = array(), $encoding)
	{
		parent::__construct($encoding);
		$this->to = $to;
		$this->from = $from;
		$this->subject = $subject;
		$this->body = $body;
		$this->params = $params;
		$this->files = $files;
		// Headers
		//$from_enc = $this->encode_string($this->from);
		//$this->headers[] = "From: {$from_enc}";
		//$this->headers[] = "Reply-To: {$from_enc}";
		$this->headers[] = "From: {$from}";
		$this->headers[] = "Reply-To: {$from}";
		$this->set_content_type_header();
		// Body & subj.
		$this->body_prepared = $this->body;
		$this->subject_prepared = $this->subject;
	}
	
	protected function set_content_type_header()
	{
		$this->headers[] = "Content-Type: text/plain; charset={$this->encoding}";
	}

	protected function prepare()
	{
		$ret = parent::prepare();
		foreach ($this->params as $idx => $val)
		{
			$this->body_prepared = str_replace($idx, $val, $this->body_prepared);
		}
		return MAIL_OK;
	}
	
}

?>