<?php

/*
  drop table if exists account;
  create table account select id, login, email, create_datetime, is_admin from member

 */

class dbconvert_2_load
{

	private $db;
	private $cmp4_dbname;
	private $idsrv_dbname;
	private $prefix_profile;
	private $prefix_photo;

	function __construct($db, $cmp4_dbname, $idsrv_dbname)
	{
		$this->db = $db;
		$this->cmp4_dbname = $cmp4_dbname;
		$this->idsrv_dbname = $idsrv_dbname;
		$this->prefix_profile = "http://users.idsrv.ru/people/";
		$this->prefix_photo = "http://static.idsrv.ru/photo/";
	}
	
	// ------------------------------------------------------------------------------------------------------------------------
// ----- user ( пользователи )
	private function cvt_0000000_user()
	{
		$this->db->sql(
				"INSERT INTO user (
		id,login,password,is_admin, sex, first_name, mid_name, last_name,
		photo_large_width, photo_large_height,photo_large_url,
		photo_big_width, photo_big_height, photo_big_url,
		photo_mid_width, photo_mid_height, photo_mid_url,
		photo_small_width, photo_small_height, photo_small_url,
		profile_url,
		email, skype, phone, icq, www,
		about, profile_id_bak,
		register_ip,
		add_time, edit_time
	)
	SELECT
		account_id, login, password, is_admin,
		CASE sex WHEN 'm' THEN 'm' WHEN 'w' THEN 'f' ELSE '?' END AS sex,
		first_name, mid_name, last_name,
		
		IFNULL(photo_large_width,0) AS photo_large_width,
		IFNULL(photo_large_height,0) AS photo_large_height,
		IF(ISNULL(photo_large_width) OR 0=photo_large_width OR ISNULL(photo_large_height) OR 0=photo_large_height, '', concat('{$this->prefix_photo}','large/',prf.id,'.jpeg')) AS photo_large_url,
		
		IFNULL(photo_big_width,0) AS photo_big_width,
		IFNULL(photo_big_height,0) AS photo_big_height,
		IF(ISNULL(photo_big_width) OR 0=photo_big_width OR ISNULL(photo_big_height) OR 0=photo_big_height, '', concat('{$this->prefix_photo}','big/',prf.id,'.jpeg')) AS photo_big_url,
		
		IFNULL(photo_mid_width,0) AS photo_mid_width,
		IFNULL(photo_mid_height,0) AS photo_mid_height,
		IF(ISNULL(photo_mid_width) OR 0=photo_mid_width OR ISNULL(photo_mid_height) OR 0=photo_mid_height, '', concat('{$this->prefix_photo}','mid/',prf.id,'.jpeg')) AS photo_mid_url,
		
		IFNULL(photo_small_width,0) AS photo_small_width,
		IFNULL(photo_small_height,0) AS photo_small_height,
		IF(ISNULL(photo_small_width) OR 0=photo_small_width OR ISNULL(photo_small_height) OR 0=photo_small_height, '', concat('{$this->prefix_photo}','small/',prf.id,'.jpeg')) AS photo_small_url,
		
		concat('{$this->prefix_profile}',account_id,'/') AS profile_url,
		email,
		IFNULL(skype,'') AS skype,
		IFNULL(phone,'') AS phone,
		IFNULL(icq,'') AS icq,
		IFNULL(www,'') AS www,
		
		about,
		prf.id as profile_id_bak,
		
		register_ip,
		
		acc.create_datetime AS add_time,
		IF(acc.change_datetime > prf.change_datetime, acc.change_datetime, prf.change_datetime) AS edit_time
	FROM
		{$this->idsrv_dbname}.profile AS prf, {$this->idsrv_dbname}.account AS acc
	WHERE
		acc.id=prf.account_id

ON DUPLICATE KEY UPDATE
	login=VALUES(login),password=VALUES(password),is_admin=VALUES(is_admin), sex=VALUES(sex), first_name=VALUES(first_name), mid_name=VALUES(mid_name), last_name=VALUES(last_name),
	photo_large_width=VALUES(photo_large_width), photo_large_height=VALUES(photo_large_height), photo_large_url=VALUES(photo_large_url),
	photo_big_width=VALUES(photo_big_width), photo_big_height=VALUES(photo_big_height), photo_big_url=VALUES(photo_big_url),
	photo_mid_width=VALUES(photo_mid_width), photo_mid_height=VALUES(photo_mid_height), photo_mid_url=VALUES(photo_mid_url),
	photo_small_width=VALUES(photo_small_width), photo_small_height=VALUES(photo_small_height), photo_small_url=VALUES(photo_small_url),
	profile_url=VALUES(profile_url),
	email=VALUES(email), skype=VALUES(skype), phone=VALUES(phone), icq=VALUES(icq), www=VALUES(www),
	about=VALUES(about), profile_id_bak=VALUES(profile_id_bak),
	register_ip=VALUES(register_ip),
	add_time=VALUES(add_time), edit_time=VALUES(edit_time)");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- projects ( проекты )
	private function cvt_00001_project()
	{
		$this->db->sql(
				"INSERT INTO project (
		id,name,title,descr,adder_user_id,add_time,edit_time,start_time,finish_time,logo_big_width,logo_big_height,main_url
	)
SELECT
	id,name,title,`desc`,author_id,create_datetime,change_datetime,start_datetime,finish_datetime,
	IFNULL(pic_pic_width,0) AS logo_mid_width,
	IFNULL(pic_pic_height,0) AS logo_mid_height,
	IFNULL(main_url,'') AS main_url
FROM
	{$this->cmp4_dbname}.project");
	}
	
	private function cvt_00001_project_admin()
	{
		$this->db->sql("
			INSERT INTO project_admin (user_id, project_id, is_admin, add_time, edit_time)
			SELECT adder_user_id, id, 1 as is_admin, add_time as add_time, add_time /* it's correct */ as edit_time
			FROM project
			WHERE adder_user_id IS NOT NULL
		");
		
		$this->db->sql("
			REPLACE INTO project_admin (user_id, project_id, is_admin, add_time, edit_time)
			SELECT member_id, project_id, 1 as is_admin, join_datetime as add_time, join_datetime as edit_time
			FROM {$this->cmp4_dbname}.project_member_link
			WHERE is_project_admin = 1
		");
	}
	
	private function cvt_00001_project_intmenu_item()
	{
		$this->db->sql("
			INSERT INTO project_intmenu_item
				(id, project_id, title, url, position, add_time, edit_time)
			SELECT 
				id, project_id, title, url, `order`, create_datetime, change_datetime
			FROM {$this->idsrv_dbname}.project_menu
		");
	}
	

// ------------------------------------------------------------------------------------------------------------------------
// -----  competence_set ( наборы кометенций )
	private function cvt_0001_competence_set()
	{
		$this->db->sql(
				"INSERT INTO
	competence_set (
		id, title, add_time, edit_time, descr, adder_user_id
	)
	SELECT
		id, title, create_datetime, change_datetime, `desc`, author_id
	FROM
		{$this->cmp4_dbname}.competence_tree
ON DUPLICATE KEY UPDATE
	title=VALUES(title), add_time=VALUES(add_time), edit_time=VALUES(edit_time), descr=VALUES(descr), adder_user_id=VALUES(adder_user_id) ");
	}
	
	private function cvt_0001_competence_set_admin()
	{
		$this->db->sql("
			INSERT INTO competence_set_admin (user_id, competence_set_id, is_admin, add_time, edit_time)
			SELECT adder_user_id, id, 1 as is_admin, add_time as add_time, add_time /* it's correct */ as edit_time
			FROM competence_set
			WHERE adder_user_id IS NOT NULL
		");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- competence_group ( группы компетенций в наборе )
	private function cvt_0002_competence_group()
	{
		$this->db->sql("SET foreign_key_checks = 0");
		$this->db->sql(
				"INSERT INTO
	competence_group (
		id, parent_id, title, position, level, competence_set_id, add_time, edit_time
	)
	SELECT
		id, IF(parent_id, parent_id, NULL), title, `order`, level, competence_tree_id, create_datetime, change_datetime
	FROM	
		{$this->cmp4_dbname}.competence_group
	WHERE
		is_deleted=0 AND id!=0
	ORDER BY
		id
");
		$this->db->sql("SET foreign_key_checks = 1");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- competence_full ( все компетенции, в том числе удаленные )
	private function cvt_0003_competence_full()
	{
		$this->db->sql(
				"INSERT INTO
	competence_full (
		id, title, number, add_time, edit_time, competence_set_id, is_deleted
	)
	SELECT
		cf.id,cf.title,cf.number,cf.create_datetime,cf.change_datetime,cf.competence_tree_id,cf.is_deleted
	FROM
		{$this->cmp4_dbname}.competence AS cf
	LEFT JOIN
		{$this->cmp4_dbname}.competence_tree AS ct ON ct.id=cf.competence_tree_id
	WHERE
		ct.id IS NOT null");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- competence_link ( раскидка компетенций по группам )
	private function cvt_0004_competence_link()
	{
		$this->db->sql(
				"INSERT INTO competence_link (
		competence_id,
		competence_group_id
	)
	SELECT
		cl.competence_id, cl.competence_group_id
	FROM
		{$this->cmp4_dbname}.competence_link AS cl
	LEFT JOIN
		competence_full AS cf ON cf.id = cl.competence_id
	LEFT JOIN
		competence_group AS cg ON cg.id = cl.competence_group_id
	WHERE
		(cf.id IS NOT NULL) AND (cg.id IS NOT NULL)");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- professiogram ( профессиограммы )
	private function cvt_0005_professiogram()
	{
		$this->db->sql("DROP FUNCTION IF EXISTS insert_rate");
		$this->db->sql("
CREATE FUNCTION `insert_rate`(
       rate_type_id1 int unsigned,
       title1 varchar(255) CHARACTER SET utf8,
       competence_set_id1 int unsigned,
       add_time1 datetime,
       edit_time1 datetime
) RETURNS int(10) unsigned
begin
insert into rate
(rate_type_id,title,competence_set_id,add_time,edit_time) values
(rate_type_id1,title1,competence_set_id1,add_time1,edit_time1);
return last_insert_id();
end");

		$this->db->sql(
				"INSERT INTO professiogram (
		id, title, descr, add_time, edit_time, enabled, competence_set_id, adder_user_id,
		rate_id
	)
	SELECT
		id, title, `desc`, create_datetime, change_datetime, is_public, competence_tree_id, author_member_id ,
		insert_rate(1,title,competence_tree_id,create_datetime,change_datetime)
	FROM
		{$this->cmp4_dbname}.professiogram
	WHERE
		1=is_profession");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- rate_competence_link ( связка рейтинги-компетенции )

	private function cvt_0007_rate_competence_link()
	{
		$this->db->sql("
			INSERT INTO rate_competence_link
				(rate_id, competence_id, mark)
			SELECT p.rate_id, pcm.competence_id, pcm.mark
			FROM {$this->cmp4_dbname}.professiogram_competence_mark pcm
			INNER JOIN professiogram p ON p.id = pcm.professiogram_id
		");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- user_project_link ( раскидка пользователей по проектам )
	private function cvt_0010_user_project_link()
	{
		$this->db->sql(
				"INSERT INTO user_project_link (
		project_id, user_id, add_time, is_analyst
	)
	SELECT project_id, member_id, join_datetime, is_project_analyst
	FROM {$this->cmp4_dbname}.project_member_link
	ORDER BY project_id, member_id");

		// -- game_name
		$project_ids = $this->db->fetch_column_values("SELECT id  FROM project");
		foreach ($project_ids as $project_id)
		{
			// а есть ли таблица с профилями ?
			$profile_table = "project_profile_{$project_id}";
			$maybe_profile_table = $this->db->fetch_column_values("SHOW TABLES FROM {$this->idsrv_dbname} LIKE  '{$profile_table}'");
			if (0 == count($maybe_profile_table))
			{
				// нет таблицы с игровыми профилями
				continue;
			}

			// а есть ли в профиле игровое имя ?
			$maybe_game_name = $this->db->fetch_column_values("SHOW COLUMNS FROM {$this->idsrv_dbname}.{$profile_table} LIKE  'game_name'");
			if (0 == count($maybe_game_name))
			{
				// нет игрового имени в профиле
				continue;
			}

			// здесь уже точно есть и профиль и имя в нем
			$this->db->sql("UPDATE user_project_link AS upl, {$this->idsrv_dbname}.{$profile_table} AS pp
			SET upl.game_name = IFNULL(TRIM(pp.game_name),'')
			WHERE upl.project_id = {$project_id}
				AND upl.user_id = pp.account_id");
		}
		// -- user_project_link admin fix
		$this->db->sql("
			UPDATE user_project_link l
			LEFT JOIN project_admin a ON a.project_id = l.project_id AND a.user_id = l.user_id
			SET l.status = 'admin'
			WHERE a.is_admin = 1
		");
		// -- project.use_game_name
		$this->db->sql("
UPDATE project,
(
SELECT project_id, IF( COUNT( * ) >0, 1, 0 ) AS ugn
FROM user_project_link
WHERE LENGTH( game_name ) > 0
GROUP BY project_id
) AS tmp
SET project.use_game_name = tmp.ugn WHERE project.id = tmp.project_id");
	}

	// МИГи
	private function cvt_0020_precedent_group()
	{
		$this->db->sql(
				"INSERT INTO
	precedent_group (id,title,position,add_time,edit_time,project_id)
SELECT
	id,title,`order`,create_datetime,change_datetime,project_id
FROM
	{$this->cmp4_dbname}.subproject");
	}

	// события
	private function cvt_0030_precedent()
	{
		$this->db->sql(
				"INSERT INTO
	precedent (id,title,descr,add_time,edit_time,precedent_group_id,project_id,
	last_editor_user_id,
	adder_user_id,
	responsible_user_id,
	access,
	complete_descr,
	complete_members,
	complete_marks
	)
SELECT
	id, title, IFNULL(`desc`,''), create_datetime, change_datetime, subproject_id, project_id,
	last_editor_member_id,
	author_member_id,
	responsible_member_id,
	CASE access WHEN 'h' THEN 'private' WHEN 'r' THEN 'public' ELSE 'private' END AS access,
	is_done_desc AS complete_descr,
	is_done_user AS complete_members,
	is_done_mark AS complete_marks
	
FROM
	{$this->cmp4_dbname}.event");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- precedent_flash_group ( группы участников событий )
	private function cvt_0040_precedent_flash_group()
	{
		$this->db->sql(
				"INSERT INTO
	precedent_flash_group (id, title, precedent_id, add_time)
SELECT
	id, title, event_id, create_datetime
FROM
	{$this->cmp4_dbname}.event_member_group");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- precedent_flash_group_user_link ( связка  "группы участников событий"->"участники" )
	private function cvt_0050_precedent_flash_group_user_link()
	{
		$this->db->sql(
				"INSERT INTO
	precedent_flash_group_user_link (id, user_id, precedent_flash_group_id)
SELECT
	id, member_id , event_member_group_id
FROM
	{$this->cmp4_dbname}.event_member_link");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- precedent_rater_link ( связка  "события"->"наблюдатели" )
	private function cvt_0060_precedent_rater_link()
	{
		$this->db->sql(
				"INSERT INTO
	precedent_rater_link (precedent_id, rater_user_id, comment)
SELECT
	event_id, rater_member_id, comment
FROM
	{$this->cmp4_dbname}.event_rater_member");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- precedent_competence_group_link ()
	private function cvt_0070_precedent_competence_group_link()
	{
		$this->db->sql(
				"INSERT INTO
	precedent_competence_group_link (precedent_id, competence_group_id,competence_set_id)
SELECT
	event_id, competence_group_id, competence_set_id
FROM
	{$this->cmp4_dbname}.event_competence_group AS ecg,
	competence_group AS cg
WHERE
	cg.id=ecg.competence_group_id
	");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- mark_personal ( личные оценки )
	private function cvt_0080_mark_personal()
	{
		$this->db->sql(
				"INSERT INTO mark_personal (
		precedent_flash_group_user_link_id,
		competence_id,
		rater_user_id,
		adder_user_id,
		value,
		comment
	)
	SELECT
		event_member_link_id, competence_id, rater_member_id, adder_member_id, mark, m_comment
	FROM	
		{$this->cmp4_dbname}.mark,
		competence_full AS cmp
	WHERE
		mark.src_competence_tree_id=0
		AND cmp.id = mark.competence_id
		AND 0=cmp.is_deleted
	ORDER BY
		event_member_link_id, competence_id");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- mark_group ( групповые оценки )
	private function cvt_0090_mark_group()
	{
		$this->db->sql(
				"INSERT INTO mark_group (
		precedent_flash_group_id,
		competence_id,
		rater_user_id,
		adder_user_id,
		value,
		comment
	)
	SELECT
		event_member_group_id, competence_id, rater_member_id, adder_member_id, mark, m_comment
	FROM
		{$this->cmp4_dbname}.group_mark,
		competence_full AS cmp
	WHERE
		group_mark.src_competence_tree_id=0
		AND cmp.id = group_mark.competence_id
		AND cmp.is_deleted=0
	ORDER BY
		event_member_group_id, competence_id");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- fix competence group position
	private function cvt_0130_fix_competence_group_position()
	{
		$res = $this->db->sql("select id, title, competence_set_id, parent_id, level, position, add_time, edit_time from competence_group order by id");
		$competence_group = array();
		$competence_root = array();
		while ($d = $this->db->fetch_array($res))
		{
			$d['_leafs'] = array();
			$competence_group[$d['id']] = $d;
			if ($d['parent_id'] == 0)
			{
				$competence_root[] = $d['id'];
			}
		}

		foreach ($competence_group as $cgk => $cgv)
		{
			if (!$cgv['parent_id'])
				continue;
			if (!isset($competence_group[$cgv['parent_id']]))
				continue;

			$competence_group[$cgv['parent_id']]['_leafs'][] = $cgk;
		}

		$position = 0;
		foreach ($competence_root as $cgk)
		{
			$this->rfunc1($competence_group, $position, $cgk);
		}

		foreach ($competence_group as $cgk => $cgv)
		{
			$res = $this->db->sql("update competence_group set position={$cgv['position']} where id={$cgk}");
		}
	}

	private function rfunc1(&$competence_group, &$position, $cgk)
	{
		$competence_group[$cgk]['position'] = $position++;
		// echo $cgk.":".$position."<br>";
		foreach ($competence_group[$cgk]['_leafs'] as $cgk2)
		{
			$this->rfunc1($competence_group, $position, $cgk2);
		}
	}

	function run()
	{
		global $config;
		$tocall = array();
		foreach (get_class_methods($this) as $v)
		{
			if (strlen($v) > 4 and 0 === substr_compare($v, "cvt_", 0, 4))
			{
				$tocall[] = $v;
			}
		}
		sort($tocall);
		foreach ($tocall as $v)
		{
			$this->$v();
		}
		return;
	}

}

?>