<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class dbconvert_3_calc
{

	private $db;

	function __construct($db)
	{
		$this->db = $db;
	}

	function run()
	{
		global $config;
		$tocall = array();
		foreach (get_class_methods($this) as $v)
		{
			if (strlen($v) > 5 and 0 === substr_compare($v, "calc_", 0, 5))
			{
				$tocall[] = $v;
			}
		}
		sort($tocall);
		foreach ($tocall as $v)
		{
			// echo "=== $v ===\r\n";
			$this->$v();
		}
		return;
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- mark_calc ( личные оценки, групповые оценки )
	private function calc_010_mark_calc()
	{
		// личные оценки
		$this->db->sql(
			"INSERT INTO mark_calc (
		project_id, competence_set_id, competence_id, precedent_group_id, precedent_id, rater_user_id, ratee_user_id, precedent_flash_group_user_link_id, precedent_flash_group_id, type, value, comment
	)
	SELECT
		precedent.project_id,					# project_id
		cmp.competence_set_id,			# competence_set_id
		mp.competence_id,				# competence_id
		precedent.precedent_group_id,				# precedent_group_id
		precedent.id as precedent_id,				# precedent_id
		mp.rater_user_id,
		efl.user_id as ratee_user_id,
		efl.id,							# precedent_member_link_id
		# wm.rater_group_id,
		efl.precedent_flash_group_id,
		'personal' as type,				# type
		mp.value,
		# 1 as weight,
		mp.comment					# comment
	FROM
		competence_full AS cmp,
		mark_personal AS mp,
		precedent_flash_group_user_link AS efl,
		precedent_flash_group AS efg,
		precedent,
		user
	WHERE
		cmp.is_deleted=0
		AND cmp.id = mp.competence_id
		AND efl.id = mp.precedent_flash_group_user_link_id
		AND efg.id = efl.precedent_flash_group_id
		AND precedent.id = efg.precedent_id
		AND user.id = mp.rater_user_id");

		// групповые оценки
		$this->db->sql(
			"INSERT INTO mark_calc (
		project_id, competence_set_id, competence_id, precedent_group_id, precedent_id, rater_user_id, ratee_user_id, precedent_flash_group_user_link_id, precedent_flash_group_id, type, value, comment
	)
	SELECT
		precedent.project_id,					# project_id
		cmp.competence_set_id,			# competence_set_id
		mg.competence_id,				# competence_id
		precedent.precedent_group_id,				# precedent_group_id
		precedent.id AS precedent_id,				# precedent_id
		mg.rater_user_id,				# rater_user_id
		efl.user_id AS ratee_user_id,			# ratee_user_id
		efl.id,							# precedent_member_link_id
		# wm.rater_group_id,
		mg.precedent_flash_group_id,
		'group' as type,					# type
		mg.value,						# value
		# 0.5 as weight
		mg.comment					# comment
	FROM
		competence_full AS cmp,
		mark_group AS mg,
		precedent_flash_group AS efg,
		precedent_flash_group_user_link AS efl,
		precedent,
		user
	WHERE
		cmp.is_deleted=0
		AND cmp.id = mg.competence_id
		AND efg.id = mg.precedent_flash_group_id
		AND precedent.id = efg.precedent_id
		AND user.id = mg.rater_user_id
		AND efl.precedent_flash_group_id = mg.precedent_flash_group_id");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- project_competence_set_link_calc
	function calc_015_project_competence_set_link_calc()
	{
		// -- personal_mark_count_calc
		$this->db->sql(
			"INSERT INTO project_competence_set_link_calc (project_id, competence_set_id, personal_mark_count_calc)
SELECT project_id, competence_set_id, count(*) FROM mark_calc WHERE type='personal' GROUP BY  project_id, competence_set_id");

		// -- group_mark_count_calc
		$this->db->sql(
			"INSERT INTO project_competence_set_link_calc (project_id, competence_set_id, group_mark_count_calc)
SELECT project_id, competence_set_id, count(*) AS c FROM mark_calc WHERE type='group' GROUP BY  project_id, competence_set_id
ON DUPLICATE KEY UPDATE group_mark_count_calc=VALUES(group_mark_count_calc)");

		// -- group_mark_raw_count_calc
		$this->db->sql(
			"INSERT INTO project_competence_set_link_calc (project_id, competence_set_id, group_mark_raw_count_calc)
SELECT project_id, competence_set_id, count(DISTINCT precedent_flash_group_id,competence_id) AS c FROM mark_calc WHERE type='group' GROUP BY project_id, competence_set_id
ON DUPLICATE KEY UPDATE group_mark_raw_count_calc=VALUES(group_mark_raw_count_calc)");

		// -- total_mark_count_calc
		$this->db->sql("UPDATE project_competence_set_link_calc SET total_mark_count_calc=personal_mark_count_calc+group_mark_count_calc");

		// -- personal_mark_count_with_trans_calc
		// -- group_mark_count_with_trans_calc
		// -- group_mark_raw_count_with_trans_calc
		// -- total_mark_count_with_trans_calc
		$this->db->sql("
-- инициализация: считаем оценки без трансляторов
UPDATE project_competence_set_link_calc
SET 
	personal_mark_count_with_trans_calc=personal_mark_count_calc,
	group_mark_count_with_trans_calc=group_mark_count_calc,
	group_mark_raw_count_with_trans_calc=group_mark_raw_count_calc,
	total_mark_count_with_trans_calc=total_mark_count_calc");

		$this->db->sql("
-- посчитаем оценки через трансляторы
UPDATE project_competence_set_link_calc AS pcslc3,
(
	-- для набора_компетенций: сумма количеств_оценок, сумма количеств_проектов с учетом трансляции
	SELECT tmp1.project_id, tmp1.competence_set_id, SUM( tmp1.personal_mark_count_calc )  AS pmcc, SUM( tmp1.group_mark_count_calc ) AS gmcc,  SUM( tmp1.group_mark_raw_count_calc ) AS gmrcc, SUM( tmp1.total_mark_count_calc ) AS tmcc
	FROM (
		-- для проекта,набора_компетенций: набор_из_которого_транслируем, количества_оценок(4), количество_проектов_с_оценками
		SELECT pcslc1.project_id, pcslc1.competence_set_id, from_competence_set_id, pcslc2.personal_mark_count_calc, pcslc2.group_mark_count_calc, pcslc2.group_mark_raw_count_calc, pcslc2.total_mark_count_calc
		FROM project_competence_set_link_calc AS pcslc1, competence_translator, project_competence_set_link_calc AS pcslc2
		WHERE to_competence_set_id = pcslc1.competence_set_id
			AND pcslc2.competence_set_id = from_competence_set_id
			AND pcslc2.project_id = pcslc1.project_id
			AND is_default =1
		) AS tmp1
	GROUP BY tmp1.project_id, tmp1.competence_set_id
) AS tmp2
SET 
	pcslc3.personal_mark_count_with_trans_calc=pcslc3.personal_mark_count_with_trans_calc+tmp2.pmcc,
	pcslc3.group_mark_count_with_trans_calc=pcslc3.group_mark_count_with_trans_calc+tmp2.gmcc,
	pcslc3.group_mark_raw_count_with_trans_calc=pcslc3.group_mark_raw_count_with_trans_calc+tmp2.gmrcc,
	pcslc3.total_mark_count_with_trans_calc=pcslc3.total_mark_count_with_trans_calc+tmp2.tmcc
WHERE
	pcslc3.project_id=tmp2.project_id
	AND pcslc3.competence_set_id=tmp2.competence_set_id");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- competence_full
	private function calc_020_competence_full()
	{
		// -- link_count_calc
		$this->db->sql(
			"UPDATE
	competence_full, 
	(SELECT competence_id, count(competence_group_id) AS lcc FROM competence_link GROUP BY competence_id) AS tmp
SET
	link_count_calc=lcc
WHERE id=competence_id");

		// -- personal_mark_count_calc
		$this->db->sql(
			"UPDATE
	competence_full,
	(SELECT competence_id, count(*) AS c FROM mark_calc WHERE type='personal' GROUP BY competence_id) AS tmp
SET
	personal_mark_count_calc=c
WHERE
	id=competence_id");

		// -- group_mark_count_calc
		$this->db->sql(
			"UPDATE
	competence_full,
	(SELECT competence_id, count(*) AS c FROM mark_calc WHERE type='group' GROUP BY competence_id) AS tmp
SET
	group_mark_count_calc=c
WHERE
	id=competence_id");

		// -- group_mark_raw_count_calc
		$this->db->sql(
			"UPDATE
	competence_full,
	(SELECT competence_id, count(distinct precedent_flash_group_id) AS c FROM mark_calc WHERE type='group' GROUP BY competence_id) AS tmp
SET
	group_mark_raw_count_calc=c
WHERE
	id=competence_id");

		// -- total_mark_count_calc
		$this->db->sql("UPDATE competence_full SET total_mark_count_calc=personal_mark_count_calc+group_mark_count_calc");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- competence_calc ( рассчетная таблица компетенций )
	private function calc_030_competence_calc()
	{
		$this->db->sql(
			"INSERT INTO competence_calc (
		competence_id,title,number,competence_set_id,personal_mark_count_calc,group_mark_count_calc,group_mark_raw_count_calc,total_mark_count_calc
	)
	SELECT  id, title, number, competence_set_id, personal_mark_count_calc, group_mark_count_calc, group_mark_raw_count_calc, total_mark_count_calc
	FROM competence_full
	WHERE
		0=is_deleted
		AND link_count_calc>0");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- competence_set
	private function calc_040_competence_set()
	{
		// -- personal_mark_count_calc
		$this->db->sql(
			"UPDATE competence_set AS cs, (SELECT competence_set_id, count(*)  AS c FROM mark_calc WHERE type='personal' GROUP BY competence_set_id) AS mpmc
SET cs.personal_mark_count_calc = mpmc.c
WHERE cs.id=mpmc.competence_set_id");

		// -- group_mark_count_calc
		$this->db->sql(
			"UPDATE competence_set AS cs, (SELECT competence_set_id, count(*)  AS c FROM mark_calc WHERE type='group' GROUP BY competence_set_id) AS mgmc
SET cs.group_mark_count_calc = mgmc.c
WHERE cs.id=mgmc.competence_set_id");

		// -- group_mark_raw_count_calc
		$this->db->sql(
			"UPDATE competence_set AS cs, (SELECT competence_set_id, count(distinct competence_id,precedent_flash_group_id)  AS c FROM mark_calc WHERE type='group' GROUP BY competence_set_id) AS mgrc
SET cs.group_mark_raw_count_calc = mgrc.c
WHERE cs.id=mgrc.competence_set_id");

		// -- total_mark_count_calc
		$this->db->sql("UPDATE competence_set SET total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc");

		// -- competence_count_calc
		$this->db->sql(
			"UPDATE competence_set AS cs, (SELECT competence_set_id, count(*) AS c FROM competence_full GROUP BY competence_set_id) AS cc
SET cs.competence_count_calc = cc.c
WHERE cs.id=cc.competence_set_id");

		// -- professiogram_count_calc
		$this->db->sql("UPDATE competence_set SET professiogram_count_calc=0");
		$this->db->sql(
			"UPDATE competence_set AS cs, (SELECT competence_set_id, count(*) AS apc FROM professiogram WHERE enabled=1 GROUP BY competence_set_id) AS pc
SET cs.professiogram_count_calc = pc.apc
WHERE cs.id=pc.competence_set_id");

		// -- project_count_calc, количество проектов с !0 оценок
		$this->db->sql("
UPDATE competence_set AS cs,
(
	-- количество проектов с оценками для каждого дерева компетенций
	SELECT competence_set_id, COUNT(*) AS cnt
	FROM project_competence_set_link_calc
	GROUP BY competence_set_id
) AS tmp
SET cs.project_count_calc = tmp.cnt
WHERE cs.id = tmp.competence_set_id");

		// -- personal_mark_count_with_trans_calc
		// -- group_mark_count_with_trans_calc
		// -- group_mark_raw_count_with_trans_calc
		// -- total_mark_count_with_trans_calc
		// -- project_count_with_trans_calc
		$this->db->sql("
-- инициализация: считаем оценки без трансляторов
UPDATE competence_set
SET 
	personal_mark_count_with_trans_calc=personal_mark_count_calc,
	group_mark_count_with_trans_calc=group_mark_count_calc,
	group_mark_raw_count_with_trans_calc=group_mark_raw_count_calc,
	total_mark_count_with_trans_calc=total_mark_count_calc,
	project_count_with_trans_calc=project_count_calc");

		$this->db->sql("
-- посчитаем оценки через трансляторы
UPDATE competence_set AS cs3,
(
	-- для набора_компетенций: сумма количеств_оценок, сумма количеств_проектов с учетом трансляции
	SELECT tmp1.id, SUM( tmp1.personal_mark_count_calc )  AS pmcc, SUM( tmp1.group_mark_count_calc ) AS gmcc,  SUM( tmp1.group_mark_raw_count_calc ) AS gmrcc, SUM( tmp1.total_mark_count_calc ) AS tmcc, SUM( project_count_calc ) AS pcc 
	FROM (
		-- для набора_компетенций: набор_из_которого_транслируем, количества_оценок(4), количество_проектов_с_оценками
		SELECT cs1.id, from_competence_set_id, cs2.personal_mark_count_calc, cs2.group_mark_count_calc, cs2.group_mark_raw_count_calc, cs2.total_mark_count_calc, cs2.project_count_calc
		FROM competence_set AS cs1, competence_translator, competence_set AS cs2
		WHERE to_competence_set_id = cs1.id
			AND cs2.id = from_competence_set_id
			AND is_default = 1
		) AS tmp1
	GROUP BY tmp1.id
) AS tmp2
SET 
	cs3.personal_mark_count_with_trans_calc=cs3.personal_mark_count_with_trans_calc+tmp2.pmcc,
	cs3.group_mark_count_with_trans_calc=cs3.group_mark_count_with_trans_calc+tmp2.gmcc,
	cs3.group_mark_raw_count_with_trans_calc=cs3.group_mark_raw_count_with_trans_calc+tmp2.gmrcc,
	cs3.total_mark_count_with_trans_calc=cs3.total_mark_count_with_trans_calc+tmp2.tmcc,
	cs3.project_count_with_trans_calc=cs3.project_count_with_trans_calc+tmp2.pcc
WHERE
	cs3.id=tmp2.id");
	}

// ------------------------------------------------------------------------------------------------------------------------
// -----
	function calc_050_project_user_mark_count_calc()
	{
		// -- user_project_link *_mark_count_calc
		$this->db->sql("CREATE TEMPORARY TABLE tmp SELECT project_id, ratee_user_id AS user_id, type,COUNT( * ) AS c FROM mark_calc GROUP BY project_id, ratee_user_id,type");
		$this->db->sql("UPDATE user_project_link AS upl, tmp SET personal_mark_count_calc=c WHERE upl.project_id=tmp.project_id AND upl.user_id=tmp.user_id AND type='personal'");
		$this->db->sql("UPDATE user_project_link AS upl, tmp SET group_mark_count_calc=c WHERE upl.project_id=tmp.project_id AND upl.user_id=tmp.user_id AND type='group'");
		$this->db->sql("UPDATE user_project_link SET total_mark_count_calc=personal_mark_count_calc+group_mark_count_calc");

		// -- table: user *_mark_count_calc
		$this->db->sql("CREATE temporary table tmp2 SELECT user_id,type,sum(c) as c2 FROM tmp GROUP BY user_id,type");
		$this->db->sql("UPDATE user, tmp2 SET personal_mark_count_calc=c2 WHERE user.id=tmp2.user_id AND type='personal'");
		$this->db->sql("UPDATE user, tmp2 SET group_mark_count_calc=c2 WHERE user.id=tmp2.user_id AND type='group'");
		$this->db->sql("UPDATE user SET total_mark_count_calc=personal_mark_count_calc+group_mark_count_calc");

		// -- clean up
		$this->db->sql("DROP TEMPORARY TABLE tmp");
		$this->db->sql("DROP TEMPORARY TABLE tmp2");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- project
	function calc_060_project()
	{
		// -- personal_mark_count_calc
		$this->db->sql(
			"UPDATE project AS p, (SELECT project_id, count(*) AS pmc FROM mark_calc WHERE type='personal' GROUP BY project_id) AS mpmc
SET p.personal_mark_count_calc=mpmc.pmc
WHERE p.id=mpmc.project_id");

		// -- group_mark_count_calc
		$this->db->sql(
			"UPDATE project AS p, (SELECT project_id, count(*) AS gmc FROM mark_calc WHERE type='group' GROUP BY project_id) AS mgmc
SET p.group_mark_count_calc=mgmc.gmc
WHERE p.id=mgmc.project_id");

		// -- group_mark_raw_count_calc
		$this->db->sql(
			"UPDATE project AS p, (SELECT project_id, count(DISTINCT competence_id,precedent_flash_group_id) AS grc FROM mark_calc WHERE type='group' GROUP BY project_id) AS mgrc
SET p.group_mark_raw_count_calc=mgrc.grc
WHERE p.id=mgrc.project_id");

		// -- total_mark_count_calc
		$this->db->sql("UPDATE project SET total_mark_count_calc=personal_mark_count_calc+group_mark_count_calc");

		// -- project.precedent_count_calc
		$this->db->sql("UPDATE project AS p,
		(
			SELECT project_id, COUNT( * ) AS ecc
			FROM  precedent 
			GROUP BY project_id
		) AS ec
		SET p.precedent_count_calc = ec.ecc WHERE p.id = ec.project_id");

		// -- project.user_count_calc
		$this->db->sql("UPDATE project AS p,
		(
			SELECT project_id, COUNT( * ) AS ucc
			FROM  user_project_link
			GROUP BY project_id
		) AS uc
		SET p.user_count_calc = uc.ucc WHERE p.id = uc.project_id");

		// -- project.main_url_human_calc
		$db_res = $this->db->sql("SELECT id, descr, main_url  FROM project");
		while ($row = $this->db->fetch_array($db_res))
		{
			$main_url_human_calc = $this->db->escape(text_processor::get_url_human($row['main_url']));
			$descr = $this->db->escape(text_processor::get_preview($row['descr']));
			$this->db->sql("UPDATE project SET descr='{$descr}', main_url_human_calc='{$main_url_human_calc}' WHERE id={$row['id']}");
		}
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- user
	function calc_065_user()
	{
		// -- user.www_human_calc
		$db_res = $this->db->sql("SELECT id, www  FROM user");
		while ($row = $this->db->fetch_array($db_res))
		{
			$www_human_calc = text_processor::get_url_human($row['www']);
			$this->db->sql("UPDATE user SET www_human_calc='{$www_human_calc}' WHERE id={$row['id']}");
		}
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- precedent
	function calc_070_precedent()
	{
		// -- personal_mark_count_calc
		$this->db->sql(
			"UPDATE precedent AS e, (SELECT precedent_id, count(*) AS pmc FROM mark_calc WHERE type='personal' GROUP BY precedent_id) AS epmc
SET e.personal_mark_count_calc=epmc.pmc
WHERE e.id=epmc.precedent_id");

		// -- group_mark_count_calc
		$this->db->sql(
			"UPDATE precedent AS e, (SELECT precedent_id, count(*) AS gmc FROM mark_calc WHERE type='group' GROUP BY precedent_id) AS egmc
SET e.group_mark_count_calc=egmc.gmc
WHERE e.id=egmc.precedent_id");

		// -- group_mark_raw_count_calc
		$this->db->sql(
			"UPDATE precedent AS e, (SELECT precedent_id, count(DISTINCT competence_id,precedent_flash_group_id) AS grc FROM mark_calc WHERE type='group' GROUP BY precedent_id) AS egrc
SET e.group_mark_raw_count_calc=egrc.grc
WHERE e.id=egrc.precedent_id");

		// -- total_mark_count_calc
		$this->db->sql("UPDATE precedent SET total_mark_count_calc=personal_mark_count_calc+group_mark_count_calc");

		// -- comment_count_calc
		$this->db->sql(
			"UPDATE precedent AS e, (SELECT precedent_id, count(*) AS comment_count FROM precedent_comment WHERE is_deleted=0 GROUP BY precedent_id) AS tmp
SET e.comment_count_calc=tmp.comment_count
WHERE e.id=tmp.precedent_id");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- user_competence_set_link_calc
	function calc_080_user_competence_set_link_calc()
	{
		// -- personal_mark_count_calc
		$this->db->sql(
			"INSERT INTO user_competence_set_link_calc (user_id,competence_set_id,personal_mark_count_calc)
SELECT ratee_user_id, competence_set_id, count(*) FROM mark_calc WHERE type='personal' GROUP BY ratee_user_id,competence_set_id");

		// -- group_mark_count_calc
		$this->db->sql(
			"INSERT INTO user_competence_set_link_calc (user_id,competence_set_id,group_mark_count_calc)
SELECT ratee_user_id AS user_id, competence_set_id, count(*) AS c FROM mark_calc WHERE type='group' GROUP BY ratee_user_id,competence_set_id
ON DUPLICATE KEY UPDATE group_mark_count_calc=VALUES(group_mark_count_calc)");

		// -- total_mark_count_calc
		$this->db->sql("UPDATE user_competence_set_link_calc SET total_mark_count_calc=personal_mark_count_calc+group_mark_count_calc");

		// -- project_count_calc, количество проектов с !0 оценок
		$this->db->sql("
UPDATE user_competence_set_link_calc AS ucslc,
(
	-- количество проектов с оценками на пользователя/дерево_компетенций
	SELECT pcu.user_id, pcu.competence_set_id, COUNT( * ) AS cnt
	FROM (
		-- разные сочетания project_id, competence_set_id, ratee_user_id
		SELECT DISTINCT project_id, competence_set_id, ratee_user_id AS user_id
		FROM mark_calc
	) AS pcu
	GROUP BY pcu.user_id, pcu.competence_set_id
) AS tmp
SET ucslc.project_count_calc = tmp.cnt
WHERE ucslc.user_id = tmp.user_id AND ucslc.competence_set_id = tmp.competence_set_id");

		// -- personal_mark_count_with_trans_calc
		// -- group_mark_count_with_trans_calc
		// -- total_mark_count_with_trans_calc
		// -- project_count_with_trans_calc
		$this->db->sql("
-- инициализация: оценки без трансляторов
UPDATE user_competence_set_link_calc
SET
	personal_mark_count_with_trans_calc=personal_mark_count_calc,
	group_mark_count_with_trans_calc=group_mark_count_calc,
	total_mark_count_with_trans_calc=total_mark_count_calc,
	project_count_with_trans_calc=project_count_calc");

		$this->db->sql("
-- посчитаем оценки через трансляторы
UPDATE user_competence_set_link_calc AS ucslc3,
(
	-- для пользователя, набора_компетенций: сумма количеств_оценок, сумма количеств_проектов с учетом трансляции
	SELECT tmp1.user_id, tmp1.competence_set_id, SUM( tmp1.personal_mark_count_calc )  AS pmcc, SUM( tmp1.group_mark_count_calc ) AS gmcc, SUM( tmp1.total_mark_count_calc ) AS tmcc, SUM( project_count_calc ) AS pcc 
	FROM (
		-- для пользователя, набора_компетенций: набор_из_которого_транслируем, количества_оценок(3), количество_проектов_с_оценками
		SELECT ucslc1.user_id, ucslc1.competence_set_id, from_competence_set_id, ucslc2.personal_mark_count_calc, ucslc2.group_mark_count_calc, ucslc2.total_mark_count_calc, ucslc2.project_count_calc
		FROM user_competence_set_link_calc AS ucslc1, competence_translator, user_competence_set_link_calc AS ucslc2
		WHERE to_competence_set_id = ucslc1.competence_set_id
			AND ucslc1.user_id = ucslc2.user_id
			AND ucslc2.competence_set_id = from_competence_set_id
			AND is_default = 1
		) AS tmp1
	GROUP BY tmp1.user_id, tmp1.competence_set_id
) AS tmp2
SET 
	ucslc3.personal_mark_count_with_trans_calc=ucslc3.personal_mark_count_with_trans_calc+tmp2.pmcc,
	ucslc3.group_mark_count_with_trans_calc=ucslc3.group_mark_count_with_trans_calc+tmp2.gmcc,
	ucslc3.total_mark_count_with_trans_calc=ucslc3.total_mark_count_with_trans_calc+tmp2.tmcc,
	ucslc3.project_count_with_trans_calc=ucslc3.project_count_with_trans_calc+tmp2.pcc
WHERE
	ucslc3.user_id=tmp2.user_id 
	AND ucslc3.competence_set_id=tmp2.competence_set_id");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- user_competence_set_project_link
	function calc_090_user_competence_set_project_link()
	{
		// -- personal_mark_count_calc
		$this->db->sql(
			"INSERT INTO user_competence_set_project_link (user_id,competence_set_id,project_id,personal_mark_count_calc)
SELECT ratee_user_id, competence_set_id, project_id, count(*) FROM mark_calc WHERE type='personal' GROUP BY ratee_user_id,competence_set_id,project_id");

		// -- group_mark_count_calc
		$this->db->sql(
			"INSERT INTO user_competence_set_project_link (user_id,competence_set_id,project_id,group_mark_count_calc)
SELECT ratee_user_id AS user_id, competence_set_id, project_id, count(*) AS c FROM mark_calc WHERE type='group' GROUP BY ratee_user_id,competence_set_id,project_id
ON DUPLICATE KEY UPDATE group_mark_count_calc=VALUES(group_mark_count_calc)");

		// -- total_mark_count_calc
		$this->db->sql("UPDATE user_competence_set_project_link SET total_mark_count_calc=personal_mark_count_calc+group_mark_count_calc");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- user_group
	function calc_110_user_group()
	{
		$this->db->sql("UPDATE user_group, (SELECT user_group_id, count(*) AS ucc FROM user_user_group_link GROUP BY user_group_id) AS ugc SET user_count_calc=ucc WHERE id=user_group_id");
	}

// ------------------------------------------------------------------------------------------------------------------------
// ----- vector
	function calc_120_competence_count_calc()
	{
		$this->db->sql("
			UPDATE professiogram p
			LEFT JOIN (
			  SELECT r.id, COUNT(l.competence_id) as cnt
			  FROM rate r
			  LEFT JOIN rate_competence_link l ON l.rate_id = r.id
			  GROUP BY r.id
			) x ON x.id = p.rate_id
			SET competence_count_calc = IFNULL(x.cnt, 0);
		");
	}

}

?>