<?php
class dbconvert_1_clean
{
	private $db;
	
	function __construct( $db )
	{
		$this->db = $db;
	}

	function run()
	{
		global $config;
				
		$qv = array(
			"SET foreign_key_checks = 0",
			"truncate table competence_full",
			"truncate table competence_group",
			"truncate table competence_link",
			//"truncate table competence_set",
			"truncate table competence_set_admin",
			"truncate table precedent",
			"truncate table mark_calc",
			"truncate table professiogram",
			"truncate table project_role",
			"truncate table project",
			"truncate table project_admin",
			"truncate table project_intmenu_item",
			"truncate table rate",
			"truncate table rate_competence_link",
			"truncate table precedent_group",
			//"truncate table user",
			"truncate table user_project_link",
			"truncate table vector",
			"truncate table vector_focus",
			"truncate table vector_focus_history",
			"truncate table vector_future",
			"truncate table vector_future_competences_calc",
			"truncate table vector_future_history",
			"truncate table vector_self",
			"truncate table vector_self_history",
			"truncate table vector_self_stat",
			"truncate table competence_calc",
			"truncate table project_competence_set_link_calc",
			"truncate table user_competence_set_link_calc",
			"truncate table user_competence_set_project_link",
			"TRUNCATE TABLE precedent_competence_group_link",
			"TRUNCATE TABLE precedent_flash_group",
			"TRUNCATE TABLE precedent_flash_group_user_link",
			"TRUNCATE TABLE precedent_rater_link",
			"TRUNCATE TABLE precedent_comment",
			"TRUNCATE TABLE mark_group",
			"TRUNCATE TABLE mark_personal",
			"SET foreign_key_checks = 1",
		);
		
		foreach( $qv as $q )
		{
			$this->db->sql( $q );
		}
	}
}
?>