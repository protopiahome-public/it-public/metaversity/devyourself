<?php

/**
 * Data conversion: vector and project roles. It is not set on cron now.
 */
class dbconvert_4_vector
{

	/**
	 * @var db
	 */
	private $db;
	private $cmp4_dbname;
	private $idsrv_dbname;
	private $vector_convert_data;

	function __construct($db, $cmp4_dbname, $idsrv_dbname)
	{
		$this->db = $db;
		$this->cmp4_dbname = $cmp4_dbname;
		$this->idsrv_dbname = $idsrv_dbname;

		/**
		 * array(
		 *   <competence_set_id> => array(
		 *     "project_id" => <project_id>,
		 *     "add_time" => <datetime>,
		 *   ),
		 *   ...
		 * )
		 */
		$this->vector_convert_data = array(
			9 => array("project_id" => 16, "add_date" => "2010-02-15"),
			12 => array("project_id" => 16, "add_date" => "2010-02-15"),
			15 => array("project_id" => 16, "add_date" => "2010-02-15"),
			16 => array("project_id" => 14, "add_date" => "2010-01-20"),
			31 => array("project_id" => 26, "add_date" => "2010-10-16"),
			34 => array("project_id" => 31, "add_date" => "2008-10-01"),
		);
	}

	private function cvt_0006_project_role()
	{
		foreach ($this->vector_convert_data as $competence_set_id => $data)
		{
			$this->db->sql("
				INSERT INTO project_role (
					id, title, descr, add_time, edit_time, enabled, competence_set_id, adder_user_id,
					rate_id,
					project_id,
					based_on_professiogram_id
				)
				SELECT
					pf.id, pf.title, pf.`desc`, pf.create_datetime, pf.change_datetime, pf.is_public, pf.competence_tree_id, pf.author_member_id,
					insert_rate(2, pf.title, pf.competence_tree_id, pf.create_datetime, pf.change_datetime),
					{$data["project_id"]},
					IF((1 = pf.is_role AND 1 = pf.is_profession), pf.id, NULL)
				FROM {$this->cmp4_dbname}.professiogram pf
				WHERE 1 = pf.is_role AND pf.competence_tree_id = {$competence_set_id}
			");
		}
	}

	private function cvt_0007_rate_competence_link()
	{
		// This is possible only because each role belongs to only one project
		$this->db->sql("
			INSERT INTO rate_competence_link
				(rate_id, competence_id, mark)
			SELECT p.rate_id, pcm.competence_id, pcm.mark
			FROM {$this->cmp4_dbname}.professiogram_competence_mark pcm
			INNER JOIN project_role p ON p.id = pcm.professiogram_id
		");
	}
	
	private function cvt_0008_project_role_stat()
	{
		$this->db->sql("
			UPDATE project_role r
			LEFT JOIN (
			  SELECT r.id, COUNT(l.competence_id) as cnt
			  FROM rate r
			  LEFT JOIN rate_competence_link l ON l.rate_id = r.id
			  GROUP BY r.id
			) x ON x.id = r.rate_id
			SET competence_count_calc = IFNULL(x.cnt, 0);
		");
	}

	private function cvt_0100_vector()
	{
		$db_result = $this->db->sql("
			SELECT competence_tree_id, member_id
			FROM {$this->cmp4_dbname}.vector_professiogram_member_choice
			GROUP BY competence_tree_id, member_id
			ORDER BY competence_tree_id
		");
		$this->db->sql("SET group_concat_max_len = 512 * 1024");
		while ($row = $this->db->fetch_array($db_result))
		{
			$competence_set_id = $row["competence_tree_id"];
			$user_id = $row["member_id"];
			$project_id = $this->vector_convert_data[$competence_set_id]["project_id"];
			$add_date = $this->vector_convert_data[$competence_set_id]["add_date"];
			$this->db->sql("
				INSERT INTO vector
					(project_id, user_id, competence_set_id, calculations_base, add_time, edit_time)
				VALUES
					({$project_id}, {$user_id}, {$competence_set_id}, 'results', '{$add_date} 00:00:01', '{$add_date} 00:00:01')
			");
			$vector_id = $this->db->get_last_id();
			// Links to the vector - table user_project_link
			if ($this->db->row_exists("SELECT * FROM user_project_link WHERE user_id = {$user_id} AND project_id = {$project_id}"))
			{
				$this->db->sql("UPDATE user_project_link SET last_vector_id_calc = {$vector_id} WHERE user_id = {$user_id} AND project_id = {$project_id}");
			}
			else
			{
				$this->db->sql("
					INSERT INTO user_project_link
						(user_id, project_id, add_time, status, last_vector_id_calc)
					VALUES
						({$user_id}, {$project_id}, '{$add_date} 00:00:00', 'member', {$vector_id})
				");
			}
			// Links to the vector - table user_competence_set_link_calc
			if ($this->db->row_exists("SELECT * FROM user_competence_set_link_calc WHERE user_id = {$user_id} AND competence_set_id = {$competence_set_id}"))
			{
				$this->db->sql("UPDATE user_competence_set_link_calc SET default_vector_id_calc = {$vector_id} WHERE user_id = {$user_id} AND competence_set_id = {$competence_set_id}");
			}
			else
			{
				$this->db->sql("
					INSERT INTO user_competence_set_link_calc
						(user_id, competence_set_id, default_vector_id_calc)
					VALUES
						({$user_id}, {$competence_set_id}, {$vector_id})
				");
			}
			// Future
			$this->db->sql("
				INSERT INTO vector_future
				SELECT {$vector_id}, mc.professiogram_id, '{$add_date} 00:00:01'
				FROM {$this->cmp4_dbname}.vector_professiogram_member_choice mc
				INNER JOIN {$this->cmp4_dbname}.professiogram p ON p.id = mc.professiogram_id
				WHERE 
					mc.member_id = {$user_id}
					AND mc.competence_tree_id = {$competence_set_id}
					AND p.is_profession = 1
			");
			$n = $this->db->get_affected_row_count();
			$this->db->sql("
				UPDATE vector
				SET 
					future_professiogram_count_calc = {$n},
					edit_time = '{$add_date} 00:00:01',
					future_edit_time = '{$add_date} 00:00:01'
				WHERE id = {$vector_id}
			");
			$this->db->sql("
				INSERT INTO vector_future_history
				SELECT
					NULL, {$vector_id}, '{$add_date} 00:00:01',
					CAST(GROUP_CONCAT('+|', p.id, '|', p.title, '\n' ORDER BY p.title SEPARATOR '') AS CHAR)
				FROM {$this->cmp4_dbname}.vector_professiogram_member_choice x
				LEFT JOIN {$this->cmp4_dbname}.professiogram p ON p.id = x.professiogram_id
				WHERE x.member_id = {$user_id} AND x.competence_tree_id = {$competence_set_id}
				GROUP BY ''
			");
			// Self
			$this->db->sql("
				INSERT INTO vector_self
				SELECT member_id, competence_id, self_mark, competence_tree_id, '{$add_date} 00:00:03'
				FROM {$this->cmp4_dbname}.vector_competence_member_self_mark
				WHERE member_id = {$user_id} AND competence_tree_id = {$competence_set_id}
			");
			$n = $this->db->get_affected_row_count();
			if ($n)
			{
				$this->db->sql("
					UPDATE vector
					SET
						edit_time = '{$add_date} 00:00:02',
						self_edit_time = '{$add_date} 00:00:02',
						calculations_base = 'self'
					WHERE id = {$vector_id}
				");
				$this->db->sql("
					INSERT INTO vector_self_history
						(user_id, competence_set_id, add_time, log)
					SELECT
						{$user_id}, {$competence_set_id}, '{$add_date} 00:00:02',
						GROUP_CONCAT('+|', c.id, '|-|', x.self_mark, '|', c.title, '\n' ORDER BY c.id SEPARATOR '')
					FROM {$this->cmp4_dbname}.vector_competence_member_self_mark x
					LEFT JOIN {$this->cmp4_dbname}.competence c ON c.id = x.competence_id
					WHERE x.member_id = {$user_id} AND x.competence_tree_id = {$competence_set_id}
					GROUP BY ''
				");
			}
			// Focus
			$this->db->sql("
				INSERT INTO vector_focus
				SELECT {$vector_id}, competence_id, '{$add_date} 00:00:03'
				FROM {$this->cmp4_dbname}.vector_competence_member_observe
				WHERE member_id = {$user_id} AND competence_tree_id = {$competence_set_id}
			");
			$n = $this->db->get_affected_row_count();
			if ($n)
			{
				$this->db->sql("
					UPDATE vector
					SET
						focus_competence_count_calc = {$n},
						edit_time = '{$add_date} 00:00:03',
						focus_edit_time = '{$add_date} 00:00:03'
					WHERE id = {$vector_id}
				");
				$this->db->sql("
					INSERT INTO vector_focus_history
					SELECT
						NULL, {$vector_id}, '{$add_date} 00:00:03',
						CAST(GROUP_CONCAT('+|', c.id, '|', c.title, '\n' ORDER BY c.id SEPARATOR '') AS CHAR)
					FROM {$this->cmp4_dbname}.vector_competence_member_observe x
					LEFT JOIN {$this->cmp4_dbname}.competence c ON c.id = x.competence_id
					WHERE x.member_id = {$user_id} AND x.competence_tree_id = {$competence_set_id}
					GROUP BY ''
				");
			}
		}
		// project update
		foreach ($this->vector_convert_data as $competence_set_id => $data)
		{
			$project_id = $data["project_id"];
			$this->db->sql("
				UPDATE project
				SET
					vector_is_on = 1,
					vector_competence_set_id = {$competence_set_id}
				WHERE id = {$project_id}
			");
		}
		// calc tables - vector_self_stat
		$this->db->sql("
			INSERT INTO vector_self_stat
			SELECT user_id, competence_set_id, COUNT(*) as count, add_time
			FROM vector_self
			GROUP BY user_id, competence_set_id
		");
		// calc tables - vector_future_competences_calc
		$this->db->sql("
			INSERT INTO vector_future_competences_calc
			SELECT vf.vector_id, c.competence_id, MAX(rc.mark) as mark
			FROM vector_future vf
			LEFT JOIN professiogram p ON p.id = vf.professiogram_id
			LEFT JOIN rate_competence_link rc ON rc.rate_id = p.rate_id
			INNER JOIN competence_calc c ON c.competence_id = rc.competence_id
			GROUP BY vf.vector_id, c.competence_id
		");
	}

	function run()
	{
		global $config;
		$tocall = array();
		foreach (get_class_methods($this) as $v)
		{
			if (strlen($v) > 4 and 0 === substr_compare($v, "cvt_", 0, 4))
			{
				$tocall[] = $v;
			}
		}
		sort($tocall);
		foreach ($tocall as $v)
		{
			$this->$v();
		}
		return;
	}

}

?>