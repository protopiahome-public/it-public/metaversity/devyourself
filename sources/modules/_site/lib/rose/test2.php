<?
require_once "rose.php";

function rndd($n)
{
	$d = array();
	for ($i = 0; $i < $n; $i++)
	{
		$d[] = rand(0, 3);
	}
	return $d;
}

$scale = array(0, 20, 35, 40, 55, 80, 120, 250, 320);

$r = new rose(
		$scale, // массив надписей для осей,
		array(0, 20, 40, 80), // массив надписей, которые нужно выделить
		320, // ширина квадрата
		null, // откуда брать шрифты
		0x00FFFFFF, // цвет фона
		0x00E0E0E0, // цвет осей
		0x00000000, // цвет надписей
		0x00800000  // цвет первой надписи
);

//header( "Content-Type: image/png" );

$r->draw(
	rndd(count($scale)), // массив данных по осям
	0x00FF0000, // цвет обводки
	0x50FF0000  // цвет заполнения
);
$r->draw(rndd(count($scale)), 0x0000FF00, 0x5000FF00);
$r->draw(rndd(count($scale)), 0x000000FF, 0x500000FF);
$r->draw(rndd(count($scale)), 0x00A0A0A0);

ob_start();

$r->to_png(
	NULL	// имя файла или NULL для stdout
);

$img_data = ob_get_contents();
ob_end_clean();

/*

  $map = rose::get_map( 10,320 );
  echo "<pre>".print_r($map,1)."</pre>";

 */
?>
<html>
	<head>
	</head>
	<body>
		<img src="data:image/png;base64,<?= base64_encode($img_data) ?>">
	</body>
</html>