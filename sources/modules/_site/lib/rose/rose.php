<?php

/*
  класс для рисования "розочек"
 */

class rose
{

	private $width;
	private $font_path;
	private $axis_count;
	private $ktx, $kty, $ktr; // осевые коэффициенты
	private $cen_x, $cen_y; // координаты центра
	private $img;   // собственно, картинка

	public static function get_map(
	$count, // число точек на диаграмме
		$width				// ширина диаграммы
	)
	{
		$cen_x = $width / 2;
		$cen_y = $width / 2;
		$map = array();

		for ($i = 0; $i < $count; $i++)
		{
			$mm = array();

			$rm = ($width / 2);
			$r3 = $rm - 28;

			// левый радиус
			$ktx = cos(($i - 0.45) * 2 * M_PI / $count - M_PI / 2);
			$kty = sin(($i - 0.45) * 2 * M_PI / $count - M_PI / 2);
			$mm[] = round($cen_x + 8 * $ktx);
			$mm[] = round($cen_y + 8 * $kty);
			$mm[] = round($cen_x + $r3 * $ktx);
			$mm[] = round($cen_y + $r3 * $kty);

			// вершинка
			$ktx = cos($i * 2 * M_PI / $count - M_PI / 2);
			$kty = sin($i * 2 * M_PI / $count - M_PI / 2);
			$mm[] = round($cen_x + $rm * $ktx);
			$mm[] = round($cen_y + $rm * $kty);

			// правый радиус
			$ktx = cos(($i + 0.45) * 2 * M_PI / $count - M_PI / 2);
			$kty = sin(($i + 0.45) * 2 * M_PI / $count - M_PI / 2);
			$mm[] = round($cen_x + $r3 * $ktx);
			$mm[] = round($cen_y + $r3 * $kty);
			$mm[] = round($cen_x + 8 * $ktx);
			$mm[] = round($cen_y + 8 * $kty);

			$map[] = implode(",", $mm);
		}
		return $map;
	}

	function __construct(
	$axis, // массив надписей для осей,
		$bold=false, // массив надписей, которые нужно выделить
		$width=480, // ширина квадрата
		$font_path=null, // откуда брать шрифты
		$hcl_bgnd=0x00FFFFFF, // цвет фона
		$hcl_axis=0x00E0E0E0, // цвет осей
		$hcl_text=0x00000000, // цвет надписей
		$hcl_text0=false   // цвет первой надписи
	)
	{
		if (is_null($font_path))
		{
			$font_path = dirname(__FILE__);
		}
		$this->font_path = $font_path;

		// TODO: количество осей больше 2
		$this->axis_count = count($axis);
		$this->width = $width;

		// координаты центра
		$this->cen_x = $this->width / 2;
		$this->cen_y = $this->width / 2;

		// рассчет осевых коэффициентов
		$ktx = array();
		$kty = array();
		for ($i = 0; $i < $this->axis_count; $i++)
		{
			$this->ktx[] = cos($i * 2 * M_PI / $this->axis_count - M_PI / 2);
			$this->kty[] = sin($i * 2 * M_PI / $this->axis_count - M_PI / 2);
		}

		// надо, чтобы это был массив
		if (!is_array($bold))
		{
			$bold = array();
		}

		// рассчет радиусов
		$this->ktr = array();
		$this->ktr[3] = ($this->width / 2) - 36;
		$this->ktr[2] = 2 * $this->ktr[3] / 3;
		$this->ktr[1] = $this->ktr[3] / 3;
		$this->ktr[0] = 8;

		// создать пустую картинку, заполнить ее цветом фона
		$this->img = imagecreatetruecolor($this->width, $this->width);
		$cl_bgnd = $this->hex2color($hcl_bgnd);
		$cl_axis = $this->hex2color($hcl_axis);
		$cl_text = $this->hex2color($hcl_text);
		$cl_text0 = ( $hcl_text0 !== false ) ? $this->hex2color($hcl_text0) : $cl_text;
		imagefilledrectangle($this->img, 0, 0, $this->width - 1, $this->width - 1, $cl_bgnd);

		// эти цвета потом пригодятся!
		$this->cl_text = $cl_text;

		// прорисовка окружностей
		for ($i = 1; $i < 4; $i++)
		{
			$rad = $this->ktr[$i] * 2;
			imagearc($this->img, $this->cen_x, $this->cen_y, $rad, $rad, 0, 0, $cl_axis);
		}

		// прорисовка осей
		for ($i = 0; $i < $this->axis_count; $i++)
		{
			$x = $this->cen_x + $this->ktx[$i] * $this->ktr[3];
			$y = $this->cen_y + $this->kty[$i] * $this->ktr[3];
			imageline($this->img, $this->cen_x, $this->cen_y, $x, $y, $cl_axis);
		}

		// прорисовка надписей
		
		$font_size_base = 10;
		$font_dx = -3;
		$font_dy = -1;
		if ($this->axis_count>19)
		{
			$font_dx = 0;
			$font_dy = -1;
			$font_size_base = 9;
		}
		if ($this->axis_count>29)
		{
			$font_dx = 2;
			$font_dy = -2;
			$font_size_base = 8;
		}
		if ($this->axis_count>39)
		{
			$font_dx = 4;
			$font_dy = -3;
			$font_size_base = 7.5;
		}
		
		for ($i = 0; $i < $this->axis_count; $i++)
		{
			$font_size = ($axis[$i] > 999) ? $font_size_base-1 : $font_size_base;
			if (in_array($axis[$i], $bold) and $font_size>7 )
			{
				$font_size += 1;
			}
			
			$bbox = imagettfbbox($font_size, 0, $font_path ."/arial.ttf", $axis[$i]);
			$text_halfwidth = ($bbox[2] - $bbox[0]) / 2;
			$text_halfheight = ($bbox[1] - $bbox[5]) / 2;
			$x = $this->cen_x + $this->ktx[$i] * ($this->width / 2 - 32 + $text_halfwidth);
			$y = $this->cen_y + $this->kty[$i] * ($this->width / 2 - 30 + $text_halfheight);
			$t_x = $x - $text_halfwidth;
			$t_y = $y + $text_halfheight;
			
			if (in_array($axis[$i], $bold))
			{
				$r = imagettftext($this->img, $font_size, 0, $t_x, $t_y, $cl_text0, $font_path . "/arial.ttf", $axis[$i]);
				$r = imagettftext($this->img, $font_size, 0, $t_x + 1, $t_y, $cl_text0, $font_path . "/arial.ttf", $axis[$i]);
			}
			else
			{
				$r = imagettftext($this->img, $font_size, 0, $t_x, $t_y, $cl_text0, $font_path . "/arial.ttf", $axis[$i]);
			}
			$cl_text0 = $cl_text;
		}
	}

	function draw(
	$data, // массив значений от 0 до 2
		$color = false, // цвет
		$color_fill = false // цвет заполнения
	)
	{
		for ($i = 0; $i < $this->axis_count; $i++)
		{
			$tmp[] = $this->cen_x + $this->ktr[$data[$i]] * $this->ktx[$i];
			$tmp[] = $this->cen_y + $this->ktr[$data[$i]] * $this->kty[$i];
		}

		if (sizeof($tmp) > 4)
		{
			if ($color !== false)
			{
				$cl_1 = $this->hex2color($color);
				imagepolygon($this->img, $tmp, $this->axis_count, $cl_1);
			}

			if ($color_fill !== false)
			{
				$cl_1 = $this->hex2color($color_fill);
				imagefilledpolygon($this->img, $tmp, $this->axis_count, $cl_1);
			}
		}
	}

	function to_png($fname)
	{
		// надписи над окружностями
		$r = imagettftext($this->img, 10, 0, $this->cen_x - 4, $this->cen_y - $this->ktr[3] + 14, $this->cl_text, $this->font_path . "/arial.ttf", "К.");
		$r = imagettftext($this->img, 10, 0, $this->cen_x - 3, $this->cen_y - $this->ktr[3] + 14, $this->cl_text, $this->font_path . "/arial.ttf", "К.");

		$r = imagettftext($this->img, 10, 0, $this->cen_x - 8, $this->cen_y - $this->ktr[2] + 14, $this->cl_text, $this->font_path . "/arial.ttf", "Сп.");
		$r = imagettftext($this->img, 10, 0, $this->cen_x - 7, $this->cen_y - $this->ktr[2] + 14, $this->cl_text, $this->font_path . "/arial.ttf", "Сп.");

		$r = imagettftext($this->img, 10, 0, $this->cen_x - 8, $this->cen_y - $this->ktr[1] + 14, $this->cl_text, $this->font_path . "/arial.ttf", "Ск.");
		$r = imagettftext($this->img, 10, 0, $this->cen_x - 7, $this->cen_y - $this->ktr[1] + 14, $this->cl_text, $this->font_path . "/arial.ttf", "Ск.");

		// сохранить картинку в файл
		imagepng($this->img, $fname, 9);
	}

	private function hex2color($color)
	{
		$blue = $color & 0xFF;
		$green = ($color & 0xFF00) >> 8;
		$red = ($color & 0xFF0000) >> 16;
		$alpha = ($color & 0xFF000000) >> 24;
		//return imagecolorallocate( $this->img, $red, $green, $blue );
		return imagecolorallocatealpha($this->img, $red, $green, $blue, $alpha);
	}

}

?>