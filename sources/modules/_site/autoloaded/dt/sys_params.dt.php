<?php

class sys_params_dt extends base_dt
{
	
	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	public function __construct()
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", "Основное");

		$dtf = new string_dtf("title", "Название инсталляции");
		$dtf->set_max_length(60);
		$dtf->set_importance(true);
		$dtf->set_comment("Краткость — сестра таланта.");
		$this->add_field($dtf, "main");
		
		$dtf = new text_html_dtf("main_page_text", "Текст на главной странице");
		$dtf->set_editor_height(200);
		$dtf->set_body_class("color-yellow content content-about");
		$this->add_field($dtf, "main");
		
		$dtf = new image_pack_dtf("logo", "Логотип проекта", PATH_TMP);
		$dtf->add_image("main", "Main", 0, 0, "/data/logo/", null, PATH_PUBLIC_DATA . "/logo");
		$dtf->get_image_by_name("main")->add_processor(new image_pack_crop_processor(150, 28));
		$dtf->set_add_prefix_to_url($this->domain_logic->get_main_prefix());
		$dtf->set_comment("Не более 150х28 пикселей.");
		$this->add_field($dtf);
		
		$dtf = new email_dtf("info_email", "Контактный email");
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$dtf->set_comment("Отображается в подвале и ещё в некоторых местах на сайте.");
		$this->add_field($dtf, "main");
		
		$dtf = new int_dtf("copyright_start_year", "Год запуска инсталляции");
		$dtf->set_importance(true);
		$dtf->set_default_value(date("Y"));
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$dtf->set_min_value(2007);
		$dtf->set_max_value(2030);
		$dtf->set_comment("Отображается в подвале.");
		$this->add_field($dtf, "main");
		
		$dtf = new string_dtf("ga_tracking_id", "Код отслеживания Google Analytics");
		$dtf->set_max_length(60);
		$dtf->set_importance(false);
		$dtf->set_regexp("/^UA-[1-9][0-9]*-[1-9][0-9]*$/", "Некорректное значение. Ожидается что-то вроде UA-1234567-123");
		$dtf->set_comment("Формат: UA-1234567-123. Оставьте пустую строку, если вы не хотите использовать Google Analytics.");
		$this->add_field($dtf, "main");
		
		$this->add_fields_in_axis("full", array("title", "main_page_text", "logo", "info_email", "copyright_start_year", "ga_tracking_id"));
		$this->add_fields_in_axis("short", array("title", "logo", "info_email", "copyright_start_year", "ga_tracking_id"));
		$this->add_fields_in_axis("edit", array("title", "main_page_text", "logo", "info_email", "copyright_start_year", "ga_tracking_id"));
	}

}

?>