<?php

class university_short_xml_ctrl extends base_simple_xml_ctrl
{

	protected $university_id;
	protected $data_one_row = true;
	protected $row_name = "university";
	protected $allow_error_404_if_no_data = false;
	protected $cache = true;
	protected $cache_key_property_name = "university_id";

	public function __construct($university_id)
	{
		$this->university_id = $university_id;
		parent::__construct();
	}

	protected function set_result()
	{
		$this->result = "
			SELECT
				id, title, title_full
			FROM university
			WHERE id = {$this->university_id}
		";
	}

}

?>