<?php

class translator_short_xml_ctrl extends base_simple_xml_ctrl
{

	protected $translator_id;

	protected $data_one_row = true;

	protected $row_name = "translator";

	protected $allow_error_404_if_no_data = true;

	protected $cache = true;

	protected $cache_key_property_name = "translator_id";

	public function __construct($translator_id)
	{
		$this->translator_id = $translator_id;
		
		parent::__construct();
	}

	protected function set_result()
	{
		$this->result = "
			SELECT
				id,
				from_competence_set_id,
				to_competence_set_id,
				title
			FROM competence_translator
			WHERE id = {$this->translator_id}
		";
	}

	protected function set_auxil_data()
	{
		
	}

}

?>