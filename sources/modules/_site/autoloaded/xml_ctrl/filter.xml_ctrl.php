<?php

class filter_xml_ctrl extends base_xml_ctrl
{
	
	/**
	 * @var filter_main_math
	 */
	private $filter_main;

	/**
	 * @var filter_raters_real_math
	 */
	private $filter_raters_real;

	private $used_mark_count;

	private $used_rater_count;

	public function __construct(filter_main_math $filter_main, filter_raters_real_math $filter_raters_real = null, $used_mark_count = null, $used_rater_count = null)
	{
		$this->filter_main = $filter_main;
		$this->filter_raters_real = $filter_raters_real;
		$this->used_mark_count = $used_mark_count;
		$this->used_rater_count = $used_rater_count;
		
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		$xdom = xdom::create("filter");
		if (!is_null($this->used_mark_count))
		{
			$xdom->set_attr("used_mark_count", $this->used_mark_count);
		}
		$xdom->set_attr("competence_set_id", $this->filter_main->get_competence_set_id());

		/* Main filter */
		$competence_sets_node = $xdom->create_child_node("competence_sets");
		foreach ($this->filter_main->get_filter() as $competence_set_id => $competence_set_data)
		{
			$competence_set_node = $competence_sets_node->create_child_node("competence_set")
				->set_attr("id", $competence_set_id);
			$projects_node = $competence_set_node->create_child_node("projects");
			foreach ($competence_set_data["projects"] as $project_id => $project_data)
			{
				$project_node = $projects_node->create_child_node("project")
				->set_attr("id", $project_id)
				->set_attr("title", $project_data["title"])
				->set_attr("total_mark_count", $project_data["total_mark_count"])
				->set_attr("is_selected", isset($project_data["is_selected"]) and $project_data["is_selected"]);
			}
			if (isset($competence_set_data["translators"]))
			{
				$translators_node = $competence_set_node->create_child_node("translators");
				foreach ($competence_set_data["translators"] as $translator_id => $translator_data)
				{
					$translator_node = $translators_node->create_child_node("translator")
					->set_attr("translator_id", $translator_data["translator_id"])
					->set_attr("translator_title", $translator_data["translator_title"])
					->set_attr("from_competence_set_id", $translator_data["from_competence_set_id"])
					->set_attr("from_competence_set_title", $translator_data["from_competence_set_title"])
					->set_attr("is_selected", isset($translator_data["is_selected"]) and $translator_data["is_selected"]);
				}
			}
		}
		//dd($this->filter_main->get_competence_set_id(), $this->filter_main->get_filter());

		/* rater filter */
		$rater_user_groups_node = $xdom->create_child_node("rater_user_groups");
		$rater_user_groups_node->set_attr("total_rater_count", $this->filter_raters_real->get_total_rater_count());
		$rater_user_groups_node->set_attr("used_rater_count", $this->used_rater_count);
		$rater_user_groups_node->set_attr("not_grouped_rater_count", $this->filter_raters_real->get_not_grouped_rater_count());
		$rater_user_groups_node->set_attr("select_all_checked", $this->filter_raters_real->is_select_all_checked());
		foreach ($this->filter_raters_real->get_user_groups() as $user_group_id => $user_group_data)
		{
			$user_group_node = $rater_user_groups_node->create_child_node("user_group")
			->set_attr("id", $user_group_id)
			->set_attr("title", $user_group_data["title"])
			->set_attr("user_count", $user_group_data["user_count"])
			->set_attr("is_selected", isset($user_group_data["is_selected"]) and $user_group_data["is_selected"]);
		}
		//dd($this->filter_raters_real->get_user_groups());

		return $xdom->get_xml(true);
	}

}

?>