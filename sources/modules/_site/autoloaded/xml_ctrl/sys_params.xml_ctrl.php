<?php

class sys_params_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "sys_params";
	protected $axis_name = "short";

	public function __construct()
	{
		parent::__construct(1);
	}

	protected function get_cache()
	{
		return sys_params_cache::init();
	}
	
	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("is_devyourself");
	}
	
	protected function fill_xml(xdom $xdom)
	{
		parent::fill_xml($xdom);
		$xdom->set_attr("is_devyourself", $this->data[0]["is_devyourself"]);
	}

}

?>