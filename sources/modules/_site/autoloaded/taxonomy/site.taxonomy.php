<?php

final class site_taxonomy extends base_taxonomy
{

	public function run()
	{
		$p = $this->get_parts_relative();

		$this->xml_loader->set_main_xslt("base", "_site");
		$this->xml_loader->add_xml(new pass_info_xml_ctrl());
		$this->xml_loader->add_xml(new user_xml_ctrl());
		if ($this->user->get_user_id())
		{
			$this->xml_loader->add_xml(new user_head_xml_ctrl());
		}

		$this->xml_loader->add_xml(new request_xml_ctrl());
		$this->xml_loader->add_xml(new sys_params_xml_ctrl());

		if ($this->request->get_user_agent_name() == "ie" && $this->request->get_user_agent_version() <= 7.0)
		{
			response::set_content_html('
				<p><strong>Вы используете устаревший браузер</strong> (Internet Explorer < 8).</p>
				<p>Пожалуйста, скачайте последнюю версию одного из поддерживаемых браузеров:</p>
				<p>
					<a href="http://www.opera.com/">Opera</a><br/>
					<a href="http://www.google.com/chrome/">Google Chrome</a><br/>
					<a href="http://www.mozilla.com/">Mozilla Firefox</a><br/>
					<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home">Internet Explorer</a><br/>
				</p>
			');
			return;
		}

		if ($this->domain_logic->is_on() and !$this->domain_logic->is_main_domain())
		{
			if ($this->domain_logic->is_secondary_domain())
			{

				$this->xml_loader->set_redirect_url($this->domain_logic->get_main_prefix() . "/" . $this->request->get_all_folders() . $this->request->get_params_string());
				$this->xml_loader->set_redirect_type_permanent();
			}
			elseif (($project_id = $this->domain_logic->get_current_domain_project_id()))
			{
				$project_taxonomy = new project_taxonomy($this->xml_loader, $this, 0, $project_id, true);
				$project_taxonomy->run();
			}
			else
			{
				$this->xml_loader->add_xml_with_xslt(new unknown_host_xml_page());
				$this->xml_loader->set_error_404();
			}
			$this->check_404();
			return;
		}

		if ($p[1] === null)
		{
			// (Main page)
			$this->xml_loader->add_xml_with_xslt(new main_xml_page());
		}
		/* 		
		  elseif ($p[1] === "login" and $project_id = $this->get_project_id_by_url_name($p[2]) )
		  {
		  $this->xml_loader->add_xml_with_xslt(new login_xml_page($project_id));
		  $this->xml_loader->add_xml_by_class_name("project_short_xml_ctrl", $project_id);
		  }
		 */
		elseif ($p[1] === "projects" or $p[1] === "p")
		{
			$projects_taxonomy = new projects_taxonomy($this->xml_loader, $this);
			$projects_taxonomy->run();
		}
		elseif ($p[1] === "users")
		{
			$users_taxonomy = new users_taxonomy($this->xml_loader, $this, 1);
			$users_taxonomy->run();
		}
		elseif ($p[1] === "ratings")
		{
			$ratings_taxonomy = new ratings_taxonomy($this->xml_loader, $this, 1);
			$ratings_taxonomy->run();
		}
		elseif ($p[1] === "sets")
		{
			$competences_taxonomy = new competences_taxonomy($this->xml_loader, $this, 1);
			$competences_taxonomy->run();
		}
		elseif ($p[1] === "admin" and $this->user->is_admin())
		{
			//admin/
			$this->xml_loader->add_xml_with_xslt(new admin_xml_page());
		}
		elseif ($p[1] === "cache-cleaner" and $p[2] === null /* and $this->error->is_debug_mode() */)
		{
			//cache-cleaner/
			$this->xml_loader->add_xml(new cache_cleaner_xml_ctrl());
			//@dm9
			$this->xml_loader->add_xslt("main", "_site");
		}

		if (!$this->xml_loader->is_xml_page_loaded())
		{
			$auth_taxonomy = new auth_taxonomy($this->xml_loader, $this);
			$auth_taxonomy->run();
		}
		if (!$this->xml_loader->is_xml_page_loaded())
		{
			$research_taxonomy = new research_taxonomy($this->xml_loader, $this);
			$research_taxonomy->run();
		}

		$this->check_404();
	}

	private function get_project_id_by_url_name($url_name)
	{
		return url_helper::get_project_id_by_name($url_name);
	}

	protected function check_404()
	{
		if (!$this->xml_loader->is_xml_page_loaded() and !$this->xml_loader->get_redirect_url())
		{
			if (!($this->xml_loader->is_error_404() || $this->xml_loader->is_error_403()))
			{
				$this->xml_loader->add_xml_with_xslt(new error_404_xml_page());
			}
		}
	}

}

?>