<?php

class admin_save_page extends base_dt_edit_save_ctrl
{

	protected $dt_name = "sys_params";
	protected $axis_name = "edit";
	
	protected function fill_id()
	{
		$this->id = 1;
	}
	
	public function check_rights()
	{
		return $this->user->is_admin();
	}

	public function clean_cache()
	{
		sys_params_cache_tag::init()->update();
	}
	
}

?>