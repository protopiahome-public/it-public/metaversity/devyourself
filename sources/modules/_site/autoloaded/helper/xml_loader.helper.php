<?php

require_once PATH_CORE . "/loader/xml_loader.php";

class xml_loader_helper
{

	/**
	 *
	 * @var xml_loader
	 */
	protected $xml_loader;

	public function __construct()
	{
		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new request_xml_ctrl());
		$this->xml_loader = $xml_loader;
	}

	/**
	 *
	 * @return xml_loader
	 */
	public function get_xml_loader()
	{
		return $this->xml_loader;
	}

	public function get_html()
	{
		$this->xml_loader->run();
		return $this->xml_loader->get_content();
	}

}

?>
