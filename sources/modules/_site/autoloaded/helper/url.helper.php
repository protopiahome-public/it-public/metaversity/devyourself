<?php

class url_helper
{

	public static function get_user_id_by_login($login)
	{
		/* @var $db db */
		global $db;

		$user_id = taxonomy_user_cache::init($login)->get();
		if (!$user_id)
		{
			$login_escaped = $db->escape($login);
			if ($user_id = $db->get_value("SELECT id FROM user WHERE BINARY login = '{$login_escaped}'"))
			{
				taxonomy_user_cache::init($login, $user_id)->set($user_id);
			}
		}
		return $user_id;
	}

	public static function extract_project_id($url)
	{
		/* @var $domain_logic domain_logic */
		global $domain_logic;

		// @dm9 the same logic in auth_xml_page
		if ($domain_logic->is_on()
			and ($host_name = parse_url($url, PHP_URL_HOST))
			and ($project_id = $domain_logic->get_project_id_by_host_name($host_name)))
		{
			return $project_id;
		}

		if (!($url_without_prefix = self::remove_prefix($url)))
		{
			return false;
		}

		if (preg_match("#^/p/([a-z0-9\-]+)/#i", $url_without_prefix, $matches))
		{
			return self::get_project_id_by_name($matches[1]);
		}
		else
		{
			return false;
		}
	}

	public static function get_project_id_by_name($project_name)
	{
		/* @var $db db */
		global $db;

		$project_id = taxonomy_project_cache::init($project_name)->get();
		if (!$project_id)
		{
			$project_name_escaped = $db->escape($project_name);
			if (($project_id = $db->get_value("SELECT id FROM project WHERE BINARY name = '{$project_name_escaped}'")))
			{
				taxonomy_project_cache::init($project_name, $project_id)->set($project_id);
			}
		}
		return $project_id;
	}

	public static function extract_community_id($url, $check_project_id)
	{
		/* @var $domain_logic domain_logic */
		global $domain_logic;

		// @dm9 what is prefix when we're on domain's host?
		$url_without_prefix = self::remove_prefix($url);
		if (!$url_without_prefix)
		{
			return false;
		}

		$project_id = false;
		$community_name = "";

		if ($domain_logic->is_on() and preg_match("#^/co/([a-z0-9\-]+)/#i", $url_without_prefix, $matches))
		{
			$project_id = $check_project_id;
			$community_name = $matches[1];
		}
		elseif (!$domain_logic->is_on() and preg_match("#^/p/([a-z0-9\-]+)/co/([a-z0-9\-]+)/#i", $url_without_prefix, $matches))
		{
			$project_id = self::extract_project_id($url);
			$community_name = $matches[2];
		}

		if (!$project_id or $project_id != $check_project_id)
		{
			return false;
		}

		return self::get_community_id_by_name($community_name, $project_id);
	}

	public static function get_community_id_by_name($community_name, $project_id)
	{
		/* @var $db db */
		global $db;

		$community_id = taxonomy_community_cache::init($community_name, $project_id)->get();
		if (!$community_id)
		{
			$community_name_escaped = $db->escape($community_name);
			$community_id = $db->get_value("
				SELECT id 
				FROM community 
				WHERE 
					BINARY name = '{$community_name_escaped}' 
					AND project_id = {$project_id} 
					AND is_deleted = 0
			");
			if ($community_id)
			{
				taxonomy_community_cache::init($community_name, $project_id, $community_id)->set($community_id);
			}
		}
		return $community_id;
	}

	public static function extract_section_id($url, $check_community_id, $check_project_id)
	{
		/* @var $domain_logic domain_logic */
		global $domain_logic;

		$url_without_prefix = self::remove_prefix($url);
		if (!$url_without_prefix)
		{
			return false;
		}

		$section_name = false;

		if ($domain_logic->is_on() and preg_match("#^/co/([a-z0-9\-]+)/([a-z0-9\-]+)/#i", $url_without_prefix, $matches))
		{
			$section_name = $matches[2];
		}
		elseif (!$domain_logic->is_on() and preg_match("#^/p/([a-z0-9\-]+)/co/([a-z0-9\-]+)/([a-z0-9\-]+)/#i", $url_without_prefix, $matches))
		{
			$section_name = $matches[3];
		}

		if ($section_name)
		{
			$community_id = self::extract_community_id($url, $check_project_id);
			if (!$community_id or $community_id != $check_community_id)
			{
				return false;
			}
			return self::get_section_id_by_name($section_name, $community_id);
		}
		else
		{
			return false;
		}
	}

	public static function get_section_id_by_name($section_name, $community_id)
	{
		/* @var $db db */
		global $db;

		$section_id = taxonomy_section_cache::init($section_name, $community_id)->get();
		if (!$section_id)
		{
			$section_name_escaped = $db->escape($section_name);
			$section_id = $db->get_value("
				SELECT id 
				FROM section 
				WHERE 
					BINARY name = '{$section_name_escaped}' 
					AND community_id = {$community_id}
			");
			if ($section_id)
			{
				taxonomy_section_cache::init($section_name, $community_id)->set($section_id);
			}
		}
		return $section_id;
	}

	public static function get_project_custom_feed_id_by_name($project_custom_feed_name, $project_id)
	{
		/* @var $db db */
		global $db;

		$project_custom_feed_id = taxonomy_project_custom_feed_cache::init($project_custom_feed_name, $project_id)->get();
		if (!$project_custom_feed_id)
		{
			$project_custom_feed_name_escaped = $db->escape($project_custom_feed_name);
			$project_custom_feed_id = $db->get_value("
				SELECT id 
				FROM project_custom_feed 
				WHERE 
					BINARY name = '{$project_custom_feed_name_escaped}' 
					AND project_id = {$project_id}
			");
			if ($project_custom_feed_id)
			{
				taxonomy_project_custom_feed_cache::init($project_custom_feed_name, $project_id, $project_custom_feed_id)->set($project_custom_feed_id);
			}
		}
		return $project_custom_feed_id;
	}

	public static function get_community_custom_feed_id_by_name($community_custom_feed_name, $community_id)
	{
		/* @var $db db */
		global $db;

		$community_custom_feed_id = taxonomy_community_custom_feed_cache::init($community_custom_feed_name, $community_id)->get();
		if (!$community_custom_feed_id)
		{
			$community_custom_feed_name_escaped = $db->escape($community_custom_feed_name);
			$community_custom_feed_id = $db->get_value("
				SELECT id 
				FROM community_custom_feed 
				WHERE 
					BINARY name = '{$community_custom_feed_name_escaped}' 
					AND community_id = {$community_id}
			");
			if ($community_custom_feed_id)
			{
				taxonomy_community_custom_feed_cache::init($community_custom_feed_name, $community_id, $community_custom_feed_id)->set($community_custom_feed_id);
			}
		}
		return $community_custom_feed_id;
	}

	public static function get_static_page_id_by_name($static_page_name, $project_id)
	{
		/* @var $db db */
		global $db;

		$static_page_id = taxonomy_static_page_cache::init($static_page_name, $project_id)->get();
		if (!$static_page_id)
		{
			$static_page_name_escaped = $db->escape($static_page_name);
			$static_page_id = $db->get_value("
				SELECT id 
				FROM project_taxonomy_item
				WHERE 
					BINARY static_page_name = '{$static_page_name_escaped}' 
					AND project_id = {$project_id} 
					AND type = 'static_page'
			");
			if ($static_page_id)
			{
				taxonomy_static_page_cache::init($static_page_name, $project_id, $static_page_id)->set($static_page_id);
			}
		}
		return $static_page_id;
	}

	public static function get_taxonomy_module_id_by_module_name($module_name, $project_id)
	{
		/* @var $db db */
		global $db;

		$module_id = taxonomy_module_cache::init($module_name, $project_id)->get();
		if (!$module_id)
		{
			$module_name_escaped = $db->escape($module_name);
			$module_id = $db->get_value("
				SELECT id 
				FROM project_taxonomy_item
				WHERE 
					BINARY module_name = '{$module_name_escaped}' 
					AND project_id = {$project_id} 
					AND type = 'module'
			");
			if ($module_id)
			{
				taxonomy_module_cache::init($module_name, $project_id, $module_id)->set($module_id);
			}
		}
		return $module_id;
	}

	protected static function remove_prefix($url)
	{
		/* @var $request request */
		global $request;
		if (!str_begins($url, $request->get_full_prefix()))
		{
			return false;
		}

		return substr($url, strlen($request->get_full_prefix()));
	}

}

?>