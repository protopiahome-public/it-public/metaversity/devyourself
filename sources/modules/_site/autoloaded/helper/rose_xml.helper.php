<?php

require_once PATH_MODULE_SITE_LIB . "/rose/rose.php";

class rose_xml_helper
{

	public static function rose_output($competence_id_array, $need_values, $results, $self_data, $highlight_competence_id_array, $user_id, $show_self_by_default = false)
	{
		// Rose colors
		$result_colors = array(
			"case1" => array("border" => 0x009d5969, "background" => 0x50c2002d),
			"case2" => array("border" => 0x005ea28f, "background" => 0x500dcf99),
			"case3" => array("border" => 0x00a67e59, "background" => 0x50dc6a00),
			"case4" => array("border" => 0x008e9359, "background" => 0x5098a600),
			"case5" => array("border" => 0x007759a1, "background" => 0x505600cb),
			"need" => array("border" => 0x00818181, "background" => 0x40727272),
			"sum" => array("border" => 0x005d935f, "background" => 0x450ba611),
			"self" => array("border" => 0x00a659a4, "background" => 0x50db00d3),
		);
		$rose = new rose(
				$competence_id_array, // Axes captions,
				array_intersect($competence_id_array, $highlight_competence_id_array), // Captions to highlight
				320, // Image width
				null, // Fonts dir
				0x00FFFFFF, // Background color
				0x00E0E0E0, // Сolor of axes
				0x00000000, // Color of titles
				0x00800000 // First title color
		);

		// First plot - necessary level of the competences for the professiogram
		$rose->draw($need_values, $result_colors["need"]["border"], $result_colors["need"]["background"]);

		// Project results to draw...
		$result_keys = array();
		for ($i = 1; $i <= 5; $i++)
		{
			$key = "case" . $i;
			if (isset($_GET[$key]) and isset($results[$user_id][$_GET[$key]]))
			{
				$result_keys[$key] = $_GET[$key];
			}
		}

		if (!count($result_keys))
		{
			// ... if no project results to draw, drawing the summary result
			$result_keys["sum"] = "sum";
		}

		if (isset($_GET["self"]) and $_GET["self"] === "1")
		{
			// Self marks
			$results[$user_id]["self"] = $self_data;
			$result_keys["self"] = "self";
		}

		// Drawing plots...
		foreach ($result_keys as $k => $v)
		{
			$results_draw_array = array();
			foreach ($competence_id_array as $competence_id)
			{
				$results_draw_array[] = isset($results[$user_id][$v][$competence_id]) ? $results[$user_id][$v][$competence_id] : 0;
			}
			$rose->draw($results_draw_array, $result_colors[$k]["border"], $result_colors[$k]["background"]);
		}

		// PNG output
		$tmp_name = PATH_TMP . "/" . uniqid("rose_");
		$rose->to_png($tmp_name);
		response::set_content_type("image/png");
		response::set_content(file_get_contents($tmp_name));
		unlink_safe($tmp_name);
	}

	public static function rose_xml_output(xnode $parent_node, $competence_id_array, $rose_competences_node_name = "rose_competences")
	{
		$map = rose::get_map(sizeof($competence_id_array), 320);
		reset($map);
		$rose_competences_node = $parent_node->create_child_node($rose_competences_node_name);
		foreach ($competence_id_array as $competence_id)
		{
			$rose_competences_node->create_child_node("competence")
				->set_attr("id", $competence_id)
				->set_attr("map_area_coords", current($map));
			next($map);
		}
	}

}

?>
