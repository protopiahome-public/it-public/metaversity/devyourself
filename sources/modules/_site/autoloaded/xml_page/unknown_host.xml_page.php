<?php

class unknown_host_xml_page extends base_xml_ctrl
{
	
	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		return "<unknown_host></unknown_host>";
	}

}

?>