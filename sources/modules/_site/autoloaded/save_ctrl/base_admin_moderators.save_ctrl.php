<?php

abstract class base_admin_moderators_save_ctrl extends base_save_ctrl
{
	/* Settings */

	protected $do_member_check = false;

	/* Internal */
	protected $user_login;
	protected $user_id;
	protected $action;
	protected $was_member;
	protected $was_admin;

	/**
	 * @var base_access_save
	 */
	protected $access_save = null;
	protected $access_save_array = array();

	/**
	 * @return base_access_save
	 */
	abstract protected function get_access_save($user_id);

	/**
	 * @return array
	 */
	abstract protected function get_rights_array();

	public function start()
	{
		/* action */
		$this->action = POST("action");
		if (!in_array($this->action, array("add", "delete", "edit")))
		{
			return false;
		}

		return true;
	}

	public function check()
	{
		if ($this->action == "edit")
		{
			// no checks are required for the "edit" action
			return true;
		}

		/* "add" and "delete" action: user check */
		$this->user_login = POST("user_login");
		$this->user_id = POST("user_id");
		if (!is_null($this->user_login))
		{
			$user_login_escaped = $this->db->escape($this->user_login);
			$user_row = $this->db->get_row("SELECT id, login FROM user WHERE login = '{$user_login_escaped}' LOCK IN SHARE MODE");
			if (!$user_row)
			{
				$this->pass_info->write_error("USER_NOT_FOUND", $this->user_login);
				$this->pass_info->dump_vars();
				return false;
			}
			$this->user_id = (int) $user_row["id"];
			$this->user_login = $user_row["login"];
		}
		elseif (is_good_id($this->user_id))
		{
			$this->user_login = $this->db->get_value("SELECT login FROM user WHERE id = {$this->user_id} LOCK IN SHARE MODE");
			if (!$this->user_login)
			{
				$this->pass_info->write_error("USER_NOT_FOUND");
				return false;
			}
		}
		else
		{
			$this->set_error_404();
			return false;
		}

		/* access */
		$this->access_save = $this->get_access_save($this->user_id);

		/* other checks */
		if ($this->action == "add")
		{
			$this->was_member = $this->access_save->is_member();
			if ($this->access_save->is_moderator_strict())
			{
				$this->pass_info->write_error("ALREADY_MODERATOR", $this->user_login);
				$this->pass_info->dump_vars();
				return false;
			}
			if ($this->access_save->is_admin())
			{
				$this->pass_info->write_error("IS_ADMIN", $this->user_login);
				$this->pass_info->dump_vars();
				return false;
			}
			if ($this->do_member_check && !$this->was_member && !$this->user->is_admin())
			{
				$this->pass_info->write_error("NOT_MEMBER", $this->user_login);
				$this->pass_info->dump_vars();
				return false;
			}
		}
		elseif ($this->action == "delete")
		{
			if (!$this->access_save->is_moderator_strict())
			{
				$this->pass_info->write_error("ALREADY_NOT_MODERATOR", $this->user_login);
				return false;
			}
		}

		return true;
	}

	public function commit()
	{
		if ($this->action == "add")
		{
			if ($this->access_save->add_moderator())
			{
				$this->pass_info->write_info("MODERATOR_ADDED", $this->user_login);
				if (!$this->was_member && $this->do_member_check)
				{
					$this->pass_info->write_info("WAS_NOT_MEMBER");
				}
			}
			else
			{
				$this->pass_info->write_error("UNKNOWN_ERROR", $this->user_login);
			}
		}
		elseif ($this->action == "delete")
		{
			if ($this->access_save->delete_moderator())
			{
				$this->pass_info->write_info("MODERATOR_DELETED", $this->user_login);
			}
			else
			{
				$this->pass_info->write_error("UNKNOWN_ERROR", $this->user_login);
			}
		}
		elseif ($this->action == "edit")
		{
			$user_rights = POST_AS_ARRAY("rights");
			foreach ($user_rights as $user_id => $rights)
			{
				if (is_good_id($user_id))
				{
					$user_access_save = $this->get_access_save($user_id);
					$this->access_save_array[] = $user_access_save;
					if ($user_access_save->is_moderator_strict())
					{
						$rights_write_array = array();

						$rights_array = $this->get_rights_array();
						foreach ($rights_array as $right)
						{
							$rights_write_array[$right] = intval(isset($rights[$right]));
						}

						$user_access_save->set_moderator_rights($rights_write_array);
					}
				}
			}
			$this->pass_info->write_info("RIGHTS_CHANGED");
		}
		return true;
	}

	public function clean_cache()
	{
		if (!is_null($this->access_save))
		{
			$this->access_save->clean_cache();
		}
		foreach ($this->access_save_array as $access_save)
		{
			/* @var $access_save base_access_save */
			$access_save->clean_cache();
		}
	}

}

?>