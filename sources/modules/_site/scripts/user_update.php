<?php

require_once dirname(__FILE__) . "/../../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";
require_once PATH_CORE . "/api_client_interaction.php";

class user_update
{
	const TIME_LIMIT_ON_PASS = 180;
	const MAX_SEND_USER_COUNT = 100;
	private $log = array();

	/**
	 * @var db
	 */
	private $db;

	public function __construct()
	{
		global $db;
		$this->db = $db;
	}

	public function run()
	{
		// hardcoded max users in pack
		$projects = $this->db->fetch_all("
			SELECT id, endpoint_url, endpoint_resend 
			FROM project 
			WHERE endpoint_is_on = 1 AND endpoint_connect_attempts_left > 0
		");
		foreach ($projects as $project)
		{
			if ($project["endpoint_resend"])
			{
				$this->log[] = "project {$project["id"]} requested user data resend";
				$this->db->sql("
					REPLACE INTO api_user_update 
					SELECT project_id, user_id 
					FROM user_project_link 
					WHERE project_id = {$project["id"]}
				");
				$this->db->sql("
					UPDATE project 
					SET endpoint_resend = 0 
					WHERE id = {$project["id"]}
				");
			}

			while (1)
			{
				set_time_limit(self::TIME_LIMIT_ON_PASS);
				$user_id_array = $this->db->fetch_column_values("
					SELECT user_id 
					FROM api_user_update AS api 
					WHERE api.project_id = {$project["id"]} 
					LIMIT " . self::MAX_SEND_USER_COUNT . "
				");

				if (!$user_id_array)
				{
					break;
				}

				$req = new api_client_interaction(new update_user_api_message($user_id_array, $project["id"]));
				$time_start = microtime(true);

				if (!$result = $req->call($project["endpoint_url"], "update_user") or $result != "OK")
				{
					// any sort of fail, i.e server down or timeout or not all data received (they MUST be received)
					$this->log[] = "project {$project["id"]} get '{$req->get_called_url()}' failed, got '{$result}', curl_errno: {$req->get_curl_errno()}, http_code: {$req->get_http_code()}";

					$connect_result = ((false === $result) ? "FAIL" : "NOT_OK") . ":{$req->get_curl_errno()}";
					if ($http_code = $req->get_http_code())
					{
						$connect_result .= ":{$http_code}";
					}
					$connect_result_escaped = $this->db->escape($connect_result);

					$this->db->sql("
						UPDATE project 
						SET
							endpoint_connect_attempts_left = endpoint_connect_attempts_left - 1,
							endpoint_last_connect_time = NOW(),
							endpoint_last_connect_result = '{$connect_result_escaped}'
						WHERE id = {$project["id"]}
					");
					break;
				}

				// endoint url called successfully
				$user_id_list = implode(",", $user_id_array);
				$this->db->sql("DELETE FROM api_user_update WHERE project_id = ${project["id"]} AND user_id IN ({$user_id_list})");
				$time_lapse = microtime(true) - $time_start;
				$this->log[] = "project {$project["id"]} sending block (" . count($user_id_array) . " users) took {$time_lapse} sec key={$req->get_key()}";

				$this->db->sql("
					UPDATE project 
					SET
						endpoint_last_connect_time = NOW(),
						endpoint_last_connect_result = 'OK'
					WHERE id = {$project["id"]}
				");
			}
		}
	}

	public function get_log()
	{
		return $this->log;
	}

}

$uu = new user_update();
$uu->run();
echo "ok";
$log = $uu->get_log();
if (count($log))
{
	$log = join("\r\n\t", $log);
	file_put_contents(PATH_LOG . "/user_update.log", date("r") . "\r\n\t" . $log . "\r\n", FILE_APPEND);
}
?>