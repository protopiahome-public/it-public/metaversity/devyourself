#!/usr/bin/php
<?php
$global_start_time = microtime(true);

require_once dirname(__FILE__) . "/../../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

$emails = $db->fetch_all("SELECT * FROM email ORDER BY id LIMIT 32");

$k = 0;
$k_ok = 0;
foreach ($emails as $email)
{
	$headers = array();
	$headers[] = "Content-Type: text/html; charset=UTF-8";
	$headers[] = "From: {$config["from"]}";
	$headers = join(PHP_EOL, $headers);
	$html = str_replace("\r", "", $email["html"]);
	$html = str_replace("\n", PHP_EOL, $html);
	$subject = $email["subject"] == ":AUTO:" ? get_subject($html) : $email["subject"];
	$subject_quoted = $db->escape($subject);
	$success = mail($email["to_email"], $subject, $html, $headers) ? "1" : "0";
	$db->sql("
		INSERT INTO email_log
		SELECT NULL, to_email, to_user_id, '{$subject_quoted}', add_time, html, NOW(), {$success}
		FROM email
		WHERE id = {$email["id"]}
	");
	$db->sql("
		DELETE FROM email
		WHERE id = {$email["id"]}
	");
	$k++;
	$k_ok += $success ? 1 : 0;
}

$time = microtime(true) - $global_start_time;
$date = date("r");
$log_run = "{$date}\t{$time}\n";
file_put_contents(PATH_LOG . "/mail_run.log", $log_run, FILE_APPEND);
if ($k)
{
	$log_send = "{$date}\tProcessed:\t{$k}\tSent:\t{$k_ok}\tTime:\t{$time}\n";
	file_put_contents(PATH_LOG . "/mail_send.log", $log_send, FILE_APPEND);
}

function get_subject($body)
{
	$dom = new DOMDocument("1.0", "UTF-8");
	$dom->loadXML($body);
	$html_sxml = simplexml_import_dom($dom);
	return $html_sxml->head->title;
}

?>