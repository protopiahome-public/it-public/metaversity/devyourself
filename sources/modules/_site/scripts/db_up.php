#!/usr/bin/php
<?php
require_once dirname(__FILE__) . "/../../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

$TRIGGER_FILE_REGEXP = "/^([a-z0-9_]+)\.(after|before)\.(insert|update|delete)\.sql$/";
$ROUTINE_FILE_REGEXP = "/^([a-z0-9_]+)\.sql$/";
$SQL_FILE_PATH = PATH_TMP . "/trigger.sql";

$params = array(
	"help" => array(
		"names" => array("help", "-help", "--help", "-?", "/?"),
		"type" => "HELP",
	),
	"trigger_file" => array(
		"names" => array("ft", "-ft"),
		"type" => "VALUE",
		"default" => "",
	),
	"function_file" => array(
		"names" => array("ff", "-ff"),
		"type" => "VALUE",
		"default" => "",
	),
	"procedure_file" => array(
		"names" => array("fp", "-fp"),
		"type" => "VALUE",
		"default" => "",
	),
	"no_test_dbs" => array(
		"names" => array("-t", "t"),
		"type" => "EXISTS",
	),
);
$param0 = array_shift($_SERVER["argv"]);
array_shift($_SERVER["argv"]);
array_shift($_SERVER["argv"]);
array_unshift($_SERVER["argv"], $param0);
$conf = parse_params($params);

if ($conf["help"])
{
	$help = array();
	$help[] = "USAGE:";
	$help[] = "  php {$param0} 1|2 HOST [OPTIONS]";
	$help[] = "OPTIONS:";
	$help[] = "  -t - Do not change test DBs";
	$help[] = "  -ft=trigger_file.sql - Process only db/triggers/trigger_file.sql";
	$help[] = "  -ff=function_file.sql - Process only db/functions/function_file.sql";
	$help[] = "  -fp=procedure_file.sql - Process only db/procedures/procedure_file.sql";
	$help[] = "WARNING: Params -ft, -ff, -fp can NOT be used simultaneously!";
	die(join("\n", $help) . "\n");
}

$mysql_cmd = "mysql -h {$config["db_host"]}";
if ($config["db_user"])
{
	$mysql_cmd .= " -u {$config["db_user"]}";
}
if ($config["db_pass"])
{
	$mysql_cmd .= " -p{$config["db_pass"]}";
}

$test_db_suffices = $conf["no_test_dbs"] ? array() : array("", "_math");

go();
unlink_safe($SQL_FILE_PATH);

function go()
{
	global $conf;

	$c = 0;
	if ($conf["function_file"])
	{
		++$c;
	}
	if ($conf["procedure_file"])
	{
		++$c;
	}
	if ($conf["trigger_file"])
	{
		++$c;
	}
	if ($c == 0)
	{
		drop_all_old();
		process_routine("function");
		process_routine("procedure");
		process_triggers();
	}
	elseif ($c == 1)
	{
		if ($conf["function_file"])
		{
			process_routine("function");
		}
		if ($conf["procedure_file"])
		{
			process_routine("procedure");
		}
		if ($conf["trigger_file"])
		{
			process_triggers();
		}
	}
	else
	{
		die("Error: Params -ft, -ff, -fp can NOT be used simultaneously\n");
	}
}

function drop_all_old()
{
	global $config;
	global $test_db_suffices;

	drop_old($config["db_name"]);
	foreach ($test_db_suffices as $db_suffix)
	{
		drop_old($config["test_db_name"] . $db_suffix);
	}
}

function drop_old($db_name)
{
	global $db;
	echo "CLEAN `{$db_name}`\n";
	
	$db->sql("USE `{$db_name}`");
	$triggers = $db->fetch_all("
		SHOW TRIGGERS;
	");
	foreach ($triggers as $trigger)
	{
		$db->sql("DROP TRIGGER `{$db_name}`.`{$trigger['Trigger']}`");
		echo "  DROP TRIGGER `{$trigger['Trigger']}`\n";
	}

	$procedures = $db->fetch_all("
		SELECT * FROM `mysql`.`proc` where db = '{$db_name}' and type = 'PROCEDURE'
	");
	foreach ($procedures as $procedure)
	{
		$db->sql("DROP PROCEDURE `{$db_name}`.`{$procedure['name']}`");
		echo("  DROP PROCEDURE `{$procedure['name']}`\n");
	}
	$functions = $db->fetch_all("
		SELECT * FROM `mysql`.`proc` where db = '{$db_name}' and type = 'FUNCTION'
	");
	foreach ($functions as $function)
	{
		$db->sql("DROP FUNCTION `{$db_name}`.`{$function['name']}`");
		echo("  DROP FUNCTION `{$function['name']}`\n");
	}
}

function process_triggers()
{
	global $conf;
	global $TRIGGER_FILE_REGEXP;

	$dir = PATH_DB . "/triggers";
	if ($conf["trigger_file"])
	{
		if (substr($conf["trigger_file"], -4, 4) != ".sql")
		{
			$conf["trigger_file"] .= ".sql";
		}
		if (!preg_match($TRIGGER_FILE_REGEXP, $conf["trigger_file"]))
		{
			die("Error: Incorrect trigger file name: {$conf["trigger_file"]}\n");
		}
		$file_path = $dir . "/" . $conf["trigger_file"];
		if (!file_exists($file_path))
		{
			die("Error: File does not exist: {$file_path}\n");
		}
		$files = array($conf["trigger_file"]);
	}
	else
	{
		$files = scandir($dir);
	}

	foreach ($files as $file_name)
	{
		if (preg_match($TRIGGER_FILE_REGEXP, $file_name, $matches))
		{
			// preparing
			$table = $matches[1];
			$when = $matches[2];
			$action = $matches[3];
			$trigger_name = "{$table}_{$when}_{$action}";
			$code = file_get_contents($dir . "/" . $file_name);
			echo "[TRIGGER {$trigger_name}]\n";

			// sql
			$sql = "SET NAMES 'utf8';\n\n";
			$sql .= "DROP TRIGGER IF EXISTS `{$trigger_name}`;\n\n";
			$sql .= "DELIMITER ;;\n\n";
			$sql .= "CREATE TRIGGER `{$trigger_name}` " . strtoupper($when) . " " . strtoupper($action) . " ON `{$table}`\n";
			$sql .= "FOR EACH ROW\n";
			$sql .= $code . "\n";
			exec_sql($sql);
		}
	}
}

function process_routine($routine_type)
{
	global $conf;
	global $ROUTINE_FILE_REGEXP;

	$dir = PATH_DB . "/{$routine_type}s";
	if ($conf["{$routine_type}_file"])
	{
		if (substr($conf["{$routine_type}_file"], -4, 4) != ".sql")
		{
			$conf["{$routine_type}_file"] .= ".sql";
		}
		if (!preg_match($ROUTINE_FILE_REGEXP, $conf["{$routine_type}_file"]))
		{
			die("Error: Incorrect {$routine_type} file name: {$conf["{$routine_type}_file"]}\n");
		}
		$file_path = $dir . "/" . $conf["{$routine_type}_file"];
		if (!file_exists($file_path))
		{
			die("Error: File does not exist: {$file_path}\n");
		}
		$files = array($conf["{$routine_type}_file"]);
	}
	else
	{
		$files = scandir($dir);
	}

	foreach ($files as $file_name)
	{
		if (preg_match($ROUTINE_FILE_REGEXP, $file_name, $matches))
		{
			// preparing
			$routine_name = $matches[1];
			$code = file_get_contents($dir . "/" . $file_name);
			echo "[" . strtoupper($routine_type) . " {$routine_name}]\n";

			// sql
			$sql = "SET NAMES 'utf8';\n\n";
			$sql .= "DROP " . strtoupper($routine_type) . " IF EXISTS `{$routine_name}`;\n\n";
			$sql .= "DELIMITER ;;\n\n";
			$sql .= $code . "\n";
			exec_sql($sql);
		}
	}
}

function exec_sql($sql)
{
	global $config;
	global $test_db_suffices;
	global $SQL_FILE_PATH;

	file_put_contents($SQL_FILE_PATH, $sql);
	_exec_sql_helper($config["db_name"]);
	foreach ($test_db_suffices as $db_suffix)
	{
		_exec_sql_helper($config["test_db_name"] . $db_suffix);
	}
}

function _exec_sql_helper($db_name)
{
	global $mysql_cmd;
	global $SQL_FILE_PATH;

	echo "  USE {$db_name};\n";
	$cmd = "{$mysql_cmd} {$db_name} < {$SQL_FILE_PATH} 2>&1";
	exec($cmd, $output, $return_var);
	if (!empty($output))
	{
		echo join("\n", $output) . "\n";
	}
	if ($return_var != 0)
	{
		die("Error: RETURN_CODE={$return_var}\nSee {$SQL_FILE_PATH} for SQL.\n");
	}
}
?>