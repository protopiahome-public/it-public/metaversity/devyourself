<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
header("Content-Type: text/plain; charset=UTF-8");

if (!isset($_SERVER["argv"][1]))
{
	die("File is not specified\n");
}

$file_path = $_SERVER["argv"][1];
$file = file($file_path);
$table_name = "";

foreach ($file as $line_idx => $line)
{
	if (preg_match("/CREATE TABLE `([a-zA-Z0-9_]+)`/", $line, $m))
	{
		$table_name = $m[1];
	}
	$file[$line_idx] = preg_replace("/ KEY `([a-zA-Z0-9_]+)` \(`([a-zA-Z0-9_]+)`\)/", " KEY `KEY_{$table_name}_\\2` (`\\2`)", $file[$line_idx]);
	$file[$line_idx] = preg_replace("/ KEY `([a-zA-Z0-9_]+)_id` \(`([a-zA-Z0-9_]+)`\)/", " KEY `FK_{$table_name}_\\2` (`\\2`)", $file[$line_idx]);
	$file[$line_idx] = preg_replace("/ CONSTRAINT `([a-zA-Z0-9_]+)` FOREIGN KEY \(`([a-zA-Z0-9_]+)`\) REFERENCES /", " CONSTRAINT `FK_{$table_name}_\\2` FOREIGN KEY (`\\2`) REFERENCES ", $file[$line_idx]);
}

echo join("", $file);

?>