#!/usr/bin/php
<?php
require_once dirname(__FILE__) . "/../../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";
require_once PATH_DBCONVERT . "/dbconvert_1_clean.php";
require_once PATH_DBCONVERT . "/dbconvert_2_load.php";
require_once PATH_DBCONVERT . "/dbconvert_3_calc.php";
require_once PATH_DBCONVERT . "/dbconvert_4_vector.php";

$time_1 = microtime(true);
$cv = new dbconvert_1_clean($db);
$cv->run();

$time_2 = microtime(true);
$cv = new dbconvert_2_load($db, $config["db_cmp4_source"], $config["db_idsrv_source"]);
$cv->run();

$time_3 = microtime(true);
$cv = new dbconvert_3_calc($db);
$cv->run();

$time_4 = microtime(true);
$cv = new dbconvert_4_vector($db, $config["db_cmp4_source"], $config["db_idsrv_source"]);
$cv->run();

$time_5 = microtime(true);
$log = date("r");
$log .= "\tclean\t" . ($time_2 - $time_1);
$log .= "\tload\t" . ($time_3 - $time_2);
$log .= "\tcalc\t" . ($time_4 - $time_3);
$log .= "\tvector\t" . ($time_5 - $time_4);
file_put_contents(PATH_LOG . "/dbconvert.log", $log . "\n", FILE_APPEND);
?>