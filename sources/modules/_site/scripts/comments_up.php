#!/usr/bin/php
<?php
require_once dirname(__FILE__) . "/../../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

require_once(PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php");
$db->begin();

$db_res = $db->sql("
	SELECT object_id, MAX(id) as id
	FROM `comment`
	WHERE type_id = 3 AND is_deleted = 0
	GROUP BY object_id
");
while ($row = $db->fetch_array($db_res))
{
	$comment_id = $row["id"];
	$post_id = $row["object_id"];
	$comment_data = $db->get_row("
		SELECT * 
		FROM comment 
		WHERE id = {$comment_id} AND type_id = 3 AND is_deleted = 0
	");

	$html_short = text_processor::get_preview($comment_data["html"]);
	$html_short_quoted = $db->escape($html_short);

	$db->sql("
		UPDATE post
		SET
			last_comment_id = '{$comment_data["id"]}',
			last_comment_author_user_id = '{$comment_data["author_user_id"]}',
			last_comment_html = '{$html_short_quoted}'
		WHERE id = {$post_id}
	");
}

$db->commit();
?>