#!/usr/bin/php
<?php
require_once dirname(__FILE__) . "/../../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

$db->begin();

$fields_to_process = array(
	array(
		"table" => "competence_full",
		"fields" => array("title")
	),
	array(
		"table" => "competence_calc",
		"fields" => array("title")
	),
	array(
		"table" => "competence_group",
		"fields" => array("title")
	),
);

foreach ($fields_to_process as $data)
{
	$set_sql = array();
	foreach ($data["fields"] as $field)
	{
		$set_sql[] = "`{$field}` = TRIM(`{$field}`)";
	}
	$set_sql = join(", ", $set_sql);
	$db->sql("
		UPDATE {$data["table"]}
		SET {$set_sql}
	");
}

$db->commit();
?>