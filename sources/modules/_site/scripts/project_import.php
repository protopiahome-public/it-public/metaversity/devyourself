#!/usr/bin/php
<?php

// (!) Run after this script:
// db_up.php
// comments_up.php
// xss_up.php
// calc sql scripts

set_time_limit(1800);

$global_start_time = microtime(true);

require_once dirname(__FILE__) . "/../../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";
require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";
$conf = $_SERVER["argv"];
if (sizeof($conf) != 6)
{
	echo "USAGE:\n";
	echo "  {$conf[0]} DEV(1)|PROD(2) <ENV> <project_id> <tigra_db_name> <tigra_path>\n";
	echo "EXAMPLE:\n";
	echo "  {$conf[0]} 2 DEVYOURSELF 121 metagame2010_tigra /var/www/vhosts/metagame2010\n";
	die;
}
array_shift($conf);
array_shift($conf);
array_shift($conf);
$project_id = $conf[0];
if (!is_good_id($project_id))
{
	die("Wrong project id");
}
$source_project_db = $conf[1];
$source_project_path = $conf[2];
if (!file_exists($source_project_path . "/_sources/xml.ctrl/community_full.simple.xml.ctrl.php"))
{
	die("'{$source_project_path}' is wrong tigra path!");
}
$project_db = $db->get_db_name();

$project_data = $db->get_row("SELECT * FROM project WHERE id = {$project_id}");
if (!$project_data)
{
	die("'{$project_id}' is wrong project id");
}

echo "\rDROP TRIGGERS...               ";
$triggers = $db->fetch_all("
	SHOW TRIGGERS;
");
foreach ($triggers as $trigger)
{
	$db->sql("DROP TRIGGER `{$trigger['Trigger']}`");
}

echo "\rproject...                     ";
$db->sql("
	UPDATE {$project_db}.project, {$source_project_db}.project
	SET
		{$project_db}.project.bg_head_main_width = {$source_project_db}.project.bg_head_width,
		{$project_db}.project.bg_head_main_height = {$source_project_db}.project.bg_head_height,
		{$project_db}.project.bg_head_version = {$source_project_db}.project.bg_head_version,
		{$project_db}.project.bg_body_main_width = {$source_project_db}.project.bg_width,
		{$project_db}.project.bg_body_main_height = {$source_project_db}.project.bg_height,
		{$project_db}.project.bg_body_version = {$source_project_db}.project.bg_version
	WHERE {$project_db}.project.id = {$project_id} AND {$source_project_db}.project.id = 1
");

echo "\rusers...                       ";
/*$db->sql("
	DELETE FROM user_project_link
	WHERE project_id = {$project_id}
");*/

$db->sql("
	INSERT INTO {$project_db}.user_project_link (project_id, user_id, number, add_time, edit_time, status)
	(
		SELECT {$project_id}, id, id, NOW(), NOW(), IF(project_god.god_user_id > 0, 'admin', 'member')
		FROM {$source_project_db}.user
		LEFT JOIN {$source_project_db}.project_god ON (user.id = project_god.god_user_id)
	)
	ON DUPLICATE KEY UPDATE
		status = VALUES(status)
");

$db->sql("
	DELETE FROM project_admin
	WHERE project_id = {$project_id}
");

$db->sql("
	INSERT INTO {$project_db}.project_admin (project_id, user_id, add_time, edit_time, is_admin)
	(
		SELECT {$project_id}, god_user_id, NOW(), NOW(), 1
		FROM {$source_project_db}.project_god
	)
");

echo "\rclean comments...              ";
$db->sql("
	UPDATE {$project_db}.comment com
	LEFT JOIN {$project_db}.post p ON (p.id = com.object_id)
	LEFT JOIN {$project_db}.community c ON (c.id = p.community_id)
	SET com.parent_id = NULL
	WHERE (c.project_id = {$project_id} OR c.project_id IS NULL) AND com.type_id = 3
");
	
$db->sql("
	DELETE com
	FROM {$project_db}.comment com
	LEFT JOIN {$project_db}.post p ON (p.id = com.object_id)
	LEFT JOIN {$project_db}.community c ON (c.id = p.community_id)
	WHERE (c.project_id = {$project_id} OR c.project_id IS NULL) AND com.type_id = 3
");

echo "\rcommunities...                 ";
$db->sql("
	DELETE FROM community
	WHERE project_id = {$project_id}
");

$db->sql("
	INSERT INTO {$project_db}.community
	(
		old_id, name, title, descr_short, logo_big_width, logo_big_height, logo_small_width, logo_small_height,
		project_id, add_time, edit_time, allow_posting_without_section, is_deleted, join_premoderation,
		bg_head_main_width, bg_head_main_height,  bg_body_main_width, bg_body_main_height,
		old_parent_id, parent_approved, allow_child_communities, access_read, access_add_post, access_comment,
		logo_version, bg_head_version, bg_body_version
	)
	(
		SELECT
			id, url_name, title, descr_short, logo_big_width, logo_big_height, logo_small_width, logo_small_height,
			{$project_id}, add_time, add_time, allow_posting_without_section, is_deleted, join_premoderation,
			bg_head_width, bg_head_height, bg_width, bg_height,
			parent_id, parent_id_approved, IF(allow_subcommunities = 'YES', 'user', IF(allow_subcommunities = 'NO', 'nobody', IF(allow_subcommunities = 'ASK', 'user_premoderation', ''))), LOWER(right_read), LOWER(right_write), LOWER(right_comment),
			logo_big_version, bg_head_version, bg_version
		FROM {$source_project_db}.community
	)
");

$db->sql("
	UPDATE {$project_db}.community c1
	LEFT JOIN {$project_db}.community c2 ON (c2.old_id = c1.old_parent_id)
	SET c1.parent_id = c2.id
	WHERE c1.project_id = {$project_id}
");

echo "\rcommunity users...             ";
$db->sql("
	DELETE l
	FROM {$project_db}.community_user_link l
	LEFT JOIN {$project_db}.community c ON (l.community_id = c.id)
	WHERE c.project_id = {$project_id}
");

$db->sql("
	INSERT INTO {$project_db}.community_user_link (community_id, user_id, add_time, edit_time, status)
	(
		SELECT
		(
			SELECT c.id
			FROM {$project_db}.community c
			WHERE c.old_id = sl.community_id AND c.project_id = {$project_id}
		),
		member_user_id, join_time, join_time, IF(is_admin > 1, 'admin', 'member')
		FROM {$source_project_db}.community_member sl
	)
");

$db->sql("
	DELETE l
	FROM {$project_db}.community_admin l
	LEFT JOIN {$project_db}.community c ON (l.community_id = c.id)
	WHERE c.project_id = {$project_id}
");

$db->sql("
	INSERT INTO {$project_db}.community_admin (community_id, user_id, add_time, edit_time, is_admin)
	(
		SELECT
		(
			SELECT c.id
			FROM {$project_db}.community c
			WHERE c.old_id = sl.community_id AND c.project_id = {$project_id}
		),
		member_user_id, join_time, join_time, is_admin
		FROM {$source_project_db}.community_member sl
		WHERE is_admin = 1
	)
");

echo "\rsections...                    ";
$db->sql("
	DELETE s
	FROM {$project_db}.section s
	LEFT JOIN {$project_db}.community c ON (s.community_id = c.id)
	WHERE c.project_id = {$project_id}
");

$db->sql("
	INSERT INTO {$project_db}.section (old_id, name, title, community_id, descr, add_time, edit_time, position, access_add_post, access_comment, access_default)
	(
		SELECT id, url_name, title, (
			SELECT c.id
			FROM {$project_db}.community c
			WHERE c.old_id = {$source_project_db}.section.community_id AND c.project_id = {$project_id}
		), descr, add_time, add_time, sort,
		LOWER(IF(right_write = 'INHERITED', 'USER', right_write)), LOWER(IF(right_comment = 'INHERITED', 'USER', right_comment)),
		IF(right_write = 'INHERITED', 1, 0)
		FROM {$source_project_db}.section
		WHERE is_deleted = 0 AND is_default = 0
	)
");

echo "\rmenus...                       ";
$db->sql("
	DELETE
	FROM communities_menu_item
	WHERE project_id = {$project_id}
");

$db->sql("
	INSERT INTO communities_menu_item (community_id, project_id, position, add_time, edit_time)
	(
		SELECT (
			SELECT c.id
			FROM {$project_db}.community c
			WHERE c.old_id = {$source_project_db}.project_menu.community_id AND c.project_id = {$project_id}
		), {$project_id}, sort, NOW(), NOW()
		FROM {$source_project_db}.project_menu
	)
");

$db->sql("
	DELETE
	FROM communities_menu_dd_item
	WHERE project_id = {$project_id}
");

$db->sql("
	INSERT INTO communities_menu_dd_item (community_id, project_id, position, add_time, edit_time)
	(
		SELECT (
			SELECT c.id
			FROM {$project_db}.community c
			WHERE c.old_id = {$source_project_db}.project_menu.community_id AND c.project_id = {$project_id}
		), {$project_id}, sort, NOW(), NOW()
		FROM {$source_project_db}.project_menu
	)
");

echo "\rposts...                       ";
$db->sql("
	DELETE s
	FROM {$project_db}.block_set s
	LEFT JOIN {$project_db}.post p ON (p.block_set_id = s.id)
	LEFT JOIN {$project_db}.community c ON (c.id = p.community_id)
	WHERE c.project_id = {$project_id}
");
	
$db->sql("
	DELETE b FROM block_set b
	LEFT JOIN post p ON (p.block_set_id = b.id)
	LEFT JOIN material m ON (m.block_set_id = b.id)
	LEFT JOIN project_taxonomy_item i ON (p.block_set_id = i.id)
	WHERE p.id IS NULL AND m.id IS NULL and i.id IS NULL
");

$db->sql("
	DELETE p
	FROM {$project_db}.post p
	LEFT JOIN {$project_db}.community c ON (p.community_id = c.id)
	WHERE c.project_id = {$project_id}
");

$db->sql("
	INSERT INTO {$project_db}.post
	(old_id, title, author_user_id, add_time, edit_time, community_id, section_id, hide_in_project_feed, is_deleted)
	(
		SELECT  id, title, author_user_id, add_time, edit_time, (
			SELECT c.id
			FROM {$project_db}.community c
			WHERE c.old_id = {$source_project_db}.post.community_id AND c.project_id = {$project_id}
		), (
			SELECT s.id
			FROM {$project_db}.section s
			LEFT JOIN {$project_db}.community c ON c.id = s.community_id
			WHERE s.old_id = {$source_project_db}.post.section_id AND c.project_id = {$project_id}
		), IF(show_on_main = 1, 0, 1), is_deleted
		FROM {$source_project_db}.post
	)
");

$post_html_results = $db->fetch_all("
	SELECT
	p.id, '' as xml, p.add_time, p.edit_time
	FROM {$project_db}.post p
	LEFT JOIN {$source_project_db}.post sp ON (p.old_id = sp.id)
	LEFT JOIN community c ON (p.community_id = c.id)
	WHERE p.old_id > 0 AND c.project_id = {$project_id}
");

$i = 1;
foreach ($post_html_results as $post_html_result)
{
	echo "\rblock set: {$i} of " . sizeof($post_html_results) . "                 ";
	$post_html_result["xml"] = $db->escape($post_html_result["xml"]);
	$db->sql("
		INSERT INTO {$project_db}.block_set (xml, add_time, edit_time)
		VALUES ('{$post_html_result["xml"]}', '{$post_html_result["add_time"]}', '{$post_html_result["edit_time"]}')
	");
	$block_set_id = $db->get_last_id();
	$db->sql("
		UPDATE {$project_db}.post
		SET block_set_id = {$block_set_id}
		WHERE id = {$post_html_result["id"]}
	");
	++$i;
}

echo "\rinserting comments...          ";
$db->sql("
	INSERT INTO {$project_db}.comment
	(old_id, is_deleted, old_parent_id, author_user_id, html, position, level, type_id, object_id, add_time, edit_time)
	(
		SELECT id, is_deleted, parent_id, commenter_user_id, html, sort, level, 3,
		(
			SELECT p.id
			FROM {$project_db}.post p
			LEFT JOIN {$project_db}.community c ON c.id = p.community_id
			WHERE p.old_id = {$source_project_db}.comment.post_id AND c.project_id = {$project_id}
		),
		add_time, edit_time
		FROM {$source_project_db}.comment
	)
");

echo "\rupdating comments...            ";
$db->sql("
	UPDATE {$project_db}.comment c1
	LEFT JOIN {$project_db}.comment c2 ON (c2.old_id = c1.old_parent_id)
	LEFT JOIN {$project_db}.post p ON (p.id = c1.object_id)
	LEFT JOIN {$project_db}.community cty ON (cty.id = p.community_id)
	SET c1.parent_id = c2.id
	WHERE c1.old_id > 0 AND c1.old_parent_id > 0 AND c1.type_id = 3 AND cty.project_id = {$project_id}
");

echo "\rcustom feeds...                ";

$db->sql("
	INSERT INTO {$project_db}.community_custom_feed (old_id, name, community_id, title, descr, add_time, edit_time)
	(
		SELECT id, url_name, (
			SELECT id
			FROM {$project_db}.community
			WHERE old_id = {$source_project_db}.agl.community_id AND {$project_db}.community.project_id = {$project_id}
		),
		title, '', add_time, add_time
		FROM {$source_project_db}.agl
	)		
");

$db->sql("
	INSERT INTO {$project_db}.community_custom_feed_source (custom_feed_id, community_id, section_id)
	(
		SELECT f.id, c.id, null
		FROM {$source_project_db}.agl_community a
		LEFT JOIN {$project_db}.community_custom_feed f ON (f.old_id = a.agl_id)
		LEFT JOIN {$project_db}.community c ON (c.old_id = a.community_id)
		WHERE c.project_id = {$project_id}
	)
");

$db->sql("
	INSERT INTO {$project_db}.community_custom_feed_source (custom_feed_id, community_id, section_id)
	(
		SELECT f.id, c1.id, s.id
		FROM {$source_project_db}.agl_section a
		LEFT JOIN {$project_db}.community_custom_feed f ON (f.old_id = a.agl_id)
		LEFT JOIN {$project_db}.section s ON (s.old_id = a.section_id)
		LEFT JOIN {$project_db}.community c1 ON (c1.id = s.community_id)
		LEFT JOIN {$project_db}.community c2 ON (c2.id = f.community_id)
		WHERE c1.project_id = {$project_id} AND c2.project_id = {$project_id}
	)
");

echo "\rproject widgets...             ";
$db->sql("
	DELETE FROM {$project_db}.project_widget
	WHERE project_id = {$project_id}
");
$db->sql("
	INSERT INTO {$project_db}.project_widget (old_id, project_id, `column`, position, title, type_id, lister_item_count)
	(
		SELECT id, {$project_id}, `column`, sort, title,
		CASE type_id
			WHEN 'most_active_members' THEN 'top_users'
			ELSE type_id
		END,
		5
		FROM {$source_project_db}.project_widget
	)
");
$db->sql("
	INSERT INTO {$project_db}.project_widget_communities (widget_id, community_id, position)
	(
		SELECT w.id, c.id, sort
		FROM {$source_project_db}.project_widget_communities wc
		LEFT JOIN {$project_db}.project_widget w ON (w.old_id = wc.widget_id)
		LEFT JOIN {$project_db}.community c ON (c.old_id = wc.community_id)
		WHERE c.project_id = {$project_id} AND w.project_id = {$project_id}
	)
");
$db->sql("
	INSERT INTO {$project_db}.project_widget_text (widget_id, html)
	(
		SELECT w.id, html
		FROM {$source_project_db}.project_widget_text wt
		LEFT JOIN {$project_db}.project_widget w ON (w.old_id = wt.widget_id)
		WHERE w.project_id = {$project_id}
	)
");
		
echo "\rcommunity widgets...           ";

$db->sql("
	DELETE cw
	FROM {$project_db}.community_widget cw
	LEFT JOIN {$project_db}.community c ON (c.id = cw.community_id)
	WHERE c.project_id = {$project_id};
");
$db->sql("
	INSERT INTO {$project_db}.community_widget (old_id, community_id, `column`, position, title, type_id, lister_item_count)
	(
		SELECT cw.id, c.id, cw.`column`, cw.sort, cw.title,
		CASE type_id
			WHEN 'agl' THEN 'custom_feed'
			WHEN 'most_active_members' THEN 'top_users'
			WHEN 'new_members' THEN 'last_users'
			WHEN 'subcommunities' THEN 'child_communities'
			ELSE type_id
		END,
		5
		FROM {$source_project_db}.community_widget cw
		LEFT JOIN {$project_db}.community c ON (c.old_id = cw.community_id)
		WHERE type_id != 'last_posts_subcommunities' AND type_id != 'last_posts_tree' AND c.project_id = {$project_id}
	)
");
$db->sql("
	INSERT INTO {$project_db}.community_widget_custom_feed (widget_id, community_custom_feed_id)
	(
		SELECT w.id, f.id
		FROM {$source_project_db}.community_widget_agl wc
		LEFT JOIN {$project_db}.community_widget w ON (w.old_id = wc.widget_id)
		LEFT JOIN {$project_db}.community_custom_feed f ON (f.old_id = wc.agl_id)
		LEFT JOIN {$project_db}.community c1 ON c1.id = w.community_id
		LEFT JOIN {$project_db}.community c2 ON c2.id = f.community_id
		WHERE wc.agl_id > 0 AND c1.project_id = {$project_id} AND c2.project_id = {$project_id}
	)
");
$db->sql("
	INSERT INTO {$project_db}.community_widget_text (widget_id, html)
	(
		SELECT w.id, html
		FROM {$source_project_db}.community_widget_text wt
		LEFT JOIN {$project_db}.community_widget w ON (w.old_id = wt.widget_id)
		LEFT JOIN {$project_db}.community c ON c.id = w.community_id
		WHERE c.project_id = {$project_id}
	)
");

echo "\rfiles...                       ";
$communities_data = $db->fetch_all("
	SELECT *
	FROM {$project_db}.community
	WHERE project_id = {$project_id} AND old_id > 0
");
$i = 1;
foreach ($communities_data as $community_data)
{
	echo "\rfile {$i} of " . sizeof($communities_data) . "                     ";
	if (file_exists($source_project_path . "/httpdocs/data/community-bg/{$community_data["old_id"]}.jpeg"))
	{
		copy($source_project_path . "/httpdocs/data/community-bg/{$community_data["old_id"]}.jpeg", PATH_PUBLIC_DATA . "/community/bg_body/{$community_data["id"]}.jpeg");
	}
	else
	{
		//echo "\rwarning: community bg {$community_data["old_id"]} does not exist\n";
	}
	if (file_exists($source_project_path . "/httpdocs/data/community-bg-head/{$community_data["old_id"]}.jpeg"))
	{
		copy($source_project_path . "/httpdocs/data/community-bg-head/{$community_data["old_id"]}.jpeg", PATH_PUBLIC_DATA . "/community/bg_head/{$community_data["id"]}.jpeg");
	}
	else
	{
		//echo "\rwarning: community bg head {$community_data["old_id"]} does not exist\n";
	}
	if (file_exists($source_project_path . "/httpdocs/data/community-logo-big/{$community_data["old_id"]}.jpeg"))
	{
		copy($source_project_path . "/httpdocs/data/community-logo-big/{$community_data["old_id"]}.jpeg", PATH_PUBLIC_DATA . "/community/big/{$community_data["id"]}.jpeg");
	}
	else
	{
		//echo "\rwarning: community logo big {$community_data["old_id"]} does not exist\n";
	}
	if (file_exists($source_project_path . "/httpdocs/data/community-logo-small/{$community_data["old_id"]}.jpeg"))
	{
		copy($source_project_path . "/httpdocs/data/community-logo-small/{$community_data["old_id"]}.jpeg", PATH_PUBLIC_DATA . "/community/small/{$community_data["id"]}.jpeg");
	}
	else
	{
		//echo "\rwarning: community logo small {$community_data["old_id"]} does not exist\n";
	}
	++$i;
}

echo "\rupdating block sets...         ";
$allowed_extensions_types = array(
	"pdf" => array("pdf"),
	"psd" => array("psd"),
	"flash" => array("swf"),
	"ssheet" => array("xls", "xlsx", "ods"),
	"doc" => array("doc", "docx", "odt"),
	"richtxt" => array("rtf", "ps", "djvu"),
	"txt" => array("txt"),
	"img" => array("png", "jpg", "jpeg", "gif", "bmp", "ico", "tiff", "tif", "svg"),
	"slides" => array("ppt", "pptx", "odp"),
	"archive" => array("zip", "rar", "7z", "tar", "bzip", "bz", "bz2", "gzip", "gz")
);

$posts_data = $db->fetch_all("
	SELECT p.*
	FROM {$project_db}.post p
	LEFT JOIN {$project_db}.community c ON (c.id = p.community_id)
	WHERE c.project_id = {$project_id} AND p.old_id > 0
");
$i = 1;
foreach ($posts_data as $post_data)
{
	echo "\rupdating post {$i} of " . sizeof($posts_data) . "                  ";
	$source_post_data = $db->get_row("
		SELECT *
		FROM {$source_project_db}.post
		WHERE id = {$post_data["old_id"]}
	");

	$source_post_data["html"] = text_processor::tidy($source_post_data["html"]);
	$source_post_data["html_more"] = text_processor::tidy($source_post_data["html_more"]);
		
	$xml = "<block_set with_cut=\"x\" id=\"{$post_data["block_set_id"]}\">
		<block type=\"text\">
		<html>{$source_post_data["html"]}</html>
		</block>
	";

	if (strlen($source_post_data["html_more"]) > 0)
	{
		$xml .= "<block type=\"cut\" title=\"Читать далее...\"/>";
		$xml .= "<block type=\"text\">
		<html>{$source_post_data["html_more"]}</html>
		</block>
	";
	}

	if ($source_post_data["attach_file_size"] > 0)
	{
		$full_file_name = $source_post_data["attach_file_name"];
		$file_name_array = explode(".", $full_file_name);
		if (count($file_name_array) == 1)
		{
			$file_name = $full_file_name;
			$file_ext = ".";
		}
		else
		{
			$file_name = implode(".", $file_name_array);
			$file_ext = "." . array_pop($file_name_array);
		}

		$file_type = "";
		foreach ($allowed_extensions_types as $type => $extensions)
		{
			if (in_array(substr($file_ext, 1), $extensions))
			{
				$file_type = $type;
			}
		}
		if (!$file_type)
		{
			$file_ext = ".bin";
		}

		$full_file_name_escaped = $db->escape($full_file_name);
		$file_name_escaped = $db->escape($file_name);
		$file_ext_escaped = $db->escape($file_ext);
		$db->sql("
			INSERT INTO {$project_db}.block_file
			(block_set_id, user_id, title, ext, size, type, file_name, add_time, edit_time)
			VALUES (
				{$post_data["block_set_id"]}, {$post_data["author_user_id"]}, '{$file_name_escaped}', '{$file_ext_escaped}', 
				{$source_post_data["attach_file_size"]}, '{$file_type}', '{$full_file_name_escaped}',
				'{$post_data["add_time"]}', '{$post_data["edit_time"]}'
			);
		");
		$block_file_id = $db->get_last_id();

		$full_file_name_xml = htmlspecialchars($full_file_name);
		$file_name_xml = htmlspecialchars($file_name);
		$file_ext_xml = htmlspecialchars(substr($file_ext, 1));
		$file_size_xml_array = format_file_size($source_post_data["attach_file_size"], $as_array = true);
		$xml .= <<<EOF
			<block type="file" id="{$block_file_id}" title="{$file_name_xml}" file_name="{$full_file_name_xml}" ext="{$file_ext_xml}" file_type="{$file_type}" size="{$file_size_xml_array["size"]}" size_unit="{$file_size_xml_array["size_unit"]}"/>
EOF;

		$src_file_path = $source_project_path . "/_data/post_attach/{$source_post_data["id"]}";
		if (file_exists($src_file_path))
		{
			copy($src_file_path, PATH_PRIVATE_DATA . "/block_set/file/{$block_file_id}{$file_ext}");
		}
		else
		{
			echo "\rwarning: post attach {$source_post_data["id"]} does not exist\n";
		}
	}
	$xml .= "
		</block_set>
	";
	$xml_escaped = $db->escape($xml);
	$db->sql("
		UPDATE block_set
		SET xml = '{$xml_escaped}'
		WHERE id = {$post_data["block_set_id"]}
	");
	++$i;
}

echo "\rupdating links...         ";

$posts = $db->fetch_column_values("
	SELECT p.id, p.old_id
	FROM {$project_db}.post p
	LEFT JOIN {$project_db}.community c ON (c.id = p.community_id)
	WHERE c.project_id = {$project_id} AND p.old_id > 0
", "id", "old_id");

$communities = $db->fetch_column_values("
	SELECT c.id, c.old_id
	FROM {$project_db}.community c
	WHERE c.project_id = {$project_id} AND c.old_id > 0
", "id", "old_id");

$replaces_post_ids = 0;
$replaces_community_ids = 0;

function rep_posts($matches)
{
	global $posts, $replaces_post_ids;
	$old_id = $matches[2];
	if (isset($posts[$old_id]))
	{
		++$replaces_post_ids;
	}
	return $matches[1] . (isset($posts[$old_id]) ? $posts[$old_id] : $matches[2]) . $matches[3];
}

function rep_communities($matches)
{
	global $communities, $replaces_community_ids;
	$old_id = $matches[5];
	if (isset($communities[$old_id]))
	{
		++$replaces_community_ids;
		return $matches[1] . "/data/community/{$matches[3]}/{$communities[$old_id]}.jpeg";
	}
	else
	{
		return $matches[1] . $matches[2] . $matches[3] . $matches[4] . $matches[5] . $matches[6];
	}
}

function get_new_html($html) {
	$html = preg_replace('#(href\s*=\s*")(/(?!(test|tst|co|post|widget|all|create|members|users|admins|settings|search|admin|god|cp|dm9)/))#i', "\\1/co\\2", $html);
	$html = preg_replace_callback('#((?:href|src)\s*=\s*"/co/[^/\s]{1,60}/post-)(\d+)(/)#i', "rep_posts", $html);
	$html = preg_replace_callback('#((?:href|src)\s*=\s*")(/data/community-logo-)(big|small)(/)(\d+)(\.jpeg)#i', "rep_communities", $html);
	return $html;
}

$fields_to_process = array(
	array(
		"table" => "block_set",
		"id_field" => "id",
		"update_field" => "xml",
		"ids" => $db->fetch_column_values("
			SELECT p.block_set_id
			FROM {$project_db}.post p
			LEFT JOIN {$project_db}.community c ON (c.id = p.community_id)
			WHERE c.project_id = {$project_id} AND p.old_id > 0
		"),
	),
	array(
		"table" => "section",
		"id_field" => "id",
		"update_field" => "descr",
		"ids" => $db->fetch_column_values("
			SELECT s.id
			FROM {$project_db}.section s
			LEFT JOIN {$project_db}.community c ON (c.id = s.community_id)
			WHERE c.project_id = {$project_id}
		"),
	),
	array(
		"table" => "comment",
		"id_field" => "id",
		"update_field" => "html",
		"ids" => $db->fetch_column_values("
			SELECT cm.id
			FROM {$project_db}.comment cm
			LEFT JOIN {$project_db}.post p ON (p.id = cm.object_id)
			LEFT JOIN {$project_db}.community c ON (c.id = p.community_id)
			WHERE cm.type_id = 3 AND c.project_id = {$project_id}
		"),
	),
	array(
		"table" => "community_widget_text",
		"id_field" => "widget_id",
		"update_field" => "html",
		"ids" => $db->fetch_column_values("
			SELECT cw.id
			FROM {$project_db}.community_widget_text cwt
			LEFT JOIN {$project_db}.community_widget cw ON (cw.id = cwt.widget_id)
			LEFT JOIN {$project_db}.community c ON (c.id = cw.community_id)
			WHERE c.project_id = {$project_id}
		"),
	),
	array(
		"table" => "project_widget_text",
		"id_field" => "widget_id",
		"update_field" => "html",
		"ids" => $db->fetch_column_values("
			SELECT id
			FROM {$project_db}.project_widget pw
			WHERE pw.project_id = {$project_id}
		"),
	),
);

foreach ($fields_to_process as $data)
{
	if (!sizeof($data["ids"])) {
		return;
	}
	$records = $db->fetch_all("
		SELECT {$data["id_field"]} as id, {$data["update_field"]} as html
		FROM {$project_db}.{$data["table"]}
		WHERE {$data["id_field"]} IN (" . join(", ", $data["ids"]) . ")
	");
	$i = 1;
	$total = sizeof($records);
	foreach ($records as $row)
	{
		echo "\rupdating links ({$data["table"]}): {$i} of {$total}, ids replaced: {$replaces_post_ids}+{$replaces_community_ids}...         ";
		$html = get_new_html($row["html"]);
		$html_quoted = $db->escape($html);
		$db->sql("
			UPDATE {$project_db}.{$data["table"]}
			SET `{$data["update_field"]}` = '{$html_quoted}'
			WHERE {$data["id_field"]} = {$row["id"]}
		");
		++$i;
	}
}

echo "\r                                                                                ";

echo "\rdone                           \n";

?>