<?php

function check_files($dir, $types)
{
	if (strstr($dir, "tiny_mce"))
	{
		return;
	}
	$files = scandir($dir);

	foreach ($files as $file)
	{
		$path = $dir . "/" . $file;
		if ($file == "." || $file == "..")
		{
			continue;
		}

		$extension = end(explode('.', $file));
		if (is_dir($path))
		{
			check_files($path, $types);
		}
		elseif (in_array($extension, $types))
		{
			$content = file_get_contents($path);
			if (strstr($content, "\r\n") !== false)
			{
				$content = str_replace("\r\n", "\n", $content);
				//echo "<pre>$content</pre>";
				file_put_contents($path, $content);

				echo $path . "<br/>\n";
			}
		}
	}
}

$base_dir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
$types = array("php", "xml", "xslt", "js", "htaccess");
check_files($base_dir, $types);
echo "END.";
?>