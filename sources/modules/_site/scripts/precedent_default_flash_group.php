<?php

require_once dirname(__FILE__) . "/../../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

class precedent_default_flash_group_check
{
	const DEFAULT_FLASH_GROUP_TITLE = "Все участники";
	private $log = array();

	/**
	 * @var db
	 */
	private $db;

	public function __construct()
	{
		global $db;
		$this->db = $db;
	}

	public function run()
	{
		// hardcoded max users in pack
		$precedents = $this->db->fetch_all("
			SELECT id
			FROM precedent
		");
		
		foreach($precedents as $precedent)
		{
			$this->process_precedent($precedent);
		}
	}
	
	protected function process_precedent($precedent)
	{
		$flash_groups = $this->precedent_get_flash_groups($precedent['id']);
		if(!$flash_groups)
		{
			// no flash groups for precedent
			return;
		}
		
		$default_flash_group_id = $this->precedent_calc_default_flash_group($flash_groups, $all_users);
		
		if(!$default_flash_group_id)
		{
			// default falsh group not exists
			$default_flash_group_id = $this->precedent_create_default_flash_group($precedent['id'], $all_users);
			$this->log[] = "precdedent {$precedent['id']}: default flash group created, id={$default_flash_group_id}";
		}
	
		$this->db->sql("
				UPDATE precedent
				SET default_flash_group_id = {$default_flash_group_id}
				WHERE id = {$precedent['id']}
			");
		
	}
	
	protected function precedent_get_flash_groups($precedent_id)
	{
		// get flash groups for precedent
		$flash_groups = $this->db->fetch_all("
			SELECT id, title
			FROM precedent_flash_group
			WHERE precedent_id = {$precedent_id}
			ORDER BY id
		", "id");
		
		// populate flash groups with users
		foreach($flash_groups as $flash_group_id => &$flash_group)
		{
			$flash_group['users'] = $this->db->fetch_column_values("
				SELECT user_id, 1 AS found
				FROM precedent_flash_group_user_link
				WHERE precedent_flash_group_id = {$flash_group_id}
			", "found", "user_id");
		}
		
		return $flash_groups;
	}
	
	protected function precedent_calc_default_flash_group($flash_groups, &$all_users)
	{
		if(1 == count($flash_groups))
		{
			// only 1 flash group, it will be default, $all_users doesn't matter
			return reset(array_keys($flash_groups));
		}
		
		// collect all users
		$all_users = array();
		foreach($flash_groups as $flash_group)
		{
			$all_users += $flash_group['users'];
		}
		
		// find group with all users
		foreach($flash_groups as $flash_group_id => $flash_group)
		{
			if($flash_group['users'] == $all_users)
			{
				return $flash_group_id;
			}
		}
		
		// no group, but $all_users set
		return 0;
	}
	
	protected function precedent_create_default_flash_group($precedent_id, $all_users)
	{
		// create empty flash group
		$this->db->sql("
				INSERT INTO precedent_flash_group
				SET
					title = '". self::DEFAULT_FLASH_GROUP_TITLE ."',
					precedent_id = {$precedent_id},
					add_time = NOW()
			");
					
		$default_flash_group_id = $this->db->get_last_id();
		
		// populate flash group with $all_users
		foreach(array_keys($all_users) as $user_id)
		{
			$this->db->sql("
					INSERT INTO precedent_flash_group_user_link
					SET
						user_id = {$user_id},
						precedent_flash_group_id = {$default_flash_group_id}
				");
		}
		
		return $default_flash_group_id;
	}

	public function get_log()
	{
		return $this->log;
	}

}

$checker = new precedent_default_flash_group_check();
$checker->run();
echo "ok";
$log = $checker->get_log();
if (count($log))
{
	$log = join("\r\n", $log);
	file_put_contents(PATH_LOG . "/precedent_default_flash_group_check.log", date("r") . "\r\n" . $log . "\r\n", FILE_APPEND);
	echo $log;
}
?>