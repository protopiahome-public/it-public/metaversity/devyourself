<?php

class project_custom_feed_dt extends base_dt
{

	/**
	 *
	 * @var url_name_dtf
	 */
	protected $name_dtf;

	protected function init()
	{
		$this->add_block("main", "Основное");

		$dtf = new string_dtf("title", "Название");
		$dtf->set_max_length(255);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$this->name_dtf = $dtf = new url_name_dtf("name", "URL ленты");
		$dtf->set_max_length(40);
		$dtf->set_url_attribute("url");
		$dtf->set_unique_restriction("MUST BE SET BELOW");
		$dtf->set_deprecated_values(array(
			"admin", "god", "moderator", "editor", "user", "all", // roles
			"cp", "add", "create", "edit", "delete", "remove", // actions
		));
		$dtf->set_comment("Разрешены английские буквы, цифры и дефис. Рекомендуется не менять.");
		$this->add_field($dtf, "main");

		$dtf = new text_html_dtf("descr", "Описание");
		$dtf->set_editor_height(100);
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("add", array("title", "name", "descr"));
		$this->add_fields_in_axis("edit", array("title", "name", "descr"));
		$this->add_fields_in_axis("full", array("title", "name", "descr"));
	}

	public function set_project_obj(project_obj $project_obj, $use_project_url_placeholder = false)
	{
		$this->name_dtf->set_unique_restriction("project_id = " . $project_obj->get_id());
		
		$project_url = $use_project_url_placeholder ? "%PROJECT_URL%" : $project_obj->get_url();
		$this->name_dtf->set_url_prefix($project_url . "co/feeds/");
	}

}

?>