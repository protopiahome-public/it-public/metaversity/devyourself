<?php

class post_dt extends base_dt
{

	/**
	 * @var foreign_key_dtf
	 */
	protected $section_dtf;

	protected function init()
	{
		$dtf = new string_dtf("title", "Заголовок");
		$dtf->set_max_length(255);
		$dtf->set_importance(true);
		$this->add_field($dtf);

		$this->section_dtf = $dtf = new foreign_key_dtf("section_id", "Раздел", "section");
		$this->add_field($dtf);

		$dtf = new bool_dtf("hide_in_project_feed", "Приватная запись (видна только внутри сообщества)");
		$this->add_field($dtf);

		$dtf = new int_dtf("comment_count_calc", "Количество комментариев");
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$this->add_fields_in_axis("add", array(
			"title", "section_id", "hide_in_project_feed",
		));

		$this->add_fields_in_axis("edit", array(
			"title", "section_id", "hide_in_project_feed",
		));

		$this->add_fields_in_axis("full", array(
			"title", "section_id", "hide_in_project_feed", "comment_count_calc",
		));
	}

	public function set_community_obj(community_obj $community_obj)
	{
		$this->section_dtf->set_importance(!$community_obj->allow_posting_without_section());
		$community_id = $community_obj->get_id();
		$this->section_dtf->set_restrict_items_sql("
			SELECT id, title
			FROM section
			WHERE community_id = {$community_id}
		", "
			SELECT id, title
			FROM section
			WHERE community_id = {$community_id} AND id = %s
		");
	}

}

?>