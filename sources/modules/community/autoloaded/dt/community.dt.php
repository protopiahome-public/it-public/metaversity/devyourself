<?php

class community_dt extends base_dt
{

	/**
	 * @var url_name_dtf
	 */
	protected $name_dtf;

	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	public function __construct()
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", "Основное");
		$this->add_block("access_levels", "Доступ к сообществу");
		$this->add_block("access_posts", "Добавление записей");
		$this->add_block("access_child_communities", "Дочерние сообщества");

		$dtf = new string_dtf("title", "Название сообщества");
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$this->name_dtf = $dtf = new url_name_dtf("name", "URL сообщества");
		$dtf->set_max_length(40);
		$dtf->set_url_attribute("url");
		$dtf->set_unique_restriction("MUST BE SET BELOW");
		$dtf->set_deprecated_values(array(
			"www", "mail", "ns", "ns1", "ns2", "ns3", "ns4", // std. subdomains
			"admin", "god", "moderator", "editor", "user", "all", // roles
			"cp", "add", "create", "edit", "delete", "remove", // actions
			"widget", "members", "users", "admins", "moderators", "settings", "search", "company", "join", // std. URLs
			"co", "post", // URLs
		));
		$dtf->set_comment("Разрешены английские буквы, цифры и дефис. Рекомендуется не менять.");
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("descr_short", "Краткое описание сообщества (до 140 символов)");
		$dtf->set_max_length(140);
		$this->add_field($dtf, "main");

		$dtf = new image_pack_dtf("logo", "Логотип", PATH_TMP);
		$dtf->add_image("big", "Big", 100, 75, "/data/community/big/", "/img/defaults/logo-big-default.jpg", PATH_PUBLIC_DATA . "/community/big");
		$dtf->add_image("small", "Small", 50, 38, "/data/community/small/", "/img/defaults/logo-small-default.jpg", PATH_PUBLIC_DATA . "/community/small");
		$dtf->set_comment("Отображается в «шапке» сообщества. Изображение будет сжато до 100x75 пикселей.");
		$dtf->set_add_prefix_to_url($this->domain_logic->get_main_prefix());
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("join_premoderation", "Включить премодерацию на присоединение участников к сообществу");
		$dtf->set_comment("Если галочка стоит, участники будут попадать в сообщество только после вашего подтверждения");
		$this->add_field($dtf, "access_levels");

		$community_access_maper = new community_access_maper();

		$dtf = new access_level_dtf("access_read", "Доступ к сообществу на чтение", $community_access_maper);
		$dtf->add_level(ACCESS_LEVEL_GUEST, "Все");
		$dtf->add_level(ACCESS_LEVEL_USER, "Зарегистрированные пользователи");
		$dtf->add_level(ACCESS_LEVEL_PROJECT_MEMBER, "Участники проекта или сообщества");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники сообщества");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы сообщества");
		$this->add_field($dtf, "access_levels");

		$dtf = new access_level_dtf("access_comment", "Комментирование записей", $community_access_maper);
		$dtf->add_level(ACCESS_LEVEL_USER, "Все");
		$dtf->add_level(ACCESS_LEVEL_PROJECT_MEMBER, "Участники проекта или сообщества");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники сообщества");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы сообщества");
		$dtf->set_required_level_field_name("access_read");
		$this->add_field($dtf, "access_levels");

		$dtf = new access_level_dtf("access_add_post", "Добавление записей", $community_access_maper);
		$dtf->add_level(ACCESS_LEVEL_USER, "Все");
		$dtf->add_level(ACCESS_LEVEL_PROJECT_MEMBER, "Участники проекта или сообщества");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники сообщества");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы сообщества");
		$dtf->set_required_level_field_name("access_read");
		$this->add_field($dtf, "access_levels");

		$dtf = new bool_dtf("allow_posting_without_section", "Разрешить публиковать записи в корень сообщества");
		$dtf->set_default_value(true);
		$dtf->set_comment("Если галочка стоит, выбор раздела будет не обязателен. Если галочка снята, вы должны будете создать в сообществе хотя бы один раздел.");
		$this->add_field($dtf, "access_posts");

		$dtf = new select_dtf("allow_child_communities", "Создание дочерних сообществ");
		$dtf->set_cases(array(
			"nobody" => "Запретить",
			"user" => "Разрешить",
			"user_premoderation" => "Разрешить с премодерацией",
		));
		$dtf->set_drop_down_view(true);
		$this->add_field($dtf, "access_child_communities");

		$dtf = new select_dtf("color_scheme", "Цветовая схема сайта");
		$dtf->set_cases(array(
			"project" => "Как в проекте",
			"green" => "Зелёная",
			"blue" => "Голубая",
			"yellow" => "Оливковая",
			/* "red" => "Розовая", */
		));
		$dtf->set_default_key("project");
		$this->add_field($dtf);

		$dtf = new image_pack_dtf("bg_head", "Фон шапки", PATH_TMP);
		$dtf->add_image("main", "Main", 0, 0, "/data/community/bg_head/", null, PATH_PUBLIC_DATA . "/community/bg_head");
		$dtf->get_image_by_name("main")->add_processor(new image_pack_crop_processor(0, 87));
		$dtf->set_add_prefix_to_url($this->domain_logic->get_main_prefix());
		$dtf->set_comment("Фон для шапки сообщества. Будет обрезан по вертикали до высоты шапки (87 пикселя).");
		$this->add_field($dtf);

		$dtf = new image_pack_dtf("bg_body", "Фон", PATH_TMP);
		$dtf->add_image("main", "Main", 0, 0, "/data/community/bg_body/", null, PATH_PUBLIC_DATA . "/community/bg_body");
		$dtf->set_add_prefix_to_url($this->domain_logic->get_main_prefix());
		$dtf->set_comment("Фон сообщества. Постарайтесь заливать не очень большие (по размеру файлов) изображения.");
		$this->add_field($dtf);

		$dtf = new int_dtf("parent_id", "Родительское сообщество");
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$dtf = new bool_dtf("parent_approved", "Родитель одобрен");
		$this->add_field($dtf);

		//@xxx УДАЛИТЬ ВСЕ КАЛКИ ИЗ ВСЕХ DT
		$dtf = new int_dtf("child_community_count_calc", "Количество сообществ следующего уровня");
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$dtf = new int_dtf("custom_feed_count_calc", "Количество лент");
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$this->add_fields_in_axis("short", array(
			"title", "name", "descr_short", "logo",
			"allow_posting_without_section", "join_premoderation",
			"color_scheme", "bg_head", "bg_body",
			"allow_child_communities", "parent_id", "parent_approved", "custom_feed_count_calc",
			"child_community_count_calc",
		));

		$this->add_fields_in_axis("show", array(
			"title", "name", "descr_short", "logo",
			"allow_posting_without_section", "join_premoderation",
		));

		$this->add_fields_in_axis("add", array(
			"title", "name", "descr_short", "logo",
		));

		$this->add_fields_in_axis("edit_general", array(
			"title", "name", "descr_short", "logo",
		));

		$this->add_fields_in_axis("edit_access", array(
			"allow_posting_without_section", "join_premoderation", "allow_child_communities",
			"access_read", "access_comment", "access_add_post",
		));

		$this->add_fields_in_axis("edit_design", array(
			"color_scheme", "bg_head", "bg_body",
		));
	}

	public function set_project_obj(project_obj $project_obj, $use_project_url_placeholder = false)
	{
		$this->name_dtf->set_unique_restriction("project_id = " . $project_obj->get_id());

		$project_url = $use_project_url_placeholder ? "%PROJECT_URL%" : $project_obj->get_url();
		$this->name_dtf->set_url_prefix($project_url . "co/");
	}

}

?>