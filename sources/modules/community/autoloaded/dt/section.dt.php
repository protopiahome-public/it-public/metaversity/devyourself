<?php

class section_dt extends base_dt
{

	/**
	 * @var url_name_dtf
	 */
	protected $name_dtf;

	/**
	 * @var access_level_dtf
	 */
	protected $access_comment_dtf;

	/**
	 * @var access_level_dtf
	 */
	protected $access_add_post_dtf;

	protected function init()
	{
		$this->add_block("main", "Основное");
		$this->add_block("access", "Права");

		$dtf = new string_dtf("title", "Название");
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$this->name_dtf = $dtf = new url_name_dtf("name", "URL раздела");
		$dtf->set_max_length(40);
		$dtf->set_url_attribute("url");
		$dtf->set_deprecated_values(array(
			"www", "mail", "ns", "ns1", "ns2", "ns3", "ns4", // std. subdomains
			"admin", "god", "moderator", "editor", "user", "all", // roles
			"cp", "add", "create", "edit", "delete", "remove", // actions
			"widget", "members", "users", "admins", "moderators", "settings", "search", "company", "join", // std. URLs
			"co", "post", "feed", // URLs
		));
		$dtf->set_comment("Разрешены английские буквы, цифры и дефис. Рекомендуется не менять.");
		$this->add_field($dtf, "main");

		$dtf = new text_html_dtf("descr", "Описание");
		$dtf->set_editor_height(100);
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("access_default", "Права по умолчанию");
		$dtf->set_default_value(true);
		$this->add_field($dtf, "access");

		$community_access_maper = new community_access_maper();

		$this->access_comment_dtf = $dtf = new access_level_dtf("access_comment", "Комментирование записей", $community_access_maper);
		$dtf->add_level(ACCESS_LEVEL_USER, "Все");
		$dtf->add_level(ACCESS_LEVEL_PROJECT_MEMBER, "Участники проекта или сообщества");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники сообщества");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы сообщества");
		$dtf->add_dependency("access_default", "0", INTCMF_DTF_DEP_ACTION_ENABLE);
		$dtf->add_dependency("access_default", "1", INTCMF_DTF_DEP_ACTION_DO_NOT_STORE);
		$this->add_field($dtf, "access");

		$this->access_add_post_dtf = $dtf = new access_level_dtf("access_add_post", "Добавление записей", $community_access_maper);
		$dtf->add_level(ACCESS_LEVEL_USER, "Все");
		$dtf->add_level(ACCESS_LEVEL_PROJECT_MEMBER, "Участники проекта или сообщества");
		$dtf->add_level(ACCESS_LEVEL_MEMBER, "Только участники сообщества");
		$dtf->add_level(ACCESS_LEVEL_ADMIN, "Администраторы сообщества");
		$dtf->add_dependency("access_default", "0", INTCMF_DTF_DEP_ACTION_ENABLE);
		$dtf->add_dependency("access_default", "1", INTCMF_DTF_DEP_ACTION_DO_NOT_STORE);
		$this->add_field($dtf, "access");

		$this->add_fields_in_axis("add", array(
			"title", "name", "descr",
			"access_default", "access_comment", "access_add_post",
		));

		$this->add_fields_in_axis("edit", array(
			"title", "name", "descr",
			"access_default", "access_comment", "access_add_post",
		));

		$this->add_fields_in_axis("full", array(
			"title", "name", "descr",
		));
	}

	public function set_objects(project_obj $project_obj, community_obj $community_obj, $use_project_url_placeholder = false)
	{
		$this->name_dtf->set_unique_restriction("community_id = " . $community_obj->get_id());
		
		$project_url = $use_project_url_placeholder ? "%PROJECT_URL%" : $project_obj->get_url(); 
		$this->name_dtf->set_url_prefix($project_url . "co/{$community_obj->get_name()}/");
				
		$this->access_comment_dtf->set_min_level($community_obj->get_access()->get_required_level("read"));
		$this->access_comment_dtf->set_default_level($community_obj->get_access()->get_required_level("comment"));
		$this->access_add_post_dtf->set_min_level($community_obj->get_access()->get_required_level("read"));
		$this->access_add_post_dtf->set_default_level($community_obj->get_access()->get_required_level("add_post"));
	}

}

?>