<?php

class post_access_helper extends base_static_db_helper
{

	public static function can_add_post(community_obj $community_obj, section_obj $section_obj = null)
	{
		return self::access_can_add_post($community_obj, $section_obj);
	}

	public static function can_edit_post_without_section_change($author_id, community_obj $community_obj, section_obj $section_obj = null)
	{
		return self::can_change_post_without_section_change($author_id, $community_obj, $section_obj);
	}

	public static function can_edit_post_with_section_change($author_id, community_obj $community_obj, section_obj $old_section_obj = null, section_obj $new_section_obj = null)
	{
		return self::can_change_post_without_section_change($author_id, $community_obj, $old_section_obj) && self::access_can_add_post($community_obj, $new_section_obj);
	}

	public static function can_delete_post($author_id, community_obj $community_obj, section_obj $section_obj = null)
	{
		return self::can_change_post_without_section_change($author_id, $community_obj, $section_obj);
	}

	public static function can_comment_post(community_obj $community_obj, section_obj $section_obj = null)
	{
		return self::access_can_comment_post($community_obj, $section_obj);
	}

	public static function get_comment_status(community_obj $community_obj, section_obj $section_obj = null)
	{
		if ($section_obj)
		{
			return $section_obj->get_access()->get_required_status("comment");
		}
		else
		{
			return $community_obj->get_access()->get_required_status("comment");
		}
	}

	protected static function can_change_post_without_section_change($author_id, community_obj $community_obj, section_obj $section_obj = null)
	{
		global $user;
		/* @var $user user */

		if ($user->is_admin() || $community_obj->get_access()->has_admin_rights())
		{
			return true;
		}
		if ($user->get_user_id() != $author_id)
		{
			return false;
		}
		return self::access_can_add_post($community_obj, $section_obj);
	}

	protected static function access_can_add_post(community_obj $community_obj, section_obj $section_obj = null)
	{
		if ($section_obj)
		{
			return $section_obj->get_access()->can_add_post();
		}
		else
		{
			if (!$community_obj->allow_posting_without_section())
			{
				return false;
			}
			else
			{
				return $community_obj->get_access()->can_add_post();
			}
		}
	}

	protected static function access_can_comment_post(community_obj $community_obj, section_obj $section_obj = null)
	{
		if ($section_obj)
		{
			return $section_obj->get_access()->can_comment();
		}
		else
		{
			return $community_obj->get_access()->can_comment();
		}
	}

}

?>