<?php

require_once PATH_CORE . "/loader/xml_loader.php";

class subcommunities_helper extends base_static_db_helper
{

	public static function send_emails_to_pretender_admins(project_obj $project_obj, community_obj $community_obj, community_obj $pretender_community_obj, $is_now_child_community)
	{
		$admin_emails = self::db()->fetch_column_values("
			SELECT u.id, u.email
			FROM community_user_link l
			JOIN user u ON u.id = l.user_id
			WHERE
				l.community_id = {$pretender_community_obj->get_id()}
				AND l.status = 'admin'
		", "email", "id");

		global $user;
		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new project_short_xml_ctrl($project_obj->get_id()));
		$xml_loader->add_xml(new community_short_xml_ctrl($community_obj->get_id(), $project_obj->get_id()));
		$xml_loader->add_xml(new community_short_xml_ctrl($pretender_community_obj->get_id(), $project_obj->get_id()));
		$xml_loader->add_xml(new string_to_xml_xml_ctrl("<is_now_child_community>{$is_now_child_community}</is_now_child_community>"));
		$xml_loader->add_xml(new user_short_xml_ctrl($user->get_user_id()));
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xml(new sys_params_xml_ctrl());
		$xml_loader->add_xslt("mail/community_admin_child_community_pretender", "community_admin");
		$xml_loader->run();
		$body = $xml_loader->get_content();

		foreach ($admin_emails as $admin_user_id => $admin_email)
		{
			mail_helper::add_letter(":AUTO:", $body, $admin_email, $admin_user_id);
		}
	}

	public static function send_emails_to_admins_about_pretender(project_obj $project_obj, community_obj $community_obj, community_obj $pretender_community_obj)
	{
		$admin_emails = self::db()->fetch_column_values("
			SELECT u.id, u.email
			FROM community_user_link l
			JOIN user u ON u.id = l.user_id
			WHERE
				l.community_id = {$community_obj->get_id()}
				AND l.status = 'admin'
				AND u.allow_email_on_premoderation = 1
		", "email", "id");

		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new project_short_xml_ctrl($project_obj->get_id()));
		$xml_loader->add_xml(new community_short_xml_ctrl($community_obj->get_id(), $project_obj->get_id()));
		$xml_loader->add_xml(new community_short_xml_ctrl($pretender_community_obj->get_id(), $project_obj->get_id()));
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xml(new sys_params_xml_ctrl());
		$xml_loader->add_xslt("mail/community_admin_new_child_community_pretender", "community_admin");
		$xml_loader->run();
		$body = $xml_loader->get_content();

		foreach ($admin_emails as $admin_user_id => $admin_email)
		{
			mail_helper::add_letter(":AUTO:", $body, $admin_email, $admin_user_id);
		}
	}

	public static function get_descendants_ids($project_id, $community_id)
	{
		$cache = subcommunities_descendants_ids_cache::init($project_id, $community_id);
		$descendants_ids = $cache->get();
		if (!is_array($descendants_ids))
		{
			$descendants_ids = array();

			$children_ids_data = self::db()->fetch_all("
				SELECT id
				FROM community
				WHERE parent_id = {$community_id} AND parent_approved = 1
			", "id");
			foreach ($children_ids_data as $child_id => $child_row)
			{
				$descendants_ids[] = $child_id;
				$descendants_ids = array_merge($descendants_ids, self::get_descendants_ids($project_id, $child_id));
			}

			$cache = subcommunities_descendants_ids_cache::init($project_id, $community_id);
			$cache->set($descendants_ids);
		}

		return $descendants_ids;
	}

}

?>