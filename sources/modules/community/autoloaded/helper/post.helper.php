<?php

class post_helper extends base_static_db_helper
{

	public static function update_post_count($project_id, $community_id, $section_id)
	{
		self::db()->sql("
			UPDATE project
			SET post_count_calc = (
				SELECT COUNT(p.id)
				FROM post p
				LEFT JOIN community c ON p.community_id = c.id
				WHERE c.project_id = {$project_id} AND c.is_deleted = 0 AND p.is_deleted = 0
			)
			WHERE id = {$project_id}
		");

		self::db()->sql("
			UPDATE community
			SET post_count_calc = (
				SELECT COUNT(p.id)
				FROM post p 
				WHERE p.community_id = {$community_id} AND p.is_deleted = 0
			)
			WHERE id = {$community_id}
		");

		if ($section_id)
		{
			self::db()->sql("
				UPDATE section
				SET post_count_calc = (
					SELECT COUNT(p.id)
					FROM post p 
					WHERE p.section_id = {$section_id} AND p.is_deleted = 0
				)
				WHERE id = {$section_id}
			");
		}
	}

}

?>