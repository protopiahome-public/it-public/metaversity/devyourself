<?php

class community_helper extends base_static_db_helper
{

	public static function update_community_count($project_id)
	{
		self::db()->sql("
			UPDATE project
			SET community_count_calc = (
				SELECT COUNT(id)
				FROM community
				WHERE project_id = {$project_id} AND is_deleted = 0
			)
			WHERE id = {$project_id}
		");
	}

}

?>
