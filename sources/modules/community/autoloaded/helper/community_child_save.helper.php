<?php

class community_child_save_helper extends base_db_helper
{

	/**
	 * @var community_obj 
	 */
	protected $parent_community_obj;

	/**
	 * @var community_obj 
	 */
	protected $child_community_obj;

	public function __construct(community_obj $parent_community_obj, community_obj $child_community_obj)
	{
		$this->parent_community_obj = $parent_community_obj;
		$this->child_community_obj = $child_community_obj;
		parent::__construct();
	}

	public function add_pretender()
	{
		$this->db->sql("
			UPDATE community
			SET parent_id = " . $this->parent_community_obj->get_id() . ",
				parent_approved = 0
			WHERE id = " . $this->child_community_obj->get_id() . "
		");
	}

	public function add_child()
	{
		$this->db->sql("
			UPDATE community
			SET parent_id = " . $this->parent_community_obj->get_id() . ",
				parent_approved = 1
			WHERE id = " . $this->child_community_obj->get_id() . "
		");

		$this->update_child_community_calc(1);
	}

	public function delete_child()
	{
		$this->db->sql("
			UPDATE community
			SET parent_id = NULL,
				parent_approved = 0
			WHERE id = " . $this->child_community_obj->get_id() . "
		");

		if ($this->child_community_obj->get_parent_approved())
		{
			$this->update_child_community_calc(-1);
		}
	}

	protected function update_child_community_calc($diff)
	{
		$this->db->sql("
			UPDATE community
			SET child_community_count_calc = child_community_count_calc + {$diff}
			WHERE id = " . $this->parent_community_obj->get_id() . "
		");
	}

}

?>
