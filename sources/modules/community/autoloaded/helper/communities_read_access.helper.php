<?php

class communities_read_access_helper extends base_db_helper
{

	/**
	 * @var project_obj
	 */
	private $project_obj;

	/**
	 * @var user
	 */
	private $user;
	private $user_id;
	private $user_communities_cache = null;

	public function __construct(project_obj $project_obj)
	{
		/* @var $user user */
		global $user;

		parent::__construct();

		$this->project_obj = $project_obj;

		$this->user = $user;
		$this->user_id = $this->user->get_user_id();
	}

	private function get_all_communities_access_levels()
	{
		$cache = communities_read_all_communities_access_levels_cache::init($this->project_obj->get_id());
		$all_communities_access_levels = $cache->get();
		if (!is_array($all_communities_access_levels))
		{
			$community_access_maper = new community_access_maper();

			$all_communities_access_levels = $this->db->fetch_all("
				SELECT id, access_read as status
				FROM community
				WHERE is_deleted = 0 AND project_id = {$this->project_obj->get_id()}
				ORDER BY id
			", "id");
			foreach ($all_communities_access_levels as &$access_level)
			{
				$access_level = $community_access_maper->get_level_from_status($access_level["status"]);
			}

			$cache = communities_read_all_communities_access_levels_cache::init($this->project_obj->get_id());
			$cache->set($all_communities_access_levels);
		}
		return $all_communities_access_levels;
	}

	private function get_communities_user_access_levels()
	{
		$cache = communities_read_communities_user_access_levels_cache::init($this->project_obj->get_id(), $this->user_id);
		$user_communities_access_levels = $cache->get();
		if (!is_array($user_communities_access_levels))
		{
			$community_access_maper = new community_access_maper();

			$user_communities_access_levels = $this->db->fetch_all("
				SELECT l.status, c.id
				FROM community_user_link l
				LEFT JOIN community c ON l.community_id = c.id
				WHERE l.user_id = {$this->user_id} AND c.project_id = {$this->project_obj->get_id()}
				ORDER BY l.community_id
			", "id");
			foreach ($user_communities_access_levels as &$access_level)
			{
				$access_level = $community_access_maper->get_level_from_status($access_level["status"]);
			}

			$cache = communities_read_communities_user_access_levels_cache::init($this->project_obj->get_id(), $this->user_id);
			$cache->get($user_communities_access_levels);
		}

		return $user_communities_access_levels;
	}

	// @dm9 CR
	private function get_user_communities()
	{
		if ($this->user_communities_cache === null)
		{
			$all_communities_access_levels = $this->get_all_communities_access_levels();
			$user_communities_access_levels = $this->get_communities_user_access_levels();
			$result_communities_ids = $this->calc_user_communities($all_communities_access_levels, $user_communities_access_levels);
			$this->user_communities_cache = $result_communities_ids;
		}

		return $this->user_communities_cache;
	}

	private function calc_user_communities($all_communities_access_levels, $user_communities_access_levels)
	{
		$result_communities_ids = array();
		if ($this->user_id == 0)
		{
			foreach ($all_communities_access_levels as $community_id => $access_level)
			{
				if ($access_level <= ACCESS_LEVEL_GUEST)
				{
					$result_communities_ids[] = $community_id;
				}
			}
		}
		elseif ($this->user->is_admin() || $this->project_obj->get_access()->can_moderate_communities())
		{
			$result_communities_ids = array_keys($all_communities_access_levels);
		}
		else
		{
			reset($all_communities_access_levels);
			reset($user_communities_access_levels);
			while ($community_access_level = current($all_communities_access_levels))
			{
				$community_id = key($all_communities_access_levels);
				$user_community_id = key($user_communities_access_levels);
				// @dm9 CR
				$user_community_access_level = current($user_communities_access_levels);

				if ($community_access_level <= ACCESS_LEVEL_USER)
				{
					$result_communities_ids[] = $community_id;
					next($all_communities_access_levels);
					continue;
				}
				elseif ($community_access_level == ACCESS_LEVEL_PROJECT_MEMBER && $this->project_obj->get_access($this->user_id)->has_member_rights())
				{
					$result_communities_ids[] = $community_id;
					next($all_communities_access_levels);
					continue;
				}
				elseif ($user_community_id && $community_id == $user_community_id && $user_community_access_level >= $community_access_level)
				{
					$result_communities_ids[] = $community_id;
				}

				if ($community_id >= $user_community_id)
				{
					next($user_communities_access_levels);
				}
				if (!$user_community_id or $community_id <= $user_community_id)
				{
					next($all_communities_access_levels);
				}
			}
		}

		return $result_communities_ids;
	}

	public function _test_calc_user_communities($all_communities_access_levels, $user_communities_access_levels)
	{
		return $this->calc_user_communities($all_communities_access_levels, $user_communities_access_levels);
	}

	public function user_communities_modify_sql(select_sql $select_sql)
	{
		$user_communities_ids = $this->get_user_communities();
		$select_sql->add_where_in("community_id", $user_communities_ids);
	}

	private function get_user_communities_intersection($communities_ids)
	{
		$user_communities_ids = $this->get_user_communities();
		$result_communities_ids = $this->calc_user_communities_intersection($communities_ids, $user_communities_ids);
		return $result_communities_ids;
	}

	private function calc_user_communities_intersection($communities_ids, $user_communities_ids)
	{
		$result_communities_ids = array();
		reset($communities_ids);
		reset($user_communities_ids);
		while ($community_id = current($communities_ids) and $user_community_id = current($user_communities_ids))
		{
			if ($community_id == $user_community_id)
			{
				$result_communities_ids[] = $community_id;
			}
			if ($community_id >= $user_community_id)
			{
				next($user_communities_ids);
			}
			if ($community_id <= $user_community_id)
			{
				next($communities_ids);
			}
		}

		return $result_communities_ids;
	}

	public function _test_calc_user_communities_intersection($communities_ids, $user_communities_ids)
	{
		return $this->calc_user_communities_intersection($communities_ids, $user_communities_ids);
	}

	public function user_communities_intersection_modify_sql($communities_ids, select_sql $select_sql)
	{
		$user_communities_intersection_ids = $this->get_user_communities_intersection($communities_ids);
		$select_sql->add_where_in("community_id", $user_communities_intersection_ids);
	}

	private function get_user_sources_in_custom_feed($custom_feed_sources)
	{
		$user_communities_ids = $this->get_user_communities();
		$result_custom_feed_sources = $this->calc_user_sources_in_custom_feed($custom_feed_sources, $user_communities_ids);
		return $result_custom_feed_sources;
	}

	private function calc_user_sources_in_custom_feed($custom_feed_sources, $user_communities_ids)
	{
		$result_custom_feed_sources = array();

		reset($custom_feed_sources);
		reset($user_communities_ids);
		while ($community_id = key($custom_feed_sources) and $user_community_id = current($user_communities_ids))
		{
			$section_id = current($custom_feed_sources);
			if ($community_id == $user_community_id)
			{
				$result_custom_feed_sources[$community_id] = $section_id;
			}
			if ($community_id >= $user_community_id)
			{
				next($user_communities_ids);
			}
			if ($community_id <= $user_community_id)
			{
				next($custom_feed_sources);
			}
		}

		return $result_custom_feed_sources;
	}

	public function _test_calc_user_sources_in_custom_feed($custom_feed_sources, $user_communities_ids)
	{
		return $this->calc_user_sources_in_custom_feed($custom_feed_sources, $user_communities_ids);
	}

	public function user_communities_in_custom_feed_modify_sql($custom_feed_sources, select_sql $select_sql)
	{
		$user_sources_in_custom_feed = $this->get_user_sources_in_custom_feed($custom_feed_sources);
		$communities_ids = array();
		$sections_ids = array();
		foreach ($user_sources_in_custom_feed as $community_id => $section_id)
		{
			if ($section_id)
			{
				$sections_ids[] = $section_id;
			}
			else
			{
				$communities_ids[] = $community_id;
			}
		}

		$feed_where = array();
		if (count($communities_ids))
		{
			$feed_where[] = "community_id IN (" . implode(", ", $communities_ids) . ")";
		}
		if (count($sections_ids))
		{
			$feed_where[] = "section_id IN (" . implode(", ", $sections_ids) . ")";
		}
		if (count($feed_where))
		{
			$select_sql->add_where(implode(" OR ", $feed_where));
		}
		else
		{
			$select_sql->add_where("FALSE");
		}
	}

}

?>