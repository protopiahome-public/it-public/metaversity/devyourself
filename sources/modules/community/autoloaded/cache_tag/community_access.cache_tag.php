<?php

class community_access_cache_tag extends base_cache_tag
{

	public static function init($community_id, $user_id)
	{
		return parent::get_tag(__CLASS__, $community_id, $user_id);
	}

}

?>