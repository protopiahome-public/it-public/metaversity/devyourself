<?php

require_once PATH_INTCMF . "/tree.php";
require_once PATH_INTCMF . "/xml_builder.php";

class community_subcommunities_xml_page extends base_easy_xml_ctrl
{

	//Settings
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "community_short",
			"param2" => "project_id",
		),
	);
	protected $xml_row_name = "community";
	// Internal
	protected $project_id;
	protected $community_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($project_id, $community_id)
	{
		$this->project_id = $project_id;
		$this->community_id = $community_id;
		
		$this->project_obj = project_obj::instance($this->project_id);
		
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("date", "add_time DESC", "add_time ASC");
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("posts", "post_count_calc DESC", "post_count_calc ASC");
		$processor->add_order("comments", "comment_count_calc DESC", "comment_count_calc ASC");
		$this->add_easy_processor($processor);
	}

	public function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("id, IF(parent_approved = 1, parent_id, NULL) as parent_id, post_count_calc, comment_count_calc, member_count_calc");
		$select_sql->add_from("community");
		$select_sql->add_where("is_deleted = 0");
		$select_sql->add_where("project_id = {$this->project_id}");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	protected function modify_xml(xdom $xdom)
	{
		$tree = new tree($this->data);
		$tree->set_root_id($this->community_id);
		$tree->make_tree();
		$tree_node = $xdom->create_child_node("tree");
		xml_builder::tree($tree_node, $tree, "community");
	}

}

?>