<?php

class post_edit_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	protected $dt_name = "post";
	protected $axis_name = "edit";
	protected $dependencies_settings = array(
		array(
			"column" => "block_set_id",
			"ctrl" => "block_set_cut",
		),
		array(
			"column" => "section_id",
			"ctrl" => "section_access",
			"param2" => "community_id",
			"param3" => "project_id",
		),
	);

	/**
	 * @var post_dt
	 */
	protected $dt;
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $section_id = null;

	/**
	 * @var section_obj
	 */
	protected $section_obj = null;

	/**
	 * @var section_access
	 */
	protected $section_access = null;

	public function __construct($post_id, $community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);

		parent::__construct($post_id);
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_community_obj($this->community_obj);
		return true;
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("author_user_id, section_id");
		$select_sql->add_where("community_id = {$this->community_id} AND is_deleted = 0");
		$select_sql->add_select_fields("IF(block_set_id > 0, block_set_id, 0) as block_set_id");
	}

	protected function postprocess()
	{
		if (($this->section_id = $this->data[0]["section_id"]))
		{
			$this->section_obj = new section_obj($this->section_id, $this->community_obj);
			$this->section_access = $this->section_obj->get_access();
		}

		$rights_ok = post_access_helper::can_edit_post_without_section_change($this->data[0]["author_user_id"], $this->community_obj, $this->section_obj);

		if (!$rights_ok)
		{
			$this->set_error_403();
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		parent::fill_xml($xdom);
		$this->add_xslt("include/section_foreign_key.dtf", "community");
	}

}

?>