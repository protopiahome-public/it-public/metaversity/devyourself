<?php

class post_file_download_xml_page extends base_block_set_file_download_xml_ctrl
{

	// Internal
	protected $project_id;

	/**
	 *
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 *
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 *
	 * @var community_access
	 */
	protected $community_access;
	protected $post_id;

	public function __construct($block_file_id, $community_id, $project_id, $post_id = 0)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;
		$this->post_id = $post_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);
		$this->community_access = $this->community_obj->get_access();

		parent::__construct($block_file_id);
	}

	protected function check_file_rights()
	{
		if (!$this->community_access->can_read_community())
		{
			return false;
		}

		// @dm9 CR
		$post_data = $this->db->get_row("
			SELECT *
			FROM post
			WHERE id = {$this->post_id}
		");

		if (!$post_data)
		{
			return false;
		}
		if ($post_data["community_id"] != $this->community_id)
		{
			return false;
		}
		if ($post_data["block_set_id"] != $this->data["block_set_id"])
		{
			return false;
		}

		return true;
	}

}

?>