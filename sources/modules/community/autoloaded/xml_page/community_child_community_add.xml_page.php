<?php

class community_child_community_add_xml_page extends base_dt_add_xml_ctrl
{

	// Settings
	protected $dt_name = "community";
	protected $axis_name = "add";
	protected $enable_blocks = false;

	/**
	 * @var community_dt
	 */
	protected $dt;
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	public function __construct($community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);
		$this->community_access = $this->community_obj->get_access();

		parent::__construct();
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_project_obj($this->project_obj);
		return true;
	}

	public function check_rights()
	{
		// @rr
		return $this->project_obj->communities_are_on() && $this->community_access->can_read_community() && $this->community_obj->get_allow_child_communities() != "nobody";
	}

}

?>