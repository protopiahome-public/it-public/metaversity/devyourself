<?php

class post_show_xml_page extends base_xml_ctrl
{

	protected $post_id;
	protected $project_id;
	protected $community_id;

	public function __construct($post_id, $project_id, $community_id)
	{
		$this->post_id = $post_id;
		$this->project_id = $project_id;
		$this->community_id = $community_id;
	}

	public function init()
	{
		$this->xml_loader->add_xml(new post_full_xml_ctrl($this->post_id, $this->project_id, $load_block_set = true, $this->community_id));
		$this->xml_loader->add_xml(new comments_xml_ctrl("post", $this->post_id, $this->project_id));
	}

	public function get_xml()
	{
		return $this->get_node_string("post_show");
	}

}

?>