<?php

class community_feed_xml_page extends base_feed_xml_ctrl
{

	//Settings
	protected $xml_attrs = array("community_id", "project_id", "output_type", "section_id", "filter");
	protected $xml_row_name = "post";
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $page;
	protected $section_id;
	protected $filter = "all";

	public function __construct($community_id, $project_id, $page, $section_id = null, $filter = "all")
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;
		$this->page = $page;
		$this->section_id = $section_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);

		$this->filter = $filter;
		if (!$this->community_obj->get_child_community_count())
		{
			$this->filter = "own";
		}

		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_fast_easy_processor($this->page, 25));
	}

	public function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("id");
		$select_sql->add_from("post_calc");
		if ($this->filter == "all" || $this->filter == "own")
		{
			$select_sql->add_where_in("community_id", array($this->community_id));
		}
		if ($this->section_id)
		{
			$select_sql->add_where("section_id = {$this->section_id}");
		}
		elseif ($this->filter == "all" || $this->filter == "sub")
		{
			$communities_read_access_helper = $this->project_obj->get_communities_read_access_helper();
			$communities_ids = subcommunities_helper::get_descendants_ids($this->project_id, $this->community_id);
			$communities_read_access_helper->user_communities_intersection_modify_sql($communities_ids, $select_sql);
		}
		$select_sql->add_order("id DESC");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	public function process_data()
	{
		foreach ($this->data as $post_data)
		{
			$this->xml_loader->add_xml(new post_full_xml_ctrl($post_data["id"], $this->project_id, $this->output_type == FEED_TYPE_EXPANDED));
		}
	}

}

?>