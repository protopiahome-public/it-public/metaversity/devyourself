<?php

class community_users_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "admin";
	protected $dependencies_settings = array(
		array(
			"column" => "user_id",
			"ctrl" => "user_short",
			"param2" => "project_id",
		),
	);
	// Internal
	protected $community_id;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $page;

	public function __construct($community_id, $project_id, $page)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;
		$this->page = $page;

		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

		$processor = new sort_easy_processor();
		// @dm9 "join_time" - может быть нулём, протестировать
		$processor->add_order("join-time", "l.add_time DESC", "l.add_time ASC", true);
		$processor->add_order("post-count", "l.post_count_calc DESC", "l.post_count_calc ASC");
		$processor->add_order("comment-count", "l.comment_count_calc DESC", "l.comment_count_calc ASC");
		if ($this->project_obj->show_user_numbers())
		{
			$processor->add_order("number", "pl.number ASC", "pl.number DESC", false);
		}
		$this->add_easy_processor($processor);
		
		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new text_sql_filter("search", "имя/логин", array("u.login", "u.first_name", "u.last_name", "pl.game_name")));
		//$processor->add_sql_filter(new multi_link_sql_filter("group", "группа", "u.id", "user_user_group_link", "user_id", "user_group_id", "user_group"));
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("l.user_id, l.add_time, l.status");
		$select_sql->add_select_fields("l.post_count_calc, l.comment_count_calc");
		$select_sql->add_from("user u");
		$select_sql->add_join("JOIN community_user_link l ON l.user_id = u.id");
		$select_sql->add_join("LEFT JOIN user_project_link pl ON project_id = {$this->project_id} AND pl.user_id = l.user_id");
		$select_sql->add_where("l.community_id = {$this->community_id}");
		$select_sql->add_where("l.status <> 'pretender' AND l.status <> 'deleted'");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>