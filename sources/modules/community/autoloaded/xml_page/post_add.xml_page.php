<?php

class post_add_xml_page extends base_dt_add_xml_ctrl
{

	// Settings
	protected $dt_name = "post";
	protected $axis_name = "add";

	/**
	 * @var post_dt
	 */
	protected $dt;
	//Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	public function __construct($community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);

		parent::__construct();
	}

	public function start()
	{
		$this->xml_loader->add_xml(new block_set_cut_xml_ctrl(null));

		return true;
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_community_obj($this->community_obj);
		return true;
	}

	protected function fill_xml(xdom $xdom)
	{
		parent::fill_xml($xdom);
		$this->add_xslt("include/section_foreign_key.dtf", "community");
	}

}

?>