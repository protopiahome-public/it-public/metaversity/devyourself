<?php

class community_custom_feeds_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "feed";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "community_custom_feed_full",
			"param2" => "community_id",
			"param3" => "project_id",
		)
	);
	// Internal
	protected $project_id;
	protected $community_id;

	public function __construct($community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("community_custom_feed");
		$select_sql->add_select_fields("id");
		$select_sql->add_where("community_id = {$this->community_id}");
		$select_sql->add_order("title ASC");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>