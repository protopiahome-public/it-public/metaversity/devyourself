<?php

require_once PATH_CORE . "/loader/xml_loader.php";

class community_access_save extends base_access_save
{

	/**
	 * @var community_obj
	 */
	private $community_obj;

	/**
	 * @var project_obj
	 */
	private $project_obj;

	/**
	 * @var community_access_maper
	 */
	protected $access_maper;

	/**
	 * @var community_access_fetcher
	 */
	protected $access_fetcher;
	private $link_row;
	private $admin_row;
	private $clean_community_cache = false;

	public function __construct(community_obj $community_obj, project_obj $project_obj, $user_id)
	{
		$this->community_obj = $community_obj;
		$this->project_obj = $project_obj;
		parent::__construct($user_id);
	}

	protected function fill_data()
	{
		$this->access_maper = new community_access_maper();
		$this->access_fetcher = new community_access_fetcher($this->community_obj, $this->access_maper, $this->user_id, $lock = true);
		$this->link_row = $this->access_fetcher->get_link_row();
		$this->admin_row = $this->access_fetcher->get_admin_row();
		$this->level = $this->access_fetcher->get_level();
		return !$this->access_fetcher->error_occured();
	}
	
	public function join()
	{
		if ($this->community_obj->join_premoderation())
		{
			$this->add_pretender();
		}
		else
		{
			$this->add_member();
		}
	}

	public function unjoin()
	{
		$this->delete_member();
	}

	public function add_pretender()
	{
		if ($this->is_pretender_strict() or $this->is_member())
		{
			return true;
		}
		$new_status = ACCESS_STATUS_PRETENDER;
		$this->db->sql("
			INSERT INTO community_user_link (community_id, user_id, add_time, edit_time, status)
			VALUES ('{$this->community_obj->get_id()}', '{$this->user_id}', NOW(), NOW(), '{$new_status}')
			ON DUPLICATE KEY UPDATE
				status = '{$new_status}',
				edit_time = NOW()
		");
		$this->send_emails_to_admins_about_pretender();
		return true;
	}

	public function add_member()
	{
		if ($this->is_member())
		{
			return true;
		}
		$new_status = ACCESS_STATUS_MEMBER;
		$this->db->sql("
			INSERT INTO community_user_link (community_id, user_id, add_time, edit_time, status)
			VALUES ('{$this->community_obj->get_id()}', '{$this->user_id}', NOW(), NOW(), '{$new_status}')
			ON DUPLICATE KEY UPDATE
				status = '{$new_status}',
				edit_time = NOW()
		");
		$this->update_user_count();
		$this->send_email_to_pretender(true);
		return true;
	}

	public function add_moderator()
	{
		trigger_error("Community can't have moderators");
		return false;
	}

	public function add_admin()
	{
		if ($this->level > ACCESS_LEVEL_ADMIN)
		{
			return false;
		}
		if ($this->level == ACCESS_LEVEL_ADMIN)
		{
			return true;
		}
		$new_status = ACCESS_STATUS_ADMIN;
		$adder_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		$this->db->sql("
			INSERT INTO community_user_link (community_id, user_id, add_time, edit_time, status)
			VALUES ('{$this->community_obj->get_id()}', '{$this->user_id}', NOW(), NOW(), '{$new_status}')
			ON DUPLICATE KEY UPDATE
				status = '{$new_status}',
				edit_time = NOW()
		");
		$this->db->sql("
			INSERT INTO community_admin (community_id, user_id, add_time, adder_admin_user_id, edit_time, editor_admin_user_id, is_admin)
			VALUES ('{$this->community_obj->get_id()}', '{$this->user_id}', NOW(), '{$adder_user_id}', NOW(), '{$adder_user_id}', 1)
			ON DUPLICATE KEY UPDATE
				edit_time = NOW(),
				editor_admin_user_id = {$adder_user_id},
				is_admin = 1
		");
		$this->update_user_count();
		$this->send_email_to_pretender(true);
		return true;
	}

	public function delete_member()
	{
		$new_status = ACCESS_STATUS_DELETED;
		$this->db->sql("
			UPDATE community_user_link
			SET
				status = '{$new_status}',
				edit_time = NOW()
			WHERE
				community_id = '{$this->community_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$this->update_user_count();
		$this->send_email_to_pretender(false);
		return true;
	}

	public function delete_moderator()
	{
		trigger_error("Community can't have moderators");
		return false;
	}

	public function delete_admin()
	{
		if ($this->level < ACCESS_LEVEL_ADMIN)
		{
			return false;
		}
		$new_status = ACCESS_STATUS_MEMBER;
		$this->db->sql("
			UPDATE community_user_link
			SET
				status = '{$new_status}',
				edit_time = NOW()
			WHERE
				community_id = '{$this->community_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$editor_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		$this->db->sql("
			UPDATE community_admin
			SET
				is_admin = 0,
				edit_time = NOW(),
				editor_admin_user_id = {$editor_user_id}
			WHERE
				community_id = '{$this->community_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$this->update_user_count();
		return true;
	}

	public function set_moderator_rights($rights_array)
	{
		trigger_error("Community can't have moderators");
		return false;
	}

	private function update_user_count()
	{
		$this->db->sql("
			UPDATE community
			SET member_count_calc = (
				SELECT COUNT(*)
				FROM community_user_link l
				WHERE
					l.community_id = {$this->community_obj->get_id()}
					AND l.status <> 'pretender'
					AND l.status <> 'deleted'
			)
			WHERE id = {$this->community_obj->get_id()}
		");
		$this->clean_community_cache = true;
	}

	public function clean_cache()
	{
		community_access_cache_tag::init($this->community_obj->get_id(), $this->user_id)->update();
		if ($this->clean_community_cache)
		{
			community_cache_tag::init($this->community_obj->get_id())->update();
		}
	}

	private function send_emails_to_admins_about_pretender()
	{
		$admin_emails = $this->db->fetch_column_values("
			SELECT u.id, u.email
			FROM community_user_link l
			JOIN user u ON u.id = l.user_id
			WHERE
				l.community_id = {$this->community_obj->get_id()}
				AND l.status = 'admin'
				AND u.allow_email_on_premoderation = 1
		", "email", "id");

		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new user_short_xml_ctrl($this->user_id));
		$xml_loader->add_xml(new project_short_xml_ctrl($this->project_obj->get_id()));
		$xml_loader->add_xml(new community_short_xml_ctrl($this->community_obj->get_id(), $this->project_obj->get_id()));
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xslt("mail/community_admin_new_member", "community_admin");
		$xml_loader->run();
		$body = $xml_loader->get_content();

		foreach ($admin_emails as $admin_user_id => $admin_email)
		{
			mail_helper::add_letter(":AUTO:", $body, $admin_email, $admin_user_id);
		}
	}

	private function send_email_to_pretender($is_now_member)
	{
		if (!$this->is_pretender_strict() or $this->user_id == $this->user->get_user_id())
		{
			return;
		}

		$is_now_member = $is_now_member ? "1" : "0";

		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new user_short_xml_ctrl($this->user_id));
		$xml_loader->add_xml(new user_short_xml_ctrl($this->user->get_user_id()));
		$xml_loader->add_xml(new project_short_xml_ctrl($this->project_obj->get_id()));
		$xml_loader->add_xml(new community_short_xml_ctrl($this->community_obj->get_id(), $this->project_obj->get_id()));
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xml(new sys_params_xml_ctrl());
		$xml_loader->add_xml(new string_to_xml_xml_ctrl("<is_now_member>{$is_now_member}</is_now_member>"));
		$xml_loader->add_xslt("mail/community_pretender", "community");
		$xml_loader->run();
		$body = $xml_loader->get_content();

		$pretender_user = new user($this->user_id);
		mail_helper::add_letter(":AUTO:", $body, $pretender_user->get_email(), $this->user_id);
	}

}

?>