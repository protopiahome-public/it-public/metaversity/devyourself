<?php

class section_access extends base_access
{

	/**
	 * @var section_obj
	 */
	protected $section_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	/**
	 * @var project_access
	 */
	protected $project_access;

	/**
	 * @var section_access_maper
	 */
	protected $access_maper;

	public function __construct(section_obj $section_obj, community_access $community_access, $user_id = null)
	{
		$this->section_obj = $section_obj;
		$this->community_access = $community_access;
		$this->project_access = $community_access->get_project_access();
		parent::__construct($user_id);
	}

	protected function get_new_access_maper()
	{
		return new section_access_maper();
	}

	protected function fill_required_levels()
	{
		if ($this->section_obj->get_access_default())
		{
			$this->required_statuses["comment"] = $this->community_access->get_community_obj()->get_param("access_comment");
			$this->required_statuses["add_post"] = $this->community_access->get_community_obj()->get_param("access_add_post");
		}
		else
		{
			$this->required_statuses["comment"] = $this->section_obj->get_param("access_comment");
			$this->required_statuses["add_post"] = $this->section_obj->get_param("access_add_post");
		}
		// @dm9 move this code to base
		$this->required_levels = array();
		foreach ($this->required_statuses as $name => $status)
		{
			$this->required_levels[$name] = $this->access_maper->get_level_from_status($status);
		}
	}

	protected function fill_data()
	{
		$this->level = $this->community_access->get_level();
	}

	public function can_comment()
	{
		if ($this->section_obj->get_access_default())
		{
			return $this->community_access->can_comment();
		}
		$required_level = $this->required_levels["comment"];
		return $this->community_access->can_read_community() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_PROJECT_MEMBER && $this->project_access->has_member_rights());
	}

	public function can_add_post()
	{
		if ($this->section_obj->get_access_default())
		{
			return $this->community_access->can_add_post();
		}
		$required_level = $this->required_levels["add_post"];
		return $this->community_access->can_read_community() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_PROJECT_MEMBER && $this->project_access->has_member_rights());
	}

	public function has_member_rights()
	{
		trigger_error("section can't have members");
	}

	public function has_admin_rights()
	{
		trigger_error("section can't have admins");
	}

	public function has_moderator_rights()
	{
		trigger_error("section can't have moderators");
	}

	public function get_section_id()
	{
		return $this->section_obj->get_id();
	}

}

?>