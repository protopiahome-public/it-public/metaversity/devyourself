<?php

class community_access extends base_access
{

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	/**
	 * @var community_access_fetcher
	 */
	protected $access_fetcher;

	/**
	 * @var community_access_maper
	 */
	protected $access_maper;
	protected $link_row = false;
	protected $admin_row = false;

	public function __construct($community_obj, project_access $project_access, $user_id = null)
	{
		$this->community_obj = $community_obj;
		$this->project_access = $project_access;
		parent::__construct($user_id);
	}

	protected function get_new_access_maper()
	{
		return new community_access_maper();
	}

	protected function fill_required_levels()
	{
		$this->required_statuses["read"] = $this->community_obj->get_param("access_read");
		$this->required_statuses["comment"] = $this->community_obj->get_param("access_comment");
		$this->required_statuses["add_post"] = $this->community_obj->get_param("access_add_post");

		$this->required_levels = array();
		foreach ($this->required_statuses as $name => $status)
		{
			$this->required_levels[$name] = $this->access_maper->get_level_from_status($status);
		}
	}

	protected function fill_data()
	{
		if (!$this->user_id)
		{
			$this->level = ACCESS_LEVEL_GUEST;
			return;
		}
		$cache = community_access_cache::init($this->community_obj->get_id(), $this->user_id);
		$data = $cache->get();
		if ($data)
		{
			$this->level = $data["level"];
			$this->admin_row = $data["admin_row"];
			return;
		}
		$this->access_fetcher = new community_access_fetcher($this->community_obj, $this->access_maper, $this->user_id);
		$this->level = $this->access_fetcher->get_level();
		if (!$this->access_fetcher->error_occured())
		{
			$this->link_row = $this->access_fetcher->get_link_row();
			$this->admin_row = $this->access_fetcher->get_admin_row();
			$data = array(
				"level" => $this->level,
				"admin_row" => $this->admin_row,
			);
			$cache->set($data);
		}
	}

	public function join_premoderation()
	{
		return $this->community_obj->join_premoderation();
	}

	public function can_read_community()
	{
		$required_level = $this->required_levels["read"];
		return $this->project_access->can_read_communities() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_PROJECT_MEMBER && $this->project_access->has_member_rights());
	}

	public function can_comment()
	{
		$required_level = $this->required_levels["comment"];
		return $this->can_read_community() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_PROJECT_MEMBER && $this->project_access->has_member_rights());
	}

	public function can_add_post()
	{
		$required_level = $this->required_levels["add_post"];
		return $this->can_read_community() && ($this->user->is_admin() || $this->level >= $required_level || $required_level == ACCESS_LEVEL_PROJECT_MEMBER && $this->project_access->has_member_rights());
	}

	public function has_member_rights()
	{
		return $this->project_access->can_moderate_communities() || $this->can_read_community() && $this->level >= ACCESS_LEVEL_MEMBER;
	}

	public function has_admin_rights()
	{
		return $this->project_access->can_moderate_communities() || $this->can_read_community() && $this->level >= ACCESS_LEVEL_ADMIN;
	}

	public function has_moderator_rights()
	{
		trigger_error("Community can't have moderators");
	}

	public function get_community_id()
	{
		return $this->community_obj->get_id();
	}

	/**
	 *
	 * @return community_obj
	 */
	public function get_community_obj()
	{
		return $this->community_obj;
	}

	/**
	 * @return project_access
	 */
	public function get_project_access()
	{
		return $this->project_access;
	}

}

?>