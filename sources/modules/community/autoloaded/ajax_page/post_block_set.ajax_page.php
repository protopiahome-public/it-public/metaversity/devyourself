<?php

class post_block_set_ajax_page extends base_block_set_ajax_ctrl
{

	protected $mixins = array(
		"community_before_start"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;
	protected $post_id;

	public function on_before_start()
	{
		$this->post_id = POST("post_id");
		if (!is_good_num($this->post_id))
		{
			return false;
		}

		if ($this->post_id > 0 and !$this->db->row_exists("SELECT id FROM post WHERE id = {$this->post_id} AND community_id = {$this->community_id} AND is_deleted = 0"))
		{
			return false;
		}

		return true;
	}

	public function check_rights()
	{
		$this->community_access = $this->community_obj->get_access();
		return $this->community_access->can_read_community();
	}

	protected function get_object_url()
	{
		$community_name = $this->community_obj->get_name();
		
		$object_url = $this->project_obj->get_url() . "co/{$community_name}/";

		if ($this->post_id)
		{
			$object_url .= "post-{$this->post_id}/";
		}
		else
		{
			$object_url .= "add/";
		}

		return $object_url;
	}

}

?>