<?php

class post_comment_add_ajax_page extends base_comment_add_ajax_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_modify_sql",
		"project_stat_clean_cache",
		"community_stat_clean_cache"
	);
	protected $community_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	/**
	 * @var section_obj
	 */
	protected $section_obj = null;

	/**
	 * @var section_access
	 */
	protected $section_access = null;

	protected function get_type()
	{
		return "post";
	}

	public function check_rights()
	{
		if ($this->object_row["section_id"])
		{
			$this->section_obj = new section_obj($this->object_row["section_id"], $this->community_obj);
		}
		return post_access_helper::can_comment_post($this->community_obj, $this->section_obj);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("author_user_id, section_id");
		$select_sql->add_where("is_deleted = 0");
	}

	protected function update_object()
	{
		$user_id = $this->user->get_user_id();
		$comment_html_escaped = $this->db->escape(text_processor::get_preview($this->comment_html));
		$this->db->sql("CALL post_comment_add_count_calc({$this->object_id}, {$this->community_id}, {$this->project_id}, {$user_id})");
		$this->db->sql("
			UPDATE post SET 
				last_comment_id = {$this->new_comment_id}, 
				last_comment_author_user_id = {$user_id},
				last_comment_html = '{$comment_html_escaped}'
			WHERE id = {$this->object_id}
		");
	}

	protected function filter_subscribers($subscribers_ids)
	{
		$community_read_access_helper = new project_read_access_helper($this->project_obj);
		$allowed_subscribers_ids = $community_read_access_helper->get_community_users_access_read($subscribers_ids, $this->community_obj);
		return $allowed_subscribers_ids;
	}

	protected function add_object_xml_ctrls(xml_loader $xml_loader)
	{
		$xml_loader->add_xml(new post_full_xml_ctrl($this->object_id, $this->project_id));
		$xml_loader->add_xml(new project_short_xml_ctrl($this->project_id));
	}

	protected function add_mail_xslt(xml_loader $xml_loader)
	{
		$xml_loader->add_xslt("mail/new_comment_{$this->type}", "community");
	}

	public function clean_cache()
	{
		post_cache_tag::init($this->object_id)->update();
	}

}

?>