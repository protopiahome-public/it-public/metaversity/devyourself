<?php

class post_comments_subscribe_ajax_page extends base_comments_subscribe_ajax_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_modify_sql"
	);
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	protected function get_type()
	{
		return "post";
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("is_deleted = 0");
	}

	public function check_rights()
	{
		$this->community_access = $this->community_obj->get_access();
		return $this->community_access->can_read_community();
	}

}

?>
