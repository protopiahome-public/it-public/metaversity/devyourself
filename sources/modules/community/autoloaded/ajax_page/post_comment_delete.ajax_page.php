<?php

class post_comment_delete_ajax_page extends base_comment_delete_ajax_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_admin_check_rights",
		"community_modify_sql",
		"project_stat_clean_cache",
		"community_stat_clean_cache"
	);

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	protected function get_type()
	{
		return "post";
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("is_deleted = 0");
	}

	protected function update_object()
	{
		$user_id = $this->comment_data["author_user_id"];
		$this->db->sql("CALL post_comment_" . ($this->is_deleted ? "delete" : "add") . "_count_calc({$this->object_id}, {$this->community_id}, {$this->project_id}, {$user_id})");

		$last_comment = $this->db->get_row("
			SELECT * 
			FROM comment 
			WHERE type_id = {$this->type_id} AND object_id = {$this->object_id} AND is_deleted = 0
			ORDER BY id DESC
			LIMIT 1
		");
		if (!$last_comment)
		{
			$last_comment = array("id" => "NULL", "author_user_id" => "NULL", "html" => "");
		}
		
		$last_comment_html_escaped = $this->db->escape(text_processor::get_preview($last_comment['html']));
		$this->db->sql("
			UPDATE post SET 
				last_comment_id = {$last_comment["id"]},
				last_comment_author_user_id = {$last_comment["author_user_id"]},
				last_comment_html = '{$last_comment_html_escaped}'
			WHERE id = {$this->object_id}
		");
	}

	public function clean_cache()
	{
		post_cache_tag::init($this->object_id)->update();
	}

}

?>
