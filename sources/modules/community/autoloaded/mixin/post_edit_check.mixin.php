<?php

class post_edit_check_mixin extends base_mixin
{

	/**
	 * @var pass_info
	 */
	protected $pass_info;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var section_obj
	 */
	protected $section_obj;

	/**
	 * @var section_obj
	 */
	protected $old_section_obj;
	protected $old_db_row;

	public function check()
	{
		$result = post_access_helper::can_edit_post_with_section_change($this->old_db_row["author_user_id"], $this->community_obj, $this->old_section_obj, $this->section_obj);
		if (!$result)
		{
			$this->pass_info->write_error("SECTION_ACCESS_DISABLED");
		}
		return $result;
	}

}

?>