<?php

class community_modify_sql_mixin extends base_mixin
{

	protected $community_id;

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("community_id = {$this->community_id}");
	}

}

?>