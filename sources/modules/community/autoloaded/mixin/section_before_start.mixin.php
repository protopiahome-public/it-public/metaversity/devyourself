<?php

class section_before_start_mixin extends community_before_start_mixin
{

	protected $section_id;

	/**
	 * @var section_obj
	 */
	protected $section_obj;

	public function on_before_start()
	{
		if (!parent::on_before_start())
		{
			return false;
		}

		$this->section_id = $this->get_section_id();

		if (empty($this->section_id))
		{
			return true;
		}
		if (!is_good_id($this->section_id))
		{
			return false;
		}

		$this->section_obj = new section_obj($this->section_id, $this->community_obj, $lock = true);
		if (!$this->section_obj->exists())
		{
			return false;
		}

		return true;
	}

	protected function get_section_id()
	{
		return POST("section_id");
	}

}

?>