<?php

class community_before_commit_mixin extends base_mixin
{

	protected $community_id;
	protected $update_array;

	public function on_before_commit()
	{
		$this->update_array["community_id"] = $this->community_id;
		return true;
	}

}

?>