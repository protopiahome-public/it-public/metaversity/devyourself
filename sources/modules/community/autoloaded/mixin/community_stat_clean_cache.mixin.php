<?php

class community_stat_clean_cache_mixin extends base_mixin
{

	protected $community_id;

	public function clean_cache()
	{
		community_stat_cache_tag::init($this->community_id)->update();
	}

}

?>