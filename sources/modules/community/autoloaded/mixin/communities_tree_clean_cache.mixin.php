<?php

class communities_tree_clean_cache_mixin extends base_mixin
{

	protected $project_id;

	public function clean_cache()
	{
		communities_tree_cache_tag::init($this->project_id)->update();
	}

}

?>