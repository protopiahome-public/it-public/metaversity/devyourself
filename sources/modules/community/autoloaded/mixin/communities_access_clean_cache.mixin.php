<?php

class communities_access_clean_cache_mixin extends base_mixin
{

	protected $project_id;

	public function clean_cache()
	{
		communities_access_cache_tag::init($this->project_id)->update();
	}

}

?>