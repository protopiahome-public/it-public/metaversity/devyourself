<?php

class community_custom_feed_dt_init_mixin extends base_mixin
{

	/**
	 * @var community_custom_feed_dt
	 */
	protected $dt;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	protected function on_after_dt_init()
	{
		$this->dt->set_objects($this->project_obj, $this->community_obj);
		return true;
	}

}

?>