<?php

class community_before_start_mixin extends project_before_start_mixin
{

	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	public function on_before_start()
	{
		if (!parent::on_before_start())
		{
			return false;
		}

		$this->community_id = $this->get_community_id();
		if (!is_good_id($this->community_id))
		{
			return false;
		}

		$this->community_obj = new community_obj($this->community_id, $this->project_obj, $lock = true);
		if (!$this->community_obj->exists())
		{
			return false;
		}

		return true;
	}

	protected function get_community_id()
	{
		return POST("community_id");
	}

}

?>