<?php

class post_add_check_mixin extends base_mixin
{

	/**
	 * @var pass_info
	 */
	protected $pass_info;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var section_obj
	 */
	protected $section_obj;

	public function check()
	{
		$result = post_access_helper::can_add_post($this->community_obj, $this->section_obj);
		if (!$result)
		{
			$this->pass_info->write_error("SECTION_ACCESS_DISABLED");
		}
		return $result;
	}

}

?>