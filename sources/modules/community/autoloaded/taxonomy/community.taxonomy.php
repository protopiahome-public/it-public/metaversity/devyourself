<?php

class community_taxonomy extends base_taxonomy
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, project_obj $project_obj, $community_id)
	{
		$this->project_obj = $project_obj;
		$this->community_id = $community_id;
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		$p = $this->get_parts_relative();

		$project_obj = $this->project_obj;
		$project_id = $project_obj->get_id();
		$project_access = $project_obj->get_access();

		$community_id = $this->community_id;
		$community_obj = community_obj::instance($community_id, $project_obj);
		$community_access = $community_obj->get_access();

		//p/<project_name>/co/<community_name>/...
		$this->xml_loader->add_xml_by_class_name("community_short_xml_ctrl", $community_id, $project_id);
		
		$this->xml_loader->add_xml(new community_access_xml_ctrl($community_access));

		if (!$community_access->can_read_community())
		{
			$this->xml_loader->set_error_403();
			$this->xml_loader->set_error_403_xslt("community_403", "community");
		}
		else
		{
			$this->xml_loader->add_xml(new community_sections_xml_ctrl($community_id, $project_id));

			if ($parent_id = $community_obj->get_parent_id())
			{
				$this->xml_loader->add_xml_by_class_name("community_short_xml_ctrl", $parent_id, $project_id);
			}

			if ($p[1] === null)
			{
				//p/<project_name>/co/<community_name>/
				$this->xml_loader->add_xml_with_xslt(new community_widgets_xml_page($project_id, $community_id));
			}
			elseif ($p[1] === "feed" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
			{
				//p/<project_name>/co/<community_name>/
				$this->xml_loader->add_xml_with_xslt(new community_feed_xml_page($community_id, $project_id, $page));
			}
			elseif ($p[1] === "feed" and ($p[2] === "own" or $p[2] === "sub") and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				//p/<project_name>/co/<community_name>/(own|sub)/
				$this->xml_loader->add_xml_with_xslt(new community_feed_xml_page($community_id, $project_id, $page, null, $p[2]));
			}
			elseif ($section_id = $this->get_section_id_by_url_name($p[1], $community_id) and $page = $this->is_page_folder($p[2]) and $p[3] === null)
			{
				//p/<project_name>/co/<community_name>/<section_name>/
				$this->xml_loader->add_xml_with_xslt(new community_feed_xml_page($community_id, $project_id, $page, $section_id));
			}
			elseif ($p[1] === "subcommunities" and $p[2] === null)
			{
				//p/<project_name>/co/<community_name>/subcommunities/
				$this->xml_loader->add_xml_with_xslt(new community_subcommunities_xml_page($project_id, $community_id));
			}
			elseif ($p[1] === "subcommunities" and $p[2] === "add" and $p[3] === null)
			{
				//p/<project_name>/co/<community_name>/subcommunities/add/
				if ($project_access->can_add_communities() && $community_obj->get_allow_child_communities() != "nobody")
				{
					$this->xml_loader->add_xml_with_xslt(new community_child_community_add_xml_page($community_id, $project_id));
				}
				else
				{
					$this->xml_loader->set_error_403();
					$this->xml_loader->set_error_403_xslt("community_child_community_add_403", "community");
				}
			}
			elseif ($p[1] === "add" and $p[2] === null)
			{
				//p/<project_name>/co/<community_name>/add/
				$this->xml_loader->add_xml_with_xslt(new post_add_xml_page($community_id, $project_id));
			}
			elseif ($p[1] === "add" and $file_id = $this->is_type_folder($p[2], "file", true) and $p[3] === null)
			{
				//p/<project_name>/co/<community_name>/add/file-<id>/
				$this->xml_loader->add_xml_with_xslt(new post_file_download_xml_page($file_id, $community_id, $project_id));
			}
			elseif ($post_id = $this->is_type_folder($p[1], "post", true))
			{
				if ($p[2] === null)
				{
					//p/<project_name>/co/<community_name>/post-<id>/
					$this->xml_loader->add_xml_with_xslt(new post_show_xml_page($post_id, $project_id, true, $community_id));
				}
				elseif ($p[2] === "edit" and $p[3] === null)
				{
					//p/<project_name>/co/<community_name>/post-<id>/edit/
					$this->xml_loader->add_xml_with_xslt(new post_edit_xml_page($post_id, $community_id, $project_id), null, "post_403");
				}
				elseif ($p[2] === "delete" and $p[3] === null)
				{
					//p/<project_name>/co/<community_name>/post-<id>/delete/
					$this->xml_loader->add_xml_with_xslt(new post_delete_xml_page($post_id, $community_id, $project_id), null, "post_403");
				}
				elseif ($file_id = $this->is_type_folder($p[2], "file", true) and $p[3] === null)
				{
					//p/<project_name>/co/<community_name>/post-<id>/file-<id>/
					$this->xml_loader->add_xml_with_xslt(new post_file_download_xml_page($file_id, $community_id, $project_id, $post_id));
				}
			}
			elseif ($p[1] === "users" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new community_users_xml_page($community_id, $project_id, $page));
			}
			elseif ($p[1] === "feeds" and $p[2] === null)
			{
				//p/<community_name>/co/feeds/
				$this->xml_loader->add_xml_with_xslt(new community_custom_feeds_xml_page($community_id, $project_id));
			}
			elseif ($p[1] === "feeds" and $community_custom_feed_id = $this->get_community_custom_feed_id_by_url_name($p[2], $community_id) and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new community_custom_feed_xml_page($project_id, $community_id, $community_custom_feed_id, $page));
				$this->xml_loader->add_xml(new community_custom_feed_full_xml_ctrl($community_custom_feed_id, $community_id, $project_id));
			}
			elseif ($p[1] === "admin")
			{
				$community_admin_taxonomy = new community_admin_taxonomy($this->xml_loader, $this, 1, $project_obj, $community_obj);
				$community_admin_taxonomy->run();
			}
		}
	}

	private function get_section_id_by_url_name($url_name, $community_id)
	{
		return url_helper::get_section_id_by_name($url_name, $community_id);
	}

	private function get_community_custom_feed_id_by_url_name($url_name, $community_id)
	{
		return url_helper::get_community_custom_feed_id_by_name($url_name, $community_id);
	}

}

?>