<?php

class section_obj extends base_obj
{

	// Settings
	// Internal
	protected $access_array = array();

	/**
	 * @var community_obj
	 */
	private $community_obj;

	public function __construct($id, community_obj $community_obj, $lock = false)
	{
		$this->community_obj = $community_obj;
		parent::__construct($id, $lock);
	}

	private static $cache = array();

	/**
	 * @return section_obj
	 */
	public static function instance($id, community_obj $community_obj, $lock = false)
	{
		if (!$lock && isset(self::$cache[$id]))
		{
			return self::$cache[$id];
		}
		$class = __CLASS__;
		$instance = new $class($id, $community_obj, $lock);
		if (!$lock)
		{
			self::$cache[$id] = $instance;
		}
		return $instance;
	}

	/**
	 * @return section_access
	 */
	public function get_access($user_id = null)
	{
		if (!$this->exists())
		{
			trigger_error("Can't return access for unexisted section (id={$this->get_id()})");
			return null;
		}
		$key = $user_id ? $user_id : 0;
		if (!isset($this->access_array[$key]))
		{
			$this->access_array[$key] = new section_access($this, $this->community_obj->get_access($user_id), $user_id);
		}
		return $this->access_array[$key];
	}

	public function get_access_default()
	{
		return $this->get_param("access_default");
	}

	public function get_community_id()
	{
		return $this->community_obj->get_id();
	}

	public function get_name()
	{
		return $this->get_param("name");
	}

	public function get_title()
	{
		return $this->get_param("title");
	}

	public function get_position()
	{
		return $this->get_param("position");
	}

	protected function fill_data($lock)
	{
		$cache = section_cache::init($this->id);
		if ($lock)
		{
			$this->data = $this->fetch_data(true);
		}
		else
		{
			$this->data = $cache->get();
			if (empty($this->data))
			{
				$this->data = $this->fetch_data();
				if (!$this->data)
				{
					$this->data = array();
				}
				else
				{
					$cache->set($this->data);
				}
			}
		}
	}

	private function fetch_data($lock = false)
	{
		$lock_sql = $lock ? "LOCK IN SHARE MODE" : "";
		$row = $this->db->get_row("
			SELECT *
			FROM section
			WHERE id = {$this->id}
			{$lock_sql}
		");

		if ($row)
		{
			return $row["community_id"] == $this->community_obj->get_id() ? $row : null;
		}
		else
		{
			return null;
		}
	}

}

?>