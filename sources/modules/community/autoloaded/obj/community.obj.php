<?php

class community_obj extends base_obj
{

	// Settings
	protected $fake_delete = true;
	// Internal
	protected $access_array = array();

	/**
	 * @var project_obj
	 */
	private $project_obj;

	public function __construct($id, project_obj $project_obj, $lock = false)
	{
		$this->project_obj = $project_obj;
		parent::__construct($id, $lock);
	}

	private static $cache = array();

	/**
	 * @return community_obj
	 */
	public static function instance($id, project_obj $project_obj, $lock = false)
	{
		if (!$lock && isset(self::$cache[$id]))
		{
			return self::$cache[$id];
		}
		$class = __CLASS__;
		$instance = new $class($id, $project_obj, $lock);
		if (!$lock)
		{
			self::$cache[$id] = $instance;
		}
		return $instance;
	}

	/**
	 * @return community_access
	 */
	public function get_access($user_id = null)
	{
		if (!$this->exists())
		{
			trigger_error("Can't return access for unexisted community (id={$this->get_id()})");
			return null;
		}
		$key = $user_id ? $user_id : 0;
		if (!isset($this->access_array[$key]))
		{
			$this->access_array[$key] = new community_access($this, $this->project_obj->get_access($user_id), $user_id);
		}
		return $this->access_array[$key];
	}

	public function get_project_id()
	{
		return $this->project_obj->get_id();
	}

	public function get_parent_id()
	{
		return $this->get_param("parent_id");
	}

	public function get_parent_approved()
	{
		return $this->get_param("parent_approved");
	}

	public function get_name()
	{
		return $this->get_param("name");
	}

	public function get_title()
	{
		return $this->get_param("title");
	}

	public function get_child_community_count()
	{
		return $this->get_param("child_community_count_calc");
	}

	public function join_premoderation()
	{
		return $this->get_param("join_premoderation");
	}

	public function allow_posting_without_section()
	{
		return $this->get_param("allow_posting_without_section");
	}

	// allow_child_communities()
	public function get_allow_child_communities()
	{
		return $this->get_param("allow_child_communities");
	}

	protected function fill_data($lock)
	{
		$cache = community_cache::init($this->id);
		if ($lock)
		{
			$this->data = $this->fetch_data(true);
		}
		else
		{
			$this->data = $cache->get();
			if (empty($this->data))
			{
				$this->data = $this->fetch_data();
				if (!$this->data)
				{
					$this->data = array();
				}
				else
				{
					$cache->set($this->data);
				}
			}
		}
	}

	private function fetch_data($lock = false)
	{
		$lock_sql = $lock ? "LOCK IN SHARE MODE" : "";
		$row = $this->db->get_row("
			SELECT *
			FROM community
			WHERE id = {$this->id}
			{$lock_sql}
		");

		if ($row)
		{
			return $row["project_id"] == $this->project_obj->get_id() ? $row : null;
		}
		else
		{
			return null;
		}
	}

}

?>