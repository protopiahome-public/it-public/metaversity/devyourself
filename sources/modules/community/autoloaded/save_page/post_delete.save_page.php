<?php

class post_delete_save_page extends base_delete_virtual_save_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_modify_sql",
		"project_stat_clean_cache",
		"community_stat_clean_cache"
	);
	protected $db_table = "post";
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;
	protected $old_section_id;

	/**
	 * @var section_obj
	 */
	protected $old_section_obj = null;

	/**
	 * @var section_access
	 */
	protected $old_section_access = null;

	public function on_after_start()
	{
		$this->old_section_id = $this->old_db_row["section_id"];
		if ($this->old_section_id)
		{
			$this->old_section_obj = new section_obj($this->old_section_id, $this->community_obj);
		}
		return true;
	}

	public function check_rights()
	{
		$this->community_access = $this->community_obj->get_access();
		return $this->community_access->can_read_community();
	}

	public function check()
	{
		$result = post_access_helper::can_delete_post($this->old_db_row["author_user_id"], $this->community_obj, $this->old_section_obj);
		if (!$result)
		{
			$this->pass_info->write_error("SECTION_ACCESS_DISABLED");
		}
		return parent::check() && $result;
	}

	public function clean_cache()
	{
		post_cache_tag::init($this->id)->update();
		community_sections_cache_tag::init($this->community_id)->update();
	}

}

?>