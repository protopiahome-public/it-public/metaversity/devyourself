<?php

class post_edit_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"section_before_start",
		"post_edit_check",
		"community_modify_sql",
		"block_set_cut_dt_save",
	);
	// Settings
	protected $dt_name = "post";
	protected $axis_name = "edit";
	// Internal

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;
	protected $section_id;

	/**
	 * @var section_obj
	 */
	protected $section_obj;

	/**
	 * @var section_access
	 */
	protected $section_access;
	protected $old_section_id;

	/**
	 * @var section_obj
	 */
	protected $old_section_obj;

	/**
	 * @var section_access
	 */
	protected $old_section_access;

	public function on_after_start()
	{
		$this->old_section_id = $this->old_db_row["section_id"];
		if ($this->old_section_id)
		{
			$this->old_section_obj = new section_obj($this->old_section_id, $this->community_obj);
		}
		return true;
	}

	public function check_rights()
	{
		$this->community_access = $this->community_obj->get_access();
		return $this->community_access->can_read_community();
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_community_obj($this->community_obj);
		return true;
	}

	public function clean_cache()
	{
		post_cache_tag::init($this->id)->update();
		if ($this->old_db_row["section_id"] != $this->updated_db_row["section_id"])
		{
			community_sections_cache_tag::init($this->community_id)->update();
		}
	}

}

?>