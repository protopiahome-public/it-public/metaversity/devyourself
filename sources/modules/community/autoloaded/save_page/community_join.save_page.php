<?php

class community_join_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_stat_clean_cache"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	private $join;

	/**
	 * @var community_access_save
	 */
	private $community_access_save;

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->user->get_user_id() && $this->project_access->can_read_communities();
	}

	public function start()
	{
		/* join param check */
		$this->join = POST("join");
		if ($this->join !== "1" and $this->join !== "0")
		{
			return false;
		}

		return true;
	}

	public function commit()
	{
		$this->community_access_save = new community_access_save($this->community_obj, $this->project_obj, $this->user->get_user_id());
		
		if ($this->join)
		{
			$this->community_access_save->join();
		}
		else
		{
			$this->community_access_save->unjoin();
		}
		return true;
	}

	public function clean_cache()
	{
		$this->community_access_save->clean_cache();
	}

}

?>