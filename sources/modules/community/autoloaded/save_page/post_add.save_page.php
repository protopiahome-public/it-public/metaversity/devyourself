<?php

class post_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"section_before_start",
		"post_add_check",
		"project_stat_clean_cache",
		"community_stat_clean_cache",
		"block_set_cut_dt_save",
	);
	// Settings
	protected $dt_name = "post";
	protected $axis_name = "add";

	/**
	 * @var post_dt
	 */
	protected $dt;
	// Internal

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;
	protected $section_id;

	/**
	 * @var section_obj
	 */
	protected $section_obj;

	/**
	 * @var section_access
	 */
	protected $section_access;

	
	public function check_rights()
	{
		$this->community_access = $this->community_obj->get_access();
		return $this->community_access->can_read_community();
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_community_obj($this->community_obj);
		return true;
	}

	public function on_before_commit()
	{
		$this->update_array["community_id"] = $this->community_id;
		$this->update_array["author_user_id"] = $this->user->get_user_id();
		return true;
	}

	public function on_after_commit()
	{
		comments_subscribe_helper::subscribe("post", $this->last_id, $this->user->get_user_id());

		return true;
	}

	public function clean_cache()
	{
		community_sections_cache_tag::init($this->community_id)->update();
	}

}

?>