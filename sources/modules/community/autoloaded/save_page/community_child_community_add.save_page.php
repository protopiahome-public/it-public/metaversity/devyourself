<?php

class community_child_community_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"community_before_start",
		"community_dt_init",
		"project_stat_clean_cache",
		"community_add_widgets_after_commit",
		"communities_tree_clean_cache",
		"communities_access_clean_cache",
	);
	// Settings
	protected $dt_name = "community";
	protected $axis_name = "add";

	/**
	 * @var community_dt
	 */
	protected $dt;
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	/**
	 * @var community_access
	 */
	protected $community_access;

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		$this->community_access = $this->community_obj->get_access();
		return $this->project_access->can_add_communities() && $this->community_access->can_read_community() && $this->community_obj->get_allow_child_communities() != "nobody";
	}

	public function on_before_commit()
	{
		$this->update_array["project_id"] = "'{$this->project_id}'";
		return true;
	}

	public function on_after_commit()
	{
		$new_community_obj = new community_obj($this->last_id, $this->project_obj, $lock = true);
		$access_save = new community_access_save($new_community_obj, $this->project_obj, $this->user->get_user_id());
		$access_save->add_admin();

		$community_child_helper = new community_child_save_helper($this->community_obj, $new_community_obj);
		if ($this->community_obj->get_allow_child_communities() == "user" || $this->community_access->has_admin_rights())
		{
			$community_child_helper->add_child();
		}
		else
		{
			$community_child_helper->add_pretender();
			subcommunities_helper::send_emails_to_admins_about_pretender($this->project_obj, $this->community_obj, $new_community_obj);
		}
	}

	public function clean_cache()
	{
		community_cache_tag::init($this->community_id)->update();
	}

}

?>