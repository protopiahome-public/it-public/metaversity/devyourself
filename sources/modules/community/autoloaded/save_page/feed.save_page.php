<?php

class feed_save_page extends base_save_ctrl
{

	public function commit()
	{
		$feed_output_type = GET("type") == "short" ? "short" : "expanded";
		output_buffer::set_cookie("feed", $feed_output_type, 1591905600 /* Year 2020 */, $this->request->get_prefix() . "/");
		return true;
	}

}

?>