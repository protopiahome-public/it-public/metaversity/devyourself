<?php

class taxonomy_community_custom_feed_cache extends base_cache
{

	public static function init($name, $community_id, $community_custom_feed_id = null)
	{
		$tag_keys = array();
		if ($community_custom_feed_id)
		{
			$tag_keys[] = community_custom_feed_cache_tag::init($community_custom_feed_id)->get_key();
		}
		return parent::get_cache(__CLASS__, $name, $community_id, $tag_keys);
	}

}

?>