<?php

class community_access_cache extends base_cache
{

	public static function init($community_id, $user_id)
	{
		$tag_keys = array();
		$tag_keys[] = community_access_cache_tag::init($community_id, $user_id)->get_key();
		return parent::get_cache(__CLASS__, $community_id, $user_id, $tag_keys);
	}

}

?>