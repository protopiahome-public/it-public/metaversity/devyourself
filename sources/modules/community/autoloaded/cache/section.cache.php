<?php

class section_cache extends base_cache
{

	public static function init($section_id)
	{
		$tag_keys = array();
		$tag_keys[] = section_cache_tag::init($section_id)->get_key();
		return parent::get_cache(__CLASS__, $section_id, $tag_keys);
	}

}

?>