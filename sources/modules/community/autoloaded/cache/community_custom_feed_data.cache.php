<?php

class community_custom_feed_data_cache extends base_cache
{

	public static function init($feed_id)
	{
		$tag_keys = array();
		$tag_keys[] = community_custom_feed_sources_cache_tag::init($feed_id)->get_key();
		return parent::get_cache(__CLASS__, $feed_id, $tag_keys);
	}

}

?>