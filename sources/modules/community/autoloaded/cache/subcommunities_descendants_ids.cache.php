<?php

class subcommunities_descendants_ids_cache extends base_cache
{

	public static function init($project_id, $community_id)
	{
		$tag_keys = array();
		$tag_keys[] = communities_tree_cache_tag::init($project_id)->get_key();
		return parent::get_cache(__CLASS__, $community_id, $tag_keys);
	}

}

?>