<?php

class taxonomy_section_cache extends base_cache
{

	public static function init($name, $community_id)
	{
		$tag_keys = array();
		$tag_keys[] = community_sections_cache_tag::init($community_id)->get_key();
		return parent::get_cache(__CLASS__, $name, $community_id, $tag_keys);
	}

}

?>