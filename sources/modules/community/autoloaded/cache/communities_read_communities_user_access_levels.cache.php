<?php

class communities_read_communities_user_access_levels_cache extends base_cache
{

	public static function init($project_id, $user_id)
	{
		$tag_keys = array();
		$tag_keys[] = communities_user_access_cache_tag::init($project_id, $user_id)->get_key();
		return parent::get_cache(__CLASS__, $project_id, $tag_keys);
	}

}

?>