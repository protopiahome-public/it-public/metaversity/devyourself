<?php

class community_sections_cache extends base_cache
{

	public static function init($community_id)
	{
		$tag_keys = array();
		$tag_keys[] = community_sections_cache_tag::init($community_id)->get_key();
		$tag_keys[] = community_cache_tag::init($community_id)->get_key();
		return parent::get_cache(__CLASS__, $community_id, $tag_keys);
	}

}

?>