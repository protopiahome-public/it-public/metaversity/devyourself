<?php

class taxonomy_project_custom_feed_cache extends base_cache
{

	public static function init($name, $project_id, $project_custom_feed_id = null)
	{
		$tag_keys = array();
		if ($project_custom_feed_id)
		{
			$tag_keys[] = project_custom_feed_cache_tag::init($project_custom_feed_id)->get_key();
		}
		return parent::get_cache(__CLASS__, $name, $project_id, $tag_keys);
	}

}

?>