<?php

class post_full_cache extends base_cache
{

	public static function init($post_id, $community_id, $section_id = null)
	{
		$tag_keys = array();
		$tag_keys[] = post_cache_tag::init($post_id)->get_key();
		$tag_keys[] = community_posts_cache_tag::init($community_id)->get_key();
		if ($section_id)
		{
			$tag_keys[] = section_cache_tag::init($section_id)->get_key();
		}
		return parent::get_cache(__CLASS__, $post_id, $tag_keys);
	}

}

?>