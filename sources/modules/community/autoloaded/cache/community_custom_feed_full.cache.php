<?php

class community_custom_feed_full_cache extends base_cache
{

	public static function init($feed_id, $community_id, $project_id)
	{
		$tag_keys = array();
//		$tag_keys[] = project_cache_tag::init($project_id)->get_key();
		$tag_keys[] = community_cache_tag::init($community_id)->get_key();
		$tag_keys[] = community_custom_feed_cache_tag::init($feed_id)->get_key();
		return parent::get_cache(__CLASS__, $feed_id, $tag_keys);
	}

}

?>