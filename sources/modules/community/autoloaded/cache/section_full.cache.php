<?php

class section_full_cache extends base_cache
{

	public static function init($section_id, $community_id = null, $project_id = null)
	{
		$tag_keys = array();
		if ($project_id && $community_id)
		{
//			$tag_keys[] = project_cache_tag::init($project_id)->get_key();
			$tag_keys[] = community_cache_tag::init($community_id)->get_key();
		}
		$tag_keys[] = section_cache_tag::init($section_id)->get_key();
		return parent::get_cache(__CLASS__, $section_id, $tag_keys);
	}

}

?>