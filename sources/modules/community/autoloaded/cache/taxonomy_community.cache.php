<?php

class taxonomy_community_cache extends base_cache
{

	public static function init($name, $project_id, $community_id = null)
	{
		$tag_keys = array();
		if ($community_id)
		{
			$tag_keys[] = community_cache_tag::init($community_id)->get_key();
		}
		return parent::get_cache(__CLASS__, $name, $project_id, $tag_keys);
	}

}

?>