<?php

class post_full_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "post";
	protected $axis_name = "full";
	// Internal
	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $project_id;
	protected $load_block_set = false;
	protected $community_id = null;
	protected $section_id = null;
	protected $dependencies_settings = array(
		array(
			"column" => "community_id",
			"ctrl" => "community_short",
			"param2" => "project_id",
		),
		array(
			"column" => "author_user_id",
			"ctrl" => "user_short",
			"param2" => "project_id",
		),
	);

	// @dm9 CR all
	public function __construct($post_id, $project_id, $load_block_set = false, $community_id = null)
	{
		$this->project_id = $project_id;
		$this->load_block_set = $load_block_set;
		$this->community_id = $community_id;

		$this->project_obj = project_obj::instance($this->project_id);

		parent::__construct($post_id);
	}

	protected function get_cache()
	{
		// @dm9 Is NULL OK?
		return post_full_cache::init($this->id, $this->community_id, $this->section_id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("community_id");
		$select_sql->add_select_fields("author_user_id");
		$select_sql->add_select_fields("block_set_id");
		$select_sql->add_where("is_deleted = 0");
	}

	protected function process_data()
	{
		if ($this->data)
		{
			$this->store_data = array(
				"community_id" => $this->data[0]["community_id"],
				"block_set_id" => $this->data[0]["block_set_id"],
				"section_id" => $this->data[0]["section_id"],
				"author_user_id" => $this->data[0]["author_user_id"],
			);

			if ($this->data[0]["section_id"])
			{
				// @dm9 why not to load section_full?
				// @dm9 check cache deps
				$section_row = $this->db->get_row("SELECT * FROM section WHERE id = {$this->data[0]["section_id"]}");
				$this->data[0]["section_name"] = $section_row["name"];
				$this->data[0]["section_title"] = $section_row["title"];
				
				$this->section_id = $this->data[0]["section_id"];
			}
		}
	}

	protected function modify_xml(xdom $xdom)
	{
		$xdom->set_attr("community_id", $this->data[0]["community_id"]);
		$xdom->set_attr("author_user_id", $this->data[0]["author_user_id"]);
		$xdom->set_attr("block_set_id", $this->data[0]["block_set_id"]);
		if ($this->data[0]["section_id"])
		{
			$xdom->set_attr("section_name", $this->data[0]["section_name"]);
			$xdom->set_attr("section_title", $this->data[0]["section_title"]);
		}
	}

	protected function postprocess()
	{
		if ($this->store_data)
		{
			// @dm9 oh shit!
			if ($this->community_id and $this->community_id != $this->store_data["community_id"])
			{
				$this->set_error_404();
				return;
			}

			if ($this->load_block_set)
			{
				$this->xml_loader->add_xml_by_class_name("block_set_xml_ctrl", $this->store_data["block_set_id"]);
			}

			$community_obj = new community_obj($this->store_data["community_id"], $this->project_obj);
			$section_obj = $this->store_data["section_id"] ? new section_obj($this->store_data["section_id"], $community_obj) : null;
			if ($this->store_data["section_id"])
			{
				$this->xml_loader->add_xml_by_class_name("section_access_xml_ctrl", $this->store_data["section_id"], $community_obj->get_id(), $this->project_id);
			}
			$this->xml_loader->add_xml(new post_access_xml_ctrl($this->id, $this->store_data["author_user_id"], $community_obj, $section_obj), "post_access_{$this->id}");
		}
	}

}

?>