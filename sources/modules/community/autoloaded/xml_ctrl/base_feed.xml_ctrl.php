<?php

define("FEED_TYPE_EXPANDED", "expanded");
define("FEED_TYPE_SHORT", "short");

abstract class base_feed_xml_ctrl extends base_easy_xml_ctrl
{
	protected $output_type; // "expanded" or "short"
	
	public function __construct()
	{
		parent::__construct();
		$this->fill_output_type();
	}
	
	protected function fill_output_type()
	{
		$this->output_type = isset($_COOKIE["feed"]) && $_COOKIE["feed"] == FEED_TYPE_SHORT ? FEED_TYPE_SHORT : FEED_TYPE_EXPANDED;
	}
	
}

?>
