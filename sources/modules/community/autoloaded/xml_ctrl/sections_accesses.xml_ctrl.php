<?php

class sections_accesses_xml_ctrl extends base_xml_ctrl
{

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;
	protected $section_accesses = array();
	protected $can_add_post = false;

	public function __construct(array $section_ids, $community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);
		$this->community_access = $this->community_obj->get_access();
		foreach ($section_ids as $section_id)
		{
			$section_obj = new section_obj($section_id, $this->community_obj);
			$this->section_accesses[] = $section_obj->get_access();
		}

		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		$access_nodes = "";
		$any_section_add_post_access_level = null;
		$any_section_add_post_access_status = null;
		foreach ($this->section_accesses as $section_access)
		{
			/* @var $section_access section_access */

			$attrs = array(
				"section_id" => $section_access->get_section_id(),
				"status" => $section_access->get_status(),
				"can_add_post" => $section_access->can_add_post(),
				"can_comment" => $section_access->can_comment(),
			);

			foreach ($section_access->get_required_statuses() as $access_name => $access_status)
			{
				$attrs["access_" . $access_name] = $access_status;
				if ($access_name == "add_post")
				{
					$access_level = $section_access->get_access_maper()->get_level_from_status($access_status);
					if (is_null($any_section_add_post_access_level) or $access_level < $any_section_add_post_access_level)
					{
						$any_section_add_post_access_level = $access_level;
						$any_section_add_post_access_status = $access_status;
					}
				}
			}

			$this->can_add_post = $this->can_add_post || $section_access->can_add_post();

			$access_nodes .= $this->get_node_string("section_access", $attrs);
		}

		$accesses_attrs = array();
		if ($this->community_obj->allow_posting_without_section())
		{
			$accesses_attrs["allow_posting_without_section"] = 1;
			$accesses_attrs["any_section_add_post_access_status"] = $this->community_access->get_required_status("add_post");
			$this->can_add_post = $this->can_add_post || $this->community_access->can_add_post();
		}
		elseif (!is_null($any_section_add_post_access_level))
		{
			$accesses_attrs["any_section_add_post_access_status"] = $any_section_add_post_access_status;
		}

		$accesses_attrs["any_section_can_add_post"] = $this->can_add_post;

		return $this->get_node_string("sections_accesses", $accesses_attrs, $access_nodes);
	}

}

?>