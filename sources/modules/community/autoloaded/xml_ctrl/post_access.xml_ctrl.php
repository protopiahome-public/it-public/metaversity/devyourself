<?php

class post_access_xml_ctrl extends base_xml_ctrl
{

	protected $post_id;
	protected $post_author_id;

	/**
	 *
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 *
	 * @var section_obj
	 */
	protected $section_obj;

	public function __construct($post_id, $post_author_id, community_obj $community_obj, section_obj $section_obj = null)
	{
		$this->post_id = $post_id;
		$this->post_author_id = $post_author_id;
		$this->community_obj = $community_obj;
		$this->section_obj = $section_obj;
		parent::__construct();
	}

	public function get_xml()
	{
		$accesses_attrs = array();
		$accesses_attrs["post_id"] = $this->post_id;
		$accesses_attrs["community_id"] = $this->community_obj->get_id();
		if ($this->section_obj)
		{
			$accesses_attrs["section_id"] = $this->section_obj->get_id();
		}
		$accesses_attrs["can_edit"] = post_access_helper::can_edit_post_without_section_change($this->post_author_id, $this->community_obj, $this->section_obj);
		$accesses_attrs["can_comment"] = post_access_helper::can_comment_post($this->community_obj, $this->section_obj);
		$accesses_attrs["access_comment"] = post_access_helper::get_comment_status($this->community_obj, $this->section_obj);

		return $this->get_node_string("post_access", $accesses_attrs);
	}

}

?>