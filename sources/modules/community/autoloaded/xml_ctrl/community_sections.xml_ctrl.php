<?php

class community_sections_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "section";
	protected $xml_attrs = array("full_count");
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "section_full",
			"param2" => "community_id",
			"param3" => "project_id",
		)
	);
	// Internal
	protected $community_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $project_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	protected $full_count;

	public function __construct($community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);

		parent::__construct();
	}

	protected function get_cache()
	{
		return community_sections_cache::init($this->community_id);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("section dt");
		$select_sql->add_select_fields("dt.id, dt.post_count_calc");
		$select_sql->add_where("dt.community_id = {$this->community_id}");
		$select_sql->add_order("dt.position");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
		
		$this->store_data = array("ids" => array());
		foreach ($this->data as $row)
		{
			$this->store_data["ids"][] = $row["id"];
		}
		
		$this->full_count = $this->db->get_value("SELECT post_count_calc FROM community WHERE id = {$this->community_id}");
	}

	protected function postprocess()
	{
		$this->xml_loader->add_xml(new sections_accesses_xml_ctrl($this->store_data["ids"], $this->community_id, $this->project_id));
	}

}

?>