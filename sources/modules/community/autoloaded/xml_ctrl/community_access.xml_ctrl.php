<?php

class community_access_xml_ctrl extends base_xml_ctrl
{

	/**
	 * @var community_access
	 */
	private $community_access;

	public function __construct(community_access $community_access)
	{
		$this->community_access = $community_access;
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		$attrs = array(
			"community_id" => $this->community_access->get_community_id(),
			"status" => $this->community_access->get_status(),
			"is_member" => $this->community_access->is_member(),
			"is_pretender" => $this->community_access->is_pretender_strict(),
			"has_member_rights" => $this->community_access->has_member_rights(),
			"has_admin_rights" => $this->community_access->has_admin_rights(),
			"can_add_post" => $this->community_access->can_add_post(),
			"can_comment" => $this->community_access->can_comment(),
		);
		foreach ($this->community_access->get_required_statuses() as $access_name => $status)
		{
			$attrs["access_" . $access_name] = $status;
		}

		foreach ($this->community_access->get_required_statuses() as $access_name => $status)
		{
			$attrs["access_" . $access_name] = $status;
		}

		return $this->get_node_string("community_access", $attrs);
	}

}

?>