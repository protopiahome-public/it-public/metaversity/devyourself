<?php

class community_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "community";
	protected $axis_name = "short";

	/**
	 * @var community_dt
	 */
	protected $dt;
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($community_id, $project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct($community_id);
	}

	protected function get_cache()
	{
		return community_short_cache::init($this->id, $this->project_id);
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_project_obj($this->project_obj, $use_project_url_placeholder = true);
	}
	
	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("dt.is_deleted = 0");
	}

	protected function postprocess()
	{
		$this->xml = preg_replace("/%PROJECT_URL%/", $this->project_obj->get_url(), $this->xml, 1);
	}

}

?>