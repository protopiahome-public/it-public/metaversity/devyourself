<?php

class community_custom_feed_full_xml_ctrl extends base_dt_show_xml_ctrl
{

	protected $dt_name = "community_custom_feed";
	protected $axis_name = "full";
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 *
	 * @var community_dt
	 */
	protected $dt;

	public function __construct($feed_id, $community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);

		parent::__construct($feed_id);
	}

	protected function get_cache()
	{
		return community_custom_feed_full_cache::init($this->id, $this->community_obj->get_id(), $this->project_obj->get_id());
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_objects($this->project_obj, $this->community_obj, $use_project_url_placeholder = true);
		return true;
	}

	protected function postprocess()
	{
		$this->xml = preg_replace("/%PROJECT_URL%/", $this->project_obj->get_url(), $this->xml, 1);
	}

}

?>