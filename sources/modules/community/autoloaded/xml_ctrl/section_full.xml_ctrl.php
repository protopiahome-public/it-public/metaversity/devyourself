<?php

class section_full_xml_ctrl extends base_dt_show_xml_ctrl
{

	protected $dt_name = "section";
	protected $axis_name = "full";
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;

	/**
	 * @var community_access
	 */
	protected $community_access;

	/**
	 * @var section_dt
	 */
	protected $dt;

	public function __construct($section_id, $community_id, $project_id)
	{
		$this->community_id = $community_id;
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);
		$this->community_access = $this->community_obj->get_access();

		parent::__construct($section_id);
	}

	protected function get_cache()
	{
		return section_full_cache::init($this->id, $this->community_obj ? $this->community_obj->get_id() : null, $this->project_obj->get_id());
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_objects($this->project_obj, $this->community_obj);
	}

	protected function postprocess()
	{
		$this->xml = preg_replace("/%PROJECT_URL%/", $this->project_obj->get_url(), $this->xml, 1);
	}

}

?>