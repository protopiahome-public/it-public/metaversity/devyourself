<?php

class section_access_xml_ctrl extends base_xml_ctrl
{

	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $community_id;

	/**
	 * @var community_obj
	 */
	protected $community_obj;
	
	protected $section_id;

	/**
	 * @var section_obj
	 */
	protected $section_obj;
	
	/**
	 * @var section_access
	 */
	private $section_access;

	public function __construct($section_id, $community_id, $project_id)
	{
		$this->section_id = $section_id;
		$this->community_id = $community_id;
		$this->project_id = $project_id;
		
		$this->project_obj = project_obj::instance($this->project_id);
		$this->community_obj = community_obj::instance($this->community_id, $this->project_obj);
		$this->section_obj = section_obj::instance($section_id, $this->community_obj);
		$this->section_access = $this->section_obj->get_access();

		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		$attrs = array(
			"section_id" => $this->section_id,
			"status" => $this->section_access->get_status(),
			"can_add_post" => $this->section_access->can_add_post(),
			"can_comment" => $this->section_access->can_comment(),
		);

		foreach ($this->section_access->get_required_statuses() as $access_name => $status)
		{
			$attrs["access_" . $access_name] = $status;
		}

		return $this->get_node_string("section_access", $attrs);
	}

}

?>