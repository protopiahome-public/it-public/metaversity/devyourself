<?php

class communities_lists_item_calc_db_test extends base_test
{

	public function set_up()
	{
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user')");
		}
		$this->db->sql("TRUNCATE project");
		$this->db->sql("TRUNCATE post_calc");
		$this->db->sql("TRUNCATE post");
		$this->db->sql("TRUNCATE comment");
		$this->db->sql("TRUNCATE project_custom_feed_source");
		$this->db->sql("TRUNCATE project_custom_feed");
		$this->db->sql("TRUNCATE community_custom_feed_source");
		$this->db->sql("TRUNCATE community_custom_feed");
		$this->db->sql("TRUNCATE communities_menu_item");
		$this->db->sql("TRUNCATE community");
		$this->db->sql("TRUNCATE project_widget_communities");
		$this->db->sql("TRUNCATE community_widget_communities");
		$this->db->sql("REPLACE INTO project (id, name, title) VALUES (1, 'test', 'test')");
		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (1, 'test', 'test', 1)");
		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (2, 'test2', 'test2', 1)");

		$this->db->sql("INSERT INTO communities_menu_item (id, project_id, community_id) VALUES (1,1,1)");
		$this->db->sql("INSERT INTO communities_menu_item (id, project_id, community_id) VALUES (2,1,2)");

		$this->db->sql("INSERT INTO project_widget (id, project_id, type_id) VALUES (1,1,'communities')");
		$this->db->sql("INSERT INTO project_widget_communities (widget_id, community_id) VALUES (1,1)");
		$this->db->sql("INSERT INTO project_widget_communities (widget_id, community_id) VALUES (1,2)");

		$this->db->sql("INSERT INTO community_widget (id, community_id, type_id) VALUES (1,1,'communities')");
		$this->db->sql("INSERT INTO community_widget_communities (widget_id, community_id) VALUES (1,1)");
		$this->db->sql("INSERT INTO community_widget_communities (widget_id, community_id) VALUES (1,2)");

		$this->db->sql("INSERT INTO project_custom_feed (id, project_id, descr, name, title) VALUES (1,1, '', '1', '1')");
		$this->db->sql("INSERT INTO project_custom_feed_source (custom_feed_id, community_id) VALUES (1,1)");
		$this->db->sql("INSERT INTO project_custom_feed_source (custom_feed_id, community_id) VALUES (1,2)");

		$this->db->sql("INSERT INTO community_custom_feed (id, community_id, descr, name, title) VALUES (1,1, '', '1', '1')");
		$this->db->sql("INSERT INTO community_custom_feed_source (custom_feed_id, community_id) VALUES (1,1)");
		$this->db->sql("INSERT INTO community_custom_feed_source (custom_feed_id, community_id) VALUES (1,2)");
	}

	public function communities_menu_delete_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE community SET is_deleted = 1 WHERE id = 1");

		$this->assert_db_value("communities_menu_item", "community_id", 1, "", "community_id");
		$this->assert_db_value("communities_menu_item", "community_id", 2, "2", "community_id");

		$this->db->rollback();
	}

	public function project_widget_communities_delete_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE community SET is_deleted = 1 WHERE id = 1");

		$this->assert_db_value("project_widget_communities", "community_id", 1, "", "community_id");
		$this->assert_db_value("project_widget_communities", "community_id", 2, "2", "community_id");

		$this->db->rollback();
	}

	public function community_widget_communities_delete_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE community SET is_deleted = 1 WHERE id = 1");

		$this->assert_db_value("community_widget_communities", "community_id", 1, "", "community_id");
		$this->assert_db_value("community_widget_communities", "community_id", 2, "2", "community_id");

		$this->db->rollback();
	}

	public function project_custom_feed_source_delete_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE community SET is_deleted = 1 WHERE id = 1");

		$this->assert_db_value("project_custom_feed_source", "community_id", 1, "", "community_id");
		$this->assert_db_value("project_custom_feed_source", "community_id", 2, "2", "community_id");

		$this->db->rollback();
	}

	public function community_custom_feed_source_delete_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE community SET is_deleted = 1 WHERE id = 2");

		$this->assert_db_value("community_custom_feed_source", "community_id", 1, "1", "community_id");
		$this->assert_db_value("community_custom_feed_source", "community_id", 2, "", "community_id");

		$this->db->rollback();
	}

}

?>