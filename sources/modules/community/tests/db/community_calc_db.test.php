<?php

class community_calc_db_test extends base_test
{

	public function set_up()
	{

		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user')");
		}
		$this->db->sql("TRUNCATE project");
		$this->db->sql("TRUNCATE project_widget");
		$this->db->sql("TRUNCATE community_widget");
		$this->db->sql("TRUNCATE post_calc");
		$this->db->sql("TRUNCATE post");
		$this->db->sql("TRUNCATE comment");
		$this->db->sql("TRUNCATE project_custom_feed_source");
		$this->db->sql("TRUNCATE community_user_link");
		$this->db->sql("TRUNCATE section");
		$this->db->sql("TRUNCATE community");
		$this->db->sql("TRUNCATE event");
		$this->db->sql("TRUNCATE material");
		$this->db->sql("REPLACE INTO project (id, name, title) VALUES (1, 'test', 'test')");
		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (1, 'test', 'test', 1)");
	}

	public function basic_test()
	{
		$this->db->begin();

		//check set_up
		$this->assert_db_value("project", "community_count_calc", 1, "1");
		$this->db->rollback();
	}

	public function community_add_test()
	{
		$this->db->begin();

		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (2, 'test2', 'test', 1)");

		$this->assert_db_value("project", "community_count_calc", 1, "2");
		$this->db->rollback();
	}

	public function community_delete_and_restore_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE community SET is_deleted = 1 WHERE id = 1");

		$this->assert_db_value("project", "community_count_calc", 1, "0");

		$this->db->sql("UPDATE community SET is_deleted = 0 WHERE id = 1");

		$this->assert_db_value("project", "community_count_calc", 1, "1");

		$this->db->rollback();
	}

}

?>