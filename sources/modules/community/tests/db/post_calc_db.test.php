<?php

class post_calc_db_test extends base_test
{

	public function set_up()
	{
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user')");
		}
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 2"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (2, 'test_user2')");
		}
		$this->db->sql("TRUNCATE project_widget");
		$this->db->sql("TRUNCATE community_widget");
		$this->db->sql("TRUNCATE post_calc");
		$this->db->sql("TRUNCATE post");
		$this->db->sql("TRUNCATE comment");
		$this->db->sql("TRUNCATE project_custom_feed_source");
		$this->db->sql("TRUNCATE section");
		$this->db->sql("TRUNCATE community");
		$this->db->sql("TRUNCATE event");
		$this->db->sql("TRUNCATE material");
		$this->db->sql("TRUNCATE user_project_link");
		$this->db->sql("TRUNCATE community_user_link");
		$this->db->sql("REPLACE INTO project (id, name, title, post_count_calc, comment_count_calc) VALUES (1, 'test', 'test', 0, 0)");
		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (1, 'test', 'test', 1)");
		$this->db->sql("INSERT INTO section (id, name, title, community_id) VALUES (1, 'test', 'test', 1)");
		$this->db->sql("INSERT INTO comment (id, type_id, author_user_id, object_id, html) VALUES (50, 3, 1, 1, '')");
		$this->db->sql("INSERT INTO comment (id, type_id, author_user_id, object_id, html) VALUES (51, 3, 1, 1, '')");
		$this->db->sql("INSERT INTO post (id, author_user_id, title, community_id, section_id, hide_in_project_feed, comment_count_calc, last_comment_id, last_comment_author_user_id, last_comment_html) VALUES (1, 1, 'test', 1, 1, 1, 15, 50, 1, 'bla')");
	}

	protected function get_community_user_post_count_calc()
	{
		return $this->db->get_value("
			SELECT post_count_calc 
			FROM community_user_link 
			WHERE user_id = 1 AND community_id = 1
		");
	}

	protected function get_project_user_post_count_calc()
	{
		return $this->db->get_value("
			SELECT post_count_calc 
			FROM user_project_link 
			WHERE user_id = 1 AND project_id = 1
		");
	}

	public function basic_test()
	{
		//check set_up

		$this->db->begin();

		$this->assert_db_value("project", "post_count_calc", 1, "1");
		$this->assert_db_value("community", "post_count_calc", 1, "1");
		$this->assert_db_value("section", "post_count_calc", 1, "1");
		$this->assert_equal($this->get_community_user_post_count_calc(), "1");
		$this->assert_equal($this->get_project_user_post_count_calc(), "1");

		$this->db->rollback();
	}

	public function calc_add_test()
	{
		$this->db->begin();

		$this->assert_db_value("post_calc", "project_id", 1, "1");
		$this->assert_db_value("post_calc", "community_id", 1, "1");
		$this->assert_db_value("post_calc", "section_id", 1, "1");
		$this->assert_db_value("post_calc", "author_user_id", 1, "1");
		$this->assert_db_value("post_calc", "hide_in_project_feed", 1, "1");
		$this->assert_db_value("post_calc", "comment_count_calc", 1, "15");
		$this->assert_db_value("post_calc", "last_comment_id", 1, "50");
		$this->assert_db_value("post_calc", "last_comment_author_user_id", 1, "1");
		$this->assert_db_value("post_calc", "last_comment_html", 1, "bla");

		$this->db->rollback();
	}

	public function calc_edit_test()
	{
		$this->db->begin();

		$this->db->sql("INSERT INTO section (id, name, title, community_id) VALUES (2, 'test2', 'test2', 1)");

		$this->db->sql("UPDATE post SET hide_in_project_feed = 1, section_id = 2, comment_count_calc = 14,
			last_comment_id = 51, last_comment_author_user_id = 2, last_comment_html = 'bla2'
			WHERE id = 1");

		$this->assert_db_value("post_calc", "project_id", 1, "1");
		$this->assert_db_value("post_calc", "community_id", 1, "1");
		$this->assert_db_value("post_calc", "section_id", 1, "2");
		$this->assert_db_value("post_calc", "author_user_id", 1, "1");
		$this->assert_db_value("post_calc", "hide_in_project_feed", 1, "1");
		$this->assert_db_value("post_calc", "comment_count_calc", 1, "14");
		$this->assert_db_value("post_calc", "last_comment_id", 1, "51");
		$this->assert_db_value("post_calc", "last_comment_author_user_id", 1, "2");
		$this->assert_db_value("post_calc", "last_comment_html", 1, "bla2");

		$this->db->rollback();
	}

	public function calc_delete_and_restore_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE post SET is_deleted = 1 WHERE id = 1");

		$this->assert_db_row_not_exist("SELECT * FROM post_calc WHERE id = 1");

		$this->db->sql("UPDATE post SET is_deleted = 0 WHERE id = 1");

		$this->assert_db_value("post_calc", "project_id", 1, "1");
		$this->assert_db_value("post_calc", "community_id", 1, "1");
		$this->assert_db_value("post_calc", "section_id", 1, "1");
		$this->assert_db_value("post_calc", "author_user_id", 1, "1");
		$this->assert_db_value("post_calc", "hide_in_project_feed", 1, "1");
		$this->assert_db_value("post_calc", "comment_count_calc", 1, "15");
		$this->assert_db_value("post_calc", "last_comment_id", 1, "50");
		$this->assert_db_value("post_calc", "last_comment_author_user_id", 1, "1");
		$this->assert_db_value("post_calc", "last_comment_html", 1, "bla");

		$this->db->rollback();
	}

	public function calc_community_delete_and_restore_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE community SET is_deleted = 1 WHERE id = 1");

		$this->assert_db_row_not_exist("SELECT * FROM post_calc WHERE id = 1");

		$this->db->sql("UPDATE community SET is_deleted = 0 WHERE id = 1");

		$this->assert_db_value("post_calc", "project_id", 1, "1");
		$this->assert_db_value("post_calc", "community_id", 1, "1");
		$this->assert_db_value("post_calc", "section_id", 1, "1");
		$this->assert_db_value("post_calc", "author_user_id", 1, "1");
		$this->assert_db_value("post_calc", "hide_in_project_feed", 1, "1");

		$this->db->rollback();
	}

	public function post_add_test()
	{
		$this->db->begin();

		$this->db->sql("INSERT INTO post (id, author_user_id, title, community_id, section_id) VALUES (2, 1, 'test', 1, 1)");

		$this->assert_db_value("project", "post_count_calc", 1, "2");
		$this->assert_db_value("community", "post_count_calc", 1, "2");
		$this->assert_db_value("section", "post_count_calc", 1, "2");
		$this->assert_equal($this->get_community_user_post_count_calc(), "2");
		$this->assert_equal($this->get_project_user_post_count_calc(), "2");

		$this->db->rollback();
	}

	public function post_delete_and_restore_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE post SET is_deleted = 1 WHERE id = 1");

		$this->assert_db_value("project", "post_count_calc", 1, "0");
		$this->assert_db_value("community", "post_count_calc", 1, "0");
		$this->assert_db_value("section", "post_count_calc", 1, "0");
		$this->assert_equal($this->get_community_user_post_count_calc(), "0");
		$this->assert_equal($this->get_project_user_post_count_calc(), "0");

		$this->db->sql("UPDATE post SET is_deleted = 0 WHERE id = 1");

		$this->assert_db_value("project", "post_count_calc", 1, "1");
		$this->assert_db_value("community", "post_count_calc", 1, "1");
		$this->assert_db_value("section", "post_count_calc", 1, "1");
		$this->assert_equal($this->get_community_user_post_count_calc(), "1");
		$this->assert_equal($this->get_project_user_post_count_calc(), "1");

		$this->db->rollback();
	}

	public function community_delete_and_restore_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE community SET is_deleted = 1 WHERE id = 1");

		$this->assert_db_value("project", "post_count_calc", 1, "0");
		$this->assert_db_value("community", "post_count_calc", 1, "1");
		$this->assert_db_value("section", "post_count_calc", 1, "1");
		$this->assert_equal($this->get_community_user_post_count_calc(), "1");
		$this->assert_equal($this->get_project_user_post_count_calc(), "1");

		$this->db->sql("UPDATE community SET is_deleted = 0 WHERE id = 1");

		$this->assert_db_value("project", "post_count_calc", 1, "1");
		$this->assert_db_value("community", "post_count_calc", 1, "1");
		$this->assert_db_value("section", "post_count_calc", 1, "1");
		$this->assert_equal($this->get_community_user_post_count_calc(), "1");
		$this->assert_equal($this->get_project_user_post_count_calc(), "1");

		$this->db->rollback();
	}

	public function section_move_test()
	{
		$this->db->begin();

		$this->db->sql("INSERT INTO section (id, name, title, community_id) VALUES (2, 'test2', 'test2', 1)");

		$this->db->sql("UPDATE post SET hide_in_project_feed = 1, section_id = 2 WHERE id = 1");
		$this->assert_db_value("project", "post_count_calc", 1, "1");
		$this->assert_db_value("community", "post_count_calc", 1, "1");
		$this->assert_db_value("section", "post_count_calc", 1, "0");
		$this->assert_db_value("section", "post_count_calc", 2, "1");
		$this->assert_equal($this->get_community_user_post_count_calc(), "1");
		$this->assert_equal($this->get_project_user_post_count_calc(), "1");

		$this->db->rollback();
	}

	public function section_delete_test()
	{
		$this->db->begin();

		$this->db->sql("DELETE FROM section WHERE id = 1");

		$this->assert_db_value("project", "post_count_calc", 1, "1");
		$this->assert_db_value("community", "post_count_calc", 1, "1");
		$this->assert_db_value("post", "section_id", 1, "");
		$this->assert_equal($this->get_community_user_post_count_calc(), "1");
		$this->assert_equal($this->get_project_user_post_count_calc(), "1");

		$this->db->rollback();
	}

}

?>