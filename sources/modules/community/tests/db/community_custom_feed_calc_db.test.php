<?php

class community_custom_feed_calc_db_test extends base_test
{

	public function set_up()
	{
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user')");
		}
		$this->db->sql("TRUNCATE project");
		$this->db->sql("TRUNCATE community_custom_feed");
		$this->db->sql("TRUNCATE community");
		$this->db->sql("REPLACE INTO project (id, name, title) VALUES (1, 'test', 'test')");

		$this->db->sql("INSERT INTO community (id, project_id, name, title) VALUES (1, 1, 'test', 'test')");
		$this->db->sql("INSERT INTO community_custom_feed (id, name, title, community_id, descr) VALUES (1, 'test', 'test', 1, '')");
	}

	public function basic_test()
	{
		$this->db->begin();

		//check set_up
		$this->assert_db_value("community", "custom_feed_count_calc", 1, "1");
		$this->db->rollback();
	}

	public function community_custom_feed_add_test()
	{
		$this->db->begin();

		$this->db->sql("INSERT INTO community_custom_feed (id, name, title, community_id, descr) VALUES (2, 'test2', 'test2', 1, '')");

		$this->assert_db_value("community", "custom_feed_count_calc", 1, "2");
		$this->db->rollback();
	}

	public function community_custom_feed_delete_test()
	{
		$this->db->begin();

		$this->db->sql("DELETE FROM community_custom_feed WHERE id = 1");

		$this->assert_db_value("community", "custom_feed_count_calc", 1, "0");

		$this->db->rollback();
	}

}

?>