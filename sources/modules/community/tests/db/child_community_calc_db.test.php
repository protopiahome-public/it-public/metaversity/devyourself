<?php

class child_community_calc_db_test extends base_test
{

	protected $project_obj;

	/**
	 * @return community_child_save_helper
	 */
	protected function get_helper($parent_id, $child_id)
	{
		$this->mcache->flush();
		return new community_child_save_helper(new community_obj($parent_id, $this->project_obj), new community_obj($child_id, $this->project_obj));
	}

	public function set_up()
	{
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user')");
		}
		$this->db->sql("TRUNCATE project");
		$this->db->sql("TRUNCATE community");
		$this->db->sql("REPLACE INTO project (id, name, title) VALUES (1, 'test', 'test')");

		$this->mcache->flush();
		$this->project_obj = project_obj::instance(1);
		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (1, 'test', 'test', 1)");
		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (2, 'test2', 'test', 1)");
		$helper = $this->get_helper(1, 2);
		$helper->add_child();
		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (3, 'test3', 'test', 1)");
		$helper = $this->get_helper(1, 3);
		$helper->add_child();
		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (4, 'test4', 'test', 1)");
		$helper = $this->get_helper(1, 4);
		$helper->add_pretender();
		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (5, 'test5', 'test', 1)");
	}

	public function basic_test()
	{
		$this->db->begin();

		//check set_up
		$this->assert_db_value("community", "child_community_count_calc", 1, "2");
		$this->assert_db_value("community", "child_community_count_calc", 2, "0");
		$this->assert_db_value("community", "child_community_count_calc", 3, "0");
		$this->assert_db_value("community", "child_community_count_calc", 4, "0");
		$this->assert_db_value("community", "child_community_count_calc", 5, "0");
		$this->db->rollback();
	}

	public function community_add_test()
	{
		$this->db->begin();

		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (6, 'test6', 'test', 1)");
		$helper = $this->get_helper(1, 6);
		$helper->add_child();

		$this->assert_db_value("community", "child_community_count_calc", 1, "3");
		$this->assert_db_value("community", "child_community_count_calc", 2, "0");
		$this->assert_db_value("community", "child_community_count_calc", 3, "0");
		$this->assert_db_value("community", "child_community_count_calc", 4, "0");
		$this->assert_db_value("community", "child_community_count_calc", 5, "0");

		$this->db->rollback();
	}

	public function community_delete_and_restore_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE community SET is_deleted = 1 WHERE id = 2");
		$helper = $this->get_helper(1, 2);
		$helper->delete_child();

		$this->assert_db_value("community", "child_community_count_calc", 1, "1");
		$this->assert_db_value("community", "child_community_count_calc", 2, "0");
		$this->assert_db_value("community", "child_community_count_calc", 3, "0");
		$this->assert_db_value("community", "child_community_count_calc", 4, "0");
		$this->assert_db_value("community", "child_community_count_calc", 5, "0");

		$this->db->sql("UPDATE community SET is_deleted = 0 WHERE id = 2");
		$helper = $this->get_helper(1, 2);
		$helper->add_child();

		$this->assert_db_value("community", "child_community_count_calc", 1, "2");
		$this->assert_db_value("community", "child_community_count_calc", 2, "0");
		$this->assert_db_value("community", "child_community_count_calc", 3, "0");
		$this->assert_db_value("community", "child_community_count_calc", 4, "0");
		$this->assert_db_value("community", "child_community_count_calc", 5, "0");

		$this->db->rollback();
	}

	public function community_connect_test()
	{
		$this->db->begin();

		$helper = $this->get_helper(1, 5);
		$helper->add_child();

		$this->assert_db_value("community", "child_community_count_calc", 1, "3");
		$this->assert_db_value("community", "child_community_count_calc", 2, "0");
		$this->assert_db_value("community", "child_community_count_calc", 3, "0");
		$this->assert_db_value("community", "child_community_count_calc", 4, "0");
		$this->assert_db_value("community", "child_community_count_calc", 5, "0");

		$this->db->rollback();
	}

	public function community_disconnect_test()
	{
		$this->db->begin();

		$helper = $this->get_helper(1, 2);
		$helper->delete_child();

		$this->assert_db_value("community", "child_community_count_calc", 1, "1");
		$this->assert_db_value("community", "child_community_count_calc", 2, "0");
		$this->assert_db_value("community", "child_community_count_calc", 3, "0");
		$this->assert_db_value("community", "child_community_count_calc", 4, "0");
		$this->assert_db_value("community", "child_community_count_calc", 5, "0");

		$this->db->rollback();
	}

	public function community_accept_test()
	{
		$this->db->begin();

		$helper = $this->get_helper(1, 4);
		$helper->add_child();

		$this->assert_db_value("community", "child_community_count_calc", 1, "3");
		$this->assert_db_value("community", "child_community_count_calc", 2, "0");
		$this->assert_db_value("community", "child_community_count_calc", 3, "0");
		$this->assert_db_value("community", "child_community_count_calc", 4, "0");
		$this->assert_db_value("community", "child_community_count_calc", 5, "0");

		$this->db->rollback();
	}

	public function community_move_from_test()
	{
		$this->db->begin();

		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (6, 'test6', 'test', 1)");
		$helper = $this->get_helper(5, 6);
		$helper->add_child();
		$helper = $this->get_helper(5, 6);
		$helper->delete_child();
		$helper = $this->get_helper(1, 6);
		$helper->add_child();

		$this->assert_db_value("community", "child_community_count_calc", 1, "3");
		$this->assert_db_value("community", "child_community_count_calc", 2, "0");
		$this->assert_db_value("community", "child_community_count_calc", 3, "0");
		$this->assert_db_value("community", "child_community_count_calc", 4, "0");
		$this->assert_db_value("community", "child_community_count_calc", 5, "0");
		$this->assert_db_value("community", "child_community_count_calc", 6, "0");

		$this->db->rollback();
	}

	public function community_move_to_test()
	{
		$this->db->begin();

		$helper = $this->get_helper(1, 2);
		$helper->delete_child();
		$helper = $this->get_helper(5, 2);
		$helper->add_child();

		$this->assert_db_value("community", "child_community_count_calc", 1, "1");
		$this->assert_db_value("community", "child_community_count_calc", 2, "0");
		$this->assert_db_value("community", "child_community_count_calc", 3, "0");
		$this->assert_db_value("community", "child_community_count_calc", 4, "0");
		$this->assert_db_value("community", "child_community_count_calc", 5, "1");

		$this->db->rollback();
	}

}

?>