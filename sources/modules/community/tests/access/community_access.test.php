<?php

class community_access_test extends base_test
{

	private $log_path;

	public function set_up()
	{
		$this->log_path = PATH_LOG . "/community_access.log";
		$this->db->begin();
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user')");
		}
		$this->db->sql("REPLACE INTO project (id, name, title, communities_are_on) VALUES (1, 'test', 'test', 1)");
		$this->db->sql("REPLACE INTO community (id, name, title, project_id) VALUES (1, 'test', 'test', 1)");
		$this->db->sql("REPLACE INTO community_user_link (community_id, user_id, status) VALUES (1, 1, 'member')");
		$this->db->sql("DELETE FROM user_project_link WHERE user_id = 1 AND project_id = 1");
		$this->db->sql("DELETE FROM project_admin WHERE user_id = 1 AND project_id = 1");
		$this->db->sql("DELETE FROM community_admin WHERE user_id = 1 AND community_id = 1");
		$this->user->set_user_id(1);
	}

	public function basic_test()
	{
		$this->mcache->flush();
		$project_obj = project_obj::instance(1);
		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_true($access->has_member_rights());
	}

	public function basic_cache_test()
	{
		$this->mcache->flush();
		$project_obj = project_obj::instance(1);
		
		$query_count = $this->db->debug_get_query_count();
		
		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_identical($this->db->debug_get_query_count() - $query_count, 3);

		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_identical($this->db->debug_get_query_count() - $query_count, 3);
	}

	public function admin_test()
	{
		$this->db->sql("REPLACE INTO community_user_link (community_id, user_id, status) VALUES (1, 1, 'admin')");
		$this->db->sql("REPLACE INTO community_admin (community_id, user_id, is_admin) VALUES (1, 1, 1)");
		$this->mcache->flush();

		$project_obj = project_obj::instance(1);
		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_true($access->has_admin_rights());
	}

	public function admin_second_table_test()
	{
		$this->db->sql("REPLACE INTO community_user_link (community_id, user_id, status) VALUES (1, 1, 'admin')");
		$this->db->sql("REPLACE INTO community_admin (community_id, user_id, is_admin) VALUES (1, 1, 0)");
		$this->mcache->flush();

		$project_obj = project_obj::instance(1);
		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_false($access->has_admin_rights());

		$this->db->sql("REPLACE INTO community_user_link (community_id, user_id, status) VALUES (1, 1, 'member')");
		$this->db->sql("REPLACE INTO community_admin (community_id, user_id, is_admin) VALUES (1, 1, 1)");
		$this->mcache->flush();

		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_false($access->has_admin_rights());
	}

	public function admin_cache_test()
	{
		$this->db->sql("REPLACE INTO community_user_link (community_id, user_id, status) VALUES (1, 1, 'admin')");
		$this->db->sql("REPLACE INTO community_admin (community_id, user_id, is_admin) VALUES (1, 1, 1)");
		$this->mcache->flush();

		$project_obj = project_obj::instance(1);

		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_true($access->has_admin_rights());

		$query_count = $this->db->debug_get_query_count();
		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_true($access->has_admin_rights());
		$this->assert_identical($this->db->debug_get_query_count() - $query_count, 0);
	}

	public function project_admin_test()
	{
		$this->db->sql("DELETE FROM community_user_link WHERE community_id = 1 AND user_id = 1");
		$this->db->sql("DELETE FROM community_admin WHERE user_id = 1 AND community_id = 1");
		$project_access_save = new project_access_save(project_obj::instance(1), 1);

		$project_access_save->delete_member();
		$this->mcache->flush();
		$project_obj = project_obj::instance(1);
		
		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_false($access->has_member_rights());
		$this->assert_false($access->has_admin_rights());
		$this->assert_false($access->is_member());

		$project_access_save->add_moderator();
		$project_access_save = new project_access_save(project_obj::instance(1), 1);
		$project_access_save->set_moderator_rights(array("is_community_moderator" => 1));
		$this->mcache->flush();
		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_true($access->has_admin_rights());
		$this->assert_false($access->is_member());

		$project_access_save->delete_member();
		$project_access_save->add_admin();
		$this->mcache->flush();
		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_true($access->has_admin_rights());
		$this->assert_false($access->is_member());

		$project_access_save->delete_member();
	}

	public function super_admin_test()
	{
		$this->db->sql("DELETE FROM community_user_link WHERE community_id = 1 AND user_id = 1");
		$this->db->sql("DELETE FROM community_admin WHERE user_id = 1 AND community_id = 1");
		$this->mcache->flush();

		$this->user->set_user_data(array("is_admin" => "1"));
		$project_obj = project_obj::instance(1);
		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_false($access->is_member());
		$this->assert_true($access->has_admin_rights());
		$this->user->set_user_data(null);
	}

	public function not_admin_test()
	{
		$this->db->sql("REPLACE INTO community_user_link (community_id, user_id, status) VALUES (1, 1, 'member')");
		$this->db->sql("DELETE FROM community_admin WHERE user_id = 1 AND community_id = 1");
		$this->mcache->flush();

		$project_obj = project_obj::instance(1);
		$access = new community_access(new community_obj(1, $project_obj), new project_access($project_obj, 1), 1);
		$this->assert_false($access->has_admin_rights());
	}

//	public function unexisted_community_test()
//	{
//		$this->db->sql("DELETE FROM community WHERE id = 99999");
//		$this->mcache->flush();
//
//		$project_obj = project_obj::instance(1);
//		$community_obj = new community_obj(99999, $project_obj);
//		$this->assert_null($community_obj->get_access());
//	}

}

?>