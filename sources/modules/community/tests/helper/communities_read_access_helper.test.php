<?php

require_once PATH_MODULES . "/community/autoloaded/access/community.access_maper.php";

class communities_read_access_helper_test extends base_test
{

	/**
	 *
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @return communities_read_access_helper
	 */
	protected function get_helper()
	{
		return new communities_read_access_helper($this->project_obj);
	}

	public function set_up()
	{
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user')");
		}
		$this->db->sql("REPLACE INTO project (id, name, title) VALUES (1, 'test', 'test')");
		$this->db->sql("REPLACE INTO user_project_link (project_id, user_id, status) VALUES (1, 1, 'member')");
		$this->db->sql("DELETE FROM project_admin WHERE project_id = 1 AND user_id = 1");

		$this->project_obj = project_obj::instance(1);
		$this->user->set_user_id(1);
		$this->mcache->flush();
	}

	public function user_communities_guest_test()
	{
		$this->user->set_user_id(0);
		$helper = $this->get_helper();

		$all_communities_access_levels = array(
			1 => ACCESS_LEVEL_GUEST,
			2 => ACCESS_LEVEL_GUEST,
			3 => ACCESS_LEVEL_USER,
			4 => ACCESS_LEVEL_MEMBER,
			5 => ACCESS_LEVEL_USER,
		);
		$user_communities_access_levels = array(
		);
		$result_communities_right = array(1, 2);
		$result_communities = $helper->_test_calc_user_communities($all_communities_access_levels, $user_communities_access_levels);
		$this->assert_identical($result_communities, $result_communities_right);
		$this->user->set_user_id(1);
	}

	public function user_communities_community_shift_test()
	{
		$helper = $this->get_helper();

		$all_communities_access_levels = array(
			3 => ACCESS_LEVEL_USER,
			4 => ACCESS_LEVEL_MEMBER,
			5 => ACCESS_LEVEL_USER,
		);
		$user_communities_access_levels = array(
			1 => ACCESS_LEVEL_USER,
			2 => ACCESS_LEVEL_USER,
			3 => ACCESS_LEVEL_USER,
			4 => ACCESS_LEVEL_MEMBER,
			5 => ACCESS_LEVEL_USER,
		);
		$result_communities_right = array(3, 4, 5);
		$result_communities = $helper->_test_calc_user_communities($all_communities_access_levels, $user_communities_access_levels);
		$this->assert_identical($result_communities, $result_communities_right);
	}

	public function user_communities_user_shift_test()
	{
		$helper = $this->get_helper();

		$all_communities_access_levels = array(
			1 => ACCESS_LEVEL_USER,
			2 => ACCESS_LEVEL_USER,
			3 => ACCESS_LEVEL_USER,
			4 => ACCESS_LEVEL_MEMBER,
			5 => ACCESS_LEVEL_USER,
		);
		$user_communities_access_levels = array(
			4 => ACCESS_LEVEL_MEMBER,
			5 => ACCESS_LEVEL_USER,
		);
		$result_communities_right = array(1, 2, 3, 4, 5);
		$result_communities = $helper->_test_calc_user_communities($all_communities_access_levels, $user_communities_access_levels);
		$this->assert_identical($result_communities, $result_communities_right);
	}

	public function user_communities_project_member_test()
	{
		$helper = $this->get_helper();

		$all_communities_access_levels = array(
			1 => ACCESS_LEVEL_MEMBER,
			2 => ACCESS_LEVEL_PROJECT_MEMBER,
			3 => ACCESS_LEVEL_MEMBER,
			4 => ACCESS_LEVEL_MEMBER,
			5 => ACCESS_LEVEL_MEMBER,
		);
		$user_communities_access_levels = array(
			5 => ACCESS_LEVEL_MEMBER,
		);
		$result_communities_right = array(2, 5);
		$result_communities = $helper->_test_calc_user_communities($all_communities_access_levels, $user_communities_access_levels);
		$this->assert_identical($result_communities, $result_communities_right);
	}

	public function calc_user_communities_extra_test()
	{
		$all_communities_access_levels = array(
			1 => ACCESS_LEVEL_GUEST,
			2 => ACCESS_LEVEL_GUEST,
			3 => ACCESS_LEVEL_GUEST,
			9 => ACCESS_LEVEL_GUEST,
			10 => ACCESS_LEVEL_ADMIN,
			13 => ACCESS_LEVEL_GUEST,
			14 => ACCESS_LEVEL_GUEST,
			15 => ACCESS_LEVEL_GUEST,
			16 => ACCESS_LEVEL_GUEST,
			17 => ACCESS_LEVEL_GUEST,
			18 => ACCESS_LEVEL_GUEST,
			19 => ACCESS_LEVEL_GUEST,
			20 => ACCESS_LEVEL_GUEST,
			21 => ACCESS_LEVEL_GUEST,
			22 => ACCESS_LEVEL_GUEST,
			23 => ACCESS_LEVEL_GUEST,
			24 => ACCESS_LEVEL_GUEST,
			25 => ACCESS_LEVEL_GUEST,
			26 => ACCESS_LEVEL_GUEST,
			27 => ACCESS_LEVEL_GUEST,
			28 => ACCESS_LEVEL_GUEST,
			29 => ACCESS_LEVEL_GUEST,
			30 => ACCESS_LEVEL_GUEST,
			31 => ACCESS_LEVEL_GUEST,
			32 => ACCESS_LEVEL_GUEST,
			33 => ACCESS_LEVEL_GUEST,
			34 => ACCESS_LEVEL_GUEST,
			35 => ACCESS_LEVEL_GUEST,
			37 => ACCESS_LEVEL_GUEST,
			39 => ACCESS_LEVEL_GUEST,
			43 => ACCESS_LEVEL_GUEST,
			44 => ACCESS_LEVEL_GUEST,
			45 => ACCESS_LEVEL_GUEST,
			46 => ACCESS_LEVEL_GUEST,
			47 => ACCESS_LEVEL_GUEST,
		);

		$user_communities_access_levels = array(
			9 => ACCESS_LEVEL_MEMBER,
			10 => ACCESS_LEVEL_MEMBER,
			33 => ACCESS_LEVEL_DELETED,
			36 => ACCESS_LEVEL_MEMBER,
			37 => ACCESS_LEVEL_MEMBER,
		);

		$result_communities_right = array(
			1, 2, 3, 9, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 37, 39, 43, 44, 45, 46, 47
		);

		$helper = $this->get_helper();
		$result_communities = $helper->_test_calc_user_communities($all_communities_access_levels, $user_communities_access_levels);
		$this->assert_identical($result_communities, $result_communities_right);
	}

	public function user_communities_intersection_test()
	{
		$helper = $this->get_helper();

		$communities_ids = array(2, 3, 5, 6, 7);
		$user_communities_ids = array(1, 3, 6, 7, 8);
		$result_communities_right = array(3, 6, 7);
		$result_communities = $helper->_test_calc_user_communities_intersection($communities_ids, $user_communities_ids);
		$this->assert_identical($result_communities, $result_communities_right);
	}

	public function user_sources_in_custom_feed_test()
	{
		$helper = $this->get_helper();

		$custom_feed_sources = array(
			1 => 3,
			2 => 5,
			3 => 6,
			4 => 7,
			5 => 8,
		);
		$user_communities_ids = array(3, 5, 6);
		$result_feed_sources_right = array(
			3 => 6,
			5 => 8,
		);
		$result_feed_sources = $helper->_test_calc_user_sources_in_custom_feed($custom_feed_sources, $user_communities_ids);
		$this->assert_identical($result_feed_sources, $result_feed_sources_right);
	}

}

?>