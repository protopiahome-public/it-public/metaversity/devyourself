<?php

class project_vector_self_xml_page extends base_xml_ctrl
{

	protected $options = array(
		"-" => "—",
		"0" => "Не проявлена",
		"1" => "Склонность",
		"2" => "Способность",
		"3" => "Компетенция"
	);
	private $project_id;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		/* Check that vector is on */
		$vector_config = vector_info_math::get_project_vector_config($this->project_id);
		if (!$vector_config)
		{
			$this->set_error_404();
			return;
		}

		/* Check user rights */
		$user_id = $this->user->get_user_id();
		if (!$user_id)
		{
			$this->set_error_403();
			return;
		}

		/* Init vector */
		$competence_set_id = $vector_config["vector_competence_set_id"];
		$vector_data = null;
		$future_data = array();
		$vector_info_struct = vector_info_math::get_project_vector_info($user_id, $this->project_id);
		if ($vector_info_struct)
		{
			$vector_data = new vector_data_math($vector_info_struct["id"], $user_id, $vector_info_struct["competence_set_id"]);
			$future_data = $vector_data->get_future_data(true);
		}

		/* Basic XMLs */
		$this->xml_loader->add_xml(new vector_config_xml_ctrl($vector_config));
		$this->xml_loader->add_xml(new project_vector_menu_xml_ctrl($vector_config, $vector_info_struct, $vector_data, "self"));

		/* Previous step is required */
		if (!sizeof($future_data))
		{
			return "<{$this->name} step_required=\"1\" />";
		}

		/* Other XMLs */
		$this->xml_loader->add_xml(new competence_set_competences_xml_page($competence_set_id));
		$this->xml_loader->add_xml(new vector_xml_ctrl($vector_info_struct, $vector_data, VECTOR_DISPLAY_FUTURE_COMPETENCES | VECTOR_DISPLAY_SELF));

		/* Output data (options) */
		$xdom = xdom::create($this->name);
		$options_node = $xdom->create_child_node("options");
		foreach ($this->options as $mark => $mark_title)
		{
			$options_node->create_child_node("option")
				->set_attr("mark", $mark)
				->set_attr("mark_title", $mark_title);
		}
		return $xdom->get_xml(true);
	}

}

?>