<?php

class project_vector_future_xml_page extends base_easy_xml_ctrl
{

	private $project_id;

	/**
	 * @var sort_easy_processor
	 */
	private $sort_processor;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}

	public function init()
	{
		$this->sort_processor = new sort_easy_processor();
		$this->sort_processor->add_order("name", "u.first_name ASC, u.last_name ASC", "u.first_name DESC, u.last_name DESC");
		$this->sort_processor->add_order("marks", "u.total_mark_count_calc DESC", "u.total_mark_count_calc ASC", true);
		$this->sort_processor->add_order("add_time", "u.add_time DESC", "u.add_time ASC");
		$this->add_easy_processor($this->sort_processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		/* Check that vector is on */
		$vector_config = vector_info_math::get_project_vector_config($this->project_id);
		if (!$vector_config)
		{
			$this->set_error_404();
			return;
		}

		/* Check user rights */
		$user_id = $this->user->get_user_id();
		if (!$user_id)
		{
			$this->set_error_403();
			return;
		}

		/* Init vector */
		$competence_set_id = $vector_config["vector_competence_set_id"];
		$vector_info_struct = vector_info_math::get_project_vector_info($user_id, $this->project_id);
		if ($vector_info_struct)
		{
			$vector_data = new vector_data_math($vector_info_struct["id"], $user_id, $vector_info_struct["competence_set_id"]);
		}
		else
		{
			$vector_info_struct = array(
				"fake_vector" => true,
				"competence_set_id" => $competence_set_id
			);
			$vector_data = new vector_data_math(0, 0, $competence_set_id);
		}

		/* Basic XMLs */
		$this->xml_loader->add_xml(new vector_config_xml_ctrl($vector_config));
		$this->xml_loader->add_xml(new project_vector_menu_xml_ctrl($vector_config, $vector_info_struct, $vector_data, "future"));

		/* Math */
		
		// Vector data
		$future_data = $vector_data->get_future_data();
		$professiogram_data = $vector_data->get_possible_professiograms_data();

		// Results
		$results_math = new results_math($competence_set_id, array($user_id));
		$results = $results_math->get_results();

		if ($results)
		{
			// Filters output
			$filter_main = $results_math->get_filter_main();
			$filter_raters_real = $results_math->get_filter_raters_real();
			$this->xml_loader->add_xml(new filter_xml_ctrl($filter_main, $filter_raters_real, $results_math->get_used_mark_count(), $results_math->get_used_rater_count()));

			// Match calculation
			$professiogram_match_math = new rate_match_math($results, $professiogram_data);
			$rating = $professiogram_match_math->get_rate_rating_for_user($user_id);
		}

		/* Building and sorting data for XML */
		$this->sort_processor->add_order("title", "", "", true);
		$sort = isset($_GET["sort"]) ? $_GET["sort"] : "";
		if ($results)
		{
			$this->sort_processor->add_order("match", "", "");
		}
		$this->sort_processor->load_order();
		$this->data = array();
		if ($this->sort_processor->get_order() === "match")
		{
			foreach ($rating as $professiogram_id => $match)
			{
				$this->data[$professiogram_id] = array(
					"title" => $future_data[$professiogram_id]["title"],
					"checked" => $future_data[$professiogram_id]["checked"],
					"match" => $match
				);
			}
		}
		else
		{
			foreach ($future_data as $professiogram_id => $professiogram_data)
			{
				$this->data[$professiogram_id] = array(
					"title" => $professiogram_data["title"],
					"checked" => $professiogram_data["checked"]
				);
				if ($results)
				{
					$this->data[$professiogram_id]["match"] = $rating[$professiogram_id];
				}
			}
		}
		if ($this->sort_processor->get_back())
		{
			$this->data = array_reverse($this->data, true);
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		foreach ($this->data as $professiogram_id => $professiogram_data)
		{
			$professiogram_node = $xdom->create_child_node("professiogram")
				->set_attr("id", $professiogram_id)
				->set_attr("title", $professiogram_data["title"])
				->set_attr("checked", $professiogram_data["checked"]);
			if (isset($professiogram_data["match"]))
			{
				$professiogram_node->set_attr("match", $professiogram_data["match"]);
			}
		}
	}

}

?>