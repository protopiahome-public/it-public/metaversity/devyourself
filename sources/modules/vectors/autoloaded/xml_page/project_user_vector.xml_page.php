<?php

class project_user_vector_xml_page extends base_xml_ctrl
{

	private $project_id;
	private $user_id;
	private $output_type;

	public function __construct($project_id, $user_id, $output_type)
	{
		$this->project_id = $project_id;
		$this->user_id = $user_id;
		$this->output_type = $output_type;

		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		$is_rose = $this->output_type == "rose";

		/* Check that vector is on */

		$vector_config = vector_info_math::get_project_vector_config($this->project_id);
		if (!$vector_config)
		{
			$this->set_error_404();
			return false;
		}

		/* User access check */

		if (is_null($this->user_id))
		{
			$this->user_id = $this->user->get_user_id();
		}
		if (!$this->user_id)
		{
			$this->set_error_403();
			return false;
		}

		/* XML initial */

		$xdom = xdom::create("project_user_vector")->set_attr("project_id", $this->project_id)->set_attr("user_id", $this->user_id)->set_attr("output_type", $this->output_type);

		/* Math */

		// Vector info
		$vector_info_struct = vector_info_math::get_project_vector_info($this->user_id, $this->project_id);
		if (!$vector_info_struct)
		{
			return $xdom->get_xml(true);
		}

		// Vector data
		$competence_set_id = $vector_info_struct["competence_set_id"];
		$vector_data = new vector_data_math($vector_info_struct["id"], $this->user_id, $competence_set_id, false);
		$future_data = $vector_data->get_future_data(true, true);
		$future_competences_data = $vector_data->get_future_competences_data();
		$future_competence_id_array = array_keys($future_competences_data);
		$focus_data = $vector_data->get_focus_data();
		$focus_competence_id_array = array_keys($vector_data->get_focus_data());
		$self_data = $vector_data->get_self_data();

		// Results
		$calculate_competence_id_array = array_unique(array_merge($future_competence_id_array, $focus_competence_id_array));
		$results_math = new results_math($competence_set_id, array($this->user_id), $calculate_competence_id_array);
		$results = $results_math->get_results();

		// Filters
		$filter_main = $results_math->get_filter_main();
		$filter_raters_real = $results_math->get_filter_raters_real();

		// Match calculation
		if (!$is_rose)
		{
			$professiogram_matches = array();
			$professiograms_data = $vector_data->get_possible_professiograms_data();
			$professiogram_match_math = new rate_match_math($results, $professiograms_data);
			foreach ($professiograms_data as $index => $professiogram_data)
			{
				if (isset($future_data[$professiogram_data["id"]]))
				{
					$match = $professiogram_match_math->get_user_rate_match($this->user_id, $index);
					$professiogram_matches[$professiogram_data["id"]] = array(
						"title" => $professiogram_data["title"],
						"match" => $match
					);
				}
			}
		}

		/* Rose output */

		if ($is_rose)
		{
			if (GET("img-focus") == 1)
			{
				if (!sizeof($focus_competence_id_array))
				{
					$this->set_error_404();
					return false;
				}

				$need_values = array();
				foreach ($focus_competence_id_array as $competence_id)
				{
					$need_values[] = isset($future_competences_data[$competence_id]) ? $future_competences_data[$competence_id] : 0;
				}

				rose_xml_helper::rose_output($focus_competence_id_array, $need_values, $results, $self_data, array(), $this->user_id);
				return true;
			}
			elseif (GET("img-future") == 1)
			{
				if (!sizeof($future_competence_id_array))
				{
					$this->set_error_404();
					return false;
				}

				$need_values = array();
				foreach ($focus_competence_id_array as $competence_id)
				{
					$need_values[] = isset($future_competences_data[$competence_id]) ? $future_competences_data[$competence_id] : 0;
				}

				rose_xml_helper::rose_output($future_competence_id_array, array_values($future_competences_data), $results, $self_data, $focus_competence_id_array, $this->user_id);
				return true;
			}
		}

		/* Additional controls */

		$this->xml_loader->add_xml(new filter_xml_ctrl($filter_main, $filter_raters_real, $results_math->get_used_mark_count(), $results_math->get_used_rater_count()));
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($competence_set_id));
		$this->xml_loader->add_xml(new vector_xml_ctrl($vector_info_struct, $vector_data, VECTOR_DISPLAY_FUTURE | VECTOR_DISPLAY_SELF | VECTOR_DISPLAY_FUTURE_COMPETENCES | VECTOR_DISPLAY_SELF_STAT | VECTOR_DISPLAY_FOCUS | ($is_rose ? 0 : VECTOR_DISPLAY_FUTURE_MATCH_USING_SELF | VECTOR_DISPLAY_FUTURE_COMPETENCES_DETAILED)));
		$this->xml_loader->add_xml(new vector_config_xml_ctrl($vector_config));

		/* Output */

		// Rose data
		if ($is_rose)
		{
			rose_xml_helper::rose_xml_output($xdom, $focus_competence_id_array, "focus_rose_competences");
			rose_xml_helper::rose_xml_output($xdom, $future_competence_id_array, "future_rose_competences");
		}

		results_xml_helper::export_results($xdom, $filter_main, $results_math, $results, $this->user_id, $this->output_type, true, true, $vector_info_struct["calculations_base"]);

		if (!$is_rose)
		{
			// Table order
			$xdom->create_child_node("sort")->set_attr("sort", "title");

			// Professiogram matches
			$professiograms_node = $xdom->create_child_node("professiograms");
			$i = 1;
			foreach ($professiogram_matches as $professiogram_id => $professiogram_data)
			{
				$professiograms_node->create_child_node("professiogram")->set_attr("id", $professiogram_id)->set_attr("title", $professiogram_data["title"])->set_attr("match", $professiogram_data["match"])->set_attr("index", $i++);
			}
		}

		return $xdom->get_xml(true);
	}

}

?>