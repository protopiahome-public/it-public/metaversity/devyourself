<?php

class project_vector_xml_page extends base_xml_ctrl
{
	
	private $project_id;
	
	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}
	
	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		/* Check that vector is on */
		
		$vector_config = vector_info_math::get_project_vector_config($this->project_id);
		if (!$vector_config)
		{
			$this->set_error_404();
			return false;
		}
		
		/* Redirect */
		
		$redirect_is_set = false;
		if ($user_id = $this->user->get_user_id())
		{
			$vector_info_struct = vector_info_math::get_project_vector_info($user_id, $this->project_id);
			if ($vector_info_struct)
			{
				$this->set_redirect_url($this->request->get_full_prefix() . $this->request->get_folders_string() . "/my/");
				$redirect_is_set = true;
			}
		}
		if (!$redirect_is_set)
		{
			$this->set_redirect_url($this->request->get_full_prefix() . $this->request->get_folders_string() . "/intro/");
		}
		
		return "<project_vector />";
	}

}

?>