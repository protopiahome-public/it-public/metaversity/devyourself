<?php

class project_vector_focus_xml_page extends base_xml_ctrl
{

	private $project_id;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		/* Check that vector is on */
		$vector_config = vector_info_math::get_project_vector_config($this->project_id);
		if (!$vector_config)
		{
			$this->set_error_404();
			return;
		}

		/* Check user rights */
		$user_id = $this->user->get_user_id();
		if (!$user_id)
		{
			$this->set_error_403();
			return;
		}

		/* Init vector */
		$competence_set_id = $vector_config["vector_competence_set_id"];
		$vector_data = null;
		$future_data = array();
		$vector_info_struct = vector_info_math::get_project_vector_info($user_id, $this->project_id);
		if ($vector_info_struct)
		{
			$vector_data = new vector_data_math($vector_info_struct["id"], $user_id, $competence_set_id);
			$future_data = $vector_data->get_future_data(true, true);
		}

		/* Basic XMLs */
		$this->xml_loader->add_xml(new vector_config_xml_ctrl($vector_config));
		$this->xml_loader->add_xml(new project_vector_menu_xml_ctrl($vector_config, $vector_info_struct, $vector_data, "focus"));

		/* Previous step is required */
		if (!sizeof($future_data))
		{
			return "<{$this->name} step_required=\"1\" />";
		}
		if (!$vector_data->is_self_complete())
		{
			return "<{$this->name} step_required=\"2\" />";
		}

		/* Math */

		// Vector data
		$future_competences_data = $vector_data->get_future_competences_data();
		$focus_data = $vector_data->get_focus_data();
		$focus_competence_id_array = array_keys($vector_data->get_focus_data());
		$self_data = $vector_data->get_self_data();

		// Results
		$calculate_competence_set_id_array = array_unique(array_merge(array_keys($future_competences_data), $focus_competence_id_array));
		$results_math = new results_math($competence_set_id, array($user_id), $calculate_competence_set_id_array);
		$results = $results_math->get_results();

		// Filters
		$filter_main = $results_math->get_filter_main();
		$filter_raters_real = $results_math->get_filter_raters_real();

		// Match calculation
		$professiogram_matches = array();
		$professiograms_data = $vector_data->get_possible_professiograms_data();
		$professiogram_match_math = new rate_match_math($results, $professiograms_data);
		foreach ($professiograms_data as $index => $professiogram_data)
		{
			if (isset($future_data[$professiogram_data["id"]]))
			{
				$match = $professiogram_match_math->get_user_rate_match($user_id, $index);
				$professiogram_matches[$professiogram_data["id"]] = array(
					"title" => $professiogram_data["title"],
					"match" => $match
				);
			}
		}

		/* Other XMLs */
		$this->xml_loader->add_xml(new filter_xml_ctrl($filter_main, $filter_raters_real, $results_math->get_used_mark_count(), $results_math->get_used_rater_count()));
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($competence_set_id));
		$this->xml_loader->add_xml(new vector_xml_ctrl($vector_info_struct, $vector_data, VECTOR_DISPLAY_FUTURE | VECTOR_DISPLAY_SELF | VECTOR_DISPLAY_FUTURE_COMPETENCES | VECTOR_DISPLAY_SELF_STAT | VECTOR_DISPLAY_FOCUS | VECTOR_DISPLAY_FUTURE_MATCH_USING_SELF | VECTOR_DISPLAY_FUTURE_COMPETENCES_DETAILED));

		/* Output */
		$xdom = xdom::create($this->name);
		results_xml_helper::export_results($xdom, $filter_main, $results_math, $results, $user_id, "detailed", true, true, $vector_info_struct["calculations_base"]);

		// Professiogram matches
		$professiograms_node = $xdom->create_child_node("professiograms");
		$i = 1;
		foreach ($professiogram_matches as $professiogram_id => $professiogram_data)
		{
			$professiograms_node->create_child_node("professiogram")->set_attr("id", $professiogram_id)->set_attr("title", $professiogram_data["title"])->set_attr("match", $professiogram_data["match"])->set_attr("index", $i++);
		}

		return $xdom->get_xml(true);
	}

}

?>