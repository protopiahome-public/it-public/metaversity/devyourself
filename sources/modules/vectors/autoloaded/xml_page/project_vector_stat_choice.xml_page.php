<?php

class project_vector_stat_choice_xml_page extends base_project_user_stat_choice_xml_ctrl
{

	protected $resource_names = array(
		"event" => array(
			"title" => "События",
			"not_finished_title" => "Запланированные события",
			"finished_title" => "Посещённые события",
			"empty_note" => "Либо вы не посещаете события, либо у событий не отмечены компетенции.",
			"check_on" => "events_are_on",
		),
		"material" => array(
			"title" => "Материалы",
			"not_finished_title" => "Запланированные к изучению",
			"finished_title" => "Изученные",
			"empty_note" => "Либо вы не изучаете материалы, либо у материалов не отмечены компетенции.",
			"check_on" => "materials_are_on",
		),
	);

}

?>