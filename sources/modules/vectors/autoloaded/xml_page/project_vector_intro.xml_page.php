<?php

class project_vector_intro_xml_page extends base_xml_ctrl
{
	
	private $project_id;
	
	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}
	
	public function get_xml()
	{
		global $config;
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		/* Check that vector is on */
		
		$vector_config = vector_info_math::get_project_vector_config($this->project_id);
		if (!$vector_config)
		{
			$this->set_error_404();
			return false;
		}
		
		$this->xml_loader->add_xml(new vector_config_xml_ctrl($vector_config));
		
		return "<project_vector_intro />";
	}

}

?>