<?php

final class vectors_taxonomy extends base_taxonomy
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, project_obj $project_obj)
	{
		$this->project_obj = $project_obj;
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		$p = $this->get_parts_relative();

		$project_obj = $this->project_obj;
		$project_id = $project_obj->get_id();
		$project_access = $project_obj->get_access();
		
		//p/<project_name>/vector/...
			if (!$project_obj->vector_is_on())
			{
				$this->xml_loader->set_error_404();
				$this->xml_loader->set_error_404_xslt("project_module_404", "project");
			}
			elseif ($p[1] === null)
			{
				//p/<project_name>/vector/ --> intro/ or my/
				$this->xml_loader->add_xml(new project_vector_xml_page($project_id));
				$this->xml_loader->set_error_404_xslt("project_vector_404", "vectors");
			}
			elseif ($p[1] === "intro" and $p[2] === null)
			{
				//p/<project_name>/vector/intro/
				$this->xml_loader->add_xml_with_xslt(new project_vector_intro_xml_page($project_id), "project_vector_404");
			}
			elseif ($p[1] === "my" and $p[2] === null)
			{
				//p/<project_name>/vector/my/
				$this->xml_loader->add_xml(new project_user_vector_xml_page($project_id, null, "rose"));
				$this->xml_loader->add_xslt("project_vector_my_rose", "vectors");
				$this->xml_loader->set_error_403_xslt("project_vector_403", "vectors");
				$this->xml_loader->set_error_404_xslt("project_vector_404", "vectors");
			}
			elseif ($p[1] === "my" and $p[2] === "detailed" and $p[3] === null)
			{
				//p/<project_name>/vector/my/detailed/
				$this->xml_loader->add_xml(new project_user_vector_xml_page($project_id, null, "detailed"));
				$this->xml_loader->add_xslt("project_vector_my_detailed", "vectors");
				$this->xml_loader->set_error_403_xslt("project_vector_403", "vectors");
				$this->xml_loader->set_error_404_xslt("project_vector_404", "vectors");
			}
			elseif ($p[1] === "future" and $p[2] === null)
			{
				//p/<project_name>/vector/future/
				$this->xml_loader->add_xml_with_xslt(new project_vector_future_xml_page($project_id), "project_vector_404", "project_vector_403");
			}
			elseif ($p[1] === "self" and $p[2] === null)
			{
				//p/<project_name>/vector/self/
				$this->xml_loader->add_xml_with_xslt(new project_vector_self_xml_page($project_id), "project_vector_404", "project_vector_403");
			}
			elseif ($p[1] === "focus" and $p[2] === null)
			{
				//p/<project_name>/vector/focus/
				$this->xml_loader->add_xml_with_xslt(new project_vector_focus_xml_page($project_id), "project_vector_404", "project_vector_403");
			}
			elseif ($p[1] === "recommend" and $p[2] === null)
			{
				//p/<project_name>/vector/recommend/
				$this->xml_loader->add_xml_with_xslt(new project_vector_recommend_xml_page($project_id), "project_vector_404", "project_vector_403");
			}
			elseif ($p[1] === "stat-choice" and ($p[2] === null or $p[2] === "focus" or $p[2] === "all") and $p[3] === null)
			{
				//p/<project_name>/vector/stat-choice/
				$this->xml_loader->add_xml_with_xslt(new project_vector_stat_choice_xml_page($project_id, $p[2]), "project_vector_404", "project_vector_stat_choice_403");
			}
	}

}

?>