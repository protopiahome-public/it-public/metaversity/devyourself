<?php

class vector_self_save_page extends base_save_ctrl
{
	
	// @todo check rights can_read_project()
	// @todo add mixin

	private $user_id;
	private $project_id;
	private $vector_config;
	private $vector_info;
	private $vector_id;
	private $competence_set_id;
	private $vector_data;
	private $future_competences_data;
	private $self_data;
	private $competences;
	private $self_to_add = array();
	private $self_to_change = array();
	private $self_to_delete = array();
	private $total_count = 0;
	private $something_changed = false;

	public function start()
	{
		$this->user_id = $this->user->get_user_id();
		if (!$this->user_id)
		{
			return false;
		}

		$this->project_id = POST("project_id");
		if (!is_good_id($this->project_id))
		{
			return false;
		}

		$this->vector_config = vector_info_math::get_project_vector_config($this->project_id);
		if (!$this->vector_config)
		{
			// Vector is turned off
			return false;
		}

		$this->vector_info = vector_info_math::get_project_vector_info($this->user_id, $this->project_id);
		if (!$this->vector_info)
		{
			return false;
		}
		$this->vector_id = $this->vector_info["id"];
		$this->competence_set_id = $this->vector_info["competence_set_id"];
		$this->vector_data = new vector_data_math($this->vector_id, $this->user_id, $this->competence_set_id);
		$this->future_competences_data = $this->vector_data->get_future_competences_data();
		$this->self_data = $this->vector_data->get_self_data();
		$this->competences = $this->db->fetch_column_values("SELECT competence_id as id, title FROM competence_calc WHERE competence_set_id = {$this->competence_set_id}", "title", "id");
		return true;
	}
	
	public function check()
	{
		foreach ($this->competences as $competence_id => $competence_title)
		{
			$new_value = POST("competence-" . $competence_id);
			$old_value = isset($this->self_data[$competence_id]) ? $this->self_data[$competence_id] : "-";
			if (is_null($new_value) or !in_array($new_value, array("-", "0", "1", "2", "3")))
			{
				$new_value = $old_value;
			}
			if ($new_value != "-")
			{
				$new_value = (int) $new_value;
				if (isset($this->future_competences_data[$competence_id]))
				{
					unset($this->future_competences_data[$competence_id]);
				}
				++$this->total_count;
			}
			if ($old_value !== $new_value)
			{
				if ($old_value === "-")
				{
					$this->self_to_add[$competence_id] = $new_value;
				}
				elseif ($new_value === "-")
				{
					$this->self_to_delete[$competence_id] = $new_value;
				}
				else
				{
					$this->self_to_change[$competence_id] = $new_value;
				}
			}
		}

		return true;
	}

	public function commit()
	{
		$log = "";
		foreach ($this->self_to_add as $competence_id => $new_value)
		{
			$this->db->sql("
				INSERT IGNORE INTO vector_self
					(user_id, competence_id, mark, competence_set_id, add_time)
				VALUES
					({$this->user_id}, {$competence_id}, {$new_value}, {$this->competence_set_id}, NOW())
			");
			$log .= "+|{$competence_id}|-|{$new_value}|{$this->competences[$competence_id]}\n";
			$this->something_changed = true;
		}
		foreach ($this->self_to_change as $competence_id => $new_value)
		{
			$this->db->sql("
				UPDATE vector_self
				SET mark = {$new_value}
				WHERE user_id = {$this->user_id} AND competence_id = {$competence_id}
			");
			$log .= "~|{$competence_id}|{$this->self_data[$competence_id]}|{$new_value}|{$this->competences[$competence_id]}\n";
			$this->something_changed = true;
		}
		foreach ($this->self_to_delete as $competence_id => $new_value)
		{
			$this->db->sql("
				DELETE FROM vector_self
				WHERE user_id = {$this->user_id} AND competence_id = {$competence_id}
			");
			$log .= "-|{$competence_id}|{$this->self_data[$competence_id]}|{$new_value}|{$this->competences[$competence_id]}\n";
			$this->something_changed = true;
		}
		if ($this->something_changed)
		{
			$log_escaped = $this->db->escape($log);
			$this->db->sql("
				INSERT INTO vector_self_history
					(user_id, competence_set_id, add_time, log)
				VALUES
					({$this->user_id}, {$this->competence_set_id}, NOW(), '{$log_escaped}')
			");

			$this->db->sql("
				UPDATE vector
				SET
					edit_time = NOW(),
					self_edit_time = NOW()
				WHERE id = {$this->vector_id}
			");

			$this->db->sql("
				INSERT INTO vector_self_stat
					(user_id, competence_set_id, self_competence_count_calc, edit_time)
				VALUES
					({$this->user_id}, {$this->competence_set_id}, {$this->total_count}, NOW())
				ON DUPLICATE KEY UPDATE
					self_competence_count_calc = {$this->total_count},
					edit_time = NOW()
			");

			$this->db->sql("
				INSERT INTO user_project_link
					(project_id, user_id, status, add_time, edit_time, last_vector_id_calc)
				VALUES
					({$this->project_id}, {$this->user_id}, 'deleted', NOW(), NOW(), {$this->vector_id})
				ON DUPLICATE KEY UPDATE
					last_vector_id_calc = {$this->vector_id},
					edit_time = NOW()
			");

			$this->db->sql("
				INSERT INTO user_competence_set_link_calc
					(competence_set_id, user_id, default_vector_id_calc)
				VALUES
					({$this->competence_set_id}, {$this->user_id}, {$this->vector_id})
				ON DUPLICATE KEY UPDATE
					default_vector_id_calc = {$this->vector_id}
			");
		}
		return true;
	}
	
	public function on_after_commit()
	{
		if (sizeof($this->future_competences_data) and $this->request->get_referrer())
		{
			$this->pass_info->write_info("SELF_NOT_SET", sizeof($this->future_competences_data));
			$this->set_redirect_url($this->request->get_referrer());
		}
	}

	public function clean_cache()
	{
		if ($this->something_changed)
		{
			vector_cache_tag::init($this->vector_id)->update();
		}
	}

}

?>