<?php

class vector_focus_save_page extends base_save_ctrl
{
	
	// @todo check rights can_read_project()
	// @todo add mixin

	private $user_id;
	private $project_id;
	private $vector_config;
	private $vector_info;
	private $vector_id;
	private $competence_set_id;
	private $vector_data;
	private $focus_data;
	private $competences;
	private $focus_to_add = array();
	private $focus_to_delete = array();
	private $total_count = 0;
	private $something_changed = false;

	public function start()
	{
		$this->user_id = $this->user->get_user_id();
		if (!$this->user_id)
		{
			return false;
		}

		$this->project_id = POST("project_id");
		if (!is_good_id($this->project_id))
		{
			return false;
		}

		$this->vector_config = vector_info_math::get_project_vector_config($this->project_id);
		if (!$this->vector_config)
		{
			// Vector is turned off
			return false;
		}

		$this->vector_info = vector_info_math::get_project_vector_info($this->user_id, $this->project_id);
		if (!$this->vector_info)
		{
			return false;
		}
		$this->vector_id = $this->vector_info["id"];
		$this->competence_set_id = $this->vector_info["competence_set_id"];
		$this->vector_data = new vector_data_math($this->vector_id, $this->user_id, $this->competence_set_id);
		$this->focus_data = $this->vector_data->get_focus_data();
		$this->competences = $this->db->fetch_column_values("SELECT competence_id as id, title FROM competence_calc WHERE competence_set_id = {$this->competence_set_id}", "title", "id");
		return true;
	}

	public function check()
	{
		foreach ($this->competences as $competence_id => $competence_title)
		{
			$new = POST("competence-" . $competence_id) == "1";
			$old = isset($this->focus_data[$competence_id]);
			if ($new)
			{
				++$this->total_count;
			}
			if ($old != $new)
			{
				if ($new)
				{
					$this->focus_to_add[] = $competence_id;
				}
				else
				{
					$this->focus_to_delete[] = $competence_id;
				}
			}
		}
		if ($this->total_count < 1 or $this->total_count > 10)
		{
			$this->pass_info->write_error($this->total_count < 1 ? "TOO_SMALL_COUNT" : "TOO_BIG_COUNT", $this->total_count);
			$this->pass_info->dump_vars();
			return false;
		}
		return true;
	}

	public function commit()
	{
		$log = "";
		foreach ($this->focus_to_add as $competence_id)
		{
			$this->db->sql("
				INSERT IGNORE INTO vector_focus
					(vector_id, competence_id, add_time)
				VALUES
					({$this->vector_id}, {$competence_id}, NOW())
			");
			$log .= "+|{$competence_id}|{$this->competences[$competence_id]}\n";
			$this->something_changed = true;
		}
		foreach ($this->focus_to_delete as $competence_id)
		{
			$this->db->sql("
				DELETE FROM vector_focus
				WHERE 
					vector_id = {$this->vector_id}
					AND competence_id = {$competence_id}
			");
			$log .= "-|{$competence_id}|{$this->competences[$competence_id]}\n";
			$this->something_changed = true;
		}
		if ($this->something_changed)
		{
			$log_escaped = $this->db->escape($log);
			$this->db->sql("
				INSERT INTO vector_focus_history
					(vector_id, add_time, log)
				VALUES
					({$this->vector_id}, NOW(), '{$log_escaped}')
			");

			$this->db->sql("
				UPDATE vector
				SET
					focus_competence_count_calc = {$this->total_count},
					edit_time = NOW(),
					focus_edit_time = NOW()
				WHERE id = {$this->vector_id}
			");

			$this->db->sql("
				INSERT INTO user_project_link
					(project_id, user_id, status, add_time, edit_time, last_vector_id_calc)
				VALUES
					({$this->project_id}, {$this->user_id}, 'deleted', NOW(), NOW(), {$this->vector_id})
				ON DUPLICATE KEY UPDATE
					last_vector_id_calc = {$this->vector_id},
					edit_time = NOW()
			");

			$this->db->sql("
				INSERT INTO user_competence_set_link_calc
					(competence_set_id, user_id, default_vector_id_calc)
				VALUES
					({$this->competence_set_id}, {$this->user_id}, {$this->vector_id})
				ON DUPLICATE KEY UPDATE
					default_vector_id_calc = {$this->vector_id}
			");
		}
		return true;
	}

	public function clean_cache()
	{
		if ($this->something_changed)
		{
			vector_cache_tag::init($this->vector_id)->update();
		}
	}

}

?>