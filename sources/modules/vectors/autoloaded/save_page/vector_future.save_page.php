<?php

class vector_future_save_page extends base_save_ctrl
{
	
	// @todo check rights can_read_project()
	// @todo add mixin

	private $user_id;
	private $project_id;
	private $vector_config;
	private $vector_info;
	private $vector_id;
	private $competence_set_id;
	private $vector_data;
	private $future_data;
	private $prof_to_add = array();
	private $prof_to_delete = array();
	private $total_count = 0;
	private $something_changed = false;

	public function start()
	{
		$this->user_id = $this->user->get_user_id();
		if (!$this->user_id)
		{
			return false;
		}

		$this->project_id = POST("project_id");
		if (!is_good_id($this->project_id))
		{
			return false;
		}

		$this->vector_config = vector_info_math::get_project_vector_config($this->project_id);
		if (!$this->vector_config)
		{
			// Vector is turned off
			return false;
		}

		$this->vector_info = vector_info_math::get_project_vector_info($this->user_id, $this->project_id, true);
		if (!$this->vector_info)
		{
			return false;
		}
		$this->vector_id = $this->vector_info["id"];
		$this->competence_set_id = $this->vector_info["competence_set_id"];
		$this->vector_data = new vector_data_math($this->vector_id, $this->user_id, $this->competence_set_id);
		$this->future_data = $this->vector_data->get_future_data();
		return true;
	}

	public function check()
	{
		foreach ($this->future_data as $prof_id => $data)
		{
			$old = $data["checked"];
			$new = POST("prof-" . $prof_id) == "1";
			if ($new)
			{
				++$this->total_count;
			}
			if ($old != $new)
			{
				if ($new)
				{
					$this->prof_to_add[] = $prof_id;
				}
				else
				{
					$this->prof_to_delete[] = $prof_id;
				}
			}
		}
		if ($this->total_count < 1 or $this->total_count > 4)
		{
			$this->pass_info->write_error($this->total_count < 1 ? "TOO_SMALL_COUNT" : "TOO_BIG_COUNT", $this->total_count);
			$this->pass_info->dump_vars();
			return false;
		}
		return true;
	}

	public function commit()
	{
		$log = "";
		foreach ($this->prof_to_add as $prof_id)
		{
			$this->db->sql("
				INSERT IGNORE INTO vector_future
					(vector_id, professiogram_id, add_time)
				VALUES
					({$this->vector_id}, {$prof_id}, NOW())
			");
			$log .= "+|{$prof_id}|{$this->future_data[$prof_id]["title"]}\n";
			$this->something_changed = true;
		}
		foreach ($this->prof_to_delete as $prof_id)
		{
			$this->db->sql("
				DELETE FROM vector_future
				WHERE 
					vector_id = {$this->vector_id}
					AND professiogram_id = {$prof_id}
			");
			$log .= "-|{$prof_id}|{$this->future_data[$prof_id]["title"]}\n";
			$this->something_changed = true;
		}
		if ($this->something_changed)
		{
			$log_escaped = $this->db->escape($log);
			$this->db->sql("
				INSERT INTO vector_future_history
					(vector_id, add_time, log)
				VALUES
					({$this->vector_id}, NOW(), '{$log_escaped}')
			");

			$this->db->sql("
				UPDATE vector
				SET
					future_professiogram_count_calc = {$this->total_count},
					edit_time = NOW(),
					future_edit_time = NOW()
				WHERE id = {$this->vector_id}
			");

			$this->db->sql("
				DELETE FROM vector_future_competences_calc
				WHERE vector_id = {$this->vector_id}
			");

			$this->db->sql("
				INSERT INTO vector_future_competences_calc
					(vector_id, competence_id, mark)
				SELECT vf.vector_id, c.competence_id, MAX(rc.mark) as mark
				FROM vector_future vf
				LEFT JOIN professiogram p ON p.id = vf.professiogram_id
				LEFT JOIN rate_competence_link rc ON rc.rate_id = p.rate_id
				INNER JOIN competence_calc c ON c.competence_id = rc.competence_id
				WHERE vf.vector_id = {$this->vector_id}
				GROUP BY c.competence_id
			");

			$this->db->sql("
				INSERT INTO user_project_link
					(project_id, user_id, status, add_time, edit_time, last_vector_id_calc)
				VALUES
					({$this->project_id}, {$this->user_id}, 'deleted', NOW(), NOW(), {$this->vector_id})
				ON DUPLICATE KEY UPDATE
					last_vector_id_calc = {$this->vector_id},
					edit_time = NOW()
			");

			$this->db->sql("
				INSERT INTO user_competence_set_link_calc
					(competence_set_id, user_id, default_vector_id_calc)
				VALUES
					({$this->competence_set_id}, {$this->user_id}, {$this->vector_id})
				ON DUPLICATE KEY UPDATE
					default_vector_id_calc = {$this->vector_id}
			");
		}
		return true;
	}

	public function clean_cache()
	{
		if ($this->something_changed)
		{
			vector_cache_tag::init($this->vector_id)->update();
		}
	}

}

?>