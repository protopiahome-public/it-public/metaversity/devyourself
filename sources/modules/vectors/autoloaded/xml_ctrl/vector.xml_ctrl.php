<?php

define("VECTOR_DISPLAY_FUTURE", 1);
define("VECTOR_DISPLAY_FUTURE_MATCH_USING_SELF", 2);
define("VECTOR_DISPLAY_FUTURE_ALL_PROFESSIONS", 4);
define("VECTOR_DISPLAY_FUTURE_COMPETENCES", 8);
define("VECTOR_DISPLAY_FUTURE_COMPETENCES_DETAILED", 16);
define("VECTOR_DISPLAY_SELF_STAT", 32);
define("VECTOR_DISPLAY_SELF", 64);
define("VECTOR_DISPLAY_FOCUS", 128);

class vector_xml_ctrl extends base_xml_ctrl
{

	private $vector_info_struct;
	/**
	 * @var vector_data_math
	 */
	private $vector_data;
	private $display_mask;

	/**
	 * @param array $vector_info_struct - the result obtained from
	 *   vector_info_math::get_project_vector_info().
	 * @param vector_data_math $vector_data
	 * @param bitmask $display_mask - bitmask of VECTOR_DISPLAY_*
	 */
	public function __construct($vector_info_struct, vector_data_math $vector_data, $display_mask)
	{
		$this->vector_info_struct = $vector_info_struct;
		$this->vector_data = $vector_data;
		$this->display_mask = $display_mask;

		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		$xdom = xdom::create("vector");
		if ($this->vector_info_struct)
		{
			foreach ($this->vector_info_struct as $idx => $val)
			{
				$xdom->set_attr($idx, $val);
			}

			// Future
			if ($this->display_mask & VECTOR_DISPLAY_FUTURE)
			{
				$future_data = $this->vector_data->get_future_data(!($this->display_mask & VECTOR_DISPLAY_FUTURE_ALL_PROFESSIONS), $this->display_mask & VECTOR_DISPLAY_FUTURE_MATCH_USING_SELF);
				$future_node = $xdom->create_child_node("future");
				foreach ($future_data as $professiogram_id => $data)
				{
					$professiogram_node = $future_node->create_child_node("professiogram")
							->set_attr("id", $professiogram_id)
							->set_attr("checked", $data["checked"])
							->set_attr("title", $data["title"]);
					if (isset($data["match"]))
					{
						$professiogram_node->set_attr("match", $data["match"]);
					}
				}
			}

			// Future competences
			if ($this->display_mask & VECTOR_DISPLAY_FUTURE_COMPETENCES)
			{
				$future_competences_data = $this->vector_data->get_future_competences_data();
				$future_competences_node = $xdom->create_child_node("future_competences");
				foreach ($future_competences_data as $competence_id => $mark)
				{
					$competence_node = $future_competences_node->create_child_node("competence")
							->set_attr("id", $competence_id)
							->set_attr("mark", $mark);
				}
			}

			// Future competences' details
			if ($this->display_mask & VECTOR_DISPLAY_FUTURE_COMPETENCES_DETAILED)
			{
				$future_competences_details = $this->vector_data->get_future_competences_details();
				$future_competences_details_node = $xdom->create_child_node("future_competences_details");
				foreach ($future_competences_details as $competence_id => $data)
				{
					$competence_node = $future_competences_details_node->create_child_node("competence")
							->set_attr("id", $competence_id);
					foreach ($data as $professiogram_id => $mark)
					{
						$competence_node->create_child_node("professiogram")
							->set_attr("id", $professiogram_id)
							->set_attr("mark", $mark);
					}
				}
			}

			// Self
			if ($this->display_mask & VECTOR_DISPLAY_SELF | VECTOR_DISPLAY_SELF_STAT)
			{
				$self_node = $xdom->create_child_node("self");
				if ($this->display_mask & VECTOR_DISPLAY_SELF_STAT)
				{
					$self_stat = $this->vector_data->get_self_stat();
					$self_node->set_attr("mark_count", $self_stat["mark_count"]);
					$self_node->set_attr("edit_time", $self_stat["edit_time"]);
				}
				if ($this->display_mask & VECTOR_DISPLAY_SELF)
				{
					$self_data = $this->vector_data->get_self_data();
					foreach ($self_data as $competence_id => $mark)
					{
						$competence_node = $self_node->create_child_node("competence")
								->set_attr("id", $competence_id)
								->set_attr("mark", $mark);
					}
				}
			}

			// Focus
			if ($this->display_mask & VECTOR_DISPLAY_FOCUS)
			{
				$focus_data = $this->vector_data->get_focus_data();
				$focus_node = $xdom->create_child_node("focus");
				foreach ($focus_data as $competence_id => $tmp)
				{
					$competence_node = $focus_node->create_child_node("competence")
							->set_attr("id", $competence_id);
				}
			}
		}
		return $xdom->get_xml(true);
	}

}

?>