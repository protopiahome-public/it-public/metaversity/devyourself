<?php

class vector_config_xml_ctrl extends base_xml_ctrl
{

	private $vector_config;

	/**
	 * @param array $vector_config - the result obtained from
	 *   vector_info_math::get_project_vector_config().
	 */
	public function __construct($vector_config)
	{
		$this->vector_config = $vector_config;

		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		if (is_array($this->vector_config))
		{
			return $this->get_node_string("vector_config", $this->vector_config);
		}
		else
		{
			return "<vector_config/>";
		}
	}

}

?>