<?php

class project_vector_menu_xml_ctrl extends base_xml_ctrl
{

	private $vector_config;
	private $vector_info;

	/**
	 * @var vector_data_math
	 */
	private $vector_data;
	private $current_step_name;

	public function __construct($vector_config, $vector_info, vector_data_math $vector_data = null, $current_step_name = "")
	{
		$this->vector_config = $vector_config;
		$this->vector_info = $vector_info;
		$this->vector_data = $vector_data;
		$this->current_step_name = $current_step_name;
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		$xdom = xdom::create($this->name);

		$is_future_filled = $this->vector_data && sizeof($this->vector_data->get_future_data(true)) > 0;
		$is_self_complete = $this->vector_data && $this->vector_data->is_self_complete();

		/* $self = array_keys($this->vector_data->get_self_data());
		  $future = array_keys($this->vector_data->get_future_competences_data());
		  dd(array_diff($future, $self)); */

		$is_focus_filled = $this->vector_data && sizeof($this->vector_data->get_focus_data()) > 0;

		/* Future */
		$step_node = $xdom->create_child_node("step")
			->set_attr("number", "1")
			->set_attr("name", "future")
			->set_attr("available", "1");
		if ($this->current_step_name == "future")
		{
			$step_node->set_attr("is_current", "1");
		}

		/* Self */
		$available = $is_future_filled;
		$step_node = $xdom->create_child_node("step")
			->set_attr("number", "2")
			->set_attr("name", "self")
			->set_attr("available", $available);
		if ($this->current_step_name == "self")
		{
			$step_node->set_attr("is_current", "1");
		}

		/* Focus */
		$available = $is_future_filled && $is_self_complete;
		$step_node = $xdom->create_child_node("step")
			->set_attr("number", "3")
			->set_attr("name", "focus")
			->set_attr("available", $available);
		if ($this->current_step_name == "focus")
		{
			$step_node->set_attr("is_current", "1");
		}

		/* Recommendations */
		$available = $is_future_filled && $is_self_complete && $is_focus_filled;
		$step_node = $xdom->create_child_node("step")
			->set_attr("number", "4")
			->set_attr("name", "recommend")
			->set_attr("available", $available);
		if ($this->current_step_name == "recommend")
		{
			$step_node->set_attr("is_current", "1");
		}

		// Ha!
		return $xdom->get_xml(true);
	}

}

?>