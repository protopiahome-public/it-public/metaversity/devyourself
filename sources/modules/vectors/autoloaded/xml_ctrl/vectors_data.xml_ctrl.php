<?php

class vectors_data_xml_ctrl extends base_xml_ctrl
{

	/**
	 * @var vectors_data_math
	 */
	private $vectors_data;
	private $show_future;
	private $show_self;
	private $show_focus;

	public function __construct(vectors_data_math $vectors_data, $show_future, $show_self, $show_focus)
	{
		$this->vectors_data = $vectors_data;
		$this->show_future = $show_future;
		$this->show_self = $show_self;
		$this->show_focus = $show_focus;

		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		$xdom = xdom::create("vectors_data");
		$xdom->set_attr("user_id", $this->vectors_data->get_user_id());
		$xdom->set_attr("competence_set_id", $this->vectors_data->get_competence_set_id());

		// Future
		if ($this->show_future)
		{
			$future_node = $xdom->create_child_node("future");
			foreach ($this->vectors_data->get_vectors_future_data() as $vector_future_data)
			{
				$vector_node = $future_node->create_child_node("vector");
				$vector_node->set_attr("id", $vector_future_data["vector_id"]);
				$vector_node->set_attr("type", $vector_future_data["type"]);
				$vector_node->set_attr($vector_future_data["type"] . "_id", $vector_future_data[$vector_future_data["type"] . "_id"]);
				$vector_node->set_attr("future_edit_time", $vector_future_data["future_edit_time"]);
				$competences_node = $vector_node->create_child_node("competences");
				foreach ($vector_future_data["competences"] as $competence_id => $mark)
				{
					$competences_node->create_child_node("competence")
						->set_attr("id", $competence_id)
						->set_attr("mark", $mark);
				}
				if (isset($vector_future_data["project_id"]))
				{
					$this->xml_loader->add_xml_by_class_name("project_short_xml_ctrl", $vector_future_data["project_id"]);
				}
			}
		}

		// Self
		if ($this->show_self)
		{
			$self_node = $xdom->create_child_node("self");
			foreach ($this->vectors_data->get_vectors_self_data() as $competence_id => $mark)
			{
				$self_node->create_child_node("competence")
					->set_attr("id", $competence_id)
					->set_attr("mark", $mark);
			}
		}

		// Focus
		if ($this->show_focus)
		{
			$focus_node = $xdom->create_child_node("focus");
			foreach ($this->vectors_data->get_vectors_focus_data() as $competence_id => $competence_focus_data)
			{
				$competence_node = $focus_node->create_child_node("competence")
						->set_attr("id", $competence_id);
				foreach ($competence_focus_data as $competence_focus_vector_data)
				{
					$vector_node = $competence_node->create_child_node("vector");
					$vector_node->set_attr("id", $competence_focus_vector_data["vector_id"]);
					$vector_node->set_attr("type", $competence_focus_vector_data["type"]);
					$vector_node->set_attr($competence_focus_vector_data["type"] . "_id", $competence_focus_vector_data[$competence_focus_vector_data["type"] . "_id"]);
					$vector_node->set_attr("add_time", $competence_focus_vector_data["add_time"]);
					if (isset($competence_focus_vector_data["project_id"]))
					{
						$this->xml_loader->add_xml_by_class_name("project_short_xml_ctrl", $competence_focus_vector_data["project_id"]);
					}
				}
			}
		}

		return $xdom->get_xml(true);
	}

}

?>