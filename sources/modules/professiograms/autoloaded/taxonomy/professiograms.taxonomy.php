<?php

final class professiograms_taxonomy extends base_taxonomy
{

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, competence_set_obj $competence_set_obj)
	{
		$this->competence_set_obj = $competence_set_obj;
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		$p = $this->get_parts_relative();

		$competence_set_obj = $this->competence_set_obj;
		$competence_set_id = $competence_set_obj->get_id();
		$competence_set_access = $competence_set_obj->get_access();

		//sets/set-<id>/professiograms/
		if ($p[1] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new professiograms_xml_page($competence_set_id));
		}
		elseif ($p[1] === "add" and $p[2] === null)
		{
			//sets/set-<id>/professiograms/add/
			$this->xml_loader->add_xml_with_xslt(new professiogram_add_xml_page($competence_set_id), null, "professiogram_403");
		}
		elseif (is_good_id($p[1]) and $p[2] === null)
		{
			//sets/set-<id>/professiograms/<id>/
			$this->xml_loader->add_xml_with_xslt(new professiogram_show_xml_page($competence_set_id, $p[1]));
		}
		elseif (is_good_id($p[1]) and $p[2] === "edit" and $p[3] === null)
		{
			//sets/set-<id>/professiograms/<id>/edit/
			$this->xml_loader->add_xml_with_xslt(new professiogram_edit_xml_page($competence_set_id, $p[1]), null, "professiogram_403");
		}
		elseif (is_good_id($p[1]) and $p[2] === "delete" and $p[3] === null)
		{
			//sets/set-<id>/professiograms/<id>/delete/
			$this->xml_loader->add_xml_with_xslt(new professiogram_delete_xml_page($competence_set_id, $p[1]), null, "professiogram_403");
		}
	}

}

?>