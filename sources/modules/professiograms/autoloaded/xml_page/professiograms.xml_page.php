<?php

class professiograms_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "professiogram";
	// Internal
	protected $competence_set_id;

	public function __construct($competence_set_id)
	{
		$this->competence_set_id = $competence_set_id;
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("competences", "competence_count_calc ASC", "competence_count_calc DESC");
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("professiogram");
		$select_sql->add_select_fields("id, enabled, rate_id, competence_count_calc, title");
		$select_sql->add_where("competence_set_id = {$this->competence_set_id}");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>