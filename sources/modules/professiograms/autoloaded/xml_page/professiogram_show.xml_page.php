<?php

class professiogram_show_xml_page extends base_dt_show_xml_ctrl
{

	// Settings level 2
	protected $options = array(
		0 => "—",
		1 => "Склонность",
		2 => "Способность",
		3 => "Компетенция"
	);
	// Settings
	protected $dt_name = "professiogram";
	protected $axis_name = "full";
	// Internal
	protected $competence_set_id;

	public function __construct($competence_set_id, $professiogram_id)
	{
		$this->competence_set_id = $competence_set_id;
		parent::__construct($professiogram_id);
	}

	public function postprocess()
	{
		$this->xml_loader->add_xml(new rate_xml_ctrl($this->data[0]["rate_id"], $this->competence_set_id, $this->options));
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("dt.rate_id");
		$select_sql->add_where("dt.competence_set_id = {$this->competence_set_id}");
	}

}

?>