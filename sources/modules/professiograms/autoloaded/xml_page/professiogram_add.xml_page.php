<?php

class professiogram_add_xml_page extends base_dt_add_xml_ctrl
{

	// Settings level 2
	protected $options = array(
		0 => "—",
		1 => "Склонность",
		2 => "Способность",
		3 => "Компетенция"
	);
	// Settings
	protected $dt_name = "professiogram";
	protected $axis_name = "add";
	protected $enable_blocks = true;
	// Internal
	protected $competence_set_id;

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;

	/**
	 * @var competence_set_access
	 */
	protected $competence_set_access;

	public function __construct($competence_set_id)
	{
		$this->competence_set_id = $competence_set_id;

		$this->competence_set_obj = competence_set_obj::instance($this->competence_set_id);
		$this->competence_set_access = $this->competence_set_obj->get_access();

		parent::__construct();
	}

	public function init()
	{
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->competence_set_id));
		$this->xml_loader->add_xml(new rate_xml_ctrl(0, $this->competence_set_id, $this->options));
	}

	public function check_rights()
	{
		return $this->competence_set_access->can_moderate_professiograms();
	}

}

?>