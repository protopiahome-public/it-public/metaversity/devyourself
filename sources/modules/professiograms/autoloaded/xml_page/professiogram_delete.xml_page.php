<?php

class professiogram_delete_xml_page extends base_delete_xml_ctrl
{

	// Settings
	protected $db_table = "professiogram";
	// Internal
	protected $competence_set_id;

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;

	/**
	 * @var competence_set_access
	 */
	protected $competence_set_access;

	public function __construct($competence_set_id, $professiogram_id)
	{
		$this->competence_set_id = $competence_set_id;

		$this->competence_set_obj = competence_set_obj::instance($this->competence_set_id);
		$this->competence_set_access = $this->competence_set_obj->get_access();
		
		parent::__construct($professiogram_id);
	}
	
	public function check_rights()
	{
		return $this->competence_set_access->can_moderate_professiograms();
	}
	
	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("competence_set_id = {$this->competence_set_id}");
	}

}

?>