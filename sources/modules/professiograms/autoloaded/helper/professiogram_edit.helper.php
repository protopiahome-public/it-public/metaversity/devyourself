<?php

class professiogram_edit_helper extends base_static_db_helper
{

	public static function update_competence_set_stat($competence_set_id)
	{
		self::db()->sql("
			UPDATE `competence_set`
			SET
				professiogram_count_calc = (
					SELECT count(*)
					FROM professiogram
					WHERE competence_set_id = {$competence_set_id}
						AND enabled = 1
				)
			WHERE id = {$competence_set_id}
		");
	}

}

?>