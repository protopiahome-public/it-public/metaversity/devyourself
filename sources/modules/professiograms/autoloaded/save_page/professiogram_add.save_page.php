<?php

class professiogram_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"competence_set_before_start",
	);
	// Settings
	protected $dt_name = "professiogram";
	protected $axis_name = "edit";
	// Internal
	protected $competence_set_id;

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;
	
	/**
	 * @var competence_set_access
	 */
	protected $competence_set_access;

	/**
	 * @var rate_save_ctrl
	 */
	protected $rate_save_ctrl;

	public function init()
	{
		$this->save_loader->add_ctrl($this->rate_save_ctrl = new rate_save_ctrl($this->dt_name, array(0, 1, 2, 3)));
	}

	public function on_after_start()
	{
		$this->rate_save_ctrl->set_rate_id(0);
		$this->rate_save_ctrl->set_competence_set_id($this->competence_set_id);
		return true;
	}

	public function check_rights()
	{
		$this->competence_set_access = $this->competence_set_obj->get_access();
		return $this->competence_set_access->can_moderate_professiograms();
	}

	public function on_before_commit()
	{
		$this->update_array["competence_set_id"] = $this->competence_set_id;
		$this->update_array["adder_user_id"] = $this->user->get_user_id();
		return true;
	}

	public function on_after_commit()
	{
		$this->rate_save_ctrl->set_object_id($this->last_id);
		professiogram_edit_helper::update_competence_set_stat($this->competence_set_id);
	}

	public function clean_cache()
	{
		competence_set_cache_tag::init($this->competence_set_id)->update();
	}

}

?>