<?php

class professiogram_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"competence_set_before_start",
	);
	// Settings
	protected $db_table = "professiogram";
	// Internal
	protected $competence_set_id;

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;

	/**
	 * @var competence_set_access
	 */
	protected $competence_set_access;

	public function check_rights()
	{
		$this->competence_set_access = $this->competence_set_obj->get_access();
		return $this->competence_set_access->can_moderate_professiograms();
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("competence_set_id = {$this->competence_set_id}");
	}

	public function on_after_commit()
	{
		// @todo cleaner
		if ($this->old_db_row["rate_id"])
		{
			$this->db->sql("DELETE FROM rate WHERE id = {$this->old_db_row["rate_id"]}");
		}
		professiogram_edit_helper::update_competence_set_stat($this->competence_set_id);
	}

	public function clean_cache()
	{
		competence_set_cache_tag::init($this->competence_set_id)->update();
	}

}

?>