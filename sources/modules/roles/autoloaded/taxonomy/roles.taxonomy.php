<?php

final class roles_taxonomy extends base_taxonomy
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, project_obj $project_obj)
	{
		$this->project_obj = $project_obj;
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		$p = $this->get_parts_relative();

		$project_obj = $this->project_obj;
		$project_id = $project_obj->get_id();
		$project_access = $project_obj->get_access();

		//p/<project_name>/roles/...
		if (!$project_obj->role_recommendation_is_on())
		{
			$this->xml_loader->set_error_404();
			$this->xml_loader->set_error_404_xslt("project_module_404", "project");
		}
		elseif ($p[1] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new project_role_recommendation_xml_page());
		}
	}

}

?>