<?php

class project_role_dt extends base_dt
{

	protected function init()
	{
		$this->add_block("main", "Основное");

		$dtf = new string_dtf("title", "Имя роли");
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("enabled", "Включена");
		$dtf->set_default_value(true);
		$this->add_field($dtf, "main");

		$dtf = new text_html_dtf("descr", "Описание роли");
		$dtf->set_editor_height(120);
		$this->add_field($dtf, "main");
		
		$this->add_fields_in_axis("edit", array("title", "enabled", "descr"));
		$this->add_fields_in_axis("full", array("title", "enabled", "descr"));
		
	}

}

?>