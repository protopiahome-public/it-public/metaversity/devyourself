<?php

final class research_taxonomy extends base_taxonomy
{

	public function run()
	{
		$p = $this->get_parts_relative();

		if ($p[1] === "research" and $this->error->is_debug_mode())
		{
			//research/...
			if ($p[2] === null)
			{
				//research/
				$this->xml_loader->add_xml_with_xslt(new research_xml_page());
			}
			elseif ($p[2] === "cluster-competences" and $p[3] === null)
			{
				//research/cluster-competences/
				$this->xml_loader->add_xml_with_xslt(new research_cluster_competences_xml_page());
			}
			elseif ($p[2] === "group-results-clusters" and $p[3] === null)
			{
				//research/group-results-clusters/
				$this->xml_loader->add_xml_with_xslt(new research_group_results_clusters_xml_page());
			}
			elseif ($p[2] === "cluster-users" and $p[3] === null)
			{
				//research/cluster-users/
				$this->xml_loader->add_xml_with_xslt(new research_cluster_users_xml_page());
			}
		}
		elseif ($research_num = $this->is_type_folder($p[1], "research", true) and $p[2] === null and $this->error->is_debug_mode())
		{
			//research-<N>/
			$this->xml_loader->add_xml_by_class_name("research{$research_num}_xml_page", 1);
		}
		elseif ($p[1] === "util1" and $p[2] === null and $this->error->is_debug_mode())
		{
			//util1/
			$this->xml_loader->add_xml_with_xslt(new util1_xml_ctrl());
		}
	}

}

?>