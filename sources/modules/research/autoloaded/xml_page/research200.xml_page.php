<?php

class research200_xml_page extends base_xml_ctrl
{

	private $competence_set_id;

	public function __construct($tmp)
	{
		$this->competence_set_id = 24;
		parent::__construct();
	}

	public function get_xml()
	{
		$competence_set_id = $this->competence_set_id;

		$results_math = new results_math(
				$this->competence_set_id,
				null,
				$competence_id_array = array(),
				$ignore_user_selection = true,
				$calc_stat = false,
				$export_marks_log = false,
				$project_id = 16
		);
		$results = $results_math->get_results();
		
		$this->db->sql("DROP TABLE IF EXISTS `competence_user_link`");
		$this->db->sql("
			CREATE TABLE `competence_user_link` (
				`competence_id` INTEGER UNSIGNED NOT NULL,
				`user_id` INTEGER UNSIGNED NOT NULL,
				`mark` INTEGER UNSIGNED NOT NULL,
				PRIMARY KEY (`competence_id`, `user_id`)
			)
			ENGINE = InnoDB;
		");
		
		foreach ($results as $user_id => $user_data)
		{
			$data = array();
			foreach ($user_data["sum"] as $competence_id => $mark)
			{
				$data[] = array($competence_id, $user_id, $mark);
			}
			$this->db->multi_insert("competence_user_link", array("competence_id", "user_id", "mark"), $data);
		}
		
		$text = "Done";
		
		response::set_content_type_text();
		response::set_content($text);
		return true;
	}

}

?>