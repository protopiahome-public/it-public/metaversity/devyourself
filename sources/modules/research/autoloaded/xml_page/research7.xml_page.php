<?php

class research7_xml_page extends base_xml_ctrl
{

	private $competence_set_id;

	public function __construct($tmp)
	{
		$this->competence_set_id = 30;
		parent::__construct();
	}

	public function get_xml()
	{
		$competence_set_id = $this->competence_set_id;
		$id_delta = 100000;
		chdir(PATH_TMP);
		$this->db->select_db("cmp_research2");
		$this->db->sql("SET SESSION group_concat_max_len = 8192");

		$cmp_threshold = 15;
		$text = "\n";

		// Users
		$users_data = $this->db->fetch_column_values("
			SELECT user_id, user_group_id
			FROM user_user_group_link 
			WHERE user_group_id IN (12, 13)
				AND user_id IN (
					SELECT DISTINCT ratee_user_id
					FROM mark_calc
					WHERE competence_id IN (SELECT competence_id FROM mark_stat WHERE cnt >= {$cmp_threshold})
				)
		", "user_group_id", "user_id");
		$users = array_keys($users_data);
		$users_index = array_flip($users);
		
		// Results
		$results_math_initial = new results_math(
				$this->competence_set_id,
				$user_id_array = $users,
				$competence_id_array = array(),
				$ignore_user_selection = true,
				$calc_stat = false,
				$export_marks_log = false,
				$project_id = 26
		);
		$results_initial = $results_math_initial->get_results();

		$initial_competences = $competences_code = $vectors_code = null;
		$this->fetch_initial_data2($cmp_threshold, $users_index, $results_initial, $initial_competences, $competences_code, $vectors_code);
		
		// Competences
		$competences_data = $this->db->fetch_column_values("
			SELECT id, title
			FROM competence_full
			WHERE id IN (" . join(", ", $initial_competences) . ")
		", "title", "id");

		//$code = "data={$vectors_code};data1=Transpose[data];cov=N[Covariance[data1]];vec=N[Eigenvectors[cov]];data1.Transpose[vec]";
		
		$code = "data={$vectors_code};data1=Transpose[data];cov=N[Covariance[data1]];N[Eigenvectors[cov]]";
		$matrix_text = $this->math_run($code);
		//$matrix_text = file_get_contents("out.txt");
		$matrix = $this->parse_math_matrix($matrix_text);

		$competences_index = array_flip($initial_competences);

		$results = array();
		for ($vector_idx = 0; $vector_idx < 45; ++$vector_idx)
		{
			$results[$vector_idx] = array(
				"group_stat" => array(),
				"cmp_stat" => array(),
			);
			// "group_stat"
			foreach ($results_initial as $user_id => $user_data)
			{
				$group_id = $users_data[$user_id];
				if (!isset($results[$vector_idx]["group_stat"][$group_id]))
				{
					$results[$vector_idx]["group_stat"][$group_id] = array(
						"users" => array(),
						"num" => 0,
						"den" => 0,
						"cnt" => 0,
					);
				}
				$results[$vector_idx]["group_stat"][$group_id]["users"][$user_id] = true;
				foreach ($initial_competences as $competence_idx => $competence_id)
				{
					$result = isset($user_data["sum"][$competence_id]) ? $user_data["sum"][$competence_id] : 0;
					$rate = abs($matrix[$vector_idx][$competence_idx]);
					$results[$vector_idx]["group_stat"][$group_id]["num"] += $result * $rate;
					$results[$vector_idx]["group_stat"][$group_id]["den"] += $rate;
					$results[$vector_idx]["group_stat"][$group_id]["cnt"] += 1;
				}
			}
			foreach ($results[$vector_idx]["group_stat"] as $group_id => &$group_data)
			{
				$group_data["result"] = $group_data["num"] / $group_data["den"];
				//$text .= "{$group_data["cnt"]} / {$group_data["num"]} / {$group_data["den"]}\n";
			}
			$results[$vector_idx]["difference"] = $results[$vector_idx]["group_stat"][13]["result"] - $results[$vector_idx]["group_stat"][12]["result"];
			$results[$vector_idx]["difference_abs"] = abs($results[$vector_idx]["difference"]);
			unset($group_data);
			
			// "cmp_stat"
			$results[$vector_idx]["cmp_stat"] = array();
			foreach ($initial_competences as $competence_idx => $competence_id)
			{
				$rate = abs($matrix[$vector_idx][$competence_idx]);
				$results[$vector_idx]["cmp_stat"][$competence_id] = $rate;
			}
			arsort($results[$vector_idx]["cmp_stat"]);
		}
		$results = array_sort($results, "difference_abs", SORT_DESC);

		// Output
		
		foreach ($results as $vector_idx => $data)
		{
			$text .= "[{$vector_idx}] " . $data["difference"] . "\n";
			$j = 0;
			foreach ($data["cmp_stat"] as $competence_id => $rate)
			{
				++$j;
				//if ($j > 10) break;
				//$text .= "\t({$competence_id}) " . $competences_data[$competence_id] . " - {$rate}\n";
			}
		}

		response::set_content_type_text();
		response::set_content($text);
		return true;
	}

	private function fetch_initial_data2($cmp_threshold, $users_index, $results_initial, &$initial_competences, &$competences_code, &$vectors_code)
	{
		// Fetching competences
		$initial_competences = $this->db->fetch_column_values("
			SELECT competence_id
			FROM mark_stat
			WHERE cnt >= {$cmp_threshold}
		");
		
		////////////////////////////////////////
		$users_data = $this->db->fetch_column_values("
			SELECT user_id, user_group_id
			FROM user_user_group_link 
			WHERE user_group_id IN (12)
				AND user_id IN (
					SELECT DISTINCT ratee_user_id
					FROM mark_calc
					WHERE competence_id IN (SELECT competence_id FROM mark_stat WHERE cnt >= {$cmp_threshold})
				)
		", "user_group_id", "user_id");
		$users = array_keys($users_data);
		$users_index = array_flip($users);
		$results_math_initial = new results_math(
				$this->competence_set_id,
				$user_id_array = $users,
				$competence_id_array = array(),
				$ignore_user_selection = true,
				$calc_stat = false,
				$export_marks_log = false,
				$project_id = 26
		);
		$results_initial = $results_math_initial->get_results();
		////////////////////////////////////////

		$blank_vector = array_fill(0, sizeof($users_index), 0);
		$vectors = array();
		$vectors_sq_len = array();
		foreach ($initial_competences as $competence_id)
		{
			$vectors[$competence_id] = $blank_vector;
			$vectors_sq_len[$competence_id] = 0;
		}
		foreach ($results_initial as $user_id => $user_results)
		{
			$user_results_sum = $user_results["sum"];
			foreach ($user_results_sum as $competence_id => $mark)
			{
				if (isset($vectors[$competence_id]))
				{
					$vectors[$competence_id][$users_index[$user_id]] = $mark;
					$vectors_sq_len[$competence_id] += $mark * $mark;
				}
			}
		}
		$vectors_normalized = $vectors;
		foreach ($vectors_normalized as $competence_id => &$vector_normalized)
		{
			foreach ($vector_normalized as &$val)
			{
				//$val = $val ? ($val . "/" . "Sqrt[" . $vectors_sq_len[$competence_id] . "]") : 0; //!
				$val = $val;
			}
			unset($val);
		}
		unset($vector_normalized);

		// Output
		$competences_code = "{" . join(", ", array_keys($vectors)) . "}";

		$vectors_code_array = array();
		foreach ($vectors_normalized as $competence_id => $vector_normalized)
		{
			$vectors_code_array[] = "{" . join(", ", $vector_normalized) . "}";
		}
		$vectors_code .= "{" . join(", \n", $vectors_code_array) . "}";
	}

	protected function parse_math_matrix($text)
	{
		$text = preg_replace("/[\\x00-\\x20\\\\]/", "", $text);
		$text = preg_replace("/^\{\{/", "", $text);
		$text = preg_replace("/\}\}$/", "", $text);
		$rows = explode("},{", $text);
		$result = array();
		foreach ($rows as $row)
		{
			if (!trim($row))
			{
				dd("Bad format");
			}
			$row_items = explode(",", $row);
			if (sizeof($row_items) < 1)
			{
				dd("Bad format");
			}
			$row_array = array();
			foreach ($row_items as $row_item)
			{
				if (!preg_match("/^-?[0-9]+\.?[0-9]*$/", $row_item))
				{
					dd("Bad format (float): '{$row_item}'");
				}
				$row_array[] = floatval($row_item);
			}
			$result[] = $row_array;
		}
		return $result;
	}

	protected function remove_single($matrix)
	{
		$result = array();
		foreach ($matrix as $row)
		{
			if (sizeof($row) > 1)
			{
				$result[] = $row;
			}
		}
		return $result;
	}

	protected function all_matrix_elements($matrix)
	{
		$items = array();
		foreach ($matrix as $row)
		{
			foreach ($row as $item)
			{
				$items[$item] = true;
			}
		}
		return array_keys($items);
	}

	protected function array_column($array, $key)
	{
		$result = array();
		foreach ($array as $row)
		{
			foreach ($row as $_key => $value)
			{
				if ($_key === $key)
				{
					$result[] = $value;
				}
			}
		}
		return $result;
	}

	protected function math_run($code)
	{
		file_put_contents("in.txt", $code);
		$cmd = '"C:\Program Files\Wolfram Research\Mathematica\8.0\math.exe" -batchinput -batchoutput < in.txt > out.txt 2>&1';
		$output = $return_var = null;
		exec($cmd, $output, $return_var);
		if (!empty($output))
		{
			dd("Error", join("\n", $output));
		}
		if ($return_var != 0)
		{
			dd("Error: RETURN_CODE={$return_var}\n");
		}
		return file_get_contents("out.txt");
	}

}

?>