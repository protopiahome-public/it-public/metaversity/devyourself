<?php

class research_cluster_users_xml_page extends base_research_xml_ctrl
{

	private $clustering_methods = array(
		"KMeans" => "K-means",
		"Agglomerate" => "Agglomerative",
		"Optimize" => "Local Optimization",
	);
	private $distances = array(
		"EuclideanDistance" => "Euclidean distance",
		"SquaredEuclideanDistance" => "Squared Euclidean distance",
		"NormalizedSquaredEuclideanDistance" => "Normalized squared Euclidean distance",
		"CosineDistance" => "Angular cosine distance",
		"CorrelationDistance" => "Correlation coefficient distance",
		"ManhattanDistance" => "Manhattan or \"city block\" distance",
	);
	// Input
	private $project_id;
	private $project_row;
	private $competence_set_id;
	private $competence_set_row;
	private $mark_count_threshold = 20;
	private $cluster_count = 10;
	private $clusters_to_leave = array();
	private $clustering_method;
	private $distance = false;
	private $normalize = false;
	// Data
	private $users;
	private $competences;
	private $code_users;
	private $code_vectors;
	private $results_initial;
	private $results_key;
	private $matrix;

	/**
	 * @var xdom
	 */
	private $xdom;

	private function init_input()
	{
		$this->project_id = GET("project_id");
		if (!$this->project_row = $this->check_entity_row("project", $this->project_id))
		{
			$this->xdom->set_attr("error", "BAD_PROJECT_ID");
			return false;
		}

		$this->competence_set_id = GET("competence_set_id");
		if (!$this->competence_set_row = $this->check_entity_row("competence_set", $this->competence_set_id))
		{
			$this->xdom->set_attr("error", "BAD_COMPETENCE_SET_ID");
			return false;
		}

		$this->mark_count_threshold = GET("mark_count_threshold");
		if (!is_good_id($this->mark_count_threshold))
		{
			$this->xdom->set_attr("error", "BAD_MARK_COUNT_THRESHOLD");
			return false;
		}

		$this->cluster_count = GET("cluster_count");
		if (!is_good_id($this->cluster_count))
		{
			$this->xdom->set_attr("error", "BAD_CLUSTER_COUNT");
			return false;
		}

		$this->clusters_to_leave = explode(",", GET("clusters_to_leave"));
		foreach ($this->clusters_to_leave as $index => &$cluster_number)
		{
			$cluster_number = trim($cluster_number);
			if ($cluster_number === "")
			{
				unset($this->clusters_to_leave[$index]);
				continue;
			}
			if (!is_good_id($cluster_number))
			{
				$this->xdom->set_attr("error", "BAD_CLUSTER_TO_LEAVE");
				return false;
			}
		}
		$this->clusters_to_leave = array_flip($this->clusters_to_leave);
		ksort($this->clusters_to_leave);

		$this->clustering_method = GET("clustering_method");
		if (!isset($this->clustering_methods[$this->clustering_method]))
		{
			$this->xdom->set_attr("error", "BAD_CLUSTERING_METHOD");
			return false;
		}

		$this->distance = GET("distance");
		if (!isset($this->distances[$this->distance]))
		{
			$this->xdom->set_attr("error", "BAD_DISTANCE");
			return false;
		}

		$this->normalize = GET("normalize") === "1";

		return true;
	}

	private function init_form()
	{
		$projects = $this->db->fetch_column_values("SELECT * FROM project WHERE total_mark_count_calc > 0 ORDER BY title", "title", "id");
		$projects_node = $this->xdom->create_child_node("projects");
		foreach ($projects as $id => $title)
		{
			$projects_node->create_child_node("project")
				->set_attr("id", $id)
				->set_attr("title", $title);
		}

		$competence_sets = $this->db->fetch_column_values("SELECT * FROM competence_set WHERE total_mark_count_calc > 0 ORDER BY title", "title", "id");
		$competence_sets_node = $this->xdom->create_child_node("competence_sets");
		foreach ($competence_sets as $id => $title)
		{
			$competence_sets_node->create_child_node("competence_set")
				->set_attr("id", $id)
				->set_attr("title", $title);
		}

		$clustering_methods_node = $this->xdom->create_child_node("clustering_methods");
		foreach ($this->clustering_methods as $key => $title)
		{
			$clustering_methods_node->create_child_node("method")
				->set_attr("key", $key)
				->set_attr("title", $title);
		}

		$distances_node = $this->xdom->create_child_node("distances");
		foreach ($this->distances as $key => $title)
		{
			$distances_node->create_child_node("distance")
				->set_attr("key", $key)
				->set_attr("title", $title);
		}

		$this->xdom->set_attr("project_id", $this->project_id);
		$this->xdom->set_attr("competence_set_id", $this->competence_set_id);
		$this->xdom->set_attr("mark_count_threshold", $this->mark_count_threshold);
		$this->xdom->set_attr("cluster_count", $this->cluster_count);
		$this->xdom->set_attr("clusters_to_leave", join(", ", array_keys($this->clusters_to_leave)));
		$this->xdom->set_attr("clustering_method", $this->clustering_method);
		$this->xdom->set_attr("distance", $this->distance);
		$this->xdom->set_attr("normalize", $this->normalize);
	}

	private function fetch_data()
	{
		$this->users = $this->db->fetch_all("
			SELECT l.user_id id, u.login, IF(u.first_name <> '' OR u.last_name <> '', TRIM(CONCAT(u.first_name, ' ', u.last_name)), '<без имени>') full_name
			FROM user_competence_set_project_link l
			LEFT JOIN user u ON u.id = l.user_id
			WHERE l.competence_set_id = {$this->competence_set_id} AND l.project_id = {$this->project_id} AND l.total_mark_count_calc >= {$this->mark_count_threshold}
		", "id");

		$this->competences = $this->db->fetch_column_values("
			SELECT competence_id id, title
			FROM competence_calc
			WHERE
				competence_set_id = {$this->competence_set_id}
				AND total_mark_count_calc > 0
		", "title", "id");

		$results_math = new results_math(
				$this->competence_set_id,
				$user_id_array = array_keys($this->users),
				$competence_id_array = array_keys($this->competences),
				$ignore_user_selection = true,
				$calc_stat = false,
				$export_marks_log = false,
				$project_id = $this->project_id
		);
		$this->results_initial = $results_math->get_results();
		$this->results_key = "project_{$this->competence_set_id}_{$this->project_id}";
		$this->xdom->set_attr("used_mark_count", $results_math->get_used_mark_count());

		$code_users_array = array();
		$code_vectors_array = array();
		foreach ($this->users as $user_id => $user_params)
		{
			if (!isset($this->results_initial[$user_id]))
			{
				trigger_error("User results must exist: '{$user_id}'");
			}
			$user_data = $this->results_initial[$user_id];
			$vector = array();
			foreach ($this->competences as $competence_id => $competence_title)
			{
				$vector[$competence_id] = isset($user_data[$this->results_key][$competence_id]) ? $user_data[$this->results_key][$competence_id] : 0;
			}
			$code_vectors_array[$user_id] = $this->get_math_vector($vector, $this->normalize);
		}

		$this->code_users = $this->get_math_vector(array_keys($this->users));
		$this->code_vectors = $this->get_math_vector($code_vectors_array);

		if ($this->cluster_count > sizeof($this->users))
		{
			$this->cluster_count = sizeof($this->users);
		}

		$this->xdom->set_attr("competence_count", sizeof($this->competences));
		$this->xdom->set_attr("user_count", sizeof($this->users));
	}

	private function math()
	{
		$code = "";
		$code .= "data={$this->code_vectors};\n";
		$code .= "users={$this->code_users};\n";
		// Old way
		//$code .= "FindClusters[data -> users, {$this->cluster_count}, Method -> \"Agglomerate\"]\n";
		$code .= "ClusteringComponents[data, {$this->cluster_count}, 1, Method -> \"{$this->clustering_method}\", DistanceFunction -> {$this->distance}]";
		$this->xdom->create_child_node("code", $code);

		$code_draw = "";
		$code_draw .= "data={$this->code_vectors};\n";
		$code_draw .= "pc = PrincipalComponents[N[data]];\n";
		$code_draw .= "r2 = Take[pc, All, {1, 2}];\n";
		$code_draw .= "clust = FindClusters[data -> r2, {$this->cluster_count}];\n";
		$code_draw .= "ListPlot[Take[clust, {1, {$this->cluster_count}}], PlotStyle -> {PointSize[Large]}, PlotRange -> All]\n";
		$this->xdom->create_child_node("code_draw", $code_draw);

		$error_desc = null;
		if (!$return_text = $this->math_run($code, $error_desc))
		{
			$this->xdom->set_attr("error", "MATH_RUN_FAILED");
			return false;
		}

		// Old way
		//if (!$this->matrix = $this->parse_math_matrix($return_text))
		//{
		//	$this->xdom->set_attr("error", "MATH_MATRIX_PARSING_FAILED");
		//	return false;
		//}
		if (!$clusters = $this->parse_math_vector($return_text))
		{
			$this->xdom->set_attr("error", "MATH_MATRIX_PARSING_FAILED");
			return false;
		}

		$this->matrix = array();
		for ($i = 0; $i < $this->cluster_count; ++$i)
		{
			$this->matrix[$i] = array();
		}
		$users_index = array_keys($this->users);
		foreach ($clusters as $user_index => $cluster_number)
		{
			if ($cluster_number > $this->cluster_count)
			{
				$this->xdom->set_attr("error", "INCORRECT_CLUSTER_NUMBER_RECEIVED");
				return false;
			}
			$cluster_index = $cluster_number - 1;
			$this->matrix[$cluster_index][] = $users_index[$user_index];
		}

		return true;
	}

	private function analyze()
	{
		if (sizeof($this->clusters_to_leave))
		{
			foreach ($this->matrix as $user_set_index => $user_id_array)
			{
				if (!isset($this->clusters_to_leave[$user_set_index + 1]))
				{
					unset($this->matrix[$user_set_index]);
				}
			}
		}

		if (!sizeof($this->matrix))
		{
			$this->xdom->set_attr("error", "NO_DATA");
			return false;
		}

		// Professiograms
		$professiogram_id_array = $this->db->fetch_column_values("SELECT id FROM professiogram WHERE enabled = 1 AND competence_set_id = {$this->competence_set_id}");

		// Professiogram data
		$professiogram_data_math = new professiogram_data_math();
		$professiogram_data = $professiogram_data_math->get_professiograms_data($professiogram_id_array);

		// Stat over competences
		$blank_competences_stat = array();
		foreach ($this->competences as $competence_id => $competence_title)
		{
			$blank_competences_stat[$competence_id] = array(
				"title" => $competence_title,
				"mark_sum" => 0,
			);
		}
		$cmp_stat = $blank_competences_stat;
		$cmp_stat_sets = array();
		foreach ($this->matrix as $user_set_index => $user_id_array)
		{
			$cmp_stat_sets[$user_set_index] = $blank_competences_stat;
		}

		// Building stat over competences
		$total_user_count = 0;
		foreach ($this->matrix as $user_set_index => $user_id_array)
		{
			foreach ($user_id_array as $user_id)
			{
				foreach ($this->results_initial[$user_id][$this->results_key] as $competence_id => $mark)
				{
					$cmp_stat[$competence_id]["mark_sum"] += $mark;
					$cmp_stat_sets[$user_set_index][$competence_id]["mark_sum"] += $mark;
				}
			}
			$total_user_count += sizeof($user_id_array);
		}

		// Calculating average mark
		foreach ($cmp_stat as $competence_id => &$stat_data)
		{
			$stat_data["result"] = round($stat_data["mark_sum"] / $total_user_count, 1);
		}
		unset($stat_data);
		$cmp_stat = array_sort($cmp_stat, "result", SORT_DESC);
		foreach ($this->matrix as $user_set_index => $user_id_array)
		{
			$user_count = sizeof($user_id_array);
			foreach ($cmp_stat_sets[$user_set_index] as $competence_id => &$stat_data)
			{
				$stat_data["result"] = round($stat_data["mark_sum"] / $user_count, 1);
				$stat_data["delta"] = $stat_data["result"] - $cmp_stat[$competence_id]["result"];
			}
			unset($stat_data);
			$cmp_stat_sets[$user_set_index] = array_sort($cmp_stat_sets[$user_set_index], "delta", SORT_DESC);
			if (sizeof($this->clusters_to_leave) == 1)
			{
				$cmp_stat_sets[$user_set_index] = array_sort($cmp_stat_sets[$user_set_index], "result", SORT_DESC);
			}
		}

		// Match calculation
		$professiogram_match_math = new rate_match_math($this->results_initial, $professiogram_data);

		// Stat over professiograms
		$stat = array();
		foreach ($professiogram_data as $prof_index => $data)
		{
			$user_rating = $professiogram_match_math->get_user_rating_for_rate($prof_index);
			$max_match = reset($user_rating);
			$stat[$data["id"]] = array(
				"title" => $data["title"],
				"max_match" => $max_match,
				"match_sum" => 0,
			);
		}
		$stat_sets = array();
		foreach ($this->matrix as $user_set_index => $user_id_array)
		{
			$stat_sets[$user_set_index] = array();
			foreach ($professiogram_data as $data)
			{
				$stat_sets[$user_set_index][$data["id"]] = array(
					"title" => $data["title"],
					"match_sum" => 0,
				);
			}
		}

		// Building stat
		$total_user_count = 0;
		foreach ($this->matrix as $user_set_index => $user_id_array)
		{
			foreach ($user_id_array as $user_id)
			{
				$rating = $professiogram_match_math->get_rate_rating_for_user($user_id);
				foreach ($rating as $professiogram_id => $match)
				{
					$real_match = round($match / $stat[$professiogram_id]["max_match"] * 100);
					$stat[$professiogram_id]["match_sum"] += $real_match;
					$stat_sets[$user_set_index][$professiogram_id]["match_sum"] += $real_match;
				}
			}
			$total_user_count += sizeof($user_id_array);
		}

		// Calculating average match
		foreach ($stat as $professiogram_id => &$stat_data)
		{
			$stat_data["result"] = round($stat_data["match_sum"] / $total_user_count);
		}
		unset($stat_data);
		$stat = array_sort($stat, "result", SORT_DESC);

		foreach ($this->matrix as $user_set_index => $user_id_array)
		{
			$user_count = sizeof($user_id_array);
			foreach ($stat_sets[$user_set_index] as $professiogram_id => &$stat_data)
			{
				$stat_data["result"] = round($stat_data["match_sum"] / $user_count);
				$stat_data["delta"] = $stat_data["result"] - $stat[$professiogram_id]["result"];
			}
			unset($stat_data);
			$stat_sets[$user_set_index] = array_sort($stat_sets[$user_set_index], "delta", SORT_DESC);
			if (sizeof($this->clusters_to_leave) == 1)
			{
				$stat_sets[$user_set_index] = array_sort($stat_sets[$user_set_index], "result", SORT_DESC);
			}
		}

		// Output
		$clusters_node = $this->xdom->create_child_node("clusters");
		foreach ($this->matrix as $user_set_index => $user_id_array)
		{
			$cluster_node = $clusters_node->create_child_node("cluster");
			$cluster_node->set_attr("number", $user_set_index + 1);
			$users_node = $cluster_node->create_child_node("users");
			foreach ($user_id_array as $user_id)
			{
				$users_node->create_child_node("user")
					->set_attr("id", $user_id)
					->set_attr("login", $this->users[$user_id]["login"])
					->set_attr("full_name", $this->users[$user_id]["full_name"]);
			}
			$professiograms_node = $cluster_node->create_child_node("professiograms");
			$n = 1;
			foreach ($stat_sets[$user_set_index] as $professiogram_id => $stat_data)
			{
				$professiograms_node->create_child_node("professiogram")
					->set_attr("id", $professiogram_id)
					->set_attr("number", $n)
					->set_attr("title", $stat_data["title"])
					->set_attr("match", $stat_data["result"])
					->set_attr("delta", $stat_data["delta"]);
				++$n;
			}
			$competences_node = $cluster_node->create_child_node("competences");
			$n = 1;
			foreach ($cmp_stat_sets[$user_set_index] as $competence_id => $competence_data)
			{
				$competences_node->create_child_node("competence")
					->set_attr("id", $competence_id)
					->set_attr("number", $n)
					->set_attr("title", $competence_data["title"])
					->set_attr("result", $competence_data["result"])
					->set_attr("delta", $competence_data["delta"]);
				++$n;
			}
		}
	}

	public function get_xml()
	{
		$this->xdom = xdom::create($this->name);

		$init_ok = $this->init_input();
		$this->init_form();
		if (!$init_ok)
		{
			return $this->xdom->get_xml(true);
		}
		$this->fetch_data();
		if (!sizeof($this->competences) or !sizeof($this->users))
		{
			return $this->xdom->get_xml(true);
		}
		if (!$this->math())
		{
			return $this->xdom->get_xml(true);
		}
		$this->analyze();
		return $this->xdom->get_xml(true);
	}

}

?>