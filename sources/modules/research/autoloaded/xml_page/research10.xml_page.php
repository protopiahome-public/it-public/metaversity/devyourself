<?php

class research10_xml_page extends base_research_xml_ctrl
{

	// Input
	private $project_id = 26;
	private $competence_set_id = 30;
	private $user_mark_count_threshold = 20;
	private $competence_mark_count_threshold = 25;
	// Data
	private $users;
	private $competences;
	private $results_initial;
	private $results_key;

	public function get_xml()
	{
		$this->db->sql("SET SESSION group_concat_max_len = 8192");
		$this->db->sql("
			CREATE TEMPORARY TABLE mark_stat
			SELECT competence_id, count(*) cnt
			FROM mark_calc
			WHERE type = 'personal'
				AND project_id = {$this->project_id}
				AND competence_set_id = {$this->competence_set_id}
			GROUP BY competence_id
			ORDER BY cnt DESC
		");

		$this->users = $this->db->fetch_all("
			SELECT m.ratee_user_id id, u.login, IF(u.first_name <> '' OR u.last_name <> '', TRIM(CONCAT(u.first_name, ' ', u.last_name)), '<без имени>') full_name
			FROM mark_calc m
			LEFT JOIN user u ON u.id = m.ratee_user_id
			WHERE 
				m.type = 'personal'
				AND m.project_id = {$this->project_id}
				AND m.competence_set_id = {$this->competence_set_id}
			GROUP BY m.ratee_user_id
			HAVING COUNT(*) >= {$this->user_mark_count_threshold}
		", "id");

		$this->competences = $this->db->fetch_column_values("
			SELECT competence_id id, title
			FROM competence_calc
			WHERE competence_id IN (
				SELECT competence_id
				FROM mark_stat
				WHERE cnt >= {$this->competence_mark_count_threshold}
			)
		", "title", "id");

		$results_math = new results_math(
				$this->competence_set_id,
				$user_id_array = array_keys($this->users),
				$competence_id_array = array_keys($this->competences),
				$ignore_user_selection = true,
				$calc_stat = false,
				$export_marks_log = false,
				$project_id = $this->project_id
		);
		$this->results_initial = $results_math->get_results();
		$this->results_key = "project_{$this->competence_set_id}_{$this->project_id}";

		$text = "\n";
		foreach ($this->competences as $competence_id => $competence_data)
		{
			//$text .= "[{$competence_id}]\n";
			$results = array();
			foreach ($this->results_initial as $user_id => $user_results)
			{
				if (isset($user_results[$this->results_key][$competence_id]))
				{
					$results[] = $user_results[$this->results_key][$competence_id];
				}
				else
				{
					$results[] = 0;
				}
			}
			//$text .= $this->get_math_vector($results) . "\n";
			$text .= "m=" . $this->get_math_vector($results) . "\n";
			$text .= "JarqueBeraALMTest[m, \"PValue\", SignificanceLevel -> 0.05]\n";
			$text .= "JarqueBeraALMTest[m, \"ShortTestConclusion\", SignificanceLevel -> 0.05]\n";
		}

		response::set_content_type_text();
		response::set_content($text);
		return true;
	}

}

?>