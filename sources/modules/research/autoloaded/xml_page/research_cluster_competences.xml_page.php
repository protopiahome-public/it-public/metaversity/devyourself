<?php

class research_cluster_competences_xml_page extends base_research_xml_ctrl
{

	private $id_delta = 100000;
	private $clustering_methods = array(
		"KMeans" => "K-means",
		"Agglomerate" => "Agglomerative",
		"Optimize" => "Local Optimization",
	);
	private $distances = array(
		"EuclideanDistance" => "Euclidean distance",
		"SquaredEuclideanDistance" => "Squared Euclidean distance",
		"NormalizedSquaredEuclideanDistance" => "Normalized squared Euclidean distance",
		"CosineDistance" => "Angular cosine distance",
		"CorrelationDistance" => "Correlation coefficient distance",
		"ManhattanDistance" => "Manhattan or \"city block\" distance",
	);
	private $vector_dimensions = array(
		"Precedents" => "Совместная встречаемость в прецедентах",
		"Results" => "Результаты",
	);
	// Input
	private $project_id;
	private $project_row;
	private $competence_set_id;
	private $competence_set_row;
	private $user_mark_count_threshold = 20;
	private $competence_mark_count_threshold = 15;
	private $cluster_count = 10;
	private $clusters_to_leave = array();
	private $clustering_method;
	private $distance = false;
	private $normalize = false;
	private $vector_dimension;
	// Data
	private $total_competence_count;
	private $users;
	private $competences;
	private $code_competences;
	private $code_vectors;
	private $results_initial;
	private $results_key;
	private $matrix;
	private $report = array();

	/**
	 * @var xdom
	 */
	private $xdom;

	private function init_form()
	{
		$projects = $this->db->fetch_column_values("SELECT * FROM project WHERE total_mark_count_calc > 0 ORDER BY title", "title", "id");
		$projects_node = $this->xdom->create_child_node("projects");
		foreach ($projects as $id => $title)
		{
			$projects_node->create_child_node("project")
				->set_attr("id", $id)
				->set_attr("title", $title);
		}

		$competence_sets = $this->db->fetch_column_values("SELECT * FROM competence_set WHERE total_mark_count_calc > 0 ORDER BY title", "title", "id");
		$competence_sets_node = $this->xdom->create_child_node("competence_sets");
		foreach ($competence_sets as $id => $title)
		{
			$competence_sets_node->create_child_node("competence_set")
				->set_attr("id", $id)
				->set_attr("title", $title);
		}

		$clustering_methods_node = $this->xdom->create_child_node("clustering_methods");
		foreach ($this->clustering_methods as $key => $title)
		{
			$clustering_methods_node->create_child_node("method")
				->set_attr("key", $key)
				->set_attr("title", $title);
		}

		$distances_node = $this->xdom->create_child_node("distances");
		foreach ($this->distances as $key => $title)
		{
			$distances_node->create_child_node("distance")
				->set_attr("key", $key)
				->set_attr("title", $title);
		}

		$vector_dimensions_node = $this->xdom->create_child_node("vector_dimensions");
		foreach ($this->vector_dimensions as $key => $title)
		{
			$vector_dimensions_node->create_child_node("dimension")
				->set_attr("key", $key)
				->set_attr("title", $title);
		}
	}

	private function init_input()
	{
		$this->project_id = GET("project_id");
		if (!$this->project_row = $this->check_entity_row("project", $this->project_id))
		{
			$this->xdom->set_attr("error", "BAD_PROJECT_ID");
			return false;
		}

		$this->competence_set_id = GET("competence_set_id");
		if (!$this->competence_set_row = $this->check_entity_row("competence_set", $this->competence_set_id))
		{
			$this->xdom->set_attr("error", "BAD_COMPETENCE_SET_ID");
			return false;
		}

		$this->user_mark_count_threshold = GET("user_mark_count_threshold");
		if (!is_good_id($this->user_mark_count_threshold))
		{
			$this->xdom->set_attr("error", "BAD_USER_MARK_COUNT_THRESHOLD");
			return false;
		}

		$this->competence_mark_count_threshold = GET("competence_mark_count_threshold");
		if (!is_good_id($this->competence_mark_count_threshold))
		{
			$this->xdom->set_attr("error", "BAD_COMPETENCE_MARK_COUNT_THRESHOLD");
			return false;
		}

		$this->cluster_count = GET("cluster_count");
		if (!is_good_id($this->cluster_count))
		{
			$this->xdom->set_attr("error", "BAD_CLUSTER_COUNT");
			return false;
		}

		$this->clusters_to_leave = explode(",", GET("clusters_to_leave"));
		foreach ($this->clusters_to_leave as $index => &$cluster_number)
		{
			$cluster_number = trim($cluster_number);
			if ($cluster_number === "")
			{
				unset($this->clusters_to_leave[$index]);
				continue;
			}
			if (!is_good_id($cluster_number))
			{
				$this->xdom->set_attr("error", "BAD_CLUSTER_TO_LEAVE");
				return false;
			}
		}
		$this->clusters_to_leave = array_flip($this->clusters_to_leave);
		ksort($this->clusters_to_leave);

		$this->clustering_method = GET("clustering_method");
		if (!isset($this->clustering_methods[$this->clustering_method]))
		{
			$this->xdom->set_attr("error", "BAD_CLUSTERING_METHOD");
			return false;
		}

		$this->distance = GET("distance");
		if (!isset($this->distances[$this->distance]))
		{
			$this->xdom->set_attr("error", "BAD_DISTANCE");
			return false;
		}

		$this->vector_dimension = GET("vector_dimension");
		if (!isset($this->vector_dimensions[$this->vector_dimension]))
		{
			$this->xdom->set_attr("error", "BAD_VECTOR_DIMENSION");
			return false;
		}

		$this->normalize = GET("normalize") === "1";

		return true;
	}

	private function init_form_values()
	{
		$this->xdom->set_attr("project_id", $this->project_id);
		$this->xdom->set_attr("competence_set_id", $this->competence_set_id);
		$this->xdom->set_attr("user_mark_count_threshold", $this->user_mark_count_threshold);
		$this->xdom->set_attr("competence_mark_count_threshold", $this->competence_mark_count_threshold);
		$this->xdom->set_attr("cluster_count", $this->cluster_count);
		$this->xdom->set_attr("clusters_to_leave", join(", ", array_keys($this->clusters_to_leave)));
		$this->xdom->set_attr("clustering_method", $this->clustering_method);
		$this->xdom->set_attr("distance", $this->distance);
		$this->xdom->set_attr("vector_dimension", $this->vector_dimension);
		$this->xdom->set_attr("normalize", $this->normalize);
	}

	private function fetch_data()
	{
		$this->db->sql("SET SESSION group_concat_max_len = 8192");
		$this->db->sql("
			CREATE TEMPORARY TABLE mark_stat
			SELECT competence_id, count(*) cnt
			FROM mark_calc
			WHERE type = 'personal'
				AND project_id = {$this->project_id}
				AND competence_set_id = {$this->competence_set_id}
			GROUP BY competence_id
			ORDER BY cnt DESC
		");

		$this->total_competence_count = $this->db->get_value("SELECT COUNT(*) FROM mark_stat");

		$this->users = $this->db->fetch_all("
			SELECT m.ratee_user_id id, u.login, IF(u.first_name <> '' OR u.last_name <> '', TRIM(CONCAT(u.first_name, ' ', u.last_name)), '<без имени>') full_name
			FROM mark_calc m
			LEFT JOIN user u ON u.id = m.ratee_user_id
			WHERE 
				m.type = 'personal'
				AND m.project_id = {$this->project_id}
				AND m.competence_set_id = {$this->competence_set_id}
			GROUP BY m.ratee_user_id
			HAVING COUNT(*) >= {$this->user_mark_count_threshold}
		", "id");
			
		$this->competences = $this->db->fetch_column_values("
			SELECT competence_id id, title
			FROM competence_calc
			WHERE competence_id IN (
				SELECT competence_id
				FROM mark_stat
				WHERE cnt >= {$this->competence_mark_count_threshold}
			)
		", "title", "id");
		
		$competences_code = $vectors_code = "";
		switch ($this->vector_dimension)
		{
			case "Precedents":
				$this->fetch_initial_data();
				break;
			case "Results":
				$this->fetch_initial_data2();
				break;
			default:
				trigger_error("Unknown vector dim: '{$this->vector_dimension}'");
		}

		if ($this->cluster_count > sizeof($this->competences))
		{
			$this->cluster_count = sizeof($this->competences);
		}

		$this->xdom->set_attr("competence_count", sizeof($this->competences));
		$this->xdom->set_attr("user_count", sizeof($this->users));
	}

	private function fetch_initial_data()
	{
		// Building vectors
		$marks = $this->db->fetch_all("
			SELECT ratee_user_id, precedent_id, CAST(GROUP_CONCAT(DISTINCT competence_id SEPARATOR ',') AS CHAR) competences
			FROM mark_calc
			WHERE 
				type = 'personal' 
				AND competence_id IN (SELECT competence_id FROM mark_stat WHERE cnt >= {$this->competence_mark_count_threshold})
				AND project_id = {$this->project_id}
				AND competence_set_id = {$this->competence_set_id}
			GROUP BY ratee_user_id, precedent_id
		");
		$blank_vector = array_fill(0, sizeof($marks), 0);
		$vectors = array();
		foreach ($this->competences as $competence_id => $competence_title)
		{
			$vectors[$competence_id] = $blank_vector;
		}
		foreach ($marks as $index => $marks_row)
		{
			foreach (explode(",", $marks_row["competences"]) as $competence_id)
			{
				$vectors[$competence_id][$index] = 1;
			}
		}

		// Output
		$this->code_competences = $this->get_math_vector(array_keys($vectors));

		$code_vectors_array = array();
		foreach ($vectors as $competence_id => $vector)
		{
			$code_vectors_array[] = $this->get_math_vector($vector, $this->normalize);
		}
		$this->code_vectors = $this->get_math_vector($code_vectors_array);
	}

	private function fetch_initial_data2()
	{
		$results_math = new results_math(
				$this->competence_set_id,
				$user_id_array = array_keys($this->users),
				$competence_id_array = array(),
				$ignore_user_selection = true,
				$calc_stat = false,
				$export_marks_log = false,
				$project_id = $this->project_id
		);
		$this->results_initial = $results_math->get_results();
		$this->results_key = "project_{$this->competence_set_id}_{$this->project_id}";
		$this->xdom->set_attr("used_mark_count", $results_math->get_used_mark_count());

		$users_index = array_flip(array_keys($this->users));
		$blank_vector = array_fill(0, sizeof($users_index), 0);
		$vectors = array();
		foreach ($this->competences as $competence_id => $competence_title)
		{
			$vectors[$competence_id] = $blank_vector;
		}
		foreach ($this->results_initial as $user_id => $user_results)
		{
			foreach ($user_results[$this->results_key] as $competence_id => $mark)
			{
				if (isset($vectors[$competence_id]))
				{
					$vectors[$competence_id][$users_index[$user_id]] = $mark;
				}
			}
		}

		// Output
		$this->code_competences = $this->get_math_vector(array_keys($vectors));

		$code_vectors_array = array();
		foreach ($vectors as $competence_id => $vector)
		{
			$code_vectors_array[] = $this->get_math_vector($vector, $this->normalize);
		}
		$this->code_vectors = $this->get_math_vector($code_vectors_array);
	}

	private function math()
	{
		$code = "";
		$code .= "data = {$this->code_vectors};\n";
		$code .= "names = {$this->code_competences};\n";
		$code .= "ClusteringComponents[data, {$this->cluster_count}, 1, Method -> \"{$this->clustering_method}\", DistanceFunction -> {$this->distance}]";
		$this->xdom->create_child_node("code", $code);

		$error_desc = null;
		if (!$return_text = $this->math_run($code, $error_desc))
		{
			$this->xdom->set_attr("error", "MATH_RUN_FAILED");
			return false;
		}

		if (!$clusters = $this->parse_math_vector($return_text))
		{
			$this->xdom->set_attr("error", "MATH_MATRIX_PARSING_FAILED");
			return false;
		}

		$this->matrix = array();
		for ($i = 0; $i < $this->cluster_count; ++$i)
		{
			$this->matrix[$i] = array();
		}
		$competences_index = array_keys($this->competences);
		foreach ($clusters as $user_index => $cluster_number)
		{
			if ($cluster_number > $this->cluster_count)
			{
				$this->xdom->set_attr("error", "INCORRECT_CLUSTER_NUMBER_RECEIVED");
				return false;
			}
			$cluster_index = $cluster_number - 1;
			$this->matrix[$cluster_index][] = $competences_index[$user_index];
		}

		return true;
	}

	private function analyze()
	{
		$this->matrix = $this->remove_single($this->matrix);
		$convertable_competences = $this->all_matrix_elements($this->matrix);

		if (sizeof($this->clusters_to_leave))
		{
			foreach ($this->matrix as $user_set_index => $user_id_array)
			{
				if (!isset($this->clusters_to_leave[$user_set_index + 1]))
				{
					unset($this->matrix[$user_set_index]);
				}
			}
		}

		if (!sizeof($this->matrix))
		{
			$this->xdom->set_attr("error", "NO_DATA");
			return false;
		}

		$this->db->begin();
		
		// New data
		foreach ($this->matrix as $competence_num => $sub_competences)
		{
			$competence_id = $this->id_delta + $competence_num;
			//$old_competence_titles = $this->db->fetch_column_values("SELECT CONCAT('(', id, ') ', title) FROM competence_full WHERE id IN (" . join(",", $sub_competences) . ")");
			//$new_competence_title = join(" / ", $old_competence_titles);
			//$new_competence_title_escaped = $this->db->escape($new_competence_title);
			$new_competence_title_escaped = "*** AUTO COMPETENCE ***";
			$this->db->sql("INSERT INTO competence_full (id, title, competence_set_id) VALUES ({$competence_id}, '{$new_competence_title_escaped}', {$this->competence_set_id})");
			$this->db->sql("INSERT INTO competence_calc (competence_id, title, competence_set_id) VALUES ({$competence_id}, '{$new_competence_title_escaped}', {$this->competence_set_id})");
		}
		$old_prefessiograms = $this->db->fetch_all("SELECT * FROM professiogram WHERE competence_set_id = {$this->competence_set_id} AND enabled = 1");
		$professiogram_ids = array_column($old_prefessiograms, "id");
		$rate_ids = array_column($old_prefessiograms, "rate_id");
		$this->db->sql("
			INSERT INTO rate (id, rate_type_id, competence_set_id, add_time, edit_time)
			SELECT id + {$this->id_delta}, rate_type_id, competence_set_id, add_time, edit_time
			FROM rate
			WHERE id IN (" . $this->array_to_IN_sql($rate_ids) . ")
		");
		$this->db->sql("
			INSERT INTO professiogram (id, title, enabled, add_time, edit_time, rate_id, competence_set_id, descr, adder_user_id)
			SELECT id + {$this->id_delta}, CONCAT('*** AUTO *** ', title), enabled, add_time, edit_time, rate_id + {$this->id_delta}, competence_set_id, descr, adder_user_id
			FROM professiogram
			WHERE id IN (" . $this->array_to_IN_sql($professiogram_ids) . ")
		");
		$convertable_competences_list_sql = sizeof($convertable_competences) ? join(", ", $convertable_competences) : "-1";
		$this->db->sql("
			INSERT INTO rate_competence_link (rate_id, competence_id, mark)
			SELECT rate_id + {$this->id_delta}, competence_id, mark
			FROM rate_competence_link
			WHERE rate_id IN (" . $this->array_to_IN_sql($rate_ids) . ")
				AND competence_id NOT IN ({$convertable_competences_list_sql})
		");
		foreach ($this->matrix as $competence_num => $sub_competences)
		{
			if (sizeof($sub_competences))
			{
				$competence_id = $this->id_delta + $competence_num;
				$this->db->sql("
					INSERT INTO rate_competence_link (rate_id, competence_id, mark)
					SELECT rate_id + {$this->id_delta}, {$competence_id}, ROUND(MAX(mark))
					FROM rate_competence_link
					WHERE rate_id IN (" . $this->array_to_IN_sql($rate_ids) . ")
						AND competence_id IN (" . join(", ", $sub_competences) . ")
					GROUP BY rate_id
				");
				$this->db->sql("
					INSERT INTO mark_calc (id, project_id, precedent_group_id, precedent_flash_group_id, precedent_flash_group_user_link_id, ratee_user_id, rater_user_id, adder_user_id, competence_id, competence_set_id, type, value, comment, precedent_id)
					SELECT id + {$this->id_delta}, project_id, precedent_group_id, precedent_flash_group_id, precedent_flash_group_user_link_id, ratee_user_id, rater_user_id, adder_user_id, {$competence_id}, competence_set_id, type, MAX(value), comment, precedent_id
					FROM mark_calc
					WHERE competence_id IN (" . join(", ", $sub_competences) . ")
					GROUP BY precedent_flash_group_id, ratee_user_id, rater_user_id, type
				");
			}
		}

		// Professiograms
		$old_professiogram_id_array = $this->db->fetch_column_values("SELECT id FROM professiogram WHERE enabled = 1 AND competence_set_id = {$this->competence_set_id} AND id < {$this->id_delta} ORDER BY id");
		$new_professiogram_id_array = $this->db->fetch_column_values("SELECT id FROM professiogram WHERE enabled = 1 AND competence_set_id = {$this->competence_set_id} AND id >= {$this->id_delta} ORDER BY id");

		// Results
		$this->db->sql("UPDATE competence_translator SET is_default = 1");
		$results_math = new results_math(
				$this->competence_set_id,
				$user_id_array = array_keys($this->users),
				$competence_id_array = array(),
				$ignore_user_selection = true,
				$calc_stat = false,
				$export_marks_log = false,
				$project_id = $this->project_id
		);
		$results = $results_math->get_results();

		for ($truncate_competences = 0; $truncate_competences <= 1; ++$truncate_competences)
		{
			if ($truncate_competences)
			{
				$initial_competences_list_sql = sizeof($this->competences) ? join(", ", array_keys($this->competences)) : "-1";
				$this->db->sql("
					DELETE FROM rate_competence_link
					WHERE competence_id NOT IN ({$initial_competences_list_sql}) AND competence_id < {$this->id_delta}
				");
			}

			// Professiogram data
			$old_professiogram_data_math = new professiogram_data_math();
			$old_professiogram_data = $old_professiogram_data_math->get_professiograms_data($old_professiogram_id_array);
			$old_professiogram_match_math = new rate_match_math($results, $old_professiogram_data);
			$new_professiogram_data_math = new professiogram_data_math();
			$new_professiogram_data = $new_professiogram_data_math->get_professiograms_data($new_professiogram_id_array);
			$new_professiogram_match_math = new rate_match_math($results, $new_professiogram_data);

			$diff = array();
			foreach ($old_professiogram_data as $prof_index => $data)
			{
				$old_user_rating = $old_professiogram_match_math->get_user_rating_for_rate($prof_index);
				$new_user_rating = $new_professiogram_match_math->get_user_rating_for_rate($prof_index);
				foreach ($this->users as $user_id => $user_params)
				{
					$new_match = isset($new_user_rating[$user_id]) ? $new_user_rating[$user_id] : 0;
					$old_match = isset($old_user_rating[$user_id]) ? $old_user_rating[$user_id] : 0;
					if ($new_match < 0)
					{
						$new_match = 0;
					}
					$max_match = max($new_match, $old_match);
					if ($max_match > 10)
					{
						//$diff[] = $max_match ? (($new_match - $old_match) / $max_match * 100) : 0; //!
						$diff[] = $new_match - $old_match; //!
					}
				}
			}

			// Diff stat
			$diff_mod = array();
			$diff_sq = array();
			$diff_stat = array();
			$big_diff_count = 0;
			for ($i = -100; $i <= 100; ++$i)
			{
				$diff_stat[$i] = 0;
			}
			foreach ($diff as $delta)
			{
				$abs = abs($delta);
				$diff_mod[] = $abs;
				if ($abs > 10)
				{
					++$big_diff_count;
				}
				$diff_sq[] = $delta * $delta;
				++$diff_stat[$delta];
			}

			if ($truncate_competences == 0)
			{
				$result_competence_count = $this->total_competence_count - sizeof($this->competences) + $this->cluster_count;
				$result_factor_local = round($this->cluster_count / sizeof($this->competences) * 100);
				$result_factor_total = round($result_competence_count / $this->total_competence_count * 100);

				$this->report["competence_mark_count_threshold"] = $this->competence_mark_count_threshold;
				$this->report["total_competence_count"] = $this->total_competence_count;
				$this->report["clustered_competence_count"] = sizeof($this->competences);
				$this->report["cluster_count"] = $this->cluster_count;
				$this->report["result_competence_count"] = $result_competence_count;
				$this->report["result_factor_local"] = $result_factor_local;
				$this->report["result_factor_total"] = $result_factor_total;
			}
			$this->report["TRUNC{$truncate_competences}_max_diff"] = sizeof($diff) ? max($diff) : "?";
			$this->report["TRUNC{$truncate_competences}_min_diff"] = sizeof($diff) ? min($diff) : "?";
			$this->report["TRUNC{$truncate_competences}_avg_diff"] = sizeof($diff) ? str_replace(",", ".", round(array_sum($diff) / sizeof($diff), 2)) : "?";
			$this->report["TRUNC{$truncate_competences}_avg_abs_diff"] = sizeof($diff) ? str_replace(",", ".", round(array_sum($diff_mod) / sizeof($diff_mod), 2)) : "?";
			$this->report["TRUNC{$truncate_competences}_avg_diff_sq"] = sizeof($diff) ? str_replace(",", ".", round(sqrt(array_sum($diff_sq) / sizeof($diff_sq)), 2)) : "?";
			$this->report["TRUNC{$truncate_competences}_big_diff_count"] = sizeof($diff) ? str_replace(",", ".", round($big_diff_count / sizeof($diff), 2)) : "?";
			$this->report["TRUNC{$truncate_competences}_diff"] = $this->get_math_vector($diff);
		}

		//file_put_contents(PATH_TMP . "/report.txt", join("\t", $this->report) . "\n", FILE_APPEND);

		$this->db->rollback();

		$report_node = $this->xdom->create_child_node("report");
		foreach ($this->report as $name => $value)
		{
			$report_node->create_child_node("param")
				->set_attr("name", $name)
				->set_attr("value", $value);
		}

		$clusters_node = $this->xdom->create_child_node("clusters");
		$cluster_num = 1;
		foreach ($this->matrix as $competences)
		{
			$cluster_node = $clusters_node->create_child_node("cluster");
			$cluster_node->set_attr("number", $cluster_num);
			foreach ($competences as $competence_id)
			{
				$cluster_node->create_child_node("competence")
					->set_attr("id", $competence_id)
					->set_attr("title", $this->competences[$competence_id]);
			}
			++$cluster_num;
		}
	}

	public function get_xml()
	{
		$this->xdom = xdom::create($this->name);

		$this->init_form();
		$input_ok = $this->init_input();
		$this->init_form_values();
		if (!$input_ok)
		{
			return $this->xdom->get_xml(true);
		}
		set_time_limit(60);
		$this->fetch_data();
		if (!sizeof($this->competences) or !sizeof($this->users))
		{
			return $this->xdom->get_xml(true);
		}
		if (!$this->math())
		{
			return $this->xdom->get_xml(true);
		}
		$this->analyze();
		return $this->xdom->get_xml(true);
	}

}

?>