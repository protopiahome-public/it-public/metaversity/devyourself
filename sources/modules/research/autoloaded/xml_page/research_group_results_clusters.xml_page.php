<?php

class research_group_results_clusters_xml_page extends base_research_xml_ctrl
{

	private $clustering_methods = array(
		"KMeans" => "K-means",
		"Agglomerate" => "Agglomerative",
		"Optimize" => "Local Optimization",
	);
	private $distances = array(
		"EuclideanDistance" => "Euclidean distance",
		"SquaredEuclideanDistance" => "Squared Euclidean distance",
		"NormalizedSquaredEuclideanDistance" => "Normalized squared Euclidean distance",
		"CosineDistance" => "Angular cosine distance",
		"CorrelationDistance" => "Correlation coefficient distance",
		"ManhattanDistance" => "Manhattan or \"city block\" distance",
	);
	// Input
	private $groups = array();
	private $project_id;
	private $project_row;
	private $competence_set_id;
	private $competence_set_row;
	private $user_mark_count_threshold = 20;
	private $competence_mark_count_threshold = 15;
	private $cluster_count = 10;
	private $clusters_to_leave = array();
	private $clustering_method;
	private $distance = false;
	private $normalize = false;
	private $groups_selected = array();
	// Data
	private $users;
	private $competences;
	private $code_users;
	private $code_vectors;
	private $results_initial;
	private $results_key;
	private $matrix;

	/**
	 * @var xdom
	 */
	private $xdom;

	private function init_form()
	{
		$projects = $this->db->fetch_column_values("SELECT * FROM project WHERE total_mark_count_calc > 0 ORDER BY title", "title", "id");
		$projects_node = $this->xdom->create_child_node("projects");
		foreach ($projects as $id => $title)
		{
			$projects_node->create_child_node("project")
				->set_attr("id", $id)
				->set_attr("title", $title);
		}

		$competence_sets = $this->db->fetch_column_values("SELECT * FROM competence_set WHERE total_mark_count_calc > 0 ORDER BY title", "title", "id");
		$competence_sets_node = $this->xdom->create_child_node("competence_sets");
		foreach ($competence_sets as $id => $title)
		{
			$competence_sets_node->create_child_node("competence_set")
				->set_attr("id", $id)
				->set_attr("title", $title);
		}

		$clustering_methods_node = $this->xdom->create_child_node("clustering_methods");
		foreach ($this->clustering_methods as $key => $title)
		{
			$clustering_methods_node->create_child_node("method")
				->set_attr("key", $key)
				->set_attr("title", $title);
		}

		$distances_node = $this->xdom->create_child_node("distances");
		foreach ($this->distances as $key => $title)
		{
			$distances_node->create_child_node("distance")
				->set_attr("key", $key)
				->set_attr("title", $title);
		}

		$this->groups = $this->db->fetch_all("
			SELECT id, title, user_count_calc
			FROM user_group
			ORDER BY id
		", "id");
		$groups_node = $this->xdom->create_child_node("groups");
		foreach ($this->groups as $id => $group_data)
		{
			$group_node = $groups_node->create_child_node("group")
				->set_attr("id", $id)
				->set_attr("title", $group_data["title"])
				->set_attr("user_count", $group_data["user_count_calc"]);
		}
	}

	private function init_input()
	{
		$this->project_id = GET("project_id");
		if (!$this->project_row = $this->check_entity_row("project", $this->project_id))
		{
			$this->xdom->set_attr("error", "BAD_PROJECT_ID");
			return false;
		}

		$this->competence_set_id = GET("competence_set_id");
		if (!$this->competence_set_row = $this->check_entity_row("competence_set", $this->competence_set_id))
		{
			$this->xdom->set_attr("error", "BAD_COMPETENCE_SET_ID");
			return false;
		}

		$this->user_mark_count_threshold = GET("user_mark_count_threshold");
		if (!is_good_id($this->user_mark_count_threshold))
		{
			$this->xdom->set_attr("error", "BAD_USER_MARK_COUNT_THRESHOLD");
			return false;
		}

		$this->competence_mark_count_threshold = GET("competence_mark_count_threshold");
		if (!is_good_id($this->competence_mark_count_threshold))
		{
			$this->xdom->set_attr("error", "BAD_COMPETENCE_MARK_COUNT_THRESHOLD");
			return false;
		}

		$this->cluster_count = GET("cluster_count");
		if (!is_good_id($this->cluster_count))
		{
			$this->xdom->set_attr("error", "BAD_CLUSTER_COUNT");
			return false;
		}

		$this->clusters_to_leave = explode(",", GET("clusters_to_leave"));
		foreach ($this->clusters_to_leave as $index => &$cluster_number)
		{
			$cluster_number = trim($cluster_number);
			if ($cluster_number === "")
			{
				unset($this->clusters_to_leave[$index]);
				continue;
			}
			if (!is_good_id($cluster_number))
			{
				$this->xdom->set_attr("error", "BAD_CLUSTER_TO_LEAVE");
				return false;
			}
		}
		$this->clusters_to_leave = array_flip($this->clusters_to_leave);
		ksort($this->clusters_to_leave);

		$this->clustering_method = GET("clustering_method");
		if (!isset($this->clustering_methods[$this->clustering_method]))
		{
			$this->xdom->set_attr("error", "BAD_CLUSTERING_METHOD");
			return false;
		}

		$this->distance = GET("distance");
		if (!isset($this->distances[$this->distance]))
		{
			$this->xdom->set_attr("error", "BAD_DISTANCE");
			return false;
		}

		$this->normalize = GET("normalize") === "1";

		foreach (GET_AS_ARRAY("groups") as $group_id)
		{
			if (is_good_id($group_id) and isset($this->groups[$group_id]))
			{
				$this->groups_selected[$group_id] = $this->db->fetch_column_values("
					SELECT user_id 
					FROM user_user_group_link 
					WHERE user_group_id = {$group_id}
				");
			}
		}

		return true;
	}

	private function init_form_values()
	{
		$this->xdom->set_attr("project_id", $this->project_id);
		$this->xdom->set_attr("competence_set_id", $this->competence_set_id);
		$this->xdom->set_attr("user_mark_count_threshold", $this->user_mark_count_threshold);
		$this->xdom->set_attr("competence_mark_count_threshold", $this->competence_mark_count_threshold);
		$this->xdom->set_attr("cluster_count", $this->cluster_count);
		$this->xdom->set_attr("clusters_to_leave", join(", ", array_keys($this->clusters_to_leave)));
		$this->xdom->set_attr("clustering_method", $this->clustering_method);
		$this->xdom->set_attr("distance", $this->distance);
		$this->xdom->set_attr("normalize", $this->normalize);
		$groups_selected_node = $this->xdom->create_child_node("groups_selected");
		foreach ($this->groups_selected as $id => $true)
		{
			$group_node = $groups_selected_node->create_child_node("group")
				->set_attr("id", $id);
		}
	}

	private function fetch_data()
	{
		$this->db->sql("
			CREATE TEMPORARY TABLE mark_stat
			SELECT competence_id, count(*) cnt
			FROM mark_calc
			WHERE type = 'personal'
				AND project_id = {$this->project_id}
				AND competence_set_id = {$this->competence_set_id}
			GROUP BY competence_id
			ORDER BY cnt DESC
		");

		$this->competences = $this->db->fetch_column_values("
			SELECT competence_id id, title
			FROM competence_calc
			WHERE competence_id IN (
				SELECT competence_id
				FROM mark_stat
				WHERE cnt >= {$this->competence_mark_count_threshold}
			)
		", "title", "id");

		$users_proposed = $this->all_matrix_elements($this->groups_selected);

		$this->users = $this->db->fetch_all("
			SELECT l.user_id id, u.login, IF(u.first_name <> '' OR u.last_name <> '', TRIM(CONCAT(u.first_name, ' ', u.last_name)), '<без имени>') full_name
			FROM user_competence_set_project_link l
			LEFT JOIN user u ON u.id = l.user_id
			WHERE l.competence_set_id = {$this->competence_set_id}
				AND l.project_id = {$this->project_id} 
				AND l.user_id IN (" . $this->array_to_IN_sql($users_proposed) . ")
				AND l.user_id IN (
					SELECT ratee_user_id
					FROM mark_calc
					WHERE 
						type = 'personal'
						AND project_id = {$this->project_id}
						AND competence_set_id = {$this->competence_set_id}
						AND competence_id IN (SELECT competence_id FROM mark_stat WHERE cnt >= {$this->competence_mark_count_threshold})
					GROUP BY ratee_user_id
					HAVING COUNT(*) >= {$this->user_mark_count_threshold}
				)
		", "id");

		foreach ($this->groups_selected as $group_id => &$group_users)
		{
			foreach ($group_users as $user_idx => $user_id)
			{
				if (!isset($this->users[$user_id]))
				{
					unset($group_users[$user_idx]);
				}
			}
		}
		unset($group_users);

		$results_math = new results_math(
				$this->competence_set_id,
				$user_id_array = array_keys($this->users),
				$competence_id_array = array_keys($this->competences),
				$ignore_user_selection = true,
				$calc_stat = false,
				$export_marks_log = false,
				$project_id = $this->project_id
		);
		$this->results_initial = $results_math->get_results();
		$this->results_key = "project_{$this->competence_set_id}_{$this->project_id}";
		$this->xdom->set_attr("used_mark_count", $results_math->get_used_mark_count());

		$code_users_array = array();
		$code_vectors_array = array();
		foreach ($this->users as $user_id => $user_params)
		{
			if (!isset($this->results_initial[$user_id]))
			{
				trigger_error("User results must exist: '{$user_id}'");
			}
			$user_data = $this->results_initial[$user_id];
			$vector = array();
			foreach ($this->competences as $competence_id => $competence_title)
			{
				$vector[$competence_id] = isset($user_data[$this->results_key][$competence_id]) ? $user_data[$this->results_key][$competence_id] : 0;
			}
			$code_vectors_array[$user_id] = $this->get_math_vector($vector, $this->normalize);
		}

		$this->code_users = $this->get_math_vector(array_keys($this->users));
		$this->code_vectors = $this->get_math_vector($code_vectors_array);

		if ($this->cluster_count > sizeof($this->competences))
		{
			$this->cluster_count = sizeof($this->competences);
		}

		$this->xdom->set_attr("competence_count", sizeof($this->competences));
		$this->xdom->set_attr("user_count", sizeof($this->users));
	}

	private function math()
	{
		$code = "";
		$code .= "data = Transpose[{$this->code_vectors}];\n";
		$code .= "ClusteringComponents[data, {$this->cluster_count}, 1, Method -> \"{$this->clustering_method}\", DistanceFunction -> {$this->distance}]";
		$this->xdom->create_child_node("code", $code);

		$error_desc = null;
		if (!$return_text = $this->math_run($code, $error_desc))
		{
			$this->xdom->set_attr("error", "MATH_RUN_FAILED");
			return false;
		}

		if (!$clusters = $this->parse_math_vector($return_text))
		{
			$this->xdom->set_attr("error", "MATH_MATRIX_PARSING_FAILED");
			return false;
		}

		$this->matrix = array();
		for ($i = 0; $i < $this->cluster_count; ++$i)
		{
			$this->matrix[$i] = array();
		}
		$competences_index = array_keys($this->competences);
		foreach ($clusters as $user_index => $cluster_number)
		{
			if ($cluster_number > $this->cluster_count)
			{
				$this->xdom->set_attr("error", "INCORRECT_CLUSTER_NUMBER_RECEIVED");
				return false;
			}
			$cluster_index = $cluster_number - 1;
			$this->matrix[$cluster_index][] = $competences_index[$user_index];
		}

		return true;
	}

	private function analyze()
	{
		if (sizeof($this->clusters_to_leave))
		{
			foreach ($this->matrix as $user_set_index => $user_id_array)
			{
				if (!isset($this->clusters_to_leave[$user_set_index + 1]))
				{
					unset($this->matrix[$user_set_index]);
				}
			}
		}

		if (!sizeof($this->matrix))
		{
			$this->xdom->set_attr("error", "NO_DATA");
			return false;
		}

		$results = array();
		foreach ($this->matrix as $vector_idx => $vector_competences)
		{
			$results[$vector_idx] = array(
				"group_stat" => array(),
				"cmp_stat" => array(),
			);
			// "group_stat"
			$max = 0;
			foreach ($this->groups_selected as $group_id => $group_users)
			{
				$results[$vector_idx]["group_stat"][$group_id] = array(
					"num" => 0,
					"cnt" => 0,
				);
				foreach ($group_users as $user_id)
				{
					foreach ($vector_competences as $competence_id)
					{
						$results[$vector_idx]["group_stat"][$group_id]["num"] += isset($this->results_initial[$user_id][$this->results_key][$competence_id]) ? $this->results_initial[$user_id][$this->results_key][$competence_id] : 0;
						$results[$vector_idx]["group_stat"][$group_id]["cnt"] += 1;
					}
				}
				$result = $results[$vector_idx]["group_stat"][$group_id]["cnt"] ? $results[$vector_idx]["group_stat"][$group_id]["num"] / $results[$vector_idx]["group_stat"][$group_id]["cnt"] : 0;
				$results[$vector_idx]["group_stat"][$group_id]["result"] = $result;
				$max = round($result, 2) > $max ? round($result, 2) : $max;
			}
			foreach ($this->groups_selected as $group_id => $group_users)
			{
				$results[$vector_idx]["group_stat"][$group_id]["is_leader"] = round($results[$vector_idx]["group_stat"][$group_id]["result"], 2) == $max;
			}
			if (sizeof($this->groups_selected) == 2)
			{
				$a = reset($results[$vector_idx]["group_stat"]);
				$b = next($results[$vector_idx]["group_stat"]);
				$a = $a["result"];
				$b = $b["result"];
				$results[$vector_idx]["diff"] = abs($a - $b);
			}

			// "cmp_stat"
			$results[$vector_idx]["cmp_stat"] = array();
			foreach ($vector_competences as $competence_id)
			{
				$results[$vector_idx]["cmp_stat"][$competence_id] = $this->competences[$competence_id];
			}
		}
		if (sizeof($this->groups_selected) == 2)
		{
			$results = array_sort($results, "diff", SORT_DESC);
		}

		// Output
		$clusters_node = $this->xdom->create_child_node("clusters");
		$cluster_num = 1;
		foreach ($results as $vector_idx => $results_data)
		{
			$vector_competences = $this->matrix[$vector_idx];
			
			$cluster_node = $clusters_node->create_child_node("cluster");
			$cluster_node->set_attr("number", $cluster_num);

			$competences_node = $cluster_node->create_child_node("competences");
			foreach ($results[$vector_idx]["cmp_stat"] as $competence_id => $competence_title)
			{
				$competences_node->create_child_node("competence")
					->set_attr("id", $competence_id)
					->set_attr("title", $competence_title);
			}

			$groups_node = $cluster_node->create_child_node("groups");
			foreach ($results[$vector_idx]["group_stat"] as $group_id => $group_data)
			{
				$groups_node->create_child_node("group")
					->set_attr("id", $group_id)
					->set_attr("title", $this->groups[$group_id]["title"])
					->set_attr("user_count", sizeof($this->groups_selected[$group_id]))
					->set_attr("result", round($group_data["result"], 2))
					->set_attr("is_leader", $group_data["is_leader"]);
			}
			++$cluster_num;
		}

		$group_users_node = $this->xdom->create_child_node("group_users");
		foreach ($this->groups_selected as $group_id => $user_id_array)
		{
			$group_node = $group_users_node->create_child_node("group");
			$group_node->set_attr("id", $group_id);
			foreach ($user_id_array as $user_id)
			{
				$group_node->create_child_node("user")
					->set_attr("id", $user_id)
					->set_attr("login", $this->users[$user_id]["login"])
					->set_attr("full_name", $this->users[$user_id]["full_name"]);
			}
		}
	}

	public function get_xml()
	{
		$this->xdom = xdom::create($this->name);

		$this->init_form();
		$input_ok = $this->init_input();
		$this->init_form_values();
		if (!$input_ok)
		{
			return $this->xdom->get_xml(true);
		}
		$this->fetch_data();
		if (!sizeof($this->competences) or !sizeof($this->users))
		{
			return $this->xdom->get_xml(true);
		}
		if (!$this->math())
		{
			return $this->xdom->get_xml(true);
		}
		$this->analyze();
		return $this->xdom->get_xml(true);
	}

}

?>