<?php

class research5_xml_page extends base_xml_ctrl
{

	private $competence_set_id;

	public function __construct($tmp)
	{
		$this->competence_set_id = 15;
		parent::__construct();
	}

	public function get_xml()
	{
		$competence_set_id = $this->competence_set_id;
		$this->db->select_db("cmp_research");
		$this->db->sql("SET SESSION group_concat_max_len = 8192");

		chdir(PATH_TMP);
		$file = file("report_full_not_normalized_r_x.txt");
		$result = array();
		foreach ($file as $line_idx => $line)
		{
			$parts = explode("\t", trim($line));
			//$result[] = "{{$parts[0]}, {$parts[3]}, {$parts[17]}}";
			$result[] = "{{$parts[0]}, {$parts[3]}, {$parts[11]}}"; // NC - no clean of competences
		}

		// Output
		$text = "{" . join(", ", $result) . "}\n\n\n";

		response::set_content_type_text();
		response::set_content($text);
		return true;
	}

}

?>