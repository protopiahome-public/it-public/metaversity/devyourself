<?php

class research4_xml_page extends base_xml_ctrl
{

	private $competence_set_id;

	public function __construct($tmp)
	{
		$this->competence_set_id = 15;
		parent::__construct();
	}

	public function get_xml()
	{
		$competence_set_id = $this->competence_set_id;
		$id_delta = 100000;
		chdir(PATH_TMP);
		$this->db->select_db("cmp_research");
		$this->db->sql("SET SESSION group_concat_max_len = 8192");

		// Clean
		$this->db->sql("DELETE FROM competence_full WHERE id >= {$id_delta}");
		$this->db->sql("DELETE FROM rate WHERE id >= {$id_delta}");
		$this->db->sql("DELETE FROM professiogram WHERE id >= {$id_delta}");
		$this->db->sql("DELETE FROM mark_calc WHERE competence_id >= {$id_delta}");

		// Users
		$users = $this->db->fetch_column_values("
			SELECT ratee_user_id
			FROM mark_calc
			GROUP BY ratee_user_id
			HAVING COUNT(*) >= 40
		");

		// Users' index
		$users_index = array_flip($users);

		// Results
		$results_math_initial = new results_math(
				$this->competence_set_id,
				$user_id_array = $users,
				$competence_id_array = array(),
				$ignore_user_selection = true,
				$calc_stat = false,
				$export_marks_log = false,
				$project_id = 11
		);
		$results_initial = $results_math_initial->get_results();

		for ($cmp_threshold = 1; $cmp_threshold <= 40; ++$cmp_threshold)
		{
			$initial_competences = $competences_code = $vectors_code = null;
			$this->fetch_initial_data($cmp_threshold, $initial_competences, $competences_code, $vectors_code);
			//$this->fetch_initial_data2($cmp_threshold, $users_index, $results_initial, $initial_competences, $competences_code, $vectors_code);

			$cluster_count = 1;

			//$cluster_count = 11; ////////////////////////////////////

			$cluster_step = 5;
			while ($cluster_count < sizeof($initial_competences) + $cluster_step)
			{
				set_time_limit(30);

				if ($cluster_count > sizeof($initial_competences))
				{
					$cluster_count = sizeof($initial_competences);
				}

				$code = "data={$vectors_code};pc = PrincipalComponents[N[data]];names={$competences_code};FindClusters[pc -> names, {$cluster_count}]";
				//$code = "data={$vectors_code};names={$competences_code};FindClusters[data -> names, {$cluster_count}]";
				$new_competences_text = $this->math_run($code);
				$new_competences = $this->parse_math_matrix($new_competences_text);
				$new_competences = $this->remove_single($new_competences);
				$convertable_competences = $this->all_matrix_elements($new_competences);

				$this->db->begin();

				// New data
				foreach ($new_competences as $competence_num => $sub_competences)
				{
					$competence_id = $id_delta + $competence_num;
					$old_competence_titles = $this->db->fetch_column_values("SELECT CONCAT('(', id, ') ', title) FROM competence_full WHERE id IN (" . join(",", $sub_competences) . ")");
					$new_competence_title = join(" / ", $old_competence_titles);
					$new_competence_title_escaped = $this->db->escape($new_competence_title);
					$this->db->sql("INSERT INTO competence_full (id, title, competence_set_id) VALUES ({$competence_id}, '{$new_competence_title_escaped}', {$competence_set_id})");
					$this->db->sql("INSERT INTO competence_calc (competence_id, title, competence_set_id) VALUES ({$competence_id}, '{$new_competence_title_escaped}', {$competence_set_id})");
				}
				$old_prefessiograms = $this->db->fetch_all("SELECT * FROM professiogram WHERE competence_set_id = {$competence_set_id} AND enabled = 1");
				$professiogram_ids = $this->array_column($old_prefessiograms, "id");
				$rate_ids = $this->array_column($old_prefessiograms, "rate_id");
				$this->db->sql("
					INSERT INTO rate (id, rate_type_id, competence_set_id, add_time, edit_time)
					SELECT id + {$id_delta}, rate_type_id, competence_set_id, add_time, edit_time
					FROM rate
					WHERE id IN (" . join(", ", $rate_ids) . ")
				");
				$this->db->sql("
					INSERT INTO professiogram (id, title, enabled, add_time, edit_time, rate_id, competence_set_id, descr, adder_user_id)
					SELECT id + {$id_delta}, CONCAT('*** AUTO *** ', title), enabled, add_time, edit_time, rate_id + {$id_delta}, competence_set_id, descr, adder_user_id
					FROM professiogram
					WHERE id IN (" . join(", ", $professiogram_ids) . ")
				");
				$convertable_competences_list_sql = sizeof($convertable_competences) ? join(", ", $convertable_competences) : "-1";
				$this->db->sql("
					INSERT INTO rate_competence_link (rate_id, competence_id, mark)
					SELECT rate_id + {$id_delta}, competence_id, mark
					FROM rate_competence_link
					WHERE rate_id IN (" . join(", ", $rate_ids) . ")
						AND competence_id NOT IN ({$convertable_competences_list_sql})
				");
				foreach ($new_competences as $competence_num => $sub_competences)
				{
					if (sizeof($sub_competences))
					{
						$competence_id = $id_delta + $competence_num;
						$this->db->sql("
							INSERT INTO rate_competence_link (rate_id, competence_id, mark)
							SELECT rate_id + {$id_delta}, {$competence_id}, ROUND(MAX(mark))
							FROM rate_competence_link
							WHERE rate_id IN (" . join(", ", $rate_ids) . ")
								AND competence_id IN (" . join(", ", $sub_competences) . ")
							GROUP BY rate_id
						");
						$this->db->sql("
							INSERT INTO mark_calc (id, project_id, precedent_group_id, precedent_flash_group_id, precedent_flash_group_user_link_id, ratee_user_id, rater_user_id, competence_id, competence_set_id, type, value, comment, precedent_id)
							SELECT id + {$id_delta}, project_id, precedent_group_id, precedent_flash_group_id, precedent_flash_group_user_link_id, ratee_user_id, rater_user_id, {$competence_id}, competence_set_id, type, MAX(value), comment, precedent_id
							FROM mark_calc
							WHERE competence_id IN (" . join(", ", $sub_competences) . ")
							GROUP BY precedent_flash_group_id, ratee_user_id, rater_user_id, type
						");
					}
				}

				// Professiograms
				$old_professiogram_id_array = $this->db->fetch_column_values("SELECT id FROM professiogram WHERE enabled = 1 AND competence_set_id = {$this->competence_set_id} AND id < {$id_delta} ORDER BY id");
				$new_professiogram_id_array = $this->db->fetch_column_values("SELECT id FROM professiogram WHERE enabled = 1 AND competence_set_id = {$this->competence_set_id} AND id >= {$id_delta} ORDER BY id");

				// Results
				$results_math = new results_math(
						$this->competence_set_id,
						$user_id_array = $users,
						$competence_id_array = array(),
						$ignore_user_selection = true,
						$calc_stat = false,
						$export_marks_log = false,
						$project_id = 11
				);
				$results = $results_math->get_results();

				$report = array();
				for ($truncate_competences = 0; $truncate_competences <= 1; ++$truncate_competences)
				{
					if ($truncate_competences)
					{
						$initial_competences_list_sql = sizeof($initial_competences) ? join(", ", $initial_competences) : "-1";
						$this->db->sql("
						DELETE FROM rate_competence_link
						WHERE competence_id NOT IN ({$initial_competences_list_sql}) AND competence_id < {$id_delta}
					");
					}

					// Professiogram data
					$old_professiogram_data_math = new professiogram_data_math();
					$old_professiogram_data = $old_professiogram_data_math->get_professiograms_data($old_professiogram_id_array);
					$old_professiogram_match_math = new rate_match_math($results, $old_professiogram_data);
					$new_professiogram_data_math = new professiogram_data_math();
					$new_professiogram_data = $new_professiogram_data_math->get_professiograms_data($new_professiogram_id_array);
					$new_professiogram_match_math = new rate_match_math($results, $new_professiogram_data);

					$diff = array();
					foreach ($old_professiogram_data as $prof_index => $data)
					{
						$old_user_rating = $old_professiogram_match_math->get_user_rating_for_rate($prof_index);
						$new_user_rating = $new_professiogram_match_math->get_user_rating_for_rate($prof_index);
						foreach ($users as $user_id)
						{
							$new_match = isset($new_user_rating[$user_id]) ? $new_user_rating[$user_id] : 0;
							$old_match = isset($old_user_rating[$user_id]) ? $old_user_rating[$user_id] : 0;
							if ($new_match < 0)
							{
								$new_match = 0;
							}
							$max_match = max($new_match, $old_match);
							if ($max_match > 10)
							{
								//$diff[] = $max_match ? (($new_match - $old_match) / $max_match * 100) : 0; //!
								$diff[] = $new_match - $old_match; //!
							}
						}
					}

					// Diff stat
					$diff_mod = array();
					$diff_sq = array();
					$diff_stat = array();
					$big_diff_count = 0;
					for ($i = -100; $i <= 100; ++$i)
					{
						$diff_stat[$i] = 0;
					}
					foreach ($diff as $delta)
					{
						$abs = abs($delta);
						$diff_mod[] = $abs;
						if ($abs > 10)
						{
							++$big_diff_count;
						}
						$diff_sq[] = $delta * $delta;
						++$diff_stat[$delta];
					}

					if ($truncate_competences == 0)
					{
						$result_competence_count = 116 - sizeof($initial_competences) + $cluster_count;
						$result_factor_local = round($cluster_count / sizeof($initial_competences) * 100);
						$result_factor_total = round($result_competence_count / 116 * 100);

						$report[] = $cmp_threshold;
						$report[] = sizeof($initial_competences);
						$report[] = $cluster_count;
						$report[] = $result_competence_count;
						$report[] = $result_factor_local;
						$report[] = $result_factor_total;
					}
					$report[] = max($diff);
					$report[] = min($diff);
					$report[] = str_replace(",", ".", round(array_sum($diff) / sizeof($diff), 2));
					$report[] = str_replace(",", ".", round(array_sum($diff_mod) / sizeof($diff_mod), 2));
					$report[] = str_replace(",", ".", round(sqrt(array_sum($diff_sq) / sizeof($diff_sq)), 2));
					$report[] = str_replace(",", ".", round($big_diff_count / sizeof($diff), 2));
				}

				file_put_contents("report.txt", join("\t", $report) . "\n", FILE_APPEND);

				$this->db->rollback();

				$cluster_count += $cluster_step;
				//break;
			}
		}

		$text = file_get_contents("report.txt") . "\n";
//		$text .= "Mark count = " . $results_math->get_used_mark_count() . "\n";
//		$text .= "Diff len = " . sizeof($diff) . "\n";
//		$text .= "Avg Abs = " . array_sum($diff_mod) / sizeof($diff_mod) . "\n";
//		$text .= "Avg = " . array_sum($diff) / sizeof($diff) . "\n";
//		$text .= "Max = " . max($diff) . "\n";
//		$text .= "Min = " . min($diff) . "\n";
//		$text .= "\n";
//		$text_blocks = array();
//		foreach ($diff_stat as $delta => $count)
//		{
//			$text_blocks[] = "{" . $delta . ", " . $count . "}";
//		}
//		$text .= "{" . join(", ", $text_blocks) . "}" . "\n";
//		$text .= "\n";
//		$text .= join(", ", $diff) . "\n";
//		$text .= "\n";
//		$text .= "\n";

		response::set_content_type_text();
		response::set_content($text);
		return true;
	}

	private function fetch_initial_data($cmp_threshold, &$initial_competences, &$competences_code, &$vectors_code)
	{
		// Fetching competences
		$initial_competences = $this->db->fetch_column_values("
			SELECT competence_id
			FROM mark_stat
			WHERE cnt >= {$cmp_threshold}
		");

		// Building vectors
		$marks = $this->db->fetch_all("
			SELECT ratee_user_id, precedent_id, CAST(GROUP_CONCAT(DISTINCT competence_id SEPARATOR ',') AS CHAR) competences
			FROM mark_calc
			WHERE type = 'personal' AND competence_id IN (SELECT competence_id FROM mark_stat WHERE cnt >= {$cmp_threshold})
			GROUP BY ratee_user_id, precedent_id
		");
		$blank_vector = array_fill(0, sizeof($marks), 0);
		$vectors = array();
		$vectors_len = array();
		foreach ($initial_competences as $competence_id)
		{
			$vectors[$competence_id] = $blank_vector;
			$vectors_len[$competence_id] = 0;
		}
		foreach ($marks as $index => $marks_row)
		{
			foreach (explode(",", $marks_row["competences"]) as $competence_id)
			{
				if (!$vectors[$competence_id][$index])
				{
					++$vectors_len[$competence_id];
				}
				$vectors[$competence_id][$index] = 1;
			}
		}
		$vectors_normalized = $vectors;
		foreach ($vectors_normalized as $competence_id => &$vector_normalized)
		{
			foreach ($vector_normalized as &$val)
			{
				//$val = $val ? ($val . "/" . "Sqrt[" . $vectors_len[$competence_id] . "]") : 0; //!
				$val = $val ? "1" : "0"; //!
			}
			unset($val);
		}
		unset($vector_normalized);

		// Output
		$competences_code = "{" . join(", ", array_keys($vectors)) . "}";

		$vectors_code_array = array();
		foreach ($vectors_normalized as $competence_id => $vector_normalized)
		{
			$vectors_code_array[] = "{" . join(", ", $vector_normalized) . "}";
		}
		$vectors_code .= "{" . join(", \n", $vectors_code_array) . "}";
	}

	private function fetch_initial_data2($cmp_threshold, $users_index, $results_initial, &$initial_competences, &$competences_code, &$vectors_code)
	{
		// Fetching competences
		$initial_competences = $this->db->fetch_column_values("
			SELECT competence_id
			FROM mark_stat
			WHERE cnt >= {$cmp_threshold}
		");

		$blank_vector = array_fill(0, sizeof($users_index), 0);
		$vectors = array();
		$vectors_sq_len = array();
		foreach ($initial_competences as $competence_id)
		{
			$vectors[$competence_id] = $blank_vector;
			$vectors_sq_len[$competence_id] = 0;
		}
		foreach ($results_initial as $user_id => $user_results)
		{
			$user_results_sum = $user_results["sum"];
			foreach ($user_results_sum as $competence_id => $mark)
			{
				if (isset($vectors[$competence_id]))
				{
					$vectors[$competence_id][$users_index[$user_id]] = $mark;
					$vectors_sq_len[$competence_id] += $mark * $mark;
				}
			}
		}
		$vectors_normalized = $vectors;
		foreach ($vectors_normalized as $competence_id => &$vector_normalized)
		{
			foreach ($vector_normalized as &$val)
			{
				//$val = $val ? ($val . "/" . "Sqrt[" . $vectors_sq_len[$competence_id] . "]") : 0; //!
				//$val = $val ? "1" : "0"; //!
				$val = $val;
			}
			unset($val);
		}
		unset($vector_normalized);

		// Output
		$competences_code = "{" . join(", ", array_keys($vectors)) . "}";

		$vectors_code_array = array();
		foreach ($vectors_normalized as $competence_id => $vector_normalized)
		{
			$vectors_code_array[] = "{" . join(", ", $vector_normalized) . "}";
		}
		$vectors_code .= "{" . join(", \n", $vectors_code_array) . "}";
	}

	protected function parse_math_matrix($text)
	{
		$text = preg_replace("/[\\x00-\\x20\\\\]/", "", $text);
		$text = preg_replace("/^\{\{/", "", $text);
		$text = preg_replace("/\}\}$/", "", $text);
		$rows = explode("},{", $text);
		$result = array();
		foreach ($rows as $row)
		{
			if (!trim($row))
			{
				dd("Bad format");
			}
			$row_items = explode(",", $row);
			if (sizeof($row_items) < 1)
			{
				dd("Bad format");
			}
			$row_array = array();
			foreach ($row_items as $row_item)
			{
				if (!is_good_id($row_item))
				{
					dd("Bad format");
				}
				$row_array[] = $row_item;
			}
			$result[] = $row_array;
		}
		return $result;
	}

	protected function remove_single($matrix)
	{
		$result = array();
		foreach ($matrix as $row)
		{
			if (sizeof($row) > 1)
			{
				$result[] = $row;
			}
		}
		return $result;
	}

	protected function all_matrix_elements($matrix)
	{
		$items = array();
		foreach ($matrix as $row)
		{
			foreach ($row as $item)
			{
				$items[$item] = true;
			}
		}
		return array_keys($items);
	}

	protected function array_column($array, $key)
	{
		$result = array();
		foreach ($array as $row)
		{
			foreach ($row as $_key => $value)
			{
				if ($_key === $key)
				{
					$result[] = $value;
				}
			}
		}
		return $result;
	}

	protected function math_run($code)
	{
		file_put_contents("in.txt", $code);
		$cmd = '"C:\Program Files\Wolfram Research\Mathematica\8.0\math.exe" -batchinput -batchoutput < in.txt > out.txt 2>&1';
		$output = $return_var = null;
		exec($cmd, $output, $return_var);
		if (!empty($output))
		{
			dd("Error", join("\n", $output));
		}
		if ($return_var != 0)
		{
			dd("Error: RETURN_CODE={$return_var}\n");
		}
		return file_get_contents("out.txt");
	}

}

?>