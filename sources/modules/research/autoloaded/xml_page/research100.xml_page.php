<?php

class research100_xml_page extends base_research_xml_ctrl
{

	public function get_xml()
	{
		$this->db->select_db("a_sp");

		//$this->fill_projects();
		//$this->fill_sections();
		//$this->fill_posts();
		$this->test();

		$text = "\n";

		response::set_content_type_text();
		response::set_content($text);
		return true;
	}

	private function test()
	{
		$project_id = 4;
		//$sections = $this->db->fetch_column_values("SELECT id FROM section WHERE project_id = {$project_id}");
		$sections = $this->db->fetch_column_values("SELECT id FROM section WHERE project_id BETWEEN 300 AND 400");
		$sections_alt = $this->db->fetch_column_values("SELECT id FROM section WHERE project_id BETWEEN 800 AND 900");
		$sections_all = $this->db->fetch_column_values("SELECT id FROM section WHERE (project_id BETWEEN 800 AND 900) OR (project_id BETWEEN 1 AND 100)");
		
		$this->db->sql("FLUSH TABLES");
		$this->db->fetch_all("
			SELECT
			-- SQL_CALC_FOUND_ROWS
			*
			FROM post
			-- USE INDEX (Index_8)
			WHERE 
			(project_id BETWEEN 750 AND 850) AND section_id NOT IN (" . $this->array_to_IN_sql($sections_alt) . ")
			-- section_id IN (" . $this->array_to_IN_sql($sections) . ")
			-- Alternative:
			-- (project_id BETWEEN 300 AND 400) OR (section_id IN (" . $this->array_to_IN_sql($sections_alt) . "))
			-- (section_id IN (" . $this->array_to_IN_sql($sections_all) . "))
			ORDER BY id DESC
			LIMIT 10
		");
		
		//$this->db->sql("FLUSH TABLES");
		//$this->db->sql("SELECT * FROM post WHERE id LIKE '%11%'");
	}
	
	private function fill_posts()
	{
		for ($k = 1; $k <= 100; $k++)
		{
			set_time_limit(30);
			$id_min = (10000 * ($k - 1)) + 1;
			$id_max = 10000 * $k;
			$data = array();
			for ($id = $id_min; $id <= $id_max; ++$id)
			{
				$section_id = mt_rand(1, 10000);
				$data[] = "({$id}, {$section_id})";
			}
			$data = join(", ", $data);
			$this->db->sql("INSERT INTO post (id, section_id) VALUES {$data}");
		}
		$this->db->sql("
			UPDATE post p
			LEFT JOIN section s ON s.id = p.section_id
			SET p.project_id = s.project_id
		");
	}

	private function fill_sections()
	{
		$data = array();
		for ($id = 1; $id <= 10000; ++$id)
		{
			$project_id = mt_rand(1, 1000);
			$data[] = "({$id}, {$project_id})";
		}
		$data = join(", ", $data);
		$this->db->sql("INSERT INTO section (id, project_id) VALUES {$data}");
	}

	private function fill_projects()
	{
		$data = array();
		for ($id = 1; $id <= 1000; ++$id)
		{
			$data[] = "({$id})";
		}
		$data = join(", ", $data);
		$this->db->sql("INSERT INTO project (id) VALUES {$data}");
	}

}

?>