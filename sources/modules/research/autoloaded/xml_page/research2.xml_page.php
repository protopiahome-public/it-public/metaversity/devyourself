<?php

class research2_xml_page extends base_xml_ctrl
{

	private $competence_set_id;

	public function __construct($tmp)
	{
		$this->competence_set_id = 15;
		parent::__construct();
	}

	public function get_xml()
	{
		$competence_set_id = $this->competence_set_id;

		$user_sets = array(
			array(75, 87, 107, 114, 154, 168, 176, 229),
			array(88, 106, 130, 174, 197, 208, 222, 228, 233, 291, 297, 334),
			array(90, 160, 186, 248, 323),
			array(91, 117, 158, 164),
			array(93, 150, 220, 221, 303),
			array(111, 271, 328),
			array(119, 120, 121, 204),
			array(129, 144, 194, 218, 226, 265, 321),
			array(136, 162, 165, 239, 283, 288, 314),
			array(169, 205),
		);
		
		$user_sets = array(
			array(93, 150, 220, 221, 303),
			array(129, 144, 194, 218, 226, 265, 321),
			array(136, 162, 165, 239, 283, 288, 314),
		);

		// Competences
		$competences = $this->db->fetch_column_values("
			SELECT competence_id id, title
			FROM competence_calc
			WHERE
				competence_set_id = {$competence_set_id}
				AND total_mark_count_calc > 0
		", "title", "id");

		// Professiograms
		$professiogram_id_array = $this->db->fetch_column_values("SELECT id FROM professiogram WHERE enabled = 1 AND competence_set_id = {$this->competence_set_id}");

		// Professiogram data
		$professiogram_data_math = new professiogram_data_math();
		$professiogram_data = $professiogram_data_math->get_professiograms_data($professiogram_id_array);

		// Results
		$results_math = new results_math(
				$this->competence_set_id,
				null,
				$competence_id_array = array_keys($competences),
				$ignore_user_selection = true,
				$calc_stat = false,
				$export_marks_log = false,
				$project_id = 11
		);
		$results = $results_math->get_results();
		
		// Stat over competences
		$blank_competences_stat = array();
		foreach ($competences as $competence_id => $competence_title)
		{
			$blank_competences_stat[$competence_id] = array(
				"title" => $competence_title,
				"mark_sum" => 0,
			);
		}
		$cmp_stat = $blank_competences_stat;
		$cmp_stat_sets = array();
		foreach ($user_sets as $user_set_index => $user_id_array)
		{
			$cmp_stat_sets[$user_set_index] = $blank_competences_stat;
		}
		
		// Building stat over competences
		$total_user_count = 0;
		foreach ($user_sets as $user_set_index => $user_id_array)
		{
			foreach ($user_id_array as $user_id)
			{
				foreach ($results[$user_id]["sum"] as $competence_id => $mark)
				{
					$cmp_stat[$competence_id]["mark_sum"] += $mark;
					$cmp_stat_sets[$user_set_index][$competence_id]["mark_sum"] += $mark;
				}
			}
			$total_user_count += sizeof($user_id_array);
		}
		
		// Calculating average mark
		foreach ($cmp_stat as $competence_id => &$stat_data)
		{
			$stat_data["result"] = round($stat_data["mark_sum"] / $total_user_count);
		}
		unset($stat_data);
		$cmp_stat = array_sort($cmp_stat, "result", SORT_DESC);
		foreach ($user_sets as $user_set_index => $user_id_array)
		{
			$user_count = sizeof($user_id_array);
			foreach ($cmp_stat_sets[$user_set_index] as $competence_id => &$stat_data)
			{
				$stat_data["result"] = round($stat_data["mark_sum"] / $user_count, 1);
				$stat_data["delta"] = $stat_data["result"] - $cmp_stat[$competence_id]["result"];
			}
			unset($stat_data);
			$cmp_stat_sets[$user_set_index] = array_sort($cmp_stat_sets[$user_set_index], "delta", SORT_DESC);
		}

		// Match calculation
		$professiogram_match_math = new rate_match_math($results, $professiogram_data);

		// Stat over professiograms
		$stat = array();
		foreach ($professiogram_data as $prof_index => $data)
		{
			$user_rating = $professiogram_match_math->get_user_rating_for_rate($prof_index);
			$max_match = reset($user_rating);
			$stat[$data["id"]] = array(
				"title" => $data["title"],
				"max_match" => $max_match,
				"match_sum" => 0,
			);
		}
		$stat_sets = array();
		foreach ($user_sets as $user_set_index => $user_id_array)
		{
			$stat_sets[$user_set_index] = array();
			foreach ($professiogram_data as $data)
			{
				$stat_sets[$user_set_index][$data["id"]] = array(
					"title" => $data["title"],
					"match_sum" => 0,
				);
			}
		}

		// Building stat
		$total_user_count = 0;
		foreach ($user_sets as $user_set_index => $user_id_array)
		{
			foreach ($user_id_array as $user_id)
			{
				$rating = $professiogram_match_math->get_rate_rating_for_user($user_id);
				foreach ($rating as $professiogram_id => $match)
				{
					$real_match = round($match / $stat[$professiogram_id]["max_match"] * 100);
					$stat[$professiogram_id]["match_sum"] += $real_match;
					$stat_sets[$user_set_index][$professiogram_id]["match_sum"] += $real_match;
				}
			}
			$total_user_count += sizeof($user_id_array);
		}

		// Calculating average match
		foreach ($stat as $professiogram_id => &$stat_data)
		{
			$stat_data["result"] = round($stat_data["match_sum"] / $total_user_count);
		}
		unset($stat_data);
		$stat = array_sort($stat, "result", SORT_DESC);

		foreach ($user_sets as $user_set_index => $user_id_array)
		{
			$user_count = sizeof($user_id_array);
			foreach ($stat_sets[$user_set_index] as $professiogram_id => &$stat_data)
			{
				$stat_data["result"] = round($stat_data["match_sum"] / $user_count);
				$stat_data["delta"] = $stat_data["result"] - $stat[$professiogram_id]["result"];
			}
			unset($stat_data);
			$stat_sets[$user_set_index] = array_sort($stat_sets[$user_set_index], "delta", SORT_DESC);
		}

		// Output
		$text = "\n";
		foreach ($user_sets as $user_set_index => $user_id_array)
		{
			// Output
			$text .= join(", ", $user_id_array) . "\n";
			foreach ($stat_sets[$user_set_index] as $professiogram_id => $stat_data)
			{
				$text .= $stat_data["title"] . " => " . $stat_data["result"] . " (" . $stat_data["delta"] . ")" . "\n";
			}
			$text .= "===\n";
			foreach ($cmp_stat_sets[$user_set_index] as $competence_id => $competence_data)
			{
				if ($competence_data["result"] < 1)
				{
					continue;
				}
				$text .= $competence_data["title"] . " => " . $competence_data["result"] . " (" . $competence_data["delta"] . ")" . "\n";
			}
			$text .= "\n";
		}

		response::set_content_type_text();
		response::set_content($text);
		return true;
	}

}

?>