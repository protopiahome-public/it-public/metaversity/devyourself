<?php

class research1_xml_page extends base_xml_ctrl
{

	private $project_id;
	private $competence_set_id;

	public function __construct($tmp)
	{
		$this->project_id = 11;
		$this->competence_set_id = 15;

		$this->project_id = 26;
		$this->competence_set_id = 30;

		parent::__construct();
	}

	public function get_xml()
	{
		$competence_set_id = $this->competence_set_id;

		$users = $this->db->fetch_column_values("
			SELECT user_id
			FROM user_competence_set_project_link
			WHERE competence_set_id = {$this->competence_set_id} AND project_id = {$this->project_id} AND total_mark_count_calc > 25
		");

		$competences = $this->db->fetch_column_values("
			SELECT competence_id id
			FROM competence_calc
			WHERE
				competence_set_id = {$competence_set_id}
				AND total_mark_count_calc > 0
		", "id");
		$i = 0;
		foreach ($competences as $idx => $competence_id)
		{
			++$i;
			if ($i > 10)
			{
				//unset($competences[$idx]);
			}
		}
		//$competences = array(498, 500, 501);
		// Results
		$results_math = new results_math(
				$competence_set_id,
				$user_id_array = $users,
				$competence_id_array = $competences,
				$ignore_user_selection = true,
				$calc_stat = false,
				$export_marks_log = false,
				$project_id = $this->project_id
		);
		$results = $results_math->get_results();

		$text = "\n";
		$text .= sizeof($results) . "\n\n";
		foreach ($competences as $competence_id)
		{
			$text .= $competence_id . ", ";
		}
		$text .= "\n\n\n";
		foreach ($results as $user_id => $user_data)
		{
			$vector = array();
			foreach ($competences as $competence_id)
			{
				$vector[$competence_id] = isset($user_data["sum"][$competence_id]) ? $user_data["sum"][$competence_id] : 0;
			}
			$text .= $user_id . ", ";
			//$text .= var_export($vector, true);
			//$text .= "{" . join(", ", $vector) . "} -> {$user_id}, \n";
			//$text .= "{" . join(", ", $vector) . "},\n";
		}

		$text .= "\n\n\n";

		response::set_content_type_text();
		response::set_content($text);
		return true;
	}

}

?>