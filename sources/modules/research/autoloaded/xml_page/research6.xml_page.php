<?php

class research6_xml_page extends base_xml_ctrl
{

	private $competence_set_id;

	public function __construct($tmp)
	{
		$this->competence_set_id = 15;
		parent::__construct();
	}

	public function get_xml()
	{
		$competence_set_id = $this->competence_set_id;
		$this->db->select_db("cmp_research");
		$this->db->sql("SET SESSION group_concat_max_len = 8192");

		chdir(PATH_TMP);
		//$data = $this->parse_math_matrix(file_get_contents("cmp_clusters_1_77.txt"));
		$data = $this->parse_math_matrix(file_get_contents("cmp_clusters_27_11_m.txt"));
		$data = $this->remove_single($data);
		
		$competences = $this->db->fetch_column_values("
			SELECT id, title FROM competence_full
		", "title", "id");
		
		// Output
		$text = "\n";
		foreach ($data as $cluster)
		{
			$tmp = array();
			foreach ($cluster as $competence_id)
			{
				$tmp[] = $competence_id;
			}
			$text .= "{" . join(", ", $tmp) . "},\n";
			foreach ($cluster as $competence_id)
			{
				$text .= "({$competence_id}) {$competences[$competence_id]}\n";
			}
			$text .= "\n";
		}

		response::set_content_type_text();
		response::set_content($text);
		return true;
	}
	
	protected function parse_math_matrix($text)
	{
		$text = preg_replace("/[\\x00-\\x20\\\\]/", "", $text);
		$text = preg_replace("/^\{\{/", "", $text);
		$text = preg_replace("/\}\}$/", "", $text);
		$rows = explode("},{", $text);
		$result = array();
		foreach ($rows as $row)
		{
			if (!trim($row))
			{
				dd("Bad format");
			}
			$row_items = explode(",", $row);
			if (sizeof($row_items) < 1)
			{
				dd("Bad format");
			}
			$row_array = array();
			foreach ($row_items as $row_item)
			{
				if (!is_good_id($row_item))
				{
					dd("Bad format");
				}
				$row_array[] = $row_item;
			}
			$result[] = $row_array;
		}
		return $result;
	}
	
	protected function remove_single($matrix)
	{
		$result = array();
		foreach ($matrix as $row)
		{
			if (sizeof($row) > 1)
			{
				$result[] = $row;
			}
		}
		return $result;
	}

}

?>