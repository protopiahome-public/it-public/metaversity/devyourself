<?php

/*
  1. Leave only necessary competence_set_id and project_id IN mark_calc
  delete from mark_calc where project_id != 26 and competence_set_id != 30;

  2.
  -- create table mark_stat
  SELECT competence_id, count(*) cnt
  FROM `mark_calc`
  where type = 'personal'
  group by competence_id
  HAVING cnt >= 25
  ORDER BY cnt DESC
 */

class research3_xml_page extends base_xml_ctrl
{

	private $competence_set_id;

	public function __construct($tmp)
	{
		$this->competence_set_id = 15;
		parent::__construct();
	}

	public function get_xml()
	{
		$competence_set_id = $this->competence_set_id;
		$this->db->select_db("cmp_research");
		$this->db->sql("SET SESSION group_concat_max_len = 8192");
		$cmp_threshold = 25;

		// Fetching competences
		$competence_id_array = $this->db->fetch_column_values("
			SELECT competence_id
			FROM mark_stat
			WHERE cnt >= {$cmp_threshold}
		");

		// Building vectors
		$marks = $this->db->fetch_all("
			SELECT ratee_user_id, precedent_id, CAST(GROUP_CONCAT(DISTINCT competence_id SEPARATOR ',') AS CHAR) competences
			FROM mark_calc
			WHERE type = 'personal' AND competence_id IN (SELECT competence_id FROM mark_stat WHERE cnt >= {$cmp_threshold})
			GROUP BY ratee_user_id, precedent_id
		");
		$blank_vector = array_fill(0, sizeof($marks), 0);
		$vectors = array();
		$vectors_len = array();
		foreach ($competence_id_array as $competence_id)
		{
			$vectors[$competence_id] = $blank_vector;
			$vectors_len[$competence_id] = 0;
		}
		foreach ($marks as $index => $marks_row)
		{
			foreach (explode(",", $marks_row["competences"]) as $competence_id)
			{
				if (!$vectors[$competence_id][$index])
				{
					++$vectors_len[$competence_id];
				}
				$vectors[$competence_id][$index] = 1;
			}
		}
		$vectors_normalized = $vectors;
		foreach ($vectors_normalized as $competence_id => &$vector_normalized)
		{
			foreach ($vector_normalized as &$val)
			{
				$val = $val ? ($val . "/" . "Sqrt[" . $vectors_len[$competence_id] . "]") : 0;//!
			}
			unset($val);
		}
		unset($vector_normalized);

		// Output
		$text = "\n";
		$text .= "Competences (" . sizeof($vectors) . "):\n";
		$text .= "{" . join(", ", array_keys($vectors)) . "}";
		$text .= "\n";
		$text .= "\n";
		$text .= "Vectors:\n";
		$text_array = array();
		foreach ($vectors_normalized as $competence_id => $vector_normalized)
		{
			$text_array[] = "{" . join(", ", $vector_normalized) . "}";
		}
		$text .= "{" . join(", \n", $text_array) . "}";
		$text .= "\n";
		$text .= "\n";


		response::set_content_type_text();
		response::set_content($text);
		return true;
	}

}

?>