<?php

abstract class base_research_xml_ctrl extends base_xml_ctrl
{

	protected function check_entity_row($type, $id)
	{
		if (!is_good_id($id))
		{
			return false;
		}
		$row = $this->db->get_row("SELECT * FROM {$type} WHERE id = {$id}");
		return $row;
	}

	protected function get_math_vector($items, $normalize = false)
	{
		if ($normalize)
		{
			$sq = 0;
			foreach ($items as $item)
			{
				$sq += $item * $item;
			}
			foreach ($items as &$item)
			{
				$item .= "/Sqrt[{$sq}]";
			}
			unset($item);
		}
		return "{" . join(", ", $items) . "}";
	}

	protected function math_run($code, &$error_desc)
	{
		global $config;

		chdir(PATH_TMP);
		file_put_contents("in.txt", $code);
		$cmd = '"' . $config["math_path"] . '" -batchinput -batchoutput < in.txt > out.txt 2>&1';
		$output = $return_var = null;
		exec($cmd, $output, $return_var);
		$error_desc = false;
		if (!empty($output))
		{
			$error_desc = join("\n", $output);
			return false;
		}
		if ($return_var != 0)
		{
			$error_desc = "RETURN_CODE={$return_var}";
			return false;
		}
		$result = file_get_contents_safe("out.txt");
		unlink_safe("in.txt");
		unlink_safe("out.txt");
		return $result;
	}

	protected function parse_math_vector($text)
	{
		$text = preg_replace("/[\\x00-\\x20\\\\]/", "", $text);
		$text = preg_replace("/^\{/", "", $text);
		$text = preg_replace("/\}$/", "", $text);
		$result = array();
		$items = explode(",", $text);
		foreach ($items as $item)
		{
			if (!is_good_id($item))
			{
				return false;
			}
			$result[] = $item;
		}
		return $result;
	}

	protected function parse_math_matrix($text)
	{
		$text = preg_replace("/[\\x00-\\x20\\\\]/", "", $text);
		$text = preg_replace("/^\{\{/", "", $text);
		$text = preg_replace("/\}\}$/", "", $text);
		$rows = explode("},{", $text);
		$result = array();
		foreach ($rows as $row)
		{
			if (!trim($row))
			{
				return false;
			}
			$row_items = explode(",", $row);
			if (sizeof($row_items) < 1)
			{
				return false;
			}
			$row_array = array();
			foreach ($row_items as $row_item)
			{
				if (!is_good_num($row_item))
				{
					return false;
				}
				$row_array[] = $row_item;
			}
			$result[] = $row_array;
		}
		return $result;
	}

	protected function remove_single($matrix)
	{
		$result = array();
		foreach ($matrix as $row)
		{
			if (sizeof($row) > 1)
			{
				$result[] = $row;
			}
		}
		return $result;
	}

	protected function all_matrix_elements($matrix)
	{
		$items = array();
		foreach ($matrix as $row)
		{
			foreach ($row as $item)
			{
				$items[$item] = true;
			}
		}
		return array_keys($items);
	}
	
	protected function array_to_IN_sql($array)
	{
		return sizeof($array) ? join(", ", $array) : "-1";
	}

}

?>
