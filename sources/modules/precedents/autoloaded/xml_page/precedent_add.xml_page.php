<?php

class precedent_add_xml_page extends base_dt_add_xml_ctrl
{

	// Settings
	protected $dt_name = "precedent";
	protected $axis_name = "add";
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct();
	}

	public function check_rights()
	{
		return $this->project_access->can_moderate_precedents();
	}

	protected function on_after_dt_init()
	{
		$dtf = $this->dt->get_field("groups");
		/* @var $dtf multi_link_dtf */
		$dtf->set_restrict_sql("WHERE project_id = {$this->project_id}");
	}

}

?>