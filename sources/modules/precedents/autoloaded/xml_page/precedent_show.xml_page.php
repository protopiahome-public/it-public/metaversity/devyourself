<?php

class precedent_show_xml_page extends base_xml_ctrl
{

	protected $project_id;
	protected $precedent_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($project_id, $precedent_id)
	{
		$this->project_id = $project_id;
		$this->precedent_id = $precedent_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct();
	}

	public function check_rights()
	{
		// @dm9: 2 запроса конечно жаль...
		// возможно, имеет смысл добавить $project_obj->is_finished()
		// и сделать $precedent_obj, $precedent_obj->is_public()
		// @ar решение такое:
		// 
		// для первого запроса нужно отнаследоваться от base_dt_show_xml_ctrl,
		// сделав соответствующую ось show в dt. И вытащить сюда, 
		// соответственно, access и что-то другое.
		// 
		// для второго запроса ты прав, нужно сделать get_project_status(),
		// который возвращает один из статусов, которые можно посмотреть в 
		// project_short: "NOT_ST" | "RUNNING" | "FINISHED". Алгоритм такой:
		// $state = $now < $start ? "NOT_ST" : (!$finish || $now < $finish ? "RUNNING" : "FINISHED");

		if ($this->project_access->can_moderate_precedents())
		{
			return true;
		}

		$is_precedent_public = $this->db->get_value("
			SELECT IF( access = 'public', 1, 0)
			FROM precedent
			WHERE id = {$this->precedent_id}
		");

		if ($is_precedent_public)
		{
			return true;
		}

		$is_project_finished = $this->db->get_value("
			SELECT IF (NOW() > finish_time, 1, 0)
			FROM project
			WHERE id = {$this->project_id}
		");

		if ($is_project_finished)
		{
			return true;
		}

		return false;
	}

	public function get_xml()
	{
		return $this->get_node_string(null, array("id" => $this->precedent_id));
	}

}

?>