<?php

class precedent_edit_ratees_xml_page extends base_xml_ctrl
{

	protected $project_id;
	protected $precedent_id;

	public function __construct($project_id, $precedent_id)
	{
		$this->project_id = $project_id;
		$this->precedent_id = $precedent_id;
		parent::__construct();
	}
	
	public function get_xml()
	{
		$xdom = xdom::create($this->name);
		$xdom
			->set_attr("project_id", $this->project_id)
			->set_attr("precedent_id", $this->precedent_id)
			->create_child_node("fake_mark_count")
				->set_attr("personal_mark_count_calc", 0)
				->set_attr("group_mark_count_calc", 0)
				->set_attr("group_mark_raw_count_calc", 0)
				->set_attr("total_mark_count_calc", 0);
		
		return $xdom->get_xml(true);
	}

}

?>