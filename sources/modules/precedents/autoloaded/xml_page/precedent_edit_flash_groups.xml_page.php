<?php

class precedent_edit_flash_groups_xml_page extends base_xml_ctrl
{

	protected $project_id;
	protected $precedent_id;

	public function __construct($project_id, $precedent_id)
	{
		$this->project_id = $project_id;
		$this->precedent_id = $precedent_id;
		parent::__construct();
	}
	
	public function get_xml()
	{
		$xdom = xdom::create($this->name);
		$xdom
			->set_attr("project_id", $this->project_id)
			->set_attr("precedent_id", $this->precedent_id);
		return $xdom->get_xml(true);
	}

}

?>