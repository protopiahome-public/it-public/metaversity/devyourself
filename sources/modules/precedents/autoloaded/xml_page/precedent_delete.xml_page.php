<?php

class precedent_delete_xml_page extends base_delete_xml_ctrl
{
	protected $db_table = "precedent";
	protected $project_id;
	protected $precedent_id;
	/**
	* @var project_obj 
	*/
	protected $project_obj;
	/**
	 * @var project_access
	 */
	protected $project_access;
	
	public function __construct($project_id, $precedent_id)
	{
		$this->project_id = $project_id;
		
		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();
		
		parent::__construct($precedent_id);
	}
	
	public function check_rights()
	{
		return $this->project_access->can_moderate_precedents();
	}
	
	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>