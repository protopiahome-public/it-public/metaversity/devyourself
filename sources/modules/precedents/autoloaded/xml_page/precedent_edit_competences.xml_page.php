<?php

class precedent_edit_competences_xml_page extends base_xml_ctrl
{

	protected $project_id;
	protected $precedent_id;
	protected $selected_competence_set_id;
	/**
	* @var project_obj 
	*/
	protected $project_obj;
	
	public function __construct($project_id, $precedent_id)
	{
		$this->project_id = $project_id;
		$this->precedent_id = $precedent_id;
		
		$this->project_obj = project_obj::instance($this->project_id);
		
		parent::__construct();
	}
	
	public function start()
	{
		if (($this->selected_competence_set_id = GET("id")))
		{
			if (!is_good_id($this->selected_competence_set_id))
			{
				return false;
			}
		}
		return true;
	}
	
	public function get_xml()
	{
		$default_competence_set_id = $this->project_obj->get_marks_competence_set_id();
		
		if (!$this->selected_competence_set_id)
		{
			$this->selected_competence_set_id = $default_competence_set_id;
		}
		
		$precedent_edit_helper = new precedent_edit_helper();
		$precedent_edit_helper->init($this->project_id, $this->precedent_id);
		$competence_groups = $precedent_edit_helper->competence_group_list();
				
		$this->xml_loader->add_xml_by_class_name("competence_set_fast_navigation_xml_ctrl", 0);
		$this->xml_loader->add_xml_by_class_name("competence_set_competences_xml_ctrl", $this->selected_competence_set_id);
		
		$xdom = xdom::create($this->name)
			->set_attr("project_id", $this->project_id)
			->set_attr("precedent_id", $this->precedent_id)
			->set_attr("default_competence_set_id", $default_competence_set_id)
			->set_attr("selected_competence_set_id", $this->selected_competence_set_id);
		
		foreach($competence_groups as $competence_group_id => $data)
		{
			$xdom->create_child_node("competence_group")
				->set_attr("id", $competence_group_id)
				->set_attr("has_marks", $data["has_marks"]);
		}
		
		return $xdom->get_xml(true);
	}

}

?>