<?php

class precedents_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"ctrl" => "user_short",
			"column" => "responsible_user_id",
			"param2" => "project_id",
		),
	);
	protected $xml_row_name = "precedent";
	// Internal
	protected $project_id;
	protected $selected_status;
	protected $selected_group_id;
	protected $page;

	/**
	 * @var precedents_user_summary_xml_ctrl 
	 */
	protected $precedents_user_summary;

	public function __construct($project_id, $page, precedents_user_summary_xml_ctrl $precedents_user_summary)
	{
		parent::__construct();
		$this->project_id = $project_id;
		$this->page = $page;
		$this->precedents_user_summary = $precedents_user_summary;
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 25));

		$processor = new sort_easy_processor();
		$processor->add_order("date", "add_time DESC", "add_time ASC");
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("access", "access DESC", "access ASC");
		$processor->add_order("marks", "total_mark_count_calc DESC", "total_mark_count_calc ASC");
		$this->add_easy_processor($processor);

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new text_sql_filter("search", "поиск по заголовку", array("title")));
		$this->add_easy_processor($processor);
	}

	public function load_data(select_sql $select_sql = null)
	{
		$precedents_id_string = join(",", $this->precedents_user_summary->get_filtered_precedents_id());
		$select_sql->add_select_fields("id, title, personal_mark_count_calc, group_mark_count_calc, group_mark_raw_count_calc, total_mark_count_calc, add_time, access, complete_descr, complete_members, complete_marks, responsible_user_id");
		$select_sql->add_from("precedent");
		$select_sql->add_where($precedents_id_string ? "id IN ({$precedents_id_string})" : "ISNULL(id)");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	protected function append_xml()
	{
		if (!$precedent_id_string = join(",", $this->get_selected_ids()))
		{
			// no precedents - no user links
			return "";
		}

		// load precedent to ratee link
		$xdom = xdom::create("precedent_to_ratee");
		$precedent_to_ratee = $this->db->fetch_all("SELECT precedent_id, ratee_user_id FROM precedent_ratee_calc WHERE precedent_id IN ({$precedent_id_string})");
		foreach ($precedent_to_ratee as $ptr)
		{
			$xdom->create_child_node("link")
				->set_attr("precedent_id", $ptr["precedent_id"])
				->set_attr("user_id", $ptr["ratee_user_id"]);
			$this->xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $ptr["ratee_user_id"], $this->project_id);
		}
		$xml = $xdom->get_xml(true);

		// load precedent to rater link
		$xdom = xdom::create("precedent_to_rater");
		$precedent_to_rater = $this->db->fetch_all("SELECT precedent_id, rater_user_id FROM precedent_rater_link WHERE precedent_id IN ({$precedent_id_string})");
		foreach ($precedent_to_rater as $ptr)
		{
			$xdom->create_child_node("link")
				->set_attr("precedent_id", $ptr["precedent_id"])
				->set_attr("user_id", $ptr["rater_user_id"]);
			$this->xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $ptr["rater_user_id"], $this->project_id);
		}
		$xml .= $xdom->get_xml(true);

		return $xml;
	}

}

?>