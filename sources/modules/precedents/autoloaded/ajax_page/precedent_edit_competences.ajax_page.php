<?php

class precedent_edit_competences_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"precedent_edit"
	);
	
	/**
	 * @var project_obj
	 */
	protected $project_obj;
	
	/**
	 * @var project_access 
	 */
	protected $project_access;
	
	/**
	 * @var precedent_edit_helper
	 */
	protected $precedent_edit_helper;
	
	protected $project_id;
	protected $precedent_id;
	protected $method;
	protected $competence_group_id;

	public function start()
	{
		$this->method = POST("method");
		$this->competence_group_id = POST("competence_group_id");
		if (!in_array($this->method, array("competence_group_add", "competence_group_remove"))
			or !is_good_id($this->competence_group_id))
		{
			return false;
		}

		return true;
	}

	public function commit()
	{
		switch ($this->method)
		{
			case "competence_group_add":
				return $this->precedent_edit_helper->competence_group_add($this->competence_group_id);

			case "competence_group_remove":
				return $this->precedent_edit_helper->competence_group_delete($this->competence_group_id);
		}
		return false;
	}

	public function get_data()
	{
		return array(
			"status" => "OK",
			"method" => $this->method
		);
	}

}

?>