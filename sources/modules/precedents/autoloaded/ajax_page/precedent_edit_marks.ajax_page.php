<?php

class precedent_edit_marks_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"precedent_edit"
	);
	
	/**
	 * @var project_obj
	 */
	protected $project_obj;
	
	/**
	 * @var project_access 
	 */
	protected $project_access;
	
	/**
	 * @var precedent_edit_helper
	 */
	protected $precedent_edit_helper;

	protected $project_id;
	protected $precedent_id;
	protected $type;
	protected $value;
	protected $comment;
	protected $ratee_entity_id;
	protected $rater_user_id;
	protected $competence_id;
	protected $competence_set_id;

	public function start()
	{
		$this->ratee_entity_id = POST("ratee_entity_id");
		$this->rater_user_id = POST("rater_user_id");
		$this->competence_id = POST("competence_id");
		$this->competence_set_id = POST("competence_set_id");
		$this->type = POST("type");
		$this->value = POST("value");

		if (!is_good_id($this->ratee_entity_id)
			or !is_good_id($this->rater_user_id)
			or !is_good_id($this->competence_id)
			or !is_good_id($this->competence_set_id)
			or !in_array($this->type, array("personal", "group"))
			or !is_good_num($this->value)
			or $this->value > 3)
		{
			return false;
		}
		
		$this->comment = trim(POST("comment"));
		
		return true;
	}

	public function check_rights()
	{
		global $user;
		/* @var $user user */

		if (($this->rater_user_id != $user->get_user_id())
			and !$this->project_access->can_moderate_marks())
		{
			return false;
		}

		return true;
	}

	public function commit()
	{
		switch ($this->type)
		{
			case "personal":
				return $this->precedent_edit_helper->personal_mark_set(
						$this->ratee_entity_id, $this->competence_set_id, $this->competence_id, $this->value, $this->comment, $this->rater_user_id
				);

			case "group":
				return $this->precedent_edit_helper->group_mark_set(
						$this->ratee_entity_id, $this->competence_set_id, $this->competence_id, $this->value, $this->comment, $this->rater_user_id
				);
		}
		return false;
	}

	public function get_data()
	{
		return array(
			"status" => "OK",
			"value" => $this->value,
			"comment" => $this->comment
		);
	}

}

?>