<?php

class precedent_edit_users_ajax_page extends base_ajax_ctrl
{
	protected $mixins = array(
		"project_before_start",
		"precedent_edit"
	);
	
	/**
	 * @var project_obj
	 */
	protected $project_obj;
	
	/**
	 * @var project_access 
	 */
	protected $project_access;
	
	/**
	 * @var precedent_edit_helper
	 */
	protected $precedent_edit_helper;
	
	protected $project_id;
	protected $precedent_id;
	protected $method;
	protected $user_id;

	public function start()
	{
		$this->user_id = POST("user_id");
		if (!is_good_id($this->user_id))
		{
			return false;
		}

		$this->method = POST("target") . "_" . POST("action");
		if (!in_array($this->method, array("rater_add", "rater_remove", "ratee_add", "ratee_remove")))
		{
			return false;
		}

		return true;
	}

	public function commit()
	{
		switch ($this->method)
		{
			case "rater_add":
				return $this->precedent_edit_helper->rater_add($this->user_id);

			case "rater_remove":
				return $this->precedent_edit_helper->rater_delete($this->user_id);

			case "ratee_add":
				return $this->precedent_edit_helper->ratee_add($this->user_id);

			case "ratee_remove":
				return $this->precedent_edit_helper->ratee_delete($this->user_id);
		}
		return false;
	}

	public function get_data()
	{
		return array(
			"status" => "OK",
			"method" => $this->method,
			"user_id" => $this->user_id
		);
	}

}

?>