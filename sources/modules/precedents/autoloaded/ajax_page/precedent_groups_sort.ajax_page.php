<?php

class precedent_groups_sort_ajax_page extends base_sort_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_admin_check_rights",
		"project_modify_sql"
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	/* Settings */
	protected $table_name = "precedent_group";

	public function clean_cache()
	{
		precedent_groups_cache_tag::init($this->project_id)->update();
	}

}

?>