<?php

class precedent_edit_flash_group_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"precedent_edit"
	);

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	
	/**
	 * @var project_access 
	 */
	protected $project_access;
	
	/**
	 * @var precedent_edit_helper
	 */
	protected $precedent_edit_helper;
	
	protected $project_id;
	protected $precedent_id;
	protected $method;
	protected $user_id;

	public function start()
	{
		$this->method = POST("method");
		if (!in_array($this->method, array("flash_group_create", "flash_group_delete", "flash_group_rename", "flash_group_ratee_add", "flash_group_ratee_remove")))
		{
			return false;
		}

		return true;
	}

	public function commit()
	{
		switch ($this->method)
		{
			case "flash_group_create":
				$flash_group_title = POST("title");
				return $this->precedent_edit_helper->flash_group_add($flash_group_title);

			case "flash_group_delete":
				$flash_group_id = POST("flash_group_id");
				return is_good_id($flash_group_id) ? $this->precedent_edit_helper->flash_group_delete($flash_group_id) : false;

			case "flash_group_rename":
				$flash_group_id = POST("flash_group_id");
				$flash_group_title = POST("title");
				return is_good_id($flash_group_id) ? $this->precedent_edit_helper->flash_group_rename($flash_group_id, $flash_group_title) : false;

			case "flash_group_ratee_add":
				$flash_group_id = POST("flash_group_id");
				$ratee_user_id = POST("user_id");
				return (is_good_id($flash_group_id) and is_good_id($ratee_user_id)) 
					? $this->precedent_edit_helper->flash_group_ratee_add($flash_group_id, $ratee_user_id) 
					: false;

			case "flash_group_ratee_remove":
				$flash_group_id = POST("flash_group_id");
				$ratee_user_id = POST("user_id");
				return (is_good_id($flash_group_id) and is_good_id($ratee_user_id)) ? $this->precedent_edit_helper->flash_group_ratee_delete($flash_group_id, $ratee_user_id) : false;
		}
		return false;
	}

	public function get_data()
	{
		return array(
			"status" => "OK",
			"method" => $this->method
		);
	}

}

?>