<?php

class precedent_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
	);
	// Settings
	protected $dt_name = "precedent";
	protected $axis_name = "edit";
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->project_access->can_moderate_precedents();
	}

	public function on_before_commit()
	{
		$this->update_array["project_id"] = "'{$this->project_id}'";
		$this->update_array["adder_user_id"] = $this->user->get_user_id();
		$this->update_array["last_editor_user_id"] = $this->user->get_user_id();
		return true;
	}

	public function clean_cache()
	{
		precedents_cache_tag::init($this->project_id)->update();
	}

}

?>