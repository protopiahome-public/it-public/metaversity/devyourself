<?php

class precedent_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"precedent_edit"
	);
	
	protected $precedent_id_field = "id";
	
	// Settings
	protected $db_table = "precedent";
	
	/**
	 * @var project_obj
	 */
	protected $project_obj;
	
	/**
	 * @var project_access 
	 */
	protected $project_access;
	
	/**
	 * @var precedent_edit_helper
	 */
	protected $precedent_edit_helper;
	
	protected $project_id;
	protected $precedent_id;
	
	public function on_before_check()
	{
		return $this->precedent_edit_helper->can_delete();
	}
	
	protected function modify_sql(select_sql $select_sql)
	{
		//dd(1);
		$select_sql->add_where("project_id = {$this->project_id}");
	}
	
	public function clean_cache()
	{
		precedents_cache_tag::init($this->project_id)->update();
		precedent_cache_tag::init($this->id)->update();
	}

}

?>