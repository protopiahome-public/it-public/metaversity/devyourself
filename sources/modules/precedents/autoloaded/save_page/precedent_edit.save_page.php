<?php

class precedent_edit_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
	);
	// Settings
	protected $dt_name = "precedent";
	protected $axis_name = "edit";
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->project_access->can_moderate_precedents();
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
	}

	public function clean_cache()
	{
		precedents_cache_tag::init($this->project_id)->update();
		precedent_cache_tag::init($this->id)->update();
	}

}

?>