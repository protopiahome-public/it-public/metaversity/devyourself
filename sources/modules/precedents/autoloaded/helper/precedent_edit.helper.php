<?php
class precedent_edit_helper
{
	const DEFAULT_FLASH_GROUP_TITLE = "Все участники";

	/**
	 * @var db
	 */
	private $db;
	private $project_id;
	private $precedent_id;
	private $default_flash_group_id = null;
	
	public function __construct()
	{
		global $db;
		$this->db = $db;
	}

	public function init($project_id, $precedent_id)
	{
		$precedent_exists = $this->db->row_exists("
			SELECT *
			FROM precedent
			WHERE
				id = {$precedent_id}
				AND project_id = {$project_id}"
		);
		if (!$precedent_exists)
		{
			return false;
		}

		$this->project_id = $project_id;
		$this->precedent_id = $precedent_id;
		$this->default_flash_group_id = null;
		return true;
	}

	public function rater_add($rater_user_id)
	{
		if (!$this->check_user_in_project_exists($rater_user_id))
		{
			// rater_user must be in project
			return false;
		}
		
		$this->db->sql("
			INSERT IGNORE INTO precedent_rater_link
			SET
				precedent_id = {$this->precedent_id},
				rater_user_id = {$rater_user_id}
		");
				
		return true;
	}

	public function rater_delete($rater_user_id)
	{
		if ($this->check_rater_has_marks($rater_user_id))
		{
			// cannot delete rater with marks
			return false;
		}

		$this->db->sql("
			DELETE FROM precedent_rater_link
			WHERE
				precedent_id = {$this->precedent_id}
				AND rater_user_id = {$rater_user_id}
		");
		return true;
	}

	public function ratee_add($ratee_user_id)
	{
		if (!$this->check_user_in_project_exists($ratee_user_id))
		{
			// ratee_user must be in project
			return false;
		}
		
		$this->fill_default_flash_group_id($create_if_null = true);
		$this->flash_group_ratee_add_raw($ratee_user_id, $this->default_flash_group_id);
		
		return true;
	}

	public function ratee_delete($ratee_user_id)
	{
		// delete ratee from precedent at all (default and any flash group)
		$check_ratee_has_personal_marks_in_precedent = $this->db->row_exists("
			SELECT *
			FROM mark_personal
			WHERE precedent_flash_group_user_link_id IN
			(
				SELECT pfgul.id
				FROM
					precedent_flash_group AS pfg,
					precedent_flash_group_user_link AS pfgul
				WHERE
					pfg.precedent_id = {$this->precedent_id}
					AND pfgul.precedent_flash_group_id = pfg.id
					AND pfgul.user_id = {$ratee_user_id}
			)
			LIMIT 1"
		);
		
		if ($check_ratee_has_personal_marks_in_precedent)
		{
			// can't delete ratee_user with personal marks
			return false;
		}
		
		$this->db->sql("
			DELETE FROM precedent_flash_group_user_link
			WHERE
				precedent_flash_group_id IN
				(
					SELECT id
					FROM precedent_flash_group
					WHERE precedent_id = {$this->precedent_id}
				)
				AND user_id = {$ratee_user_id}
		");
		$this->db->sql("
			DELETE FROM precedent_ratee_calc
			WHERE
				precedent_id = {$this->precedent_id}
				AND ratee_user_id = {$ratee_user_id}
		");
		return true;
	}
	
	public function flash_group_add($title)
	{
		$this->db->sql("
			INSERT INTO precedent_flash_group
			SET
				precedent_id = {$this->precedent_id},
				title = '{$this->db->escape($title)}',
				add_time = NOW()
		");
		return $this->db->get_last_id();
	}

	public function flash_group_delete($flash_group_id)
	{
		// TEST: cannot delete with p or g marks
		$this->fill_default_flash_group_id();
		
		if ($flash_group_id == $this->default_flash_group_id
			or $this->check_flash_group_has_marks($flash_group_id))
		{
			return false;
		}

		// flash_group_user_link will be deleted by foreign key
		$this->db->sql("
			DELETE FROM precedent_flash_group
			WHERE
				id = {$flash_group_id}
				AND precedent_id = {$this->precedent_id}
		");
		return true;
	}
	
	public function flash_group_rename($flash_group_id, $title)
	{
		$this->db->sql("
			UPDATE precedent_flash_group
			SET
				title = '{$this->db->escape($title)}'
			WHERE
				id = {$flash_group_id}
		");
		return true;
	}
	
	public function flash_group_ratee_add($flash_group_id, $ratee_user_id)
	{
		$this->fill_default_flash_group_id();
		
		if (!$this->check_ratee_in_precedent($ratee_user_id)
			or !$this->check_flash_group_exists($flash_group_id)
			or $this->default_flash_group_id == $flash_group_id)
		{
			// can't add ratee not in precedent
			// can't add to flash_group not in precedent
			// can't add to default flash group this way
			return false;
		}

		return $this->flash_group_ratee_add_raw($ratee_user_id, $flash_group_id);
	}
	
	public function flash_group_ratee_delete($flash_group_id, $ratee_user_id)
	{
		$this->fill_default_flash_group_id();
		
		if (($this->default_flash_group_id == $flash_group_id))
		{
			// can't delete from default flash group this way
			return false;
		}
		
		$check_ratee_has_personal_marks_in_group = $this->db->row_exists("
			SELECT *
			FROM mark_personal
			WHERE precedent_flash_group_user_link_id IN
			(
				SELECT id
				FROM precedent_flash_group_user_link AS pfgul
				WHERE
					precedent_flash_group_id = {$flash_group_id}
					AND pfgul.user_id = {$ratee_user_id}
			)
			LIMIT 1"
		);
		
		if ($check_ratee_has_personal_marks_in_group)
		{
			// can't delete ratee_user with personal marks
			return false;
		}
		
		$this->db->sql("
			DELETE FROM precedent_flash_group_user_link
			WHERE
				precedent_flash_group_id = {$flash_group_id}
				AND user_id = {$ratee_user_id}
		");
		return true;
	}

	public function competence_group_list()
	{
		// get competences groups, selected or with marks
		$groups_selected = $this->db->fetch_all("
			SELECT
				competence_group_id,
				0 AS has_marks
			FROM precedent_competence_group_link
			WHERE precedent_id = {$this->precedent_id}
		", "competence_group_id");
		
		$competences_used = $this->db->fetch_all("
			SELECT competence_id
			FROM mark_calc
			WHERE precedent_id = {$this->precedent_id}
			GROUP BY competence_id
		", "competence_id");
		
		if (!$competences_used)
		{
			return $groups_selected;
		}
		
		$groups_used = $this->db->fetch_all("
			SELECT
				competence_group_id,
				1 AS has_marks
			FROM competence_link
			WHERE competence_id IN (" . join(",", array_keys($competences_used)) . ")
			GROUP BY competence_group_id
		", "competence_group_id");
		$result = $groups_used + $groups_selected;
		return $result;
	}
	
	public function competence_group_add($competence_group_id)
	{
		$is_not_leaf = $this->db->row_exists("
			SELECT *
			FROM competence_group
			WHERE parent_id = {$competence_group_id}
			LIMIT 1
		");
		
		$is_have_competences = $this->db->row_exists("
			SELECT *
			FROM competence_link
			WHERE competence_group_id = {$competence_group_id}
			LIMIT 1
		");
		if ($is_not_leaf or !$is_have_competences) {
			return false;
		}
		
		$this->db->sql("
			INSERT IGNORE INTO precedent_competence_group_link
				(precedent_id, competence_group_id, competence_set_id)
				SELECT
					{$this->precedent_id},
					{$competence_group_id},
					competence_set_id
				FROM competence_group
				WHERE id = {$competence_group_id}
		");
		return true;
	}

	public function competence_group_delete($competence_group_id)
	{
		if ($this->check_competence_group_has_marks($competence_group_id))
		{
			// cannot delete competence_group with marks
			return false;
		}

		$this->db->sql("
			DELETE FROM precedent_competence_group_link
			WHERE
				competence_group_id = {$competence_group_id}
				AND precedent_id = {$this->precedent_id}
		");
		return true;
	}

	public function personal_mark_set($link_id, $competence_set_id, $competence_id, $value, $comment = "", $rater_user_id = null)
	{
		global $user;
		/* @var $user user */

		if (!$this->check_userlink($link_id)
			or !$this->check_competence_in_set($competence_id, $competence_set_id))
		{
			return false;
		}

		$adder_user_id = $user->get_user_id();
		if (!$rater_user_id)
		{
			$rater_user_id = $adder_user_id;
		}

		if ($value == 0)
		{
			$this->db->sql("
				DELETE FROM mark_personal
				WHERE
					precedent_flash_group_user_link_id = {$link_id}
					AND competence_id = {$competence_id}
					AND rater_user_id = {$rater_user_id}
			");
		}
		else
		{
			$this->db->sql("
				INSERT INTO mark_personal
				SET
					precedent_flash_group_user_link_id = {$link_id},
					competence_id = {$competence_id},
					rater_user_id = {$rater_user_id},
					adder_user_id = {$adder_user_id},
					value = {$value},
					comment = '{$this->db->escape($comment)}'
				ON DUPLICATE KEY UPDATE
					adder_user_id = VALUES(adder_user_id),
					value = VALUES(value),
					comment = VALUES(comment)
			");
		}
		
		return true;
	}

	public function group_mark_set($flash_group_id, $competence_set_id, $competence_id, $value, $comment = "", $rater_user_id = null)
	{
		global $user;
		/* @var $user user */

		if (!$this->check_flash_group_exists($flash_group_id)
			or !$this->check_competence_in_set($competence_id, $competence_set_id))
		{
			return false;
		}
/*		
		$flash_group_has_ratees = $this->db->row_exists("
			SELECT *
			FROM precedent_flash_group_user_link
			WHERE precedent_flash_group_id = {$flash_group_id}
			LIMIT 1
		");
			
		if (!$flash_group_has_ratees)
		{
			return false;
		}
*/
		$adder_user_id = $user->get_user_id();
		if (!$rater_user_id)
		{
			$rater_user_id = $adder_user_id;
		}

		if ($value == 0)
		{
			$this->db->sql("
				DELETE FROM mark_group
				WHERE
					precedent_flash_group_id = {$flash_group_id}
					AND competence_id = {$competence_id}
					AND rater_user_id = {$rater_user_id}
			");
		}
		else
		{
			$this->db->sql("
				INSERT INTO mark_group
				SET
					precedent_flash_group_id = {$flash_group_id},
					competence_id = {$competence_id},
					rater_user_id = {$rater_user_id},
					adder_user_id = {$adder_user_id},
					value = {$value},
					comment = '{$this->db->escape($comment)}'
				ON DUPLICATE KEY UPDATE
					adder_user_id = VALUES(adder_user_id),
					value = VALUES(value),
					comment = VALUES(comment)
			");
		}
		
		return true;
	}

	public function can_delete()
	{
		if ($this->check_precedent_has_marks())
		{
			return false;
		}
	
		return true;
	}
	
	private function check_userlink($link_id)
	{
		return $this->db->row_exists("
			SELECT *
			FROM
				precedent_flash_group_user_link AS pfgul,
				precedent_flash_group AS pfg
			WHERE
				pfgul.id = {$link_id}
				AND pfg.id = pfgul.precedent_flash_group_id
				AND pfg.precedent_id = {$this->precedent_id}
		");
	}

	private function check_flash_group_exists($flash_group_id)
	{
		return $this->db->row_exists("
			SELECT *
			FROM precedent_flash_group
			WHERE
				id = {$flash_group_id}
				AND precedent_id = {$this->precedent_id}
		");
	}

	private function check_competence_in_set($competence_id, $competence_set_id)
	{
		return $this->db->row_exists("
			SELECT *
			FROM
				competence_full
			WHERE
				id = {$competence_id}
				AND competence_set_id = {$competence_set_id}
			LIMIT 1
		");
	}
	
	private function check_user_in_project_exists($user_id)
	{
		return $this->db->row_exists("
			SELECT *
			FROM user_project_link
			WHERE project_id = {$this->project_id}
			AND user_id  = {$user_id}
		");
	}
	
	private function check_ratee_in_precedent($ratee_user_id)
	{
		return $this->db->row_exists("
			SELECT *
			FROM precedent,
				precedent_flash_group_user_link AS pfgul
			WHERE
				precedent.id = {$this->precedent_id}
				AND pfgul.precedent_flash_group_id = precedent.default_flash_group_id
				AND user_id = {$ratee_user_id}
			LIMIT 1
		");
	}
	
	private function check_rater_has_marks($rater_user_id)
	{
		$personal_where = "
			precedent_flash_group_user_link_id IN
				(
					SELECT pfgul.id
					FROM
						precedent_flash_group AS pfg,
						precedent_flash_group_user_link AS pfgul
					WHERE
						pfg.precedent_id = {$this->precedent_id}
						AND pfgul.precedent_flash_group_id = pfg.id
				)
			AND rater_user_id = {$rater_user_id}
		";
		$group_where = "
			precedent_flash_group_id IN
				(
					SELECT id
					FROM
						precedent_flash_group AS pfg
					WHERE
						precedent_id = {$this->precedent_id}
				)
			AND rater_user_id = {$rater_user_id}
		";
		return $this->has_marks_by_where($personal_where, $group_where);
	}
	
	private function check_competence_group_has_marks($competence_group_id)
	{
		$personal_where = "
			precedent_flash_group_user_link_id IN
				(
					SELECT pfgul.id
					FROM
						precedent_flash_group AS pfg,
						precedent_flash_group_user_link AS pfgul
					WHERE
						pfg.precedent_id = {$this->precedent_id}
						AND pfgul.precedent_flash_group_id = pfg.id
				)
			AND competence_id IN (SELECT competence_id FROM competence_link WHERE competence_group_id = {$competence_group_id})
		";
		$group_where = "
			precedent_flash_group_id IN
				(
					SELECT id
					FROM
						precedent_flash_group AS pfg
					WHERE
						precedent_id = {$this->precedent_id}
				)
			AND competence_id IN (SELECT competence_id FROM competence_link WHERE competence_group_id = {$competence_group_id})
		";
		return $this->has_marks_by_where($personal_where, $group_where);
	}
	
	private function check_flash_group_has_marks($flash_group_id)
	{
		$personal_where = "
			precedent_flash_group_user_link_id IN
			(
				SELECT id
				FROM precedent_flash_group_user_link AS pfgul
				WHERE
					precedent_flash_group_id = {$flash_group_id}
			)
		";
		$group_where = "
			precedent_flash_group_id = {$flash_group_id}
		";
		return $this->has_marks_by_where($personal_where, $group_where);
	}
	
	private function check_precedent_has_marks()
	{
		$personal_where = "
			precedent_flash_group_user_link_id IN
				(
					SELECT pfgul.id
					FROM
						precedent_flash_group AS pfg,
						precedent_flash_group_user_link AS pfgul
					WHERE
						pfg.precedent_id = {$this->precedent_id}
						AND pfgul.precedent_flash_group_id = pfg.id
				)
		";
		$group_where = "
			precedent_flash_group_id IN
				(
					SELECT id
					FROM
						precedent_flash_group AS pfg
					WHERE
						precedent_id = {$this->precedent_id}
				)
		";
		return $this->has_marks_by_where($personal_where, $group_where);
	}
	
	private function has_marks_by_where($personal_where, $group_where)
	{
		$personal_mark_exists = $this->db->row_exists($sq1="
			SELECT *
			FROM mark_personal
			WHERE {$personal_where}
			LIMIT 1
		");
		
		$group_mark_exists = $this->db->row_exists($sq2="
			SELECT *
			FROM mark_group
			WHERE {$group_where}
			LIMIT 1
		");
			
		return $personal_mark_exists or $group_mark_exists;
	}
	
	private function fill_default_flash_group_id($create_if_null = false)
	{
		if ($this->default_flash_group_id)
		{
			return $this->default_flash_group_id;
		}

		$this->default_flash_group_id = $this->db->get_value("
			SELECT default_flash_group_id
			FROM precedent
			WHERE id = {$this->precedent_id}
		");
			
		if ($create_if_null and !$this->default_flash_group_id)
		{
			$default_flash_group_title_escaped = $this->db->escape(self::DEFAULT_FLASH_GROUP_TITLE);
			$this->db->sql("
				INSERT INTO precedent_flash_group
				SET
					precedent_id = {$this->precedent_id},
					title = '{$default_flash_group_title_escaped}',
					add_time = NOW()
			");
			$this->default_flash_group_id = $this->db->get_last_id();
			$this->db->sql("
				UPDATE precedent
				SET default_flash_group_id = {$this->default_flash_group_id}
				WHERE id = {$this->precedent_id}
			");
		}

		$this->default_flash_group_id;
	}

	private function flash_group_ratee_add_raw($ratee_user_id, $flash_group_id)
	{
		$this->db->sql("
			INSERT IGNORE INTO precedent_flash_group_user_link
			SET
				user_id = {$ratee_user_id},
				precedent_flash_group_id = {$flash_group_id}
		");
		$this->db->sql("
			INSERT IGNORE INTO precedent_ratee_calc
			SET
				precedent_id = {$this->precedent_id},
				ratee_user_id = {$ratee_user_id}
		");
		if ($this->db->get_affected_row_count())
		{
			$this->db->sql("
				UPDATE precedent
				SET ratee_count_calc = ratee_count_calc + 1
				WHERE id = {$this->precedent_id}
			");
		}

		$user_link_id = $this->db->get_value("
			SELECT id
			FROM precedent_flash_group_user_link
			WHERE
				user_id = {$ratee_user_id}
				AND precedent_flash_group_id = {$flash_group_id}
		");
		
		return $user_link_id;
	}
	
}

?>