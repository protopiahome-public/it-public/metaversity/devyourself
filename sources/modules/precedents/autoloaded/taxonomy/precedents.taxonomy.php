<?php

final class precedents_taxonomy extends base_taxonomy
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, project_obj $project_obj)
	{
		$this->project_obj = $project_obj;
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		$p = $this->get_parts_relative();

		$project_obj = $this->project_obj;
		$project_id = $project_obj->get_id();
		$project_access = $project_obj->get_access();

		//p/<project_name>/precedents/...
		if (!$project_obj->marks_are_on())
		{
			$this->xml_loader->set_error_404();
			$this->xml_loader->set_error_404_xslt("project_module_404", "project");
		}
		elseif (!$project_access->can_read_precedents())
		{
			$this->xml_loader->set_error_403();
			$this->xml_loader->set_error_403_xslt("precedents_403", "precedents");
		}
		elseif ($page = $this->is_page_folder($p[1]) and $p[2] === null)
		{
			$this->xml_loader->add_xml(new precedent_groups_xml_ctrl($project_id));
			$this->xml_loader->add_xml($precedents_user_summary = new precedents_user_summary_xml_ctrl($project_id, base_sql_filter::is_filter_active()));
			$this->xml_loader->add_xml_with_xslt(new precedents_xml_page($project_id, $page, $precedents_user_summary));
		}
		elseif ($p[1] === "add" and $p[2] === null)
		{
			if ($project_access->can_moderate_precedents())
			{
				$this->xml_loader->add_xml_with_xslt(new precedent_add_xml_page($project_id));
			}
			else
			{
				$this->xml_loader->set_error_403();
				$this->xml_loader->set_error_403_xslt("precedent_403", "precedents");
			}
		}
		elseif (is_good_id($precedent_id = $this->is_type_folder($p[1], "precedent")))
		{
			$this->xml_loader->add_xml_by_class_name("precedent_full_xml_ctrl", $precedent_id, $project_id);
			$this->xml_loader->add_xml_by_class_name("precedent_data_xml_ctrl", $precedent_id, $project_id);
			if ($p[2] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new precedent_show_xml_page($project_id, $precedent_id), null, "precedent_403");
			}
			elseif ($p[2] === "delete")
			{
				if ($project_access->can_moderate_precedents())
				{
					$this->xml_loader->add_xml_with_xslt(new precedent_delete_xml_page($project_id, $precedent_id));
				}
				else
				{
					$this->xml_loader->set_error_403();
					$this->xml_loader->set_error_403_xslt("precedent_403", "precedents");
				}
			}
			elseif ($p[2] === "edit")
			{
				if ($project_access->can_moderate_precedents())
				{
					if ($p[3] === null)
					{
						$this->xml_loader->add_xml_with_xslt(new precedent_edit_xml_page($project_id, $precedent_id));
					}
					elseif ($p[3] === "ratees" and $p[4] === null)
					{
						$this->xml_loader->add_xml_by_class_name("project_members_xml_ctrl", $project_id);
						$this->xml_loader->add_xml_with_xslt(new precedent_edit_ratees_xml_page($project_id, $precedent_id));
					}
					elseif ($p[3] === "flash-groups" and $p[4] === null)
					{
						$this->xml_loader->add_xml_with_xslt(new precedent_edit_flash_groups_xml_page($project_id, $precedent_id));
					}
					elseif ($p[3] === "raters" and $p[4] === null)
					{
						$this->xml_loader->add_xml_by_class_name("project_members_xml_ctrl", $project_id);
						$this->xml_loader->add_xml_with_xslt(new precedent_edit_raters_xml_page($project_id, $precedent_id));
					}
					elseif ($p[3] === "competences" and $p[4] === null)
					{
						$this->xml_loader->add_xml_with_xslt(new precedent_edit_competences_xml_page($project_id, $precedent_id));
					}
					elseif ($p[3] === "marks" and $p[4] === null)
					{
						$this->xml_loader->add_xml_with_xslt(new precedent_edit_marks_xml_page($project_id, $precedent_id));
					}
				}
				else
				{
					$this->xml_loader->set_error_403();
					$this->xml_loader->set_error_403_xslt("precedent_403", "precedents");
				}
			}
		}
	}

}

?>