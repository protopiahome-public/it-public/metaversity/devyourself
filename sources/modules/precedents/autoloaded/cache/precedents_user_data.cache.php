<?php

class precedents_user_data_cache extends base_cache
{

	public static function init($project_id, $user_id)
	{
		$tag_keys = array();
		$tag_keys[] = precedents_cache_tag::init($project_id)->get_key();
		$tag_keys[] = precedents_user_data_cache_tag::init($project_id, $user_id)->get_key();
		return parent::get_cache(__CLASS__, $project_id, $user_id, $tag_keys);
	}

}

