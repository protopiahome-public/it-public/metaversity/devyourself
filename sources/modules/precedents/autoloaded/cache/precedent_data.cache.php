<?php

class precedent_data_cache extends base_cache
{

	public static function init($precedent_id)
	{
		$tag_keys = array();
		$tag_keys[] = precedent_data_cache_tag::init($precedent_id)->get_key();
		return parent::get_cache(__CLASS__, $precedent_id, $tag_keys);
	}

}

?>