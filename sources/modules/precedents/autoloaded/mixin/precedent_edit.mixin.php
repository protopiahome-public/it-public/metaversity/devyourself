<?php

class precedent_edit_mixin extends base_mixin
{

	protected $precedent_id_field;
	protected $precedent_id;
	protected $project_id;
	
	/**
	 * @var project_obj
	 */
	protected $project_obj;
	
	/**
	 * @var project_access 
	 */
	protected $project_access;
	
	/**
	 * @var precedent_edit_helper
	 */
	protected $precedent_edit_helper;

	public function on_before_start()
	{
		$this->precedent_id = POST(isset($this->precedent_id_field)
									? $this->precedent_id_field
									: "precedent_id");
		if (!is_good_id($this->precedent_id))
		{
			return false;
		}
		
		return true;
	}

	public function on_before_check()
	{
		$this->precedent_edit_helper = new precedent_edit_helper();
		return $this->precedent_edit_helper->init($this->project_id, $this->precedent_id);
	}

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->project_access->can_moderate_precedents();
	}
	
	public function clean_cache()
	{
		precedent_data_cache_tag::init($this->precedent_id)->update();
		precedents_cache_tag::init($this->project_id)->update();
	}
}

?>