<?php

class precedent_dt extends base_dt
{

	protected function init()
	{
		$this->add_block("main", "Основное");

		$dtf = new string_dtf("title", "Название");
		$dtf->set_max_length(255);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new text_html_dtf("descr", "Описание");
		$dtf->set_editor_height(300);
		$this->add_field($dtf, "main");
		
		$dtf = new select_dtf("access", "Доступ", array(
			"private" => "Скрыт до окончания проекта",
			"public" => "Открыт",
		));
		$dtf->set_default_key("private");
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("complete_descr", "Описание заполнено");
		$dtf->set_default_value(false);
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("complete_members", "Участники заполнены");
		$dtf->set_default_value(false);
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("complete_marks", "Оценки заполнены");
		$dtf->set_default_value(false);
		$this->add_field($dtf, "main");

		$dtf = new multi_link_dtf("groups", "Разделы", "precedent_group", "precedent_group_link", "precedent_id", "precedent_group_id");
		$dtf->set_max_height("200");
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("full", array(
			"title", "descr", "access", "complete_descr", "complete_members", "complete_marks", "groups",
		));

		$this->add_fields_in_axis("add", array(
			"title", "descr", "access", "groups",
		));

		$this->add_fields_in_axis("edit", array(
			"title", "descr", "access", "complete_descr", "complete_members", "complete_marks", "groups",
		));
	}

}

?>