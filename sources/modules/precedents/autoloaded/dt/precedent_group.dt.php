<?php

class precedent_group_dt extends base_dt
{

	protected $db_table = "precedent_group";

	protected function init()
	{
		$this->add_block("main", "Основное");

		$dtf = new string_dtf("title", "Название группы прецедентов");
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		//$dtf->set_unique("title", "Группа с таким заголовком уже создана");
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("edit", array("title"));
	}

}

?>