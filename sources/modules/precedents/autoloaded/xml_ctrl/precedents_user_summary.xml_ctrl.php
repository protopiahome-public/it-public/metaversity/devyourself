<?php

class precedents_user_summary_xml_ctrl extends base_xml_ctrl
{

	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $status = false;
	protected $group_id = false;
	protected $is_filter;
	protected $user_id;
	protected $precedent_list;
	protected $user_data;
	protected $precedent_group_counts = array();
	protected $precedents_id_string;
	// status= acceptable in URL
	protected $statuses_to_go = array();
	// status= viewable in right column
	protected $statuses_to_show = array();
	protected $filtered_precedents_id;

	public function __construct($project_id, $is_filter)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		if (!($this->is_filter = $is_filter))
		{
			$this->status = GET("status");
			$this->group_id = GET("group");
		}
		parent::__construct();
	}

	public function start()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		$this->user_id = $this->user->get_user_id();
		$this->load_precedent_list();

		if ($this->user_id)
		{
			$this->load_user_data();
			$this->calc_possible_statuses();
		}

		if ($this->status)
		{
			// check user data and status possibility
			if (!$this->user_data or !isset($this->statuses_to_go[$this->status]))
			{
				$this->set_redirect_url($this->request->get_full_prefix() . $this->request->get_folders_string() . "/");
				return true;
			}
			$this->remove_precedents_by_status();
		}

		if ($this->group_id)
		{
			// check if group_id exists
			if (!is_good_id($this->group_id) or !$this->db->get_value("SELECT count(*) FROM precedent_group WHERE id= {$this->group_id}"))
			{
				// no group, 404
				return false;
			}
		}

		if (!$this->project_access->can_moderate_precedents())
		{
			$this->remove_private_precedents();
		}

		$this->count_precedents_in_groups_and_filter_precedents();

		return true;
	}

	public function check_rights()
	{
		return true;
	}

	public function get_xml()
	{
		$xdom = xdom::create("precedents_user_summary")
			->set_attr("is_filter", $this->is_filter ? 1 : 0);

		// precedent counts by user status
		if ($this->user_data)
		{
			$statuses_node = $xdom->create_child_node("statuses")
				->set_attr("selected", $this->status ? $this->status : "all");

			foreach (array_keys($this->statuses_to_show) as $status)
			{
				$status_node = $statuses_node->create_child_node("status")->set_attr("type", $status);
				foreach ($this->user_data[$status]["counts"] as $key => $value)
				{
					$status_node->set_attr($key, $value);
				}
			}
		}

		// precedent counts by group
		$groups_node = $xdom->create_child_node("groups")
			->set_attr("selected", $this->group_id ? $this->group_id : "all")
			->set_attr("precedent_count", count($this->precedent_list));
		foreach ($this->precedent_group_counts as $precedent_group_id => $count)
		{
			$groups_node->create_child_node("group")
				->set_attr("id", $precedent_group_id)
				->set_attr("precedent_count", $count);
		}

		return $xdom->get_xml(true);
	}

	public function get_filtered_precedents_id()
	{
		return $this->filtered_precedents_id;
	}

	protected function load_precedent_list()
	{
		$cache = precedents_cache::init($this->project_id);
		if (($this->precedent_list = $cache->get()))
		{
			return;
		}

		$precedent_list = $this->db->fetch_all("
			SELECT
				id,
				access,
				adder_user_id,
				responsible_user_id,
				complete_descr,
				complete_members,
				complete_marks,
				personal_mark_count_calc,
				group_mark_count_calc,
				group_mark_raw_count_calc,
				total_mark_count_calc,
				ratee_count_calc
			FROM
				precedent
			WHERE
				project_id = {$this->project_id}", "id");

		foreach ($precedent_list as &$precedent_item)
		{
			$precedent_item["groups"] = array();
			$precedent_item["is_complete"] = $precedent_item["complete_descr"] and $precedent_item["complete_members"] and $precedent_item["complete_marks"];
		}

		if (($precedents_id_string = join(",", array_keys($precedent_list))))
		{
			$precedent_group_link = $this->db->fetch_all("
				SELECT
					precedent_id,
					precedent_group_id
				FROM
					precedent_group_link
				WHERE
					precedent_id IN ({$precedents_id_string})
			");
			foreach ($precedent_group_link as $pgl)
			{
				$precedent_item = &$precedent_list[$pgl["precedent_id"]];
				$precedent_item["groups"][] = (int) $pgl["precedent_group_id"];
			}
		}
		$cache->set($precedent_list);
		$this->precedent_list = $precedent_list;
	}

	protected function load_user_data()
	{
		$cache = precedents_user_data_cache::init($this->project_id, $this->user_id);
		if (($this->user_data = $cache->get()))
		{
			return;
		}
		$this->user_data = array(
			"ratee" => array(
				"precedents" => array(),
				"counts" => array(
					// all these attributes will go to XML later
					"personal_mark_count_calc" => 0,
					"group_mark_count_calc" => 0,
					"total_mark_count_calc" => 0,
				// "precedent_count" will be set below
				),
			),
			// "rater"=>, "rater-todo"=>, "untarated"=>, "responsible"=>, "responsible-todo"=>
			// have the same structure
			"rater" => array(
				"precedents" => array(),
				"counts" => array(
					"personal_mark_count_calc" => 0,
					"group_mark_count_calc" => 0,
					"group_mark_raw_count_calc" => 0,
					"total_mark_count_calc" => 0
				),
			),
			"rater-todo" => array(
				"precedents" => array(),
				"counts" => array()
			),
			"unrated" => array(
				"precedents" => array(),
				"counts" => array()
			),
			"responsible" => array(
				"precedents" => array(),
				"counts" => array(
					"personal_mark_count_calc" => 0,
					"group_mark_count_calc" => 0,
					"group_mark_raw_count_calc" => 0,
					"total_mark_count_calc" => 0
				),
			),
			"responsible-todo" => array(
				"precedents" => array(),
				"counts" => array()
			),
		);

		if (($this->precedents_id_string = join(",", array_keys($this->precedent_list))))
		{
			// only if there are some precedents in list
			$this->load_user_ratee_data();
			$this->load_user_rater_data();
			$this->load_user_unrated_data();
			$this->load_user_responsible_data();
		}

		// count precedents in all statuses
		foreach ($this->user_data as $status => &$precedent_data)
		{
			$precedent_data["counts"]["precedent_count"] = count($precedent_data["precedents"]);
		}
		unset($precedent_data);

		$cache->set($this->user_data);
	}

	protected function load_user_ratee_data()
	{
		$ratee_data = $this->db->fetch_all("
			SELECT *
			FROM precedent_ratee_calc
			WHERE ratee_user_id = {$this->user_id}
				AND precedent_id IN ($this->precedents_id_string)
		");
		foreach ($ratee_data as $row)
		{
			$this->user_data_propagate_counts("ratee", $row);
		}
	}

	protected function load_user_rater_data()
	{
		$rater_row = $this->db->fetch_all("
			SELECT *
			FROM precedent_rater_link
			WHERE rater_user_id = {$this->user_id}
				AND precedent_id IN ($this->precedents_id_string)
		");
		foreach ($rater_row as $row)
		{
			//dd($s,$row);
			$this->user_data_propagate_counts("rater", $row);
			$precedent_id = $row["precedent_id"];
			if (!$this->precedent_list[$precedent_id]["is_complete"])
			{
				$this->user_data["rater-todo"]["precedents"][] = $precedent_id;
			}
		}
	}

	protected function load_user_unrated_data()
	{
		// status=unrated, but only ratee_count_calc>1, not to offer mark himself
		$precedents = array_diff($this->user_data["ratee"]["precedents"], $this->user_data["rater"]["precedents"]);
		foreach ($precedents as $index => $precedent_id)
		{
			if ($this->precedent_list[$precedent_id]["ratee_count_calc"] < 2)
			{
				unset($precedents[$index]);
			}
		}
		$this->user_data["unrated"]["precedents"] = $precedents;
	}

	protected function load_user_responsible_data()
	{
		foreach ($this->precedent_list as $precedent_id => $precedent)
		{
			if ($precedent["responsible_user_id"] == $this->user_id)
			{
				// status=responsible
				$this->user_data_propagate_counts("responsible", $precedent, "id");
				if (!$precedent["is_complete"])
				{
					// status=responsible-todo
					$this->user_data["responsible-todo"]["precedents"][] = $precedent_id;
				}
			}
		}
	}

	protected function user_data_propagate_counts($status, $row, $id_field = "precedent_id")
	{
		$status_data = &$this->user_data[$status];
		$status_data["precedents"][] = (int) $row[$id_field];
		foreach ($status_data["counts"] as $count_field => &$count_value)
		{
			$count_value += $row[$count_field];
		}
		unset($count_value);
		unset($status_data);
	}

	protected function remove_private_precedents()
	{
		foreach ($this->precedent_list as $precedent_id => $precedent_data)
		{
			if ($precedent_data["access"] != "public")
			{
				if (!$this->user_data or
					(!in_array($precedent_id, $this->user_data["ratee"]["precedents"])
					and !in_array($precedent_id, $this->user_data["rater"]["precedents"]))
				)
					unset($this->precedent_list[$precedent_id]);
			}
		}
	}

	protected function remove_precedents_by_status()
	{
		$new_list = array();
		foreach ($this->user_data[$this->status]["precedents"] as $precedent_id)
		{
			$new_list[$precedent_id] = $this->precedent_list[$precedent_id];
		}
		$this->precedent_list = $new_list;
	}

	protected function count_precedents_in_groups_and_filter_precedents()
	{
		if ($this->group_id)
		{
			// была выбрана группа, прецеденты будем собирать
			$this->filtered_precedents_id = array();
		}
		else
		{
			// группа не выбрана, выбираем все прецеденты
			$this->filtered_precedents_id = array_keys($this->precedent_list);
		}

		foreach ($this->precedent_list as $precedent_id => $precedent)
		{
			foreach ($precedent["groups"] as $group_id)
			{
				if (is_null($group_count = &$this->precedent_group_counts[$group_id]))
				{
					$group_count = 0;
				}
				$group_count++;
				if ($this->group_id and $group_id == $this->group_id)
				{
					// собираем прецеденты из выбранной группы
					$this->filtered_precedents_id[] = $precedent_id;
				}
			}
		}
	}

	protected function calc_possible_statuses()
	{
		// это можно всем и всегда
		$this->statuses_to_go["ratee"] = 1;
		$this->statuses_to_show["ratee"] = 1;

		if ($this->user_data["rater"]["counts"]["precedent_count"])
		{
			// уже есть прецеденты, которые он оценивал - надо показать
			$this->statuses_to_go["rater"] = 1;
			$this->statuses_to_show["rater"] = 1;
		}

		if ($this->project_access->can_moderate_marks())
		{
			// он - модератор оценок
			$this->statuses_to_go["rater"] = 1;
			$this->statuses_to_go["rater-todo"] = 1;
			$this->statuses_to_go["unrated"] = 1;
			$this->statuses_to_show["rater"] = 1;
			if ($this->user_data["rater-todo"]["counts"]["precedent_count"])
			{
				$this->statuses_to_show["rater-todo"] = 1;
			}
			if ($this->user_data["unrated"]["counts"]["precedent_count"])
			{
				$this->statuses_to_show["unrated"] = 1;
			}
		}

		if ($this->project_access->can_moderate_precedents())
		{
			// он - модератор прецедентов
			$this->statuses_to_go["responsible"] = 1;
			$this->statuses_to_go["responsible-todo"] = 1;
			$this->statuses_to_show["responsible"] = 1;
			if ($this->user_data["responsible-todo"]["counts"]["precedent_count"])
			{
				$this->statuses_to_show["responsible-todo"] = 1;
			}
		}
	}

}

?>