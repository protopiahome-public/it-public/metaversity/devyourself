<?php

/*
 * @ar есть всё ещё непонятно, позвони, обсудим. Иначе удали этот комментарий
 * 
 * Не понятно, как так может быть...
  <precedent_data id="1850">
  <raters>
  <user id="28" comment="" personal_mark_count_calc="0" group_mark_count_calc="0" group_mark_raw_count_calc="4" total_mark_count_calc="0"/>
 * 
 * 
 * nuclear-sprint precedent-1888
 * 
 * 
 * http://cmp5.lan/p/nuclear-sprint/precedents/precedent-1892/
 */

// @ar заменить $a['x'] на $a["x"]
// @ar просмотреть все файлы с прецедентами на эту тему

class precedent_data_xml_ctrl extends base_xml_ctrl
{

	protected $precedent_id;
	protected $project_id;

	/**
	 * @var xdom
	 */
	protected $xdom;
	protected $flash_groups;
	protected $flash_groups_id_string;
	protected $default_flash_group_id;
	protected $default_competence_set_id;
	protected $competences_used = array();
	protected $competence_sets = array();
	protected $users = array();
	protected $xml;

	public function __construct($precedent_id, $project_id)
	{
		$this->precedent_id = $precedent_id;
		$this->project_id = $project_id;
		parent::__construct();
	}

	public function start()
	{
		// won't check project_id=>precedent_id consistency, done in precedent_full

		$cache = precedent_data_cache::init($this->precedent_id);
		if (!(list($this->xml, $this->users, $this->competences_used, $this->competence_sets) = $cache->get()))
		{
			// data is not cached, initialize empty arrays
			$this->competences_used = array();
			$this->users = array();
			$this->competence_sets = array();

			$this->default_competence_set_id = 0 + $this->db->get_value("
				SELECT marks_competence_set_id
				FROM project
				WHERE id = {$this->project_id}
			");
				
				
			$this->default_flash_group_id = $this->db->get_value("
				SELECT default_flash_group_id
				FROM precedent
				WHERE id = {$this->precedent_id}
			");

			// @ar Если не получилось - имеет смысл сделать следующее:
			// trigger_error("No flash group is assigned for the precedent '{$this->precedent_id}'")
			// Ставь защиты "от дурака", когда они срабатывают - очень приятно и полезно.

			if ($this->load_flash_groups())
			{
				$this->load_group_marks();
				$this->load_personal_marks();
				$this->load_competence_sets();
				$this->set_competence_set_id_in_marks();
			}
			$this->xml = $this->fill_xml();
		}

		// set cache
		$cache->set(array($this->xml, $this->users, $this->competences_used, $this->competence_sets));

		// load dependencies: users
		foreach ($this->users as $user_id => $tmp)
		{
			$this->xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $user_id, $this->project_id);
		}

		// ...: competences
		foreach ($this->competences_used as $competence_id => $tmp)
		{
			$this->xml_loader->add_xml_by_class_name("competence_short_xml_ctrl", $competence_id);
		}

		// ...: competence sets
		foreach ($this->competence_sets as $competence_set_id => $tmp)
		{
			$this->xml_loader->add_xml_by_class_name("competence_set_short_xml_ctrl", $competence_set_id);
		}

		return true;
	}

	public function get_xml()
	{
		return $this->xml;
	}

	public function fill_xml()
	{
		$this->xdom = xdom::create("precedent_data")
			->set_attr("id", $this->precedent_id)
			->set_attr("default_flash_group_id", $this->default_flash_group_id);

		$this->xml_add_raters();
		$this->xml_add_ratees();

		$this->rows_to_xml($this->competence_sets, $this->xdom, "competence_set", "competence_sets", "id");

		foreach ($this->flash_groups as $id => $flash_group)
		{
			$flash_group_node = $this->xdom->create_child_node("flash_group")
				->set_attr("id", $id)
				->set_attr("title", $flash_group["title"]);

			$this->rows_to_xml($flash_group["group_marks"], $flash_group_node, "mark", "marks");

			foreach ($flash_group["personal_marks"] as $user_id => $marks)
			{
				$user_node = $flash_group_node->create_child_node("user")
					->set_attr("id", $user_id)
					->set_attr("link_id", $flash_group["user_links"][$user_id]);

				$this->rows_to_xml($marks, $user_node, "mark", "marks");
			}
		}

		return $this->xdom->get_xml(true);
	}

	protected function load_flash_groups()
	{
		$this->flash_groups = $this->db->fetch_all("
			SELECT id, title
			FROM precedent_flash_group
			WHERE precedent_id = {$this->precedent_id}
		", "id");

		if (!$this->flash_groups)
		{
			return false;
		}

		$this->flash_groups_id_string = join(",", array_keys($this->flash_groups));

		foreach ($this->flash_groups as &$flash_group)
		{
			$flash_group["group_marks"] = array();
			$flash_group["personal_marks"] = array();
			$flash_group["user_links"] = array();
		}

		return true;
	}

	protected function load_group_marks()
	{
		$group_marks = $this->db->fetch_all("
			SELECT
				precedent_flash_group_id AS flash_group_id,
				competence_id,
				rater_user_id,
				adder_user_id,
				value,
				comment
			FROM mark_group
			WHERE precedent_flash_group_id IN ({$this->flash_groups_id_string})
		");

		foreach ($group_marks as &$mark)
		{
			$flash_group_id = $mark["flash_group_id"];
			unset($mark["flash_group_id"]);

			$this->flash_groups[$flash_group_id]["group_marks"][] = $mark;

			$this->competences_used[$mark["competence_id"]] = 1;

			$this->users[$mark["rater_user_id"]] = 1;
			$this->users[$mark["adder_user_id"]] = 1;
		}
	}

	protected function load_personal_marks()
	{
		$flash_group_user_link = $this->db->fetch_all("
			SELECT
				id,
				user_id,
				precedent_flash_group_id AS flash_group_id
			FROM precedent_flash_group_user_link
			WHERE precedent_flash_group_id IN ({$this->flash_groups_id_string})
		", "id");

		if (!$flash_group_user_link)
		{
			return;
		}

		foreach ($flash_group_user_link as $link)
		{
			$this->flash_groups[$link["flash_group_id"]]["personal_marks"][$link["user_id"]] = array();
			$this->flash_groups[$link["flash_group_id"]]["user_links"][$link["user_id"]] = $link["id"];
			$this->users[$link["user_id"]] = 1;
		}

		$personal_marks = $this->db->fetch_all("
			SELECT
				precedent_flash_group_user_link_id AS user_link_id,
				competence_id,
				rater_user_id,
				adder_user_id,
				value,
				comment
			FROM mark_personal
			WHERE precedent_flash_group_user_link_id IN (" . join(",", array_keys($flash_group_user_link)) . ")
		");

		foreach ($personal_marks as &$mark)
		{
			$link = $flash_group_user_link[$mark["user_link_id"]];
			unset($mark["user_link_id"]);
			$this->flash_groups[$link["flash_group_id"]]["personal_marks"][$link["user_id"]][] = $mark;

			$this->competences_used[$mark["competence_id"]] = 1;

			$this->users[$mark["rater_user_id"]] = 1;
			$this->users[$mark["adder_user_id"]] = 1;
		}
	}

	protected function load_competence_sets()
	{
		$this->competence_sets = $this->db->fetch_column_values("
			SELECT competence_set_id, 0 AS used
			FROM precedent_competence_group_link
			WHERE precedent_id = {$this->precedent_id}
			ORDER BY competence_set_id = {$this->default_competence_set_id} DESC,
				competence_set_id ASC
		", "used", "competence_set_id");

		if (!$this->competences_used)
		{
			return;
		}

		$competence_rows = $this->db->fetch_column_values("
			SELECT id, competence_set_id
			FROM competence_full
			WHERE id IN (" . join(",", array_keys($this->competences_used)) . ")
		", "competence_set_id", "id");

		foreach ($competence_rows as $competence_id => $competence_set_id)
		{
			$this->competence_sets[$competence_set_id] = 1;
			$this->competences_used[$competence_id] = (int) $competence_set_id;
		}
	}

	protected function set_competence_set_id_in_marks()
	{
		foreach ($this->flash_groups as &$flash_group)
		{
			// group marks
			foreach ($flash_group['group_marks'] as &$mark)
			{
				$mark['competence_set_id'] = array_key_exists($mark['competence_id'], $this->competences_used) ? $this->competences_used[$mark['competence_id']] : 0;
			}

			// personal marks
			foreach ($flash_group["personal_marks"] as &$marks)
			{
				// @ar не очень я такой код люблю
				// поставить бы unset() после циклов...
				foreach ($marks as &$mark)
				{
					$mark['competence_set_id'] = array_key_exists($mark['competence_id'], $this->competences_used) ? $this->competences_used[$mark['competence_id']] : 0;
				}
			}
		}
	}

	protected function xml_add_raters()
	{
		$raters = $this->db->fetch_all("
			SELECT
				rater_user_id AS id,
				comment,
				personal_mark_count_calc,
				group_mark_count_calc,
				group_mark_raw_count_calc,
				total_mark_count_calc
			FROM 
				precedent_rater_link
			WHERE
				precedent_id = {$this->precedent_id}
		");
		$this->rows_to_xml($raters, $this->xdom, "user", "raters");
		foreach ($raters as $rater)
		{
			$this->users[$rater["id"]] = 1;
		}
	}

	protected function xml_add_ratees()
	{
		$ratees = $this->db->fetch_all("
			SELECT
				ratee_user_id AS id,
				personal_mark_count_calc,
				group_mark_count_calc,
				total_mark_count_calc
			FROM 
				precedent_ratee_calc
			WHERE
				precedent_id = {$this->precedent_id}
		");
		$this->rows_to_xml($ratees, $this->xdom, "user", "ratees");
	}

	protected function rows_to_xml(array $rows, xnode $node, $tag_name, $container = null, $id_name = null)
	{
		if ($container)
		{
			$node = $node->create_child_node($container);
		}
		foreach ($rows as $id => $row)
		{
			$row_node = $node->create_child_node($tag_name);
			if ($id_name)
			{
				$row_node->set_attr($id_name, $id);
			}
			if (is_array($row))
			{
				if (isset($row["comment"]))
				{
					$row_node->create_child_node("comment", $row["comment"]);
					unset($row["comment"]);
				}
				foreach ($row as $key => $value)
				{
					$row_node->set_attr($key, $value);
				}
			}
		}
	}

}

?>