<?php

class precedent_groups_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "group";
	protected $xml_attrs = array("project_id");
	// Internal
	protected $project_id;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;
		parent::__construct();
	}

	protected function get_cache()
	{
		return precedent_groups_cache::init($this->project_id);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("precedent_group dt");
		$select_sql->add_where("dt.project_id = {$this->project_id}");
		$select_sql->add_order("dt.position");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>