<?php

class precedent_ordered_list_xml_ctrl extends base_simple_xml_ctrl
{

	protected $precedent_id_array;
	protected $data_one_row = false;
	protected $row_name = "precedent";
	protected $allow_error_404_if_no_data = false;
	protected $cache = false;
	protected $dependencies_ctrl = array("precedent" => "id");

	public function __construct($precedent_id_array)
	{
		$this->precedent_id_array = $precedent_id_array;

		parent::__construct();
	}

	protected function set_result()
	{
		if (!sizeof($this->precedent_id_array))
		{
			return array();
		}
		else
		{
			$this->result = "
				SELECT pr.id
				FROM precedent pr
				WHERE pr.id IN (" . join(",", $this->precedent_id_array) . ")
				ORDER BY pr.add_time DESC
			";
		}
	}

	protected function set_auxil_data()
	{
		
	}

}

?>