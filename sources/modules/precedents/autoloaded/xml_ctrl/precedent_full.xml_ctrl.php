<?php

class precedent_full_xml_ctrl extends base_dt_show_xml_ctrl
{

	protected $dt_name = "precedent";
	protected $axis_name = "full";
	
	protected $project_id;
	
	public function __construct($precedent_id, $project_id)
	{
		$this->project_id = $project_id;
		parent::__construct($precedent_id);
	}

	protected function get_cache()
	{
		return precedent_full_cache::init($this->id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>