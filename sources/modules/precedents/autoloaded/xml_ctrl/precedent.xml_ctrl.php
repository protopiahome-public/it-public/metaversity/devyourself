<?php

class precedent_xml_ctrl extends base_simple_xml_ctrl
{

	protected $precedent_id;

	protected $data_one_row = true;

	protected $row_name = "precedent";

	protected $allow_error_404_if_no_data = true;

	protected $cache = true;

	protected $cache_key_property_name = "precedent_id";

	public function __construct($precedent_id)
	{
		$this->precedent_id = $precedent_id;
		
		parent::__construct();
	}

	protected function set_result()
	{
		$this->result = "
			SELECT
				pr.id, pr.title, pr.descr, pr.add_time,
				s.id as precedent_group_id, s.title as precedent_group_title
			FROM precedent pr
			LEFT JOIN precedent_group s ON s.id = pr.precedent_group_id
			WHERE pr.id = {$this->precedent_id}
		";
	}

	protected function set_auxil_data()
	{
		
	}

}

?>