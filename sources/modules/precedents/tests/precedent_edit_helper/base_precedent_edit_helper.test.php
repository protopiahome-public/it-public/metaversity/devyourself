<?php

class base_precedent_edit_helper_test extends base_test
{

	/**
	 * @var precedent_edit_helper
	 */
	protected $edit_helper;

	public function set_up()
	{
		$this->db->sql("
			INSERT INTO USER (id,login)
			VALUES
				(1, 'root')
			ON DUPLICATE KEY UPDATE
				login = VALUES(login)
		");

		$this->db->sql("
			REPLACE INTO USER (id,login)
			VALUES
				(165, 'test_165'),
				(166, 'test_166'),
				(167, 'test_167'),
				(168, 'test_168'),
				(180, 'test_180'),
				(181, 'test_181'),
				(182, 'test_182'),
				(183, 'test_183'),
				(184, 'test_184'),
				(185, 'test_185'),
				(186, 'test_186')
		");

		$this->db->sql("REPLACE INTO competence_set (id, title) VALUES (7, 'test set')");
		$this->db->sql("REPLACE INTO competence_group (id, competence_set_id, title) VALUES (70, 7, 'test group')");
		$this->db->sql("REPLACE INTO competence_group (id, competence_set_id, title) VALUES (71, 7, 'test group 2')");
		$this->db->sql("REPLACE INTO competence_full (id, competence_set_id, title) VALUES (700, 7, 'test competence')");
		$this->db->sql("REPLACE INTO competence_full (id, competence_set_id, title) VALUES (701, 7, 'test competence 2')");
		$this->db->sql("REPLACE INTO competence_calc (competence_id, competence_set_id, title) VALUES (700, 7, 'test competence')");
		$this->db->sql("REPLACE INTO competence_calc (competence_id, competence_set_id, title) VALUES (701, 7, 'test competence 2')");

		$this->db->sql("DELETE FROM precedent WHERE project_id IN (1, 2)");
		$this->db->sql("DELETE FROM precedent_ratee_calc WHERE precedent_id IN (10,11,12,13)");
		$this->db->sql("DELETE FROM precedent_rater_link WHERE precedent_id IN (10,11,12,13)");
		$this->db->sql("DELETE FROM precedent_mark_adder WHERE precedent_id IN (10,11,12,13)");

		$this->db->sql("DELETE FROM mark_personal WHERE precedent_flash_group_user_link_id IN (100180, 100181, 100182, 100183, 101181, 102182, 103183)");
		$this->db->sql("DELETE FROM mark_group WHERE precedent_flash_group_id IN (100, 101, 102, 103)");



		$this->db->sql("
			REPLACE INTO project (id,title, name)
			VALUES
				(1, 'test', 'test')
		");

		$this->db->sql("
			REPLACE INTO precedent (id, project_id, title, descr, last_editor_user_id, adder_user_id)
			VALUES
				(10, 1, 'title', 'descr', 165, 165),
				(11, 1, 'title 1', 'descr 1', 165, 165),
				(12, 1, 'title 2', 'empty precedent', 165, 165),
				(13, 1, 'title 3', 'precedent with flash_group', 165, 165)
		");

		$this->db->sql("
			INSERT INTO precedent_rater_link (precedent_id, rater_user_id)
			VALUES
				(10, 165),
				(10, 166)
		");

		$this->db->sql("
			REPLACE INTO precedent_competence_group_link (precedent_id, competence_group_id, competence_set_id)
			VALUES
				(10, 70, 7),
				(11, 70, 7)
		");

		$this->db->sql("
			REPLACE INTO precedent_flash_group (id, title, precedent_id)
			VALUES
				(100,	'default', 10),
				(101, 'flash group 1', 10),
				(102, 'flash group 2', 10),
				(103, 'flash group 3', 10),
				(104, 'flash group empty', 10),
				(110,	'default', 11),
				(111, 'flash group 1.1', 11),
				(130,	'default', 13)
		");

		$this->db->sql("
			REPLACE INTO precedent_flash_group_user_link (id, user_id, precedent_flash_group_id)
			VALUES
				-- user in default flash group
				(100180, 180, 100),
				
				-- user in group without marks
				(100181, 181, 100),
				(101181, 181, 101),
				(100186, 186, 100),
				(101186, 186, 101),
							
				-- user with personal mark
				(100182, 182, 100),
				(102182, 182, 102),
				
				-- users with group marks
				(100183, 183, 100),
				(103183, 183, 103),
				(100184, 184, 100),
				(103184, 184, 103),
				
				-- precedent.1
				(110180, 180, 110)
				
		");

		$this->db->sql("
			REPLACE INTO mark_personal (precedent_flash_group_user_link_id, competence_id, rater_user_id, adder_user_id, value, comment)
			VALUES
				(102182, 700, 165, 165, 2, 'mark 2')
		");

		$this->db->sql("
			REPLACE INTO mark_group (precedent_flash_group_id, competence_id, rater_user_id, adder_user_id, value, comment)
			VALUES
				-- (101, 700, 165, 165, 2, 'mark 2'),
				(103, 700, 165, 165, 2, 'mark 2')
				
		");

		$this->db->sql("UPDATE precedent SET default_flash_group_id = 100 WHERE id = 10");
		$this->db->sql("UPDATE precedent SET default_flash_group_id = 110 WHERE id = 11");
		$this->db->sql("UPDATE precedent SET default_flash_group_id = 130 WHERE id = 13");

		$this->edit_helper = new precedent_edit_helper();
	}

	public function dummy_test()
	{
		
	}

}

?>