<?php
require_once("base_precedent_edit_helper.test.php");

class mark_calc_precedent_edit_helper_test extends base_precedent_edit_helper_test
{
	private $mark_calc;
	private $mark_calc_user;
	
	private $fields;
	
	public function set_up()
	{
		parent::set_up();
		
		$this->db->sql("REPLACE INTO competence_link (competence_id, competence_group_id) VALUES (700, 70)");
		
		$mark_calc_fields = "
			personal_mark_count_calc = 10,
			group_mark_count_calc = 20,
			group_mark_raw_count_calc = 30,
			total_mark_count_calc = 30
		";
		
		$user_mark_calc_fields = "
			personal_mark_count_calc = 10,
			group_mark_count_calc = 20,
			total_mark_count_calc = 30
		";
		
		$this->db->sql("UPDATE competence_set SET {$mark_calc_fields} WHERE id = 7");
		$this->db->sql("UPDATE competence_full SET {$mark_calc_fields} WHERE id = 700");
		$this->db->sql("UPDATE competence_calc SET {$mark_calc_fields} WHERE competence_id = 700");
		$this->db->sql("UPDATE project SET {$mark_calc_fields} WHERE id = 1 ");
		$this->db->sql("REPLACE INTO project_competence_set_link_calc SET {$mark_calc_fields}, project_id = 1, competence_set_id = 7");
		
		$this->db->sql("REPLACE INTO user_competence_set_link_calc SET {$user_mark_calc_fields}, user_id = 181, competence_set_id = 7");
		$this->db->sql("REPLACE INTO user_competence_set_link_calc SET {$user_mark_calc_fields}, user_id = 182, competence_set_id = 7");
		$this->db->sql("REPLACE INTO user_competence_set_link_calc SET {$user_mark_calc_fields}, user_id = 186, competence_set_id = 7");
		
		$this->db->sql("REPLACE INTO user_competence_set_project_link SET {$user_mark_calc_fields}, user_id = 181, competence_set_id = 7, project_id = 1");
		$this->db->sql("REPLACE INTO user_competence_set_project_link SET {$user_mark_calc_fields}, user_id = 182, competence_set_id = 7, project_id = 1");
		$this->db->sql("REPLACE INTO user_competence_set_project_link SET {$user_mark_calc_fields}, user_id = 186, competence_set_id = 7, project_id = 1");
		
		$this->db->sql("UPDATE precedent SET {$mark_calc_fields} WHERE id = 10");
		
		$this->db->sql("UPDATE user SET {$user_mark_calc_fields} WHERE id = 181");
		$this->db->sql("UPDATE user SET {$user_mark_calc_fields} WHERE id = 182");
		$this->db->sql("UPDATE user SET {$user_mark_calc_fields} WHERE id = 186");
		
		$this->db->sql("REPLACE INTO precedent_rater_link SET {$mark_calc_fields}, precedent_id = 10, rater_user_id = 166");
		$this->db->sql("REPLACE INTO precedent_rater_link SET {$mark_calc_fields}, precedent_id = 10, rater_user_id = 165");
		
		$this->db->sql("REPLACE INTO precedent_ratee_calc SET {$user_mark_calc_fields}, precedent_id = 10, ratee_user_id = 181");
		$this->db->sql("REPLACE INTO precedent_ratee_calc SET {$user_mark_calc_fields}, precedent_id = 10, ratee_user_id = 182");
		$this->db->sql("REPLACE INTO precedent_ratee_calc SET {$user_mark_calc_fields}, precedent_id = 10, ratee_user_id = 186");
		
		$this->db->sql("REPLACE INTO precedent_mark_adder SET {$mark_calc_fields}, precedent_id = 10, adder_user_id = 165");
		
	}
	
	public function personal_mark_add_calc_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$ok = $this->edit_helper->personal_mark_set(101181, 7, 700, 3, "zehr gut!", 166);
				
		$this->assert_true($ok);
		
		$this->load_calc_fields(1, 0, 0);
		
		$this->assert_equal($this->mark_calc, $this->get_competence_set_calc_fields(7));
		$this->assert_equal($this->mark_calc, $this->get_competence_full_calc_fields(700));
		$this->assert_equal($this->mark_calc, $this->get_competence_calc_calc_fields(700));
		
		$this->assert_equal($this->mark_calc, $this->get_project_calc_fields(1));
		$this->assert_equal($this->mark_calc, $this->get_precedent_calc_fields(10));
		
		$this->assert_equal($this->mark_calc, $this->get_rater_calc_fields(10, 166));
		$this->assert_equal($this->mark_calc, $this->get_adder_calc_fields(10, 165));
		
		$this->assert_equal($this->mark_calc_user, $this->get_user_calc_fields(181));
		$this->assert_equal($this->mark_calc_user, $this->get_ratee_calc_fields(10, 181));
		
		$this->assert_equal($this->mark_calc, $this->get_project_competence_set_link_calc_field(1, 7));
		$this->assert_equal($this->mark_calc_user, $this->get_user_competence_set_link_calc_fields(181, 7));
		$this->assert_equal($this->mark_calc_user, $this->get_user_competence_set_project_link_calc_fields(181, 7, 1));
		
		$this->db->test_end();
 	}

	public function personal_mark_delete_calc_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$ok = $this->edit_helper->personal_mark_set(102182, 7, 700, 0);
		
		$this->assert_true($ok);
		
		$this->load_calc_fields(-1, 0, 0);
		
		$this->assert_equal($this->mark_calc, $this->get_competence_set_calc_fields(7));
		$this->assert_equal($this->mark_calc, $this->get_competence_full_calc_fields(700));
		$this->assert_equal($this->mark_calc, $this->get_competence_calc_calc_fields(700));
		
		$this->assert_equal($this->mark_calc, $this->get_project_calc_fields(1));
		$this->assert_equal($this->mark_calc, $this->get_precedent_calc_fields(10));
		
		$this->assert_equal($this->mark_calc, $this->get_rater_calc_fields(10, 165));
		$this->assert_equal($this->mark_calc, $this->get_adder_calc_fields(10, 165));
		
		$this->assert_equal($this->mark_calc_user, $this->get_user_calc_fields(182));
		$this->assert_equal($this->mark_calc_user, $this->get_ratee_calc_fields(10, 182));
		
		$this->assert_equal($this->mark_calc, $this->get_project_competence_set_link_calc_field(1, 7));
		$this->assert_equal($this->mark_calc_user, $this->get_user_competence_set_link_calc_fields(182, 7));
		$this->assert_equal($this->mark_calc_user, $this->get_user_competence_set_project_link_calc_fields(182, 7, 1));

		$this->db->test_end();
	}

	public function group_mark_add_calc_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$ok = $this->edit_helper->group_mark_set(101, 7, 700, 3, "zehr gut!", 166);
		
		$this->assert_true($ok);
		
		$this->load_calc_fields(0, 2, 1);
		
		$this->assert_equal($this->mark_calc, $this->get_competence_set_calc_fields(7));
		$this->assert_equal($this->mark_calc, $this->get_competence_full_calc_fields(700));
		$this->assert_equal($this->mark_calc, $this->get_competence_calc_calc_fields(700));
		
		$this->assert_equal($this->mark_calc, $this->get_project_calc_fields(1));
		$this->assert_equal($this->mark_calc, $this->get_precedent_calc_fields(10));
		
		$this->assert_equal($this->mark_calc, $this->get_rater_calc_fields(10, 166));
		$this->assert_equal($this->mark_calc, $this->get_adder_calc_fields(10, 165));
		
		$this->assert_equal($this->mark_calc, $this->get_project_competence_set_link_calc_field(1, 7));
		
		$this->assert_equal($this->mark_calc_user, $this->get_user_calc_fields(181));
		$this->assert_equal($this->mark_calc_user, $this->get_ratee_calc_fields(10, 181));
		$this->assert_equal($this->mark_calc_user, $this->get_user_competence_set_link_calc_fields(181, 7));
		$this->assert_equal($this->mark_calc_user, $this->get_user_competence_set_project_link_calc_fields(181, 7, 1));
		
		$this->assert_equal($this->mark_calc_user, $this->get_user_calc_fields(186));
		$this->assert_equal($this->mark_calc_user, $this->get_ratee_calc_fields(10, 186));
		$this->assert_equal($this->mark_calc_user, $this->get_user_competence_set_link_calc_fields(186, 7));
		$this->assert_equal($this->mark_calc_user, $this->get_user_competence_set_project_link_calc_fields(186, 7, 1));

		$this->db->test_end();
	}

	public function group_mark_delete_calc_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$ok = $this->edit_helper->group_mark_set(103, 7, 700, 0, "");

		$this->assert_true($ok);

		$this->db->test_end();
	}

	private function load_calc_fields($add_personal, $add_group = 0, $add_raw = 0)
	{
		$this->mark_calc = array(
			"personal_mark_count_calc" => 10 + $add_personal,
			"group_mark_count_calc" => 20 + $add_group,
			"group_mark_raw_count_calc" => 30 + $add_raw,
			"total_mark_count_calc" => 30 + $add_personal + $add_group
		);
		$this->mark_calc_user = array(
			"personal_mark_count_calc" => 10 + $add_personal,
			"group_mark_count_calc" => 20 + $add_raw,
			"total_mark_count_calc" => 30 + $add_personal + $add_raw
		);
	}

	private function get_competence_set_calc_fields($competence_set_id)
	{
		return $this->db->get_row("
			SELECT
				personal_mark_count_calc,
				group_mark_count_calc,
				group_mark_raw_count_calc,
				total_mark_count_calc
			FROM competence_set
			WHERE id = {$competence_set_id}
		");
	}

	private function get_competence_full_calc_fields($competence_id)
	{
		return $this->db->get_row("
			SELECT
				personal_mark_count_calc,
				group_mark_count_calc,
				group_mark_raw_count_calc,
				total_mark_count_calc
			FROM competence_full
			WHERE id = {$competence_id}
		");
	}
	
	private function get_competence_calc_calc_fields($competence_id)
	{
		return $this->db->get_row("
			SELECT
				personal_mark_count_calc,
				group_mark_count_calc,
				group_mark_raw_count_calc,
				total_mark_count_calc
			FROM competence_calc
			WHERE competence_id = {$competence_id}
		");
	}

	private function get_project_calc_fields($project_id)
	{
		return $this->db->get_row("
			SELECT
				personal_mark_count_calc,
				group_mark_count_calc,
				group_mark_raw_count_calc,
				total_mark_count_calc
			FROM project
			WHERE id = {$project_id}
		");
	}

	private function get_precedent_calc_fields($precedent_id)
	{
		return $this->db->get_row("
			SELECT
				personal_mark_count_calc,
				group_mark_count_calc,
				group_mark_raw_count_calc,
				total_mark_count_calc
			FROM precedent
			WHERE id = {$precedent_id}
		");
	}

	private function get_user_calc_fields($user_id)
	{
		return $this->db->get_row("
			SELECT
				personal_mark_count_calc,
				group_mark_count_calc,
				total_mark_count_calc
			FROM user
			WHERE id = {$user_id}
		");
	}

	private function get_rater_calc_fields($precedent_id, $rater_user_id)
	{
		$row = $this->db->get_row("
			SELECT
				personal_mark_count_calc,
				group_mark_count_calc,
				group_mark_raw_count_calc,
				total_mark_count_calc
			FROM precedent_rater_link
			WHERE
				precedent_id = {$precedent_id}
				AND rater_user_id = {$rater_user_id}
		");
		
		return $row ? $row : array(
			"personal_mark_count_calc" => 0,
			"group_mark_count_calc" => 0,
			"group_mark_raw_count_calc" => 0,
			"total_mark_count_calc" => 0
			);
	}

	private function get_ratee_calc_fields($precedent_id, $ratee_user_id)
	{
		$row = $this->db->get_row("
			SELECT
				personal_mark_count_calc,
				group_mark_count_calc,
				total_mark_count_calc
			FROM precedent_ratee_calc
			WHERE
				precedent_id = {$precedent_id}
				AND ratee_user_id = {$ratee_user_id}
		");
				
		return $row ? $row : array(
			"personal_mark_count_calc" => 0,
			"group_mark_count_calc" => 0,
			"total_mark_count_calc" => 0
			);
	}

	private function get_adder_calc_fields($precedent_id, $adder_user_id)
	{
		$row = $this->db->get_row("
			SELECT
				personal_mark_count_calc,
				group_mark_count_calc,
				group_mark_raw_count_calc,
				total_mark_count_calc
			FROM precedent_mark_adder
			WHERE
				precedent_id = {$precedent_id}
				AND adder_user_id = {$adder_user_id}
		");
				
		return $row ? $row : array(
			"personal_mark_count_calc" => 0,
			"group_mark_count_calc" => 0,
			"group_mark_raw_count_calc" => 0,
			"total_mark_count_calc" => 0
			);
	}
	
	private function get_project_competence_set_link_calc_field($project_id, $competence_set_id)
	{
		$row = $this->db->get_row("
			SELECT
				personal_mark_count_calc,
				group_mark_count_calc,
				group_mark_raw_count_calc,
				total_mark_count_calc
			FROM project_competence_set_link_calc
			WHERE
				project_id = {$project_id}
				AND competence_set_id = {$competence_set_id}
		");
				
		return $row;
	}
	
	private function get_user_competence_set_link_calc_fields($user_id, $competence_set_id)
	{
		$row = $this->db->get_row("
			SELECT
				personal_mark_count_calc,
				group_mark_count_calc,
				total_mark_count_calc
			FROM user_competence_set_link_calc
			WHERE
				user_id = {$user_id}
				AND competence_set_id = {$competence_set_id}
		");
				
		return $row;
	}
	
	private function get_user_competence_set_project_link_calc_fields($user_id, $competence_set_id, $project_id)
	{
		$row = $this->db->get_row("
			SELECT
				personal_mark_count_calc,
				group_mark_count_calc,
				total_mark_count_calc
			FROM user_competence_set_project_link
			WHERE
				user_id = {$user_id}
				AND competence_set_id = {$competence_set_id}
				AND project_id = {$project_id}
				
		");
				
		return $row;
	}
}

?>