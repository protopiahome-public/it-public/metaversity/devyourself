<?php
require_once("base_precedent_edit_helper.test.php");

class marks_precedent_edit_helper_test extends base_precedent_edit_helper_test
{
	public function set_up()
	{
		parent::set_up();
		$this->db->sql("REPLACE INTO competence_link (competence_id, competence_group_id) VALUES (700, 70)");
		
	}

	public function personal_mark_set_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$ok = $this->edit_helper->personal_mark_set(101181, 7, 700, 3, "zehr gut!");
		
		$this->assert_true($ok);
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_personal 
			WHERE
				precedent_flash_group_user_link_id = 101181
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 3
				AND comment = 'zehr gut!'
		");
		
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_calc
			WHERE
				precedent_flash_group_user_link_id = 101181
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 3
				AND comment = 'zehr gut!'
		");

		$this->db->test_end();
	}

	public function personal_mark_set_as_user_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$ok = $this->edit_helper->personal_mark_set(101181, 7, 700, 3, "o, ja ja!", 166);
		
		$this->assert_true($ok);
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_personal 
			WHERE
				precedent_flash_group_user_link_id = 101181
				AND competence_id = 700
				AND rater_user_id = 166
				AND adder_user_id = 165
				AND value = 3
				AND comment = 'o, ja ja!'
		");

		$this->db->test_end();
	}

	public function personal_mark_set_change_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$ok = $this->edit_helper->personal_mark_set(102182, 7, 700, 3, "all right");
		
		$this->assert_true($ok);
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_personal 
			WHERE
				precedent_flash_group_user_link_id = 102182
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 3
				AND comment = 'all right'
		");

		$this->assert_db_row_count("mark_personal", 1, "
			WHERE
				precedent_flash_group_user_link_id = 102182
				AND competence_id = 700
				AND rater_user_id = 165
		");
		
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_calc
			WHERE
				precedent_flash_group_user_link_id = 102182
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 3
				AND comment = 'all right'
		");
		
		$this->assert_db_row_count("mark_calc", 1, "
			WHERE
				precedent_flash_group_user_link_id = 102182
				AND competence_id = 700
				AND rater_user_id = 165
		");

		$this->db->test_end();
	}

	public function personal_mark_set_unset_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$ok = $this->edit_helper->personal_mark_set(102182, 7, 700, 0, "");
		
		$this->assert_true($ok);
		$this->assert_db_row_not_exist("
			SELECT *
			FROM mark_personal
			WHERE
				precedent_flash_group_user_link_id = 102182
				AND competence_id = 700
				AND rater_user_id = 165
		");
		
		$this->assert_db_row_not_exist("
			SELECT *
			FROM mark_calc
			WHERE
				precedent_flash_group_user_link_id = 102182
				AND competence_id = 700
				AND rater_user_id = 165
		");

		$this->db->test_end();
	}

	public function personal_mark_set_not_in_precedent_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$mark_count = $this->db->get_value("SELECT COUNT(*) FROM mark_personal");
		$ok = $this->edit_helper->personal_mark_set(110180, 7, 700, 1, "o-lo-lo!");
		
		$this->assert_false($ok);
		$this->assert_equal($mark_count, $this->db->get_value("SELECT COUNT(*) FROM mark_personal"));

		$this->db->test_end();
	}

	public function group_mark_set_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$ok = $this->edit_helper->group_mark_set(101, 7, 700, 3, "zehr gut!");
		
		$this->assert_true($ok);
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_group 
			WHERE
				precedent_flash_group_id = 101
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 3
				AND comment = 'zehr gut!'
		");
		
		// 2 users in flash_group (id=101)
		
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_calc
			WHERE
				precedent_flash_group_id = 101
				AND precedent_flash_group_user_link_id = 101181
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 3
				AND comment = 'zehr gut!'
		");
		
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_calc
			WHERE
				precedent_flash_group_id = 101
				AND precedent_flash_group_user_link_id = 101186
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 3
				AND comment = 'zehr gut!'
		");

		$this->db->test_end();
	}

	public function group_mark_set_as_user_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$ok = $this->edit_helper->group_mark_set(101, 7, 700, 3, "o, ja ja!", 166);
		
		$this->assert_true($ok);
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_group
			WHERE
				precedent_flash_group_id = 101
				AND competence_id = 700
				AND rater_user_id = 166
				AND adder_user_id = 165
				AND value = 3
				AND comment = 'o, ja ja!'
		");

		$this->db->test_end();
	}

	public function group_mark_set_change_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$ok = $this->edit_helper->group_mark_set(103, 7, 700, 3, "all right");
		
		$this->assert_true($ok);
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_group
			WHERE
				precedent_flash_group_id = 103
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 3
				AND comment = 'all right'
		");
		$this->assert_db_row_count("mark_group", 1, "
			WHERE
				precedent_flash_group_id = 103
				AND competence_id = 700
		");
		
		// 2 users in flash_group (id=103)
		
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_calc
			WHERE
				precedent_flash_group_id = 103
				AND precedent_flash_group_user_link_id = 103183
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 3
				AND comment = 'all right'
		");
		
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_calc
			WHERE
				precedent_flash_group_id = 103
				AND precedent_flash_group_user_link_id = 103184
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 3
				AND comment = 'all right'
		");

		$this->db->test_end();
	}

	public function group_mark_set_unset_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$ok = $this->edit_helper->group_mark_set(103, 7, 700, 0, "");

		$this->assert_true($ok);
		$this->assert_db_row_not_exist("
			SELECT *
			FROM mark_group
			WHERE
				precedent_flash_group_id = 103
				AND competence_id = 700
				AND rater_user_id = 165
		");
		
		$this->assert_db_row_not_exist("
			SELECT *
			FROM mark_calc
			WHERE
				precedent_flash_group_id = 103
				AND competence_id = 700
				AND rater_user_id = 165
		");

		$this->db->test_end();
	}

	public function group_mark_set_not_in_precedent_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);

		$mark_count = $this->db->get_value("SELECT COUNT(*) FROM mark_group");
		$ok = $this->edit_helper->personal_mark_set(110, 7, 700, 1, "o-lo-lo!");
		
		$this->assert_false($ok);
		$this->assert_equal($mark_count, $this->db->get_value("SELECT COUNT(*) FROM mark_group"));
		
		$this->db->test_end();
	}
	
	public function member_add_to_flash_group_marks_update_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);
		//(103, 700, 165, 165, 2, 'mark 2')
		
		$this->assert_db_row_count("mark_calc", 2, "
			WHERE
				precedent_flash_group_id = 103
				AND competence_id = 700
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 2
				AND comment = 'mark 2'
		");
		
		$link_id = $this->edit_helper->flash_group_ratee_add(103, 180);
		$this->assert_true((bool)$link_id);
		
		$this->assert_db_row_count("mark_calc", 3, "
			WHERE
				precedent_flash_group_id = 103
				AND competence_id = 700
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 2
				AND comment = 'mark 2'
		");
		
		$this->assert_db_row_exists("
			SELECT *
			FROM mark_calc
			WHERE
				precedent_flash_group_id = 103
				AND precedent_flash_group_user_link_id = {$link_id}
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 2
				AND comment = 'mark 2'
		");
		$this->db->test_end();
	}
	
	public function member_remove_from_flash_group_marks_update_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);
		$this->user->set_user_id(165);
		//(103, 700, 165, 165, 2, 'mark 2')
		
		$this->assert_db_row_count("mark_calc", 2, "
			WHERE
				precedent_flash_group_id = 103
				AND competence_id = 700
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 2
				AND comment = 'mark 2'
		");
		
		$this->edit_helper->flash_group_ratee_delete(103, 184);
		
		$this->assert_db_row_count("mark_calc", 1, "
			WHERE
				precedent_flash_group_id = 103
				AND competence_id = 700
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 2
				AND comment = 'mark 2'
		");
		
		$this->assert_db_row_not_exist("
			SELECT *
			FROM mark_calc
			WHERE
				precedent_flash_group_id = 103
				AND precedent_flash_group_user_link_id = 103184
				AND competence_id = 700
				AND rater_user_id = 165
				AND adder_user_id = 165
				AND value = 2
				AND comment = 'mark 2'
		");
		
		$this->db->test_end();
	}

}

?>