<?php

require_once("base_precedent_edit_helper.test.php");

class flash_groups_precedent_edit_helper_test extends base_precedent_edit_helper_test
{

	public function flash_group_add_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$flash_group_id = $this->edit_helper->flash_group_add("flash group new");

		$this->assert_db_row_exists("
			SELECT *
			FROM precedent_flash_group
			WHERE
				precedent_id = 10
				AND title = 'flash group new'
				AND id = '{$flash_group_id}'
		");

		$this->db->test_end();
	}

	public function flash_group_delete_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->flash_group_delete(104);
		$this->assert_true($ok);
		$this->assert_db_row_not_exist("SELECT * FROM precedent_flash_group WHERE id = 104");

		$this->db->test_end();
	}

	public function flash_group_delete_default_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 13);

		$ok = $this->edit_helper->flash_group_delete(130);
		$this->assert_false($ok);
		$this->assert_db_row_exists("SELECT * FROM precedent_flash_group WHERE id = 130");

		$this->db->test_end();
	}

	public function flash_group_delete_with_marks()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->flash_group_delete(101);
		$this->assert_false($ok);
		$this->assert_db_row_exists("SELECT * FROM precedent_flash_group WHERE id = 101");

		$this->db->test_end();
	}

	public function flash_group_delete_cross_precedent_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$this->edit_helper->flash_group_delete(111);
		$this->assert_db_row_exists("SELECT * FROM precedent_flash_group WHERE id = 111");

		$this->db->test_end();
	}

}

?>