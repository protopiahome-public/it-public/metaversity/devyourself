<?php

require_once("base_precedent_edit_helper.test.php");

class ratee_precedent_edit_helper_test extends base_precedent_edit_helper_test
{

	public function set_up()
	{
		parent::set_up();
		$this->db->sql("REPLACE INTO user_project_link SET project_id = 1, user_id = 180");
		$this->db->sql("REPLACE INTO user_project_link SET project_id = 1, user_id = 181");
		$this->db->sql("REPLACE INTO user_project_link SET project_id = 1, user_id = 182");
		$this->db->sql("UPDATE precedent SET ratee_count_calc = 5 WHERE id = 12");
	}

	public function ratee_add_test()
	{
		$this->db->test_begin();
		$this->db->begin();

		$this->edit_helper->init(1, 12);

		$ok = $this->edit_helper->ratee_add(180);

		$this->assert_true($ok);

		// check default_flash_group
		$this->assert_db_row_exists("
			SELECT pfg.id
			FROM precedent, precedent_flash_group AS pfg
			WHERE pfg.id = precedent.default_flash_group_id
			AND precedent.id = 12
		");

		// check user_link
		$this->assert_db_row_exists("
			SELECT pfgul.id
			FROM precedent, precedent_flash_group_user_link AS pfgul
			WHERE 
				pfgul.precedent_flash_group_id = precedent.default_flash_group_id
				AND precedent.id = 12
				AND pfgul.user_id = 180
		");

		// check calc fields
		$this->assert_db_row_count("precedent_ratee_calc", 1, "WHERE precedent_id = 12");
		$this->assert_db_value("precedent", "ratee_count_calc", 12, 6);

		$this->db->test_end();
	}

	public function two_ratees_add_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 12);

		$ok = $this->edit_helper->ratee_add(180);
		$ok = $ok and $this->edit_helper->ratee_add(181);

		$this->assert_true($ok);

		// precedent_flash_group still 1
		$this->assert_db_row_count("precedent_flash_group", 1, "WHERE precedent_id = 12");

		// 2 user links
		$this->assert_db_row_count("precedent, precedent_flash_group_user_link AS pfgul", 2, "
			WHERE 
				pfgul.precedent_flash_group_id = precedent.default_flash_group_id
				AND precedent.id = 12
		");

		$this->assert_db_row_count("precedent_ratee_calc", 2, "WHERE precedent_id = 12");
		$this->assert_db_value("precedent", "ratee_count_calc", 12, 7);

		$this->db->test_end();
	}

	public function ratee_add_twice_test()
	{
		// add ratee twice, check nothing happens
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 11);

		$ok = $this->edit_helper->ratee_add(180);

		$this->assert_true($ok);

		$this->assert_db_row_exists("
			SELECT *
			FROM
				precedent,
				precedent_flash_group_user_link AS pfgul
			WHERE 
				pfgul.precedent_flash_group_id = precedent.default_flash_group_id
				AND precedent.id = 11
				AND pfgul.user_id = 180
		");

		$this->assert_db_row_exists("
			SELECT *
			FROM precedent_ratee_calc
			WHERE precedent_id = 11
		");
		
		$this->assert_db_value("precedent", "ratee_count_calc", 11, 1);
		$this->db->test_end();
	}

	public function ratee_delete_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 11);

		$ok = $this->edit_helper->ratee_delete(180);
		$this->assert_true($ok);

		// check delete from flash_group
		$this->assert_db_row_not_exist("
			SELECT pfgul.id
			FROM precedent, precedent_flash_group_user_link AS pfgul
			WHERE
				pfgul.precedent_flash_group_id = precedent.default_flash_group_id
				AND precedent.id = 11
		");

		// check calc fields
		$this->assert_db_row_not_exist("
			SELECT *
			FROM precedent_ratee_calc
			WHERE
				precedent_id=11
				AND ratee_user_id = 180
		");
		$this->assert_db_row_not_exist("SELECT * FROM precedent_ratee_calc WHERE precedent_id=11");
		$this->assert_db_value("precedent", "ratee_count_calc", 11, 0);

		$this->db->test_end();
	}

	public function ratee_with_group_marks_delete_test()
	{
		// we can delete user with group marks only
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->ratee_delete(183);

		$this->assert_true($ok);

		// check delete from flash_group
		$this->assert_db_row_not_exist("
			SELECT pfgul.id
			FROM precedent, precedent_flash_group_user_link AS pfgul
			WHERE
				pfgul.precedent_flash_group_id = precedent.default_flash_group_id
				AND user_id = 183
				AND precedent.id = 10
		");

		$this->db->test_end();
	}

	public function ratee_with_personal_marks_delete_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->ratee_delete(182);

		$this->assert_false($ok);

		// check delete from flash_group
		$this->assert_db_row_exists("
			SELECT pfgul.id
			FROM precedent, precedent_flash_group_user_link AS pfgul
			WHERE
				pfgul.precedent_flash_group_id = precedent.default_flash_group_id
				AND user_id = 182
				AND precedent.id = 10
		");

		$this->db->test_end();
	}

	public function ratee_add_to_flash_group_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$user_link_id = $this->edit_helper->flash_group_ratee_add(101, 180);

		$this->assert_true((bool) $user_link_id);

		// check user_link
		$this->assert_db_row_exists("
			SELECT pfgul.id
			FROM precedent, precedent_flash_group_user_link AS pfgul
			WHERE 
				pfgul.precedent_flash_group_id = 101
				AND precedent.id = 10
				AND pfgul.user_id = 180
				AND pfgul.id = '{$user_link_id}'
		");

		$this->db->test_end();
	}

	public function ratee_add_to_left_flash_group_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$user_link_id = $this->edit_helper->flash_group_ratee_add(110, 181);

		$this->assert_false($user_link_id);

		// check user_link
		$this->assert_db_row_not_exist("
			SELECT pfgul.id
			FROM precedent, precedent_flash_group_user_link AS pfgul
			WHERE 
				pfgul.precedent_flash_group_id = 110
				AND pfgul.user_id = 181
				AND pfgul.id = '{$user_link_id}'
		");

		$this->db->test_end();
	}

	public function not_in_precedent_ratee_add_to_flash_group_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$user_link_id = $this->edit_helper->flash_group_ratee_add(101, 185);

		$this->assert_false($user_link_id);

		// check user_link
		$this->assert_db_row_not_exist("
			SELECT pfgul.id
			FROM precedent, precedent_flash_group_user_link AS pfgul
			WHERE 
				pfgul.precedent_flash_group_id = 101
				AND precedent.id = 10
				AND pfgul.user_id = 185
		");

		$this->db->test_end();
	}

	public function ratee_delete_from_flash_group_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->flash_group_ratee_delete(101, 181);

		$this->assert_true($ok);

		$this->assert_db_row_not_exist("
			SELECT pfgul.id
			FROM precedent, precedent_flash_group_user_link AS pfgul
			WHERE 
				pfgul.precedent_flash_group_id = 102
				AND precedent.id = 10
				AND pfgul.user_id = 181
		");

		$this->db->test_end();
	}

	public function ratee_delete_from_left_flash_group()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->flash_group_ratee_delete(110, 180);

		$this->assert_false($ok);

		// check delete from flash_group
		$this->assert_db_row_exists("
			SELECT *
			FROM precedent_flash_group_user_link
			WHERE
				precedent_flash_group_id = 110
				AND user_id = 180
		");

		$this->db->test_end();
	}

	public function ratee_delete_from_default_flash_group()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->flash_group_ratee_delete(100, 180);

		$this->assert_false($ok);

		// check delete from flash_group
		$this->assert_db_row_exists("
			SELECT *
			FROM precedent_flash_group_user_link
			WHERE
				precedent_flash_group_id = 100
				AND user_id = 180
		");

		$this->db->test_end();
	}

	public function ratee_with_personal_marks_delete_from_flash_grouptest()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->flash_group_ratee_delete(102, 182);

		$this->assert_false($ok);

		// check delete from flash_group
		$this->assert_db_row_exists("
			SELECT *
			FROM precedent_flash_group_user_link
			WHERE
				precedent_flash_group_id = 102
				AND user_id = 182
		");

		$this->db->test_end();
	}

	public function ratee_with_group_marks_delete_from_flash_group_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->ratee_delete(183, 103);
		$this->assert_true($ok);

		// check delete from flash_group
		$this->assert_db_row_not_exist("
			SELECT *
			FROM precedent_flash_group_user_link
			WHERE
				precedent_flash_group_id = 103
				AND user_id = 183
		");

		$this->db->test_end();
	}

}

?>