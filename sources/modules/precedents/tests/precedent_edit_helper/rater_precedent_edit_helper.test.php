<?php
require_once("base_precedent_edit_helper.test.php");

class rater_precedent_edit_helper_test extends base_precedent_edit_helper_test
{
	public function set_up()
	{
		parent::set_up();
		$this->db->sql("REPLACE INTO user_project_link SET project_id = 1, user_id = 166");
		$this->db->sql("REPLACE INTO user_project_link SET project_id = 1, user_id = 167");
	}

	public function rater_add_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->rater_add(167);
		
		$this->assert_true($ok);
		
		$this->assert_db_row_exists("
			SELECT *
			FROM precedent_rater_link
			WHERE
				precedent_id = 10
				AND rater_user_id=167
		");
		
		$this->db->test_end();
	}
	
	public function rater_add_not_in_project_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->rater_add(168);
		
		$this->assert_false($ok);
		
		$this->assert_db_row_not_exist("
			SELECT *
			FROM precedent_rater_link
			WHERE
				precedent_id = 10
				AND rater_user_id=168
		");
		
		$this->db->test_end();
	}

	public function rater_delete_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->rater_delete(166);
		
		$this->assert_true($ok);
		
		$this->assert_db_row_not_exist("
			SELECT *
			FROM precedent_rater_link
			WHERE
				precedent_id = 10
				AND rater_user_id = 166
		");
		
		$this->db->test_end();
	}

	public function rater_delete_with_marks_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->rater_delete(165);
		
		$this->assert_false($ok);
		
		$this->assert_db_row_exists("
			SELECT *
			FROM precedent_rater_link
			WHERE
				precedent_id = 10
				AND rater_user_id = 165
		");
		
		$this->db->test_end();
	}

}

?>