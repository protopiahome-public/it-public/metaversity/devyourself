<?php
require_once("base_precedent_edit_helper.test.php");

class misc_precedent_edit_helper_test extends base_precedent_edit_helper_test
{

	public function set_up()
	{
		parent::set_up();
		$this->db->sql("REPLACE INTO competence_link (competence_id, competence_group_id) VALUES (700, 70)");
		$this->db->sql("REPLACE INTO competence_group (id, competence_set_id, title) VALUES (72, 7, 'not leaf group')");
		$this->db->sql("REPLACE INTO competence_group (id, competence_set_id, title, parent_id) VALUES (272, 7, 'test group 2', 72)");
		$this->db->sql("UPDATE project SET precedent_count_calc = 5 WHERE id = 1");
		
	}
	
	public function precedent_add_trigger_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		
		$this->db->sql("INSERT INTO precedent SET project_id = 1, title = '', descr = '', last_editor_user_id = 1, adder_user_id = 1");
	
		$this->assert_db_value("project", "precedent_count_calc", 1, 6);

		$this->db->test_end();
	}

	public function competence_group_add_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 12);

		$ok = $this->edit_helper->competence_group_add(70);

		$this->assert_true($ok);
		$this->assert_db_row_exists("
			SELECT *
			FROM precedent_competence_group_link
			WHERE
				precedent_id = 12
				AND competence_group_id = 70
				AND competence_set_id = 7
		");

		$this->db->test_end();
	}
	
	public function competence_group_add_not_leaf_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 12);

		$ok = $this->edit_helper->competence_group_add(72);

		$this->assert_false($ok);
		$this->assert_db_row_not_exist("
			SELECT *
			FROM precedent_competence_group_link
			WHERE
				precedent_id = 12
				AND competence_group_id = 72
		");
		
		$ok = $this->edit_helper->competence_group_add(272);

		$this->assert_false($ok);
		$this->assert_db_row_not_exist("
			SELECT *
			FROM precedent_competence_group_link
			WHERE
				precedent_id = 12
				AND competence_group_id = 272
		");

		$this->db->test_end();
	}
	
	public function competence_group_add_nonexistent_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 12);

		$ok = $this->edit_helper->competence_group_add(170);
		
		$this->assert_false($ok);
		$this->assert_db_row_not_exist("
			SELECT *
			FROM precedent_competence_group_link
			WHERE
				precedent_id = 12
				AND competence_group_id = 170
		");

		$this->db->test_end();
	}


	public function competence_group_delete_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 11);

		$ok = $this->edit_helper->competence_group_delete(70);
		
		$this->assert_true($ok);
		$this->assert_db_row_not_exist("
			SELECT *
			FROM precedent_competence_group_link
			WHERE
				precedent_id = 11
				AND competence_group_id = 70
		");

		$this->db->test_end();
	}

	public function competence_group_delete_with_marks()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->edit_helper->init(1, 10);

		$ok = $this->edit_helper->competence_group_delete(70);
		
		$this->assert_false($ok);
		$this->assert_db_row_exists("
			SELECT *
			FROM precedent_competence_group_link
			WHERE
				precedent_id = 10
				AND competence_group_id = 70
		");

		$this->db->test_end();
	}

}

?>