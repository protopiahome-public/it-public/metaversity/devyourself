<?php

class material_calc_db_test extends base_test
{

	public function set_up()
	{
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user')");
		}
		$this->db->sql("TRUNCATE project");
		$this->db->sql("TRUNCATE material");
		$this->db->sql("REPLACE INTO project (id, name, title) VALUES (1, 'test', 'test')");
		$this->db->sql("INSERT INTO material (id, title, project_id) VALUES (1, 'test', 1)");
	}

	public function basic_test()
	{
		$this->db->begin();

		//check set_up
		$this->assert_db_value("project", "material_count_calc", 1, "1");
		$this->db->rollback();
	}

	public function material_add_test()
	{
		$this->db->begin();

		$this->db->sql("INSERT INTO material (id, title, project_id) VALUES (2, 'test2', 1)");

		$this->assert_db_value("project", "material_count_calc", 1, "2");
		$this->db->rollback();
	}

	public function material_delete_test()
	{
		$this->db->begin();

		$this->db->sql("DELETE FROM material WHERE id = 1");

		$this->assert_db_value("project", "material_count_calc", 1, "0");

		$this->db->rollback();
	}

}

?>