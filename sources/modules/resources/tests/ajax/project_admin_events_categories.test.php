<?php

class project_admin_events_categories_test extends base_test
{

	public function set_up()
	{
		$this->db->sql("
			REPLACE INTO project (id, title, name)
			VALUES (1, 'test', 'test')
		");
		$this->db->sql("
			DELETE FROM event
			WHERE project_id = 1
		");
		$this->db->sql("
			DELETE FROM event_category
			WHERE project_id = 1 OR id < 5
		");
	}

	public function guest_test()
	{
		$this->set_user_guest();

		$this->db->test_begin();
		$tester = new ajax_ctrl_tester("project_admin_events_categories");
		$_REQUEST = array("project_id" => 1);
		$tester->run_and_assert_data(array('status' => 'BAD_RIGHTS', 'error_code' => 'CHECK_RIGHTS'));
		$this->db->test_end();
	}

	public function unexisted_competence_set_test()
	{
		$this->set_user_admin();

		$this->db->test_begin();
		$this->db->test_set_allow_queries_beyond_transaction();
		$tester = new ajax_ctrl_tester("project_admin_events_categories");
		$_REQUEST = array("project_id" => 9999999);
		$tester->run_and_assert_data(array('status' => 'ERROR', 'error_code' => 'BEFORE_START',));
		$this->db->test_end();
	}

	public function group_recursion_test()
	{
		$this->set_user_admin();

		$this->db->test_begin();
		$this->db->begin();

		$this->db->sql("
			INSERT INTO event_category (id, parent_id, project_id, title)
			VALUES
				(1, NULL, 1, '1'),
				(2, NULL, 1, '2'),
				(3, 1, 1, '3'),
				(4, 3, 1, '4')
		");
		$tester = new ajax_ctrl_tester("project_admin_events_categories");
		$_REQUEST = $_POST = array("project_id" => 1, "operation" => "move_group", "id" => "1", "group_id" => "4", "position" => 1);
		$tester->run_and_assert_data(array("status" => "ERROR", "error" => "recursive_move"));

		$tester = new ajax_ctrl_tester("project_admin_events_categories");
		$_REQUEST = $_POST = array("project_id" => 1, "operation" => "move_group", "id" => "3", "group_id" => "4", "position" => 1);
		$tester->run_and_assert_data(array("status" => "ERROR", "error" => "recursive_move"));

		$tester = new ajax_ctrl_tester("project_admin_events_categories");
		$_REQUEST = $_POST = array("project_id" => 1, "operation" => "move_group", "id" => "3", "group_id" => "3", "position" => 1);
		$tester->run_and_assert_data(array("status" => "ERROR", "error" => "recursive_move"));

		$tester = new ajax_ctrl_tester("project_admin_events_categories");
		$_REQUEST = $_POST = array("project_id" => 1, "operation" => "move_group", "id" => "4", "group_id" => "2", "position" => 1);
		$tester->run_and_assert_data(array("status" => "OK"));

		$this->db->test_end();
	}

}

?>