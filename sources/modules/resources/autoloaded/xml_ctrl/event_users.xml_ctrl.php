<?php

class event_users_xml_ctrl extends base_easy_xml_ctrl
{

	protected $xml_row_name = "user";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "user_short",
			"param2" => "project_id",
		),
	);
	protected $project_id;
	protected $event_id;

	public function __construct($project_id, $event_id)
	{
		$this->project_id = $project_id;
		$this->event_id = $event_id;
		parent::__construct();
	}

	public function get_cache()
	{
		return event_users_cache::init($this->event_id);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("event_user_link l");
		$select_sql->add_select_fields("l.user_id as id, l.before_status, l.after_status");
		$select_sql->add_where("l.event_id = {$this->event_id}");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	protected function process_data()
	{
		$helper = new event_status_helper();
		foreach ($this->data as &$row)
		{
			$helper->init_from_input($this->project_id, $this->event_id, $row["id"], $row["before_status"], $row["after_status"], 0);
			$row["before_status"] = $helper->get_before_status_name();
			$row["after_status"] = $helper->get_after_status_name();
		}
	}

}

?>