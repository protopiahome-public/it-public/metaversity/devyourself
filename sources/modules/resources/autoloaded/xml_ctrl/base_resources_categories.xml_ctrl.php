<?php

require_once PATH_INTCMF . "/tree.php";
require_once PATH_INTCMF . "/xml_builder.php";

abstract class base_resources_categories_xml_ctrl extends base_easy_xml_ctrl
{

	protected $resource_name;

	/**
	 * @var tree
	 */
	protected $tree;
	private $project_id;
	private $show_stat;

	public function __construct($project_id, $show_stat = false)
	{
		$this->project_id = $project_id;
		$this->show_stat = $show_stat;

		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		// Groups
		$data_raw_plain = $this->db->fetch_all("
			SELECT
				id, title, parent_id
			FROM {$this->resource_name}_category
			WHERE project_id = {$this->project_id}
			ORDER BY position
		");
		$this->data = $data_raw_plain;
		$this->tree = new tree($data_raw_plain);
		$this->tree->make_tree();
	}

	protected function fill_xml(xdom $xdom)
	{
		$xdom->set_attr("project_id", $this->project_id);

		xml_builder::tree($xdom, $this->tree, "category");
	}

	abstract protected function get_resources_categories_cache($project_id);

	protected function get_cache()
	{
		return $this->get_resources_categories_cache($this->project_id);
	}

}

?>