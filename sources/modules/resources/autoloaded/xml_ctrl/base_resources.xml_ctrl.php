<?php

abstract class base_resources_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $resource_name;
	protected $calendar_on = true;
	protected $options = array(
		0 => "—",
		1 => "Косвенно развивает",
		2 => "Событие сфокусировано на этом"
	);
	// Internal
	protected $project_id;
	protected $competence_set_id;
	protected $is_archive = false;
	protected $is_calendar = false;
	protected $year;
	protected $month;
	protected $day;
	protected $page;
	protected $cat_id;
	protected $with_ignored = false;
	protected $user_link_list;
	protected $count_total;
	protected $resource_category_count = array();
	protected $vector_exists = false;
	protected $is_vector_sort = false;
	protected $vector_calc_base;
	protected $count = 25;
	protected $force_sort_date = false;

	/**
	 * @var base_resource_vector_match_math
	 */
	protected $resource_vector_match_math;

	public function __construct($project_id, $page, $p1 = null, $p2 = null, $p3 = null, $p4 = null, $count = 25, $force_sort_date = false)
	{
		$this->xml_row_name = $this->resource_name;
		$this->dependencies_settings = array_merge($this->dependencies_settings, array(
			array(
				"column" => "id",
				"ctrl" => "{$this->resource_name}_short",
				"param2" => "project_id"
			),
			array(
				"column" => "reflex_project_id",
				"ctrl" => "project_short",
			)
			)
		);

		$this->count = $count;
		$this->force_sort_date = $force_sort_date;

		if ($this->calendar_on)
		{
			$this->xml_attrs = array("project_id", "is_archive", "is_calendar", "year",
				"month", "day", "cat_id", "with_ignored",
				"is_vector_sort", "vector_calc_base", "vector_exists"
			);
		}
		else
		{
			$this->xml_attrs = array("project_id", "cat_id", "with_ignored",
				"is_vector_sort", "vector_calc_base", "vector_exists"
			);
		}

		$this->project_id = $project_id;

		$this->page = $page;

		$this->is_archive = $p1 === "archive";
		$this->is_calendar = $p1 === "calendar";

		if ($this->is_calendar)
		{
			$this->year = $p2;
			if ($p3)
			{
				$this->month = $p3;
				if ($p4)
				{
					$this->day = $p4;
				}
			}
		}

		if (GET("with-ignored") === "1")
		{
			$this->with_ignored = true;
		}

		$this->vector_calc_base = COOKIE("vector-calc-base") === "future" ? "future" : "focus";
		if (GET("sort") === "vector")
		{
			$this->is_vector_sort = true;
		}

		parent::__construct();
	}

	public function init()
	{
		if ($this->calendar_on)
		{
			$calendar_class = "{$this->resource_name}s_calendar_xml_ctrl";
			if ($this->is_calendar && $this->month)
			{
				$this->xml_loader->add_xml(new $calendar_class($this->project_id, $this->year, $this->month));
			}
			elseif ($this->is_calendar && !$this->month)
			{
				$this->xml_loader->add_xml(new $calendar_class($this->project_id, $this->year, 1));
			}
			else
			{
				$this->xml_loader->add_xml(new $calendar_class($this->project_id));
			}
		}

		$this->add_easy_processor(new pager_full_fetch_easy_processor($this->page, $this->count));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		if (!$this->check())
		{
			$this->set_error_404();
			return;
		}

		$this->load_vector();
		$this->load_resources_data($select_sql);
		$this->load_user_link_data();
		$this->filter();
		$this->set_matches();
		$this->sort();
	}

	private function check()
	{
		if ($this->is_calendar)
		{
			$date_valid = true;

			if ($this->year < 1970 || $this->year > 2037)
			{
				$date_valid = false;
			}
			if ($this->month > 12)
			{
				$date_valid = false;
			}
			if ($this->day && !checkdate($this->month, $this->day, $this->year))
			{
				$date_valid = false;
			}
			if (!$date_valid)
			{
				return false;
			}
		}

		if ($cat_id = GET("cat"))
		{
			if (!is_good_id($cat_id))
			{
				return false;
			}
			$cat_exists = $this->db->row_exists("SELECT id FROM {$this->resource_name}_category WHERE id = {$cat_id} AND project_id = {$this->project_id}");
			if (!$cat_exists)
			{
				return false;
			}

			$this->cat_id = $cat_id;
		}

		return true;
	}

	private function load_vector()
	{
		$user_id = $this->user->get_user_id();
		if (!$user_id)
		{
			return;
		}

		$this->resource_vector_match_math = $this->get_resource_vector_match_math($this->project_id, $user_id, $this->vector_calc_base);
		$this->vector_exists = $this->resource_vector_match_math->vector_exists();
		$this->competence_set_id = $this->resource_vector_match_math->get_competence_set_id();
	}

	private function load_resources_data(select_sql $select_sql)
	{
		$this->data = $this->get_resources_helper()->get_resources_data();
	}

	private function load_user_link_data()
	{
		if ($user_id = $this->user->get_user_id())
		{
			$this->user_link_list = $this->get_resources_user_link_cache($this->project_id, $user_id)->get();
			if (!is_array($this->user_link_list))
			{
				$this->user_link_list = $this->db->fetch_all("
					SELECT l.*, r.id as resource_id FROM
					{$this->resource_name}_user_link l
					LEFT JOIN {$this->resource_name} r ON (IF(r.reflex_id > 0, r.reflex_id, r.id) = l.{$this->resource_name}_id)
					WHERE user_id = {$user_id} AND r.project_id = {$this->project_id}
				", "resource_id");

				$this->get_resources_user_link_cache($this->project_id, $user_id)->set($this->user_link_list);
			}
		}
	}

	private function filter()
	{
		$user_id = $this->user->get_user_id();

		if ($this->calendar_on)
		{
			if ($this->is_archive)
			{
				$this->with_ignored = true;
				$this->filter_date(0, time());
			}
			elseif ($this->is_calendar)
			{
				$this->with_ignored = true;
				if (!$this->month)
				{
					$min_date = mktime(0, 0, 0, 1, 1, $this->year);
					$max_date = mktime(23, 59, 59, 12, 31, $this->year);
					$this->filter_date($min_date, $max_date);
				}
				elseif (!$this->day)
				{
					$min_date = mktime(0, 0, 0, $this->month, 1, $this->year);
					$max_date = mktime(23, 59, 59, $this->month, date("t", $min_date), $this->year);
					$this->filter_date($min_date, $max_date);
				}
				else
				{
					$min_date = mktime(0, 0, 0, $this->month, $this->day, $this->year);
					$max_date = mktime(23, 59, 59, $this->month, $this->day, $this->year);
					$this->filter_date($min_date, $max_date);
				}
			}
			else
			{
				$this->filter_date(time(), 0);
			}
		}

		if ($user_id && !$this->with_ignored)
		{
			$this->filter_ignore();
		}

		$this->calculate_category_items_count();

		if ($this->cat_id)
		{
			$this->filter_cat($this->cat_id);
		}

		if ($this->data)
		{
			foreach ($this->data as $key => $row)
			{
				unset($this->data[$key]["cats"]);
			}
		}
	}

	private function set_matches()
	{
		if ($this->vector_exists)
		{
			$this->resource_vector_match_math->set_resources_data_getter(array($this, "get_resources_data"));

			foreach ($this->data as &$resource)
			{
				if (!count($resource["rate_data"]))
				{
					$resource["match"] = -1;
				}
				else
				{
					$resource["match"] = $this->resource_vector_match_math->get_match($resource["id"], $resource["rate_data"]);
				}
			}
		}
	}

	private function sort()
	{
		if ($this->vector_exists)
		{
			if (GET("sort") !== "date")
			{
				$this->is_vector_sort = true;
			}
		}
		else
		{
			$this->is_vector_sort = false;
		}

		if ($this->force_sort_date)
		{
			$this->is_vector_sort = false;
		}

		if ($this->is_vector_sort)
		{
			$this->sort_vector();
		}
		elseif ($this->is_archive)
		{
			$this->sort_revert();
		}
	}

	private function calculate_category_items_count()
	{
		$this->count_total = count($this->data);
		foreach ($this->data as $row)
		{
			foreach ($row["cats"] as $item_cat)
			{
				if (!isset($this->resource_category_count[$item_cat]))
				{
					$this->resource_category_count[$item_cat] = 0;
				}
				$this->resource_category_count[$item_cat]++;
			}
		}
	}

	private function filter_date($min_date, $max_date)
	{
		foreach ($this->data as $key => $row)
		{
			if ($row["start_time"] < $min_date)
			{
				unset($this->data[$key]);
			}
			if ($max_date > 0 && $row["start_time"] > $max_date)
			{
				unset($this->data[$key]);
			}
		}
	}

	private function filter_cat($cat_id)
	{
		foreach ($this->data as $key => $row)
		{
			if (!in_array($cat_id, $row["cats"]))
			{
				unset($this->data[$key]);
			}
		}
	}

	private function filter_ignore()
	{
		foreach ($this->data as $key => $row)
		{
			if (isset($this->user_link_list[$row["id"]]) and $this->user_link_list[$row["id"]]["ignored"])
			{
				unset($this->data[$key]);
			}
		}
	}

	private function sort_revert()
	{
		$this->data = array_reverse($this->data);
	}

	private function sort_vector()
	{
		usort($this->data, array($this, "vector_array_sort"));
	}

	private function vector_array_sort($resource1, $resource2)
	{
		if ($resource1["match"] == $resource2["match"])
		{
			if ($this->calendar_on)
			{
				if ($this->is_archive)
				{
					return $resource1["start_time"] < $resource2["start_time"];
				}
				else
				{
					return $resource1["start_time"] > $resource2["start_time"];
				}
			}
			else
			{
				return $resource1["add_time"] < $resource2["add_time"];
			}
		}
		else
		{
			return $resource1["match"] < $resource2["match"];
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		if (count($this->data))
		{
			if ($user_id = $this->user->get_user_id())
			{
				$resource_status_helper = $this->get_resource_status_helper();
			}

			foreach ($this->data as $resource)
			{
				$resource_node = $xdom->create_child_node($this->resource_name);
				foreach ($resource as $name => $attr)
				{
					if ($name != "rate_data")
					{
						$resource_node->set_attr($name, $attr);
					}
				}

				if ($user_id = $this->user->get_user_id())
				{
					$this->init_resource_status_helper($resource_status_helper, $resource);
					$resource_status_helper->modify_xml($resource_node);
				}
			}
		}
	}

	protected function modify_xml(xdom $xdom)
	{
		// Ignored resources XML
		$ignore_list_node = $xdom->create_child_node("user_ignore_list");
		if ($this->user->get_user_id())
		{
			foreach ($this->user_link_list as $id => $link_row)
			{
				if ($link_row["ignored"])
				{
					$ignore_list_node->create_child_node("{$this->resource_name}")->set_attr("id", $id);
				}
			}
		}

		// Category items count XML
		$count_node = $xdom->create_child_node("categories");
		$count_node->set_attr("count_total", $this->count_total);
		foreach ($this->resource_category_count as $id => $count)
		{
			$count_node->create_child_node("category")
				->set_attr("id", $id)
				->set_attr("count", $count)
			;
		}

		foreach ($this->data as $resource)
		{
			if ($resource["rate_id"])
			{
				if ($this->vector_exists)
				{
					$rate_xml_ctrl = new rate_xml_ctrl(
							$resource["rate_id"],
							$this->competence_set_id,
							$this->options,
							$resource["rate_data"],
							$this->resource_vector_match_math->get_focus_data(),
							$this->resource_vector_match_math->get_future_data()
					);
				}
				else
				{
					$rate_xml_ctrl = new rate_xml_ctrl(
							$resource["rate_id"],
							$this->competence_set_id,
							$this->options,
							$resource["rate_data"]
					);
				}

				$this->xml_loader->add_xml($rate_xml_ctrl);
			}
		}
	}

	public function get_resources_data()
	{
		return $this->data;
	}

	/**
	 * @return base_resources_helper
	 */
	abstract protected function get_resources_helper();

	abstract protected function get_resources_user_link_cache($project_id, $user_id);

	abstract protected function get_resource_vector_match_math($project_id, $user_id, $vector_calc_base);

	/**
	 * @return base_resource_status_helper
	 */
	abstract protected function get_resource_status_helper();

	abstract protected function init_resource_status_helper(base_resource_status_helper $resource_status_helper, $row);
}

?>