<?php

abstract class base_resources_import_projects_xml_ctrl extends base_easy_xml_ctrl
{

	protected $resource_name;
	protected $xml_row_name = "project_import";
	protected $project_id;
	protected $dependencies_settings = array(
		array(
			"column" => "from_project_id",
			"ctrl" => "project_short",
		)
	);

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("from_project_id, {$this->resource_name}s_status as checked_status");
		$select_sql->add_from("project_import");
		$select_sql->add_where("to_project_id = {$this->project_id} AND {$this->resource_name}s_status IN (1,-1)");

		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>
