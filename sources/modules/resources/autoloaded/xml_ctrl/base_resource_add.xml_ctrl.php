<?php

abstract class base_resource_add_xml_ctrl extends base_dt_add_xml_ctrl
{

	// Settings level 2
	protected $resource_name;
	// Settings
	protected $axis_name = "edit";
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();
		
		$this->dt_name = $this->resource_name;
		parent::__construct();
	}

	protected function on_after_dt_init()
	{
		$dtf = $this->dt->get_field("categories");
		/* @var $dtf multi_link_dtf */
		$dtf->set_restrict_sql("WHERE project_id = {$this->project_id}");
	}

}

?>