<?php

abstract class base_resource_delete_xml_ctrl extends base_delete_xml_ctrl
{

	protected $resource_name;
	protected $project_id;
	protected $dependencies_settings = array(
		array(
			"column" => "reflex_project_id",
			"ctrl" => "project_short",
		)
	);

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($project_id, $resource_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		$this->db_table = $this->resource_name;
		parent::__construct($resource_id);
	}

	public function init()
	{
		$this->xml_loader->add_xml_by_class_name("{$this->resource_name}_short_xml_ctrl", $this->id, $this->project_id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("add_time, reflex_id, reflex_project_id");
		$select_sql->add_where("project_id = {$this->project_id}");
	}

}

?>