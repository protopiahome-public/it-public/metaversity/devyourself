<?php

class events_calendar_xml_ctrl extends base_calendar_xml_ctrl
{

	protected $project_id;

	public function __construct($project_id, $year = false, $month = false)
	{
		parent::__construct($year, $month);

		$this->project_id = $project_id;

		$this->xml_attrs[] = "project_id";
	}

	public function init()
	{
		if ($this->user->get_user_id())
		{
			$this->xml_loader->add_xml(new user_events_calendar_xml_ctrl($this->user->get_user_id(), $this->year, $this->month));
		}
	}

	protected function get_cache()
	{
		return events_calendar_month_cache::init($this->project_id, $this->year, $this->month);
	}

	protected function get_calendar_data(select_sql $select_sql)
	{
		$prev_date_sql = $this->prev_year . "-" . str_pad($this->prev_month, 2, "0", STR_PAD_LEFT) . "-23 00:00:00";
		$next_date_sql = $this->next_year . "-" . str_pad($this->next_month, 2, "0", STR_PAD_LEFT) . "-07 00:00:00";
		
		$start_time_sql = "IF(e.reflex_id > 0, reflex.start_time, e.start_time)";

		return $this->db->fetch_all("
			SELECT DAYOFMONTH({$start_time_sql}) as `day`, count(*) as `count`,
				CONCAT(MONTH({$start_time_sql}), '|', DAYOFMONTH({$start_time_sql})) as date
			FROM event e
			LEFT JOIN event reflex ON (e.reflex_id = reflex.id)
			WHERE
				{$start_time_sql} >= '{$prev_date_sql}' AND {$start_time_sql} < '{$next_date_sql}'
				AND e.project_id = {$this->project_id}
			GROUP BY date
		", "date");
	}

}

?>