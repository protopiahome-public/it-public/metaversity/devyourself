<?php

class event_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	protected $dt_name = "event";
	protected $axis_name = "short";
	protected $project_id;
	
	public function __construct($id, $project_id)
	{
		parent::__construct($id);
		$this->project_id = $project_id;
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("dt.reflex_id");
	}

	protected function process_data()
	{
		if (count($this->data) and $this->data[0]["reflex_id"])
		{
			$reflex_data = $this->db->get_row("
				SELECT start_time, place, project_id 
				FROM event 
				WHERE id = {$this->data[0]["reflex_id"]}
			");
			if ($reflex_data)
			{
				$this->data[0]["start_time"] = $reflex_data["start_time"];
				$this->data[0]["place"] = $reflex_data["place"];
				$this->store_data["reflex_project_id"] = $reflex_data["project_id"];
			}
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		parent::fill_xml($xdom);
		$url = "%PROJECT_URL%" . "events/{$this->data[0]["id"]}/";
		$xdom->set_attr("url", $url);
	}

	// @dm9 понять про $this->data[0] - оно инициализировано вообще?
	protected function get_cache()
	{
		return event_short_cache::init($this->id, $this->project_id, $this->data[0] ? $this->data[0]["reflex_id"] : null);
	}

	protected function postprocess()
	{
		$url_project_obj = project_obj::instance(isset($this->store_data["reflex_project_id"]) ? $this->store_data["reflex_project_id"] : $this->project_id);
		$this->xml = preg_replace("/%PROJECT_URL%/", $url_project_obj->get_url(), $this->xml, 1);
	}

}

?>