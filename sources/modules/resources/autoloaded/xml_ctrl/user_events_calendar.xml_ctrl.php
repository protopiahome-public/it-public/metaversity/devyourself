<?php

class user_events_calendar_xml_ctrl extends base_calendar_xml_ctrl
{

	protected $user_id;

	public function __construct($user_id, $year = false, $month = false)
	{
		parent::__construct($year, $month);

		$this->user_id = $user_id;

		$this->xml_attrs[] = "user_id";
	}

	protected function get_cache()
	{
		return false;
		// @todo reload cache on event edit/delete
		//return user_events_calendar_month_cache::init($this->user_id, $this->year, $this->month);
	}

	protected function get_calendar_data(select_sql $select_sql)
	{
		$prev_date_sql = $this->prev_year . "-" . str_pad($this->prev_month, 2, "0", STR_PAD_LEFT) . "-23 00:00:00";
		$next_date_sql = $this->next_year . "-" . str_pad($this->next_month, 2, "0", STR_PAD_LEFT) . "-07 00:00:00";

		return $this->db->fetch_all("
			SELECT DAYOFMONTH(start_time) as `day`, count(*) as `count`,
				CONCAT(MONTH(start_time), '|', DAYOFMONTH(start_time)) as date
			FROM event e
			LEFT JOIN event_user_link l ON (l.event_id = e.id)
			WHERE 
				start_time >= '{$prev_date_sql}' AND start_time < '{$next_date_sql}'
				AND l.user_id = {$this->user_id}
				AND (l.before_status > 0 OR l.after_status = 1)
			GROUP BY date
		", "date");
	}

}

?>