<?php

class material_status_xml_ctrl extends base_easy_xml_ctrl
{

	protected $material_id;
	protected $footer;

	public function __construct($material_id, $footer = false)
	{
		parent::__construct();
		$this->material_id = $material_id;
		$this->footer = $footer;
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = array();
	}

	public function fill_xml(xdom $xdom)
	{
		$xdom->set_attr("id", $this->material_id);
		if ($this->footer)
		{
			$xdom->set_attr("footer", 1);
		}
		$resource_status_helper = new material_status_helper();
		$resource_status_helper->init_from_db($this->material_id, $this->user->get_user_id());
		$resource_status_helper->modify_xml($xdom);
	}

}

?>