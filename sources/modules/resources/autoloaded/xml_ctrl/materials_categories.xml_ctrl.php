<?php

class materials_categories_xml_ctrl extends base_resources_categories_xml_ctrl
{

	protected $resource_name = "material";

	protected function get_resources_categories_cache($project_id)
	{
		return materials_categories_cache::init($project_id);
	}

}

?>