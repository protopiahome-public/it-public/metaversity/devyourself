<?php

class event_status_xml_ctrl extends base_easy_xml_ctrl
{

	protected $event_id;
	protected $freeze;

	public function __construct($event_id, $freeze = false)
	{
		parent::__construct();
		$this->event_id = $event_id;
		$this->freeze = $freeze;
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = array();
	}

	public function fill_xml(xdom $xdom)
	{
		$xdom->set_attr("id", $this->event_id);
		$xdom->set_attr("freeze", $this->freeze);
		$resource_status_helper = new event_status_helper();
		$resource_status_helper->init_from_db($this->event_id, $this->user->get_user_id());
		$resource_status_helper->modify_xml($xdom);
	}

}

?>