<?php

class events_categories_xml_ctrl extends base_resources_categories_xml_ctrl
{

	protected $resource_name = "event";

	protected function get_resources_categories_cache($project_id)
	{
		return events_categories_cache::init($project_id);
	}

}

?>