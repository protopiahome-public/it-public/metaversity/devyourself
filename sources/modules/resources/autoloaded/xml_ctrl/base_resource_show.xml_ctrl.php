<?php

abstract class base_resource_show_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings level 2
	protected $options = array(
		0 => "—",
		1 => "Косвенно развивает",
		2 => "Сфокусировано на этом",
	);
	protected $resource_name;
	// Settings
	protected $axis_name = "show";
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $competence_set_id;

	public function __construct($project_id, $resource_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);

		$this->dt_name = $this->resource_name;
		parent::__construct($resource_id);
	}

	public function init()
	{
		$this->competence_set_id = $this->project_obj->get_vector_competence_set_id();
		if ($this->competence_set_id)
		{
			$this->xml_loader->add_xml_by_class_name("competence_set_short_xml_ctrl", $this->competence_set_id);
		}

		$this->xml_loader->add_xml(new comments_xml_ctrl($this->resource_name, $this->id, $this->project_id));
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("dt.reflex_id, dt.reflex_project_id");
		$select_sql->add_where("project_id = {$this->project_id}");

		if ($user_id = $this->user->get_user_id())
		{
			$select_sql->add_join("
				LEFT JOIN {$this->resource_name}_user_link
				ON {$this->resource_name}_user_link.user_id = {$user_id} AND {$this->resource_name}_user_link.{$this->resource_name}_id = {$this->id}
			");

			$select_sql->add_select_fields("IF({$this->resource_name}_user_link.ignored = 1, 1, 0) as `ignore`");
			$select_sql->add_select_fields("{$this->resource_name}_user_link.before_status");
			$select_sql->add_select_fields("{$this->resource_name}_user_link.after_status");
		}
	}

	protected function process_data()
	{
		if ($this->data and $this->data[0]["reflex_id"])
		{
			$project_obj = project_obj::instance($this->data["0"]["reflex_project_id"]);
			$this->set_redirect_url($project_obj->get_url() . "{$this->resource_name}s/{$this->data[0]["reflex_id"]}/");
			return;
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		parent::fill_xml($xdom);

		$rate_xml_ctrl = null;
		$rate_data_math = rate_data_math::get_instance_multi_link($this->resource_name, $this->id, $this->competence_set_id);
		$xdom->set_attr("rate_id", $rate_data_math->get_id());

		if ($user_id = $this->user->get_user_id())
		{
			$xdom->set_attr("ignore", $this->data[0]["ignore"]);

			$vector_calc_base = COOKIE("vector-calc-base") === "future" ? "future" : "focus";
			$resource_vector_match_math = $this->get_resource_vector_match_math($this->project_id, $user_id, $vector_calc_base);
			if ($resource_vector_match_math->vector_exists())
			{
				$match = 0;
				if ($rate_data_math->exists())
				{
					$resource_vector_match_math->set_resources_data_getter(array($this, "get_resources_data"));
					$match = $resource_vector_match_math->get_match($this->id, $rate_data_math->get_data());
					$rate_xml_ctrl = new rate_xml_ctrl($rate_data_math->get_id(), $this->competence_set_id, $this->options, $rate_data_math->get_data(), $resource_vector_match_math->get_focus_data(), $resource_vector_match_math->get_future_data());
				}
				$xdom->set_attr("match", $match);
			}
		}

		if (!$rate_xml_ctrl and $rate_data_math->exists())
		{
			$rate_xml_ctrl = new rate_xml_ctrl($rate_data_math->get_id(), $this->competence_set_id, $this->options, $rate_data_math->get_data());
		}

		if ($rate_xml_ctrl)
		{
			$this->xml_loader->add_xml($rate_xml_ctrl);
		}

		if ($user_id = $this->user->get_user_id())
		{
			$resource_status_helper = $this->get_resource_status_helper();
			$this->init_resource_status_helper($resource_status_helper, $this->data[0]);
			$resource_status_helper->modify_xml($xdom);
		}
	}

	public function get_resources_data()
	{
		return $this->get_resources_helper()->get_resources_data();
	}

	/**
	 * @return base_resources_helper
	 */
	abstract protected function get_resources_helper();

	/**
	 * @return base_resource_vector_match_math
	 */
	abstract protected function get_resource_vector_match_math($project_id, $user_id, $vector_calc_base);

	/**
	 * @return base_resource_status_helper
	 */
	abstract protected function get_resource_status_helper();

	abstract protected function init_resource_status_helper(base_resource_status_helper $resource_status_helper, $row);
}

?>