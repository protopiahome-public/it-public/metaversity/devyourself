<?php

abstract class base_resource_edit_rate_xml_ctrl extends base_dt_edit_xml_ctrl
{

	// Settings level 2
	protected $options = array(
		0 => "—",
		1 => "Косвенно развивает",
		2 => "Сфокусировано на этом",
	);
	protected $resource_name;
	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "rate_id",
			"ctrl" => "rate",
			"param2" => "options",
		),
	);
	protected $axis_name = "edit_rate";
	protected $enable_blocks = true;
	// Internal
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $competence_set_id;

	public function __construct($project_id, $resource_id)
	{
		$this->project_id = $project_id;
		$this->dt_name = $this->resource_name;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct($resource_id);
	}

	public function init()
	{
		$this->xml_loader->add_xml_by_class_name("{$this->resource_name}_short_xml_ctrl", $this->id, $this->project_id);

		$this->competence_set_id = $this->project_obj->get_vector_competence_set_id();
		if ($this->competence_set_id)
		{
			$this->xml_loader->add_xml_by_class_name("competence_set_short_xml_ctrl", $this->competence_set_id);
			$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->competence_set_id));
			$this->xml_loader->add_xml(new rate_multi_link_xml_ctrl($this->resource_name, $this->id, $this->competence_set_id, $this->options));
		}
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("dt.reflex_id, dt.reflex_project_id");
		$select_sql->add_where("project_id = {$this->project_id}");
	}

	protected function modify_xml(xdom $xdom)
	{
		if ($this->data[0]["reflex_id"])
		{
			$xdom->set_attr("reflex_id", $this->data[0]["reflex_id"]);
			$xdom->set_attr("reflex_project_id", $this->data[0]["reflex_project_id"]);
		}
	}

}

?>