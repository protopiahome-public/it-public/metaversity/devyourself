<?php

abstract class base_resource_edit_xml_ctrl extends base_dt_edit_xml_ctrl
{

	protected $resource_name;
	protected $axis_name = "edit";
	protected $project_id;
	protected $reflex_id;
	protected $dependencies_settings = array(
		array(
			"column" => "reflex_project_id",
			"ctrl" => "project_short",
		)
	);

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($project_id, $resource_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		$this->dt_name = $this->resource_name;
		parent::__construct($resource_id);
	}

	public function init()
	{
		$this->xml_loader->add_xml_by_class_name("{$this->resource_name}_short_xml_ctrl", $this->id, $this->project_id);
		
		$this->reflex_id = $this->db->get_value("SELECT reflex_id FROM {$this->resource_name} WHERE id = {$this->id}");
		if ($this->reflex_id > 0)
		{
			$this->axis_name = "edit_reflex";
		}
	}

	protected function on_after_dt_init()
	{
		$dtf = $this->dt->get_field("categories");
		/* @var $dtf multi_link_dtf */
		$dtf->set_restrict_sql("WHERE project_id = {$this->project_id}");
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("dt.reflex_id, dt.reflex_project_id");
		$select_sql->add_where("project_id = {$this->project_id}");
	}

	protected function modify_xml(xdom $xdom)
	{
		if ($this->reflex_id)
		{
			$xdom->set_attr("reflex_id", $this->reflex_id);
			$xdom->set_attr("reflex_project_id", $this->data[0]["reflex_project_id"]);
		}
	}

}

?>