<?php

abstract class base_resources_import_project_add_xml_ctrl extends base_xml_ctrl
{

	protected $resource_name;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($project_id)
	{
		$this->project_id = $project_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct();
	}

	public function get_xml()
	{
		return $this->get_node_string();
	}

}

?>