<?php

class material_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	protected $dt_name = "material";
	protected $axis_name = "short";
	protected $project_id;

	public function __construct($id, $project_id)
	{
		parent::__construct($id);
		$this->project_id = $project_id;
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("dt.reflex_id");
	}

	protected function process_data()
	{
		if (count($this->data) and $this->data[0]["reflex_id"])
		{
			$reflex_data = $this->db->get_row("
				SELECT project_id 
				FROM material 
				WHERE id = {$this->data[0]["reflex_id"]}
			");
			if ($reflex_data)
			{
				$this->store_data["reflex_project_id"] = $reflex_data["project_id"];
			}
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		parent::fill_xml($xdom);
		$url = "%PROJECT_URL%" . "materials/{$this->data[0]["id"]}/";
		$xdom->set_attr("url", $url);
	}

	// @dm9 понять про $this->data[0] - оно инициализировано вообще?
	protected function get_cache()
	{
		return material_short_cache::init($this->id, $this->project_id, $this->data[0] ? $this->data[0]["reflex_id"] : null);
	}

	protected function postprocess()
	{
		$url_project_obj = project_obj::instance(isset($this->store_data["reflex_project_id"]) ? $this->store_data["reflex_project_id"] : $this->project_id);
		$this->xml = str_replace("%PROJECT_URL1%", $url_project_obj->get_url(), $this->xml);
	}

}

?>