<?php

abstract class base_resources_import_select_xml_ctrl extends base_easy_xml_ctrl
{

	protected $resource_name;
	protected $calendar_on = true;
	protected $project_id;
	protected $page;
	protected $projects_data;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($project_id, $page)
	{
		$this->project_id = $project_id;
		$this->page = $page;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		$this->xml_row_name = $this->resource_name;
		$this->dependencies_settings = array(
			array(
				"column" => "id",
				"ctrl" => "{$this->resource_name}_short",
				"param2" => "project_id"
			)
		);
				
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 25));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("e.id, e.project_id");
		$select_sql->add_from("{$this->resource_name} e");
		$select_sql->add_join("LEFT JOIN project_import i ON (i.from_project_id = e.project_id)");
		$select_sql->add_join("LEFT JOIN {$this->resource_name} ei ON (e.id = ei.reflex_id)");
		$select_sql->add_where("i.to_project_id = {$this->project_id} AND i.{$this->resource_name}s_status = 1 AND e.reflex_id IS NULL AND (ei.project_id != {$this->project_id} OR ei.project_id IS NULL)");
		if ($this->calendar_on)
		{
			$select_sql->add_order("e.start_time DESC");
		}
		else
		{
			$select_sql->add_order("e.add_time DESC");
		}

		$this->projects_data = $this->db->fetch_all("SELECT * FROM project_import i WHERE i.to_project_id = {$this->project_id} AND i.{$this->resource_name}s_status = 1");

		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	protected function modify_xml(xdom $xdom)
	{
		foreach ($this->projects_data as $project_data)
		{
			$xdom->create_child_node("project")->set_attr("id", $project_data["from_project_id"]);
			$this->xml_loader->add_xml(new project_short_xml_ctrl($project_data["from_project_id"]));
		}
	}

}

?>
