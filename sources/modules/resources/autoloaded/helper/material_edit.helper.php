<?php

class material_edit_helper extends base_resource_edit_helper
{

	protected $resource_name = "material";

	protected function get_resources_tag($project_id)
	{
		return materials_cache_tag::init($project_id);
	}

}

?>