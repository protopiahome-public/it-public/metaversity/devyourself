<?php

class event_edit_helper extends base_resource_edit_helper
{

	protected $resource_name = "event";

	protected function get_resources_tag($project_id)
	{
		return events_cache_tag::init($project_id);
	}

}

?>