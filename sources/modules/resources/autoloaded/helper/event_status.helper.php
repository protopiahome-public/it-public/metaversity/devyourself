<?php

class event_status_helper extends base_resource_status_helper
{

	protected $resource_name = "event";
	protected $before_status_names = array(
		"0" => "none",
		"1" => "maybe",
		"2" => "go"
	);
	protected $after_status_names = array(
		"-1" => "missed",
		"0" => "unknown",
		"1" => "visited"
	);
	protected $event_start_timestamp;

	public function get_status()
	{
		return $this->event_start_timestamp > time() ? "not_started" : "finished";
	}

	public function modify_xml(xnode $xnode)
	{
		parent::modify_xml($xnode);
		$xnode->set_attr("status", $this->get_status());
	}

	public function set_before_status($before_status_name)
	{
		if ($this->get_status() != "not_started")
		{
			return false;
		}
		return parent::set_before_status($before_status_name);
	}

	public function set_after_status($after_status_name)
	{
		if ($this->get_status() != "finished")
		{
			return false;
		}
		return parent::set_after_status($after_status_name);
	}

	public function init_from_db($resource_id, $user_id)
	{
		$this->user_id = $user_id;
		$this->resource_id = $resource_id;

		$row = $this->db->get_row("
			SELECT l.before_status, l.after_status, UNIX_TIMESTAMP(e.start_time) AS start_timestamp, e.project_id
			FROM {$this->resource_name} e
			LEFT JOIN {$this->resource_name}_user_link l ON (l.{$this->resource_name}_id = e.id AND l.user_id = {$this->user_id})
			WHERE e.id = {$this->resource_id}
		");

		if (!$row)
		{
			return false;
		}
		else
		{
			$this->before_status = intval($row["before_status"]);
			$this->after_status = intval($row["after_status"]);
			$this->event_start_timestamp = $row["start_timestamp"];
			$this->project_id = $row["project_id"];

			return true;
		}
	}

	public function init_from_input($project_id, $resource_id, $user_id, $before_status, $after_status, $event_start_time)
	{
		$this->project_id = $project_id;
		$this->resource_id = $resource_id;
		$this->user_id = $user_id;
		if (!isset($this->before_status_names[$before_status]) || !isset($this->after_status_names[$after_status]))
		{
			return false;
		}
		else
		{
			$this->before_status = $before_status;
			$this->after_status = $after_status;
			$this->event_start_timestamp = $event_start_time;
			return true;
		}
	}

	protected function clean_cache()
	{
		// @todo abstract
		events_user_link_cache_tag::init($this->project_id, $this->user_id)->update();
		event_users_cache_tag::init($this->resource_id)->update();
	
		$reflexes_data = $this->db->fetch_all("
			SELECT DISTINCT project_id 
			FROM {$this->resource_name}
			WHERE reflex_id = {$this->resource_id}
		");
		foreach ($reflexes_data as $reflex_data)
		{
			events_user_link_cache_tag::init($reflex_data["project_id"], $this->user_id)->update();
		}
		
		user_events_cache_tag::init($this->user_id)->update();
	}

}

?>