<?php

class materials_helper extends base_resources_helper
{

	// Settings
	protected $resource_name = "material";
	protected $calendar_on = false;

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("block_set_id");
	}
	protected function get_resources_cache()
	{
		return materials_cache::init($this->project_id);
	}

}

?>