<?php

abstract class base_resources_helper extends base_db_helper
{

	// Settings
	protected $resource_name;
	protected $calendar_on;
	// Internal
	protected $project_id;
	protected $competence_set_id;

	public function __construct($project_id, $competence_set_id)
	{
		$this->project_id = $project_id;
		$this->competence_set_id = $competence_set_id;

		parent::__construct();
	}

	public function get_resources_data()
	{
		$select_sql = new select_sql();
		
		$data = $this->get_resources_cache($this->project_id)->get();
		if (!is_array($data))
		{
			// DB query config
			$select_sql->add_from("{$this->resource_name} r");
			$select_sql->add_select_fields("r.id");
			$select_sql->add_select_fields("rl.rate_id");

			//Time sort
			if ($this->calendar_on)
			{
				$select_sql->add_select_fields("IF(r.reflex_id > 0, UNIX_TIMESTAMP(reflex.start_time), UNIX_TIMESTAMP(r.start_time)) AS start_time");
			}
			else
			{
				$select_sql->add_select_fields("UNIX_TIMESTAMP(r.add_time) AS add_time");
			}

			//Categories
			// @todo очень так себе решение, поскольку размер GROUP_CONCAT по умолчанию ограничен весьма небольшим числом
			// Я бы сделал второй запрос \\dm9
			$select_sql->add_select_fields("GROUP_CONCAT(l.{$this->resource_name}_category_id SEPARATOR ',') as cat_ids");
			$select_sql->add_join("LEFT JOIN {$this->resource_name}_category_link l ON r.id = l.{$this->resource_name}_id");
			$select_sql->add_group_by("r.id");

			//Rate
			$select_sql->add_join("
				LEFT JOIN project p ON (p.id = r.project_id)
				LEFT JOIN {$this->resource_name}_rate_link rl
				ON (rl.{$this->resource_name}_id = r.id AND rl.competence_set_id = p.vector_competence_set_id)
			");

			//Reflex
			$select_sql->add_select_fields("r.reflex_id");
			$select_sql->add_select_fields("r.reflex_project_id");
			$select_sql->add_join("LEFT JOIN event reflex ON r.reflex_id = reflex.id");

			//Where
			$select_sql->add_where("r.project_id = {$this->project_id}");

			//Orders
			if ($this->calendar_on)
			{
				$select_sql->add_order("r.start_time ASC");
			}
			else
			{
				$select_sql->add_order("r.add_time DESC");
			}

			$this->modify_sql($select_sql);
			
			//Fetch data
			$data = $this->db->fetch_all($select_sql->get_sql());

			// Categories extraction
			foreach ($data as $key => $row)
			{
				$data[$key]["cats"] = $row["cat_ids"] ? explode(",", $row["cat_ids"]) : array();
			}

			// Building category tree
			$tree_data = $this->db->fetch_all("
				SELECT id, parent_id, position
				FROM {$this->resource_name}_category
				WHERE project_id = {$this->project_id}
			");
			require_once(PATH_INTCMF . "/tree.php");
			$tree = new tree($tree_data);
			$tree->make_tree();

			// Adding ancestors
			foreach ($data as &$row)
			{
				$cats_new = array();
				foreach ($row["cats"] as $cat_key => $cat_id)
				{
					if (!$cat_id)
					{
						continue;
					}
					$tree_item = $tree->get_by_id($cat_id);
					foreach ($tree_item["ancestors"] as $ancestor)
					{
						if (!in_array($ancestor["id"], $row["cats"]) && !in_array($ancestor["id"], $cats_new))
						{
							$cats_new[] = $ancestor["id"];
						}
					}
				}
				$row["cats"] = array_merge($row["cats"], $cats_new);
				$row["cat_ids"] = implode(",", $row["cats"]);
			}

			$this->get_resources_cache($this->project_id)->set($data);
		}
		foreach ($data as &$resource)
		{
			$rate_data_math = rate_data_math::get_instance($resource["rate_id"], $this->competence_set_id);
			$resource["rate_data"] = $rate_data_math->get_data();
		}
		return $data;
	}

	protected function modify_sql(select_sql $select_sql)
	{
		return;
	}
	
	abstract protected function get_resources_cache();
}

?>