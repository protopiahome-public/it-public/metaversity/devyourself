<?php

abstract class base_resource_edit_helper extends base_db_helper
{

	protected $resource_name;
	protected $id;

	public function __construct($resource_id)
	{
		$this->id = $resource_id;
		parent::__construct();
	}

	public function update_reflexes()
	{
		$reflex_projects = $this->db->fetch_all("
			SELECT DISTINCT project_id AS id
			FROM {$this->resource_name}
			WHERE reflex_id = {$this->id}
		");

		foreach ($reflex_projects as $reflex_project)
		{
			$this->get_resources_tag($reflex_project["id"])->update();
		}
	}

	/**
	 * @return cache_tag
	 */
	abstract protected function get_resources_tag($project_id);

}

?>