<?php

abstract class base_resource_status_helper
{

	protected $resource_name;
	protected $before_status_names = array();
	protected $after_status_names = array();
	protected $resource_id;
	protected $user_id;
	protected $before_status;
	protected $after_status;
	protected $project_id;

	/**
	 *
	 * @var db
	 */
	protected $db;

	public function __construct()
	{
		global $db;
		$this->db = $db;
	}

	public function get_before_status_name()
	{
		return $this->before_status_names[$this->before_status];
	}

	public function get_after_status_name()
	{
		return $this->after_status_names[$this->after_status];
	}

	public function modify_xml(xnode $xnode)
	{
		$xnode->set_attr("before_status", $this->get_before_status_name());
		$xnode->set_attr("after_status", $this->get_after_status_name());
	}

	public function set_before_status($before_status_name)
	{
		if (!in_array($before_status_name, $this->before_status_names))
		{
			return false;
		}
		else
		{
			$this->before_status = array_search($before_status_name, $this->before_status_names);

			$this->db->sql("
				INSERT INTO {$this->resource_name}_user_link ({$this->resource_name}_id, user_id, before_status, before_status_time, last_status_time)
					VALUES ({$this->resource_id}, {$this->user_id}, {$this->before_status}, NOW(), NOW())
				ON DUPLICATE KEY UPDATE
					before_status = ({$this->before_status}),
					before_status_time = NOW(),
					last_status_time = NOW()
			");

			$this->clean_cache();
			return true;
		}
	}

	public function set_after_status($after_status_name)
	{
		if (!in_array($after_status_name, $this->after_status_names))
		{
			return false;
		}
		else
		{
			$this->after_status = array_search($after_status_name, $this->after_status_names);

			$this->db->sql("
				INSERT INTO {$this->resource_name}_user_link ({$this->resource_name}_id, user_id, after_status, after_status_time, last_status_time)
					VALUES ({$this->resource_id}, {$this->user_id}, {$this->after_status}, NOW(), NOW())
				ON DUPLICATE KEY UPDATE
					after_status = ({$this->after_status}),
					after_status_time = NOW(),
					last_status_time = NOW()
			");

			$this->clean_cache();
			return true;
		}
	}

	public function init_from_db($resource_id, $user_id)
	{
		$this->user_id = $user_id;
		$this->resource_id = $resource_id;

		$row = $this->db->get_row("
			SELECT l.before_status, l.after_status, r.project_id
			FROM {$this->resource_name} r
			LEFT JOIN {$this->resource_name}_user_link l ON (l.{$this->resource_name}_id = r.id AND l.user_id = {$this->user_id})
			WHERE r.id = {$this->resource_id}
		");

		if (!$row)
		{
			return false;
		}
		else
		{
			$this->before_status = intval($row["before_status"]);
			$this->after_status = intval($row["after_status"]);
			$this->project_id = $row["project_id"];

			return true;
		}
	}

	abstract protected function clean_cache();
}

?>
