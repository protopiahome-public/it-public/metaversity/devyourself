<?php

class events_helper extends base_resources_helper
{

	// Settings
	protected $resource_name = "event";
	protected $calendar_on = true;

	protected function get_resources_cache()
	{
		return events_cache::init($this->project_id);
	}

}

?>