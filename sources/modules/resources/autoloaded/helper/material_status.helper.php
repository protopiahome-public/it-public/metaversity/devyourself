<?php

class material_status_helper extends base_resource_status_helper
{

	protected $resource_name = "material";
	protected $before_status_names = array(
		"0" => "none",
		"1" => "maybe",
		"2" => "read"
	);
	protected $after_status_names = array(
		"-1" => "not_read",
		"0" => "unknown",
		"1" => "have_read"
	);

	public function init_from_input($project_id, $resource_id, $user_id, $before_status, $after_status)
	{
		$this->project_id = $project_id;
		$this->resource_id = $resource_id;
		$this->user_id = $user_id;

		if (!isset($this->before_status_names[$before_status]) or !isset($this->after_status_names[$after_status]))
		{
			return false;
		}
		else
		{
			$this->before_status = $before_status;
			$this->after_status = $after_status;
			return true;
		}
	}

	protected function clean_cache()
	{
		materials_user_link_cache_tag::init($this->project_id, $this->user_id)->update();

		$reflexes_data = $this->db->fetch_all("
			SELECT DISTINCT project_id 
			FROM {$this->resource_name}
			WHERE reflex_id = {$this->resource_id}
		");
		foreach ($reflexes_data as $reflex_data)
		{
			materials_user_link_cache_tag::init($reflex_data["project_id"], $this->user_id)->update();
		}
	}

}

?>
