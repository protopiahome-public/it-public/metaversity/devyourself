<?php

class event_ignore_ajax_page extends base_resource_ignore_ajax_ctrl
{

	protected $resource_name = "event";

	public function clean_cache()
	{
		events_user_link_cache_tag::init($this->project_id, $this->user->get_user_id())->update();
	}

}

?>