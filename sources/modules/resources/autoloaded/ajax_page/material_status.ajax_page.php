<?php

class material_status_ajax_page extends base_resource_status_ajax_ctrl
{

	/**
	 * @return base_resource_status_helper
	 */
	protected function get_status_helper()
	{
		return new material_status_helper();
	}

	protected function return_html()
	{
		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new material_status_xml_ctrl($this->resource_id));
		$xml_loader->add_xslt("ajax/material_status.ajax", "resources");
		$xml_loader->run();
		$header = $xml_loader->get_content();

		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new material_status_xml_ctrl($this->resource_id, true));
		$xml_loader->add_xslt("ajax/material_status.ajax", "resources");
		$xml_loader->run();
		$footer = $xml_loader->get_content();

		return array(
			"status" => "OK",
			"header_html" => $header,
			"footer_html" => $footer
		);
	}

}

?>