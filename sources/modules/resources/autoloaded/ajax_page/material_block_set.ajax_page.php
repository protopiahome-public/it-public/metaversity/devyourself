<?php

class material_block_set_ajax_page extends base_block_set_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $material_id;

	public function on_before_start()
	{
		$this->material_id = POST("material_id");

		if (!is_good_num($this->material_id))
		{
			return false;
		}

		if ($this->material_id > 0 and !$this->db->row_exists("SELECT id FROM material WHERE id = {$this->material_id} AND project_id = {$this->project_id} LOCK IN SHARE MODE"))
		{
			return false;
		}

		return true;
	}

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		if (!$this->project_access->can_moderate_materials())
		{
			return false;
		}

		return true;
	}

	protected function get_object_url()
	{
		$object_url = "{$this->project_obj->get_url()}materials/";

		if ($this->material_id)
		{
			$object_url .= "{$this->material_id}/";
		}
		else
		{
			$object_url .= "add/";
		}

		return $object_url;
	}

}

?>