<?php

class events_day_list_ajax_page extends base_ajax_ctrl
{
	
	public function get_data()
	{
		$project_id = GET("project_id");
		$year = GET("year");
		$month = GET("month");
		$day = GET("day");
		if (!is_good_id($project_id) || !is_good_id($year) || !is_good_id($month) || !is_good_id($day))
		{
			return false;
		}

		require_once PATH_CORE . "/loader/xml_loader.php";
		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xml(new user_xml_ctrl());
		$xml_loader->add_xml(new events_xml_page($project_id, $page = 1, "calendar", $year, $month, $day));
		if ($user_id = $this->user->get_user_id())
		{
			$xml_loader->add_xml(new user_events_xml_page($user_id, $page = 1, "calendar", $year, $month, $day));
		}
		$xml_loader->add_xml(new project_short_xml_ctrl($project_id));
		$xml_loader->add_xslt("ajax/events_day_list.ajax", "resources");
		$xml_loader->run();
		return array("html" => $xml_loader->get_content());
	}

}

?>
