<?php

class event_comment_add_ajax_page extends base_comment_add_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_modify_sql",
		"project_stat_clean_cache"
	);

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	protected function get_type()
	{
		return "event";
	}

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->project_access->can_comment_events();
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("adder_user_id");
	}

	protected function update_object()
	{
		$user_id = $this->user->get_user_id();
		$this->db->sql("CALL event_comment_add_count_calc({$this->object_id}, {$this->project_id}, {$user_id})");
	}

	protected function filter_subscribers($subscribers_ids)
	{
		$project_read_access_helper = new project_read_access_helper($this->project_obj);
		$allowed_subscribers_ids = $project_read_access_helper->get_project_users_access_read($subscribers_ids);
		return $allowed_subscribers_ids;
	}

	protected function add_object_xml_ctrls(xml_loader $xml_loader)
	{
		$xml_loader->add_xml(new event_short_xml_ctrl($this->object_id, $this->project_id));
		$xml_loader->add_xml(new project_short_xml_ctrl($this->project_id));
	}

	protected function add_mail_xslt(xml_loader $xml_loader)
	{
		$xml_loader->add_xslt("mail/new_comment_{$this->type}", "resources");
	}

	public function clean_cache()
	{
		event_cache_tag::init($this->object_id)->update();
	}

}

?>
