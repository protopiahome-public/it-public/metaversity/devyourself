<?php

class events_calendar_ajax_page extends base_ajax_ctrl
{

	public function get_data()
	{
		$project_id = GET("project_id");
		$year = GET("year");
		$month = GET("month");
		if (!is_good_id($project_id) || !is_good_id($year) || !is_good_id($month))
		{
			$this->set_error_404();
			return array("status" => "error", "error" => "input");
		}
		require_once PATH_CORE . "/loader/xml_loader.php";
		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new user_xml_ctrl());
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xml(new events_calendar_xml_ctrl($project_id, $year, $month));
		$xml_loader->add_xml(new project_short_xml_ctrl($project_id));
		$xml_loader->add_xslt("ajax/events_calendar.ajax", "resources");
		$xml_loader->run();
		return array(
			"status" => "OK",
			"month" => $month,
			"year" => $year,
			"html" => $xml_loader->get_content(),
		);
	}

}

?>
