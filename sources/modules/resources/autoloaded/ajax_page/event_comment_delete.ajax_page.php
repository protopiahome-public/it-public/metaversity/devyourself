<?php

class event_comment_delete_ajax_page extends base_comment_delete_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_modify_sql",
		"project_stat_clean_cache"
	);

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	protected function get_type()
	{
		return "event";
	}

	public function check_rights()
	{
		$project_access = $this->project_obj->get_access();
		return $project_access->can_moderate_events();
	}

	protected function update_object()
	{
		$user_id = $this->comment_data["author_user_id"];
		$this->db->sql("CALL event_comment_" . ($this->is_deleted ? "delete" : "add") . "_count_calc({$this->object_id}, {$this->project_id}, {$user_id})");
	}

	public function clean_cache()
	{
		event_cache_tag::init($this->object_id)->update();
	}

}

?>
