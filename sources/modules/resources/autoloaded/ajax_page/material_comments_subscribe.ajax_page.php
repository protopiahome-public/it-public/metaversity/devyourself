<?php

class material_comments_subscribe_ajax_page extends base_comments_subscribe_ajax_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_modify_sql",
	);
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;

	protected function get_type()
	{
		return "material";
	}

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->project_access->can_read_materials();
	}

}

?>
