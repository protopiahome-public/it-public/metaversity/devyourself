<?php

class event_status_ajax_page extends base_resource_status_ajax_ctrl
{

	/**
	 * @return base_resource_status_helper
	 */
	protected function get_status_helper()
	{
		return new event_status_helper();
	}

	protected function return_html()
	{
		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new event_status_xml_ctrl($this->resource_id, POST("freeze") === "1"));
		$xml_loader->add_xslt("ajax/event_status.ajax", "resources");
		$xml_loader->run();
		return array(
			"status" => "OK",
			"html" => $xml_loader->get_content(),
		);
	}

}

?>