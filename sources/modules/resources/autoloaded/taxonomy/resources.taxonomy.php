<?php

final class resources_taxonomy extends base_taxonomy
{

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, project_obj $project_obj)
	{
		$this->project_obj = $project_obj;
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		$p = $this->get_parts_relative();

		$project_obj = $this->project_obj;
		$project_id = $project_obj->get_id();
		$project_access = $project_obj->get_access();
		
		if ($p[1] === "events" and !$project_obj->events_are_on())
		{
			$this->xml_loader->set_error_404();
			$this->xml_loader->set_error_404_xslt("project_module_404", "project");
		}
		elseif ($p[1] === "events" and !$project_access->can_read_events())
		{
			$this->xml_loader->set_error_403();
			$this->xml_loader->set_error_403_xslt("events_403", "resources");
		}
		elseif ($p[1] === "events")
		{
			$is_event_list = false;
			if ($page = $this->is_page_folder($p[2]) and $p[3] === null)
			{
				//p/<project_name>/events/[page-<page>/]
				$is_event_list = true;
				$p[2] = null;
			}
			elseif ($p[2] === "archive" and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				//p/<project_name>/events/archive/[page-<page>/]
				$is_event_list = true;
				$p[3] = null;
			}
			elseif ($p[2] === "calendar" and is_good_id($p[3]) and $page = $this->is_page_folder($p[4]) and $p[5] === null)
			{
				//p/<project_name>/events/calendar/<year>/[page-<page>/]
				$is_event_list = true;
				$p[4] = null;
			}
			elseif ($p[2] === "calendar" and is_good_id($p[3]) and is_good_id($p[4]) and $page = $this->is_page_folder($p[5]) and $p[6] === null)
			{
				//p/<project_name>/events/calendar/<year>/<month>/[page-<page>/]
				$is_event_list = true;
				$p[5] = null;
			}
			elseif ($p[2] === "calendar" and is_good_id($p[3]) and is_good_id($p[4]) and is_good_id($p[5]) and $page = $this->is_page_folder($p[6]) and $p[7] === null)
			{
				//p/<project_name>/events/calendar/<year>/<month>/<day>/[page-<page>/]
				$is_event_list = true;
				$p[6] = null;
			}

			if (!$is_event_list)
			{
				$this->xml_loader->add_xml(new events_calendar_xml_ctrl($project_id));
			}

			if ($is_event_list)
			{
				$this->xml_loader->add_xml_with_xslt(new events_xml_page($project_id, $page, $p[2], $p[3], $p[4], $p[5]));

				$this->xml_loader->add_xml_by_class_name("events_categories_xml_ctrl", $project_id);
			}
			elseif ($p[2] === "calendar" and $p[3] === null)
			{
				//p/<project_name>/events/calendar/
				$this->set_redirect_url("{$p[1]}/");
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//p/<project_name>/events/add/
				$this->xml_loader->add_xml_with_xslt(new event_add_xml_page($project_id), null, "event_403");
			}
			elseif ($p[2] === "import" and $p[3] === null)
			{
				//p/<project_name>/events/import/
				$this->xml_loader->add_xml_with_xslt(new events_import_projects_xml_page($project_id));
			}
			elseif ($p[2] === "import" and $p[3] === "add" and $p[4] === null)
			{
				//p/<project_name>/events/materials/add/
				$this->xml_loader->add_xml_with_xslt(new events_import_project_add_xml_page($project_id));
			}
			elseif ($p[2] === "import" and $p[3] === "select" and $page = $this->is_page_folder($p[4]) and $p[5] === null)
			{
				//p/<project_name>/events/import/select/
				$this->xml_loader->add_xml_with_xslt(new events_import_select_xml_page($project_id, $page));
			}
			elseif (is_good_id($p[2]))
			{
				if ($p[3] === null)
				{
					//p/<project_name>/events/<id>/
					$this->xml_loader->add_xml_with_xslt(new event_show_xml_page($project_id, $p[2]));
					$this->xml_loader->add_xml(new event_users_xml_ctrl($project_id, $p[2]));
				}
				elseif ($p[3] === "edit" and $p[4] === null)
				{
					//p/<project_name>/events/<id>/edit/
					$this->xml_loader->add_xml_with_xslt(new event_edit_xml_page($project_id, $p[2]), null, "event_403");
				}
				elseif ($p[3] === "edit" and $p[4] === "rate" and $p[5] === null)
				{
					//p/<project_name>/events/<id>/edit/rate/
					$this->xml_loader->add_xml_with_xslt(new event_edit_rate_xml_page($project_id, $p[2]), null, "event_403");
				}
				elseif ($p[3] === "delete" and $p[4] === null)
				{
					//p/<project_name>/events/<id>/delete/
					$this->xml_loader->add_xml_with_xslt(new event_delete_xml_page($project_id, $p[2]), null, "event_403");
				}
			}
		}
		elseif ($p[1] === "materials" and !$project_obj->materials_are_on())
		{
			$this->xml_loader->set_error_404();
			$this->xml_loader->set_error_404_xslt("project_module_404", "project");
		}
		elseif ($p[1] === "materials" and !$project_access->can_read_materials())
		{
			$this->xml_loader->set_error_403();
			$this->xml_loader->set_error_403_xslt("materials_403", "resources");
		}
		elseif ($p[1] === "materials")
		{
			if ($page = $this->is_page_folder($p[2]) and $p[3] === null)
			{
				//p/<project_name>/materials/[page-<page>/]
				$this->xml_loader->add_xml_with_xslt(new materials_xml_page($project_id, $page));

				$this->xml_loader->add_xml_by_class_name("materials_categories_xml_ctrl", $project_id);
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//p/<project_name>/materials/add/
				$this->xml_loader->add_xml_with_xslt(new material_add_xml_page($project_id), null, "material_403");
			}
			elseif ($p[2] === "add" and $file_id = $this->is_type_folder($p[3], "file", true) and $p[4] === null)
			{
				//p/<project_name>/materials/add/file-<id>/
				$this->xml_loader->add_xml_with_xslt(new material_file_download_xml_page($file_id, $project_id), null, "material_403");
			}
			elseif ($p[2] === "import" and $p[3] === null)
			{
				//p/<project_name>/materials/import/
				$this->xml_loader->add_xml_with_xslt(new materials_import_projects_xml_page($project_id));
			}
			elseif ($p[2] === "import" and $p[3] === "add" and $p[4] === null)
			{
				//p/<project_name>/materials/add/
				$this->xml_loader->add_xml_with_xslt(new materials_import_project_add_xml_page($project_id));
			}
			elseif ($p[2] === "import" and $p[3] === "select" and $page = $this->is_page_folder($p[4]) and $p[5] === null)
			{
				//p/<project_name>/materials/import/select/
				$this->xml_loader->add_xml_with_xslt(new materials_import_select_xml_page($project_id, $page));
			}
			elseif (is_good_id($p[2]))
			{
				if ($p[3] === null)
				{
					//p/<project_name>/materials/<id>/
					$this->xml_loader->add_xml_with_xslt(new material_show_xml_page($project_id, $p[2]));
				}
				elseif ($file_id = $this->is_type_folder($p[3], "file", true) and $p[4] === null)
				{
					//p/<project_name>/materials/<id>/file-<id>/
					$this->xml_loader->add_xml_with_xslt(new material_file_download_xml_page($file_id, $project_id, $p[2]), null, "material_403");
				}
				elseif ($p[3] === "edit" and $p[4] === null)
				{
					//p/<project_name>/materials/<id>/edit/
					$this->xml_loader->add_xml_with_xslt(new material_edit_xml_page($project_id, $p[2]), null, "material_403");
				}
				elseif ($p[3] === "edit" and $p[4] === "rate" and $p[5] === null)
				{
					//p/<project_name>/materials/<id>/edit/rate/
					$this->xml_loader->add_xml_with_xslt(new material_edit_rate_xml_page($project_id, $p[2]), null, "material_403");
				}
				elseif ($p[3] === "delete" and $p[4] === null)
				{
					//p/<project_name>/materials/<id>/delete/
					$this->xml_loader->add_xml_with_xslt(new material_delete_xml_page($project_id, $p[2]), null, "material_403");
				}
			}
		}
	}

}

?>