<?php

abstract class base_resources_import_select_save_ctrl extends base_save_ctrl
{

	protected $resource_name;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $project_id;
	protected $select_resource_ids;

	public function set_up()
	{
		$this->select_resource_ids = POST_AS_ARRAY("select_{$this->resource_name}_ids");

		return true;
	}

	public function start()
	{
		foreach ($this->select_resource_ids as $select_resource_id)
		{
			if (is_array($select_resource_id) or !is_good_id($select_resource_id))
			{
				return false;
			}
		}

		return true;
	}

	public function check()
	{
		foreach ($this->select_resource_ids as $select_resource_id)
		{
			$select_resource_exists = $this->db->row_exists("
				SELECT e.id 
				FROM {$this->resource_name} e
				JOIN project_import i ON e.project_id = i.from_project_id
				WHERE e.id = {$select_resource_id} AND i.to_project_id = {$this->project_id} AND {$this->resource_name}s_status = 1 AND reflex_id IS NULL
			");
			if (!$select_resource_exists)
			{
				return false;
			}

			$already_imported = $this->db->row_exists("
				SELECT e.id 
				FROM {$this->resource_name} e
				WHERE project_id = {$this->project_id} AND reflex_id = {$select_resource_id}
			");
			if ($already_imported)
			{
				return false;
			}
		}

		return true;
	}

	public function commit()
	{
		$user_id = $this->user->get_user_id();

		foreach ($this->select_resource_ids as $select_resource_id)
		{
			$this->db->sql("
				INSERT INTO {$this->resource_name} (project_id, title, announce, reflex_id, reflex_project_id, add_time, edit_time, adder_user_id)
				(
					SELECT {$this->project_id}, title, announce, id, project_id, NOW(), NOW(), {$user_id}
					FROM {$this->resource_name} 
					WHERE id = {$select_resource_id}
				)
			");
			$reflex_resource_id = $this->db->get_last_id();

			$rate_link_db_result = $this->db->sql("SELECT * FROM {$this->resource_name}_rate_link WHERE {$this->resource_name}_id = {$select_resource_id}");
			while ($rate_link_data = $this->db->get_row($rate_link_db_result))
			{
				$select_rate_id = $rate_link_data["rate_id"];

				$this->db->sql("
					INSERT INTO rate (rate_type_id, competence_set_id, add_time, edit_time)
					(
						SELECT rate_type_id, competence_set_id, NOW(), NOW()
						FROM rate
						WHERE id = {$select_rate_id}
					)
				");
				$reflex_rate_id = $this->db->get_last_id();

				$this->db->sql("
					INSERT INTO rate_competence_link (rate_id, competence_id, mark)
					(
						SELECT {$reflex_rate_id}, competence_id, mark
						FROM rate_competence_link
						WHERE rate_id = {$select_rate_id}
					)
				");

				$this->db->sql("
					INSERT INTO {$this->resource_name}_rate_link ({$this->resource_name}_id, rate_id, competence_set_id, competence_count_calc)
					(
						SELECT {$reflex_resource_id}, {$reflex_rate_id}, competence_set_id, competence_count_calc
						FROM {$this->resource_name}_rate_link
						WHERE {$this->resource_name}_id = {$select_resource_id} AND rate_id = {$select_rate_id}
					)
				");
			}
		}

		return true;
	}

}

?>
