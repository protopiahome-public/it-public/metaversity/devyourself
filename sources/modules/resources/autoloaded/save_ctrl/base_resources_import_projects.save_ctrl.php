<?php

abstract class base_resources_import_projects_save_ctrl extends base_save_ctrl
{

	protected $resource_name;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $project_id;
	protected $action;
	protected $add_project_url;
	protected $add_project_id;
	protected $delete_project_id;

	public function set_up()
	{
		$this->project_id = POST("project_id");
		$this->action = POST("action");

		$this->add_project_url = POST("add_project_url");
		$this->delete_project_id = POST("delete_project_id");

		return true;
	}

	public function start()
	{
		if (!($this->action == "add" or $this->action == "delete"))
		{
			return false;
		}

		if ($this->action == "delete")
		{
			if (!is_good_id($this->delete_project_id))
			{
				return false;
			}
		}

		return true;
	}

	public function check()
	{
		if ($this->action == "add")
		{
			if (!$this->add_project_id = url_helper::extract_project_id($this->add_project_url))
			{
				$this->pass_info->write_error("wrong_add_project_url");
				$this->pass_info->dump_vars();
				return false;
			}

			if ($this->add_project_id == $this->project_id)
			{
				$this->pass_info->write_error("add_itself");
				$this->pass_info->dump_vars();
				return false;
			}

			$add_project_exists = $this->db->row_exists("SELECT * FROM project WHERE id = {$this->add_project_id}");
			if (!$add_project_exists)
			{
				$this->pass_info->write_error("wrong_add_project_url");
				return false;
			}
		}

		if ($this->action == "delete")
		{
			$remove_project_exists = $this->db->row_exists("SELECT * FROM project_import WHERE to_project_id = {$this->project_id} AND from_project_id = {$this->delete_project_id} AND {$this->resource_name}s_status IN (1,-1)");
			if (!$remove_project_exists)
			{
				$this->pass_info->write_error("wrong_delete_project");
				return false;
			}
		}

		return true;
	}

	public function commit()
	{
		if ($this->action == "add")
		{
			$this->db->sql("
				INSERT INTO project_import (to_project_id, from_project_id, {$this->resource_name}s_status)
				VALUES ({$this->project_id}, {$this->add_project_id}, 1)
				ON DUPLICATE KEY UPDATE {$this->resource_name}s_status = 1
			");
		}

		if ($this->action == "delete")
		{
			$this->db->sql("
				UPDATE project_import
				SET {$this->resource_name}s_status = 0
				WHERE to_project_id = {$this->project_id} AND from_project_id = {$this->delete_project_id}
			");
		}

		return true;
	}

}

?>