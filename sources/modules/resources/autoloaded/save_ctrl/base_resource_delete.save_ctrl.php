<?php

abstract class base_resource_delete_save_ctrl extends base_delete_save_ctrl
{

	protected $resource_name;
	protected $project_id;
	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $project_row;

	public function init()
	{
		$this->db_table = $this->resource_name;
	}

	public function on_before_start()
	{
		$this->project_row = $this->db->get_row("SELECT * FROM project WHERE id = {$this->project_id} LOCK IN SHARE MODE");
		if (!$this->project_row["vector_competence_set_id"])
		{
			return false;
		}
		return true;
	}

	public function on_before_commit()
	{
		$this->db->sql("
			DELETE r
			FROM rate r
			LEFT JOIN {$this->resource_name}_rate_link rl ON (r.id = rl.rate_id)
			WHERE rl.{$this->resource_name}_id = {$this->old_db_row["id"]};
		");

		return true;
	}

}

?>