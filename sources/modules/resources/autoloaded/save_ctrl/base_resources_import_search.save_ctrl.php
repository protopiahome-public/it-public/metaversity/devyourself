<?php

abstract class base_resources_import_search_save_ctrl extends base_save_ctrl
{

	protected $resource_name;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 * @var project_access
	 */
	protected $project_access;
	protected $project_id;
	protected $select_project_ids;

	public function set_up()
	{
		$this->select_project_ids = POST_AS_ARRAY("select_project_ids");

		return true;
	}

	public function start()
	{
		foreach ($this->select_project_ids as $select_project_id)
		{
			if (is_array($select_project_id) or !is_good_id($select_project_id))
			{
				return false;
			}
		}

		return true;
	}

	public function check()
	{
		if (!count($this->select_project_ids))
		{
			$this->pass_info->write_error("projects_not_selected");
			return false;
		}

		foreach ($this->select_project_ids as $select_project_id)
		{
			$select_project_exists = $this->db->row_exists("SELECT * FROM project_import WHERE to_project_id = {$this->project_id} AND from_project_id = {$select_project_id} AND {$this->resource_name}s_status IN (1,-1)");
			if (!$select_project_exists)
			{
				$this->pass_info->write_error("wrong_projects_selected");
				return false;
			}
		}

		return true;
	}

	public function commit()
	{
		foreach ($this->select_project_ids as $select_project_id)
		{
			$this->db->sql("
				UPDATE project_import
				SET {$this->resource_name}s_status = 1
				WHERE to_project_id = {$this->project_id} AND from_project_id = {$select_project_id}
			");
		}

		$select_project_ids_sql = implode(", ", $this->select_project_ids);
		$this->db->sql("
				UPDATE project_import
				SET {$this->resource_name}s_status = -1
				WHERE 
					{$this->resource_name}s_status != 0 AND
					to_project_id = {$this->project_id} AND 
					from_project_id NOT IN ({$select_project_ids_sql});
			");

		return true;
	}

}

?>
