<?php

abstract class base_resource_edit_save_ctrl extends base_dt_edit_save_ctrl
{

	protected $resource_name;
	protected $axis_name = "edit";
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $reflex_id;

	public function __construct()
	{
		parent::__construct();

		if (is_good_id(REQUEST("id")))
		{
			$this->reflex_id = $this->db->get_value("SELECT reflex_id FROM {$this->resource_name} WHERE id = " . REQUEST("id"));

			if ($this->reflex_id > 0)
			{
				$this->axis_name = "edit_reflex";
			}
		}
	}

	public function init()
	{
		$this->dt_name = $this->resource_name;
	}

	protected function on_after_dt_init()
	{
		$dtf = $this->dt->get_field("categories");
		/* @var $dtf multi_link_dtf */
		$dtf->set_restrict_sql("WHERE project_id = {$this->project_id}");
		return true;
	}

}

?>