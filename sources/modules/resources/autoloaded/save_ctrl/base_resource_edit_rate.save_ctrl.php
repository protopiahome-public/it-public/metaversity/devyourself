<?php

abstract class base_resource_edit_rate_save_ctrl extends base_save_ctrl
{

	protected $resource_name;
	protected $rate_type;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $resource_id;
	protected $resource_row;

	/**
	 * @var rate_save_ctrl
	 */
	protected $rate_save_ctrl;

	public function __construct()
	{
		$this->rate_type = $this->resource_name;
		parent::__construct();
	}

	public function init()
	{
		$this->rate_save_ctrl = new rate_multi_link_save_ctrl($this->rate_type, array(0, 1, 2));
		$this->save_loader->add_ctrl($this->rate_save_ctrl);
	}

	public function on_before_start()
	{
		$this->resource_id = POST("id");
		if (!is_good_id($this->resource_id))
		{
			return false;
		}
		$this->resource_row = $this->db->get_row("
			SELECT r.id, r.project_id
			FROM {$this->resource_name} r
			WHERE 
				r.id = {$this->resource_id} 
				AND r.project_id = {$this->project_id} 
			LOCK IN SHARE MODE
		");
		if (!$this->resource_row)
		{
			return false;
		}
		return true;
	}

	public function on_after_start()
	{
		$this->rate_save_ctrl->set_competence_set_id($this->project_obj->get_vector_competence_set_id());
		$this->rate_save_ctrl->set_object_id($this->resource_id);
		return true;
	}

}

?>