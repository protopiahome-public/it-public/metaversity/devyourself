<?php

abstract class base_resource_add_save_ctrl extends base_dt_add_save_ctrl
{

	protected $resource_name;
	protected $axis_name = "edit";
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function init()
	{
		$this->dt_name = $this->resource_name;
	}

	protected function on_after_dt_init()
	{
		$dtf = $this->dt->get_field("categories");
		/* @var $dtf multi_link_dtf */
		$dtf->set_restrict_sql("WHERE project_id = {$this->project_id}");
		return true;
	}

	public function on_before_commit()
	{
		$this->update_array["project_id"] = "'{$this->project_id}'";
		$this->update_array["adder_user_id"] = $this->user->get_user_id();
		return true;
	}

	public function on_after_commit()
	{
		comments_subscribe_helper::subscribe($this->resource_name, $this->last_id, $this->user->get_user_id());
	}

}

?>