<?php

class materials_import_select_xml_page extends base_resources_import_select_xml_ctrl
{

	protected $resource_name = "material";
	protected $calendar_on = false;

	public function check_rights()
	{
		return $this->project_access->can_moderate_materials();
	}

}

?>
