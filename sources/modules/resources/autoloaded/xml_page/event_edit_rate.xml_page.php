<?php

class event_edit_rate_xml_page extends base_resource_edit_rate_xml_ctrl
{

	protected $resource_name = "event";

	public function check_rights()
	{
		return $this->project_access->can_moderate_events();
	}

}

?>