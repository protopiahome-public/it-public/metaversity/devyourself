<?php

class material_delete_xml_page extends base_resource_delete_xml_ctrl
{

	protected $resource_name = "material";

	public function check_rights()
	{
		return $this->project_access->can_moderate_materials();
	}

}

?>