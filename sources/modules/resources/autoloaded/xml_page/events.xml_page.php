<?php

class events_xml_page extends base_resources_xml_ctrl
{

	// Settings
	protected $resource_name = "event";
	protected $calendar_on = true;

	protected function get_resources_helper()
	{
		return new events_helper($this->project_id, $this->competence_set_id);
	}

	protected function get_resources_user_link_cache($project_id, $user_id)
	{
		return events_user_link_cache::init($project_id, $user_id);
	}

	protected function get_resource_vector_match_math($project_id, $user_id, $vector_calc_base)
	{
		return new event_vector_match_math($project_id, $user_id, $vector_calc_base);
	}

	protected function get_resource_status_helper()
	{
		return new event_status_helper();
	}

	protected function init_resource_status_helper(base_resource_status_helper $resource_status_helper, $row)
	{
		/* @var $resource_status_helper event_status_helper */
		$resource_status_helper->init_from_input($this->project_id, $row["reflex_id"] ? $row["reflex_id"] : $row["id"], $this->user->get_user_id(), intval(isset($this->user_link_list[$row["id"]]) ? $this->user_link_list[$row["id"]]["before_status"] : 0), intval(isset($this->user_link_list[$row["id"]]) ? $this->user_link_list[$row["id"]]["after_status"] : 0), $row["start_time"]);
	}

}

?>