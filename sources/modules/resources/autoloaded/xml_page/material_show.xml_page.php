<?php

class material_show_xml_page extends base_resource_show_xml_ctrl
{

	protected $resource_name = "material";
	protected $with_cut = true;
	protected $dependencies_settings = array(
		array(
			"column" => "competence_set_id",
			"ctrl" => "competence_set_short",
		), array(
			"column" => "block_set_id",
			"ctrl" => "block_set",
			"param2" => "with_cut"
		));

	protected function modify_sql(select_sql $select_sql)
	{
		parent::modify_sql($select_sql);
		$select_sql->add_select_fields("block_set_id");
	}
	
	protected function get_resources_helper()
	{
		return new materials_helper($this->project_id, $this->competence_set_id);
	}

	protected function get_resource_vector_match_math($project_id, $user_id, $vector_calc_base)
	{
		return new material_vector_match_math($project_id, $user_id, $vector_calc_base);
	}

	protected function get_resource_status_helper()
	{
		return new material_status_helper();
	}

	protected function init_resource_status_helper(base_resource_status_helper $resource_status_helper, $row)
	{
		$resource_status_helper->init_from_input($this->project_id, $row["id"], $this->user->get_user_id(), intval($row["before_status"]), intval($row["after_status"]));
	}

}

?>