<?php

class event_add_xml_page extends base_resource_add_xml_ctrl
{

	// Settings
	protected $resource_name = "event";

	public function check_rights()
	{
		return $this->project_access->can_moderate_events();
	}

}

?>