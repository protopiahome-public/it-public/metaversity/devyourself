<?php

class materials_import_project_add_xml_page extends base_resources_import_project_add_xml_ctrl
{

	protected $resource_name = "material";

	public function check_rights()
	{
		return $this->project_access->can_moderate_materials();
	}

}

?>
