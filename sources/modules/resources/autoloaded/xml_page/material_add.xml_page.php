<?php

class material_add_xml_page extends base_resource_add_xml_ctrl
{

	// Settings
	protected $resource_name = "material";

	public function start()
	{
		$this->xml_loader->add_xml(new block_set_xml_ctrl());
		return parent::start();
	}

	public function check_rights()
	{
		return $this->project_access->can_moderate_materials();
	}

}

?>