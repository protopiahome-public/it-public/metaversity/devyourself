<?php

class material_file_download_xml_page extends base_block_set_file_download_xml_ctrl
{

	protected $material_id;

	/**
	 *
	 * @var project_obj
	 */
	protected $project_obj;

	/**
	 *
	 * @var project_access
	 */
	protected $project_access;

	public function __construct($block_file_id, $project_id, $material_id = 0)
	{
		$this->project_id = $project_id;
		$this->material_id = $material_id;

		$this->project_obj = project_obj::instance($this->project_id);
		$this->project_access = $this->project_obj->get_access();

		parent::__construct($block_file_id);
	}

	protected function check_file_rights()
	{
		if (!$this->project_access->can_read_materials())
		{
			return false;
		}

		$material_data = $this->db->get_row("
			SELECT *
			FROM material
			WHERE id = {$this->material_id}
		");

		if (!$material_data)
		{
			return false;
		}
		if ($material_data["project_id"] != $this->project_id)
		{
			return false;
		}
		if ($material_data["block_set_id"] != $this->data["block_set_id"])
		{
			return false;
		}

		return true;
	}

}

?>
