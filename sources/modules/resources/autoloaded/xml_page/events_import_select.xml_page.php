<?php

class events_import_select_xml_page extends base_resources_import_select_xml_ctrl
{

	protected $resource_name = "event";

	public function check_rights()
	{
		return $this->project_access->can_moderate_events();
	}

}

?>
