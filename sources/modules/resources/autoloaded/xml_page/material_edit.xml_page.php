<?php

class material_edit_xml_page extends base_resource_edit_xml_ctrl
{

	protected $resource_name = "material";

	public function __construct($project_id, $resource_id)
	{
		parent::__construct($project_id, $resource_id);

		if (!$this->reflex_id)
		{
			$this->dependencies_settings[] = array(
				"column" => "block_set_id",
				"ctrl" => "block_set",
			);
		}
	}

	public function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("IF(block_set_id > 0, block_set_id, 0) as block_set_id");
	}

	public function check_rights()
	{
		return $this->project_access->can_moderate_materials();
	}

}

?>