<?php

class materials_user_link_cache extends base_cache
{

	public static function init($project_id, $user_id)
	{
		$tag_keys = array();
		// Not required
		//$tag_keys[] = materials_cache_tag::init($project_id)->get_key();
		$tag_keys[] = materials_user_link_cache_tag::init($project_id, $user_id)->get_key();
		return parent::get_cache(__CLASS__, $project_id, $user_id, $tag_keys);
	}

}

?>