<?php

class event_vector_match_cache extends base_cache
{

	public static function init($event_id, $vector_id, $competence_set_id, $calc_base)
	{
		$tag_keys = array();
		$tag_keys[] = event_cache_tag::init($event_id)->get_key();
		$tag_keys[] = vector_cache_tag::init($vector_id)->get_key();
		$tag_keys[] = competence_set_competences_cache_tag::init($competence_set_id)->get_key();;
		return parent::get_cache(__CLASS__, $event_id, $vector_id, $competence_set_id, $calc_base, $tag_keys);
	}

}

?>