<?php

class materials_categories_cache extends base_cache
{

	public static function init($project_id)
	{
		$tag_keys = array();
		$tag_keys[] = materials_categories_cache_tag::init($project_id)->get_key();
		return parent::get_cache(__CLASS__, $project_id, $tag_keys);
	}

}

?>