<?php

class material_short_cache extends base_cache
{

	public static function init($material_id, $project_id, $reflex_id = null)
	{
		$tag_keys = array();
		$tag_keys[] = material_cache_tag::init($material_id)->get_key();
		if ($reflex_id)
		{
			$tag_keys[] = material_cache_tag::init($reflex_id)->get_key();
		}
		$tag_keys[] = materials_categories_cache_tag::init($project_id)->get_key();
		return parent::get_cache(__CLASS__, $material_id, $project_id, $tag_keys);
	}

}

?>