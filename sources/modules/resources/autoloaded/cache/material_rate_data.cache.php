<?php

class event_rate_data_cache extends base_cache
{

	public static function init($rate_id, $competence_set_id)
	{
		$tag_keys = array();
		$tag_keys[] = rate_cache_tag::init($rate_id)->get_key();
		$tag_keys[] = competence_set_competences_cache_tag::init($competence_set_id)->get_key();
		return parent::get_cache(__CLASS__, $rate_id, $tag_keys);
	}

}

?>