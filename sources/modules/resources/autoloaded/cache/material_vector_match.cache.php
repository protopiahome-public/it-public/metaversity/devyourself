<?php

class material_vector_match_cache extends base_cache
{

	public static function init($material_id, $vector_id, $competence_set_id, $calc_base)
	{
		$tag_keys = array();
		$tag_keys[] = material_cache_tag::init($material_id)->get_key();
		$tag_keys[] = vector_cache_tag::init($vector_id)->get_key();
		$tag_keys[] = competence_set_competences_cache_tag::init($competence_set_id)->get_key();;
		return parent::get_cache(__CLASS__, $material_id, $vector_id, $competence_set_id, $calc_base, $tag_keys);
	}

}

?>