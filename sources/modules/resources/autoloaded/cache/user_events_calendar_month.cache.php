<?php

class user_events_calendar_month_cache extends base_cache
{

	public static function init($user_id, $year, $month)
	{
		$tag_keys = array();
		$tag_keys[] = user_events_cache_tag::init($user_id)->get_key();
		return parent::get_cache(__CLASS__, $user_id, $year, $month, $tag_keys);
	}

}

?>