<?php

class event_users_cache extends base_cache
{

	public static function init($event_id)
	{
		$tag_keys = array();
		$tag_keys[] = event_users_cache_tag::init($event_id)->get_key();
		return parent::get_cache(__CLASS__, $event_id, $tag_keys);
	}

}

?>