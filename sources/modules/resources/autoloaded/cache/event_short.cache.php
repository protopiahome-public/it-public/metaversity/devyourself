<?php

class event_short_cache extends base_cache
{

	public static function init($event_id, $project_id, $reflex_id = null)
	{
		$tag_keys = array();
		$tag_keys[] = event_cache_tag::init($event_id)->get_key();
		if ($reflex_id)
		{
			$tag_keys[] = event_cache_tag::init($reflex_id)->get_key();
		}
		$tag_keys[] = events_categories_cache_tag::init($project_id)->get_key();
		return parent::get_cache(__CLASS__, $event_id, $project_id, $tag_keys);
	}

}

?>