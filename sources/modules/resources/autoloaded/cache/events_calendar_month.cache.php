<?php

class events_calendar_month_cache extends base_cache
{

	public static function init($project_id, $year, $month)
	{
		$tag_keys = array();
		$tag_keys[] = events_cache_tag::init($project_id)->get_key();
		return parent::get_cache(__CLASS__, $project_id, $year, $month, $tag_keys);
	}

}

?>