<?php

class events_user_link_cache extends base_cache
{

	public static function init($project_id, $user_id)
	{
		$tag_keys = array();
		// Not required
		//$tag_keys[] = events_cache_tag::init($project_id)->get_key();
		$tag_keys[] = events_user_link_cache_tag::init($project_id, $user_id)->get_key();
		return parent::get_cache(__CLASS__, $project_id, $user_id, $tag_keys);
	}

}

?>