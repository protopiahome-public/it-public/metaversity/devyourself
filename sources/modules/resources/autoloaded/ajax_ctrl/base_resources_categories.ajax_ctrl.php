<?php

abstract class base_resources_categories_ajax_ctrl extends base_ajax_ctrl
{

	protected $autostart_db_transaction = false;
	protected $project_id;

	/**
	 * @var project_obj
	 */
	protected $project_obj;
	protected $resource_name;

	public function init()
	{
		$this->db->begin();
	}

	public function get_data()
	{
		$operation = REQUEST("operation");
		$result = false;

		switch ($operation)
		{
			case "get_nodes":
				$result = $this->get_nodes();
				break;
			case "rename_group":
				$result = $this->rename_group();
				break;
			case "delete_group":
				$result = $this->delete_group();
				break;
			case "move_group":
				$result = $this->move_group();
				break;
			case "create_group":
				$result = $this->create_group();
				break;
		}

		if ($result)
		{
			$this->db->commit();
		}
		else
		{
			$this->db->rollback();
		}

		if (!$result)
		{
			$result = array(
				"status" => "ERROR"
			);
		}

		return $result;
	}

	public function get_nodes()
	{
		$data_raw_plain = $this->db->fetch_all("
			SELECT id, parent_id, title, position
			FROM {$this->resource_name}_category
			WHERE project_id = {$this->project_id}
			ORDER BY position, id
		");
		$data_raw = array();
		foreach ($data_raw_plain as $row)
		{
			$data_raw[$row["id"]] = array(
				"data" => $row["title"],
				"attr" => array(
					"id" => "group_" . $row["id"],
					"rel" => "group"
				)
			);
		}
		foreach ($data_raw_plain as $row)
		{
			if ($row["parent_id"])
			{
				if (!isset($data_raw[$row["parent_id"]]["children"]))
				{
					$data_raw[$row["parent_id"]]["children"] = array();
				}
				$data_raw[$row["parent_id"]]["children"][] = &$data_raw[$row["id"]];
			}
		}
		$data = array();
		foreach ($data_raw_plain as $row)
		{
			if (!$row["parent_id"])
			{
				$data[] = &$data_raw[$row["id"]];
			}
		}
//		${$this->resource_name}_category_ids = array_keys($data_raw);
		return array(
			"data" => "Разделы",
			"state" => "open",
			"attr" => array(
				"id" => "root_" . 0,
				"rel" => "root"
			),
			"children" => $data,
		);
	}

	protected function create_group()
	{
		$parent_id = POST("group_id");
		$title = $this->process_title(POST("title"));
		$title_quoted = $this->db->escape($title);

		if (!is_good_num($parent_id))
		{
			return false;
		}
		if ($parent_id)
		{
			if (!$this->group_exists($parent_id))
			{
				return false;
			}
		}

		$eq = $parent_id ? "=" : "IS";
		if ($parent_id == 0)
		{
			$parent_id = "NULL";
		}

		$position = $this->db->get_value("
			SELECT IF(count(position), max(position)+1, 0)  
			FROM {$this->resource_name}_category
			WHERE parent_id {$eq} {$parent_id} AND project_id = {$this->project_id}
		");
		$this->db->sql("
			INSERT INTO {$this->resource_name}_category (title, project_id, parent_id, add_time, edit_time, position)
			VALUES ('{$title_quoted}', {$this->project_id}, {$parent_id}, NOW(), NOW(), {$position})
		");
		$id = $this->db->get_last_id();

		$this->clean_cache();

		return array(
			"status" => "OK",
			"id" => $id,
			"title" => $title,
		);
	}

	protected function move_group()
	{
		$id = POST("id");
		$parent_id = POST("group_id");
		$position = POST("position");

		if (!is_good_id($id) or !is_good_num($parent_id) or !is_good_num($position))
		{
			return false;
		}
		if (!$this->group_exists($id))
		{
			return false;
		}
		if ($parent_id)
		{
			if (!$this->group_exists($parent_id))
			{
				return false;
			}

			$ancestor_id = $parent_id;
			do
			{
				if ($ancestor_id == $id)
				{
					return array("status" => "ERROR", "error" => "recursive_move");
				}
			}
			while ($ancestor_id = $this->db->get_value("SELECT parent_id FROM {$this->resource_name}_category WHERE id = {$ancestor_id}"));
		}

		$eq = $parent_id ? "=" : "IS";
		if ($parent_id == 0)
		{
			$parent_id = "NULL";
		}

		$this->db->sql("
			UPDATE {$this->resource_name}_category
			SET parent_id = {$parent_id}
			WHERE id = {$id} AND project_id = {$this->project_id}
		");

		$group_positions = $this->db->fetch_all("
			SELECT id, position 
			FROM {$this->resource_name}_category 
			WHERE parent_id $eq {$parent_id} AND id <> {$id} AND project_id = {$this->project_id}
			ORDER BY position, id
		");
		array_splice($group_positions, $position, 0, array(
			array(
				"id" => $id
			)
		));
		foreach ($group_positions as $item_position => $item)
		{
			$this->db->sql("
				UPDATE {$this->resource_name}_category 
				SET position = {$item_position}
				WHERE id = {$item["id"]} 
			");
		}

		$this->clean_cache();

		return array(
			"status" => "OK",
		);
	}

	protected function rename_group()
	{
		$id = POST("id");
		$title = POST("title");

		if (!is_good_id($id))
		{
			return false;
		}
		if (!$this->group_exists($id))
		{
			return false;
		}

		$title = $this->process_title($title);

		$title_quoted = $this->db->escape($title);

		$this->db->sql("
			UPDATE {$this->resource_name}_category
			SET 
				title = '{$title_quoted}',
				edit_time = NOW()
			WHERE id = {$id}
				AND project_id = {$this->project_id}
		");

		$this->clean_cache();

		return array(
			"status" => "OK",
			"title" => $title,
		);
	}

	protected function delete_group($id = 0, $recursive = false)
	{
		if (!$recursive)
		{
			$id = POST("id");
		}
		if (!is_good_id($id))
		{
			return false;
		}
		if (!$this->group_exists($id))
		{
			return false;
		}

		$group_dbres = $this->db->sql("
			SELECT id
			FROM {$this->resource_name}_category
			WHERE parent_id = {$id}
			FOR UPDATE
		");

		while ($group_row = $this->db->get_row($group_dbres))
		{
			$this->delete_group($group_row["id"], true);
		}

		$this->db->sql("
			DELETE FROM {$this->resource_name}_category
			WHERE id = {$id} AND project_id = {$this->project_id}
		");

		$this->clean_cache();

		return array(
			"status" => "OK",
		);
	}

	protected function process_title($title)
	{
		$title = trim(preg_replace("/[\\x00-\\x20]+/", " ", $title));
		if (!$title)
		{
			$title = "Unnamed";
		}
		return $title;
	}

	protected function group_exists($id)
	{
		return $this->db->row_exists("
			SELECT *
			FROM {$this->resource_name}_category
			WHERE id = {$id} AND project_id = {$this->project_id}
		");
	}

}

?>