<?php

require_once PATH_CORE . "/loader/xml_loader.php";

abstract class base_resource_status_ajax_ctrl extends base_ajax_ctrl
{

	protected $resource_name;
	protected $resource_id;

	public function get_data()
	{
		if (!$user_id = $this->user->get_user_id())
		{
			return false;
		}
		$this->resource_id = POST("resource_id");
		$status_type = POST("status_type");
		$status = POST("status");
		if (!is_good_id($this->resource_id))
		{
			return false;
		}

		$resource_status_helper = $this->get_status_helper();

		if (!$resource_status_helper->init_from_db($this->resource_id, $user_id))
		{
			return false;
		}

		if ($status_type == "before_status")
		{
			$resource_status_helper->set_before_status($status);
		}
		if ($status_type == "after_status")
		{
			$resource_status_helper->set_after_status($status);
		}

		return $this->return_html();
	}

	/**
	 * @return base_resource_status_helper
	 */
	abstract protected function get_status_helper();

	abstract protected function return_html();
}

?>
