<?php

abstract class base_resource_ignore_ajax_ctrl extends base_ajax_ctrl
{

	protected $resource_name;
	protected $project_id;

	public function get_data()
	{
		$user_id = $this->user->get_user_id();
		$resource_id = POST("resource_id");
		$ignore = intval(POST("ignore"));

		if (!$user_id)
		{
			return array("status" => "BAD_RIGHTS");
		}

		if (!is_good_id($resource_id))
		{
			return array("status" => "ERROR", "error" => "input");
		}

		if (!in_array($ignore, array(0, 1)))
		{
			return array("status" => "ERROR", "error" => "input");
		}

		$resource_row = $this->db->get_row("
			SELECT id, project_id
			FROM {$this->resource_name}
			WHERE id = $resource_id
		");

		if (!$resource_row)
		{
			return array("status" => "ERROR", "error" => "input");
		}

		$this->db->sql("
			INSERT INTO {$this->resource_name}_user_link ({$this->resource_name}_id, user_id, ignored, ignored_time)
				VALUES ({$resource_id}, {$user_id}, {$ignore}, NOW())
			ON DUPLICATE KEY UPDATE
				ignored = ({$ignore}),
				ignored_time = NOW()
		");

		$this->project_id = $resource_row["project_id"];
		$this->clean_cache();

		return array(
			"status" => "OK",
			"ignored" => $ignore,
		);
	}

}

?>
