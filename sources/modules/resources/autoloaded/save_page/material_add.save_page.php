<?php

class material_add_save_page extends base_resource_add_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_stat_clean_cache",
		"block_set_dt_save",
	);
	protected $resource_name = "material";

	public function check_rights()
	{
		$project_access = $this->project_obj->get_access();
		return $project_access->can_moderate_materials();
	}

	public function clean_cache()
	{
		materials_cache_tag::init($this->project_id)->update();
	}

}

?>