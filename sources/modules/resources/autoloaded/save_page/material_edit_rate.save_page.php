<?php

class material_edit_rate_save_page extends base_resource_edit_rate_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
	);
	protected $resource_name = "material";

	public function check_rights()
	{
		$project_access = $this->project_obj->get_access();
		return $project_access->can_moderate_materials();
	}

	public function clean_cache()
	{
		material_cache_tag::init($this->resource_id)->update();
		materials_cache_tag::init($this->project_id)->update();
	}

}

?>