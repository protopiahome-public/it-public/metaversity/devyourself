<?php

class material_edit_save_page extends base_resource_edit_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_modify_sql"
	);
	protected $resource_name = "material";

	public function __construct()
	{
		parent::__construct();
		if (!$this->reflex_id)
		{
			$this->mixins[] = "block_set_dt_save";
			$this->mixin_init();
		}
	}

	public function check_rights()
	{
		$project_access = $this->project_obj->get_access();
		return $project_access->can_moderate_materials();
	}

	public function clean_cache()
	{
		material_cache_tag::init($this->id)->update();
		materials_cache_tag::init($this->project_id)->update();
		$helper = new material_edit_helper($this->id);
		$helper->update_reflexes();
	}

}

?>