<?php

class material_delete_save_page extends base_resource_delete_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_modify_sql",
		"project_stat_clean_cache"
	);
	protected $resource_name = "material";

	public function check_rights()
	{
		$project_access = $this->project_obj->get_access();
		return $project_access->can_moderate_materials();
	}

	public function on_after_commit()
	{
		parent::on_after_commit();
		if ($this->old_db_row["block_set_id"])
		{
			$this->save_loader->add_ctrl(new block_set_delete_save_ctrl($this->old_db_row["block_set_id"]));
		}
	}

	public function clean_cache()
	{
		material_cache_tag::init($this->id)->update();
		materials_cache_tag::init($this->project_id)->update();
		$helper = new material_edit_helper($this->id);
		$helper->update_reflexes();
	}

}

?>