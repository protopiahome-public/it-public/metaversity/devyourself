<?php

class event_delete_save_page extends base_resource_delete_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
		"project_modify_sql",
		"project_stat_clean_cache"
	);
	protected $resource_name = "event";

	public function check_rights()
	{
		$project_access = $this->project_obj->get_access();
		return $project_access->can_moderate_events();
	}

	public function clean_cache()
	{
		event_cache_tag::init($this->id)->update();
		events_cache_tag::init($this->project_id)->update();
		$helper = new event_edit_helper($this->id);
		$helper->update_reflexes();
	}

}

?>