<?php

class materials_import_select_save_page extends base_resources_import_select_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
	);
	protected $resource_name = "material";

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->project_access->can_moderate_materials();
	}

	public function clean_cache()
	{
		materials_cache_tag::init($this->project_id)->update();
	}

}

?>
