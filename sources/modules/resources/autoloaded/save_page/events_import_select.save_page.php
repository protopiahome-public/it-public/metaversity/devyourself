<?php

class events_import_select_save_page extends base_resources_import_select_save_ctrl
{

	protected $mixins = array(
		"project_before_start",
	);
	protected $resource_name = "event";

	public function check_rights()
	{
		$this->project_access = $this->project_obj->get_access();
		return $this->project_access->can_moderate_events();
	}

	public function clean_cache()
	{
		events_cache_tag::init($this->project_id)->update();
	}

}

?>
