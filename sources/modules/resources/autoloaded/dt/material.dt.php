<?php

class material_dt extends base_dt
{

	protected function init()
	{
		$this->set_db_table("material");

		$this->add_block("main", "Основное");
		$this->add_block("categories", "Разделы");

		$dtf = new string_dtf("title", "Заголовок");
		$dtf->set_max_length(255);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new text_br_dtf("announce", "Анонс (короткий текст для отображения в списке материалов)");
		$dtf->set_editor_height(100);
		$this->add_field($dtf, "main");

		$dtf = new multi_link_dtf("categories", "Разделы", "material_category", "material_category_link", "material_id", "material_category_id", "position");
		$dtf->set_tree_mode();
		$dtf->set_max_height("200");
		$this->add_field($dtf, "categories");

		$dtf = new int_dtf("comment_count_calc", "Количество комментариев");
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);
		
		$this->add_fields_in_axis("edit", array(
			"title", "announce", "categories"
		));

		$this->add_fields_in_axis("edit_reflex", array(
			"title", "announce", "categories"
		));

		$this->add_fields_in_axis("short", array(
			"title", "announce", "categories", "comment_count_calc"
		));

		$this->add_fields_in_axis("show", array(
			"title", "announce", "categories", "comment_count_calc"
		));

		$this->add_fields_in_axis("edit_rate", array(
			"title"
		));
	}

}

?>