<?php

class event_dt extends base_dt
{

	protected function init()
	{
		$this->set_db_table("event");

		$this->add_block("main", "Основное");
		$this->add_block("categories", "Разделы");

		$dtf = new string_dtf("title", "Название");
		$dtf->set_max_length(255);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new datetime_dtf("start_time", "Начало");
		$dtf->set_edit_mode_years_count_for_select(3, 7);
		$dtf->set_format("Y-m-d H:i:s N");
		$this->add_field($dtf, "main");
		
		$dtf = new datetime_dtf("finish_time", "Окончание");
		$dtf->set_edit_mode_years_count_for_select(3, 7);
		$dtf->set_format("Y-m-d H:i:s N");
		$dtf->set_must_be_later("start_time", false, true);
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("place", "Место проведения");
		$dtf->set_max_length(255);
		$this->add_field($dtf, "main");

		$dtf = new text_br_dtf("announce", "Анонс (короткий текст для отображения в списке событий)");
		$dtf->set_editor_height(100);
		$this->add_field($dtf, "main");

		$dtf = new text_html_dtf("descr", "Описание");
		$dtf->set_editor_height(500);
		$this->add_field($dtf, "main");

		$dtf = new multi_link_dtf("categories", "Разделы", "event_category", "event_category_link", "event_id", "event_category_id", "position");
		$dtf->set_tree_mode();
		$dtf->set_max_height("200");
		$this->add_field($dtf, "categories");
	
		$dtf = new int_dtf("comment_count_calc", "Количество комментариев");
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);
		
		$this->add_fields_in_axis("edit", array(
			"title", "start_time", "finish_time", "place", "announce", "descr", "categories"
		));

		$this->add_fields_in_axis("edit_reflex", array(
			"title", "announce", "categories"
		));

		$this->add_fields_in_axis("short", array(
			"title", "start_time", "finish_time", "place", "announce", "categories", "comment_count_calc"
		));

		$this->add_fields_in_axis("show", array(
			"title", "start_time", "finish_time", "place", "announce", "descr", "categories", "comment_count_calc"
		));

		$this->add_fields_in_axis("edit_rate", array(
			"title"
		));
	}

}

?>