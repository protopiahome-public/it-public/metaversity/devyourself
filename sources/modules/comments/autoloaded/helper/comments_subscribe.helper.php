<?php

class comments_subscribe_helper extends base_static_db_helper
{

	public static function subscribe($type, $object_id, $user_id, $type_id = null)
	{
		if (!$type_id)
		{
			$type_id = self::db()->get_value("SELECT id FROM comment_type WHERE name = '{$type}'");
		}

		$type_id = self::db()->sql("
			INSERT IGNORE INTO comment_subscription (type_id, object_id, user_id, add_time)
			VALUES({$type_id}, {$object_id}, {$user_id}, NOW())
		");
	}

	public static function unsubscribe($type, $object_id, $user_id, $type_id = null)
	{
		if (!$type_id)
		{
			$type_id = self::db()->get_value("SELECT id FROM comment_type WHERE name = '{$type}'");
		}

		$type_id = self::db()->sql("
			DELETE FROM comment_subscription
			WHERE type_id = {$type_id}
				AND object_id = {$object_id}
				AND user_id = {$user_id}
		");
	}

	public static function get_subscribers($type_id, $object_id)
	{
		$result = self::db()->fetch_all("
			SELECT s.*, u.email
			FROM comment_subscription s
			LEFT JOIN user u ON u.id = s.user_id
			WHERE type_id = {$type_id} AND object_id = {$object_id}
		", "user_id");
		return $result;
	}

}

?>
