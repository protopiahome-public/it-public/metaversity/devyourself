<?php

require_once PATH_INTCMF . "/select_sql.php";

abstract class base_comments_subscribe_ajax_ctrl extends base_ajax_ctrl
{

	protected $type;
	protected $type_id;
	protected $object_id;
	protected $is_subscribed;

	abstract protected function get_type();

	public function init()
	{
		$this->type = $this->get_type();
		$this->type_id = $this->db->get_value("SELECT id FROM comment_type WHERE name = '{$this->type}'");
		if (!$this->type_id)
		{
			trigger_error("'{$this->type}' is wrong comment type");
		}
	}

	public function start()
	{
		if (!is_good_id($this->object_id = POST("object_id")))
		{
			return false;
		}

		$this->is_subscribed = POST("subscribe");
		if ($this->is_subscribed !== "0" and $this->is_subscribed !== "1")
		{
			return false;
		}

		return $this->check_object();
	}

	protected function check_object()
	{
		$select_sql = new select_sql();

		$select_sql->add_select_fields("{$this->type}.id");
		$select_sql->add_from($this->type);
		$select_sql->add_where("{$this->type}.id = {$this->object_id}");

		$this->mixin_call_method_with_mixins("modify_sql", array($select_sql));

		return $this->db->row_exists($select_sql->get_sql());
	}

	protected function modify_sql(select_sql $select_sql)
	{
		return;
	}

	public function commit()
	{
		if ($this->is_subscribed)
		{
			comments_subscribe_helper::subscribe($this->type, $this->object_id, $this->user->get_user_id(), $this->type_id);
		}
		else
		{
			comments_subscribe_helper::unsubscribe($this->type, $this->object_id, $this->user->get_user_id(), $this->type_id);
		}

		return true;
	}

	public function get_data()
	{
		return array(
			"status" => "OK",
			"is_subscribed" => $this->is_subscribed,
		);
	}

}

?>
