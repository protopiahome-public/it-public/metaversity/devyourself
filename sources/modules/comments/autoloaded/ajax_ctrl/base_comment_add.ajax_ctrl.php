<?php

require_once PATH_CORE . "/loader/xml_loader.php";
require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";
require_once PATH_INTCMF . "/select_sql.php";
require_once PATH_MODULE_SITE_LIB . "/mail/mail_html.php";

abstract class base_comment_add_ajax_ctrl extends base_ajax_ctrl
{

	protected $type;
	protected $type_id;
	protected $project_id;
	protected $object_id;
	protected $before_id;
	protected $after_id;
	protected $new_comment_id;
	protected $parent_id;
	protected $parent_author_user_id;
	protected $object_row;
	protected $comment_html;

	abstract protected function get_type();

	public function init()
	{
		$this->type = $this->get_type();
		$this->type_id = $this->db->get_value("SELECT id FROM comment_type WHERE name = '{$this->type}'");
		if (!$this->type_id)
		{
			trigger_error("'{$this->type}' is wrong comment type");
		}
	}

	public function start()
	{
		if (!is_good_id($this->object_id = POST("object_id")))
		{
			return false;
		}

		if (!is_good_num($this->parent_id = POST("parent_id")))
		{
			return false;
		}

		if ($this->parent_id)
		{
			$parent_exists = $this->db->get_row("
				SELECT id 
				FROM comment
				WHERE 
					id = {$this->parent_id}
					AND type_id = {$this->type_id}
					AND object_id = {$this->object_id}
					AND is_deleted = 0
			");
			if (!$parent_exists)
			{
				return false;
			}
		}

		return $this->check_object();
	}

	protected function check_object()
	{
		$select_sql = new select_sql();

		$select_sql->add_select_fields("{$this->type}.id");
		$select_sql->add_from($this->type);
		$select_sql->add_where("{$this->type}.id = {$this->object_id}");

		$this->mixin_call_method_with_mixins("modify_sql", array($select_sql));

		$this->object_row = $this->db->get_row($select_sql->get_sql());
		return is_array($this->object_row);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		return;
	}

	public function commit()
	{
		if (!trim(POST("html")))
		{
			return true;
		}

		$this->comment_html = POST("html");

		$this->comment_html = text_processor::tidy($this->comment_html);
		$html_quoted = $this->db->escape($this->comment_html);
//		$html_short = text_processor::get_preview($html);
//		$html_short_quoted = $this->db->escape($html_short);

		$level = 0;
		$add_to_the_end = true;
		if ($this->parent_id)
		{
			$add_to_the_end = false;
			$parent_row = $this->db->get_row("
				SELECT * 
				FROM comment
				WHERE 
					id = {$this->parent_id}
					AND type_id = {$this->type_id}
					AND object_id = {$this->object_id}
					AND is_deleted = 0
			");
			$this->parent_author_user_id = $parent_row["author_user_id"];
			$after_row = $this->db->get_row("
				SELECT * 
				FROM comment 
				WHERE object_id = {$this->object_id} AND level <= {$parent_row["level"]} AND `position` > {$parent_row["position"]}
					AND type_id = {$this->type_id}
				ORDER BY `position`
				LIMIT 1
			");
			if (!$after_row)
			{
				$level = $parent_row["level"] + 1;
				$add_to_the_end = true;
			}
			else
			{
				$this->db->sql("
					UPDATE comment 
					SET `position` = `position` + 1
					WHERE object_id = {$this->object_id} AND `position` >= {$after_row["position"]}
						AND type_id = {$this->type_id}
				");
				$level = $parent_row["level"] + 1;
				$position = $after_row["position"];
				$this->before_id = 0;
				$this->after_id = $after_row["id"];
			}
		}
		if ($add_to_the_end)
		{
			$last_row = $this->db->get_row("
				SELECT *
				FROM comment 
				WHERE object_id = {$this->object_id} 
					AND type_id = {$this->type_id}
				ORDER BY `position` DESC 
				LIMIT 1
			");
			if ($last_row)
			{
				$position = (int) $last_row["position"] + 1;
				$this->before_id = $last_row["id"];
				$this->after_id = 0;
			}
			else
			{
				$position = 1;
				$this->before_id = 0;
				$this->after_id = 0;
			}
		}
		if (!$this->parent_id) // $parent_id is 0 (zero)
		{
			$this->parent_id = "NULL";
		}
		$this->db->sql("
			INSERT INTO comment
			SET
				object_id = {$this->object_id},
				type_id = {$this->type_id},
				position = {$position},
				html = '{$html_quoted}',
				parent_id = {$this->parent_id},
				author_user_id = " . $this->user->get_user_id() . ",
				level = {$level},
				add_time = NOW(),
				edit_time = NOW()
		");
		$this->new_comment_id = $this->db->get_last_id();

		return true;
	}

	public function on_after_commit()
	{
		if ($this->new_comment_id)
		{
			$this->send_emails(is_good_id($this->parent_id) ? $this->parent_author_user_id : 0);
			$this->update_object();
		}
	}

	private function send_emails($parent_commenter_user_id)
	{
		global $config;
		if ($config["comments_from"])
		{
			$email_to_parent_commenter = false;

			if ($this->parent_id)
			{
				$parent_commenter_data = $this->db->get_row("
					SELECT email, allow_email_on_reply
					FROM user 
					WHERE id = {$parent_commenter_user_id}
				");
				if ($parent_commenter_data["allow_email_on_reply"] and $parent_commenter_user_id != $this->user->get_user_id())
				{
					$email_to_parent_commenter = true;
				}
			}

			if ($email_to_parent_commenter)
			{
				$xml_loader = new xml_loader(true);
				$xml_loader->add_xml(new user_xml_ctrl());
				$xml_loader->add_xml(new user_short_xml_ctrl($this->user->get_user_id()));
				$xml_loader->add_xml(new comments_xml_ctrl($this->type, $this->object_id, $this->project_id, $this->new_comment_id));
				$this->add_object_xml_ctrls($xml_loader);
				$xml_loader->add_xml(new request_xml_ctrl());
				$this->add_mail_xslt($xml_loader);
				$xml_loader->add_xml(new user_short_xml_ctrl($parent_commenter_user_id));
				$xml_loader->add_xml(new comments_xml_ctrl($this->type, $this->object_id, $this->project_id, $this->parent_id));
				$xml_loader->run();
				$html = $xml_loader->get_content();
				$html_email = new mail_html($parent_commenter_data["email"], $config["comments_from"], $this->get_subject($html), $html);
				$html_email->send();
			}

			$xml_loader = new xml_loader();
			$xml_loader->add_xml(new user_xml_ctrl());
			$xml_loader->add_xml(new user_short_xml_ctrl($this->user->get_user_id()));
			$xml_loader->add_xml(new comments_xml_ctrl($this->type, $this->object_id, $this->project_id, $this->new_comment_id));
			$this->add_object_xml_ctrls($xml_loader);
			$xml_loader->add_xml(new request_xml_ctrl());
			$this->add_mail_xslt($xml_loader);
			$xml_loader->run();
			$html = $xml_loader->get_content();
			$subject = $this->get_subject($html);

			$subscribers = comments_subscribe_helper::get_subscribers($this->type_id, $this->object_id);
			$subscribers_ids = array_keys($subscribers);
			$allowed_subscribers_ids = $this->filter_subscribers($subscribers_ids);
			foreach ($subscribers_ids as $subscriber_id)
			{
				if (!isset($allowed_subscribers_ids[$subscriber_id]))
				{
					unset($subscribers[$subscriber_id]);
				}
			}

			foreach ($subscribers as $subscriber)
			{
				if ($subscriber["user_id"] == $this->user->get_user_id() or $subscriber["user_id"] == $parent_commenter_user_id)
				{
					continue;
				}

				mail_helper::add_letter($subject, $html, $subscriber["email"], $subscriber["user_id"]);
			}
		}
	}

	abstract protected function filter_subscribers($subscribers_ids);

	abstract protected function add_object_xml_ctrls(xml_loader $xml_loader);

	abstract protected function add_mail_xslt(xml_loader $xml_loader);

	private function get_subject($body)
	{
		$dom = new DOMDocument("1.0", "UTF-8");
		$dom->loadXML($body);
		$html_sxml = simplexml_import_dom($dom);
		return $html_sxml->head->title;
	}

	private function get_comment_html($comment_id)
	{
		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new comments_xml_ctrl($this->type, $this->object_id, $this->project_id, $comment_id));
		$xml_loader->add_xml(new user_xml_ctrl());
		$xml_loader->add_xslt("ajax/comment.ajax", "comments");
		$xml_loader->run();
		return $xml_loader->get_content();
	}

	abstract protected function update_object();

	protected function clean_comments_cache()
	{
		comments_cache_tag::init($this->type, $this->object_id)->update();
	}

	public function get_data()
	{
		if ($this->new_comment_id)
		{
			$this->clean_comments_cache();

			return array(
				"status" => "OK",
				"before_id" => $this->before_id,
				"after_id" => $this->after_id,
				"id" => $this->new_comment_id,
				"html" => $this->get_comment_html($this->new_comment_id),
			);
		}
		else
		{
			return array(
				"status" => "OK",
				"empty_html" => "1",
			);
		}
	}

}

?>