<?php

require_once PATH_CORE . "/loader/xml_loader.php";
require_once PATH_INTCMF . "/select_sql.php";
require_once(PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php");

abstract class base_comment_delete_ajax_ctrl extends base_ajax_ctrl
{

	protected $type;
	protected $type_id;
	protected $project_id;
	protected $object_id;
	protected $comment_id;
	protected $comment_data;
	protected $is_deleted;
	protected $old_is_deleted;

	abstract protected function get_type();

	public function init()
	{
		$this->type = $this->get_type();
		$this->type_id = $this->db->get_value("SELECT id FROM comment_type WHERE name = '{$this->type}'");
		if (!$this->type_id)
		{
			trigger_error("'{$this->type}' is wrong comment type");
		}
	}

	public function start()
	{
		if (!is_good_id($this->object_id = POST("object_id")))
		{
			return false;
		}

		if (!is_good_id($this->comment_id = POST("id")))
		{
			return array("error" => "BAD_COMMENT_ID");
		}
		$this->is_deleted = POST("delete");
		if ($this->is_deleted !== "0" and $this->is_deleted !== "1")
		{
			return array("error" => "BAD_POST_PARAM");
		}

		$this->comment_data = $this->db->get_row("
			SELECT * 
			FROM comment
			WHERE 
				id = {$this->comment_id}
				AND type_id = {$this->type_id}
				AND object_id = {$this->object_id}
		");
		if (!$this->comment_data)
		{
			return array("error" => "BAD_COMMENT_ID");
		}

		return $this->check_object();
	}

	protected function check_object()
	{
		$select_sql = new select_sql();

		$select_sql->add_select_fields("{$this->type}.id");
		$select_sql->add_from($this->type);
		$select_sql->add_where("{$this->type}.id = {$this->object_id}");

		$this->mixin_call_method_with_mixins("modify_sql", array($select_sql));

		return $this->db->row_exists($select_sql->get_sql());
	}

	protected function modify_sql(select_sql $select_sql)
	{
		return;
	}

	public function commit()
	{
		$this->old_is_deleted = $this->db->get_value("
			SELECT is_deleted
			FROM comment
			WHERE 
				id = {$this->comment_id}
				AND type_id = {$this->type_id}
				AND object_id = {$this->object_id}
		");
		if ($this->old_is_deleted == $this->is_deleted)
		{
			return true;
		}

		$this->db->sql("
			UPDATE comment
			SET is_deleted = {$this->is_deleted}
			WHERE 
				id = {$this->comment_id}
				AND type_id = {$this->type_id}
				AND object_id = {$this->object_id}
		");

		return true;
	}

	public function on_after_commit()
	{
		if ($this->old_is_deleted == $this->is_deleted)
		{
			return;
		}
		$this->update_object();
	}

	abstract protected function update_object();

	protected function clean_comment_cache()
	{
		comments_cache_tag::init($this->type, $this->object_id)->update();
	}

	private function get_comment_html($comment_id)
	{
		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new comments_xml_ctrl($this->type, $this->object_id, $this->project_id, $comment_id));
		$xml_loader->add_xml(new user_xml_ctrl());
		$xml_loader->add_xslt("ajax/comment.ajax", "comments");
		$xml_loader->run();
		return $xml_loader->get_content();
	}

	public function get_data()
	{
		$this->clean_comment_cache();

		return array(
			"status" => "OK",
			"id" => $this->comment_id,
			"is_deleted" => $this->is_deleted,
			"html" => $this->get_comment_html($this->comment_id),
		);
	}

}

?>
