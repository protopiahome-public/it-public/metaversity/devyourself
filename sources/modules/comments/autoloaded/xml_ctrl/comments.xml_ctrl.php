<?php

class comments_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "author_user_id",
			"ctrl" => "user_short",
			"param2" => "project_id",
		),
	);
	protected $xml_attrs = array("type", "object_id");
	protected $xml_row_name = "comment";
	//Internal
	protected $type_id;
	protected $type;
	protected $object_id;
	protected $project_id;
	protected $comment_id;

	public function __construct($type, $object_id, $project_id, $comment_id = null)
	{
		$this->type = $type;
		$this->object_id = $object_id;
		$this->project_id = $project_id;
		$this->comment_id = $comment_id;

		parent::__construct();
	}

	protected function get_cache()
	{
		if (!$this->comment_id)
		{
			return comments_cache::init($this->type, $this->object_id);
		}
	}

	public function init()
	{
		$this->xml_loader->add_xml(new comments_subscription_xml_ctrl($this->type, $this->object_id));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->type_id = $this->db->get_value("SELECT id FROM comment_type WHERE name = '{$this->type}'");
		$select_sql->add_select_fields("c.id, c.author_user_id, c.level, IF(c.is_deleted = 0, c.html, '') as html, c.add_time, c.is_deleted");
		$select_sql->add_from("comment c");
		$select_sql->add_where("c.type_id = '{$this->type_id}' AND c.object_id = {$this->object_id}");
		$select_sql->add_order("c.position");

		if ($this->comment_id)
		{
			$select_sql->add_where("c.id = {$this->comment_id}");
		}

		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>
