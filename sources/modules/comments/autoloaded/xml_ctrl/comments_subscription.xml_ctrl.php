<?php

class comments_subscription_xml_ctrl extends base_xml_ctrl
{

	//Internal
	protected $type;
	protected $object_id;

	public function __construct($type, $object_id)
	{
		$this->type = $type;
		$this->object_id = $object_id;

		parent::__construct();
	}


	public function get_xml()
	{
		$is_subscribed = $this->db->row_exists("
			SELECT s.user_id 
			FROM comment_subscription s
			LEFT JOIN comment_type t ON t.id = s.type_id
			WHERE s.user_id = " . $this->user->get_user_id() . " AND t.name = '{$this->type}' AND s.object_id = {$this->object_id}
		");
			
			$is_subscribed_attr = intval($is_subscribed);
			
			return <<<EOF
<comment_subscription type="{$this->type}" object_id="{$this->object_id}" is_subscribed="{$is_subscribed_attr}"/>
EOF;
	}

}

?>