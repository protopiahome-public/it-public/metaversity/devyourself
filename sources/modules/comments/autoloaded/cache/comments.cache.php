<?php

class comments_cache extends base_cache
{

	public static function init($type, $object_id)
	{
		$tag_keys = array();
		$tag_keys[] = comments_cache_tag::init($type, $object_id)->get_key();
		return parent::get_cache(__CLASS__, $type, $object_id, $tag_keys);
	}

}

?>