<?php

class comment_calc_db_test extends base_test
{

	public function set_up()
	{
		$this->db->begin();
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user')");
		}
		$this->db->sql("TRUNCATE project");
		$this->db->sql("TRUNCATE project_widget");
		$this->db->sql("TRUNCATE community_widget");
		$this->db->sql("TRUNCATE post_calc");
		$this->db->sql("TRUNCATE post");
		$this->db->sql("TRUNCATE comment");
		$this->db->sql("TRUNCATE project_custom_feed_source");
		$this->db->sql("TRUNCATE community_user_link");
		$this->db->sql("TRUNCATE section");
		$this->db->sql("TRUNCATE event");
		$this->db->sql("TRUNCATE material");
		$this->db->sql("TRUNCATE community");
		$this->db->sql("DELETE FROM project WHERE id = 1");
		$this->db->sql("TRUNCATE user_project_link");
		$this->db->sql("REPLACE INTO project (id, name, title) VALUES (1, 'test', 'test')");
		$this->db->sql("INSERT INTO community (id, name, title, project_id) VALUES (1, 'test', 'test', 1)");
		$this->db->sql("INSERT INTO section (id, name, title, community_id) VALUES (1, 'test', 'test', 1)");
		$this->db->sql("INSERT INTO post (id, author_user_id, title, community_id, section_id) VALUES (1, 1, 'test', 1, 1)");
		$this->db->sql("INSERT INTO comment (id, type_id, author_user_id, object_id, html) VALUES (1, 3, 1, 1, '')");
		$this->db->sql("CALL post_comment_add_count_calc(1, 1, 1, 1)");
		$this->db->sql("INSERT INTO event (id, title, project_id) VALUES (1, 'test', 1)");
		$this->db->sql("INSERT INTO comment (id, type_id, author_user_id, object_id, html) VALUES (2, 1, 1, 1, '')");
		$this->db->sql("CALL event_comment_add_count_calc(1, 1, 1)");
		$this->db->sql("INSERT INTO material (id, title, project_id) VALUES (1, 'test', 1)");
		$this->db->sql("INSERT INTO comment (id, type_id, author_user_id, object_id, html) VALUES (3, 2, 1, 1, '')");
		$this->db->sql("CALL material_comment_add_count_calc(1, 1, 1)");
		$this->db->commit();
	}

	protected function get_community_user_comment_count_calc()
	{
		return $this->db->get_value("
			SELECT comment_count_calc 
			FROM community_user_link 
			WHERE user_id = 1 AND community_id = 1
		");
	}

	protected function get_project_user_comment_count_calc()
	{
		return $this->db->get_value("
			SELECT comment_count_calc 
			FROM user_project_link 
			WHERE user_id = 1 AND project_id = 1
		");
	}

	public function basic_test()
	{
		//check set_up

		$this->db->begin();

		$this->assert_db_value("project", "comment_count_calc", 1, "3");
		$this->assert_db_value("community", "comment_count_calc", 1, "1");
		$this->assert_db_value("post", "comment_count_calc", 1, "1");
		$this->assert_db_value("material", "comment_count_calc", 1, "1");
		$this->assert_db_value("event", "comment_count_calc", 1, "1");
		$this->assert_equal($this->get_community_user_comment_count_calc(), "1");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "3");

		$this->db->rollback();
	}

	public function post_comment_add_test()
	{
		$this->db->begin();

		$this->db->sql("INSERT INTO comment (id, type_id, author_user_id, object_id, html) VALUES (4, 3, 1, 1, '')");
		$this->db->sql("CALL post_comment_add_count_calc(1, 1, 1, 1)");

		$this->assert_db_value("project", "comment_count_calc", 1, "4");
		$this->assert_db_value("community", "comment_count_calc", 1, "2");
		$this->assert_db_value("post", "comment_count_calc", 1, "2");
		$this->assert_equal($this->get_community_user_comment_count_calc(), "2");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "4");
		$this->db->rollback();
	}

	public function post_comment_delete_and_restore_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE comment SET is_deleted = 1 WHERE id = 1");
		$this->db->sql("CALL post_comment_delete_count_calc(1, 1, 1, 1)");

		$this->assert_db_value("project", "comment_count_calc", 1, "2");
		$this->assert_db_value("community", "comment_count_calc", 1, "0");
		$this->assert_db_value("post", "comment_count_calc", 1, "0");
		$this->assert_equal($this->get_community_user_comment_count_calc(), "0");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "2");

		$this->db->sql("CALL post_comment_add_count_calc(1, 1, 1, 1)");
		$this->db->sql("UPDATE comment SET is_deleted = 0 WHERE id = 1");

		$this->assert_db_value("project", "comment_count_calc", 1, "3");
		$this->assert_db_value("community", "comment_count_calc", 1, "1");
		$this->assert_db_value("post", "comment_count_calc", 1, "1");
		$this->assert_equal($this->get_community_user_comment_count_calc(), "1");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "3");

		$this->db->rollback();
	}

	public function post_delete_and_restore_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE post SET is_deleted = 1 WHERE id = 1");

		$this->assert_db_value("project", "comment_count_calc", 1, "2");
		$this->assert_db_value("community", "comment_count_calc", 1, "0");
		$this->assert_db_value("post", "comment_count_calc", 1, "1");
		$this->assert_equal($this->get_community_user_comment_count_calc(), "1");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "3");

		$this->db->sql("UPDATE post SET is_deleted = 0 WHERE id = 1");

		$this->assert_db_value("project", "comment_count_calc", 1, "3");
		$this->assert_db_value("community", "comment_count_calc", 1, "1");
		$this->assert_db_value("post", "comment_count_calc", 1, "1");
		$this->assert_equal($this->get_community_user_comment_count_calc(), "1");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "3");

		$this->db->rollback();
	}

	public function section_delete_test()
	{
		$this->db->begin();

		$this->db->sql("DELETE FROM section WHERE id = 1");

		$this->assert_db_value("project", "comment_count_calc", 1, "3");
		$this->assert_db_value("community", "comment_count_calc", 1, "1");
		$this->assert_db_value("post", "comment_count_calc", 1, "1");
		$this->assert_equal($this->get_community_user_comment_count_calc(), "1");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "3");

		$this->db->rollback();
	}

	public function community_delete_and_restore_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE community SET is_deleted = 1 WHERE id = 1");

		$this->assert_db_value("project", "comment_count_calc", 1, "2");
		$this->assert_db_value("community", "comment_count_calc", 1, "1");
		$this->assert_db_value("post", "comment_count_calc", 1, "1");
		$this->assert_equal($this->get_community_user_comment_count_calc(), "1");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "3");

		$this->db->sql("UPDATE community SET is_deleted = 0 WHERE id = 1");

		$this->assert_db_value("project", "comment_count_calc", 1, "3");
		$this->assert_db_value("community", "comment_count_calc", 1, "1");
		$this->assert_db_value("post", "comment_count_calc", 1, "1");
		$this->assert_equal($this->get_community_user_comment_count_calc(), "1");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "3");

		$this->db->rollback();
	}

	public function event_comment_add_test()
	{
		$this->db->begin();

		$this->db->sql("INSERT INTO comment (id, type_id, author_user_id, object_id, html) VALUES (4, 1, 1, 1, '')");
		$this->db->sql("CALL event_comment_add_count_calc(1, 1, 1)");

		$this->assert_db_value("project", "comment_count_calc", 1, "4");
		$this->assert_db_value("event", "comment_count_calc", 1, "2");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "4");

		$this->db->rollback();
	}

	public function event_comment_delete_and_restore_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE comment SET is_deleted = 1 WHERE id = 2");
		$this->db->sql("CALL event_comment_delete_count_calc(1, 1, 1)");

		$this->assert_db_value("project", "comment_count_calc", 1, "2");
		$this->assert_db_value("event", "comment_count_calc", 1, "0");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "2");

		$this->db->sql("UPDATE comment SET is_deleted = 0 WHERE id = 2");
		$this->db->sql("CALL event_comment_add_count_calc(1, 1, 1)");

		$this->assert_db_value("project", "comment_count_calc", 1, "3");
		$this->assert_db_value("event", "comment_count_calc", 1, "1");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "3");

		$this->db->rollback();
	}

	public function event_delete_test()
	{
		$this->db->begin();

		$this->db->sql("DELETE FROM event WHERE id = 1");

		$this->assert_db_value("project", "comment_count_calc", 1, "2");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "3");

		$this->db->rollback();
	}

	public function material_comment_add_test()
	{
		$this->db->begin();

		$this->db->sql("INSERT INTO comment (id, type_id, author_user_id, object_id, html) VALUES (4, 2, 1, 1, '')");
		$this->db->sql("CALL material_comment_add_count_calc(1, 1, 1)");

		$this->assert_db_value("project", "comment_count_calc", 1, "4");
		$this->assert_db_value("material", "comment_count_calc", 1, "2");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "4");

		$this->db->rollback();
	}

	public function material_comment_delete_and_restore_test()
	{
		$this->db->begin();

		$this->db->sql("UPDATE comment SET is_deleted = 1 WHERE id = 3");
		$this->db->sql("CALL material_comment_delete_count_calc(1, 1, 1)");

		$this->assert_db_value("project", "comment_count_calc", 1, "2");
		$this->assert_db_value("material", "comment_count_calc", 1, "0");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "2");

		$this->db->sql("UPDATE comment SET is_deleted = 0 WHERE id = 3");
		$this->db->sql("CALL material_comment_add_count_calc(1, 1, 1)");

		$this->assert_db_value("project", "comment_count_calc", 1, "3");
		$this->assert_db_value("material", "comment_count_calc", 1, "1");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "3");

		$this->db->rollback();
	}

	public function material_delete_test()
	{
		$this->db->begin();

		$this->db->sql("DELETE FROM material WHERE id = 1");

		$this->assert_db_value("project", "comment_count_calc", 1, "2");
		$this->assert_equal($this->get_project_user_comment_count_calc(), "3");

		$this->db->rollback();
	}

}

?>