<?php

class user_short_xml_ctrl_test extends base_test
{
	
	public function set_up()
	{
		$this->db->sql("REPLACE INTO project (id, name, title, use_game_name) VALUES (1, 'name1', 'title1', 1)");
		$this->db->sql("REPLACE INTO project (id, name, title, use_game_name) VALUES (2, 'name2', 'title2', 0)");
		$this->db->sql("
			INSERT INTO user (id, login, first_name, last_name)
			VALUES (1, 'login1', 'First', 'Last')
			ON DUPLICATE KEY UPDATE login = VALUES(login), first_name = VALUES(first_name), last_name = VALUES(last_name)
		");
		$this->db->sql("
			INSERT INTO user (id, login, first_name, last_name) 
			VALUES (2, 'login2', 'First2', 'Last2')
			ON DUPLICATE KEY UPDATE login = VALUES(login), first_name = VALUES(first_name), last_name = VALUES(last_name)
		");
		$this->db->sql("
			INSERT INTO user (id, login, first_name, last_name) 
			VALUES (99, 'login99', '', '')
			ON DUPLICATE KEY UPDATE login = VALUES(login), first_name = VALUES(first_name), last_name = VALUES(last_name)
		");
		$this->db->sql("REPLACE INTO user_project_link (user_id, project_id, game_name) VALUES (1, 1, 'UP11')");
		$this->db->sql("REPLACE INTO user_project_link (user_id, project_id, game_name) VALUES (2, 1, '')");
		$this->db->sql("REPLACE INTO user_project_link (user_id, project_id, game_name) VALUES (1, 2, 'UP12')");
		$this->db->sql("REPLACE INTO user_project_link (user_id, project_id, game_name) VALUES (2, 2, '')");
	}
	
	public function no_project_test()
	{
		$this->mcache->flush();
		
		$tester = new xml_ctrl_tester(new user_short_xml_ctrl($user_id = 1));
		$tester->run();
		$tester->assert_node_exists("/root/user_short[@id = 1]");
		$this->assert_identical($tester->get_text("/root/user_short/@visible_name"), "First Last");
		$this->assert_identical($tester->get_text("/root/user_short/@any_name"), "First Last");
		$this->assert_identical($tester->get_text("/root/user_short/@full_name"), null);
		$this->assert_identical($tester->get_text("/root/user_short/@project_id"), null);
		$this->assert_identical($tester->get_text("/root/user_short/@game_name"), null);
	}
	
	public function game_name_available_AND_exists_test()
	{
		$this->mcache->flush();
		$project_obj = project_obj::instance(1);
		
		$tester = new xml_ctrl_tester(new user_short_xml_ctrl($user_id = 1, $project_id = 1));
		$tester->run();
		$tester->assert_node_exists("/root/user_short[@id = 1 and @project_id = 1]");
		$this->assert_identical($tester->get_text("/root/user_short/@visible_name"), "UP11");
		$this->assert_identical($tester->get_text("/root/user_short/@any_name"), "UP11");
		$this->assert_identical($tester->get_text("/root/user_short/@full_name"), "First Last");
		$this->assert_identical($tester->get_text("/root/user_short/@game_name"), "UP11");
	}
	
	public function game_name_available_AND_NOT_exists_test()
	{
		$this->mcache->flush();
		$project_obj = project_obj::instance(1);
		
		$tester = new xml_ctrl_tester(new user_short_xml_ctrl($user_id = 2, $project_id = 1));
		$tester->run();
		$tester->assert_node_exists("/root/user_short[@id = 2 and @project_id = 1]");
		$this->assert_identical($tester->get_text("/root/user_short/@visible_name"), "First2 Last2");
		$this->assert_identical($tester->get_text("/root/user_short/@any_name"), "First2 Last2");
		$this->assert_identical($tester->get_text("/root/user_short/@full_name"), "First2 Last2");
		$this->assert_identical($tester->get_text("/root/user_short/@game_name"), "");
	}
	
	public function game_name_NOT_available_AND_NOT_exists_test()
	{
		$this->mcache->flush();
		$project_obj = project_obj::instance(2);
		
		$tester = new xml_ctrl_tester(new user_short_xml_ctrl($user_id = 2, $project_id = 2));
		$tester->run();
		$tester->assert_node_exists("/root/user_short[@id = 2 and @project_id = 2]");
		$this->assert_identical($tester->get_text("/root/user_short/@visible_name"), "First2 Last2");
		$this->assert_identical($tester->get_text("/root/user_short/@any_name"), "First2 Last2");
		$this->assert_identical($tester->get_text("/root/user_short/@full_name"), "First2 Last2");
		$this->assert_identical($tester->get_text("/root/user_short/@game_name"), null);
	}
	
	public function game_name_NOT_available_AND_exists_test()
	{
		$this->mcache->flush();
		$project_obj = project_obj::instance(2);
		
		$tester = new xml_ctrl_tester(new user_short_xml_ctrl($user_id = 1, $project_id = 2));
		$tester->run();
		$tester->assert_node_exists("/root/user_short[@id = 1 and @project_id = 2]");
		$this->assert_identical($tester->get_text("/root/user_short/@visible_name"), "First Last");
		$this->assert_identical($tester->get_text("/root/user_short/@any_name"), "First Last");
		$this->assert_identical($tester->get_text("/root/user_short/@full_name"), "First Last");
		$this->assert_identical($tester->get_text("/root/user_short/@game_name"), null);
	}
	
	public function game_name_available_AND_zero_name_test()
	{
		$this->mcache->flush();
		$project_obj = project_obj::instance(1);
		
		$tester = new xml_ctrl_tester(new user_short_xml_ctrl($user_id = 99, $project_id = 1));
		$tester->run();
		$tester->assert_node_exists("/root/user_short[@id = 99 and @project_id = 1]");
		$this->assert_identical($tester->get_text("/root/user_short/@visible_name"), "<без имени>");
		$this->assert_identical($tester->get_text("/root/user_short/@any_name"), "login99");
		$this->assert_identical($tester->get_text("/root/user_short/@full_name"), "<без имени>");
		$this->assert_identical($tester->get_text("/root/user_short/@game_name"), "");
	}
	
	public function cache_test()
	{
		$this->mcache->flush();
		$project_obj = project_obj::instance(1);
		
		$query_count = $this->db->debug_get_query_count();
		$tester = new xml_ctrl_tester(new user_short_xml_ctrl($user_id = 1, $project_id = 1));
		$tester->run();
		$this->assert_true($this->db->debug_get_query_count() > $query_count);
		
		$query_count = $this->db->debug_get_query_count();
		$tester = new xml_ctrl_tester(new user_short_xml_ctrl($user_id = 1, $project_id = 1));
		$tester->run();
		$this->assert_true($this->db->debug_get_query_count() == $query_count);
	}
	
}

?>