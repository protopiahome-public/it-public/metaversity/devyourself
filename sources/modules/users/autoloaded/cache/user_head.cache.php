<?php

class user_head_cache extends base_cache
{

	public static function init($user_id, $nearest_event_id = null)
	{
		$tag_keys = array();
		$tag_keys[] = user_events_cache_tag::init($user_id)->get_key();
		$tag_keys[] = event_cache_tag::init($nearest_event_id)->get_key();
		return parent::get_cache(__CLASS__, $user_id, $tag_keys);
	}

}

?>