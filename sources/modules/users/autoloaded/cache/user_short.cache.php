<?php

class user_short_cache extends base_cache
{

	public static function init($user_id, $project_id = null, $use_game_name = false)
	{
		$project_id = $project_id ? $project_id : "0";
		$use_game_name = $use_game_name ? "1" : "0";
		$tag_keys = array();
		$tag_keys[] = user_cache_tag::init($user_id)->get_key();
		if ($project_id)
		{
			$tag_keys[] = project_user_cache_tag::init($project_id, $user_id)->get_key();
		}
		return parent::get_cache(__CLASS__, $user_id, $project_id, $use_game_name, $tag_keys);
	}

}

?>