<?php

class user_cache extends base_cache
{

	public static function init($user_id)
	{
		$tag_keys = array();
		$tag_keys[] = user_cache_tag::init($user_id)->get_key();
		return parent::get_cache(__CLASS__, $user_id, $tag_keys);
	}

}

?>