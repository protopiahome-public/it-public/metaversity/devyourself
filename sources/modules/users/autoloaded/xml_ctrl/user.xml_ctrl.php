<?php

class user_xml_ctrl extends base_xml_ctrl
{

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		if ($this->user->get_user_id())
		{
			$attrs = array(
				"id" => $this->user->get_user_id(),
				"login" => $this->user->get_login(),
				"email" => $this->user->get_email(),
				"visible_name" => $this->user->get_visible_name(),
				"no_name" => $this->user->is_no_name(),
				"sex" => $this->user->get_sex(),
				"profile_url" => $this->user->get_profile_url(),
				"is_admin" => $this->user->is_admin(),
				"photo_small_width" => $this->user->get_user_param("photo_small_width"),
				"photo_small_height" => $this->user->get_user_param("photo_small_height"),
				"photo_small_url" => $this->user->get_user_param("photo_small_width") == 0 ? "" : $this->request->get_prefix() . "/data/user/small/" . $this->user->get_user_id() . ".jpeg?v" . $this->user->get_user_param("photo_version"),
			);
			return $this->get_node_string("user", $attrs);
		}
		else
		{
			return "<user/>";
		}
	}

}

?>