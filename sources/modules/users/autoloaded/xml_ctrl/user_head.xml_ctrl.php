<?php

class user_head_xml_ctrl extends base_xml_ctrl
{

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		$user_id = $this->user->get_user_id();
//		$cache = user_head_cache::init($user_id);
//		$data = $cache->get();
//		if (!$data or $data["nearest_event_id"] > 0 and $data["nearest_event_time"] < time())
//		{
//			$data = array();
//			$nearest_event_dbrow = $this->db->get_row("
//				SELECT id, start_time
//				FROM event e
//				LEFT JOIN event_user_link l ON (l.event_id = e.id)
//				WHERE e.start_time >= NOW() AND l.before_status > 0 AND l.user_id = {$user_id}
//			");
//
//			if (!$nearest_event_dbrow)
//			{
//				$nearest_event_dbrow = array("id" => 0, "start_time" => 0);
//			}

		$awaiting_event_count = $this->db->get_value("
				SELECT COUNT(e.id)
				FROM event e
				LEFT JOIN event_user_link l ON (l.event_id = e.id)
				WHERE e.start_time < NOW() AND l.before_status > 0 AND l.after_status = 0 AND l.user_id = {$user_id}
			");

		$data = array(
//				"nearest_event_id" => $nearest_event_dbrow["id"],
//				"nearest_event_time" => $nearest_event_dbrow["start_time"] ? $this->db->parse_datetime($nearest_event_dbrow["start_time"]) : 0,
			"awaiting_event_count" => $awaiting_event_count
		);

//			$cache = user_head_cache::init($user_id, $data["nearest_event_id"]);
//			$cache->set($data);
//		}


		return <<<EOF
<user_head awaiting_event_count="{$data["awaiting_event_count"]}"/>
EOF;
	}

}

?>
