<?php

class user_competence_sets_xml_ctrl extends base_simple_xml_ctrl
{

	protected $user_id;
	protected $competence_set_id_array;
	protected $data_one_row = false;
	protected $row_name = "competence_set";
	protected $allow_error_404_if_no_data = false;
	protected $cache = false;
	protected $cache_key_property_name = "user_id";

	public function __construct($user_id, $competence_set_id_array)
	{
		$this->user_id = $user_id;
		$this->competence_set_id_array = $competence_set_id_array;

		parent::__construct();
	}

	protected function set_result()
	{
		if (!sizeof($this->competence_set_id_array))
		{
			$this->result = array();
			return;
		}
		$this->result = $this->db->fetch_all("
			SELECT 
				cs.id, cs.title,
				cs.professiogram_count_calc,
				IFNULL(l.personal_mark_count_calc, 0) as personal_mark_count_calc,
				IFNULL(l.group_mark_count_calc, 0) as group_mark_count_calc,
				IFNULL(l.total_mark_count_calc, 0) as total_mark_count_calc,
				IFNULL(l.project_count_calc, 0) as project_count_calc,
				IFNULL(l.personal_mark_count_with_trans_calc, 0) as personal_mark_count_with_trans_calc,
				IFNULL(l.group_mark_count_with_trans_calc, 0) as group_mark_count_with_trans_calc,
				IFNULL(l.total_mark_count_with_trans_calc, 0) as total_mark_count_with_trans_calc,
				IFNULL(l.project_count_with_trans_calc, 0) as project_count_with_trans_calc
			FROM competence_set cs
			LEFT JOIN user_competence_set_link_calc l ON l.competence_set_id = cs.id AND user_id = {$this->user_id}
			WHERE cs.id IN (" . join(",", $this->competence_set_id_array) . ")
		");
		if (sizeof($this->result) != sizeof($this->competence_set_id_array))
		{
			$this->set_error_404();
		}
	}

}

?>