<?php

abstract class base_user_resources_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $resource_name;
	protected $calendar_on = true;
	// Internal
	protected $user_id;
	protected $is_archive = false;
	protected $is_calendar = false;
	protected $year;
	protected $month;
	protected $day;
	protected $page;

	public function __construct($user_id, $page, $p1 = null, $p2 = null, $p3 = null, $p4 = null)
	{
		$this->xml_row_name = $this->resource_name;
		$this->dependencies_settings = array(
			array(
				"column" => "project_id",
				"ctrl" => "project_short"
			),
		);

		if ($this->calendar_on)
		{
			$this->xml_attrs = array("is_archive", "is_calendar", "year",
				"month", "day",
			);
		}

		$this->user_id = $user_id;

		$this->page = $page;

		$this->is_archive = $p1 === "archive";
		$this->is_calendar = $p1 === "calendar";

		if ($this->is_calendar)
		{
			$this->year = $p2;
			if ($p3)
			{
				$this->month = $p3;
				if ($p4)
				{
					$this->day = $p4;
				}
			}
		}

		parent::__construct();
	}

	public function init()
	{
		if ($this->calendar_on)
		{
			$calendar_class = "user_{$this->resource_name}s_calendar_xml_ctrl";
			if ($this->is_calendar && $this->month)
			{
				$this->xml_loader->add_xml(new $calendar_class($this->user_id, $this->year, $this->month));
			}
			elseif ($this->is_calendar && !$this->month)
			{
				$this->xml_loader->add_xml(new $calendar_class($this->user_id, $this->year, 1));
			}
			else
			{
				$this->xml_loader->add_xml(new $calendar_class($this->user_id));
			}
		}

		$this->add_easy_processor(new pager_db_easy_processor($this->page, 25));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		if (!$this->check())
		{
			$this->set_error_404();
			return;
		}

		$this->filter($select_sql);
		$this->resource_filter($select_sql);
		$this->sort($select_sql);
		$this->load_resources_data($select_sql);
	}

	private function check()
	{
		if ($this->is_calendar)
		{
			$date_valid = true;

			if ($this->year < 1970 || $this->year > 2037)
			{
				$date_valid = false;
			}
			if ($this->month > 12)
			{
				$date_valid = false;
			}
			if ($this->day && !checkdate($this->month, $this->day, $this->year))
			{
				$date_valid = false;
			}
			if (!$date_valid)
			{
				return false;
			}
		}

		return true;
	}

	private function filter(select_sql $select_sql)
	{
		if ($this->calendar_on)
		{
			if ($this->is_archive)
			{
				$select_sql->add_where("start_time < NOW()");
			}
			elseif ($this->is_calendar)
			{
				if (!$this->month)
				{
					$select_sql->add_where("YEAR(start_time) = {$this->year}");
				}
				elseif (!$this->day)
				{
					$select_sql->add_where("
						YEAR(start_time) = {$this->year} 
						AND MONTH(start_time) = {$this->month}
					");
				}
				else
				{
					$select_sql->add_where("
						YEAR(start_time) = {$this->year} 
						AND MONTH(start_time) = {$this->month}
						AND DAYOFMONTH(start_time) = {$this->day}
					");
				}
			}
			else
			{
				$select_sql->add_where("start_time >= NOW()");
			}
		}
	}

	abstract protected function resource_filter(select_sql $select_sql);

	private function sort(select_sql $select_sql)
	{
		if ($this->calendar_on)
		{
			if ($this->is_archive)
			{
				$select_sql->add_order("start_time DESC");
			}
			else
			{
				$select_sql->add_order("start_time ASC");
			}
		}
		else
		{
			$select_sql->add_order("last_status_time DESC");
		}
	}

	private function load_resources_data(select_sql $select_sql)
	{
		// DB query config
		$select_sql->add_from("{$this->resource_name} r");
		$select_sql->add_join("
			LEFT JOIN {$this->resource_name}_user_link l 
				ON (l.{$this->resource_name}_id = r.id)
		");
		$select_sql->add_select_fields("r.id, r.project_id, l.*");

		//Start time
		if ($this->calendar_on)
		{
			$select_sql->add_select_fields("UNIX_TIMESTAMP(start_time) AS start_time");
		}

		//Where
		$select_sql->add_where("l.user_id = {$this->user_id}");

		//Fetch data
		$this->data = $this->db->fetch_all($select_sql->get_sql());

		foreach ($this->data as $row)
		{
			$class = "{$this->resource_name}_short_xml_ctrl";
			$this->xml_loader->add_xml_by_class_name($class, $row["id"], $row["project_id"]);
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		if (count($this->data))
		{
			if ($this->user_id == $this->user->get_user_id())
			{
				$resource_status_helper = $this->get_resource_status_helper();
			}

			foreach ($this->data as $row)
			{
				$row_node = $xdom->create_child_node($this->resource_name);
				foreach ($row as $name => $attr)
				{
					$row_node->set_attr($name, $attr);
				}

				if ($this->user_id == $this->user->get_user_id())
				{
					$this->init_resource_status_helper($resource_status_helper, $row);
					$resource_status_helper->modify_xml($row_node);
				}
			}
		}
	}

	/**
	 * @return base_resource_status_helper
	 */
	abstract protected function get_resource_status_helper();

	abstract protected function init_resource_status_helper(base_resource_status_helper $resource_status_helper, $row);
}

?>