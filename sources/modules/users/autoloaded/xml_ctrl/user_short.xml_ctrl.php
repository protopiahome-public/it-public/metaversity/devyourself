<?php

class user_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "user";
	protected $axis_name = "short";
	// Internal
	protected $project_id = null;
	protected $use_game_name = false;

	/**
	 * @var project_obj
	 */
	protected $project_obj;

	public function __construct($id, $project_id = null)
	{
		$this->project_id = $project_id;

		if ($project_id)
		{
			$this->project_obj = project_obj::instance($this->project_id);
			$this->use_game_name = $this->project_obj->use_game_name();
		}

		parent::__construct($id);
	}

	protected function get_cache()
	{
		return user_short_cache::init($this->id, $this->project_id, $this->use_game_name);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("first_name, last_name");
		if ($this->project_id)
		{
			$select_sql->add_join("LEFT JOIN user_project_link l ON l.user_id = dt.id AND project_id = {$this->project_id}");
			$select_sql->add_select_fields("l.status, l.number");
			if ($this->use_game_name)
			{
				$select_sql->add_select_fields("l.game_name");
			}
		}
	}

	protected function process_data()
	{
		$data = &$this->data[0];
		$full_name = trim($data["first_name"] . " " . $data["last_name"]);
		if ($full_name)
		{
			$any_name = $full_name;
		}
		else
		{
			$any_name = $data["login"];
			$full_name = "<без имени>";
		}
		$game_name = "";
		if ($this->project_id)
		{
			$game_name = $this->project_obj->use_game_name() ? $data["game_name"] : "";
			$data["full_name"] = $full_name;
		}
		if ($game_name)
		{
			$data["visible_name"] = $data["game_name"];
			$data["any_name"] = $data["game_name"];
		}
		else
		{
			$data["visible_name"] = $full_name;
			$data["any_name"] = $any_name;
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		parent::fill_xml($xdom);
		$data = &$this->data[0];
		$xdom->set_attr("visible_name", $data["visible_name"]);
		$xdom->set_attr("any_name", $data["any_name"]);
		if ($this->project_id)
		{
			$xdom->set_attr("project_id", $this->project_id);
			$xdom->set_attr("full_name", $data["full_name"]);
			$xdom->set_attr("status", $data["status"]);
			$xdom->set_attr("number", $data["number"]);
			if ($this->use_game_name)
			{
				$xdom->set_attr("game_name", $data["game_name"]);
			}
		}
	}

}

?>