<?php

final class users_taxonomy extends base_taxonomy
{

	public function run()
	{
		global $config;

		$p = $this->get_parts_relative();

		if ($page = $this->is_page_folder($p[1]) and $p[2] === null)
		{
			//users/[page-<page>/]
			$this->xml_loader->add_xml_with_xslt(new users_xml_page($page));
		}
		if ($p[1] !== false and $user_id = $this->get_user_id_by_login($p[1]))
		{
			//users/<login>/...
			$this->xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $user_id);
			if ($p[2] === null)
			{
				//users/<login>/
				$this->xml_loader->add_xml_with_xslt(new user_profile_xml_page($user_id));
				$this->xml_loader->add_xml(new user_projects_xml_page(1, $user_id, true, 4));
			}
			elseif ($p[2] === "projects" and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				//users/<login>/projects/[page-<page>/]
				$this->xml_loader->add_xml_with_xslt(new user_projects_xml_page($page, $user_id));
			}
			elseif ($p[2] === "results" and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				//users/<login>/results/[page-<page>/]
				$this->xml_loader->add_xml_with_xslt(new user_results_xml_page($page, $user_id));
				$this->xml_loader->add_xml(new competence_set_fast_navigation_xml_ctrl());
			}
			elseif ($p[2] === "results" and $p[3] === "profs" and $page = $this->is_page_folder($p[4]) and $p[5] === null)
			{
				//users/<login>/results/profs/[page-<page>/]
				$this->xml_loader->add_xml_with_xslt(new user_results_professiograms_xml_page($page, $user_id));
				$this->xml_loader->add_xml(new competence_set_fast_navigation_xml_ctrl());
			}
			elseif ($p[2] === "results" and $competence_set_id = $this->is_set_folder($p[3]) and $page = $this->is_page_folder($p[4]) and $p[5] === null)
			{
				//users/<login>/results/set-<competence_set_id>/[page-<page>/]
				$this->xml_loader->add_xml_with_xslt(new user_results_professiograms_xml_page($page, $user_id, $competence_set_id));
				$this->xml_loader->add_xml(new competence_set_fast_navigation_xml_ctrl());
			}
			elseif ($p[2] === "results" and $professiogram_id = $this->is_prof_folder($p[3]) and $p[4] === null)
			{
				//users/<login>/results/prof-<professiogram_id>/
				$this->xml_loader->add_xml(new user_results_professiogram_match_xml_page($user_id, $professiogram_id, "rose"));
				$this->xml_loader->add_xslt("user_results_professiogram_match_rose", "users");
				$this->xml_loader->set_error_404_xslt("error_404", "_site");
				$this->xml_loader->add_xml(new competence_set_fast_navigation_xml_ctrl());
			}
			elseif ($p[2] === "results" and $professiogram_id = $this->is_prof_folder($p[3]) and $p[4] === "detailed" and $p[5] === null)
			{
				//users/<login>/results/prof-<professiogram_id>/detailed/
				$this->xml_loader->add_xml(new user_results_professiogram_match_xml_page($user_id, $professiogram_id, "detailed"));
				$this->xml_loader->add_xslt("user_results_professiogram_match_detailed", "users");
				$this->xml_loader->set_error_404_xslt("error_404", "_site");
				$this->xml_loader->add_xml(new competence_set_fast_navigation_xml_ctrl());
			}
			elseif ($p[2] === "results" and $competence_set_id = $this->is_set_folder($p[3]) and $p[4] === "full" and $p[5] === null)
			{
				//users/<login>/results/set-<competence_set_id>/full/
				$this->xml_loader->add_xml_with_xslt(new user_results_competence_set_full_xml_page($user_id, $competence_set_id));
			}
			elseif ($p[2] === "results" and $competence_id = $this->is_competence_folder($p[3]) and $p[4] === null)
			{
				//users/<login>/results/competence-<competence_id>/
				$this->xml_loader->add_xml_with_xslt(new user_results_competence_marks_xml_page($user_id, $competence_id));
			}
			elseif ($p[2] === "events")
			{
				$is_event_list = false;
				if ($page = $this->is_page_folder($p[3]) and $p[4] === null)
				{
					//users/<login>/events/[page-<page>/]
					$is_event_list = true;
					$p[3] = null;
				}
				elseif ($p[3] === "archive" and $page = $this->is_page_folder($p[4]) and $p[5] === null)
				{
					//users/<login>/events/archive/[page-<page>/]
					$is_event_list = true;
					$p[4] = null;
				}
				elseif ($p[3] === "calendar" and is_good_id($p[4]) and $page = $this->is_page_folder($p[5]) and $p[6] === null)
				{
					//users/<login>/events/calendar/<year>/[page-<page>/]
					$is_event_list = true;
					$p[5] = null;
				}
				elseif ($p[3] === "calendar" and is_good_id($p[4]) and is_good_id($p[5]) and $page = $this->is_page_folder($p[6]) and $p[7] === null)
				{
					//users/<login>/events/calendar/<year>/<month>/[page-<page>/]
					$is_event_list = true;
					$p[6] = null;
				}
				elseif ($p[3] === "calendar" and is_good_id($p[4]) and is_good_id($p[5]) and is_good_id($p[6]) and $page = $this->is_page_folder($p[7]) and $p[8] === null)
				{
					//users/<login>/events/calendar/<year>/<month>/<day>/[page-<page>/]
					$is_event_list = true;
					$p[7] = null;
				}

				if ($is_event_list)
				{
					$this->xml_loader->add_xml_with_xslt(new user_events_xml_page($user_id, $page, $p[3], $p[4], $p[5], $p[6]));
				}
			}
			elseif ($p[2] === "materials")
			{
				if ($page = $this->is_page_folder($p[3]) and $p[4] === null)
				{
					//p/<project_name>/materials/[page-<page>/]
					$this->xml_loader->add_xml_with_xslt(new user_materials_xml_page($user_id, $page));
				}
				elseif (in_array($p[3], array("read", "done")) and $page = $this->is_page_folder($p[4]) and $p[5] === null)
				{
					//p/<project_name>/materials/read|done/[page-<page>/]
					$this->xml_loader->add_xml_with_xslt(new user_materials_xml_page($user_id, $page, $p[3]));
				}
			}
		}
	}

	private function is_set_folder($folder_param)
	{
		return $this->is_type_folder($folder_param, "set", $is_good_id = true);
	}

	private function is_prof_folder($folder_param)
	{
		return $this->is_type_folder($folder_param, "prof", $is_good_id = true);
	}

	private function is_competence_folder($folder_param)
	{
		return $this->is_type_folder($folder_param, "competence", $is_good_id = true);
	}

	private function get_user_id_by_login($login)
	{
		return url_helper::get_user_id_by_login($login);
	}

}

?>