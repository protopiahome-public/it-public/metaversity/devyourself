<?php

class user_dt extends base_dt
{

	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	public function __construct()
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", "Основное");
		$this->add_block("contacts", "Контакты");
		$this->add_block("other", "Прочее");
		$this->add_block("notifications", "Оповещения");
		$this->add_block("password", "Пароль");

		$dtf = new url_name_dtf("login", "Логин");
		$dtf->set_max_length(40);
		$dtf->set_url_attribute("url");
		$dtf->set_url_prefix($this->domain_logic->get_main_prefix() . "/users/");
		$dtf->set_deprecated_values(array(
			"www", "mail", "ns", "ns1", "ns2", "ns3", "ns4", // std. subdomains
			"admin", "god", "moderator", "editor", "user", "all", // roles
			"cp", "add", "create", "edit", "delete", "remove", // actions
		));
		$dtf->set_comment("Разрешены английские буквы, цифры и дефис");
		$this->add_field($dtf, "main");

		$dtf = new password_dtf("password", "Пароль");
		$dtf->set_max_length(64);
		$dtf->set_min_length(5);
		$this->add_field($dtf, "password");

		$dtf = new string_dtf("last_name", "Фамилия");
		$dtf->set_max_length(60);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("first_name", "Имя");
		$dtf->set_max_length(60);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("mid_name", "Отчество");
		$dtf->set_max_length(60);
		$this->add_field($dtf, "main");

		$dtf = new select_dtf("sex", "Пол");
		$dtf->set_cases(array(
			"?" => "Не выбрано",
			"m" => "Мужской",
			"f" => "Женский",
		));
		$dtf->set_default_key("?");
		$this->add_field($dtf, "main");

		$dtf = new image_pack_dtf("photo", "Фото", PATH_TMP);
		$dtf->add_image("large", "Large", 800, 600, "/data/user/large/", "/img/defaults/user-big-default.jpg", PATH_PUBLIC_DATA . "/user/large");
		$dtf->add_image("big", "Big", 80, 80, "/data/user/big/", "/img/defaults/user-big-default.jpg", PATH_PUBLIC_DATA . "/user/big");
		$dtf->add_image("mid", "Mid", 48, 48, "/data/user/mid/", "/img/defaults/user-mid-default.jpg", PATH_PUBLIC_DATA . "/user/mid");
		$dtf->add_image("small", "Small", 24, 24, "/data/user/small/", "/img/defaults/user-small-default.jpg", PATH_PUBLIC_DATA . "/user/small");
		$dtf->set_add_prefix_to_url($this->domain_logic->get_main_prefix());
		$dtf->set_comment("Старайтесь загружать реальное фото, а не аватар");
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("email", "Email");
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$this->add_field($dtf, "contacts");

		$dtf = new url_string_dtf("www", "Домашняя страница");
		$dtf->set_human_view_column();
		$dtf->set_comment("Должен начинаться с http:// или https://");
		$this->add_field($dtf, "contacts");

		$dtf = new string_dtf("phone", "Телефон");
		$dtf->set_max_length(40);
		$this->add_field($dtf, "contacts");

		$dtf = new string_dtf("skype", "Skype");
		$dtf->set_max_length(100);
		$this->add_field($dtf, "contacts");

		$dtf = new string_dtf("icq", "ICQ");
		$dtf->set_max_length(12);
		$this->add_field($dtf, "contacts");

		$dtf = new text_br_dtf("about", "О себе");
		$this->add_field($dtf, "other");

		$dtf = new int_dtf("personal_mark_count_calc", "Количество личных оценок");
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$dtf = new int_dtf("group_mark_count_calc", "Количество групповых оценок");
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$dtf = new int_dtf("total_mark_count_calc", "Суммарное количество оценок");
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$dtf = new url_string_dtf("profile_url", "URL-адрес профиля");
		$this->add_field($dtf);

		$dtf = new string_dtf("register_ip", "Register IP");
		$dtf->set_max_length(15);
		$this->add_field($dtf);

		$dtf = new bool_dtf("allow_email_on_reply", "Отправлять на email ответы на ваши комментарии");
		$dtf->set_default_value(1);
		$this->add_field($dtf, "notifications");

		$dtf = new bool_dtf("allow_email_on_premoderation", "Отправлять на email запросы о вступлении в ваши проекты и сообщества");
		$dtf->set_default_value(1);
		$dtf->set_comment("Письма будут приходить, если вы администратор и включена премодерация");
		$this->add_field($dtf, "notifications");

		$this->add_fields_in_axis("short", array(
			"login",
			"sex",
			"profile_url",
			"photo",
			//"personal_mark_count_calc", "group_mark_count_calc", "total_mark_count_calc",
		));

		$this->add_fields_in_axis("register", array(
			"login",
			"password",
			"email",
			"last_name", "first_name", "mid_name",
			"sex",
			"photo",
		));

		$this->add_fields_in_axis("settings", array(
			"last_name", "first_name", "mid_name",
			"sex", "photo",
			"email", "www", "phone", "skype", "icq",
			"about",
			"password",
			"allow_email_on_reply", "allow_email_on_premoderation"
		));
	}

}

?>