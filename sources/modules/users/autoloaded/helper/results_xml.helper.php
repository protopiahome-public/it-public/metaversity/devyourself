<?php

class results_xml_helper
{

	public static function export_results(xdom $xdom, filter_main_math $filter_main, results_math $results_math, $results, $user_id, $output_type, $show_self_by_default = false, $choose_delta_position = false, $calculations_base = null)
	{
		// Comparing of the projects
		$projects = $filter_main->get_selected_projects();
		$results_node = $xdom->create_child_node("project_comparison");
		$results_node->set_attr("show_self_by_default", $show_self_by_default);
		$results_node->set_attr("choose_delta_position", $choose_delta_position);
		if ($choose_delta_position)
		{
			$results_node->set_attr("calculations_base", $calculations_base);
		}
		if (sizeof($projects))
		{
			foreach ($projects as $project_name => $data)
			{
				$results_node->create_child_node("project")
						->set_attr("id", $data["project_id"])
						->set_attr("key", $project_name)
						->set_attr("competence_set_id", $data["competence_set_id"]);
			}
		}

		// Results
		$results_node = $xdom->create_child_node("results");
		$result_cache = array();
		if (isset($results[$user_id]))
		{
			foreach ($results[$user_id] as $name => &$data)
			{
				foreach ($data as $competence_id => $mark)
				{
					if (!isset($result_cache[$competence_id]))
					{
						$result_cache[$competence_id] = $results_node->create_child_node("competence")
										->set_attr("id", $competence_id);
					}
					if ($name == "sum")
					{
						$result_cache[$competence_id]
								->set_attr("mark", $mark);
					}
					elseif ($output_type === "detailed")
					{
						$result_cache[$competence_id]->create_child_node("project")
								->set_attr("key", $name)
								->set_attr("mark", $mark);
					}
				}
			}
		}

		// Detailed results (statistics)
		if ($output_type === "detailed")
		{
			$detailed_node = $xdom->create_child_node("detailed");
			foreach ($results_math->get_stat() as $competence_id => $competence_stat)
			{
				$competence_node = $detailed_node->create_child_node("competence")
								->set_attr("id", $competence_id);
				foreach ($competence_stat as $idx => $val)
				{
					$competence_node->set_attr($idx, $val);
				}
			}
		}
	}

}

?>