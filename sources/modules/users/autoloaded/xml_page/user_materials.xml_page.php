<?php

class user_materials_xml_page extends base_user_resources_xml_ctrl
{

	protected $resource_name = "material";
	protected $calendar_on = false;
	protected $filter;

	public function __construct($user_id, $page, $filter = "must_read")
	{
		$this->filter = $filter;
		parent::__construct($user_id, $page);
		$this->xml_attrs[] = "filter";
	}

	protected function resource_filter(select_sql $select_sql)
	{
		if ($this->filter == "must_read")
		{
			$select_sql->add_where("l.before_status = 2 AND l.after_status != 1");
		}
		elseif ($this->filter == "read")
		{
			$select_sql->add_where("l.before_status = 1 AND l.after_status != 1");
		}
		elseif ($this->filter == "done")
		{
			$select_sql->add_where("l.after_status = 1");
		}
	}

	protected function get_resource_status_helper()
	{
		return new material_status_helper();
	}

	protected function init_resource_status_helper(base_resource_status_helper $resource_status_helper, $row)
	{
		$resource_status_helper->init_from_input($row["project_id"], $row["id"], $this->user->get_user_id(), $row["before_status"], $row["after_status"]);
	}

}

?>