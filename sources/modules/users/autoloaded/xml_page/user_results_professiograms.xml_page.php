<?php

class user_results_professiograms_xml_page extends base_simple_xml_ctrl
{
	
	protected $competence_set_id = 0;
	protected $user_id = 0;
	protected $profession_search = false;
	protected $data_one_row = false;
	protected $row_name = "professiogram";
	protected $allow_error_404_if_no_data = false;
	protected $cache = false;
	protected $cache_key_property_name = "competence_set_id";
	protected $documents_per_page_for_set = 20;
	protected $documents_per_page = 10;
	protected $dependencies_ctrl = array ();
	protected $filter = array (
			"search" => "" 
	);
	protected $sort_default = "match";
	protected $competence_set_id_array_as_keys = array ();
	
	public function __construct($current_page, $user_id, $competence_set_id = 0)
	{
		$this->current_page = $current_page;
		$this->user_id = $user_id;
		$this->competence_set_id = $competence_set_id;
		
		parent::__construct();
	}
	
	protected function before_load()
	{
		if ($this->current_page != 1)
		{
			$this->allow_error_404_if_no_data = true;
		}
		if ($this->competence_set_id)
		{
			$this->documents_per_page = 0;
		}
	}
	
	protected function set_result()
	{
		// Where
		$where = array ();
		$where[] = "p.enabled = 1";
		if ($this->competence_set_id)
		{
			$where[] = "p.competence_set_id = {$this->competence_set_id}";
		}
		$this->filter["search"] = mb_substr(trim($this->filter["search"]), 0, 255);
		if ($this->filter["search"] !== "")
		{
			$search_quoted = $this->db->escape($this->filter["search"]);
			$where[] = "(p.title LIKE '%{$search_quoted}%')";
		}
		if (!$this->competence_set_id)
		{
			$this->sort = "title";
		}
		$where = sizeof($where) ? "WHERE " . join(" AND ", $where) : "";
		
		// Join
		$join = "";
		if (!$this->competence_set_id)
		{
			$join = "LEFT JOIN user_competence_set_link_calc l ON l.competence_set_id = p.competence_set_id AND l.user_id = {$this->user_id}";
		}
		
		// Order
		if ($this->competence_set_id)
		{
			$order = "ORDER BY p.title";
		}
		else
		{
			$order = "ORDER BY l.total_mark_count_with_trans_calc DESC, p.title";
		}
		
		// Query
		$this->result = $this->db->fetch_all("
			SELECT SQL_CALC_FOUND_ROWS
				p.id, p.title, p.competence_set_id
			FROM professiogram p
			{$join}
			{$where}
			{$order}
			{$this->get_limit_statement()}
		");
		
		// If we don't do this here, the number will we overwritten later
		$this->row_count = $this->db->get_value("SELECT FOUND_ROWS()");
		
		// Extraction of competence sets' IDs
		foreach ($this->result as $index => $data)
		{
			if (!isset($this->competence_set_id_array_as_keys[$data["competence_set_id"]]))
			{
				$this->competence_set_id_array_as_keys[$data["competence_set_id"]] = array ();
			}
			$this->competence_set_id_array_as_keys[$data["competence_set_id"]][(int) $data["id"]] = $index;
		}
		
		// Zero output
		if ($this->competence_set_id and !sizeof($this->result))
		{
			$this->competence_set_id_array_as_keys[$this->competence_set_id] = array ();
		}
		
		// Competence sets for this user
		$this->xml_loader->add_xml(new user_competence_sets_xml_ctrl($this->user_id, array_keys($this->competence_set_id_array_as_keys)));
		
		/* Match calculation */
		foreach ($this->competence_set_id_array_as_keys as $competence_set_id => $professiogram_array_as_keys)
		{
			// Professiogram data
			$professiogram_data_math = new professiogram_data_math();
			//$professiogram_data = $professiogram_data_math->get_professiograms_data_by_competence_set($this->competence_set_id, 0, true);
			$professiogram_data = $professiogram_data_math->get_professiograms_data(array_keys($professiogram_array_as_keys));
			
			// Results
			$results_math = new results_math($competence_set_id, array (
					$this->user_id 
			), array (), $this->competence_set_id == 0);
			$results = $results_math->get_results();
			
			// Filters output
			$filter_main = $results_math->get_filter_main();
			$filter_raters_real = $results_math->get_filter_raters_real();
			$this->xml_loader->add_xml(new filter_xml_ctrl($filter_main, $filter_raters_real, $results_math->get_used_mark_count(), $results_math->get_used_rater_count()));
			
			// Match calculation
			$professiogram_match_math = new rate_match_math($results, $professiogram_data);
			$rating = $professiogram_match_math->get_rate_rating_for_user($this->user_id);
			
			// Match output
			if ($this->competence_set_id) // only one competence_set
			{
				// paging
				$this->documents_per_page = $this->documents_per_page_for_set;
				$this->row_count = sizeof($rating);
				$from = ($this->current_page - 1) * $this->documents_per_page + 1;
				$to = $this->current_page * $this->documents_per_page;
				reset($rating);
				$i = 1;
				$this->result = array ();
				while (($v = current($rating)) !== false)
				{
					if ($i >= $from and $i <= $to)
					{
						$professiogram_id = key($rating);
						$this->result[$professiogram_id] = array (
								"id" => $professiogram_id, 
								"match" => $v, 
								"competence_set_id" => $this->competence_set_id 
						);
					}
					$i++;
					next($rating);
				}
				foreach ($professiogram_data as $data)
				{
					if (isset($this->result[(int) $data["id"]]))
					{
						$this->result[(int) $data["id"]]["title"] = $data["title"];
					}
				}
			}
			else
			{
				foreach ($professiogram_array_as_keys as $professiogram_id => $index)
				{
					$this->result[$index]["match"] = isset($rating[$professiogram_id]) ? $rating[$professiogram_id] : 0;
				}
			}
		}
	}
	
	protected function modify_xml(xdom $xdom)
	{
		$competence_sets_node = $xdom->create_child_node("competence_sets");
		foreach ($this->competence_set_id_array_as_keys as $competence_set_id => $professiogram_id_array)
		{
			$competence_sets_node->create_child_node("competence_set")->set_attr("id", $competence_set_id);
		}
	}

}

?>