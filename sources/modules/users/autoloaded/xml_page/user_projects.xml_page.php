<?php

class user_projects_xml_page extends base_simple_xml_ctrl
{
	
	protected $user_id;
	protected $data_one_row = false;
	protected $row_name = "project";
	protected $allow_error_404_if_no_data = false;
	protected $cache = false;
	protected $documents_per_page = 25;
	protected $dependencies_ctrl = array (
			"project_short" => "id" 
	);
	protected $filter = array ();
	protected $sort_fields = array (
			"date", 
			"site", 
			"marks", 
			"date_add" 
	);
	protected $sort_default = "date_add";
	private $project_id_array = array ();
	
	public function __construct($current_page, $user_id, $disable_sorting = false, $documents_per_page = null)
	{
		$this->current_page = $current_page;
		$this->user_id = $user_id;
		
		if ($disable_sorting)
		{
			$this->sort_fields = array ();
		}
		
		if ($documents_per_page)
		{
			$this->documents_per_page = $documents_per_page;
		}
		
		parent::__construct();
	}
	
	protected function before_load()
	{
		if ($this->current_page != 1)
		{
			$this->allow_error_404_if_no_data = true;
		}
	}
	
	protected function set_result()
	{
		// Sorting
		$order = "";
		switch ($this->sort)
		{
			case "date":
				$order = "ORDER BY p.start_time " . (!$this->sort_back ? "DESC" : "ASC");
				break;
			case "site":
				$order = "ORDER BY p.main_url_human_calc " . (!$this->sort_back ? "ASC" : "DESC");
				break;
			case "marks":
				$order = "ORDER BY l.total_mark_count_calc " . (!$this->sort_back ? "DESC" : "ASC");
				break;
			case "date_add":
				$order = "ORDER BY l.add_time " . (!$this->sort_back ? "DESC" : "ASC");
				break;
		}
		
		// Query
		$this->result = $this->db->fetch_all("
			SELECT SQL_CALC_FOUND_ROWS
				l.project_id as id, l.add_time,
				l.personal_mark_count_calc, l.group_mark_count_calc, l.total_mark_count_calc,
				p.vector_is_on, p.vector_competence_set_id
			FROM user_project_link l
			LEFT JOIN project p ON p.id = l.project_id
			WHERE
				l.user_id = {$this->user_id}
				AND l.status <> 'pretender' AND l.status <> 'deleted'
			{$order}
			{$this->get_limit_statement()}
		");
		
		// Extraction of IDs
		foreach ($this->result as $data)
		{
			$this->project_id_array[] = $data["id"];
		}
	}
	
	protected function append_xml()
	{
		/**
		 * Competence sets
		 */
		$xdom = xdom::create("user_competence_set_project_links");
		if (sizeof($this->project_id_array))
		{
			$db_result = $this->db->sql("
				SELECT
					l.project_id, l.competence_set_id, cs.title as competence_set_title,
					l.personal_mark_count_calc, l.group_mark_count_calc, l.total_mark_count_calc
				FROM user_competence_set_project_link l
				LEFT JOIN competence_set cs ON cs.id = l.competence_set_id
				WHERE
					l.user_id = {$this->user_id} 
					AND l.project_id IN (" . join(",", $this->project_id_array) . ")					
				ORDER BY l.total_mark_count_calc DESC
			");
			$data = array ();
			while ($row = $this->db->fetch_array($db_result))
			{
				if (!isset($data[$row["project_id"]]))
				{
					$data[$row["project_id"]] = array ();
				}
				if (!isset($data[$row["project_id"]][$row["competence_set_id"]]))
				{
					$data[$row["project_id"]][$row["competence_set_id"]] = $row;
				}
			}
			foreach ($data as $project_id => $project_data)
			{
				$project_node = $xdom->create_child_node("project")->set_attr("id", $project_id);
				foreach ($project_data as $competence_set_id => $competence_set_data)
				{
					$project_node->create_child_node("competence_set")->set_attr("id", $competence_set_data["competence_set_id"])->set_attr("title", $competence_set_data["competence_set_title"])->set_attr("personal_mark_count_calc", $competence_set_data["personal_mark_count_calc"])->set_attr("group_mark_count_calc", $competence_set_data["group_mark_count_calc"])->set_attr("total_mark_count_calc", $competence_set_data["total_mark_count_calc"]);
				}
			}
		}
		
		/**
		 * Vectors
		 */
		global $config;
		$xdom_vectors = xdom::create("vectors");
		$project_id_array = array ();
		foreach ($this->result as $data)
		{
			if ($data["vector_is_on"])
			{
				$project_id_array[(int) $data["id"]] = $data["vector_competence_set_id"] ? (int) $data["vector_competence_set_id"] : $config["default_vector_competence_set_id"];
			}
		}
		$vectors = vector_info_math::get_project_vectors_for_user($this->user_id, $project_id_array);
		foreach ($vectors as $vector_data)
		{
			$xdom_vectors->create_child_node("vector")->set_attr("id", $vector_data["id"])->set_attr("user_id", $vector_data["user_id"])->set_attr("project_id", $vector_data["project_id"])->set_attr("competence_set_id", $vector_data["competence_set_id"]);
		}
		
		/**
		 * Output
		 */
		return $xdom->get_xml(true) . $xdom_vectors->get_xml(true);
	}

}

?>