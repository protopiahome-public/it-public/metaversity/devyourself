<?php

class user_events_xml_page extends base_user_resources_xml_ctrl
{

	protected $resource_name = "event";

	protected function resource_filter(select_sql $select_sql)
	{
		$select_sql->add_where("l.before_status > 0 OR l.after_status = 1");
	}

	protected function get_resource_status_helper()
	{
		return new event_status_helper();
	}

	protected function init_resource_status_helper(base_resource_status_helper $resource_status_helper, $row)
	{
		$resource_status_helper->init_from_input($row["project_id"], $row["id"], $this->user->get_user_id(), $row["before_status"], $row["after_status"], $row["start_time"]);
	}

}

?>