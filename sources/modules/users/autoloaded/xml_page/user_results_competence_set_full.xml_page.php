<?php

class user_results_competence_set_full_xml_page extends base_xml_ctrl
{
	
	private $user_id;
	private $competence_set_id;
	
	public function __construct($user_id, $competence_set_id)
	{
		$this->user_id = $user_id;
		$this->competence_set_id = $competence_set_id;
		
		parent::__construct();
	}
	
	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		/* Math */
		
		$competence_set_id = $this->competence_set_id;
		$this->xml_loader->add_xml(new competence_set_short_xml_ctrl($competence_set_id));
		
		// Results
		$results_math = new results_math($competence_set_id, array ($this->user_id), array (), false, true);
		$results = $results_math->get_results();
		
		// Filters
		$filter_main = $results_math->get_filter_main();
		$filter_raters_real = $results_math->get_filter_raters_real();
		
		// Vectors' data
		$vectors_data = new vectors_data_math($competence_set_id, $this->user_id);
		
		/* Additional controls */
		
		$this->xml_loader->add_xml(new filter_xml_ctrl($filter_main, $filter_raters_real, $results_math->get_used_mark_count(), $results_math->get_used_rater_count()));
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($competence_set_id));
		$this->xml_loader->add_xml(new vectors_data_xml_ctrl($vectors_data, true, true, true));
		
		/* Output */
		
		// Rasic
		$xdom = xdom::create("user_results_competence_set_full");
		$xdom->set_attr("competence_set_id", $competence_set_id);
		$xdom->set_attr("output_type", "detailed");
		
		results_xml_helper::export_results($xdom, $filter_main, $results_math, $results, $this->user_id, "detailed");
		
		return $xdom->get_xml(true);
	}

}

?>