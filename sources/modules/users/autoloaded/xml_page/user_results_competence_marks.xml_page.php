<?php

class user_results_competence_marks_xml_page extends base_xml_ctrl
{
	
	protected $user_id;
	protected $competence_id;
	
	public function __construct($user_id, $competence_id)
	{
		$this->user_id = $user_id;
		$this->competence_id = $competence_id;
		
		parent::__construct();
	}
	
	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		// Does the competence exist?
		if (!$row = $this->db->get_row("SELECT * FROM competence_full WHERE id = {$this->competence_id}"))
		{
			$this->set_error_404();
			return "";
		}
		$competence_set_id = $row["competence_set_id"];
		
		// Main mathematics + marks export
		$default_project_id = isset($_GET["default_project_id"]) && is_good_id($_GET["default_project_id"]) ? $_GET["default_project_id"] : null;
		$results_math = new results_math($competence_set_id, array (
				$this->user_id 
		), array (
				$this->competence_id 
		), false, false, true, $default_project_id);
		$results = $results_math->get_results();
		
		// XDOM + basic attributes
		$xdom = xdom::create("user_results_competence_marks");
		$xdom->set_attr("user_id", $this->user_id);
		$xdom->set_attr("competence_id", $this->competence_id);
		$xdom->set_attr("competence_set_id", $competence_set_id);
		$xdom->set_attr("mark", isset($results[$this->user_id]["sum"][$this->competence_id]) ? $results[$this->user_id]["sum"][$this->competence_id] : 0);
		
		// In the end we will load these entries
		$projects_to_load = array ();
		$competence_sets_to_load = array (
				$competence_set_id 
		);
		$users_to_load = array (
				$this->user_id 
		);
		$translators_to_load = array ();
		$competences_to_load = array (
				$this->competence_id 
		);
		$precedents_to_load = array ();
		
		// XML export
		$projects_node = $xdom->create_child_node("projects");
		//dd($results, $results_math->get_marks_log());
		foreach ($results_math->get_marks_log() as $project_key => $project_data)
		{
			$projects_to_load[] = $project_data["project_id"];
			$project_node = $projects_node->create_child_node("project")->set_attr("id", $project_data["project_id"]);
			$project_node->set_attr("project_key", $project_key);
			$project_node->set_attr("mark", isset($results[$this->user_id][$project_key][$this->competence_id]) ? $results[$this->user_id][$project_key][$this->competence_id] : 0);
			if (isset($project_data["translator_id"]))
			{
				$project_node->set_attr("translator_id", $project_data["translator_id"]);
				$project_node->set_attr("src_competence_set_id", $project_data["src_competence_set_id"]);
				
				$translators_to_load[] = $project_data["translator_id"];
				$competence_sets_to_load[] = $project_data["src_competence_set_id"];
				$src_competences_node = $project_node->create_child_node("src_competences");
			}
			if (isset($project_data["competences"][$this->competence_id]))
			{
				$competence_data = $project_data["competences"][$this->competence_id];
				if (isset($project_data["translator_id"]))
				{
					$project_node->set_attr("translation_type", $competence_data["translation_type"]);
					foreach ($competence_data["src_competences"] as $src_competence_id => $src_competence_data)
					{
						$src_competences_node->create_child_node("competence")->set_attr("id", $src_competence_id)->set_attr("mark", $src_competence_data["mark"])->set_attr("weight", $src_competence_data["weight"]);
						$competences_to_load[] = $src_competence_id;
					}
				}
				$marks_node = $project_node->create_child_node("marks");
				foreach ($competence_data["marks"] as $mark_data)
				{
					$marks_node->create_child_node("mark")->set_attr("competence_id", $mark_data["competence_id"])->set_attr("precedent_id", $mark_data["precedent_id"])->set_attr("rater_user_id", $mark_data["rater_user_id"])->set_attr("type", $mark_data["type"])->set_attr("value", $mark_data["value"])->set_attr("comment", $mark_data["comment"]);
					$precedents_to_load[] = $mark_data["precedent_id"];
					$users_to_load[] = $mark_data["rater_user_id"];
				}
			}
		}
		
		// Loading other necessary modules
		foreach ($projects_to_load as $project_id_to_load)
		{
			$this->xml_loader->add_xml_by_class_name("project_short_xml_ctrl", $project_id_to_load);
		}
		foreach ($competence_sets_to_load as $competence_set_id_to_load)
		{
			$this->xml_loader->add_xml_by_class_name("competence_set_short_xml_ctrl", $competence_set_id_to_load);
		}
		foreach ($users_to_load as $user_id_to_load)
		{
			$this->xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $user_id_to_load);
		}
		foreach ($translators_to_load as $translator_id_to_load)
		{
			$this->xml_loader->add_xml_by_class_name("translator_short_xml_ctrl", $translator_id_to_load);
		}
		foreach ($competences_to_load as $competence_id_to_load)
		{
			$this->xml_loader->add_xml_by_class_name("competence_short_xml_ctrl", $competence_id_to_load);
		}
		$this->xml_loader->add_xml(new precedent_ordered_list_xml_ctrl($precedents_to_load));
		
		return $xdom->get_xml(true);
	}

}

?>