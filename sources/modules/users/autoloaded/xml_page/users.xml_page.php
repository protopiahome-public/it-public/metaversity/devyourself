<?php

class users_xml_page extends base_easy_xml_ctrl
{

	protected $xml_row_name = "user";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "user_short"
		),
	);

	public function __construct($page)
	{
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

		$processor = new sort_easy_processor();
		$processor->add_order("mark-count", "u.total_mark_count_calc DESC", "u.total_mark_count_calc ASC");
		$processor->add_order("name", "u.first_name ASC, u.last_name ASC", "u.first_name DESC, u.last_name DESC");
		$processor->add_order("reg-time", "u.add_time DESC", "u.add_time ASC");
		$this->add_easy_processor($processor);

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new text_sql_filter("search", "имя/логин", array("u.login", "u.first_name", "u.last_name")));
		$processor->add_sql_filter(new multi_link_sql_filter("project", "проект", "u.id", "user_project_link", "user_id", "project_id", "project"));
		$processor->add_sql_filter(new multi_link_sql_filter("group", "группа", "u.id", "user_user_group_link", "user_id", "user_group_id", "user_group"));
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("user u");
		$select_sql->add_select_fields("u.id");
		$select_sql->add_select_fields("u.personal_mark_count_calc, u.group_mark_count_calc, u.total_mark_count_calc");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	protected function append_xml()
	{
		$xdom = xdom::create("user_project_links");

		$user_id_array = $this->get_selected_ids();

		if (sizeof($user_id_array))
		{
			$db_result = $this->db->sql("
				SELECT
					l.user_id, l.project_id, p.title as project_title, p.name as url_name,
					l.personal_mark_count_calc, l.group_mark_count_calc, l.total_mark_count_calc
				FROM user_project_link l
				LEFT JOIN project p ON p.id = l.project_id
				WHERE
					l.user_id IN (" . join(",", $user_id_array) . ")
					AND l.status <> 'pretender' AND l.status <> 'deleted'
				ORDER BY l.total_mark_count_calc DESC
			");
			$data = array();
			while ($row = $this->db->fetch_array($db_result))
			{
				if (!isset($data[$row["user_id"]]))
				{
					$data[$row["user_id"]] = array();
				}
				if (!isset($data[$row["user_id"]][$row["project_id"]]))
				{
					$data[$row["user_id"]][$row["project_id"]] = $row;
				}
			}
			foreach ($data as $user_id => $user_data)
			{
				$project_node = $xdom->create_child_node("user")->set_attr("id", $user_id);
				foreach ($user_data as $project_id => $project_data)
				{
					$project_node->create_child_node("project")->set_attr("id", $project_data["project_id"])->set_attr("url_name", $project_data["url_name"])->set_attr("title", $project_data["project_title"])->set_attr("personal_mark_count_calc", $project_data["personal_mark_count_calc"])->set_attr("group_mark_count_calc", $project_data["group_mark_count_calc"])->set_attr("total_mark_count_calc", $project_data["total_mark_count_calc"]);
				}
			}
		}
		return $xdom->get_xml(true);
	}

}

?>