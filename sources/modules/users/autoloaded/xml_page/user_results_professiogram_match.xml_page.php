<?php

class user_results_professiogram_match_xml_page extends base_xml_ctrl
{
	
	private $user_id;
	private $professiogram_id;
	private $output_type;
	
	public function __construct($user_id, $professiogram_id, $output_type)
	{
		$this->user_id = $user_id;
		$this->professiogram_id = $professiogram_id;
		$this->output_type = $output_type;
		
		parent::__construct();
	}
	
	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		/* Math */
		
		// Professiogram data
		$professiogram_data_math = new professiogram_data_math();
		$professiogram_data = $professiogram_data_math->get_professiograms_data(array (
				$this->professiogram_id 
		));
		if (sizeof($professiogram_data) != 1)
		{
			$this->set_error_404();
			return false;
		}
		$competence_set_id = $professiogram_data[0]["competence_set_id"];
		$this->xml_loader->add_xml(new competence_set_short_xml_ctrl($competence_set_id));
		ksort($professiogram_data[0]["competences"], SORT_NUMERIC);
		$competence_id_array = array_keys($professiogram_data[0]["competences"]);
		
		// Results
		$results_math = new results_math($competence_set_id, array (
				$this->user_id 
		), array_keys($professiogram_data[0]["competences"]), false, $this->output_type === "detailed");
		$results = $results_math->get_results();
		
		// Filters
		$filter_main = $results_math->get_filter_main();
		$filter_raters_real = $results_math->get_filter_raters_real();
		
		// Match calculation
		$professiogram_match_math = new rate_match_math($results, $professiogram_data);
		$match = $professiogram_match_math->get_user_rate_match($this->user_id, 0);
		
		// Vectors' data
		$vectors_data = new vectors_data_math($competence_set_id, $this->user_id, null, $competence_id_array);
		
		/* Rose output */
		
		if ($this->output_type == "rose" and isset($_GET["img"]))
		{
			if (!sizeof($professiogram_data[0]["competences"]))
			{
				$this->set_error_404();
				return false;
			}
			
			rose_xml_helper::rose_output($competence_id_array, array_values($professiogram_data[0]["competences"]), $results, $vectors_data->get_vectors_self_data(), array_keys($vectors_data->get_vectors_focus_data()), $this->user_id);
			return true;
		}
		
		/* Additional controls */
		
		$this->xml_loader->add_xml(new filter_xml_ctrl($filter_main, $filter_raters_real, $results_math->get_used_mark_count(), $results_math->get_used_rater_count()));
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($competence_set_id));
		$this->xml_loader->add_xml(new vectors_data_xml_ctrl($vectors_data, false, true, true));
		
		/* Output */
		
		// Basic
		$xdom = xdom::create("user_results_professiogram_match");
		$xdom->set_attr("professiogram_id", $professiogram_data[0]["id"]);
		$xdom->set_attr("professiogram_title", $professiogram_data[0]["title"]);
		$xdom->set_attr("competence_set_id", $professiogram_data[0]["competence_set_id"]);
		$xdom->set_attr("match", $match);
		$xdom->set_attr("output_type", $this->output_type);
		
		// Professiogram marks
		$results_node = $xdom->create_child_node("professiogram");
		foreach ($professiogram_data[0]["competences"] as $competence_id => $mark)
		{
			$competence_node = $results_node->create_child_node("competence")->set_attr("id", $competence_id)->set_attr("mark", $mark);
		}
		
		// Additional info for the rose
		if ($this->output_type == "rose")
		{
			rose_xml_helper::rose_xml_output($xdom, $competence_id_array);
		}
		
		results_xml_helper::export_results($xdom, $filter_main, $results_math, $results, $this->user_id, $this->output_type);
		
		return $xdom->get_xml(true);
	}

}

?>