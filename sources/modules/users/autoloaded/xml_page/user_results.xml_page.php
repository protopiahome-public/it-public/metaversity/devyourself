<?php

class user_results_xml_page extends base_simple_xml_ctrl
{
	
	protected $user_id = 0;
	protected $data_one_row = false;
	protected $row_name = "competence_set";
	protected $allow_error_404_if_no_data = false;
	protected $cache = false;
	protected $documents_per_page = 25;
	protected $dependencies_ctrl = array ();
	protected $filter = array (
			"search" => "" 
	);
	protected $sort_fields = array (
			"title", 
			"marks_trans", 
			"marks", 
			"profs" 
	);
	protected $sort_default = "marks_trans";
	protected $competence_set_id_array = array ();
	
	public function __construct($current_page, $user_id)
	{
		$this->current_page = $current_page;
		$this->user_id = $user_id;
		
		parent::__construct();
	}
	
	protected function before_load()
	{
		if ($this->current_page != 1)
		{
			$this->allow_error_404_if_no_data = true;
		}
	}
	
	protected function set_result()
	{
		// Input filtering
		$where = array ();
		$joins = array ();
		$this->filter["search"] = mb_substr(trim($this->filter["search"]), 0, 255);
		if ($this->filter["search"] !== "")
		{
			$search_quoted = $this->db->escape($this->filter["search"]);
			$where[] = "(cs.title LIKE '%{$search_quoted}%')";
		}
		$where = sizeof($where) ? "WHERE " . join(" AND ", $where) : "";
		
		// Sorting
		$order = "";
		switch ($this->sort)
		{
			case "title":
				$order = "ORDER BY cs.title " . (!$this->sort_back ? "ASC" : "DESC");
				break;
			case "profs":
				$order = "ORDER BY cs.professiogram_count_calc " . (!$this->sort_back ? "DESC" : "ASC");
				break;
			case "marks":
				$order = "ORDER BY l.total_mark_count_calc " . (!$this->sort_back ? "DESC" : "ASC");
				break;
			case "marks_trans":
				$order = "ORDER BY l.total_mark_count_with_trans_calc " . (!$this->sort_back ? "DESC" : "ASC");
				break;
		}
		if ($this->sort == "marks" or $this->sort == "marks_trans")
		{
			$joins[] = "LEFT JOIN user_competence_set_link_calc l ON l.competence_set_id = cs.id AND user_id = {$this->user_id}";
		}
		
		// Joins
		$joins = join("\n", $joins);
		
		// Query
		$this->result = $this->db->fetch_all("
			SELECT SQL_CALC_FOUND_ROWS
				cs.id
			FROM competence_set cs
			{$joins}
			{$where}
			{$order}
			{$this->get_limit_statement()}
		");
		
		// Extraction of IDs
		foreach ($this->result as $data)
		{
			$this->competence_set_id_array[] = $data["id"];
		}
	}
	
	protected function after_load()
	{
		$this->xml_loader->add_xml(new user_competence_sets_xml_ctrl($this->user_id, $this->competence_set_id_array));
		return true;
	}

}

?>