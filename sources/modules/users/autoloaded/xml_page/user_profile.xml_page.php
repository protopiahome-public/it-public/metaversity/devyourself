<?php

class user_profile_xml_page extends base_simple_xml_ctrl
{
	
	protected $user_id;
	protected $data_one_row = true;
	protected $row_name = "user";
	protected $allow_error_404_if_no_data = true;
	protected $cache = false;
	protected $cache_key_property_name = "user_id";
	protected $dependencies_ctrl = array (
			"university_short" => "university_id" 
	);
	
	public function __construct($user_id)
	{
		$this->user_id = $user_id;
		parent::__construct();
	}
	
	protected function set_result()
	{
		$this->result = $this->db->fetch_all("
			SELECT
				u.id, u.email, u.skype, u.icq, u.www, u.www_human_calc, u.phone, u.university_id
			FROM user u
			WHERE u.id = {$this->user_id}
		");
	}
	
	protected function modify_xml(xdom $xdom)
	{
		$contacts_node = $xdom->create_child_node("contacts");
		if (isset($this->result[0]))
		{
			$data = $this->result[0];
			$contact_types = array (
					"email", 
					"phone", 
					"skype", 
					"www", 
					"icq" 
			);
			foreach ($contact_types as $contact_type)
			{
				if ($data[$contact_type])
				{
					$contact_node = $contacts_node->create_child_node("contact", $data[$contact_type]);
					$contact_node->set_attr("type", $contact_type);
					if ($contact_type == "www")
					{
						$contact_node->set_attr("url_human", $data["www_human_calc"]);
					}
					if ($contact_type == "email")
					{
						$contact_node->set_attr("obscured", str_replace(array (
								"@", 
								"." 
						), array (
								" sobaka ", 
								" tochka " 
						), $data["email"]));
					}
				}
			}
		}
	}
	
	protected function set_auxil_data()
	{
		$this->auxil_data = array (
				"photo_big_stub_url" => $this->request->get_prefix() . "/img/defaults/user-big-default.jpg", 
				"photo_big_stub_width" => 80, 
				"photo_big_stub_height" => 80, 
				"photo_small_stub_url" => $this->request->get_prefix() . "/img/defaults/user-small-default.jpg", 
				"photo_small_stub_width" => 24, 
				"photo_small_stub_height" => 24 
		);
	}

}

?>