<?php

require_once PATH_SOURCES . "/init_www.php";

//=== user ===//
require_once PATH_CORE . "/user.php";
$user = new user();

require_once PATH_CORE . "/db_xml/db_xml_type_helper.php";
require_once PATH_CORE . "/db_xml/db_xml_converter.php";
require_once PATH_CORE . "/base_http_issues.php";
require_once PATH_CORE . "/loader/base_loader.php";

//=== loading controls ===//
$db_xml_converter = new db_xml_converter();
$loader = null;

$check_cross_site_request = false;

if ($request->get_folders(1) == "save")
{
	require_once PATH_CORE . "/loader/save_loader.php";
	$save_loader = $loader = new save_loader();
	$check_cross_site_request = true;
}
elseif ($request->get_folders(1) == "ajax")
{
	require_once PATH_CORE . "/loader/ajax_loader.php";
	$ajax_loader = $loader = new ajax_loader();
	$check_cross_site_request = true;
}
else
{
	require_once PATH_CORE . "/loader/xml_loader.php";
	/**
	 * @var xml_loader
	 */
	$xml_loader = $loader = new xml_loader();
}

if ($check_cross_site_request and $request->is_cross_site_request($allow_empty_referrer = true))
{
	response::set_content('
		<p><strong>Error:</strong> cross-site requests are deprecated.</p>
		<p>Please check that your browser sends information about referrer page.
			Or simply change your browser to the latest version of
			<a href="http://www.opera.com/">Opera</a>,
			<a href="http://www.mozilla.com/">Mozilla Firefox</a>,
			<a href="http://www.google.com/chrome/">Google Chrome</a> or
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home">Internet Explorer</a>.
		</p>
		<p>Before this, you won\'t be able to send POST or AJAX requests to the site.</p>
		<p>Sorry, but this all is for safety of your data.</p>
		<p><a href="' . $request->get_full_prefix() . '/">Go to the main page</a></p>
		', "text/html");
	finalize();
	die;
}

$loader->autoload();
$loader->run();

//=== output ===//
if (!$error->is_error_occured())
{
	if ($error->is_debug_mode() && isset($_GET["sql"]))
	{
		$html = "<html><head><title>SQL Debugger</title><body bgcolor='white'><style type='text/css'> TABLE, TD, TR, BODY { font-family: verdana,arial, sans-serif;color:black;font-size:11px }</style>";
		$html .= "<h1 align='center'>SQL Total Time: " . $db->debug_get_sql_time() . " for " . $db->debug_get_query_count() . " queries</h1><br />" . $db->debug_get_html();
		$html .= "<br /><div align='center'><strong>Total SQL Time: " . $db->debug_get_sql_time() . "</div></body></html>";
		response::set_content_html($html);
	}
	elseif ($loader->get_redirect_url())
	{
		output_buffer::clean_content();
		response::set_redirect($loader->get_redirect_url(), $loader->get_redirect_type());
		if ($loader->is_error_403())
		{
			response::set_error_403();
		}
		elseif ($loader->is_error_404())
		{
			response::set_error_404();
		}
	}
	else
	{
		if ($loader->is_error_403())
		{
			response::set_error_403();
		}
		elseif ($loader->is_error_404())
		{
			response::set_error_404();
		}
		response::set_no_cache_headers();
		if (!output_buffer::content_exists())
		{
			response::set_content($loader->get_content(), $content_type = $loader->get_content_type());
			if ($content_type == "text/xml" || $content_type == "text/html")
			{
				response::append_content("<!-- " . (microtime(true) - $global_start_time) . " -->");
			}
		}
	}
}

finalize();
?>