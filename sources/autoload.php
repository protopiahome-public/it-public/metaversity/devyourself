<?php

$__autoload_cache_path = PATH_TMP . "/autoload_cache.data";

if (!file_exists($__autoload_cache_path))
{
	__autoload_load_index();
}
$__autoload_cache = unserialize(file_get_contents($__autoload_cache_path));

function __autoload_load_index()
{
	global $__autoload_cache;
	global $__autoload_cache_path;

	$autoload_suffices = array(
		"access" => "access",
		"access_save" => "access",
		"access_maper" => "access",
		"access_fetcher" => "access",
		"obj" => "obj",
		"helper" => "helper",
		"ajax_ctrl" => "ajax_ctrl",
		"ajax_page" => "ajax_page",
		"cache" => "cache",
		"cache_tag" => "cache_tag",
		"easy_processor" => "easy_processor",
		"save_ctrl" => "save_ctrl",
		"save_page" => "save_page",
		"sql_filter" => "sql_filter",
		"taxonomy" => "taxonomy",
		"xml_ctrl" => "xml_ctrl",
		"xml_page" => "xml_page",
		"dt" => "dt",
		"xml_dtf" => "dtf",
		"save_dtf" => "dtf",
		"dtf" => "dtf",
		"api_message" => "api_message",
		"block_save" => "block",
		"block_xml" => "block",
		"mixin" => "mixin",
		"ctrl" => "ctrl",
		"math" => "math",
	);

	$__autoload_cache = array();
	$modules = dir_list_plain(PATH_MODULES);
	foreach ($modules as $module)
	{
		if (file_exists($module . "/autoloaded"))
		{
			$files = dir_list_plain($module . "/autoloaded", true);
			foreach ($files as $file_path)
			{
				if (preg_match("#^.*/([a-z0-9_]+(\.[a-z0-9_]+)*(\.[a-z0-9_]+))\.php$#", $file_path, $matches))
				{
					$class_name = str_replace(".", "_", $matches[1]);
					$type_name = substr($matches[3], 1);
					if (!isset($autoload_suffices[$type_name]) || !strstr($file_path, "/{$autoload_suffices[$type_name]}/"))
					{
						trigger_error("Class {$class_name} is not in folder of type '{$type_name}' (path of it: '{$file_path}')");
					}
					$__autoload_cache[$class_name] = $file_path;
				}
			}
		}
	}
	file_put_contents($__autoload_cache_path, serialize($__autoload_cache));
}

function __autoload($class_name)
{
	global $__autoload_cache;

	if (!is_array($__autoload_cache) || !isset($__autoload_cache[$class_name]))
	{
		__autoload_load_index();
		if (!isset($__autoload_cache[$class_name]))
		{
			trigger_error("Class {$class_name} was not found in __autoload()");
		}
	}
	$file_path = $__autoload_cache[$class_name];
	require_once $file_path;
}

?>