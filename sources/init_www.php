<?php

$global_start_time = microtime(true);
ignore_user_abort(true);

require_once PATH_CONFIG . "/_global_conf.php";

require_once PATH_INTCMF . "/auxil.php";
require_once PATH_SOURCES . "/autoload.php";

//=== error ===//
require_once PATH_INTCMF . "/error.php";
$error = new error(PATH_SOURCES, PATH_HOME, PATH_INTCMF . "/error.xslt", "UTF-8", $config["debug_ip"], PATH_LOG . "/errors", "print_errors_function");

//=== common init ===//
require_once PATH_SOURCES . "/init_common.php";

//=== response ===//
require_once PATH_INTCMF . "/output_buffer.php";
require_once PATH_INTCMF . "/response.php";

//=== request ===//
require_once PATH_INTCMF . "/request.php";
$request = new request(PATH_WWW, true, false, $error->is_debug_mode() ? array("?", "?xml", "?xsl", "?xslt", "?sql") : array(), array("robots.txt", "favicon.ico"));
if ($request->is_redirect_needed())
{
	response::set_redirect_permanent($request->get_canonical_path());
	finalize();
	exit();
}

//=== domain logic ===//
require_once PATH_CORE . "/domain_logic.php";
$domain_logic = new domain_logic();

//=== session ===//
require_once PATH_INTCMF . "/x_session.php";
$session = null;
config_session(); // this function must be in the configuration file

function finalize()
{
	global $error;
	if (!$error->is_error_occured())
	{
		output_buffer::finalize();
	}
}

function print_errors_function()
{
	global $error;
	output_buffer::clean_all();
	response::set_error_500();
	if ($error->is_debug_mode())
	{
		response::set_content_html($error->get_errors_html());
	}
	else
	{
		response::set_content_html(
			"<h1>Ошибка 500</h1><p>Извините, на сервере произошла ошибка. Разработчики уже получили соответствующее уведомление. Попробуйте выполнить действие ещё раз. Если не получилось &#8212; наберитесь терпения и зайдите, пожалуйста, попозже.</p>"
			. str_repeat("<!-- IE fix -->", 50)
		);
	}
	output_buffer::finalize();
}

?>