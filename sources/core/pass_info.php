<?php

class pass_info
{

	/**
	 * @var session
	 */
	protected $session;
	/**
	 * @var xdom
	 */
	protected $xdom;
	protected $vars_dump_exists = false;

	public function __construct()
	{
		global $session;
		$this->session = $session;

		$this->xdom = xdom::create("pass_info");
	}

	public function write_error($error_name, $error_descr = "")
	{
		$this->xdom->create_child_node("error", $error_descr)
			->set_attr("name", $error_name);
	}

	public function write_field_error($field_name, $error_name, $error_descr = "")
	{
		$this->xdom->create_child_node("error", $error_descr)
			->set_attr("field", $field_name)
			->set_attr("name", $error_name);
	}

	public function write_info($info_name, $info_descr = "")
	{
		$this->xdom->create_child_node("info", $info_descr)
			->set_attr("name", $info_name);
	}

	public function dump_vars()
	{
		if ($this->vars_dump_exists)
		{
			return;
		}
		$this->vars_dump_exists = true;
		$var_dump_node = $this->xdom->create_child_node("vars");
		foreach ($_REQUEST as $idx => $val)
		{
			if (is_array($val))
			{
				// Client might send arrays... i.e. <input type="checkbox" name="foo[]" />
				$array_node = $var_dump_node->create_child_node("var")->set_attr("name", $idx);
				foreach ($val as $idx => $val)
				{
					$array_node->create_child_node("item", $val)->set_attr("index", $idx);
				}
			}
			else
			{
				$var_dump_node->create_child_node("var", $val)->set_attr("name", $idx);
			}
		}
	}

	public function vars_dump_exists()
	{
		return $this->vars_dump_exists;
	}

	public function store()
	{
		if ($this->xdom->has_child_nodes())
		{
			$xml = $this->xdom->get_xml(true);
			$session_id = $this->session->get_session_id();
			pass_info_cache::init($session_id)->set($xml);
		}
	}
	
	/**
	 * @return xdom
	 */
	public function get_xdom()
	{
		return $this->xdom;
	}

}

?>