<?php

class ajax_loader extends base_loader
{

	protected $ctrls = array();
	protected $status;
	protected $error_code;
	protected $data = array();

	public function autoload()
	{
		$p2 = $this->request->get_folders(2);
		$p3 = $this->request->get_folders(3);

		if ($p3 !== false)
		{
			$this->set_error_404();
			return;
		}

		if (!preg_match("#^[a-z0-9_\-]+$#", $p2))
		{
			$this->set_error_404();
			return;
		}

		global $__autoload_cache;
		$class = $p2 . "_ajax_page";
		if (!isset($__autoload_cache[$class]))
		{
			$this->set_error_404();
			return;
		}

		$this->add_ctrl(new $class());
	}

	public function run()
	{
		global $config;
		if (isset($config["is_archived"]) and $config["is_archived"] and !$this->user->is_admin())
		{
			$this->set_content_type_text();
			$this->set_content("Project is archived. AJAX modules are turned off");
			return;
		}
		if ($this->is_error_404())
		{
			$this->set_content_type_text();
			$this->set_content("Bad controller");
			return;
		}
		if ($this->is_error_403())
		{
			$this->set_content_type_text();
			$this->set_content("No rights");
			return;
		}

		if ($this->run_set_up())
		{
			if ($this->stop_load)
			{
				// Do nothing
			}
			elseif ($this->run_start())
			{
				if ($this->stop_load)
				{
					// Do nothing
				}
				elseif ($this->run_check_rights())
				{
					$commit_ok = false;
					$check_ok = $this->run_check();
					$check_ok ? ($commit_ok = $this->run_commit()) : $this->run_rollback();

					if ($commit_ok)
					{
						$this->run_get_data();
					}
				}
				else
				{
//					$this->set_error_403();
				}
			}
			else
			{
				$this->set_error_404();
			}
		}
		else
		{
			$this->set_error_404();
		}

		if ($this->status)
		{
			$this->data["status"] = $this->status;
		}
		if ($this->error_code)
		{
			$this->data["error_code"] = $this->error_code;
		}
		$this->set_content_type_json();
		$this->set_content(json_encode($this->data));
	}

	private function run_set_up()
	{
		if ($this->stop_load)
		{
			return true;
		}
		reset($this->ctrls);
		while (($ajax_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $ajax_ctrl base_ajax_ctrl */
			$ajax_ctrl->mixin_call_method_with_mixins("init");
			if ($this->ctrls[0]->autostart_db_transaction() && !$this->db->is_transaction_started())
			{
				$this->db->begin();
			}
			$ok = $ajax_ctrl->mixin_call_method_with_mixins("set_up", array(), "chain");
			if (!$ok)
			{
				$this->write_fail("SET_UP", "ERROR");
				return false;
			}
			next($this->ctrls);
		}
		return true;
	}

	private function run_start()
	{
		if ($this->stop_load)
		{
			return true;
		}
		reset($this->ctrls);
		while (($ajax_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $ajax_ctrl base_ajax_ctrl */
			$before_ok = $ajax_ctrl->mixin_call_method_with_mixins("on_before_start", array(), "chain");
			if (!$before_ok)
			{
				$this->write_fail("BEFORE_START", "ERROR");
				return false;
			}
			$ok = $ajax_ctrl->mixin_call_method_with_mixins("start", array(), "chain");
			if (!$ok)
			{
				$this->write_fail("START", "ERROR");
				return false;
			}
			$after_ok = $ajax_ctrl->mixin_call_method_with_mixins("on_after_start", array(), "chain");
			if (!$after_ok)
			{
				$this->write_fail("AFTER_START", "ERROR");
				return false;
			}
			next($this->ctrls);
		}
		return true;
	}

	private function run_check_rights()
	{
		if ($this->stop_load)
		{
			return true;
		}
		reset($this->ctrls);
		while (($ajax_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $ajax_ctrl base_ajax_ctrl */
			$ok = $ajax_ctrl->mixin_call_method_with_mixins("check_rights", array(), "chain");
			if (!$ok)
			{
				$this->write_fail("CHECK_RIGHTS", "BAD_RIGHTS");
				return false;
			}
			next($this->ctrls);
		}
		return true;
	}

	private function run_check()
	{
		if ($this->stop_load)
		{
			return true;
		}
		reset($this->ctrls);
		$check_ok = true;
		while (($ajax_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $ajax_ctrl base_ajax_ctrl */
			$before_ok = $ajax_ctrl->mixin_call_method_with_mixins("on_before_check", array(), "and");
			if (!$before_ok)
			{
				$this->write_fail("BEFORE_CHECK", "ERROR");
				$check_ok = false;
			}
			else
			{
				$ok = $ajax_ctrl->mixin_call_method_with_mixins("check", array(), "and");
				if (!$ok)
				{
					$this->write_fail("CHECK", "ERROR");
					$check_ok = false;
				}
				else
				{
					$ajax_ctrl->mixin_call_method_with_mixins("on_after_check", array(), "and");
				}
			}
			next($this->ctrls);
		}
		return $check_ok;
	}

	private function run_commit()
	{
		if ($this->stop_load)
		{
			return true;
		}
		$commit_ok = true;
		reset($this->ctrls);
		while (($ajax_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $ajax_ctrl base_ajax_ctrl */
			$before_ok = $ajax_ctrl->mixin_call_method_with_mixins("on_before_commit", array(), "chain");
			if (!$before_ok)
			{
				$this->write_fail("BEFORE_COMMIT", "ERROR");
				$commit_ok = false;
				break;
			}
			else
			{
				$ok = $ajax_ctrl->mixin_call_method_with_mixins("commit", array(), "chain");
				if (!$ok)
				{
					$this->write_fail("COMMIT", "ERROR");
					$commit_ok = false;
					break;
				}
				else
				{
					$ajax_ctrl->mixin_call_method_with_mixins("on_after_commit");
				}
			}
			next($this->ctrls);
		}
		if ($commit_ok && !$this->stop_load)
		{
			if ($this->ctrls[0]->autostart_db_transaction() && $this->db->is_transaction_started())
			{
				$this->db->commit();
			}
			reset($this->ctrls);
			while (($ajax_ctrl = current($this->ctrls)) && !$this->stop_load)
			{
				/* @var $ajax_ctrl base_ajax_ctrl */
				$ajax_ctrl->mixin_call_method_with_mixins("clean_cache");
				next($this->ctrls);
			}
		}
		else
		{
			if ($this->ctrls[0]->autostart_db_transaction() && $this->db->is_transaction_started())
			{
				$this->db->rollback();
			}
		}

		return $commit_ok;
	}

	private function run_rollback()
	{
		if ($this->stop_load)
		{
			return true;
		}
		reset($this->ctrls);
		while (($ajax_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $ajax_ctrl base_ajax_ctrl */
			$ajax_ctrl->mixin_call_method_with_mixins("rollback");
			next($this->ctrls);
		}
		if ($this->ctrls[0]->autostart_db_transaction() && $this->db->is_transaction_started())
		{
			$this->db->rollback();
		}
	}

	private function run_get_data()
	{
		reset($this->ctrls);
		while (($ajax_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $ajax_ctrl base_ajax_ctrl */
			$this->data = $ajax_ctrl->mixin_call_method_with_mixins("get_data", array(), "array_merge");
			if ($ajax_ctrl->is_error_403())
			{
				$this->set_error_403();
			}
			if ($ajax_ctrl->is_error_404())
			{
				$this->set_error_404();
			}
			next($this->ctrls);
		}
	}

	public function add_ctrl(base_ajax_ctrl $ajax_ctrl)
	{
		$this->ctrls[] = $ajax_ctrl;
	}

	public function write_fail($error_code, $status)
	{
		$this->error_code = $error_code;
		$this->status = $status;
	}

	public function write_error($error_name, $error_description = true)
	{
		$this->data["errors"][$error_name] = $error_description;
	}

}

?>