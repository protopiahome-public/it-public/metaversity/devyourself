<?php

class xml_loader extends base_loader
{

	private $xmls = array();
	private $main_xslt_name;
	private $multi_xslt_names = array();
	private $error_403_xslt_name;
	private $error_404_xslt_name;
	private $error_403_xml_ctrl;
	private $error_404_xml_ctrl;
	private $cache_stat = array();
	private $is_xml_page_loaded = false;

	public function autoload()
	{
		$taxonomy = new site_taxonomy($this);
		$taxonomy->run();
	}

	public function run()
	{
		$xml = $this->get_xml();
		$no_stylesheet = $this->error->is_debug_mode() && ($this->request->get_params_string() == "?" || isset($_GET ["xml"]));
		if ($no_stylesheet)
		{
			$this->set_content($xml);
			$this->set_content_type_xml();
		}
		elseif (output_buffer::content_exists())
		{
			// Do nothing
		}
		elseif (!$this->get_redirect_url())
		{
			$xslt = $this->get_xslt();
			if ($this->error->is_debug_mode() && isset($_GET["xslt"]))
			{
				$this->set_content($xslt);
				$this->set_content_type_xml();
			}
			elseif ($xml)
			{
				chdir(PATH_XSLT);
				$html = xdom::create_from_string($xml)->xslt_process($xslt);
				$this->set_content($html);
				$this->set_content_type_html();
			}
			else
			{
				$this->set_content("NO_CONTENT");
			}
		}
	}

	// It is public for tests
	public function get_xml()
	{
		// xml controls processing
		$xml = "";
		reset($this->xmls);
		while (($xml_ctrl = current($this->xmls)) && !$this->stop_load)
		{
			$ctrl_name = substr(get_class($xml_ctrl), 0, -9);

			/* @var $xml_ctrl base_xml_ctrl */
			$xml_ctrl->set_xml_loader($this);
			$xml_ctrl->mixin_call_method_with_mixins("init");
			if (!$xml_ctrl->mixin_call_method_with_mixins("start", array(), "chain"))
			{
				$this->set_error_404();
				if (!$this->error_404_xml_ctrl)
				{
					$this->error_404_xml_ctrl = $ctrl_name;
				}
			}
			if (!$xml_ctrl->mixin_call_method_with_mixins("check_rights", array(), "chain"))
			{
				$this->set_error_403();
				if (!$this->error_403_xml_ctrl)
				{
					$this->error_403_xml_ctrl = $ctrl_name;
				}
			}
			$xml .= $xml_ctrl->get_xml();

			if ($xml_ctrl->get_redirect_url())
			{
				$this->set_redirect_url($xml_ctrl->get_redirect_url());
				$this->set_redirect_type($xml_ctrl->get_redirect_type());
				return "";
			}
			if ($xml_ctrl->is_error_403())
			{
				$this->set_error_403();
				if (!$this->error_403_xml_ctrl)
				{
					$this->error_403_xml_ctrl = $ctrl_name;
				}
			}
			if ($xml_ctrl->is_error_404())
			{
				$this->set_error_404();
				if (!$this->error_404_xml_ctrl)
				{
					$this->error_404_xml_ctrl = $ctrl_name;
				}
			}
			$this->add_cache_stat_record($xml_ctrl->get_cache_state(), $ctrl_name);
			// Prev. version:
			// if ($this->is_error_403() or $this->is_error_404())
			// - but this blocked usage of $this->xml_loader->set_error_403(); in taxonomy.
			if ($xml_ctrl->is_error_403() or $xml_ctrl->is_error_404())
			{
				break;
			}
			next($this->xmls);
		}

		$stat_ctrl = new stat_xml_ctrl();
		$stat_ctrl->set_xml_loader($this);
		$xml .= $stat_ctrl->get_xml();

		// <error_403> | <error_404>
		$error_node = "";
		if ($this->is_error_403())
		{
			$error_node = '<error_403 class="' . $this->error_403_xml_ctrl . '"/>';
		}
		if ($this->is_error_404())
		{
			$error_node = '<error_404 class="' . $this->error_404_xml_ctrl . '"/>';
		}

		$time = getdate();

		$time_string = date("Y-m-d H:i:s");

		$date_xml = <<<EOF
year="{$time["year"]}" month="{$time["mon"]}" day="{$time["mday"]}"
EOF;

		// xml
		return <<<EOF
<?xml version="1.0" encoding="UTF-8"?><root time="{$time_string}" {$date_xml}>{$error_node}{$xml}</root>
EOF;
	}

	public function add_xml(base_xml_ctrl $xml_ctrl, $index = null)
	{
		if (is_null($index))
		{
			$this->xmls[] = $xml_ctrl;
		}
		else
		{
			$this->xmls[$index] = $xml_ctrl;
		}

		$class = get_class($xml_ctrl);
		if (substr($class, -4) == "page")
		{
			$this->is_xml_page_loaded = true;
		}
	}

	/**
	 * Accepts various number of params
	 */
	public function add_xml_by_class_name($class, $param, $param2 = null, $param3 = null, $param4 = null)
	{
		$args = func_get_args();
		array_shift($args);
		$index = $class . "_" . $param;
		if (!isset($this->xmls[$index]))
		{
			$ref = new ReflectionClass($class);
			$this->xmls[$index] = $ref->newInstanceArgs($args);

			if (substr($class, -4) == "page")
			{
				$this->is_xml_page_loaded = true;
			}
		}
	}

	public function add_xml_with_xslt(base_xml_ctrl $xml_ctrl, $error_404_xslt = null, $error_403_xslt = null)
	{
		$this->add_xml($xml_ctrl, null);
		$xslt_name = substr(get_class($xml_ctrl), 0, -9);
		if (substr($xslt_name, -7, 7) == "_simple")
		{
			$xslt_name = substr($xslt_name, 0, -7);
		}
		elseif (substr($xslt_name, -5, 5) == "_easy")
		{
			$xslt_name = substr($xslt_name, 0, -5);
		}

		$module_name = $xml_ctrl->get_module_name();
		$this->add_xslt($xslt_name, $module_name);

		if ($error_404_xslt)
		{
			$this->set_error_404_xslt($error_404_xslt, $module_name);
		}
		else
		{
			$this->set_error_404_xslt("error_404", "_site");
		}

		if ($error_403_xslt)
		{
			$this->set_error_403_xslt($error_403_xslt, $module_name);
		}
	}

	/**
	 * @param $xslt_name - xslt file name without extension
	 */
	public function set_main_xslt($xslt_name, $module)
	{
		$xslt_name = $module . "/" . $xslt_name;

		$this->main_xslt_name = $xslt_name;
	}

	/**
	 * @param $xslt_name - xslt file name without extension
	 */
	public function add_xslt($xslt_name, $module)
	{
		$xslt_name = $module . "/" . $xslt_name;
		if (!in_array($xslt_name, $this->multi_xslt_names))
		{
			$this->multi_xslt_names[] = $xslt_name;
		}
	}

	/**
	 * @param $error_403_xslt_name - xslt file name without extension
	 */
	public function set_error_403_xslt($error_403_xslt_name, $module)
	{
		$error_403_xslt_name = $module . "/" . $error_403_xslt_name;
		$this->error_403_xslt_name = $error_403_xslt_name;
	}

	/**
	 * @param $error_404_xslt_name - xslt file name without extension
	 */
	public function set_error_404_xslt($error_404_xslt_name, $module)
	{
		$error_404_xslt_name = $module . "/" .  $error_404_xslt_name;
		$this->error_404_xslt_name = $error_404_xslt_name;
	}

	public function get_cache_stat()
	{
		return $this->cache_stat;
	}

	public function add_cache_stat_record($cache_state, $ctrl_name)
	{
		if (!isset($this->cache_stat[$cache_state]))
		{
			$this->cache_stat[$cache_state] = array();
		}
		if (!isset($this->cache_stat[$cache_state][$ctrl_name]))
		{
			$this->cache_stat[$cache_state][$ctrl_name] = 1;
		}
		else
		{
			$this->cache_stat[$cache_state][$ctrl_name]++;
		}
	}

	public function get_xslt()
	{
		$xslt = '<?xml version="1.0" encoding="UTF-8"?>';
		$xslt .= '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">';
		if ($this->main_xslt_name)
		{
			$xslt .= $this->get_xslt_include_declaration($this->main_xslt_name);
		}
		if ($this->is_error_403() and $this->error_403_xslt_name)
		{
			$xslt .= $this->get_xslt_include_declaration($this->error_403_xslt_name);
		}
		elseif ($this->is_error_404() and $this->error_404_xslt_name)
		{
			$xslt .= $this->get_xslt_include_declaration($this->error_404_xslt_name);
		}
		else
		{
			foreach ($this->multi_xslt_names as $xslt_name)
			{
				$xslt .= $this->get_xslt_include_declaration($xslt_name);
			}
		}
		$xslt .= '</xsl:stylesheet>';
		return $xslt;
	}

	private function get_xslt_path($xslt_name)
	{
		$xslt_path = univers_filename(PATH_XSLT . "/" . $xslt_name . ".xslt");
		return $xslt_path;
	}

	private function get_xslt_include_declaration($xslt_name)
	{
		$file_path = $this->get_xslt_path($xslt_name);
		return '<xsl:include href="' . $file_path . '"/>';
		// Obsolete code which allowed uniexisted XSLT files (I do not remember
		// why this code was written this way.
		/*
		  if (file_exists($file_path))
		  {
		  return '<xsl:include href="' . $file_path . '"/>';
		  }
		  else
		  {
		  return '<!-- Unexisted file: ' . $file_path . ' -->';
		  }
		 */
	}

	public function is_xml_page_loaded()
	{
		return $this->is_xml_page_loaded;
	}

}

?>