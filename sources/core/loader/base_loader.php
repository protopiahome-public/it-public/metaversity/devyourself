<?php

abstract class base_loader extends base_http_issues
{
	
	/**
	 * @var error
	 */
	protected $error;
	/**
	 * @var request
	 */
	protected $request;
	/**
	 * @var db
	 */
	protected $db;
	/**
	 * @var user
	 */
	protected $user;
	protected $stop_load = false;
	
	public function __construct()
	{
		global $error;
		$this->error = $error;
		
		global $request;
		$this->request = $request;
		
		global $db;
		$this->db = $db;
		
		global $user;
		$this->user = $user;
	}
	
	private $content_type = "text/plain";
	private $content = "";
	
	abstract public function autoload();
	
	abstract public function run();
	
	public function get_content_type()
	{
		return $this->content_type;
	}
	
	public function get_content()
	{
		return $this->content;
	}
	
	public function stop_load()
	{
		$this->stop_load = true;
	}
	
	protected function set_content($content)
	{
		$this->content = $content;
	}
	
	protected function set_content_type($content_type)
	{
		$this->content_type = $content_type;
	}
	
	protected function set_content_type_xml()
	{
		$this->set_content_type("text/xml");
	}
	
	protected function set_content_type_xslt()
	{
		$this->set_content_type("application/xslt+xml");
	}
	
	protected function set_content_type_html()
	{
		$this->set_content_type("text/html");
	}
	
	protected function set_content_type_js()
	{
		$this->set_content_type("text/javascript");
	}
	
	protected function set_content_type_json()
	{
		$this->set_content_type("application/json");
	}
	
	protected function set_content_type_text()
	{
		$this->set_content_type("text/plain");
	}
	
}

?>