<?php

class user
{

	/**
	 * @var session
	 */
	private $session;

	/**
	 * @var db
	 */
	private $db;
	private $user_id = 0;
	private $user_data = array();

	public function __construct($user_id = null)
	{
		global $session;
		$this->session = $session;

		global $db;
		$this->db = $db;

		if (!is_null($user_id))
		{
			$this->user_id = $user_id;
		}
		elseif (isset($_SESSION["user_id"]))
		{
			$this->user_id = $_SESSION["user_id"];
		}

		if ($this->user_id)
		{
			$this->fill_user_data();
			if (!sizeof($this->user_data))
			{
				$this->user_id = 0;
			}
		}
	}

	public function login($login, $password)
	{
		if (!$login or !$password)
		{
			return false;
		}
		$login_escaped = $this->db->escape($login);
		$password_hash = $this->get_password_hash($password);
		$password_hash_escaped = $this->db->escape($password_hash);
		$user_id = $this->db->get_value("SELECT id FROM user WHERE login = '{$login_escaped}' AND BINARY password = '{$password_hash_escaped}'");
		if (!$user_id)
		{
			return false;
		}

		return $this->login_by_id($user_id);
	}
	
	public function get_password_hash($password)
	{
		return sha1("wqejiqwejqliejqwieqwiehqwoehqweiqwejhqw_set_salt_from_config" . $password); // @todo
	}

	public function login_by_id($user_id)
	{
		$this->user_id = $user_id;
		$this->clean_user_data();
		$this->fill_user_data();
		if (!sizeof($this->user_data))
		{
			$this->user_id = 0;
		}

		$this->session->session_write_begin();
		if ($this->user_id)
		{
			$_SESSION["user_id"] = $this->user_id;
		}
		else
		{
			unset($_SESSION["user_id"]);
		}
		$this->session->session_write_commit();
		if ($this->user_id)
		{
			$this->db->sql("UPDATE user SET last_login_time = NOW() WHERE id = {$user_id}");
		}
		return $this->user_id;
	}

	public function logout()
	{
		$this->session->session_write_begin();
		unset($_SESSION["user_id"]);
		$this->session->session_write_commit();
		$this->user_id = 0;
		$this->clean_user_data();
	}

	public function set_user_id($user_id)
	{
		$this->user_id = $user_id;
	}

	public function set_user_data($user_data)
	{
		$this->user_data = $user_data;
	}

	public function get_user_id()
	{
		return $this->user_id;
	}

	public function get_user_param($key)
	{
		return isset($this->user_data[$key]) ? $this->user_data[$key] : null;
	}

	public function get_login()
	{
		return $this->get_user_param("login");
	}

	public function get_email()
	{
		return $this->get_user_param("email");
	}

	public function get_visible_name()
	{
		return $this->get_user_param("visible_name");
	}

	public function is_no_name()
	{
		return $this->get_user_param("no_name");
	}

	public function get_sex()
	{
		return $this->get_user_param("sex");
	}

	public function get_profile_url()
	{
		return $this->get_user_param("profile_url");
	}

	public function is_admin()
	{
		$is_admin = $this->get_user_param("is_admin"); // might me null
		return $is_admin ? true : false;
	}

	public function get_user_data()
	{
		return is_array($this->user_data) ? $this->user_data : false;
	}

	private function clean_user_data()
	{
		$this->user_data = array();
	}

	private function fill_user_data()
	{
		if ($this->user_id and !$this->user_data)
		{
			$cache = user_cache::init($this->user_id);
			$this->user_data = $cache->get();
			if (empty($this->user_data))
			{
				$this->user_data = $this->fetch_user_data();
				if (!$this->user_data)
				{
					$this->user_data = array();
				}
				else
				{
					$cache->set($this->user_data);
				}
			}
		}
	}

	public function fetch_user_data()
	{
		$row = $this->db->get_row("
			SELECT
				IF(first_name = '' AND last_name = '', '<без имени>', TRIM(CONCAT(first_name, ' ', last_name))) AS visible_name,
				user.*
			FROM user 
			WHERE id = {$this->user_id}
		");
		if (!$row)
		{
			return null;
		}
		unset($row["password"]);
		return $row;
	}

}

?>