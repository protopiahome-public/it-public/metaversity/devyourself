<?php

// client endpoint communication
define("CLIENT_REQUEST_ARRAY", 1);
define("CLIENT_REQUEST_ERROR_MESSAGE", 100);

final class api_client_interaction
{
	const API_VERSION = "20120223";
	const KEY_TTL = 600; // 2 class const 
	const KEY_SALT = "very string";

	private $curl_errno = "";
	private $http_code = "";
	private $key = "none";
	private $called_url;

	public function __construct(base_api_message $api_message)
	{
		$this->key = sha1(self::KEY_SALT . date("r") . mt_rand());
		$cache = endpoint_cache::init($this->key)->set($api_message, self::KEY_TTL);
	}

	public function get_key()
	{
		return $this->key;
	}

	public function call($endpoint_url, $method)
	{
		$this->called_url = "{$endpoint_url}?method={$method}&key={$this->key}";
		$ch = curl_init($this->called_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 90);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; devyourself.ru/server)");
		$data = curl_exec($ch);
		$this->curl_errno = curl_errno($ch);
		if ((false === $data) or (($this->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) != 200))
		{
			// Данных совсем не пришло или может что и есть, но это не 200 ok
			//$this->log_message("Empty response from the server endpoint", __FILE__, __LINE__);
			return false;
		}
		//echo $data;
		return $data;
	}
	
	public function get_called_url()
	{
		return $this->called_url;
	}

	public function get_curl_errno()
	{
		return $this->curl_errno;
	}

	public function get_http_code()
	{
		return $this->http_code;
	}

}

?>