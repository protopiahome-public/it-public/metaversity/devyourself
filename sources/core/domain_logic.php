<?php

class domain_logic
{

	/**
	 * @var request
	 */
	protected $request;

	/**
	 * @var db
	 */
	protected $db;
	protected $is_on;
	protected $main_host_name;
	protected $main_host_prefix;
	protected $current_domain_project_id = false;

	public function __construct()
	{
		global $request;
		$this->request = $request;

		global $db;
		$this->db = $db;

		global $config;
		$this->is_on = $config["main_host_name"] ? true : false;
		$this->main_host_name = $this->is_on ? $config["main_host_name"] : false;
		$this->main_host_prefix = $config["main_host_prefix"];
	}
	
	public function get_main_host_name()
	{
		return $this->main_host_name;
	}

	public function is_on()
	{
		return $this->is_on;
	}

	public function get_main_prefix()
	{
		return $this->request->get_protocol() . "://"  . ($this->is_on ? $this->main_host_name . $this->main_host_prefix : $this->request->get_host() . $this->request->get_prefix());
	}
	
	public function get_current_prefix()
	{
		return $this->is_main_domain() ? $this->request->get_prefix() : $this->get_main_prefix();
	}

	public function get_current_domain_project_id()
	{
		if ($this->current_domain_project_id === false)
		{
			$current_host_name = $this->request->get_host();
			$this->current_domain_project_id = $this->get_project_id_by_host_name($current_host_name);
		}
		return $this->current_domain_project_id;
	}

	public function is_current_domain_project_domain($project_id)
	{
		if (!$this->is_on)
		{
			return false;
		}

		return $project_id == $this->get_current_domain_project_id();
	}

	public function is_main_domain()
	{
		if (!$this->is_on)
		{
			return true;
		}

		return $this->request->get_host() == $this->main_host_name;
	}
	
	public function is_secondary_domain()
	{
		if (!$this->is_on)
		{
			return false;
		}

		return $this->request->get_host_without_www() == $this->main_host_name;
	}

	public function get_project_id_by_host_name($host_name)
	{
		// @dm9 ensure in code that this method is called only when is_on()
		// @dm9 "taxonomy_"?
		if (($project_id = taxonomy_host_cache::init($host_name)->get()))
		{
			return $project_id;
		}

		// @dm9 BINARY and move all these checks in one helper
		// @dm9 probably, move this function somewhere else and
		// remove dependence of this class from db.
		// @dm9 check DB index (check also other tables, for example, last_comment_time)
		$host_name_escaped = $this->db->escape($host_name);
		$project_id = $this->db->get_value("
			SELECT id
			FROM project
			WHERE domain_name = '{$host_name_escaped}' AND domain_is_on = 1
		");

		if (!$project_id)
		{
			// search project by <name>.<main_host_name>
			$host_splitted = explode(".", $host_name, 2);
			if (isset($host_splitted[1]) and $host_splitted[1] == $this->get_main_host_name())
			{
				$name = $host_splitted[0];
				$name_escaped = $this->db->escape($name);
				$project_id = $this->db->get_value("
					SELECT id
					FROM project
					WHERE name = '{$name_escaped}'
				");
			}
		}

		if ($project_id)
		{
			taxonomy_host_cache::init($host_name, $project_id)->set($project_id);
		}

		// @dm9 ensure it returns null on fail, not false
		return $project_id;
	}

}

?>