<?php

define("TYPE_DB_XHTML", "xhtml");
define("TYPE_DB_IMAGE_SIMPLE", "image_simple");

class db_xml_type_helper
{

	public static function get_type(&$column_name, &$remove_columns = array(), $column_type_array = array())
	{
		if (isset($column_type_array[$column_name]))
		{
			return $column_type_array[$column_name];
		}
		else
		{
			if ($column_name == "descr_edit" || $column_name == "html_edit" || $column_name == "html_more_edit" || substr($column_name, -10, 10) == "_html_edit")
			{
				return "xhtml_edit";
			}
			elseif ($column_name == "descr" || $column_name == "html" || substr($column_name, 0, 5) == "html_" || substr($column_name, -5, 5) == "_html")
			{
				return "xhtml";
			}
			elseif ($column_name == "url_name")
			{
				$remove_columns[$column_name . "_prev"] = true;
				return "url_name";
			}
			elseif (substr($column_name, -6, 6) == "_width")
			{
				$column_name = substr($column_name, 0, -6);
				$remove_columns[$column_name . "_height"] = true;
				$remove_columns[$column_name . "_version"] = true;
				$remove_columns[$column_name . "_url"] = true;
				return "image_simple";
			}
			elseif (substr($column_name, -10, 10) == "_file_name")
			{
				$column_name = substr($column_name, 0, -10);
				$remove_columns[$column_name . "_file_size"] = true;
				$remove_columns[$column_name . "_version"] = true;
				return "file_simple";
			}
		}
		return "";
	}

	public static function get_possible_auxil_data_keys($column_name, $column_type_array = array())
	{
		$remove_columns = array();
		$type = self::get_type($column_name, $remove_columns, $column_type_array);
		$possible_auxil_data_keys = array();
		switch ($type)
		{
			case "image_simple":
				$possible_auxil_data_keys[] = $column_name . "_url";
				$possible_auxil_data_keys[] = $column_name . "_prefix";
				$possible_auxil_data_keys[] = $column_name . "_scale_max_width";
				$possible_auxil_data_keys[] = $column_name . "_scale_max_width";
				$possible_auxil_data_keys[] = $column_name . "_stub_url";
				$possible_auxil_data_keys[] = $column_name . "_stub_width";
				$possible_auxil_data_keys[] = $column_name . "_stub_height";
				break;
				
			case "url_name":
				$possible_auxil_data_keys[] = $column_name . "_prefix";
				break;
		}
		return $possible_auxil_data_keys;
	}

}

?>