<?php

require_once PATH_CORE . "/base_http_issues.php";

class save_ctrl_tester extends base_tester
{

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var test_results
	 */
	protected $test_results;
	protected $ctrl_name;
	protected $is_save_page;

	/**
	 * @var base_save_ctrl
	 */
	protected $ctrl_instance;
	protected $ctrl_result;

	public function __construct($ctrl_name, $is_save_page = true)
	{
		$this->ctrl_name = $ctrl_name;
		$this->is_save_page = $is_save_page;

		global $db;
		$this->db = $db;

		global $test_results;
		$this->test_results = $test_results;
	}

	public function run_and_assert_success()
	{
		$this->run();
		$this->assert_success();
	}

	public function run_and_assert_fail()
	{
		$this->run();
		$this->assert_fail();
	}

	protected function run()
	{
		$class = $this->ctrl_name . ($this->is_save_page ? "_save_page" : "_save_ctrl");
		$this->ctrl_instance = new $class();
		$this->ctrl_result = $this->ctrl_instance->run();
	}

	protected function assert_success()
	{
		if (!$this->ctrl_result)
		{
			$this->test_results->add_error("Save ctrl '{$this->ctrl_name}' did not return true");
		}
		if (!$this->db->test_is_commit_performed())
		{
			$this->test_results->add_error("Save ctrl '{$this->ctrl_name}' did not perform COMMIT");
		}
		if (!$this->db->test_is_transaction_finished())
		{
			$this->test_results->add_error("Save ctrl '{$this->ctrl_name}' did not finish transaction");
		}
	}

	protected function assert_fail()
	{
		if ($this->ctrl_result)
		{
			$this->test_results->add_error("Save ctrl '{$this->ctrl_name}' returned true");
		}
		if ($this->db->test_is_commit_performed())
		{
			$this->test_results->add_error("Save ctrl '{$this->ctrl_name}' performed COMMIT");
		}
		if (!$this->db->test_is_transaction_finished())
		{
			$this->test_results->add_error("Save ctrl '{$this->ctrl_name}' did not finish transaction");
		}
	}

}

?>