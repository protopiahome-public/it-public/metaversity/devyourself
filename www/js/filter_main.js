function filter_main_init()
{
	// I like closures. Do you?
	var filter_main_wug_all = false;
	var filter_main_wug_any = new Array();
	
	function foreach_in_array( array, func )
	{
		// вызов функции-метода для каждого объекта из массива
		for( var i=0; i<array.length; i++ )
		{
			func.apply( array[i] );
		}
	}
	
	function onchange_for_wug_all()
	{
		// onchange для rater_user_groups_all
		if ( this.checked )
		{
			foreach_in_array( filter_main_wug_any, function(){ this.checked = 1 } );
		}
	}
	
	function onchange_for_wug_any()
	{
		// onchange для rater_user_groups_all[]
		if ( !this.checked )
		{
			filter_main_wug_all.checked = 0;
		}
	}

	// а здесь пошла инициализация
	var filter_main = document.getElementById("filter_main");
	if (!filter_main)
	{
		// на этой странице нет filter_main
		return;
	}
	
	var cbs=filter_main.getElementsByTagName("input");
	for (var i = 0; i<cbs.length; i++)
	{
		// перебираем все элементы INPUT
		var box = cbs[i];
		if (box.type!="checkbox")
		{
			// это вообще не чекбокс
			continue;
		}			
		if (box.name=="rater_user_groups_all")
		{
			// это rater_user_groups_all
			filter_main_wug_all = box;
			box.onchange = onchange_for_wug_all;
		}
		else if (box.name=="rater_user_groups[]")
		{
			// это один из rater_user_groups[]
			filter_main_wug_any.push(box);
			box.onchange = onchange_for_wug_any;
		}
	}
}

// главное, это ставить побольше скобок. просто поверь, что сначала будет вызван misc_tools.init(), а затем - предыдущее значение window.onload()
window.onload=(function(){var chain=window.onload; return function(){filter_main_init(); if (chain) chain.apply(window);}})();


