$(function() {
	$('.jq-delete-button').click(function() {
		var $form_el = $(this).parents('form');
		if (confirm('Вы действительно хотите удалить источник?')){
			$form_el.submit();
		}
	});
});