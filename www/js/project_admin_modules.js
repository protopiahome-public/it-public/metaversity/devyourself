$(function() {
	function update_select_hints() {
		var $container = $(this).parents('.jq-module')
		var $button = $container.find('button');
		var $select = $container.find('select');
		var $hint = $container.find('.jq-select-hint');
		var ok = !!$select.val();
		$button.toggleClass('button-add-disabled', !ok);
		$button.toggleClass('jq-button-add-disabled', !ok);
		$hint.toggle(!ok);
	}
	$('.jq-module select').change(update_select_hints);
	$('.jq-module select').change();
	$('.jq-disable-link').click(function(){
		$(this).parents('.jq-disable-form').submit();
		return false;
	});
	$('.jq-enable-form').submit(function(){
		if ($(this).find('.jq-enable-button').hasClass('jq-button-add-disabled')) {
			return false;
		}
	});
});