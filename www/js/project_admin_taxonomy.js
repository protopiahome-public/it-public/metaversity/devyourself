$(function () {
	function tree_refresh(){
		$.ajax({
			async: false,
			type: 'POST',
			url: taxonomy_tree_ajax_url,
			data: {
				'operation' : 'get_nodes', 
				'project_id' : project_id,
				'id' : '-',
				'selected_item_id': selected_item_id
			},
			success: function (data) {
				$('#jq-taxonomy-tree').tree('loadData', data.data);
			}
		});
	}
	function tree_error() {
		tree_refresh();
		show_error();
	}
	function tree_error_rights() {
		tree_refresh();
		show_error('#jq-error-rights');
	}
	
	function tree_init() {
		var ids = $('#jq-taxonomy-tree').data('simple_widget_tree').tree.id_mapping;
		for (var id in ids) {
			var node = ids[id];
			var $el = $(node.element);
			var $link = $el.find('.jqtree-title').first();
			if (node.rel == 'root') {
				$link.html('<a href="' + taxonomy_admin_root_url + 'main/">' + node.name + '</a>');
			}
			if (node.rel == 'module') {
				$link.html('<a href="' + taxonomy_admin_root_url + node.module_name + '/">' + node.name + '</a>');
			}
			if (node.rel == 'link') {
				$link.html('<a href="' + taxonomy_admin_root_url + 'links/' + node['jq-item-id'] + '/">' + node.name + '</a>');
			}
			if (node.rel == 'static_page') {
				$link.html('<a href="' + taxonomy_admin_root_url + 'static-pages/' + node['static_page_name'] + '/">' + node.name + '</a>');
			}
			if (node.rel == 'folder') {
				$link.html('<a href="' + taxonomy_admin_root_url + 'folders/' + node['jq-item-id'] + '/">' + node.name + '</a>');
			}
			if (node.selected_item == 1) {
				window.selected_item_id = node['jq-item-id'];
				$link.addClass('selected-item-');
				$('#jq-taxonomy-tree').tree('selectNode', node, true);
				$('#jq-taxonomy-tree').tree('openNode', node.parent, true);
			}

		}
	}
	
	$('#jq-taxonomy-tree').bind('tree.init', tree_init);
	$('#jq-taxonomy-tree').tree({
		data: taxonomy_tree_init_data,
		dragAndDrop: true,
		autoOpen: 1,
		saveState: true,
		onCanMove: function(node) {
			return node.rel != 'root';
		},
		onCanMoveTo: function(moved_node, target_node, position) {
			var parent_node = (position == 'inside') ? target_node : target_node.parent;

			if (position == 'before' && target_node.rel == 'root') {
				return false;
			}
			
			if (moved_node.children.length) {
				return false;
			}
			if (!parent_node) {
				return true;
			}
			if (!parent_node.rel) {
				return true;
			}
			if (parent_node.parent.parent) {
				return false;
			}
			if (parent_node.rel == 'root') {
				return false;
			}
			if (parent_node.rel == 'folder' && moved_node.rel != 'folder') {
				return true;
			}
			if (parent_node.rel == 'static_page' && moved_node.rel == 'static_page') {
				return true;
			}
			
			return false;
		}
	})
	.bind('tree.move', function(event){
		var id;
		var position;
		var parent_id;
			
		event.preventDefault();
		event.move_info.do_move();
		var $moved_node = $(event.move_info.moved_node.element);
			
		id = event.move_info.moved_node['jq-item-id'];
		position = $moved_node.prevAll().length;
		parent_id = event.move_info.moved_node.parent['jq-item-id'] ? event.move_info.moved_node.parent['jq-item-id'] : '0';
		if (parent_id == '0'){
			position --;
		}
			
		$.ajax({
			async: false,
			type: 'POST',
			url: taxonomy_tree_ajax_url,
			data : {
				'operation' : 'move_item', 
				'project_id' : project_id,
				'id' : id,
				'parent_id' : parent_id,
				'position' : position
			},
			success: function(r) {
				if (r.status == 'OK') {
					tree_init();
				} else if (r.status == 'BAD_RIGHTS') {
					tree_error_rights();
				} else {
					tree_error();
				}
			},
			error: function(ret) {
				tree_error(data);
			},
			dataType: 'json'
		});
	});
	
	$('#jq-add-page').click(function () {
		$('#jq-menu-add').hide();
		$('.jq-lightbox-add-error').hide();
		xlightbox.show({
			title: 'Добавить страницу',
			content: $('#jq-lightbox-add-page'),
			button_title: 'Добавить страницу',
			success: function() {
				var title = $('#jq-lightbox-add-page-title').val();
				var name = $('#jq-lightbox-add-page-name').val();
				xpost({
					ajax_ctrl_name: 'project_admin_taxonomy_item_add',
					data: {
						type: 'static_page',
						title: title,
						name: name,
						project_id: project_id
					},
					check_fail_callback: function(result){
						$('.jq-lightbox-add-error').hide();
			
						if (result.errors.title == 'BLANK') {
							$('#jq-lightbox-add-page-error-title-blank').show();
						}
						if (result.errors.name == 'BLANK') {
							$('#jq-lightbox-add-page-error-name-blank').show();
						}
						if (result.errors.name == 'USED') {
							$('#jq-lightbox-add-page-error-name-used').show();
							$('#jq-lightbox-add-page-error-name-used-link')
							.attr('href', 
								$('#jq-lightbox-add-page-error-name-used-link')
								.attr('jq-href-template').replace('%NAME%', name)
								);
						}
						if (result.errors.name == 'WRONG') {
							$('#jq-lightbox-add-page-error-name-wrong').show();
						}
					},
					success_callback: function(result) {
						$('.jq-lightbox-add-error').hide();
						location.href = $('#jq-lightbox-add-page-retpath').val().replace('%NAME%', result.name);
					}
				});
				return false;
			}
		});
	});
	
	$('#jq-add-folder').click(function () {
		$('#jq-menu-add').hide();
		$('.jq-lightbox-add-error').hide();
		xlightbox.show({
			title: 'Добавить выпадающее меню',
			content: $('#jq-lightbox-add-folder'),
			button_title: 'Добавить выпадающее меню',
			success: function() {
				var title = $('#jq-lightbox-add-folder-title').val();
				xpost({
					ajax_ctrl_name: 'project_admin_taxonomy_item_add',
					data: {
						type: 'folder',
						title: title,
						project_id: project_id
					},
					check_fail_callback: function(result){
						$('.jq-lightbox-add-error').hide();
						
						if (result.errors.title == 'BLANK') {
							$('#jq-lightbox-add-folder-error-title-blank').show();
						}
					},
					success_callback: function(result) {
						$('.jq-lightbox-add-error').hide();
						location.href = $('#jq-lightbox-add-folder-retpath').val().replace('%ID%', result.id);
					}
				});
				return false;
			}
		});
	});
	
	$('#jq-add-link').click(function () {
		$('#jq-menu-add').hide();
		$('.jq-lightbox-add-error').hide();
		xlightbox.show({
			title: 'Добавить внешнюю ссылку',
			content: $('#jq-lightbox-add-link'),
			button_title: 'Добавить внешнюю ссылку',
			success: function() {
				var title = $('#jq-lightbox-add-link-title').val();
				var url = $('#jq-lightbox-add-link-url').val();
				xpost({
					ajax_ctrl_name: 'project_admin_taxonomy_item_add',
					data: {
						type: 'link',
						title: title,
						url: url,
						project_id: project_id
					},
					check_fail_callback: function(result){
						$('.jq-lightbox-add-error').hide();
					
						if (result.errors.title == 'BLANK') {
							$('#jq-lightbox-add-link-error-title-blank').show();
						}
						if (result.errors.url == 'BLANK') {
							$('#jq-lightbox-add-link-error-url-blank').show();
						}
						if (result.errors.url == 'WRONG') {
							$('#jq-lightbox-add-link-error-url-wrong').show();
						}
					},
					success_callback: function(result) {
						$('.jq-lightbox-add-error').hide();
						location.href = $('#jq-lightbox-add-link-retpath').val().replace('%ID%', result.id);
					}
				});
				return false;
			}
		});
	});
});