$(function() {
	$('#jq-vector-form-future tr').each(function(idx, el) {
		var input = $(':checkbox', el).eq(0);
		var checked = !!$(':checked', el).length;
		if (checked) {
			$(el).addClass('hl-');
		}
		input.click(function() {
			$(el).toggleClass('hl-');
		});
	});
});