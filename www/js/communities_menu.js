$(function() {
	$('a.jq-community-delete').click(function() {
		var $form_el = $('#community_delete_form_' + $(this).attr('data-community-id'));
		xlightbox.show({
			title: 'Подтвердите удаление',
			content: $('#dialog-confirm-delete p'),
			button_title: 'Удалить',
			red: true,
			success: function(options) {
				$form_el.submit();
				return true;
			}
		});
	});
});