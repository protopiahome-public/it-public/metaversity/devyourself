$(function () {
	$('.jq-resource-ignore').hover(
		function () {
			if ($(this).attr('data-ignore') == 0) {
				$(this).removeClass('gray-');
			}
		},
		function () {
			if ($(this).attr('data-ignore') == 0 && !$(this).hasClass('ignore-loading-')) {
				$(this).addClass('gray-');
			}
		}
	);
	$('.jq-resource-ignore').click(function () {
		$(this).removeClass('ignore-').removeClass('gray-').addClass('ignore-loading-');
		
		var button = this;
		
		//$(button).parents('div').find('h3').text($(button).attr('class'));

		$.ajax({
			async: true,
			type: 'POST',
			url: global.ajax_prefix + resource_name + '_ignore/',
			data: {
				resource_id : $(this).attr('data-id'),
				ignore: $(this).attr('data-ignore') == 1 ? 0 : 1
			},
			success: function(r) {
				if (r.status == 'OK') {
					if ($(button).attr('data-remove-on-ignore') == '1') {
						if (r.ignored == 1) {
							$('#jq-resource-head-' + $(button).attr('data-id')).fadeOut(400);
						}
					} else {
						if (r.ignored == 1) {
							$(button).attr('data-ignore', '1')
							$(button).removeClass('ignore-loading-');
							$(button).addClass('ignore-');
						} else {
							$(button).attr('data-ignore', '0')
							$(button).removeClass('ignore-loading-');
							$(button).addClass('ignore-');
							$(button).addClass('gray-');
						}
					}
				} else if (r.status == 'BAD_RIGHTS') {
					show_error('#jq-error-rights');
				} else {
					show_error();
				}
				//$(button).parents('div').find('h3').text($(button).attr('class'));
			},
			error: function(ret) {
				show_error()
			},
			dataType: 'json'
		});
	});
});