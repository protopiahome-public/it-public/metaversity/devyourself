$(function() {
	var $precedent_user_table = $('#jq-precedent-users');
	var $precedent_users_empty = $('#jq-precedent-users-empty');
	var $project_member_table = $('#jq-project-members');
	
	function member_add_remove_click_handler() {		
		var $member_row = $(this).parents('tr[data-user-id]');
		var user_id = $member_row.attr('data-user-id');
		var $precedent_user_row = $precedent_user_table.find('tr[data-user-id = ' + user_id + ']');
		
		if ($precedent_user_row.length) {
			// удаляем пользователя из списка
			precedent_user_remove($precedent_user_row);
		} else {
			// добавляем пользователя в список
			precedent_user_add($member_row);
		}
	}
	
	function precedent_user_remove_click_handler() {
		var $precedent_user_row = $(this).parents('tr[data-user-id]');
		precedent_user_remove($precedent_user_row);
	}
	
	function precedent_user_add($project_member_row) {
		xpost({
			ajax_ctrl_name : 'precedent_edit_users',
			data : {
				action : 'add',
				user_id : $project_member_row.attr('data-user-id')
			},
			success_callback : function(r) {
				var $row;
				$row = $project_member_table.find('tr[data-user-id = ' + r.user_id + ']');
				$row.find('td.jq-add-button span').addClass('added-');
				var h = $precedent_user_table.height();
				$row.clone().appendTo($precedent_user_table);
				$(document).trigger('markup_change');
				var $doc = $(document);
				$doc.scrollTop($doc.scrollTop() + $precedent_user_table.height() - h);
				precedent_user_list_check();
			}		
		});
	}

	function precedent_user_remove($precedent_user_row) {
		var user_id = $precedent_user_row.attr('data-user-id');
		// @ar Поясни, как может появиться этот диалог, если ты отключаешь 
		// кнопку удаления для тех, у кого есть оценки.
		if ($precedent_user_row.hasClass('jq-member-in-use')) {
			xlightbox.show({
				title: 'Произошла ошибка',
				content: '<p>Невозможно удалить пользователя с оценками!</p>',
				button_title: 'OK',
				show_cancel_button: false,
				red: true
			});
			return;
		}
		xpost({
			ajax_ctrl_name : 'precedent_edit_users',
			data : {
				action : 'remove',
				user_id : user_id
			},
			success_callback : function(r) {
				var $row;
				$row = $precedent_user_table.find('tr[data-user-id = ' + r.user_id + ']');
				var h = $precedent_user_table.height();
				$row.remove();
				$project_member_table.find('tr[data-user-id = ' + r.user_id + '] td.jq-add-button span').removeClass('added-');
				var $doc = $(document);
				$doc.scrollTop($doc.scrollTop() + $precedent_user_table.height() - h);	
				precedent_user_list_check();
			}
		});
	}
	
	function precedent_user_list_check(){
		var $user_row_count = $precedent_user_table.find('tr').length - 1;
		if ($user_row_count == 0) {
			$precedent_users_empty.removeClass('dn');
			$precedent_user_table.addClass('dn');
		}
		else {
			$precedent_users_empty.addClass('dn');
			$precedent_user_table.removeClass('dn');
		}
	}
	
	$project_member_table.on('click', 'td.col-add- span', member_add_remove_click_handler);
	$precedent_user_table.on('click', ' td.jq-remove-button a', precedent_user_remove_click_handler);
	
	var search_columns;
	if (precedent_edit_users_params.show_user_numbers){
		search_columns = precedent_edit_users_params.use_game_name
			? [1, 3, 4, 5]
			: [1, 3, 4];
	}
	else {
		search_columns = precedent_edit_users_params.use_game_name
			? [2, 3, 4]
			: [2, 3];
	}
	add_table_filter("jq-precedent-edit-search-user-input", "jq-project-members", 1, search_columns);
});