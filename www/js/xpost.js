/*
 * Example: 
 *	  xpost({
 *	    ajax_ctrl_name : 'test_ctrl',
 *	    data : {name1: 'value1'}, 
 *	    success_callback : function(){}, 
 *	    check_fail_callback: function(){}, 
 *	    error_callback: function(){}, 
 *	    custom_loading_msg : 'Loading...'
 *	  });
 *  
 *  if windows.post_vars is defined, automatically merges data json (second param) with it.
 */

$(function() {
	window.xpost = function (options) { 
		var th = window.xpost;
		th._call.apply(th, arguments);
	}
	
	var th = window.xpost;
	$.extend(th, {
		options_default : {
			ajax_ctrl_name : 'ctrl_was_not_set',
			data : {}, 
			success_callback : function(r, options) {}, 
			check_fail_callback : function(r, options) {}, 
			error_callback: function(r, options) {}, 
			custom_loading_msg : 'Сохранение...'
		},
		_loading_count : 0,
		_loading_show : function(options) {
			if (options.custom_loading_msg === false || options.custom_loading_msg === '') {
				return;
			}
			$('#jq-xpost-loading .content-').text(options.custom_loading_msg);
			$('#jq-xpost-loading').show();
			++th._loading_count;
		},
		_loading_hide : function(options) {
			if (options.custom_loading_msg === false || options.custom_loading_msg === '') {
				return;
			}
			--th._loading_count;
			if (th._loading_count == 0) {
				$('#jq-xpost-loading').hide();
			}
		},
		_call: function(options) {
			options = $.extend({}, th.options_default, options);
			if (typeof(post_vars) != 'undefined') {
				options.data = $.extend({}, post_vars, options.data);
			}
			th._loading_show(options);
			$.ajax({
				async: true,
				type: 'POST',
				url: global.ajax_prefix + options.ajax_ctrl_name + '/',
				data: options.data,
				success: function(r) {
					if (r.status == 'OK') {
						options.success_callback(r, options);
					} else if (r.status == 'BAD_RIGHTS') {
						show_error('#jq-error-rights');
						options.error_callback(r, options);
					} else if (r.error_code == 'CHECK') {
						options.check_fail_callback(r, options);
					} else {
						show_error();
						options.error_callback(r, options);
					}
					th._loading_hide(options);
				},
				error: function(ret) {
					show_error();
					if (typeof(debug) != 'undefined' && debug) {
						ddin(ret.responseText);
					}
					options.error_callback(ret, options);
					th._loading_hide(options);
				},
				dataType: 'json'
			});
		}
	});
});