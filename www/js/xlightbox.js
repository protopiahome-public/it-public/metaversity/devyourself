/*
Lightbox

// Usage example:
$(function () {
	xlightbox.show({
		title: 'Some crazy box',
		content: '<p>Content</p>',
		button_title: 'Save this',
		show_cancel_button: true,
		width: '200px',
		red: false, // true for delete dialogs
		side_padding: '50px', // will be used for 100% width only
		success: function(options) {
			dd(options);
			return true; // 'true' closes the dialog
		},
		close: function () {
			tinymce.remove();
		}
	});
});

*/

window.xlightbox = function() {}
$.extend(window.xlightbox, {
	options_default: {
		content: '',
		title: '',
		button_title: '',
		show_cancel_button: true,
		width: '380px',
		red: false,
		side_padding: 0, // will be used for 100% width only
		top_min_offset: 30,
		success: null,
		close: null
	},
	submit_disabled: false,
	options: {},
	$content_parent: null,
	show: function(options) {
		this.options = $.extend({}, this.options_default, options);
		this._init_elements();
		this._init_events();
		this._init_window();
		this.button_enable();
		this._show_window();
		this._init_keyboard();
	},
	hide: function() {
		this._do_close_window();
	},
	button_disable: function() {
		this.$button.addClass('disable-');
		this.submit_disabled = true;
	},
	button_enable: function() {
		this.$button.removeClass('disable-');
		this.submit_disabled = false;
	},
	set_success: function(callback) {
		this.options.success = callback;
	},
	set_close: function(callback) {
		this.options.close = callback;
	},
	_init_elements: function () {
		this.$window = $(window);
		this.$lightbox = $('#jq-lightbox');
		this.$dialog_box = $('#jq-lightbox-dialog-box');
		this.$dialog = $('#jq-lightbox-dialog');
		this.$title = $('#jq-lightbox-title');
		this.$close = $('#jq-lightbox-close');
		this.$content = $('#jq-lightbox-content');
		this.$button_box = $('#jq-lightbox-button-box');
		this.$button = $('#jq-lightbox-button');
		this.$button_cancel = $('#jq-lightbox-button-cancel');
	},
	_init_events: function () {
		var th = this;
		this.$button.unbind('click').bind('click', function(e) {
			if (th.submit_disabled) {
				return false;
			}
			th.button_disable();
			return th._do_submit.apply(th, arguments)
		});
		this.$close.unbind('click').bind('click', function(e) {
			return th._do_close_window.apply(th, arguments)
		});
		this.$button_cancel.unbind('click').bind('click', function(e) {
			return th._do_close_window.apply(th, arguments)
		});
	},
	_init_window: function () {

	},
	_show_window: function () {
		if (this.options.width == '100%') {
			this.$dialog.css('width', 'auto');
			this.$dialog.css('margin', '0 ' + this.options.side_padding);
		} else {
			this.$dialog.css('width', this.options.width);
			this.$dialog.css('margin', '0 auto');
		}
		this.$dialog.toggleClass('dialog-red-', this.options.red);
		this.$button.toggleClass('button-red', this.options.red);
		this.$button_cancel.toggle(this.options.show_cancel_button);
		this.$title.text(this.options.title);
		this.$content_parent = null;
		if (this.options.content.jquery) {
			this.$content_parent = this.options.content.parent();
			this.$content.empty();
			this.$content.append(this.options.content);
		} else {
			this.$content.html(this.options.content);
		}
		if (this.options.button_title) {
			this.$button_box.show();
			this.$button.text(this.options.button_title);
		} else {
			this.$button_box.hide();
		}
		this.$lightbox.show();
		var top = Math.round((this.$window.height() - this.$dialog_box.height() - 40) / 2);
		this.$dialog_box.css('top',  (top > this.options.top_min_offset ? top : this.options.top_min_offset) + 'px');
		if (top < this.options.top_min_offset) {
			var $autoheight_blocks = this.$content.find('.jq-xlightbox-autoheight');
			if ($autoheight_blocks.length) {
				var height = this.$window.height() - 40 - (this.$content.height() - $autoheight_blocks.height()) - this.$button_box.height() - 2 * this.options.top_min_offset;
				$autoheight_blocks.height(height > 80 ? height : 80);
			}
		}
	},
	_init_keyboard: function () {
		var th = this;
		$('input:text, select', this.$dialog).bind('keydown', function(e) {
			if (e.keyCode == 13 || e.keyCode == 10) { // Enter
				th._do_submit.apply(th, arguments);
				return false;
			}
		});
		$(document).unbind('keydown').bind('keydown', function(e) {
			if (e.keyCode == 27) { // Escape
				th._do_close_window();
				return true;
			}
		});
	},
	_do_submit: function() {
		var ok = true;
		if (this.options.success) {
			ok = this.options.success(this.options);
		}
		if (ok) {
			this._do_close_window();
		} else {
			this.button_enable();
		}
	},
	_do_close_window: function() {
		$(document).unbind('keydown');
		if (this.options.close) {
			this.options.close();
		}
		this.$lightbox.hide();
		if (this.$content_parent) {
			this.$content_parent.append(this.options.content)
		}
	}
});