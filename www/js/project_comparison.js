$(function() {
	// Maximum number of checked projects
	var MAX_CHECKED_PROJECTS = 5;

	// Is there a project comparison form?
	if (!$('#project-cmp').length) {
		return;
	}

	// Rose or bars?
	var is_rose = $('.project-cmp-rose').length > 0;

	// Every changing in the form leads to redraw
	$('#project-cmp input').change(redraw);
	function redraw() {
		/* 1. What is requested */

		// Show all projects as a summation of projects' results
		// or show separate projects
		var show_all = $('#project-cmp-rb-show-projects:checked').length == 0;
		// What projects are selectd
		var show_projects = [];
		var disable_checkboxes = show_all ? "DISABLE_ALL" : "DISABLE_NONE";
		if (!show_all) {
			$('.project-cmp-cb-project:checked').each(function(index) {
				// Index begins from 0
				if (index < MAX_CHECKED_PROJECTS) {
					show_projects[$(this).val()] = index + 1;
				}
				if (index == MAX_CHECKED_PROJECTS - 1) {
					disable_checkboxes = "DISABLE_UNCHECKED";
				}
			});
		}
		// Vector choose
		var vector_id = $('.project-cmp-rb-vector:checked').eq(0).val();
		// Calculations base
		var calculations_base = $('.project-cmp-rb-calc-base:checked').eq(0).val();
		// Show self mark
		var show_self = $('#project-cmp-show-self:checked').length == 1;
		if (calculations_base == 'self') {
			show_self = true;
		}

		/* 2. Changes in the comparison form */

		// Changing legends for projects' checkboxes
		$('.project-cmp-cb-project').each(function() {
			var project_key = $(this).val();
			var legend_img = $('#project-cmp-cb-project-' + project_key + '-label img.legend-bar');
			legend_img.attr('className', 'legend-bar');
			for (var j = 1; j <= MAX_CHECKED_PROJECTS; ++j) {
				legend_img.removeClass('legend-bar-' + (is_rose ? 'rose' : 'bars') + '-case' + j);
			}
			if (show_projects[project_key]) {
				legend_img.addClass('legend-bar-' + (is_rose ? 'rose' : 'bars') + '-case' + show_projects[project_key]);
			}
		});
		// Disabling checkboxes
		if (disable_checkboxes == "DISABLE_NONE") {
			$('.project-cmp-cb-project').removeAttr('disabled');
		} else {
			$('.project-cmp-cb-project').attr('disabled', 'disabled');
			if (disable_checkboxes == "DISABLE_UNCHECKED") {
				$('.project-cmp-cb-project:checked').removeAttr('disabled');
			}
		}
		// Disabling self checkbox
		if (calculations_base) {
			if (calculations_base == 'self') {
				$('#project-cmp-show-self').attr('disabled', 'disabled');
				$('#project-cmp-show-self').attr('checked', 'checked');
			} else {
				$('#project-cmp-show-self').removeAttr('disabled');
			}
		}

		/* 3. Changing the page content */

		if (is_rose) {
			/* Redrawing the image */
			$('.project-cmp-rose').each(function (idx, el) {
				var initial_src = $(el).attr('data-src-initial');
				if (!initial_src) {
					initial_src = $(el).attr('src').replace(/&self=[0-1]/, '');
					$(el).attr('data-src-initial', initial_src);
				}
				var new_src = initial_src;
				for (project_key in show_projects) {
					new_src += '&case' + show_projects[project_key] + '=' + project_key;
				}
				new_src += '&self=' + (show_self ? '1' : '0');
				$(el).attr('src', new_src);
			});
			show_preloaders();
			
		} else {
			/* Showing needed bars */
			// Hiding all project bars
			$('.jq-tree-bars-bar-project').hide();
			// Removing color classes from the project bars
			for (var i = 1; i <= MAX_CHECKED_PROJECTS; ++i) {
				$('.jq-tree-bars-bar-project.type-case' + i + '-').removeClass('type-case' + i + '-');
			}
			// Showing necessary project bars with their corresponding colors
			for (project_key in show_projects) {
				var bar = $('.jq-tree-bars-bar-project-' + project_key);
				bar.addClass('type-case' + show_projects[project_key] + '-');
				bar.show();
			}
			if (vector_id) { // this check is for the case of mistake...
				// Hiding vector bars
				$('.jq-tree-bars-bar-vector').addClass('dn');
				// Showing current vector bar
				$('.jq-tree-bars-bar-vector-' + vector_id).removeClass('dn');
			}
			// Calculations base
			if (calculations_base) {
				$('.jq-tree-bars-bar-calculations-base').hide();
				$('.jq-tree-bars-bar-calculations-base-' + calculations_base).show();
			}
			// Self mark
			$('.jq-tree-bars-bar-self').hide();
			if (show_self) {
				if (calculations_base) {
					$('.jq-tree-bars-bar-self.jq-tree-bars-bar-calculations-base-' + calculations_base).show();
				} else {
					$('.jq-tree-bars-bar-self').show();
				}
			}
		}
	}

	// Image preloader
	$('.project-cmp-rose').load(function () {
		$(this).parent().find('.project-cmp-rose-loading').hide(); // Hide preloader
	})
	function show_preloaders() {
		$('.project-cmp-rose-loading').show(); // Show preloader
	}
});