$(function() {
	$.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());

	head_init();

	// Freeze
	(function freeze() {
		var FROZEN_MARGIN = 3;
		var FREEZE_SELECTOR = '.freeze';
		var $freeze;
		$freeze = $(FREEZE_SELECTOR);
		var frozen = [];
		var recalc = function () {
			var scroll_pos = $(document).scrollTop();
			// Checking for frozen elements which are already removed from DOM
			for (var i in frozen) {
				var $el = frozen[i].$el;
				var $bg = frozen[i].$bg;
				var $dot = frozen[i].$dot;
				if (!$el.parent().length) {
					// element is removed from DOM
					$bg.remove();
					$dot.remove();
					frozen.splice(i, 1);
				}
			}
			// Freeze elements
			$freeze.each(function (idx, el) {
				var $el = $(el);
				if ($el.hasClass('frozen')) return;
				var pos = $el.offset().top;
				if (scroll_pos >= pos - FROZEN_MARGIN) {
					var $bg = $('<div></div>');
					$bg.css('position', 'fixed');
					$bg.css('top', '0px');
					$bg.css('z-index', '90');
					$bg.css('background-color', $el.attr('frozen-bg-color') || '#fff');
					$bg.css('border-bottom', '1px solid ' + ($el.attr('frozen-border-color') || '#eee'));
					$bg.css('width', $(window).width() + 'px');
					$bg.css('height', (Number($el.height()) + 2 * FROZEN_MARGIN) + 'px');
					$bg.appendTo('body');
					var $dot = $('<div></div>');
					$dot.css('margin-top', $el.css('marginTop'));
					$dot.css('margin-bottom', $el.css('marginBottom'));
					$dot.css('position', $el.css('position'));
					$dot.css('height', $el.height() + 'px');
					//$dot.css('z-index', '100');
					//$dot.css('width', '100px');
					//$dot.css('height', '100px');
					//$dot.css('background', 'red');
					$dot.insertBefore($el);
					$el.addClass('frozen');
					var old_css = {
						position: $el.css('position'),
						top: $el.css('top'),
						z_index: $el.css('z-index'),
						margin_top: $el.css('marginTop')
					}
					$el.css('position', 'fixed');
					$el.css('top', '0px');
					$el.css('z-index', '99');
					$el.css('margin-top', FROZEN_MARGIN + 'px');
					frozen.push({
						$el: $el,
						$bg: $bg,
						$dot: $dot,
						old_css: old_css
					});
				}
			});
			// Unfreeze elements
			for (var i in frozen) {
				var $el = frozen[i].$el;
				var $bg = frozen[i].$bg;
				var $dot = frozen[i].$dot;
				var old_css = frozen[i].old_css;
				var pos = $dot.offset().top;
				//$('button.button').parent().append('(' + $el.hasClass('freeze') + ')');
				if (!$el.hasClass('freeze') || scroll_pos < pos - FROZEN_MARGIN) {
					$el.css('position', old_css.position);
					$el.css('top', old_css.top);
					$el.css('z-index', old_css.z_index);
					$el.css('margin-top', old_css.margin_top);
					$el.removeClass('frozen');
					$bg.remove();
					$dot.remove();
					frozen.splice(i, 1);
				}
			}
		}
		recalc();
		$(window).scroll(recalc);
		window.freeze_refresh = function() {
			$freeze = $(FREEZE_SELECTOR);
			recalc();
		};
	/*$(document).unbind('keydown').bind('keydown', function(e) {
			if (e.keyCode == 27) { // Escape
				for (var i in frozen) {
					dd(frozen[i].old_css);
				}
			}
		});*/
	})();

	// PIE.js
	function refresh_pie() {
		if (window.PIE) {
			var selector = '';
			selector += '.button, .button-join, .button-add, ';
			selector += '.widget, .widget .header-, .widget .header- table, .widget .buttons- .button-, ';
			selector += '.columns-wrap-community .widget .header- td, .columns-wrap-community .widget-wrap, ';
			selector += '#project-head .bottom- .menu- .item-inner-, ';
			selector += '#community-head .bottom- .menu- .item-inner-, ';
			selector += '.box, .box-light, ';
			$(selector).each(function() {
				PIE.attach(this);
			});
		}
	}
	refresh_pie();

	$('input').placeholder({
		attr: 'jq-placeholder',
		blankSubmit: true
	});

	$('button').click(function () {
		if ($(this).hasClass('jq-manual-submit')) return true;
		var $form_el = $(this).parents('form');
		var name = $(this).attr('name');
		if (name) {
			$(this).attr('name-processed', name);
			$(this).removeAttr('name');
		} else {
			name = $(this).attr('name-processed');
		}
		$('.button-autoadded-param').remove();
		if (name) {
			$form_el.prepend('<input class="button-autoadded-param" type="hidden" name="' + name + '" value=""/>');
		}
		$form_el.eq(0).submit();
		return false;
	});

	// DD menu in project
	var $opened_dd_menu = null;
	$('.jq-dd-menu-link').click(function () {
		var dd_menu_id = $(this).attr('data-dd-menu-id');
		var $link = $(this);
		var $first_link_in_menu = $link.parents('.jq-menu').children().first();
		var $dd_menu = $('#' + dd_menu_id);
		if ($dd_menu.is($opened_dd_menu)) {
			$dd_menu.hide();
			$opened_dd_menu = null;
			return false;
		}
		if ($opened_dd_menu) {
			$opened_dd_menu.hide();
		}
		$dd_menu.find('a').unbind('click').bind('click', function () {
			$opened_dd_menu.hide();
			$opened_dd_menu = null;
			return true;
		});
		$opened_dd_menu = $dd_menu;
		$dd_menu.css('left', $link.offset().left - $first_link_in_menu.offset().left - 10);
		$dd_menu.show();
	});

	// Toggle
	(window.jq_toggle_init = function() {
		$('.toggle').each(function (idx, el) {
			var toggle_id = $(el).attr('jq-toggle');
			var toggle_group_class = $(el).attr('jq-toggle-group');

			var toggle_active_element = $(el).attr('jq-toggle-active-element');
			var toggle_active_class = $(el).attr('jq-toggle-active-class');

			if (toggle_id) {
				$(el).click(function () {
					var toggle_state = null;
					if (toggle_group_class) {
						if ($('#' + toggle_id + ':visible').length) {
							toggle_state = false;
						}
						$('.' + toggle_group_class).toggle(false);

						if(toggle_active_element && toggle_active_class) {
							$('.toggle[jq-toggle-group=' + toggle_group_class + ']')
							.attr('jq-toggle-active-element', toggle_id)
							.toggleClass(toggle_active_class, false);
							$(el).toggleClass(toggle_active_class, toggle_state);
						}
					}
					$('#' + toggle_id).toggle(toggle_state);
					return false;
				});
				$(el).removeAttr('jq-toggle');
			}
		});
	})();
	
	// Join lightbox
	if ($('#jq-join-form').length) {
		$('#jq-button-join').click(function () {
			xlightbox.show({
				title: 'Присоединение к проекту',
				content: $('#jq-join-form'),
				button_title: 'Присоединиться к проекту',
				show_cancel_button: true,
				success: function () {
					$('#jq-join-game-name').val($('#jq-game-name-input').val());
					$('#jq-button-join').parents('form').submit();
					return true;
				}
			});
			return false;
		});
	}

	// Checkbox bulk checker
	$('.check-all').each(function (idx, el) {
		var $el = $(el);
		var children_class = $el.attr('data-check-all-class');
		if (children_class) {
			var children_selector = '.' + children_class;
			var $children = $(children_selector);
			$el.click(function() {
				if ($el.attr('checked')) {
					$children.attr('checked', 'checked');
				} else {
					$children.removeAttr('checked', 'checked');
				}
			});
			$children.click(function() {
				var checked_count = $(children_selector + ':checked').length;
				var all_checked = checked_count == $children.length;
				if (all_checked) {
					$el.attr('checked', 'checked');
				} else {
					$el.removeAttr('checked', 'checked');
				}
			});
		}
	});

	$('.box p:last, .box p:only-child').css('marginBottom', 0);

	/* Superbox (lightboxes) */
	/*$.superbox.settings = {
		boxId: 'superbox', // Id attribute of the 'superbox' element
		boxClasses: '', // Class of the 'superbox' element
		overlayOpacity: .6, // Background opaqueness
		boxWidth: '724', // Default width of the box
		boxHeight: '460', // Default height of the box
		loadTxt: lang['Loading'] + '<br/><img src="' + lang['Loading_img_src'] + '" alt=""/>', // Loading text
		closeTxt: lang['Close'], // 'Close' button text
		prevTxt: 'Previous', // 'Previous' button text
		nextTxt: 'Next' // 'Next' button text
	};
	$.superbox();*/

	window.show_error = function (expr) {
		xlightbox.show({
			title: 'Произошла ошибка',
			content: $((expr || '#jq-error') + ' p'),
			button_title: 'OK',
			show_cancel_button: false,
			red: true
		});
	}

	// Navigation collapse
	/*
	var nav_hide = $.cookie('nav_hide') == '1';
	if (nav_hide) {
		var widget = $('.widget-headed-collapse');
		$('.border- .m-, .border- .b-', widget).toggle();
		$('.border- .t- .c- .button- div', widget).toggleClass('collapsed-');
	}
	$('.widget-headed-collapse .border- .t-').click(function() {
		var widget = $(this).parents('.widget-headed-collapse');
		$('.border- .m-, .border- .b-', widget).toggle();
		$('.border- .t- .c- .button- div', widget).toggleClass('collapsed-');
		$.cookie('nav_hide', nav_hide ? null : '1', {
			path: global.prefix
		});
	});
	*/

	// Adjust width
	$('#communities-menu').adjust([{
		el: 'li',
		css: 'paddingLeft',
		init: 10,
		min: 4
	}, {
		el: 'li',
		css: 'fontSize',
		init: 12,
		min: 11
	}]);

	$('.intmenu .list-').adjust([{
		set: [{
			el: 'li.item-',
			css: 'paddingRight',
			init: 10,
			min: 2
		}, {
			el: 'li.item-',
			css: 'paddingLeft',
			init: 10,
			min: 2
		}]
	}, {
		el: 'li.item-',
		css: 'fontSize',
		init: 12,
		min: 11
	}]);

	var project_head = $('#project-head');
	if(project_head.length > 0) {
		$('.menu-', project_head).adjust([{
			el: '.item-',
			css: 'marginRight',
			init: 4,
			min: 0
		}, {
			rotate: [{
				el: '.item-inner-',
				css: 'fontSize',
				init: 14,
				min: 11
			}, {
				set: [{
					el: '.item-inner-',
					css: 'paddingRight',
					init: 7,
					min: 2
				}, {
					el: '.item-inner-',
					css: 'paddingLeft',
					init: 7,
					min: 2
				}]
			}]
		}]);

		var title_margin_right = $('.status-', project_head).width() | $('.button-', project_head).width() + 6 | 0;
		$('.title-wrap-', project_head).adjust([{
			el: '.title-',
			css: 'marginRight',
			init: title_margin_right + 5,
			min: title_margin_right + 2
		}, {
			rotate: [{
				el: '.title-',
				css: 'fontSize',
				init: 24,
				min: 12
			}]
		}]);
	}

	var community_head = $('#community-head');
	if(community_head.length > 0) {
		$('.menu-', community_head).adjust([{
			el: '.item-',
			css: 'marginRight',
			init: 4,
			min: 0
		}, {
			rotate: [{
				el: '.item-inner-',
				css: 'fontSize',
				init: 14,
				min: 11
			}, {
				set: [{
					el: '.item-inner-',
					css: 'paddingRight',
					init: 7,
					min: 2
				}, {
					el: '.item-inner-',
					css: 'paddingLeft',
					init: 7,
					min: 2
				}]
			}]
		}]);

		var title_wrap_wide = $('.title-wrap-wide-', community_head);
		var status_width = $('.status-', community_head).width();
		var button_width = $('.button-', community_head).width() + 6;

		if(title_wrap_wide.length > 0) {
			$('.title-wrap-', community_head).adjust([{
				el: '.title-',
				css: 'marginRight',
				init: (button_width | 0) + 6,
				min: button_width| 0
			}, {
				rotate: [{
					el: '.title-',
					css: 'fontSize',
					init: 18,
					min: 12
				}]
			}]);

			title_wrap_wide
			.css('marginRight', status_width + 6);
		} else {
			$('.title-wrap-', community_head).adjust([{
				el: '.title-',
				css: 'marginRight',
				init: (button_width | status_width | 0) + 6,
				min: button_width | status_width | 0
			}, {
				rotate: [{
					el: '.title-',
					css: 'fontSize',
					init: 18,
					min: 12
				}]
			}]);
		}
	}




	// Emails - decoding
	$('a.email').each(function(idx, el) {
		$(this).text($(this).text().replace(/\ssobaka\s/g, '@').replace(/\stochka\s/g, '.'));
		$(this).attr('href', $(this).attr('href').replace(/%20sobaka%20/g, '@').replace(/%20tochka%20/g, '.'));
	});

	// Filter
	$('#filter_main .subtitle- .clickable-').click(function() {
		$('.expandee-', $(this).parents(".translator-")).toggle();
	});

	// Tree expanders
	$('.tree .group- .col-group-title- span').click(function () {
		var group_id = $('q', this).text();
		$('.tree .jq-tree-group-' + group_id + '-row').toggle();
		$('.tree .jq-group-' + group_id + ' .jq-tree-group-head').toggleClass('head-collapsed-');
		$('.tree .jq-tree-group-' + group_id + '-children').toggle();
	});
	$('#tree-expand').click(function() {
		$('.tree .jq-tree-group-competence').show();
		$('.tree .jq-tree-group-head').removeClass('head-collapsed-');
		$('.tree .jq-tree-group-children').show();
	});
	$('#tree-collapse').click(function() {
		$('.tree .jq-tree-group-competence').hide();
		$('.tree .jq-tree-group-head').addClass('head-collapsed-');
		$('.tree .jq-tree-group-children').hide();
	});
	$('#tree-groups').click(function() {
		$('.tree .jq-tree-group-competence').show();
		$('.tree .jq-tree-group-head').removeClass('head-collapsed-');
		$('.tree .jq-tree-group-children').show();
		$('.tree .jq-tree-group-children').each(function () {
			if (!$('.jq-tree-group-children table', this).length) {
				var group_id = $('q', this).text();
				$('.tree .jq-tree-group-' + group_id + '-row').hide();
				$('.tree .jq-group-' + group_id + ' .jq-tree-group-head').addClass('head-collapsed-');
				$('.tree .jq-tree-group-' + group_id + '-children').hide();
			}
		});
	});

	// Search form examples
	$('.search-form .example- span').click(function () {
		var span = $(this);
		$('input', span.parents('.el-')).removeClass('placeholder').val(span.text());
		span.parents('form').submit();
	});

	// Markup change
	function markup_change() {
		jq_toggle_init();
		freeze_refresh();
		$(document.body).mouse_hints();
		refresh_pie();
	}
	$(document).bind('markup_change', {}, markup_change);
});