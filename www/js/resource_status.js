$(function() {
	(function resource_status_init() {
		$('.statuses-').find('.jq-button').each(function() {
			$(this).click(function() {
				var status_form = $(this).parents('.statuses-');
				if (status_form.hasClass('statuses-disabled-')) {
					return false;
				}
				var id = status_form.attr('data-id');
				status_form.addClass('statuses-disabled-').addClass('transparent');
				$.ajax({
					async: true,
					type: 'POST',
					url: global.ajax_prefix + resource_name + '_status/',
					data: {
						resource_id : id,
						status_type : $(this).attr('data-status-type'),
						status: $(this).attr('data-status'),
						freeze: $(this).parents('.statuses-').hasClass('freeze') ? 1 : 0
					},
					success: function(r) {
						if (r.status == 'OK') {
							if (resource_name == 'material') {
								var status_form_header = $('.statuses-.header-[data-id = ' + id + ']');
								status_form_header.replaceWith(r.header_html);
								var status_form_footer = $('.statuses-.footer-[data-id = ' + id + ']');
								if (status_form_footer.length) {
									status_form_footer.replaceWith(r.footer_html);
								}
							} else {
								status_form.replaceWith(r.html);
							}
							resource_status_init();
							jq_toggle_init();
							freeze_refresh();
						} else {
							show_error();
							status_form.removeClass('statuses-disabled-').removeClass('transparent');
						}
					},
					error: function(ret) {
						show_error()
						status_form.removeClass('statuses-disabled-').removeClass('transparent');
					},
					dataType: 'json'
				});
				return false;
			})
		});
	})();
});