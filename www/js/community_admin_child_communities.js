$(function() {
	$('.jq-delete-button').click(function() {
		var $form_el = $(this).parents('form');
		$('#jq-delete-community-id').val($(this).attr('data-community-id'));
		$('#jq-dialog-confirm-title').text($(this).attr('data-community-title'));
		xlightbox.show({
			title: 'Подтвердите отсоединение',
			content: $('#jq-dialog-confirm p'),
			button_title: 'Отсоединить',
			red: true,
			success: function(options) {
				$form_el.submit();
				return true;
			}
		});
		return false;
	});
});