function delete_dialog(remove_function) {
	xlightbox.show({
		title: 'Подтвердите удаление',
		content: $('#dialog-confirm-delete p'),
		button_title: 'Удалить',
		red: true,
		success: function(options) {
			remove_function();
			return true;
		}
	});
}

$(function () {
	function tree_error(data) {
		data.inst.refresh();
		show_error();
	}
	function tree_error_rights(data) {
		data.inst.refresh();
		show_error('#jq-error-rights');
	}
	$.jstree._themes = global.prefix + "img/jstree/themes/";
	$('#tree_button_add_group span').click(function() {
		if ($(this).parent().hasClass('disabled-')) {
			return;
		}
		$('#jq-tree').jstree('create', null, 'last', {
			'data': {
				'title': 'Новый раздел'
			},
			'attr': {
				'rel' : 'group'
			}
		});
	});
	$('#tree_button_edit span').click(function() {
		if ($(this).parent().hasClass('disabled-')) {
			return;
		}
		$('#jq-tree').jstree('rename');
	});
	$('#tree_button_delete span').click(function() {
		if ($(this).parent().hasClass('disabled-')) {
			return;
		}
		delete_dialog(function() {
			$('#jq-tree').jstree('remove');
		});
	});
	$("#jq-tree").jstree({
		// List of active plugins
		"plugins" : ["themes","json_data","ui","crrm","cookies","dnd","search","types","hotkeys","contextmenu"],
		"dnd": {
			"copy_modifier": false,
			'drag_target': false,
			'drop_target': false
		},
		"json_data" : { 
			"ajax" : {
				"url" : tree_ajax_url,
				// the `data` function is executed in the instance's scope
				// the parameter is the node being loaded
				// (may be -1, 0, or undefined when loading the root nodes)
				"data" : function (n) { 
					// the result is fed to the AJAX request `data` option
					return { 
						"operation" : "get_nodes", 
						"project_id" : project_id,
						"id" : n.attr ? n.attr("id").replace(/[a-z\_]/g,"") : "-" 
					}; 
				}
			}
		},
		// Configuring the search plugin
		/*"search" : {
			// As this has been a common question - async search
			// Same as above - the `ajax` config option is actually jQuery's
			// AJAX object
			"ajax" : {
				"url" : tree_ajax_url,
				// You get the search string as a parameter
				"data" : function (str) {
					return { 
						"operation" : "search", 
						"search_str" : str 
					}; 
				}
			}
		},*/
		// Using types - most of the time this is an overkill
		// read the docs carefully to decide whether you need types
		"types" : {
			// I set both options to -2, as I do not need depth and children count checking
			// Those two checks may slow jstree a lot, so use only when needed
			"max_depth" : -2,
			"max_children" : 1,
			// I want only `drive` nodes to be root nodes
			// This will prevent moving or creating any other type as a root node
			"valid_children" : ["group"],
			"types" : {
				
				// The `folder` type
				"group" : {
					"valid_children" : ["group"]
				}
			}
		},
		// UI & core - the nodes to initially select and open will be
		// overwritten by the cookie plugin

		// the UI plugin - it handles selecting/deselecting/hovering nodes
		"ui" : {
			select_limit: 1
		},
		// the core plugin - not many options here
		"core" : { 
			
		},
		"themes": {
			theme: "competence-set"
		},
		"contextmenu": {
			"items" : function (node) {
				var can_create_group = true;
				var can_create_item = true;
					
				if ($(node).attr("rel") == "root") {
					can_create_item = false;
					can_create_group = true;
				} else if($(node).attr("rel") == "competence") {
					can_create_item = false;
					can_create_group = false;
				} else {
					node.children("ul").children("li").each(function() {
						if ($(this).attr("rel") == "group") {
							can_create_item = false;
						}
						if ($(this).attr("rel") == "competence") {
							can_create_group = false;
						}
					});
				}
				return {
					"create_group" : {
						"label" : "Создать раздел",
						"action" : function (obj) {
							this.create(obj, "last", {
								"data": {
									"title": "Новый раздел"
								},
								"attr": {
									"rel" : "group"
								}
							});
						},
						"_disabled" : !can_create_group
					},
					"rename" : {
						"label" : "Переименовать",
						"action" : function (obj) {
							this.rename(obj);
						},
						"_disabled" : $(node).attr("rel") == "root"
					},
					"delete" : {
						"label" : "Удалить",
						"action" : function (obj) {
							var th = this;
							delete_dialog(function() {
								th.remove(obj);
							})
						},
						"_disabled" : $(node).attr("rel") == "root"
					}
				}
			}
		}
	})
	.bind("create.jstree", function (e, data) {
		var operation = "";
		if (data.rslt.obj.attr("rel") == "group") {
			operation = "create_group";
		}
		if (data.rslt.obj.attr("rel") == "competence") {
			operation = "create_item";
		}
		var post_data = {
			"operation" : operation,
			"project_id" : project_id,
			"group_id" : data.rslt.parent.attr("id").replace(/[a-z\_]/g, ""),
			"position" : data.rslt.position,
			"title" : data.rslt.name,
			"type" : data.rslt.obj.attr("rel")
		};
		$.ajax({
			type: 'POST',
			url: tree_ajax_url,
			data: post_data,
			success: function(r) {
				if (r.status == "OK") {
					$(data.rslt.obj).attr("id", "node_" + r.id);
					$('a', data.rslt.obj).text(r.title);
					$('a', data.rslt.obj).prepend('<ins class="jstree-icon">&nbsp;</ins>');
				} else if (r.status == "BAD_RIGHTS") {
					tree_error_rights(data);
				} else {
					tree_error(data);
				}
			},
			error: function(ret) {
				tree_error(data);
			},
			dataType: 'json'
		});
	})
	.bind("remove.jstree", function (e, data) {
		data.rslt.obj.each(function () {
			if (data.rslt.obj.attr("rel") == "group") {
				var operation = "delete_group";
			}
			if (data.rslt.obj.attr("rel") == "competence") {
				var operation = "delete_item";
			}
			var post_data = { 
				"operation" : operation, 
				"project_id" : project_id,
				"id" : this.id.replace(/[a-z\_]/g,"")
			};
			$.ajax({
				async: false,
				type: 'POST',
				url: tree_ajax_url,
				data: post_data,
				success: function(r) {
					if (r.status == "OK") {
					// Do nothing
					} else if (r.status == "BAD_RIGHTS") {
						tree_error_rights(data);
					} else {
						tree_error(data);
					}
				},
				error: function(ret) {
					tree_error(data);
				},
				dataType: 'json'
			});
		});
	})
	.bind("rename.jstree", function (e, data) {
		if (data.rslt.obj.attr("rel") == "group") {
			var operation = "rename_group";
		}
		if (data.rslt.obj.attr("rel") == "competence") {
			var operation = "rename_item";
		}
		var post_data = {
			"operation" : operation,
			"project_id" : project_id,
			"id" : data.rslt.obj.attr("id").replace(/[a-z\_]/g,""),
			"title" : data.rslt.new_name
		};
		$.ajax({
			type: 'POST',
			url: tree_ajax_url,
			data: post_data,
			success: function(r) {
				if (r.status == "OK") {
					$('a', data.rslt.obj).eq(0).text(r.title);
					$('a', data.rslt.obj).eq(0).prepend('<ins class="jstree-icon">&nbsp;</ins>');
				} else if (r.status == "BAD_RIGHTS") {
					tree_error_rights(data);
				} else {
					tree_error(data);
				}
			},
			error: function(ret) {
				tree_error(data);
			},
			dataType: 'json'
		});
	})
	.bind("select_node.jstree", function (e, data) {
		var can_create_group = true;
		var can_create_item = true;
		var can_delete = true;
		var can_edit = true;
	
		var node = data.rslt.obj;
		if ($(node).attr("rel") == "root") {
			can_create_item = false;
			can_create_group = true;
			can_edit = false;
			can_delete = false;
		}
		else if ($(node).attr("rel") == "competence") {
			can_create_item = false;
			can_create_group = false;
		}
		else {
			node.children("ul").children("li").each(function() {
				if($(this).attr("rel") == "group") {
					can_create_item = false;
				}
				if($(this).attr("rel") == "competence") {
					can_create_group = false;
				}
			});
		}
		$("#tree_button_add_group").toggleClass("disabled-", !can_create_group);
		$("#tree_button_add_item").toggleClass("disabled-", !can_create_item);
		$("#tree_button_edit").toggleClass("disabled-", !can_edit);
		$("#tree_button_delete").toggleClass("disabled-", !can_delete);
	})
	.bind("move_node.jstree", function (e, data) {
		data.rslt.o.each(function (i) {
			if (i > 0) {
				return;
			}
			
			var position, operation;
			
			if (data.rslt.cp > data.rslt.cop && data.rslt.op.attr("id") == data.rslt.np.attr("id")) {
				position = data.rslt.cp - 1;
			} else {
				position = data.rslt.cp;
			}
			var can_create_group = true;
			var can_create_item = true;
			if (data.rslt.np.attr("rel") == "root") {
				can_create_item = false;
				can_create_group = true;
			} else if(data.rslt.np.attr("rel") == "competence") {
				can_create_item = false;
				can_create_group = false;
			} else {
				data.rslt.np.children("ul").children("li").each(function() {
					if ($(this).attr("rel") == "group") {
						can_create_item = false;
					}
					if ($(this).attr("rel") == "competence") {
						can_create_group = false;
					}
				});
			}
			
			if ($(this).attr("rel") == "group") {
				if (!can_create_group) {
					$.jstree.rollback(data.rlbk);
					return false;
				}
				
				operation = "move_group";
			}
			if ($(this).attr("rel") == "competence") {
				if (!can_create_item) {
					$.jstree.rollback(data.rlbk);
					return false;
				}
				
				operation = "move_item";
			}
			
			$.ajax({
				async: false,
				type: 'POST',
				url: tree_ajax_url,
				data : {
					"operation" : operation, 
					"project_id" : project_id,
					"id" : $(this).attr("id").replace(/[a-z\_]/g, ""),
					"old_group_id" : data.rslt.op.attr("id").replace(/[a-z\_]/g, ""),
					"group_id" : data.rslt.np.attr("id").replace(/[a-z\_]/g, ""),
					"position" : position,
					"title" : data.rslt.name,
					"copy" : data.rslt.cy ? 1 : 0
				},
				success: function(r) {
					if (r.status == "OK") {
					// Do nothing
					} else if (r.status == "BAD_RIGHTS") {
						tree_error_rights(data);
					} else {
						tree_error(data);
					}
				},
				error: function(ret) {
					tree_error(data);
				},
				dataType: 'json'
			});
		});
	});
});