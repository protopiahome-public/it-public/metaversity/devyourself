$(function() {
	window.widget_text_edit_init = function(){
		var tinymce_config = {
			script_url: global.prefix + 'editors/tiny_mce/tiny_mce.js',
			theme: 'advanced',
			language: 'ru',
			content_css: global.prefix + 'css/editor.css',
			plugins: 'lists,autolink,advimage,inlinepopups,media,contextmenu,paste,fullscreen,nonbreaking,xhtmlxtras',
			theme_advanced_buttons1: 'advhr,bold,italic,underline,strikethrough,|,fontsizeselect,|,sub,sup,nonbreaking,charmap,|,forecolor,backcolor,|,undo,redo,|,fullscreen,code',
			theme_advanced_buttons2: 'justifyleft,justifycenter,justifyright,|,bullist,numlist,|,outdent,indent,blockquote,hr,|,link,unlink,anchor,image,media,|,pastetext,pasteword,removeformat,cleanup,spellchecker',
			theme_advanced_buttons3: '',
			theme_advanced_buttons4: '',
			theme_advanced_toolbar_location: 'top',
			theme_advanced_toolbar_align: 'left',
			theme_advanced_statusbar_location: 'bottom',
			theme_advanced_resizing: true,
			force_p_newlines : true,
			fix_list_elements : true,
			convert_fonts_to_spans: true,
			font_size_style_values: '10px,11px,12px,14px,16px,18px,20px',
			invalid_elements: 'h1,h2,h4,h5,h6',
			relative_urls: false,
			remove_script_host: true,
			spellchecker_languages: '+Русский=ru,English=en',
			setup : function(editor) {
				editor.onInit.add(function(editor){
					tinyMCE.execCommand('mceFocus', false, 'widget_html');
				});
			},
			theme_advanced_resize_horizontal: 0
		};
		tinyMCE.init(tinymce_config);
		tinyMCE.execCommand('mceAddControl', false, 'widget_html');

		xlightbox.set_success(widget_text_edit);
		xlightbox.set_close(function(){
			var ed = tinyMCE.get('widget_html');
			if (ed) {
				ed.setProgressState(0);
				ed.remove();
			}
		});
		return false;
	}

	function widget_text_edit() {
		var ed = tinyMCE.get('widget_html');
		var data = {
			widget_id: $('#jq-widget-edit-id').val(),
			html: ed.getContent()
		}
		ed.setProgressState(1);
		xpost({
			ajax_ctrl_name: widget_container_type + '_widget_text_edit', 
			data: data, 
			success_callback: function (data) {
				xlightbox.hide();
				$('#widget-' + data.widget_id).html(data.html);
				widget_reinit_events();
				widget_edit_mode_on();
			}, 
			error_callback: function(){
				xlightbox.button_enable();
			}
		});
		
		return true;
	}
	
	window.widget_lister_edit_init = function(){
		xlightbox.set_success(widget_lister_edit);
		
		return false;
	}

	function widget_lister_edit() {
		var count = $('#jq-widget-lister-item-count').val();
		if (!count.match(/^[1-9][0-9]?$/) || count < 1 || count > 50) {
			alert('Неправильное количество! Введите число от 1 до 50.');
			return false;
		}
		var data = {
			widget_id: $('#jq-widget-edit-id').val(),
			count: count
		}
		xpost({
			ajax_ctrl_name: widget_container_type + '_widget_lister_edit', 
			data: data, 
			success_callback: function (data) {
				xlightbox.hide();
				$('#widget-' + data.widget_id).html(data.html);
				widget_reinit_events();
				widget_edit_mode_on();
			}, 
			error_callback: function(){
				xlightbox.button_enable();
			}
		});
		
		return true;
	}
	
	window.widget_communities_edit_init = function(){
		xlightbox.set_success(widget_communities_edit);
		$('#jq-communities-not-selected').sortable({
			appendTo: 'body',
			revert: false,
			scroll: true,
			helper: 'original',
			handle: '.jq-community, .jq-move-handle',
			connectWith: '#jq-communities-selected',
			update: function() {
				$('#jq-communities-not-selected .jq-community').sort(function (a, b) {
					var contentA = $(a).attr('jq-sort-data');
					var contentB = $(b).attr('jq-sort-data');
					return (contentA > contentB) ? 1 : -1;
				}).appendTo($('#jq-communities-not-selected'));
			},
			tolerance: 'pointer'
		});
		$('#jq-communities-selected').sortable({
			appendTo: 'body',
			revert: false,
			scroll: true,
			helper: 'original',
			handle: '.jq-community, .jq-move-handle',
			connectWith: '.jq-communities',
			tolerance: 'pointer'
		});
		return false;
	}

	function widget_communities_edit() {
		var communities_selected = new Array();
		$('#jq-communities-selected .jq-community').each(function() {
			communities_selected.push($(this).attr('jq-community-id'));
		});
		var data = {
			widget_id: $('#jq-widget-edit-id').val(),
			communities_selected: communities_selected
		}
		xpost({
			ajax_ctrl_name: widget_container_type + '_widget_communities_edit', 
			data: data, 
			success_callback: function (data) {
				xlightbox.hide();
				$('#widget-' + data.widget_id).html(data.html);
				widget_reinit_events();
				widget_edit_mode_on();
			}, 
			error_callback: function(){
				xlightbox.button_enable();
			}
		});
		
		return true;
	}
	
	window.widget_custom_feed_edit_init = function(){
		xlightbox.set_success(widget_custom_feed_edit);
		
		return false;
	}

	function widget_custom_feed_edit() {
		if ($('#jq-widget-edit-no-feeds').val() == 1) {
			return true;
		}
		var selected_feed_id = $('#jq-widget-feed-selected-id').val();
		var count = $('#jq-widget-lister-item-count').val();
		if (!count.match(/^[1-9][0-9]?$/) || count < 1 || count > 50) {
			alert('Неправильное количество! Введите число от 1 до 50.');
			return false;
		}
		var data = {
			widget_id: $('#jq-widget-edit-id').val(),
			selected_feed_id: selected_feed_id,
			count: count
		}
		xpost({
			ajax_ctrl_name: widget_container_type + '_widget_custom_feed_edit', 
			data: data, 
			success_callback: function (data) {
				xlightbox.hide();
				$('#widget-' + data.widget_id).html(data.html);
				widget_reinit_events();
				widget_edit_mode_on();
			}, 
			error_callback: function(){
				xlightbox.button_enable();
			}
		});
		
		return true;
	}
});