$(function() {
	
	/* Deps */
	
	var DEP_ACTION_SHOW = 1;
	var DEP_ACTION_ENABLE = 2;
	var DEP_ACTION_IMPORTANT = 4;
	var DEP_ACTION_DO_NOT_STORE = 8;
	
	function get_inputs_selector(field_name) {
		var selector = '';
		var inputs = ['input', 'select', 'textarea'];
		for (var input_idx in inputs) {
			selector += (field_name ? '#field-' + field_name : '.field') + ' ' + inputs[input_idx] + ', ';
		}
		return selector;
	}
	function dt_update_deps(field_name) {
		var value = null;
		if ($('#f-' + field_name + ':checkbox').length) {
			value = $('#f-' + field_name + ':checked').length ? 1 : 0;
		}
		for (var i in dt_deps) {
			if (dt_deps[i].depends_from == field_name) {
				var selector;
				if (dt_deps[i].actions & DEP_ACTION_ENABLE) {
					selector = get_inputs_selector(dt_deps[i].dependee);
					if (value == dt_deps[i].activation_value) {
						$(selector).removeAttr('disabled');
					} else {
						$(selector).attr('disabled', 'disabled');
					}
				}
				if (dt_deps[i].actions & DEP_ACTION_IMPORTANT) {
					selector = '#field-' + dt_deps[i].dependee + ' .star';
					$(selector).toggle(value == dt_deps[i].activation_value);
				}
				if (dt_deps[i].actions & DEP_ACTION_SHOW) {
					selector = '#field-' + dt_deps[i].dependee;
					$(selector).toggle(value == dt_deps[i].activation_value);
				}
			}
		}
	}
	
	var for_events = {};
	if (typeof(dt_deps) != 'undefined') {
		for (var i in dt_deps) {
			var key = '_' + dt_deps[i].depends_from;
			if (!for_events[key]) {
				dt_update_deps(dt_deps[i].depends_from);
				$(get_inputs_selector(dt_deps[i].depends_from)).change(function(field_name) {
					return function() {
						dt_update_deps(field_name);
					}
				}(dt_deps[i].depends_from));
				for_events[key] = true;
			}
		}
	}
	
	/* Access deps */
	
	$('.field[jq-type=access_level] select').change(function() {
		window.dt_access_deps = window.dt_access_deps || [];
		for (var i in window.dt_access_deps) {
			var $restrict_select = $('#f-' + window.dt_access_deps[i].depends_from);
			var restrict_level = $restrict_select.val();
			var $select = $('#f-' + window.dt_access_deps[i].field);
			var current_level = $select.val();
			var $option = null;
			var option_level = null;
			// disabling of options
			$select.find('option').each(function() {
				$option = $(this);
				option_level = $option.attr('value');
				if (option_level >= restrict_level) {
					$option.removeAttr('disabled');
				} else {
					$option.attr('disabled', 'disabled');
				}
			});
			// changing current option
			if (current_level < restrict_level) {
				var is_option_fixed = false;
				$select.find('option').each(function() {
					$option = $(this);
					option_level = $option.attr('value');
					if (!is_option_fixed && option_level >= restrict_level) {
						$select.val(option_level);
						is_option_fixed = true;
					}
				});
				if (!is_option_fixed && option_level) {
					$select.val(option_level);
				}
			}
		}
	});
	$('.field[jq-type=access_level] select').change();
	
	/* Datepicker */
	
	$('.field .datepicker-').each(function (idx, el) {
		var field_el = $(el).parents('.field');
		var name = field_el.attr('jq-name');
		var type = field_el.attr('jq-type');
		var year_el = $('#f-' + name + '_year');
		var min_year = $('option:first', year_el).attr('value');
		var max_year = $('option:last', year_el).attr('value');
		function get_current_value() {
			var value = $('#f-' + name + '_year').val() + '-' + $('#f-' + name + '_month').val() + '-' + $('#f-' + name + '_day').val();
			return value;
		}
		$('#f-' + name + '-datepicker').val(get_current_value());
		$('#f-' + name + '-datepicker').datepicker({
			showOn: "button",
			buttonImage: global.prefix + "img/calendar.png",
			buttonImageOnly: true,
			showOtherMonths: true,
			selectOtherMonths: true,
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: min_year + ':' + max_year,
			minDate: min_year + '-01-01',
			maxDate: max_year + '-12-31'
		});
		$('#f-' + name + '-datepicker').change(function () {
			$('#f-' + name + '_year').val(this.value.substr(0, 4));
			$('#f-' + name + '_month').val(this.value.substr(5, 2));
			$('#f-' + name + '_day').val(this.value.substr(8, 2));
		});
		$('#f-' + name + '_year' + ',' + '#f-' + name + '_month' + ',' + '#f-' + name + '_day').change(function () {
			$('#f-' + name + '-datepicker').datepicker("setDate" , get_current_value());
		});
	});
	
	/* Datepicker Enabler */
	
	$('.field .enabler- span').click(function () {
		var $enabler = $(this).parents('.enabler-');
		var $field = $enabler.parents('.field');
		var $content = $field.find('.content-');
		var name = $field.attr('jq-name');
		$enabler.hide();
		$content.show();
		if ($field.attr('jq-type') == 'datetime') {
			var later_than_name = $field.attr('jq-later-than');
			if (later_than_name) {
				var value = $('#f-' + later_than_name + '-datepicker').val();
				$('#f-' + name + '-datepicker').val(value);
				$('#f-' + name + '-datepicker').change();
				$('#f-' + name + '-datepicker').datepicker("setDate" , value);
				$('#f-' + name + '_hours').val($('#f-' + later_than_name + '_hours').val());
				$('#f-' + name + '_minutes').val($('#f-' + later_than_name + '_minutes'));
			}
		}
		$('#f-' + name + '_hide').val('0');
	});
	
	$('.field .disabler- span').click(function () {
		var $disabler = $(this).parents('.disabler-');
		var $field = $disabler.parents('.field');
		var $enabler = $field.find('.enabler-');
		var $content = $field.find('.content-');
		var name = $field.attr('jq-name');
		$enabler.show();
		$content.hide();
		$('#f-' + name + '_hide').val('1');
	});

	/* WYSWYG */
	
	if ($.fn.tinymce) {
		var tinymce_config_full = {
			script_url: global.prefix + 'editors/tiny_mce/tiny_mce.js',
			theme: 'advanced',
			language: 'ru',
			content_css: global.prefix + 'css/editor.css',
			plugins: 'lists,autolink,advimage,inlinepopups,media,contextmenu,paste,fullscreen,nonbreaking,xhtmlxtras',
			theme_advanced_buttons1: 'advhr,bold,italic,underline,strikethrough,|,fontsizeselect,|,sub,sup,nonbreaking,charmap,|,forecolor,backcolor,|,undo,redo,|,fullscreen,code',
			theme_advanced_buttons2: 'justifyleft,justifycenter,justifyright,|,bullist,numlist,|,outdent,indent,blockquote,hr,|,link,unlink,anchor,image,media,|,pastetext,pasteword,removeformat,cleanup,spellchecker',
			theme_advanced_buttons3: '',
			theme_advanced_buttons4: '',
			theme_advanced_toolbar_location: 'top',
			theme_advanced_toolbar_align: 'left',
			theme_advanced_statusbar_location: 'bottom',
			theme_advanced_resizing: true,
			force_p_newlines : true,
			fix_list_elements : true,
			convert_fonts_to_spans: true,
			font_size_style_values: '10px,11px,12px,14px,16px,18px,20px',
			invalid_elements: 'h1,h2,h4,h5,h6',
			relative_urls: false,
			remove_script_host: true,
			spellchecker_languages: '+Русский=ru,English=en'
		}
		var tinymce_config_short = $.extend(true, {}, tinymce_config_full);
		tinymce_config_short.theme_advanced_buttons1 = 'bold,italic,underline,strikethrough,|,forecolor,backcolor,|,bullist,numlist,|,link,unlink,image,media,|,undo,redo,|,spellchecker,code';
		tinymce_config_short.theme_advanced_buttons2 = '';
		var tinymce_config_full_cut = $.extend(true, {}, tinymce_config_full);
		var tinymce_config_short_cut = $.extend(true, {}, tinymce_config_short);
		tinymce_config_full_cut.plugins += ',postcut';
		tinymce_config_full_cut.theme_advanced_buttons1 += ',|,postcut';
		tinymce_config_short_cut.plugins += ',postcut';
		tinymce_config_short_cut.theme_advanced_buttons1 += ',|,postcut';
		function tinymce($textarea, config) {
			var body_class = $textarea.attr('data-body-class');
			var final_config = $.extend(true, {}, config);
			if (body_class) {
				final_config.body_class = body_class;
			}
			$textarea.tinymce(final_config);
		}
		tinymce($('.xhtml-editor-full'), tinymce_config_full);
		tinymce($('.xhtml-editor-full-cut'), tinymce_config_full_cut);
		tinymce($('.xhtml-editor-short'), tinymce_config_short);
		tinymce($('.xhtml-editor-short-cut'), tinymce_config_short_cut);
	}
	
	/* Multi link */
	
	function multi_link_change(element, only_check) {
		var ancestors = $(element).attr('jq-ancestors').split(',');
		var descendants = $(element).attr('jq-descendants').split(',');

		if (element.checked) {
			for (var key in ancestors) {
				if (ancestors[key]){
					$('#f-' + $(element).attr('jq-dtf-name') + '-' + ancestors[key]).attr('checked', 'checked');
				}
			}
		}

		if (!element.checked && !only_check) {
			for (var key in descendants) {
				if (descendants[key]) {
					$('#f-' + $(element).attr('jq-dtf-name') + '-' + descendants[key]).removeAttr('checked');
				}
			}
		}

		return true;
	}

	$('.jq-dtf-multi-link-item').each(function() {
		$(this).change(function() {
			multi_link_change(this, false);
		})
		multi_link_change(this, true);
	});
});