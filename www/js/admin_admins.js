$(function() {
	$('.jq-delete-button').click(function() {
		var $form_el = $(this).parents('form');
		$('#jq-dialog-confirm-login').text($(this).attr('data-user-login'));
		xlightbox.show({
			title: 'Подтвердите удаление',
			content: $('#jq-dialog-confirm p'),
			button_title: 'Удалить',
			red: true,
			success: function(options) {
				$form_el.submit();
				return true;
			}
		});
	});
});