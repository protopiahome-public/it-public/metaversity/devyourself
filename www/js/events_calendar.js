$(function() {
	(window.events_calendar_init = function() {
		var HIDE_DD_DELAY = 200;
		var remove_hover_timeout = null;
		var hide_dd_timeout = null;
		var button_hover_status = false;
		var dd_hover_status = false;
		function reload_calendar(year, month) {
			$.getJSON(
				events_calendar_ajax_url +
				events_calendar_request_params + 
				"&year=" + year +
				"&month=" + month,
				null,
				function (data) {
					events_calendar_current_year = data.year;
					events_calendar_current_month = data.month;
					$('#jq-calendar').replaceWith(data.html);
					if (window.PIE) {
						$('.box-light').each(function() {
							PIE.attach(this);
						});
					}
					events_calendar_init();
				});
		}
		function show_events(year, month, day) {
			$.getJSON(
				events_day_list_ajax_url  +
				events_calendar_request_params +
				"&year=" + year +
				"&month=" + month +
				"&day=" + day,
				null,
				function(data) {
					$('#jq-calendar-day-details .content-').css('height', 'auto');
					$('#jq-calendar-day-details .content-').html(data.html);
					$('#jq-calendar-day-details .content-').removeClass('content-loading-');
					if (window.PIE) {
						$('.calendar-day .close-').each(function() {
							PIE.attach(this);
						});
					}
					$('#jq-calendar-day-details .jq-close').click(function () {
						$('#jq-calendar-day-details').fadeOut(180);
					});
				});
			$('#jq-calendar-day-details .content-inner-').css('visibility', 'hidden');
			if ($('#jq-calendar-day-details').height() < 10) {
				$('#jq-calendar-day-details .content-').height('100px');
			}
			$('#jq-calendar-day-details .content-').addClass('content-loading-');
			$('#jq-calendar-day-details').show();
		}
		function hide_dd() {
			$('.jq-calendar-dd').hide();
		}
		if (!$('head').data('calendar_no_list')){
			$('a.jq-calendar-day').click(function() {
				show_events($(this).attr('data-year'), $(this).attr('data-month'), $(this).attr('data-day'));
				return false;
			});
		}
		$('.widget-calendar .date- td').hover(
			function() {
				button_hover_status = true;
				clearTimeout(hide_dd_timeout);
				clearTimeout(remove_hover_timeout);
				$('td', $(this).parents('table')).removeClass('arr-hover-');
				$('td', $(this).parents('table')).removeClass('link-hover-');
				var type = '';
				if ($(this).hasClass('month-')) {
					type = 'month';
				}
				if ($(this).hasClass('year-')) {
					type = 'year';
				}
				if (type) {
					$('td.' + type + '-', $(this).parents('table')).each(function (idx, el) {
						if ($(el).hasClass('arr-')) {
							$(el).addClass('arr-hover-');
						}
						if ($(el).hasClass('link-')) {
							$(el).addClass('link-hover-');
						}
					});
				}
			},
			function() {
				button_hover_status = false;
				var th = this;
				remove_hover_timeout = setTimeout(function () {
					$('td', $(th).parents('table')).removeClass('arr-hover-');
					$('td', $(th).parents('table')).removeClass('link-hover-');
				}, 10);
				if (!dd_hover_status) {
					hide_dd_timeout = setTimeout(hide_dd, HIDE_DD_DELAY);
				}
			}
			);
		$('.jq-calendar-dd .item-').hover(
			function () {
				$(this).addClass('item-hover-');
			},
			function () {
				$(this).removeClass('item-hover-');
			}
			);
		$('.jq-calendar-dd').hover(
			function () {
				dd_hover_status = true;
				clearTimeout(hide_dd_timeout);
			},
			function () {
				dd_hover_status = false;
				if (!button_hover_status) {
					hide_dd_timeout = setTimeout(hide_dd, HIDE_DD_DELAY);
				}
			}
			);
		$('.jq-calendar-dd .item-').click(function () {
			var y = events_calendar_current_year;
			var m = events_calendar_current_month;
			if ($(this).attr('data-month')) {
				m = $(this).attr('data-month');
			}
			if ($(this).attr('data-year')) {
				y = $(this).attr('data-year');
			}
			reload_calendar(y, m);
			hide_dd();
		});
		$('#jq-calendar-call-month-dd').click(function () {
			hide_dd();
			$('#jq-calendar-month-dd').show();
		});
		$('#jq-calendar-call-year-dd').click(function () {
			hide_dd();
			$('#jq-calendar-year-dd').show();
		});
		$('#jq-calendar-prev, #jq-calendar-next').click(function() {
			reload_calendar($(this).attr('data-year'), $(this).attr('data-month'));
		});
	})();
});