$.getScript(global.prefix + 'js/block_set_dialogs.js');
$.getScript(global.prefix + 'js/json2.js');

$(function () {
	var tinymce_config = {
		script_url: global.prefix + 'editors/tiny_mce/tiny_mce.js',
		theme: 'advanced',
		language: 'ru',
		//content_css: global.prefix + 'css/editor-blocks.css',
		content_css: '<style type="text/css">\
body, html {padding: 0; margin: 0;}\
body {background-color: #FFF; background-image: none; text-align: left;}\
body, td {font-family: Arial, Tahoma, Verdana, sans-serif; font-size: 12px; color: #000;}\
img {border: 0;}\
td, img, input, select, label {vertical-align: middle;}\
p {text-align: left; padding: 0; margin: 0 0 10px; line-height: 19px;}\
p.last {margin: 0;}\
a {color: #000; text-decoration: underline;}\
.extlink {padding-right: 14px; background: url(../img/extlink.gif) no-repeat 100% 50%}\
ul {margin: 0; padding: 0;}\
ol {margin: 0; padding: 0 0 0 4px;}\
li {padding: 0; margin: 0 0 10px 15px; text-align: left; line-height: 19px;}\
li ul {margin: 10px 0 0 0;}\
li ol {margin: 10px 0 0 0;}\
ul li {list-style-type: none; list-style-image: url(../img/li.gif);}\
ol li {list-style-type: decimal;}\
p {margin-bottom: 12px; font-size: 14px; line-height: 22px;}\
li {margin-bottom: 12px; font-size: 14px; line-height: 22px;}\
li ul {margin-top: 12px;}\
li ol {margin-top: 12px;}\
</style>',
		plugins: 'lists,autolink,advimage,inlinepopups,media,contextmenu,paste,fullscreen,nonbreaking,xhtmlxtras,autoresize',
		theme_advanced_buttons1: 'bold,italic,underline,strikethrough,|,forecolor,backcolor,|,bullist,numlist,outdent,indent,|,link,unlink,image,media,|,sub,sup,|,undo,redo,|,code',
		theme_advanced_buttons2: '',//pastetext,pasteword,removeformat,cleanup
		theme_advanced_buttons3: '',
		theme_advanced_buttons4: '',
		theme_advanced_toolbar_location: 'external',
		theme_advanced_toolbar_align: 'left',
		theme_advanced_resizing: true,
		theme_advanced_statusbar_location: 'none',
		theme_advanced_path: false,
		theme_advanced_resize_horizontal: 0,
		force_p_newlines : true,
		fix_list_elements : true,
		convert_fonts_to_spans: true,
		font_size_style_values: '10px,11px,12px,14px,16px,18px,20px',
		invalid_elements: 'h1,h2,h4,h5,h6',
		relative_urls: false,
		remove_script_host: true,
		spellchecker_languages: '+Русский=ru,English=en'
	}

	var editor_state = {
		STATE_START: 0,
		STATE_PRELOADED: 1,
		STATE_HIDDEN: 2,
		STATE_LOADING: 3,
		STATE_SHOWN: 4,
		state: 0,
		once_initiated: false,
		editor: null,
		toolbar_clicked: false,
		$blocks_to_remove: null
	}
	
	function scroll_to($selector) {
		$.scrollTo($selector, 800, {
			offset: {
				top: -60, 
				left: 0
			}
		});
	}
	
	function text_editor_init(editor) {
		if (editor_state.state == editor_state.STATE_START) {
			editor_state.state = editor_state.STATE_PRELOADED;
			editor_state.editor = editor;

			// Show editor if no blocks
			if($('.jq-block-set-blocks .jq-block').length == 0){
				setTimeout(function() {
					block_text_add(null, true);
				}, 10);
			}
			return;
		}
		
		// Show toolbar
		editor.onMouseUp.dispatch(editor);
		
		// Setup freeze
		if (!$.browser.msie || $.browser.version >= 8) { // I'm too lazy to fix it in IE7
			$('#' + editor.id + '_toolbargroup').addClass('freeze');
			$('#' + editor.id + '_toolbargroup').attr('frozen-bg-color', '#f0f0ee');
			$('#' + editor.id + '_toolbargroup').attr('frozen-border-color', '#ccc');
			$('#jq-block-set-buttons').removeClass('freeze');
			freeze_refresh();
		}
		
		if (editor_state.$blocks_to_remove) {
			editor_state.$blocks_to_remove.remove();
			editor_state.$blocks_to_remove = null;
		}
		
		// Setup toolbar events
		$('#' + editor.id + '_external').mousedown(function() {
			editor_state.toolbar_clicked = true;
		});
		$('.mceExternalClose').click(function() {
			toolbar_clicked = false;
			text_editor_remove();
		});
		
		$(document.getElementById(editor.id + '_ifr').contentWindow.document.body).focus();

		editor_state.toolbar_clicked = false;
		editor_state.editor = editor;
		editor_state.state = editor_state.STATE_SHOWN;
		
		return;
	}
	
	function text_editor_init_once() {
		if (editor_state.once_initiated) return;
		editor_state.once_initiated = true;
		
		$(document.body).mousedown(text_editor_body_event);
		$(document.body).keydown(text_editor_body_event);
		
		$('#jq-block-set-save-btn').mousedown(function() {
			editor_state.toolbar_clicked = true;
		});
	}
	
	function text_editor_body_event() {
		if (editor_state.state != editor_state.STATE_SHOWN) {
			return true;
		}
		// #mceModalBlocker:visible - this is out of the toolbar, it comes in 
		// the body. So we need to check it here.
		if (!editor_state.toolbar_clicked && !$('#mceModalBlocker:visible').length) {
			return !text_editor_remove();
		} else {
			editor_state.toolbar_clicked = false;
			return true;
		}
	}
	
	function text_editor_remove() {
		if (editor_state.state != editor_state.STATE_SHOWN) return false;
		
		var $block_text_editor = $('#jq-block-text-editor');
		var $temp_container = $('#jq-block-set-temp-container');
		$temp_container.html(editor_state.editor.getContent());
		
		var $new_block;
		var $last_new_block = $block_text_editor;
		$temp_container.children().each(function(){
			if (!$(this).html().match(/^(\xA0|\x20|\x09|\x0A|\x0D|&nbsp;)*$/)) {
				$new_block = $('.jq-block-text-empty').clone();
				$new_block.removeClass('jq-block-text-empty');
				$new_block.find('.jq-block-content').html($(this));
				$new_block.insertAfter($last_new_block);
				$new_block.show();
				$new_block.removeClass('dn');
				$last_new_block = $new_block;
			}
		});
		
		if (!$new_block && $('.jq-block-set-blocks .jq-block').length == 0) {
			return false;
		}
		
		// Freeze setup
		if (!$.browser.msie || $.browser.version >= 8) {
			$('#' + editor_state.editor.id + '_toolbargroup').removeClass('freeze'); // this must be done before toolbar destroy
			$('#jq-block-set-buttons').addClass('freeze');
			freeze_refresh();
		}
		
		editor_state.editor.remove();
		editor_state.editor = null;
		editor_state.state = editor_state.STATE_HIDDEN;
		
		$temp_container.html('');
		$block_text_editor.addClass('preload');
		$block_text_editor.insertAfter($('.jq-block-set-blocks'));
		
		reinit_local_events();
		
		return true;
	}
	
	function on_after_block_add() {
		text_editor_remove();
	}
	
	function block_text_add(e, no_scroll) {
		if (editor_state.state == editor_state.STATE_SHOWN) {
			$(document.getElementById(editor_state.editor.id + '_ifr').contentWindow.document.body).focus();
			return;
		}
		var $new_block = $('.jq-block-text-empty').clone();
		$new_block.removeClass('jq-block-text-empty');
		$new_block.find('.jq-block-content').html('');
		$new_block.appendTo('.jq-block-set-blocks');
		$new_block.show();
		$new_block.removeClass('dn');
		reinit_local_events();
		if (!no_scroll) {
			scroll_to($new_block);
		}
		$new_block.find('.jq-block-content').click();
	}
	
	function block_text_edit($block) {
		if (editor_state.state != editor_state.STATE_PRELOADED && editor_state.state != editor_state.STATE_HIDDEN) return;
		
		var $block_text_editor = $('#jq-block-text-editor');
		var $textarea = $('#jq-block-text-editor-textarea');
		if (editor_state.editor) {
			editor_state.editor.remove();
		}
		
		var $blocks_to_edit;
		$blocks_to_edit = $block.prevUntil(':not(.jq-block-type-text)')
		.add($block)
		.add($block.nextUntil(':not(.jq-block-type-text)'));
		/*$blocks_to_edit.find(".jq-block-content > *").removeClass('no-margin-');*/
		var html = '';
		$blocks_to_edit.find(".jq-block-content").each(function() {
			html += $(this).html();
		});
		$textarea.val(html);
		//var height = $blocks_to_edit.last().offset().top - $blocks_to_edit.first().offset().top + $blocks_to_edit.last().height() - 50;
		var height = 10;
		
		$block_text_editor.insertBefore($blocks_to_edit.first());
		$block_text_editor.removeClass("preload");
		editor_state.$blocks_to_remove = $blocks_to_edit;
		
		text_editor_init_once();
		editor_state.state = editor_state.STATE_LOADING;
		$textarea.tinymce($.extend({}, tinymce_config, {
			height: height
		}));
	}
	
	function block_cut_add() {
		if ($('.jq-block-set-blocks .jq-block-type-cut').length == 0) {
			var $new_block = $('.jq-block-cut-empty').clone();
			$new_block.removeClass('jq-block-cut-empty');
			if ($('.jq-block-set-blocks .jq-block').length > 0) {
				($('.jq-block-set-blocks .jq-block').eq(0)).after($new_block);
			} else {
				$new_block.appendTo('.jq-block-set-blocks');
			}
			$new_block.show();
			$new_block.removeClass('dn');
			reinit_local_events();
			on_after_block_add();
			scroll_to($new_block);
		} else {
			window.block_set_dialogs.error_dialog.show({
				title: 'Некорректное действие',
				message: 'Больше одного раза обрезать текст нельзя.'
			});
		}
	}

	function block_cut_edit($block) {
		var values = {
			cut_title: $block.attr('data-title')
		}
		var callback = function(data) {
			$block.find('.jq-cut-title').text(data.cut_title);
			$block.attr('data-title', data.cut_title);
		}
		window.block_set_dialogs.cut_dialog.show(null, values, callback);
	}
	
	function block_video_add() {
		var options = {};
		var post_vars = $.extend({}, block_set_post_vars, {
			block_id: 0
		});
		var callback = function(data) {
			$('.jq-block-set-blocks').append(data.html);
			reinit_local_events();
			on_after_block_add();
			scroll_to($('.jq-block-set-blocks > .block-').last());
		}
		window.block_set_dialogs.video_dialog.show(block_set_ajax_url, options, null, post_vars, callback);
	}
	
	function block_video_edit($block) {
		var values = {
			video_url: $block.attr('data-video-url'),
			video_code: $block.attr('data-video-code'),
			video_title: $block.attr('data-video-title')
		};
		var options = {};
		// see block_set_post_vars in material_edit.xslt
		var post_vars = $.extend({}, block_set_post_vars, {
			block_id: $block.attr('data-block-id'),
			block_fake_for_id: $block.attr('data-block-fake-for-id')
		});
		var callback = function(data) {
			var $new_block = $(data.html);
			$new_block.replaceAll($block);
			reinit_local_events();
		}
		window.block_set_dialogs.video_dialog.show(block_set_ajax_url, options, values, post_vars, callback);
	}
	
	function block_file_add() {
		var options = {
			max_file_size: 3 * 1024 * 1024,
			session_cookie_name: global.session_cookie_name
		}
		var post_vars = $.extend({}, block_set_post_vars, {
			block_id: 0
		});
		var callback = function(data) {
			$('.jq-block-set-blocks').append(data.html);
			reinit_local_events();
			on_after_block_add();
			scroll_to($('.jq-block-set-blocks > .block-').last());
		}
		window.block_set_dialogs.file_dialog.show(block_set_ajax_url, options, null, post_vars, callback);
	}
	
	function block_file_edit($block) {
		var values = {
			file_title: $block.attr('data-title'),
			file_name: $block.attr('data-file-name'),
			file_size: $block.attr('data-size')
		}
		var options = {
			max_file_size: 3 * 1024 * 1024,
			session_cookie_name: global.session_cookie_name
		}
		// see block_set_post_vars in material_edit.xslt
		var post_vars = $.extend({}, block_set_post_vars, {
			block_id: $block.attr('data-block-id'),
			block_fake_for_id: $block.attr('data-block-fake-for-id')
		});

		var callback = function(data) {
			var $new_block = $(data.html);
			$new_block.replaceAll($block);
			reinit_local_events();
		}
		window.block_set_dialogs.file_dialog.show(block_set_ajax_url, options, values, post_vars, callback);
	}
	
	function block_delete($block) {
		$block.fadeOut(300, function() {
			$(this).remove();
		});
		reinit_local_events();
	}
	
	function reinit_local_events() {
		(function init_hovers() {
			$('.jq-block-set-blocks .jq-block-to-init').hover(function () {
				$('.controls-', this).show();
				$('.content-', this).addClass('content-hover-');
			}, function () {
				$('.controls-', this).hide();
				$('.content-', this).removeClass('content-hover-');
			});
		})();
	
		(function init_block_text_editing(){
			$('.jq-block-set-blocks .jq-block-to-init.jq-block-type-text .jq-block-content').click(function() {
				block_text_edit($(this).parents('.jq-block'));
			});
		})();
		
		(function init_controls() {
			$('.jq-block-set-blocks .jq-block-to-init.jq-block-type-text .controls- .edit-').click(function() {
				$(this).parents('.jq-block').find('.jq-block-content').click();
				return false;
			})
			$('.jq-block-set-blocks .jq-block-to-init.jq-block-type-file .controls- .edit-').click(function() {
				block_file_edit($(this).parents('.jq-block'));
				return false;
			})
			$('.jq-block-set-blocks .jq-block-to-init.jq-block-type-video .controls- .edit-').click(function() {
				block_video_edit($(this).parents('.jq-block'));
				return false;
			})
			$('.jq-block-set-blocks .jq-block-to-init.jq-block-type-cut .controls- .edit-').click(function() {
				block_cut_edit($(this).parents('.jq-block'));
				return false;
			})
			$('.jq-block-set-blocks .jq-block-to-init .delete-').click(function() {
				block_delete($(this).parents('.jq-block'));
				return false;
			})
		})();
		
		/*$('.jq-block-set-blocks .jq-block-to-init').each(function (idx, el) {
			$(el).find('.jq-block-content > :last').addClass('no-margin-');
		})*/
		$('.jq-block-set-blocks .jq-block-to-init').removeClass('jq-block-to-init');
		
		$('.jq-block-set-blocks').sortable('refresh');
	}
	
	function init_global_events() {
		/* Text editor preload */
		tinymce_config.setup = function(editor) {
			editor.onInit.add(text_editor_init);
		}
		$('#jq-block-text-editor-textarea').tinymce(tinymce_config);
		
		/* Submit callback */
		$('#block_set_data').parents('form').submit(function() {
			text_editor_remove();
			var data = [];
			$('.jq-block-set-blocks .jq-block').each(function() {
				if ($(this).attr('data-block-type') == 'text') {
					data.push({
						type: 'text',
						html: $(this).find('.jq-block-content').html()
					});
				}else if ($(this).attr('data-block-type') == 'cut') {
					data.push({
						type: 'cut',
						title: $(this).attr('data-title')
					});
				} else {
					data.push({
						type: $(this).attr('data-block-type'),
						id: $(this).attr('data-block-id')
					});
				}
			});
			$('#block_set_data').val(JSON.stringify(data));
		});
		
		/* Sortable setup */
		$('.jq-block-set-blocks').sortable({
			handle : '.jq-move-handler',
			tolerance: 'pointer',
			revert: 100
		});
		
		/* Buttons */
		$('#jq-block-set-btn-text').click(block_text_add);
		$('#jq-block-set-btn-video').click(block_video_add);
		$('#jq-block-set-btn-file').click(block_file_add);
		$('#jq-block-set-btn-cut').click(block_cut_add);
	}

	init_global_events();
	reinit_local_events();
	
});