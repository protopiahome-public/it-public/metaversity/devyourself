$(function(){
	function on_ratee_checkbox_click() {
		$this = $(this);
		xpost({
			ajax_ctrl_name: 'precedent_edit_flash_group', 
			data: {
				method : this.checked ? 'flash_group_ratee_add' : 'flash_group_ratee_remove',
				user_id : $this.attr('data-user-id'),
				flash_group_id : $this.attr('data-flash-group-id')
			}
		});
	}
	
	function on_flash_group_add_click() {
		var flash_group_title;
		flash_group_title = prompt('Как назвать флэш-группу?', 'Без названия');
		
		if (!flash_group_title) {
			return;
		}
		
		xpost({
			ajax_ctrl_name: 'precedent_edit_flash_group', 
			data: {
				method : 'flash_group_create',
				title : flash_group_title
			},
			success_callback: function (data) {
				location.reload();
			}
		});
	}
	
	function on_flash_group_edit_click() {
		var $this = $(this);
		var $title = $('#jq-flash-group-title-' + $this.parent().attr('data-flash-group-id'));
		var title_text = prompt('Переименовать группу', $title.text());
		
		if (!title_text) {
			return;
		}
		
		$title.text(title_text);
		
		xpost({
			ajax_ctrl_name: 'precedent_edit_flash_group', 
			data: {
				method : 'flash_group_rename',
				// @ar .parent() - это запрещённый приём, так как завязан на вёрстку.
				// Верстальщик не должен думать о том, что-то сломает, добавив контейнер.
				// Нужно использовать parents('.jq-something')
				// или хотя бы .parents('[data-flash-group-id]')
				// Нужно переделать по всему файлу (файлам).
				flash_group_id : $this.parent().attr('data-flash-group-id'),
				title : title_text
			}
		});
	}
	
	function on_flash_group_delete_click() {
		$this = $(this);
		var flash_group_id = $this.parent().attr('data-flash-group-id');
		var flash_group_title = $('#jq-flash-group-title-' + flash_group_id).text();
		$('#jq-dialog-confirm-flash-group-title').text(flash_group_title);
		xlightbox.show({
			title: 'Подтвердите удаление',
			content: $('#jq-dialog-confirm p'),
			button_title: 'Удалить',
			red: true,
			success: function(options) {
				xpost({
					ajax_ctrl_name: 'precedent_edit_flash_group', 
					data: {
						method : 'flash_group_delete',
						flash_group_id : flash_group_id
					},
					success_callback: function (data) {
						location.reload();
					}
				});
				return true;
			}
		});
	}
	
	$('#jq-flash-groups').on('click', 'input.jq-user-selection', on_ratee_checkbox_click);
	$('.jq-flash-group-add').on('click', on_flash_group_add_click);
	$('.jq-flash-group-edit').on('click', on_flash_group_edit_click);
	$('.jq-flash-group-delete').on('click', on_flash_group_delete_click);
});