$(function() {
	function sync() {
		var data = {
			order: $('#jq-sort').sortable('serialize')
		}
		
		xpost({
			ajax_ctrl_name : sort_ajax_ctrl,
			data : data, 
			success_callback :function(r) {}
		});
	}
	
	$('#jq-sort').sortable({
		handle : '.sort-handler',
		update : sync
	});
});