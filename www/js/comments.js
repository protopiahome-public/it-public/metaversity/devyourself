$(function () {
	var editor_config = {
		script_url: global.prefix + 'editors/tiny_mce/tiny_mce.js',
		theme: 'advanced',
		language: 'ru',
		content_css: global.prefix + 'css/editor.css',
		plugins: 'lists,autolink,advimage,inlinepopups,media,contextmenu,paste,fullscreen,nonbreaking,xhtmlxtras',
		theme_advanced_buttons1: 'bold,italic,underline,strikethrough,|,forecolor,backcolor,|,bullist,numlist,|,link,unlink,image,media,|,undo,redo,|,spellchecker,code',
		theme_advanced_buttons2: '',
		theme_advanced_buttons3: '',
		theme_advanced_buttons4: '',
		theme_advanced_toolbar_location: 'top',
		theme_advanced_toolbar_align: 'left',
		theme_advanced_statusbar_location: 'bottom',
		theme_advanced_resizing: true,
		theme_advanced_resize_horizontal: 0,
		force_p_newlines : true,
		fix_list_elements : true,
		convert_fonts_to_spans: true,
		font_size_style_values: '10px,11px,12px,14px,16px,18px,20px',
		invalid_elements: 'h1,h2,h4,h5,h6',
		relative_urls: false,
		remove_script_host: true,
		spellchecker_languages: '+Русский=ru,English=en'
	}
	var $reply_form_inner = $('#jq-comment-reply-form-inner');
	var reply_editor = null;
	var main_editor = null;
	var $current_comment = null;
	var in_progress = false;

	var main_editor_config = $.extend({}, editor_config, {
		setup: function(editor) {
			editor.onInit.add(function(editor){
				main_editor = editor;
			});
		}
	});
	$('#jq-comment-main-reply').find('textarea').tinymce(main_editor_config);

	function reply_editor_init(editor) {
		$(document.getElementById(editor.id + '_ifr').contentWindow.document.body).focus();
		reply_editor = editor;
	}
	
	function reply_toggle () {
		if (in_progress) return;
		if (reply_editor) {
			reply_editor.remove();
			reply_editor = null;
		}
		if ($current_comment) {
			$current_comment.removeClass('comment-bordered-');
			$current_comment.find('.icons-small').show();
		}
		var $comment = $(this).parents('.jq-comment');
		if ($comment.is($current_comment)) {
			$reply_form_inner.hide();
			$current_comment = null;
		} else {
			$comment.addClass('comment-bordered-');
			$comment.find('.icons-small').hide();
			$current_comment = $comment;
			var $form = $comment.find('.jq-comment-reply-form');
			$reply_form_inner.detach().appendTo($form).show();
			var config = $.extend({}, editor_config, {
				setup: function(editor) {
					editor.onInit.add(reply_editor_init);
				}
			})
			$reply_form_inner.find('textarea').tinymce(config);
		}
	}
	
	function delete_comment(set_delete){
		var $comment = $(this).parents('.jq-comment');
		var $comments = $comment.parents('#jq-comments');

		var type = $comments.attr('data-type');
		var object_id = $comments.attr('data-object-id');
		var id = $comment.attr('data-id');

		var data = {
			type : type,
			object_id : object_id,
			'delete' : set_delete,
			id: id
		};
		
		$.extend(data, comments_post_vars);

		$.ajax({
			async: true,
			type: 'POST',
			url: global.ajax_prefix + comment_delete_ctrl + '/',
			data: data,
			success: ajax_delete_success,
			error: ajax_delete_error,
			dataType: 'json'
		});
		
		return false;
	}
	
	function subscribe_toggle() {
		var $comments = $(this).parents('#jq-comments');
		var set_subscribe = $(this).attr('data-subscribe');
		var type = $comments.attr('data-type');
		var object_id = $comments.attr('data-object-id');
		
		var data = {
			type : type,
			object_id : object_id,
			subscribe : set_subscribe
		};
		
		$.extend(data, comments_post_vars);

		$.ajax({
			async: true,
			type: 'POST',
			url: global.ajax_prefix + comments_subscribe_ctrl + '/',
			data: data,
			success: ajax_subscribe_success,
			error: ajax_subscribe_error,
			dataType: 'json'
		});
		
		return false;
	}
	
	function ajax_add_success(r, editor) {
		if (r.status == 'OK') {
			if(r.empty_html == 1){
				editor.setProgressState(false);
				in_progress = false;
			} else {
				if (r.before_id) {
					if ($('#comment-container-' + r.before_id).length == 0) {
						location.hash = '#comment-' + r.id;
						location.reload();
						return;
					}
					$('#comment-container-' + r.before_id).after(r.html);
				} else if (r.after_id) {
					if ($('#comment-container-' + r.after_id).length == 0) {
						location.hash = '#comment-' + r.id;
						location.reload();
						return;
					}
					$('#comment-container-' + r.after_id).before(r.html);
				} else {
					$('#jq-comments .list-').append(r.html);
				}
			
				editor.setContent('');
				var $new_comment = $('#comment-container-' + r.id);
			
				$new_comment.find('.jq-comment-reply-link').click(reply_toggle);
				$new_comment.find('.jq-comment-delete-link').click(function() {
					return delete_comment.call(this, 1);
				});

				if (!can_moderate_comments) {
					$new_comment.find('.jq-comment-delete-link').remove();
				}
			
				in_progress = false;
				if ($current_comment) {
					$current_comment.find('.jq-comment-reply-link').click();
				}
				$('body').scrollTo($new_comment.find('.anchor-'), 800);
					
				editor.setProgressState(false);
			}
		} else {
			show_error();

			editor.setProgressState(false);
			in_progress = false;
		}
	}
			
	function ajax_add_error(r, editor) {
		show_error();
		editor.setProgressState(false);
		in_progress = false;
	}

	function ajax_delete_success(r) {
		if (r.status == 'OK') {
			var $comment = $('#comment-container-' + r.id);
			$comment.replaceWith(r.html);
			$comment = $('#comment-container-' + r.id);
			if (r.is_deleted == 1) {
				$comment.find('.jq-comment-restore-link').click(function() {
					return delete_comment.call(this, 0);
				});
			}
			else {
				$comment.find('.jq-comment-reply-link').click(reply_toggle);
				$comment.find('.jq-comment-delete-link').click(function() {
					return delete_comment.call(this, 1);
				});
			}
		} else {
			show_error();
		}
	}
	
	function ajax_delete_error(r) {
		show_error();
	}
	
	
	function ajax_subscribe_success(r) {
		if (r.status == 'OK') {
			if (r.is_subscribed == 1) {
				$('#jq-comments-subscribe').hide();
				$('#jq-comments-unsubscribe').show();
			}
			else {
				$('#jq-comments-unsubscribe').hide();
				$('#jq-comments-subscribe').show();
			}
		} else {
			show_error();
		}
	}
	
	function ajax_subscribe_error(r) {
		show_error();
	}
	
	$('.jq-comment-add-submit').click(function () {
		var editor = $(this).parents('#jq-comment-main-reply').length > 0 ? main_editor : reply_editor;
		if (!editor) return;
		if (in_progress) return;
		in_progress = true;
		var $comment = $(this).parents('.jq-comment');
		var $comments = $(this).parents('#jq-comments');
		var parent_id = $comment.length > 0 ? $comment.attr('data-id') : 0;
		var type = $comments.attr('data-type');
		var object_id = $comments.attr('data-object-id');
		var html = editor.getContent();

		var data = {
			type : type,
			object_id : object_id,
			parent_id : parent_id,
			html: html
		};
		
		$.extend(data, comments_post_vars);
		editor.setProgressState(true);

		$.ajax({
			async: true,
			type: 'POST',
			url: global.ajax_prefix + comment_add_ctrl + '/',
			data: data,
			success: function (r){
				ajax_add_success(r, editor)
			},
			error: function (r){
				ajax_add_error(r, editor)
			},
			dataType: 'json'
		});
	});
	$('.jq-comment-reply-link').click(reply_toggle);
	$('.jq-comment-delete-link').click(function() {
		return delete_comment.call(this, 1);
	});
	$('.jq-comment-restore-link').click(function() {
		return delete_comment.call(this, 0);
	});
	$('#jq-comments-subscribe, #jq-comments-unsubscribe').click(subscribe_toggle);
});