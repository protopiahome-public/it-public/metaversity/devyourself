$(function() {
	$('a.jq-import-project-delete').click(function() {
		var $form_el = $('#project_delete_form_' + $(this).attr('data-project-id'));
		xlightbox.show({
			title: 'Подтвердите удаление',
			content: $('#dialog-confirm-delete p'),
			button_title: 'Удалить',
			red: true,
			success: function(options) {
				$form_el.submit();
				return true;
			}
		});
	});
	$('input#select-all').change(function () {
		$(this).attr("checked") ? $('.jq-resource-checkbox').attr('checked', 'checked') : $('.jq-resource-checkbox').removeAttr('checked')
	});
});