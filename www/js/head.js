function head_init() {
	function get_el(id) {
		return document.getElementById(id);
	}
	var user_el = get_el('head-user');
	if (!user_el) return;
	
	if (user_el.offsetWidth > 300) {
		// IE6 markup fix
		var width = parseInt(user_el.getElementsByTagName('span')[0].clientWidth) + 50;
		user_el.style.width = width + 'px';
	}
	var user_dd_el = get_el('head-user-dd');
	if (!user_dd_el) return;
	
	var user_dd_timer = null;
	function disable_event(e) {
		e.returnValue = false;
		return false;
	}
	function user_dd_show() {
		user_el.className = 'hover-';
		user_dd_el.style.display = 'block';
		if (user_dd_timer) {
			clearTimeout(user_dd_timer);
			user_dd_timer = null;
		}
	}
	function user_dd_hide() {
		user_el.className = '';
		user_dd_el.style.display = 'none';
		user_dd_timer = null;
	}
	function user_dd_hide_start() {
		if (!user_dd_timer) {
			user_dd_timer = setTimeout(user_dd_hide, 400);
		}
	}
	function attach_event(el, event, callback) {
		if (el.addEventListener) {
			el.addEventListener(event, callback, false);
		} else {
			el.attachEvent('on' + event, callback);
		}
	}
	attach_event(user_el, 'mouseover', user_dd_show);
	attach_event(user_el, 'mouseout', user_dd_hide_start);
	//attach_event(user_el, 'click', disable_event, false);
	attach_event(user_dd_el, 'mouseover', user_dd_show);
	attach_event(user_dd_el, 'mouseout', user_dd_hide_start);
}