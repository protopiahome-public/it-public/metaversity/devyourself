$(function () {
	$('#jq-vector-step1-rose-expand-future').click(function () {
		$('#jq-vector-step1-rose-competences').toggle();
		if ($('#jq-vector-step1-rose-competences:visible').length) {
			$('#jq-vector-moveable-content').appendTo('#jq-vector-moveable-content-top-container');
			$('#jq-vector-moveable-right').appendTo('#jq-vector-moveable-right-top-container');
		} else {
			$('#jq-vector-moveable-content').appendTo('#jq-vector-moveable-content-bottom-container');
			$('#jq-vector-moveable-right').appendTo('#jq-vector-moveable-right-bottom-container');
		}
	});
});