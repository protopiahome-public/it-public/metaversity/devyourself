$(function() {
	$('.jq-delete-button').click(function() {
		var $form_el = $(this).parents('form');
		$('#jq-delete-user-id').val($(this).attr('data-user-id'));
		$('#jq-dialog-confirm-login').text($(this).attr('data-user-login'));
		xlightbox.show({
			title: 'Подтвердите удаление',
			content: $('#jq-dialog-confirm p'),
			button_title: 'Удалить',
			red: true,
			success: function(options) {
				$form_el.submit();
				return true;
			}
		});
		return false;
	});
	$('#jq-bulk-delete-button').click(function() {
		var $form_el = $(this).parents('form');
		$('#jq-delete-user-id').val(0);
		var checked_count = $('.jq-cb:checked').length;
		$('#jq-dialog-bulk-confirm-count').text(checked_count + count_case_ru(checked_count, ' участник будет удален', ' участника будут удалены', ' участников будут удалены'));
		xlightbox.show({
			title: 'Подтвердите удаление',
			content: $('#jq-dialog-bulk-confirm p'),
			button_title: 'Удалить',
			red: true,
			success: function(options) {
				$form_el.submit();
				return true;
			}
		});
		return false;
	});
});