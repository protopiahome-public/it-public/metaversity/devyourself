$(function() {
	var re_str = '\\.' + global.main_host_name.replace(/[.]/, '\\.') + '$';
	var re = new RegExp(re_str);
	function is_external_host(url) {
		var matches = url.match(/^https?:\/\/([a-zA-Z0-9.-]+)\//);
		if (!matches) return -1;
		var host = matches[1];
		var external_host = host != global.main_host_name && !host.match(re);
		return external_host ? 1 : 0;
	}
	
	function ga_loaded() {
		return window.gat_ && window.gat_.getTracker_ || window.gaGlobal;
	}
	
	$('a').click(function() {
		if (!ga_loaded()) {
			return true;
		}
		
		var location_external = is_external_host(location.href);
		if (location_external == -1) return true;
		
		var href_external = is_external_host($(this).attr('href'));
		if (href_external == -1) return true;
		
		if (location_external != href_external) {
			_gaq.push(['_link', $(this).attr('href')]);
			return false;
		}
		return true;
	});
});