(function($) {
	function htmlsafe(str) {
		return str.replace(/\&/g, '&amp;')
		.replace(/\</g, '&lt;').replace(/\>/g, '&gt;')
		.replace(/\'/g, '&quot;').replace(/\'/g, '&#x27;');
	}
	
	$.fn.mouse_hints = function() {
		var hintEl = $('#mouse_hint');
		return this.each(function(){
			$('*[title]:not(form):not([hint])', this)
			.attr('hint', function() {
				return htmlsafe($(this).attr('title'));
			})
			.removeAttr('title');
			$('*[hint]', this).unbind('hover');
			$('*[hint]', this)
			.hover(function() {
				if($(this).attr('hint')) hintEl.html($(this).attr('hint')).show();
			}, function() {
				$('#mouse_hint').hide();
			});
		});
	};

	$(function() {
		$(document).mousemove(function(e) {
			var mh = $('#mouse_hint');
			if (mh.length == 0 || mh.is(':hidden')) {
				return;
			}

			var x = e.pageX + 16;
			var y = e.pageY + 16;

			var s = document.body.parentNode.scrollTop + $(window).height();

			if (x + mh.outerWidth() > $(window).width() - 20) {
				x = e.pageX - mh.outerWidth() - 4;
			}
			if (y + mh.outerHeight() > s - 20) {
				y = e.pageY - mh.outerHeight() - 4;
			}

			mh.css({
				maxWidth: '300px', 
				left: x + 'px', 
				top: y + 'px'
			});
		}).click(function() {
			$('#mouse_hint').hide();
		});

		$(window)
		.scroll(function(){
			$('#mouse_hint').hide();
		})
		.blur(function(){
			$('#mouse_hint').hide();
		});
		$(document.body).mouse_hints();
	});
})(jQuery);