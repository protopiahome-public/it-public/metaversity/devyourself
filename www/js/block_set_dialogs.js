$(function () {
	/* Dialogs */
	var abstract_dialog = function() {}
	$.extend(abstract_dialog.prototype, {
		type: 'abstract',
		_options: {},
		_values: {},
		options: {},
		values: {},
		ajax_url: '',
		post_vars: {},
		is_edit_mode: false,
		success_callback: null,
		_first_init: true,
		show: function(ajax_url, options, values, post_vars, success_callback) {
			this.ajax_url = ajax_url;
			this.options = $.extend({}, this._options, options || {});
			this.values = $.extend({}, this._values, values || {});
			this.post_vars = post_vars;
			this.is_edit_mode = values !== null;
			this.success_callback = success_callback || function() {};
			this._init_elements();
			this._init_custom();
			if (this._first_init) {
				this._init_events();
			}
			this._init_window();
			this._init_keyboard();
			this._first_init = false;
			this._show_window();
		},
		_init_elements: function () {
			this.$dialog = $('#jq-block-set-window-' + this.type + '-dialog');
			this.$lightbox = $('#jq-block-set-window-' + this.type + '-lightbox');
			this.$content_main = $('#jq-block-set-window-' + this.type + '-content-main');
			this.$close = $('#jq-block-set-window-' + this.type + '-close');
			this.$submit = $('#jq-block-set-window-' + this.type + '-submit');
		},
		_init_custom: function () {},
		_init_events: function () {
			var th = this;
			this.$submit.click(function() {
				return th._do_submit.apply(th, arguments)
			});
			this.$close.click(function() {
				return th._do_close_window.apply(th, arguments)
			});
		},
		_init_window: function () {},
		_init_keyboard: function () {
			var th = this;
			if (this._first_init) {
				$('input:text, select', this.$dialog).bind('keydown', function(e) {
					if (e.keyCode == 13 || e.keyCode == 10) { // Enter
						th._do_submit.apply(th, arguments);
						return false;
					}
				});
			}
			$(document).unbind('keydown').bind('keydown', function(e) {
				if (e.keyCode == 27) { // Escape
					th._do_close_window();
					return true;
				}
			});
		},
		_show_window: function () {
			this.$content_main.removeClass('content-hidden-');
			$('.overlay-', this.$dialog).css('opacity', '.25');
			this.$dialog.show();
			this.$lightbox.css('left', Math.round(($(window).width() - this.$lightbox.width()) / 2) + 'px');
			this.$lightbox.css('top', Math.round(($(window).height() - this.$lightbox.height() - 40) / 2) + 'px');
		},
		_do_close_window: function() {
			$(document).unbind('keydown');
			this.$dialog.hide();
		}
	});
	
	var error_dialog = $.extend(new abstract_dialog(), {
		type: 'error',
		_options: {},
		_values: {
			title: 'Произошла ошибка',
			message: ''
		},
		show: function(values) {
			abstract_dialog.prototype.show.call(this, '', null, values, null, null);
		},
		_init_elements: function () {
			abstract_dialog.prototype._init_elements.apply(this);
			this.$title = $('#jq-block-set-window-error-title');
			this.$message = $('#jq-block-set-window-error-message');
		},
		_init_window: function() {
			this.$title.text(this.values.title);
			this.$message.text(this.values.message);
		},
		_show_window: function () {
			abstract_dialog.prototype._show_window.apply(this);
			this.$submit.focus();
		},
		_do_submit: function() {
			this._do_close_window();
			return false;
		}
	});
	
	var file_dialog = $.extend(new abstract_dialog(), {
		// constants
		ERROR_AJAX: -1401,
		ERROR_NO_SERVER_CODE: -1402,
		ERROR_NO_SERVER_JSON: -1603,
		ERROR_WHILE_SUBMIT: -1604,
		STATE_FINAL_ERROR: -2,
		STATE_ERROR: -1,
		STATE_INITIAL: 0,
		STATE_READY: 1,
		STATE_SUBMITTING_FILE: 2,
		STATE_SUBMITTING: 3,
		STATE_COMPLETE: 4,
		STATE_CANCEL: 5,
		// settings
		type: 'file',
		_options: {
			max_file_size: 1024 * 1024,
			session_cookie_name : 'sid',
			msg_file_blank: '(не более %MAX_FILE_SIZE%)',
			msg_file_size: 'Ошибка: файл слишком большой (разрешено не более %MAX_FILE_SIZE%)',
			msg_must_choose_file: 'Ошибка: необходимо выбрать файл для загрузки',
			msg_must_choose_title: 'Ошибка: необходимо ввести название файла',
			msg_submit_error: 'Извините, при загрузке файла произошла ошибка (код = %CODE%)',
			msg_submit_error_detailed: 'Извините, при загрузке файла произошла ошибка (код = %CODE%, тип: %TYPE%, доп. информация: %INFO%)'
		},
		_values: {
			file_name: null,
			file_title: null,
			file_size: 0
		},
		// internal
		state: 0,
		ready_to_submit: false,
		state_error: false,
		dialog_key: -1,
		_init_elements: function () {
			abstract_dialog.prototype._init_elements.apply(this);
			this.$content_submitting = $('#jq-block-set-window-file-content-submitting');
			this.$content_submitting_file = $('#jq-block-set-window-file-content-submitting-file');
			this.$cancel = $('#jq-block-set-window-file-cancel');
			this.$cancel_upload = $('#jq-block-set-window-file-cancel-upload');
			this.$file_input_container = $('#jq-block-set-window-file-file-input-container');
			this.$file_input_real_container_inner = $('#file-input-real-container-inner');
			this.$file_input = $('#jq-block-set-window-file-file-input');
			this.$file_error = $('#jq-block-set-window-file-file-error');
			this.$name_input = $('#jq-block-set-window-file-name');
			this.$title_input_container = $('#jq-block-set-window-file-title-container');
			this.$title_input = $('#jq-block-set-window-file-title');
			this.$title_error = $('#jq-block-set-window-file-title-error');
			this.$reupload_container = $('#jq-block-set-window-file-reupload-container');
			this.$loader_fill = $('#jq-block-set-window-file-submitting-loader-fill');
		},
		_init_custom: function() {
			this.state = this.STATE_INITIAL;
			this.dialog_key = Math.round(Math.random() * 10000000000 + 1);
			this.__init_uploadify();
		},
		__init_uploadify: function() {
			var th = this;
			if (this._first_init) {
				this.$file_input.uploadify({
					'hideButton': false,
					'uploader': global.prefix + 'swf/uploadify.swf',
					'expressInstall': global.prefix + 'swf/expressInstall.swf',
					'script': this.ajax_url,
					'buttonImg': global.prefix + 'img/block-set-file-upload.png',
					'cancelImg': global.prefix + 'img/close.gif',
					'width': 99,
					'height': 16,
					'fileDataName': 'file',
					'sizeLimit': this.options.max_file_size,
					'multi': false,
					'auto': false,
					'onSelect': function() {
						return th.__do_file_select.apply(th, arguments);
					},
					'onProgress': function() {
						return th.__do_upload_progress.apply(th, arguments);
					},
					'onError': function () {
						return th.__do_upload_error.apply(th, arguments);
					},
					'onComplete': function() {
						return th.__do_upload_complete.apply(th, arguments);
					}
				});
			}
		},
		_init_events: function() {
			abstract_dialog.prototype._init_events.apply(this);
			var th = this;
			this.$cancel.click(function() {
				return th._do_close_window.apply(th, arguments)
			});
			this.$cancel_upload.click(function() {
				return th._do_close_window.apply(th, arguments)
			});
			$('span', this.$reupload_container).click(function () {
				th.$file_input_container.show();
				th.$reupload_container.hide();
				th.$file_input_real_container_inner.css('left', '10px');
			});
		},
		_init_window: function() {
			if (this.is_edit_mode) {
				this.$title_input_container.detach().insertBefore(this.$reupload_container);
				this.$reupload_container.show();
				this.$file_input_container.hide();
				this.$file_input_real_container_inner.css('top', '68px');
				this.$file_input_real_container_inner.css('left', '-2000px');
			} else {
				this.$title_input_container.detach().insertAfter(this.$file_input_container);
				this.$reupload_container.hide();
				this.$file_input_container.show();
				this.$file_input_real_container_inner.css('top', '10px');
				this.$file_input_real_container_inner.css('left', '10px');
			}
			this.$file_error.hide();
			this.$title_error.hide();
			this.$title_input.val(this.values.file_title);
			this.$name_input.val(this.options.msg_file_blank.replace(/%MAX_FILE_SIZE%/, this.format_size(this.options.max_file_size)));
			this.$content_submitting.addClass('content-hidden-');
			this.$content_submitting_file.addClass('content-hidden-');
		},
		_show_window: function () {
			abstract_dialog.prototype._show_window.apply(this);
			if (this.is_edit_mode) {
				this.$title_input.focus();
			}
		},
		_do_close_window: function() {
			abstract_dialog.prototype._do_close_window.apply(this);
			if (this.state == this.STATE_SUBMITTING_FILE) {
				this.$file_input.uploadifyClearQueue();
			}
			this.dialog_key = -1;
			this.state = this.STATE_CANCEL;
		},
		__do_file_select: function(event, id, file_obj) {
			// show file name
			this.$name_input.val(file_obj.name + ' (' + this.format_size(file_obj.size) + ')');
			
			// show title
			if (!this.is_edit_mode) {
				var title = file_obj.name;
				var ext_regexp = /\.[a-zA-Z0-9_]{1,4}$/;
				if (title.match(ext_regexp)) {
					title = title.replace(ext_regexp, '');
				}
				title = title.replace(/_/g, ' ');
				this.$title_input.val(title);
			}
			
			// check file size
			if (file_obj.size > this.options.max_file_size) {
				this.$file_error.show();
				$('span', this.$file_error).text(this.options.msg_file_size.replace(/%MAX_FILE_SIZE%/, this.format_size(this.options.max_file_size)));
				this.state = this.STATE_ERROR;
			} else {
				this.$file_error.hide();
				this.state = this.STATE_READY;
			}
		},
		_do_submit: function() {
			var th = this;
			// Checking state...
			if (this.state == this.STATE_ERROR) {
				return false;
			}
			if (this.state == this.STATE_INITIAL && !this.is_edit_mode) {
				this.$file_error.show();
				$('span', this.$file_error).text(this.options.msg_must_choose_file);
				return false;
			}
			if (!$.trim(this.$title_input.val())) {
				this.$title_error.show();
				$('span', this.$title_error).text(this.options.msg_must_choose_title);
				return false;
			}
			
			// Submitting...
			this.$content_main.addClass('content-hidden-');
			this.$file_input_real_container_inner.css('left', '-2000px');
			this.$loader_fill.css('width', '0');
			var post_data = $.extend({}, this.post_vars, {
				'block_type': 'file',
				'dialog_key': this.dialog_key,
				'title': this.$title_input.val()
			});
			var session_cookie_value = $.cookie(this.options.session_cookie_name);
			if (session_cookie_value) {
				post_data[this.options.session_cookie_name] = session_cookie_value;
			}
			if (this.state == this.STATE_INITIAL) {
				this.$content_submitting.removeClass('content-hidden-');
				this.state = this.STATE_SUBMITTING;
				$.ajax({
					type: 'POST',
					url: this.ajax_url,
					data: post_data,
					success: function() {
						th.__do_complete.apply(th, arguments);
					},
					error: function() {
						th.__do_ajax_error.apply(th, arguments);
					},
					dataType: 'json'
				});
			} else {
				this.$content_submitting_file.removeClass('content-hidden-');
				this.state = this.STATE_SUBMITTING_FILE;
				this.$file_input.uploadifySettings('scriptData', post_data, true);
				this.$file_input.uploadifyUpload();
			}
			return false;
		},
		__do_upload_progress: function(event, id, file_obj, data) {
			this.$loader_fill.css('width', Math.round(data.percentage) + '%');
		},
		__do_upload_error: function(event, ID, file_obj, error_obj) {
			if (error_obj.type == 'File Size') return;
			this._do_close_window();
			this.state = this.STATE_FINAL_ERROR;
			var error_code = this.ERROR_WHILE_SUBMIT;
			var error_type = error_obj.type;
			var error_info = error_obj.info;
			error_dialog.show({
				message: this.options.msg_submit_error_detailed.replace(/%CODE%/, error_code).replace(/%TYPE%/, error_type).replace(/%INFO%/, error_info)
			});
		},
		__do_ajax_error: function(response) {

			if (this.state != this.STATE_SUBMITTING) {
				return;
			}
			this._do_close_window();
			this.state = this.STATE_FINAL_ERROR;
			error_dialog.show({
				message: this.options.msg_submit_error.replace(/%CODE%/, this.ERROR_AJAX)
			});
		},
		__do_upload_complete: function(event, id, file_obj, response, data) {
			var response_obj;
			try {
				response_obj = $.parseJSON(response);
			} catch (err) {
				response_obj = null;
			}
			if (response_obj) {
				this.__do_complete(response_obj);
			} else {
				this._do_close_window();
				this.state = this.STATE_FINAL_ERROR;
				var error_code = this.ERROR_NO_SERVER_JSON;
				error_dialog.show({
					message: this.options.msg_submit_error.replace(/%CODE%/, error_code)
				});
			}
		},
		__do_complete: function(response_obj) {
			if (!response_obj || !response_obj.dialog_key || response_obj.dialog_key != this.dialog_key) {
				return;
			}
			if (response_obj.status == "OK" && response_obj.data) {
				this._do_close_window();
				this.state = this.STATE_COMPLETE;
				this.success_callback(response_obj.data);
			} else {
				this._do_close_window();
				this.state = this.STATE_FINAL_ERROR;
				var error_code = response_obj.error_code ? response_obj.error_code : this.ERROR_NO_SERVER_CODE;
				error_dialog.show({
					message: this.options.msg_submit_error.replace(/%CODE%/, error_code)
				});
			}
		},
		format_size: function(size) {
			function human_round(num) {
				if (num >= 10) return Math.round(num).toString().replace(/\./, ',');
				else return (Math.round(num * 10) / 10).toString().replace(/\./, ',');
			}
			if (size < 1024 && size % 10 >= 2 && size % 10 <= 4 && size / 10 % 10 != 1) return size + ' байта';
			else if (size < 1024) return size + ' байт';
			else if (Math.round(size / 1024) < 1024) return human_round(size / 1024) + ' КБ';
			else if (Math.round(size / 1024 / 1024) < 1024) return human_round(size / 1024 / 1024) + ' MБ';
			else return human_round(size / 1024 / 1024 / 1024) + ' ГБ';
		}
	});
	
	var video_dialog = $.extend(new abstract_dialog(), {
		// constants
		ERROR_AJAX: -2401,
		ERROR_NO_SERVER_CODE: -2402,
		ERROR_BAD_CODE: 2001,
		ERROR_UNSUPPORTED_SERVICE: 2002,
		STATE_FINAL_ERROR: -2,
		STATE_ERROR: -1,
		STATE_INITIAL: 0,
		STATE_READY: 1,
		STATE_SUBMITTING: 2,
		STATE_COMPLETE: 3,
		STATE_CANCEL: 4,
		// settings
		type: 'video',
		_options: {
			msg_blank: 'Ошибка: необходимо указать URL или embed-код',
			msg_unsupported_service: 'Ошибка: видео-сервис «%SERVICE%» нами не поддерживается. Вставьте embed-код.',
			msg_bad_code: 'Некоррекный embed-код',
			msg_error_while_submit: 'Извините, при сохранении произошла ошибка (код = %CODE%)'
		},
		_values: {
			video_url: null,
			video_code: null,
			video_title: null
		},
		// internal
		state: 0,
		ready_to_submit: false,
		state_error: false,
		dialog_key: -1,
		_init_elements: function () {
			abstract_dialog.prototype._init_elements.apply(this);
			this.$content_submitting = $('#jq-block-set-window-video-content-submitting');
			this.$cancel = $('#jq-block-set-window-video-cancel');
			this.$code_input = $('#jq-block-set-window-video-code-input');
			this.$code_error = $('#jq-block-set-window-video-code-error');
			this.$code_error_long = $('#jq-block-set-window-video-code-error-long');
			this.$title_input = $('#jq-block-set-window-video-title-input');
			this.$title_error = $('#jq-block-set-window-video-title-error');
		},
		_init_custom: function() {
			this.state = this.STATE_READY;
			this.dialog_key = Math.round(Math.random() * 10000000000 + 1);
		},
		_init_events: function() {
			abstract_dialog.prototype._init_events.apply(this);
			var th = this;
			this.$cancel.click(function() {
				return th._do_close_window.apply(th, arguments)
			});
		},
		_init_window: function() {
			this.$code_input.val(this.values.video_url || this.values.video_code);
			this.$title_input.val(this.values.video_title);
			this.$code_error.hide();
			this.$code_error_long.hide();
			this.$title_error.hide();
			this.$content_submitting.addClass('content-hidden-');
		},
		_show_window: function () {
			abstract_dialog.prototype._show_window.apply(this);
			if (this.is_edit_mode) {
				this.$title_input.focus();
			} else {
				this.$code_input.focus();
			}
		},
		_do_close_window: function() {
			abstract_dialog.prototype._do_close_window.apply(this);
			this.dialog_key = -1;
			this.state = this.STATE_CANCEL;
		},
		_do_submit: function() {
			var th = this;
			
			// Checking state...
			if (this.state == this.STATE_ERROR) {
				return false;
			}
			if (!$.trim(this.$code_input.val())) {
				this.$code_error_long.hide();
				this.$code_error.show();
				$('span', this.$code_error).text(this.options.msg_blank);
				return false;
			}
			
			// Submitting...
			this.$content_main.addClass('content-hidden-');
			this.$content_submitting.removeClass('content-hidden-');
			this.state = this.STATE_SUBMITTING;
			
			var post_data = $.extend({}, this.post_vars, {
				'block_type': 'video',
				'dialog_key': this.dialog_key,
				'title': this.$title_input.val(),
				'code': this.$code_input.val()
			});
			$.ajax({
				type: 'POST',
				url: this.ajax_url,
				data: post_data,
				success: function() {
					th.__do_complete.apply(th, arguments);
				},
				error: function() {
					th.__do_ajax_error.apply(th, arguments);
				},
				dataType: 'json'
			});
			
			return false;
		},
		__do_ajax_error: function(response) {
			if (this.state != this.STATE_SUBMITTING) {
				return;
			}
			this._do_close_window();
			this.state = this.STATE_FINAL_ERROR;
			error_dialog.show({
				message: this.options.msg_error_while_submit.replace(/%CODE%/, this.ERROR_AJAX)
			});
		},
		__do_complete: function(response_obj) {
			if (this.state != this.STATE_SUBMITTING) {
				return;
			}
			if (!response_obj || !response_obj.dialog_key || response_obj.dialog_key != this.dialog_key) {
				return;
			}
			if (response_obj && response_obj.status == "OK" && response_obj.data) {
				this._do_close_window();
				this.state = this.STATE_COMPLETE;
				this.success_callback(response_obj.data);
			} else {
				var error_code = response_obj.error_code ? response_obj.error_code : this.ERROR_NO_SERVER_CODE;
				if (error_code == this.ERROR_UNSUPPORTED_SERVICE) {
					this.$content_main.removeClass('content-hidden-');
					this.$content_submitting.addClass('content-hidden-');
					this.state = this.STATE_READY;
					this.$code_error.hide();
					this.$code_error_long.show();
					$('span', this.$code_error_long).text(this.options.msg_unsupported_service.replace(/%SERVICE%/, response_obj.service_domain));
				} else if (error_code == this.ERROR_BAD_CODE) {
					this.$content_main.removeClass('content-hidden-');
					this.$content_submitting.addClass('content-hidden-');
					this.state = this.STATE_READY;
					this.$code_error_long.hide();
					this.$code_error.show();
					$('span', this.$code_error).text(this.options.msg_bad_code);
				} else {
					this._do_close_window();
					this.state = this.STATE_FINAL_ERROR;
					error_dialog.show({
						message: this.options.msg_error_while_submit.replace(/%CODE%/, error_code)
					});
				}
			}
		}
	});
	
	var cut_dialog = $.extend(new abstract_dialog(), {
		// settings
		type: 'cut',
		_options: {
			msg_blank: 'Ошибка: текст не может быть пустым'
		},
		_values: {
			cut_title: null
		},
		show: function(options, values, success_callback) {
			abstract_dialog.prototype.show.call(this, '', options, values, null, success_callback);
		},
		_init_elements: function () {
			abstract_dialog.prototype._init_elements.apply(this);
			this.$title_input = $('#jq-block-set-window-cut-title-input');
			this.$title_error = $('#jq-block-set-window-cut-title-error');
		},
		_init_window: function() {
			this.$title_input.val(this.values.cut_title);
			this.$title_error.hide();
		},
		_show_window: function () {
			abstract_dialog.prototype._show_window.apply(this);
			this.$title_input.focus();
		},
		_do_submit: function() {
			var cut_title = this.$title_input.val();
			if (!$.trim(cut_title)) {
				this.$title_error.show();
				$('span', this.$title_error).text(this.options.msg_blank);
			} else {
				this.success_callback({
					cut_title: cut_title
				});
				this._do_close_window();
			}
			return false;
		}
	});
	
	window.block_set_dialogs = {
		error_dialog: error_dialog,
		file_dialog: file_dialog,
		video_dialog: video_dialog,
		cut_dialog: cut_dialog
	}
});