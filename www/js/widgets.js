$(function() {
	var widgets_data = [];
	window.widget_edit_mode_on = function () {
		$('.jq-widgets-inner').addClass('widgets-inner-admin-');
		$('.jq-widget-add').show();
		$('.jq-widget-edit-mode').show();
		$('.jq-default-widgets').hide();
		$('.jq-widgets-inner').sortable({
			appendTo: 'body',
			revert: false,
			scroll: true,
			helper: 'original',
			handle: '.jq-move-handle, h2',
			connectWith: '.jq-widgets-inner',
			tolerance: 'pointer',
			update: widgets_resort,
			placeholder: 'placeholder'
		});

		for(var i = 1; i < 4; i++) {
			widget_update_data(i);
		}
		widget_resize_columns();
	}
	window.widget_edit_mode_off = function() {
		$('.jq-widgets-inner').removeClass('widgets-inner-admin-');
		$('.jq-widget-add').hide();
		$('.jq-widget-edit-mode').hide();
		$('.jq-default-widgets').show();
		$('.jq-widgets-inner').sortable('destroy');
		widget_resize_columns();
	}
	function widgets_resort() {
		var i;
		var data = {
			column: $(this).attr('id').substr(11), // "jq-widgets-#"
			widgets: $(this).sortable('toArray')
		};
		for (i in data.widgets) {
			data.widgets[i] = data.widgets[i].substr(7); // "widget-#"
		}

		var new_widgets = [];

		for (i in data.widgets) {
			var in_array = false;
			for (var j in widgets_data[data.column]) {
				if (widgets_data[data.column][j] == data.widgets[i]) {
					in_array = true;
					break;
				}
			}
			if (!in_array) {
				new_widgets[new_widgets.length] = data.widgets[i];
			}
		}
		for (i in new_widgets) {
			$('#widget-' + new_widgets[i]).addClass('widget-wrap-loading').html('<div class="widget widget-loading"/>');
		}
		widgets_data[data.column] = data.widgets;
		data.widgets = data.widgets.join(',');
		data.new_widgets = new_widgets.join(',');
		xpost({
			ajax_ctrl_name: widget_container_type + '_widgets_resort',
			data : data,
			success_callback: function(data){
				for (var i in data['widgets']) {
					$('#widget-' + i).removeClass('widget-wrap-loading').html(data['widgets'][i]);
				}
				widget_reinit_events();
				$('.jq-widget-edit-mode').show();

				widget_resize_columns();
			}
		});
	}
	$('#jq-widgets-edit-link').toggle(widget_edit_mode_on, widget_edit_mode_off);

	function widget_set_title() {
		var widget_id = $(this).parents('.jq-widget').attr('jq-widget-id');
		var header_cont = '#widget-' + widget_id + ' .jq-widget-header';
		var title = $('h2', header_cont).text();
		var new_title = prompt('Введите новый заголовок', title);
		if (new_title === null) {
			return false;
		}
		new_title = $.trim(new_title);
		new_title.replace(/[\x00-\x20]/gm, '');
		var data = {
			widget_id: widget_id,
			title: new_title
		}

		if (new_title != '') {
			$(header_cont)
				.find('.text-').text(new_title).end()
				.removeClass('header-hidden-');
		} else {
			$(header_cont)
				.addClass('header-hidden-')
				.find('.text-').text('');
		}

		xpost({
			ajax_ctrl_name: widget_container_type + '_widget_set_title',
			data: data
		});

		widget_resize_columns();

		return false;
	}

	function widget_delete() {
		var widget_id = $(this).parents('.jq-widget').attr('jq-widget-id');
		var column = $(this).parents('.jq-widgets').prev('.jq-widget-add').attr('jq-column');
		var ok = confirm('Вы точно хотите удалить этот виджет?');
		if (!ok) {
			return false;
		};
		var data = {
			widget_id: widget_id
		};
		xpost({
			ajax_ctrl_name: widget_container_type + '_widget_delete',
			data: data,
			success_callback: function (data){
				$('#widget-' + widget_id).hide('fast', function() {
					$(this).remove();
					$('.jq-widgets-inner').sortable('refresh');
					widget_resize_columns();
					widget_update_data(column);
				});
			}
		});


		return false;
	}

	function widget_add_form() {
		var column = $(this).attr('jq-column');
		data = {
			column: column
		};
		xpost({
			ajax_ctrl_name: widget_container_type + '_widget_add_form',
			data: data,
			success_callback: function(data) {
				xlightbox.show({
					title: 'Добавить виджет',
					content: data.html,
					width: '452px'
				});
				$('.jq-widget-add-type').unbind('click');
				$('.jq-widget-add-type').click(widget_add);
			},
			custom_loading_msg: 'Загрузка...'
		});

		return false;
	}

	function widget_add() {
		var column = $(this).attr('jq-column');
		var type_id = $(this).attr('jq-type-id');
		var data = {
			column: column,
			type_id: type_id
		}
		xpost({
			ajax_ctrl_name: widget_container_type + '_widget_add',
			data: data,
			success_callback: function (data) {
				xlightbox.hide();
				$('#jq-widgets-' + data.column).prepend(data.html);
				widget_reinit_events();
				$('.jq-widget-edit-mode').show();
				$('#widget-' + data.id).show('fast', function () {
					widget_resize_columns();
					widget_update_data(data.column);
				});
			}
		});

		return false;
	}

	function widget_edit() {
		var widget_id = $(this).parents('.jq-widget').attr('jq-widget-id');
		var data = {
			widget_id: widget_id
		}
		xpost({
			ajax_ctrl_name: widget_container_type + '_widget_edit_form',
			data: data,
			success_callback: function (data) {
				xlightbox.show({
					title: 'Редактировать виджет',
					content: data.html,
					width: '80%',
					button_title: 'Сохранить',
					side_padding: '50px'
				});
			},
			custom_loading_msg: 'Загрузка...'
		});

		return false;
	}

	/**
	 * Update widgets_data array
	 *
	 * @param int column Column number
	 */
	function widget_update_data(column) {
		if(column) {
			var i = null;
			widgets_data[column] = $('#jq-widgets-' + column).sortable('toArray');
			for (i in widgets_data[column]) {
				widgets_data[column][i] = widgets_data[column][i].substr(7); // "widget-#"
			}
		}
	}

	window.widget_resize_columns = function() {
		var max_height = 0;
		$('.jq-widgets-inner').each(function() {
			$(this).height('auto');
			var height = $(this).height() + 60;
			if(height > max_height) {
				max_height = height;
			}
		}).height(max_height);
	};

	window.widget_reinit_events = function() {
		$('.jq-widget-set-title').unbind('click');
		$('.jq-widget-set-title').click(widget_set_title);
		$('.jq-widget-delete').unbind('click');
		$('.jq-widget-delete').click(widget_delete);
		$('.jq-widget-edit').unbind('click');
		$('.jq-widget-edit').click(widget_edit);
		$(document).trigger('markup_change');
	}
	widget_reinit_events();

	(function init_global_events(){
		$('.jq-widget-add').click(widget_add_form);
	})();
});