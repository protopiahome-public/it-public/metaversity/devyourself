$(function(){
	function competence_set_select_click_handler() {
		document.location.href = "?id=" + this.options[this.selectedIndex].value;
		
	}
	
	function competence_selection_click_handler() {
		xpost({
			ajax_ctrl_name: 'precedent_edit_competences',
			data: {
				method : this.checked ? 'competence_group_add' : 'competence_group_remove',
				competence_group_id : $(this).attr('data-competence-group-id')
			}
		});
	}
	
	$('#jq-competence-set-select').on('change', competence_set_select_click_handler);
	$('#jq-competence-set-competences').on('click', 'input.jq-competence-group', competence_selection_click_handler);
})