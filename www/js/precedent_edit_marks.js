$(function(){
	window.xmark_editor = new function() {
		var arg = {
			competence_id: 0,
			competence_set_id: 0,
			ratee_entity_id: 0,
			type: '',
			value: 0,
			$cell: null,
			$cell_value: null,
			$cell_comment: null,
			comment_save : null
		}
		this.start = function(params) {
			var $content = $('#jq-lightbox-edit-mark');
			$.extend(arg, params);
			arg.$cell_value = arg.$cell.find('.jq-mark-value');
			arg.$cell_comment = arg.$cell.find('.jq-mark-comment');
			
			this.set_value(arg.$cell_value.attr('data-value'));
			arg.comment_save = '';
			$('#jq-edit-mark-comment').val(arg.$cell_comment.prop('innerText'));
			if(arg.type) {
				$content.removeAttr('class').addClass('mark-' + arg.type + '-');
			}
			
			var $ratee_box_placeholder = $content.find('#ratee-block-placeholder').empty();
			var $ratee_box_content = $('#' + (arg.type == 'personal' ? 'user' : 'group') + '-block-' + arg.ratee_entity_id).clone();
			$ratee_box_placeholder.append($ratee_box_content);
			
			$content.find('#competence-title-placeholder').text($('#competence-'+arg.competence_id).text());
			
			xlightbox.show({
				title: 'Поставить оценку',
				content: $content,
				button_title: 'Сохранить',
				width: '416px',
				size_padding: '20px',
				success: function(options) {
					xlightbox.button_disable()
					xmark_editor.store();
					return false;
				}
			});
		}
		this.set_value = function(value) {
			$('#jq-edit-mark-value').removeClass('jq-v0 jq-v1 jq-v2 jq-v3').addClass('jq-v' + value);
			$comment = $('#jq-edit-mark-comment');
			$comment.attr('disabled', (value == 0) ? true : false);
			if ((value == 0) && (arg.value != 0)) {
				arg.comment_save = $comment.val();
				$comment.val('');
				$comment.addClass('jq-disabled');
			}
			else if ((value != 0) && (arg.value == 0)) {
				$comment.val(arg.comment_save);
				$comment.removeClass('jq-disabled');
			}
			arg.value = value;			
		}
		this.store = function() {
			var data = {
				type: arg.type,
				competence_id: arg.competence_id,
				competence_set_id: arg.competence_set_id,
				ratee_entity_id: arg.ratee_entity_id,
				value: arg.value,
				comment: arg.value ? $('#jq-edit-mark-comment').val() : ''
			}
			xpost({
				ajax_ctrl_name: 'precedent_edit_marks', 
				data: data, 
				success_callback: function (data) {
					xmark_editor.success(data);
					xlightbox.hide();
				},
				error_callback: function(data) {
					xlightbox.hide();
				}
			});
		}
		this.success = function(data) {
			arg.$cell_value.attr('data-value', data.value);
			arg.$cell_value.removeClass('jq-v0 jq-v1 jq-v2 jq-v3').addClass('jq-v' + data.value);
			arg.$cell_comment.text(data.comment);
		}
	}
	
	function selector_change_handler() {
		var competence_set_id = $('#jq-competence-set-select').val();
		document.location.href = "?rater=" + $('#jq-rater-select').val()
			+ (competence_set_id ? '&competence_set=' + competence_set_id : '' );
		
	}
	
	function mark_personal_click_handler() {
		var $this = $(this);
		xmark_editor.start({
			type: 'personal',
			competence_id: $this.attr('data-competence-id'),
			competence_set_id: $this.attr('data-competence-set-id'),
			ratee_entity_id: $this.attr('data-user-link-id'),
			$cell: $this
		});
	}
	function mark_group_click_handler() {
		var $this = $(this);
		xmark_editor.start({
			type: 'group',
			competence_id: $this.attr('data-competence-id'),
			competence_set_id: $this.attr('data-competence-set-id'),
			ratee_entity_id: $this.attr('data-flash-group-id'),
			$cell: $this
		});
	}

	var $hover_message = $('<div class="message-"><table><tr><td><span class="text-">добавить оценку</td></tr></table></div>');
	function mark_hover_handler(e) {
		var $this = $(this);
		if(e.type == 'mouseenter') {
			if($this.find('.jq-mark-value.jq-v0').length) {
				$hover_message.appendTo($this).show();
			}
		}
		else {
			$hover_message.hide();
		}
	}
	
	$('#jq-rater-select').on('change', selector_change_handler);
	$('#jq-competence-set-select').on('change', selector_change_handler);
	
	$('table.jq-mark-table').on('click', 'td.jq-mark-personal', mark_personal_click_handler)
				.on('mouseenter mouseleave', 'td.jq-mark-personal, td.jq-mark-group', mark_hover_handler);

	$('table.jq-mark-table').on('click', 'td.jq-mark-group', mark_group_click_handler);
	
	$('#jq-edit-mark-value').on('click', 'span', function() {xmark_editor.set_value(this.getAttribute('data-value'))});
})