$(function () {
	$(document).unbind('keydown').bind('keydown', function(e) {
		if (e.altKey && e.keyCode == '1'.charCodeAt(0)) {
			var classes = ['color-green', 'color-blue', 'color-yellow', 'color-red'];
			var all_selector = '';
			var idx;
			for (idx in classes) {
				all_selector += '.' + classes[idx] + ', ';
			}
			var $all = $(all_selector);
			var idx_next, new_class;
			for (idx in classes) {
				if ($all.eq(0).is('.' + classes[idx])) {
					idx_next = idx == classes.length - 1 ? 0 : Number(idx) + 1;
					new_class = classes[idx_next];
				}
				$all.removeClass(classes[idx]);
			}
			$all.addClass(new_class);
			
			return false;
		}
	});
});