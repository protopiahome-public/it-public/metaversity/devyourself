<?php

define("PATH_WWW", dirname(__FILE__));
define("PATH_HOME", dirname(PATH_WWW));
define("PATH_CONFIG", PATH_HOME . "/config");
define("PATH_DB", PATH_HOME . "/db");
define("PATH_SOURCES", PATH_HOME . "/sources");
define("PATH_VAR", PATH_HOME . "/var");
define("PATH_XSLT", PATH_HOME . "/xslt");

define("PATH_MODULES", PATH_SOURCES . "/modules");
define("PATH_MODULE_SITE", PATH_MODULES . "/_site");
define("PATH_CORE", PATH_SOURCES . "/core");
define("PATH_INTCMF", PATH_SOURCES . "/intcmf");
define("PATH_TEST_CORE", PATH_SOURCES . "/test_core");
define("PATH_DBCONVERT", PATH_MODULE_SITE . "/lib/dbconvert");
define("PATH_MODULE_SITE_LIB", PATH_MODULE_SITE . "/lib");

define("PATH_TMP", PATH_VAR . "/tmp");
define("PATH_LOG", PATH_VAR . "/log");
define("PATH_SESSIONS", PATH_VAR . "/sessions");
define("PATH_PRIVATE_DATA", PATH_VAR . "/data");

define("PATH_PUBLIC_DATA", PATH_WWW . "/data");
?>