<?php

$config = array();

if (isset($_SERVER["DEV_HOST"]))
{
	$config_file_name = PATH_CONFIG . "/dev_" . strtolower($_SERVER["DEV_HOST"]) . "_conf.php";
}
elseif (isset($_SERVER["PRODUCTION_HOST"]))
{
	$config_file_name = PATH_CONFIG . "/prod_" . strtolower($_SERVER["PRODUCTION_HOST"]) . "_conf.php";
}
else
{
	die("You should specify environment variable: either DEV_HOST or PRODUCTION_HOST.<br/>\n
Then create a config file: _config/dev_XXX.config.php or _config/prod_XXX.config.php, where XXX is a value of an environment variable.");
}

$config["debug_ip"] = array(
	"127.0.0.1", // Local
);

$config["safe_html"] = true;

$config["recaptcha_public_key"] = "";
$config["recaptcha_private_key"] = "";

$config["pass_recovery_key_life_time"] = 86400;

$config["x_session_private_key"] = "";

require_once $config_file_name;

?>