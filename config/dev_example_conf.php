<?php

$config["db_host"] = "";
$config["db_user"] = "";
$config["db_pass"] = "";
$config["db_name"]	= "";
$config["db_cmp4_source"]	= "";
$config["db_idsrv_source"]	= "";
$config["test_db_name"]	= "";

$config["memcache_host"] = "localhost";
$config["memcache_port"] = "11211";
$config["memcache_prefix"] = "dys";

$config["main_host_name"] = false;
$config["main_host_prefix"] = "";
$config["main_url"]="";
$config["from"] = "1@localhost";
$config["comments_from"] = "1@localhost";

$config["imagemagick_path"] = "C:/Program Files/ImageMagick/";
$config["math_path"] = "";

$config["default_vector_competence_set_id"] = 24;

function config_session()
{
	global $session, $request, $config;
	/* @var $request request */

	$cookie_path = $request->get_prefix() . "/";
	$cookie_domain = null;
	$session = new x_session(PATH_SESSIONS, $config["x_session_private_key"], $cookie_path, $cookie_domain, false, "X_SESSION_ID6");
	$session->remove_old_cookie("X_SESSION_ID");
	$session->remove_old_cookie("X_SESSION_ID2");
	$session->remove_old_cookie("X_SESSION_ID3");
	$session->remove_old_cookie("X_SESSION_ID4");
	$session->remove_old_cookie("X_SESSION_ID5");

	if (strpos($_SERVER["HTTP_USER_AGENT"], "Opera") !== false)
	{
		//$_SESSION["member_id"] = 4;
	}
	elseif (strpos($_SERVER["HTTP_USER_AGENT"], "Chrome") !== false)
	{
		//$_SESSION["member_id"] = 2;
	}
	$_SESSION["member_id"] = 140; 
	//$_SESSION["member_id"] = 176; 
}

?>