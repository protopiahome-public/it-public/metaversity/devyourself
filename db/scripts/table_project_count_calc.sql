UPDATE `project` p SET event_count_calc =
	(SELECT COUNT(*) FROM event WHERE project_id = p.id)
;

UPDATE `project` p SET material_count_calc =
	(SELECT COUNT(*) FROM material WHERE project_id = p.id)
;

UPDATE `project` p SET community_count_calc = 
	(SELECT COUNT(*) FROM community WHERE project_id = p.id AND is_deleted = 0)
;

UPDATE `project` p SET custom_feed_count_calc = (
	SELECT COUNT(*) FROM project_custom_feed WHERE project_id = p.id
);

UPDATE project p SET user_count_calc = (
	SELECT COUNT(*)
	FROM user_project_link l
	WHERE l.project_id = p.id AND l.status <> 'pretender' AND l.status <> 'deleted'
);