UPDATE `competence_set` cs SET professiogram_count_calc = (
	SELECT count(*)
	FROM professiogram
	WHERE competence_set_id = cs.id AND enabled = 1
);