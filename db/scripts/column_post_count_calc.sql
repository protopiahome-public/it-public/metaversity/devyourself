UPDATE `section` s SET post_count_calc = (
	SELECT COUNT(*) FROM post WHERE section_id = s.id AND is_deleted = 0
);

UPDATE `community` c SET post_count_calc = (
	SELECT COUNT(*) FROM post WHERE community_id = c.id AND is_deleted = 0
);

UPDATE `project` p SET post_count_calc = (
	SELECT IFNULL(SUM(post_count_calc), 0) FROM community WHERE project_id = p.id AND is_deleted = 0
);

/* user_project_link */

INSERT IGNORE INTO user_project_link (project_id, user_id, add_time, `status`)
(
	SELECT c.project_id, p.author_user_id, NOW(), 'deleted'
	FROM post p
	LEFT JOIN community c ON c.id = p.community_id
	WHERE p.is_deleted = 0
	GROUP BY c.project_id, p.author_user_id
);

UPDATE user_project_link l SET post_count_calc = (
	SELECT COUNT(*) 
	FROM post p 
	LEFT JOIN community c ON c.id = p.community_id
	WHERE author_user_id = l.user_id AND c.project_id = l.project_id AND p.is_deleted = 0
);

/* community_user_link */

INSERT IGNORE INTO community_user_link (community_id, user_id, add_time, `status`)
(
	SELECT p.community_id, p.author_user_id, NOW(), 'deleted'
	FROM post p
	WHERE p.is_deleted = 0
	GROUP BY p.community_id, p.author_user_id
);

UPDATE community_user_link l SET post_count_calc = (
	SELECT COUNT(*) 
	FROM post p 
	WHERE p.is_deleted = 0 AND author_user_id = l.user_id AND p.community_id = l.community_id
);