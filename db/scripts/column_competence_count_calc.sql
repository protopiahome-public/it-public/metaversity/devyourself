UPDATE `competence_set` s
SET competence_count_calc = (
	SELECT count(*)
	FROM competence_full c
	WHERE c.is_deleted = 0 AND c.competence_set_id = s.id
);
UPDATE `project_role_rate_link` rl SET `competence_count_calc` = (SELECT COUNT(*) FROM rate_competence_link rcl WHERE rcl.rate_id = rl.rate_id);
UPDATE `event_rate_link` rl SET `competence_count_calc` = (SELECT COUNT(*) FROM rate_competence_link rcl WHERE rcl.rate_id = rl.rate_id);
UPDATE `material_rate_link` rl SET `competence_count_calc` = (SELECT COUNT(*) FROM rate_competence_link rcl WHERE rcl.rate_id = rl.rate_id);
UPDATE `professiogram` p SET `competence_count_calc` = (SELECT COUNT(*) FROM rate_competence_link rcl WHERE rcl.rate_id = p.rate_id);