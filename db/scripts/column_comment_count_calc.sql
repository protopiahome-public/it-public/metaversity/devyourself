UPDATE `event` o SET comment_count_calc = (
	SELECT COUNT(*) FROM comment WHERE type_id = 1 AND object_id = o.id AND is_deleted = 0
);

UPDATE `material` o SET comment_count_calc = (
	SELECT COUNT(*) FROM comment WHERE type_id = 2 AND object_id = o.id AND is_deleted = 0
);

UPDATE `post` o SET comment_count_calc = (
	SELECT COUNT(*) FROM comment WHERE type_id = 3 AND object_id = o.id AND is_deleted = 0
);

UPDATE `community` c SET comment_count_calc = (
	SELECT IFNULL(SUM(comment_count_calc), 0)
	FROM post 
	WHERE community_id = c.id AND is_deleted = 0
);

UPDATE `project` p SET comment_count_calc = 
	(SELECT IFNULL(SUM(comment_count_calc), 0) FROM event WHERE project_id = p.id) +
	(SELECT IFNULL(SUM(comment_count_calc), 0) FROM material WHERE project_id = p.id) +
	(SELECT IFNULL(SUM(comment_count_calc), 0) FROM community WHERE project_id = p.id AND is_deleted = 0)
;

/* user_project_link */

INSERT IGNORE INTO user_project_link (project_id, user_id, add_time, `status`)
(
	SELECT comm.project_id, c.author_user_id, NOW(), 'deleted'
	FROM comment c
	JOIN post p ON c.object_id = p.id
	LEFT JOIN community comm ON comm.id = p.community_id
	WHERE c.type_id = 3 AND c.is_deleted = 0
	GROUP BY comm.project_id, c.author_user_id
);

INSERT IGNORE INTO user_project_link (project_id, user_id, add_time, `status`)
(
	SELECT o.project_id, c.author_user_id, NOW(), 'deleted'
	FROM comment c
	JOIN event o ON c.object_id = o.id
	WHERE c.type_id = 1 AND c.is_deleted = 0
	GROUP BY o.project_id, c.author_user_id
);

INSERT IGNORE INTO user_project_link (project_id, user_id, add_time, `status`)
(
	SELECT o.project_id, c.author_user_id, NOW(), 'deleted'
	FROM comment c
	JOIN material o ON c.object_id = o.id
	WHERE c.type_id = 2 AND c.is_deleted = 0
	GROUP BY o.project_id, c.author_user_id
);

UPDATE user_project_link l SET comment_count_calc = 0;

UPDATE user_project_link l SET comment_count_calc = comment_count_calc +
(
	SELECT COUNT(*)
	FROM comment c
	JOIN post p ON c.object_id = p.id
	LEFT JOIN community comm ON comm.id = p.community_id
	WHERE c.type_id = 3 AND c.is_deleted = 0 AND p.is_deleted = 0 AND comm.project_id = l.project_id AND c.author_user_id = l.user_id
);

UPDATE user_project_link l SET comment_count_calc = comment_count_calc +
(
	SELECT COUNT(*)
	FROM comment c
	JOIN event o ON c.object_id = o.id
	WHERE c.type_id = 1 AND c.is_deleted = 0 AND o.project_id = l.project_id AND c.author_user_id = l.user_id
);

UPDATE user_project_link l SET comment_count_calc = comment_count_calc +
(
	SELECT COUNT(*)
	FROM comment c
	JOIN material o ON c.object_id = o.id
	WHERE c.type_id = 2 AND c.is_deleted = 0 AND o.project_id = l.project_id AND c.author_user_id = l.user_id
);

/* community_user_link */

INSERT IGNORE INTO community_user_link (community_id, user_id, add_time, `status`)
(
	SELECT p.community_id, c.author_user_id, NOW(), 'deleted'
	FROM comment c
	JOIN post p ON c.object_id = p.id
	WHERE c.type_id = 3 AND c.is_deleted = 0 AND p.is_deleted = 0
	GROUP BY p.community_id, c.author_user_id
);

UPDATE community_user_link l SET comment_count_calc = 
(
	SELECT COUNT(*)
	FROM comment c
	JOIN post p ON c.object_id = p.id
	WHERE c.type_id = 3 AND c.is_deleted = 0 AND p.is_deleted = 0 AND p.community_id = l.community_id AND c.author_user_id = l.user_id
);