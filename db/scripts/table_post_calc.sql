DELETE FROM post_calc;
INSERT INTO post_calc
(id, author_user_id, project_id, community_id, section_id, hide_in_project_feed, comment_count_calc, 
last_comment_id, last_comment_author_user_id, last_comment_html)
(
	SELECT p.id, p.author_user_id, c.project_id, p.community_id, p.section_id, p.hide_in_project_feed, p.comment_count_calc, 
	p.last_comment_id, p.last_comment_author_user_id, p.last_comment_html
	FROM post p
	LEFT JOIN community c ON c.id = p.community_id
	WHERE p.is_deleted = 0 AND c.is_deleted = 0
);