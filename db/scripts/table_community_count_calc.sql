UPDATE `community` c SET custom_feed_count_calc = (
	SELECT COUNT(*) FROM community_custom_feed WHERE community_id = c.id
);

UPDATE community c SET member_count_calc = (
	SELECT COUNT(*)
	FROM community_user_link l
	WHERE l.community_id = c.id AND l.status <> 'pretender' AND l.status <> 'deleted'
);

DROP TABLE IF EXISTS child_community_calcs;
CREATE TEMPORARY TABLE child_community_calcs (
	SELECT c.id, COUNT(cc.id) as children_count
	FROM community c
	LEFT JOIN community cc ON cc.parent_id = c.id AND cc.parent_approved = 1 AND cc.is_deleted = 0
	GROUP BY c.id
);

UPDATE community c
LEFT JOIN child_community_calcs cc ON c.id = cc.id
SET c.child_community_count_calc = cc.children_count;
