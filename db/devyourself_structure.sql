/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица api_user_update
CREATE TABLE IF NOT EXISTS `api_user_update` (
  `project_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `FK_api_user_update_user_id` (`user_id`),
  CONSTRAINT `FK_api_user_update_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_api_user_update_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица block_file
CREATE TABLE IF NOT EXISTS `block_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `block_set_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `ext` varchar(5) NOT NULL DEFAULT '',
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(10) NOT NULL DEFAULT '',
  `file_name` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_fake` tinyint(1) NOT NULL DEFAULT '0',
  `fake_for_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_block_file_block_set_id` (`block_set_id`),
  KEY `FK_block_file_fake_for_id` (`fake_for_id`),
  CONSTRAINT `FK_block_file_block_set_id` FOREIGN KEY (`block_set_id`) REFERENCES `block_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_block_file_fake_for_id` FOREIGN KEY (`fake_for_id`) REFERENCES `block_file` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица block_set
CREATE TABLE IF NOT EXISTS `block_set` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `xml` longtext NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица block_video
CREATE TABLE IF NOT EXISTS `block_video` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `block_set_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `html` mediumtext NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `fake_for_id` int(10) unsigned DEFAULT NULL,
  `is_fake` tinyint(1) NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_block_video_block_set_id` (`block_set_id`),
  KEY `FK_block_video_fake_for_id` (`fake_for_id`),
  CONSTRAINT `FK_block_video_block_set_id` FOREIGN KEY (`block_set_id`) REFERENCES `block_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_block_video_fake_for_id` FOREIGN KEY (`fake_for_id`) REFERENCES `block_video` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица comment
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned DEFAULT NULL,
  `author_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `html` text NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `old_id` int(10) unsigned DEFAULT NULL,
  `old_parent_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_comment_parent_id` (`parent_id`),
  KEY `FK_comment_type_id` (`type_id`),
  KEY `FK_comment_author_user_id` (`author_user_id`),
  CONSTRAINT `FK_comment_author_user_id` FOREIGN KEY (`author_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_comment_type_id` FOREIGN KEY (`type_id`) REFERENCES `comment_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица comment_subscription
CREATE TABLE IF NOT EXISTS `comment_subscription` (
  `type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`type_id`,`object_id`,`user_id`),
  KEY `FK_comment_subscription_user_id` (`user_id`),
  CONSTRAINT `FK_comment_subscription_type_id` FOREIGN KEY (`type_id`) REFERENCES `comment_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_comment_subscription_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица comment_type
CREATE TABLE IF NOT EXISTS `comment_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица communities_menu_dd_item
CREATE TABLE IF NOT EXISTS `communities_menu_dd_item` (
  `community_id` int(10) unsigned DEFAULT '0',
  `project_id` int(10) unsigned DEFAULT '0',
  `position` int(10) unsigned DEFAULT '0',
  `add_time` datetime DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime DEFAULT '0000-00-00 00:00:00',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `FK_communities_menu_dd_project_id` (`project_id`),
  KEY `unique` (`community_id`,`project_id`),
  CONSTRAINT `FK_communities_menu_dd_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_communities_menu_dd_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица communities_menu_item
CREATE TABLE IF NOT EXISTS `communities_menu_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_communities_menu_project_id` (`project_id`),
  KEY `unique` (`community_id`,`project_id`),
  CONSTRAINT `FK_communities_menu_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_communities_menu_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица community
CREATE TABLE IF NOT EXISTS `community` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `descr_short` varchar(140) NOT NULL DEFAULT '',
  `logo_big_width` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_big_height` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_small_width` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_small_height` int(10) unsigned NOT NULL DEFAULT '0',
  `project_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `allow_posting_without_section` tinyint(1) NOT NULL DEFAULT '1',
  `member_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `join_premoderation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `bg_head_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_main_url` varchar(255) DEFAULT NULL,
  `bg_body_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_body_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_body_main_url` varchar(255) DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `parent_approved` tinyint(1) NOT NULL DEFAULT '0',
  `allow_child_communities` enum('nobody','user','user_premoderation') NOT NULL DEFAULT 'nobody',
  `access_read` enum('guest','user','project_member','member','moderator','admin') NOT NULL DEFAULT 'guest',
  `access_add_post` enum('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user',
  `access_comment` enum('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user',
  `logo_version` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_version` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_body_version` int(10) unsigned NOT NULL DEFAULT '0',
  `custom_feed_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `child_community_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `old_id` int(10) unsigned DEFAULT NULL,
  `old_parent_id` int(10) unsigned DEFAULT NULL,
  `color_scheme` enum('project','green','blue','yellow','red') NOT NULL DEFAULT 'project',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`,`project_id`,`is_deleted`) USING BTREE,
  KEY `FK_community_project_id` (`project_id`),
  KEY `FK_community_parent_id` (`parent_id`),
  CONSTRAINT `FK_community_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `community` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_community_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для процедура community_add_count_calc
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `community_add_count_calc`(
		IN community_id INTEGER(11),
		IN project_id INTEGER(11),
		IN community_post_count_calc INTEGER(11),
		IN community_comment_count_calc INTEGER(11)
	)
BEGIN
	UPDATE project
	SET 
		community_count_calc = community_count_calc + 1,
		post_count_calc = post_count_calc + community_post_count_calc,
		comment_count_calc = comment_count_calc + community_comment_count_calc
	WHERE id = project_id;
END//
DELIMITER ;

-- Дамп структуры для таблица community_admin
CREATE TABLE IF NOT EXISTS `community_admin` (
  `user_id` int(10) unsigned NOT NULL,
  `community_id` int(10) unsigned NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adder_admin_user_id` int(10) unsigned DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_admin_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`community_id`),
  KEY `FK_community_admin_community_id` (`community_id`),
  KEY `FK_community_admin_adder_admin_user_id` (`adder_admin_user_id`),
  KEY `FK_community_admin_editor_admin_user_id` (`editor_admin_user_id`),
  CONSTRAINT `FK_community_admin_adder_admin_user_id` FOREIGN KEY (`adder_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_community_admin_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_admin_editor_admin_user_id` FOREIGN KEY (`editor_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_community_admin_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица community_custom_feed
CREATE TABLE IF NOT EXISTS `community_custom_feed` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `descr` text NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `old_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_community_custom_feed_community_id` (`community_id`) USING BTREE,
  CONSTRAINT `FK_community_custom_feed_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица community_custom_feed_source
CREATE TABLE IF NOT EXISTS `community_custom_feed_source` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `custom_feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `section_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique_item` (`custom_feed_id`,`community_id`,`section_id`) USING BTREE,
  KEY `FK_community_custom_feed_source_community_id` (`community_id`) USING BTREE,
  KEY `FK_community_custom_feed_source_section_id` (`section_id`) USING BTREE,
  CONSTRAINT `FK_community_custom_feed_source_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_custom_feed_source_custom_feed_id` FOREIGN KEY (`custom_feed_id`) REFERENCES `community_custom_feed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_custom_feed_source_section_id` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для процедура community_delete_count_calc
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `community_delete_count_calc`(
		IN community_id INTEGER(11),
		IN project_id INTEGER(11),
		IN community_post_count_calc INTEGER(11),
		IN community_comment_count_calc INTEGER(11)
	)
BEGIN
	UPDATE project
	SET 
		community_count_calc = community_count_calc - 1,
		post_count_calc = post_count_calc - community_post_count_calc,
		comment_count_calc = comment_count_calc - community_comment_count_calc
	WHERE id = project_id;
END//
DELIMITER ;

-- Дамп структуры для таблица community_user_link
CREATE TABLE IF NOT EXISTS `community_user_link` (
  `community_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('deleted','pretender','member','admin') NOT NULL DEFAULT 'deleted',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`community_id`,`user_id`),
  KEY `FK_community_user_link_user_id` (`user_id`),
  CONSTRAINT `FK_community_user_link_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица community_widget
CREATE TABLE IF NOT EXISTS `community_widget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `column` int(2) unsigned NOT NULL DEFAULT '1',
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  `title` varchar(100) NOT NULL DEFAULT '',
  `type_id` char(40) NOT NULL,
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  `old_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_community_widget_community_id` (`community_id`) USING BTREE,
  KEY `FK_community_widget_type_id` (`type_id`) USING BTREE,
  CONSTRAINT `FK_community_widget_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_widget_type_id` FOREIGN KEY (`type_id`) REFERENCES `community_widget_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица community_widget_communities
CREATE TABLE IF NOT EXISTS `community_widget_communities` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  PRIMARY KEY (`widget_id`,`community_id`),
  KEY `FK_community_widget_communities_2` (`community_id`),
  CONSTRAINT `FK_community_widget_communities_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_widget_communities_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `community_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица community_widget_custom_feed
CREATE TABLE IF NOT EXISTS `community_widget_custom_feed` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_custom_feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`widget_id`),
  KEY `FK_community_widget_custom_feed_community_custom_feed_id` (`community_custom_feed_id`) USING BTREE,
  CONSTRAINT `FK_community_widget_custom_feed_community_feed_id` FOREIGN KEY (`community_custom_feed_id`) REFERENCES `community_custom_feed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_widget_custom_feed_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `community_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица community_widget_default
CREATE TABLE IF NOT EXISTS `community_widget_default` (
  `column` int(2) unsigned NOT NULL DEFAULT '1',
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  `title` varchar(100) NOT NULL DEFAULT '',
  `type_id` char(40) NOT NULL,
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`column`,`position`),
  KEY `FK_community_widget_default_type_id` (`type_id`) USING BTREE,
  CONSTRAINT `FK_community_widget_default_type_id` FOREIGN KEY (`type_id`) REFERENCES `community_widget_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица community_widget_text
CREATE TABLE IF NOT EXISTS `community_widget_text` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `html` text NOT NULL,
  PRIMARY KEY (`widget_id`),
  CONSTRAINT `FK_community_widget_text_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `community_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица community_widget_type
CREATE TABLE IF NOT EXISTS `community_widget_type` (
  `type_id` char(40) NOT NULL DEFAULT '',
  `title` char(100) DEFAULT NULL,
  `multiple_instances` tinyint(1) NOT NULL DEFAULT '0',
  `ctrl` char(100) NOT NULL,
  `edit_ctrl` char(100) NOT NULL DEFAULT '',
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_calc
CREATE TABLE IF NOT EXISTS `competence_calc` (
  `competence_id` int(10) unsigned NOT NULL,
  `title` varchar(1024) NOT NULL,
  `number` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_set_id` int(10) unsigned NOT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`competence_id`),
  KEY `FK_competence_calc_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_competence_calc_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_calc_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_full
CREATE TABLE IF NOT EXISTS `competence_full` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(1024) NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_competence_full_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_competence_full_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_group
CREATE TABLE IF NOT EXISTS `competence_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_competence_group_parent_id` (`parent_id`),
  KEY `FK_competence_group_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_competence_group_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_group_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `competence_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_link
CREATE TABLE IF NOT EXISTS `competence_link` (
  `competence_id` int(10) unsigned NOT NULL,
  `competence_group_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`competence_id`,`competence_group_id`),
  KEY `FK_competence_link_competence_group_id` (`competence_group_id`),
  CONSTRAINT `FK_competence_link_competence_group_id` FOREIGN KEY (`competence_group_id`) REFERENCES `competence_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_link_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_set
CREATE TABLE IF NOT EXISTS `competence_set` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `professiogram_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `personal_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `project_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `project_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_competence_set_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_competence_set_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_set_admin
CREATE TABLE IF NOT EXISTS `competence_set_admin` (
  `user_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_professiogram_moderator` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adder_admin_user_id` int(10) unsigned DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_admin_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`competence_set_id`),
  KEY `FK_competence_set_admin_competence_set_id` (`competence_set_id`),
  KEY `FK_competence_set_admin_adder_admin_user_id` (`adder_admin_user_id`),
  KEY `FK_competence_set_admin_editor_admin_user_id` (`editor_admin_user_id`),
  CONSTRAINT `FK_competence_set_admin_adder_admin_user_id` FOREIGN KEY (`adder_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_set_admin_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_set_admin_editor_admin_user_id` FOREIGN KEY (`editor_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_set_admin_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_translator
CREATE TABLE IF NOT EXISTS `competence_translator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_competence_set_id` int(10) unsigned NOT NULL,
  `to_competence_set_id` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_competence_translator_from_competence_set_id` (`from_competence_set_id`),
  KEY `FK_competence_translator_to_competence_set_id` (`to_competence_set_id`),
  CONSTRAINT `FK_competence_translator_from_competence_set_id` FOREIGN KEY (`from_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translator_to_competence_set_id` FOREIGN KEY (`to_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица email
CREATE TABLE IF NOT EXISTS `email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `to_email` varchar(255) NOT NULL,
  `to_user_id` int(10) unsigned DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `html` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица email_log
CREATE TABLE IF NOT EXISTS `email_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `to_email` varchar(255) NOT NULL,
  `to_user_id` int(10) unsigned DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `html` mediumtext,
  `sent_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `success` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица event
CREATE TABLE IF NOT EXISTS `event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finish_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `place` varchar(255) NOT NULL DEFAULT '',
  `announce` text,
  `reflex_id` int(10) unsigned DEFAULT NULL,
  `reflex_project_id` int(10) unsigned DEFAULT NULL,
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `reflex` (`project_id`,`reflex_id`),
  KEY `FK_event_project_id` (`project_id`),
  KEY `FK_event_adder_user_id` (`adder_user_id`),
  KEY `FK_event_reflex_id` (`reflex_id`),
  KEY `FK_event_reflex_project_id` (`reflex_project_id`),
  CONSTRAINT `FK_event_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_event_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_reflex_id` FOREIGN KEY (`reflex_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_reflex_project_id` FOREIGN KEY (`reflex_project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица event_category
CREATE TABLE IF NOT EXISTS `event_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_event_category_parent_id` (`parent_id`),
  KEY `FK_event_category_project_id` (`project_id`),
  CONSTRAINT `FK_event_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `event_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_category_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица event_category_link
CREATE TABLE IF NOT EXISTS `event_category_link` (
  `event_id` int(10) unsigned NOT NULL,
  `event_category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`event_id`,`event_category_id`),
  KEY `FK_event_category_link_event_category_id` (`event_category_id`),
  CONSTRAINT `FK_event_category_link_event_category_id` FOREIGN KEY (`event_category_id`) REFERENCES `event_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_category_link_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для процедура event_comment_add_count_calc
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `event_comment_add_count_calc`(
		IN in_event_id INTEGER(11),
		IN in_project_id INTEGER(11),
		IN in_author_user_id INTEGER(11)
	)
BEGIN
	UPDATE event
	SET comment_count_calc = comment_count_calc + 1
	WHERE id = in_event_id;

	UPDATE project
	SET comment_count_calc = comment_count_calc + 1
	WHERE id = in_project_id;

	INSERT IGNORE user_project_link (project_id, user_id, status) 
	VALUES (in_project_id, in_author_user_id, 'deleted');
	UPDATE user_project_link
	SET comment_count_calc = comment_count_calc + 1
	WHERE user_id = in_author_user_id AND project_id = in_project_id;
END//
DELIMITER ;

-- Дамп структуры для процедура event_comment_delete_count_calc
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `event_comment_delete_count_calc`(
		IN in_event_id INTEGER(11),
		IN in_project_id INTEGER(11),
		IN in_author_user_id INTEGER(11)
	)
BEGIN
	UPDATE event
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_event_id;

	UPDATE project
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_project_id;

	INSERT IGNORE user_project_link (project_id, user_id, status) 
	VALUES (in_project_id, in_author_user_id, 'deleted');
	UPDATE user_project_link
	SET comment_count_calc = comment_count_calc - 1
	WHERE user_id = in_author_user_id AND project_id = in_project_id;
END//
DELIMITER ;

-- Дамп структуры для таблица event_rate_link
CREATE TABLE IF NOT EXISTS `event_rate_link` (
  `event_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`event_id`,`rate_id`),
  KEY `FK_event_rate_link_rate_id` (`rate_id`),
  KEY `FK_event_rate_link_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_event_rate_link_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_rate_link_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_rate_link_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица event_user_link
CREATE TABLE IF NOT EXISTS `event_user_link` (
  `user_id` int(10) unsigned NOT NULL,
  `event_id` int(10) unsigned NOT NULL,
  `ignored_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `before_status` int(10) unsigned NOT NULL DEFAULT '0',
  `after_status` int(10) NOT NULL DEFAULT '0',
  `ignored` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `before_status_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `after_status_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_status_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`event_id`),
  KEY `FK_event_user_link_event_id` (`event_id`),
  CONSTRAINT `FK_event_user_link_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для функция insert_rate
DELIMITER //
CREATE DEFINER=`root`@`%` FUNCTION `insert_rate`(
	rate_type_id1 int unsigned,
	title1 varchar(255) CHARACTER SET utf8,
	competence_set_id1 int unsigned,
	add_time1 datetime,
	edit_time1 datetime
) RETURNS int(10) unsigned
BEGIN
	INSERT INTO rate
		(rate_type_id, title, competence_set_id, add_time, edit_time) 
	VALUES
		(rate_type_id1, title1, competence_set_id1, add_time1, edit_time1);

	RETURN LAST_INSERT_ID();
END//
DELIMITER ;

-- Дамп структуры для таблица mark_calc
CREATE TABLE IF NOT EXISTS `mark_calc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `precedent_group_id` int(10) unsigned DEFAULT NULL,
  `precedent_flash_group_id` int(10) unsigned NOT NULL,
  `precedent_flash_group_user_link_id` int(10) unsigned NOT NULL,
  `ratee_user_id` int(10) unsigned NOT NULL,
  `rater_user_id` int(10) unsigned NOT NULL,
  `adder_user_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `type` enum('personal','group') NOT NULL,
  `value` int(10) unsigned NOT NULL,
  `comment` text,
  `precedent_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique` (`precedent_flash_group_id`,`ratee_user_id`,`rater_user_id`,`competence_id`,`type`),
  KEY `FK_mark_calc_project_id` (`project_id`),
  KEY `FK_mark_calc_ratee_user_id` (`ratee_user_id`),
  KEY `FK_mark_calc_competence_id` (`competence_id`),
  KEY `FK_mark_calc_competence_set_id` (`competence_set_id`),
  KEY `FK_mark_calc_precedent_flash_group_user_link_id` (`precedent_flash_group_user_link_id`),
  KEY `FK_mark_calc_precedent_group_id` (`precedent_group_id`),
  KEY `FK_mark_calc_precedent_id` (`precedent_id`),
  KEY `FK_mark_calc_rater_user_id` (`rater_user_id`),
  KEY `FK_mark_calc_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_mark_calc_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_precedent_flash_group_id` FOREIGN KEY (`precedent_flash_group_id`) REFERENCES `precedent_flash_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_precedent_flash_group_user_link_id` FOREIGN KEY (`precedent_flash_group_user_link_id`) REFERENCES `precedent_flash_group_user_link` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_precedent_group_id` FOREIGN KEY (`precedent_group_id`) REFERENCES `precedent_group` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_ratee_user_id` FOREIGN KEY (`ratee_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_rater_user_id` FOREIGN KEY (`rater_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица mark_group
CREATE TABLE IF NOT EXISTS `mark_group` (
  `precedent_flash_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rater_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `adder_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  PRIMARY KEY (`precedent_flash_group_id`,`competence_id`,`rater_user_id`),
  KEY `FK_mark_group_competence_id` (`competence_id`),
  KEY `FK_mark_group_adder_user_id` (`adder_user_id`),
  KEY `FK_mark_group_rater_user_id` (`rater_user_id`),
  CONSTRAINT `FK_mark_group_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_group_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_group_precedent_flash_group_id` FOREIGN KEY (`precedent_flash_group_id`) REFERENCES `precedent_flash_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_group_rater_user_id` FOREIGN KEY (`rater_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица mark_personal
CREATE TABLE IF NOT EXISTS `mark_personal` (
  `precedent_flash_group_user_link_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rater_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `adder_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  PRIMARY KEY (`precedent_flash_group_user_link_id`,`competence_id`,`rater_user_id`),
  KEY `FK_mark_personal_competence_id` (`competence_id`),
  KEY `FK_mark_personal_adder_user_id` (`adder_user_id`),
  KEY `FK_mark_personal_rater_user_id` (`rater_user_id`),
  CONSTRAINT `FK_mark_personal_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_personal_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_personal_precedent_flash_group_user_link_id` FOREIGN KEY (`precedent_flash_group_user_link_id`) REFERENCES `precedent_flash_group_user_link` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_personal_rater_user_id` FOREIGN KEY (`rater_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица material
CREATE TABLE IF NOT EXISTS `material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `announce` text,
  `block_set_id` int(10) unsigned DEFAULT NULL,
  `reflex_id` int(10) unsigned DEFAULT NULL,
  `reflex_project_id` int(10) unsigned DEFAULT NULL,
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `x_post_id` int(10) unsigned NOT NULL DEFAULT '0',
  `x_sec_id` int(10) unsigned NOT NULL DEFAULT '0',
  `x_html` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reflex` (`project_id`,`reflex_id`),
  KEY `FK_material_project_id` (`project_id`),
  KEY `FK_material_adder_user_id` (`adder_user_id`),
  KEY `FK_material_block_set_id` (`block_set_id`),
  KEY `FK_material_reflex_id` (`reflex_id`),
  KEY `FK_material_reflex_project_id` (`reflex_project_id`),
  CONSTRAINT `FK_material_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_material_block_set_id` FOREIGN KEY (`block_set_id`) REFERENCES `block_set` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_material_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_reflex_id` FOREIGN KEY (`reflex_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_reflex_project_id` FOREIGN KEY (`reflex_project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица material_category
CREATE TABLE IF NOT EXISTS `material_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_material_category_parent_id` (`parent_id`),
  KEY `FK_material_category_project_id` (`project_id`),
  CONSTRAINT `FK_material_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `material_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_category_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица material_category_link
CREATE TABLE IF NOT EXISTS `material_category_link` (
  `material_id` int(10) unsigned NOT NULL DEFAULT '0',
  `material_category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`material_id`,`material_category_id`),
  KEY `FK_material_category_link_material_category_id` (`material_category_id`),
  CONSTRAINT `FK_material_category_link_material_category_id` FOREIGN KEY (`material_category_id`) REFERENCES `material_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_category_link_material_id` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для процедура material_comment_add_count_calc
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `material_comment_add_count_calc`(
		IN in_material_id INTEGER(11),
		IN in_project_id INTEGER(11),
		IN in_author_user_id INTEGER(11)
	)
BEGIN
	UPDATE material
	SET comment_count_calc = comment_count_calc + 1
	WHERE id = in_material_id;

	UPDATE project
	SET comment_count_calc = comment_count_calc + 1
	WHERE id = in_project_id;

	INSERT IGNORE user_project_link (project_id, user_id, status) 
	VALUES (in_project_id, in_author_user_id, 'deleted');
	UPDATE user_project_link
	SET comment_count_calc = comment_count_calc + 1
	WHERE user_id = in_author_user_id AND project_id = in_project_id;
END//
DELIMITER ;

-- Дамп структуры для процедура material_comment_delete_count_calc
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `material_comment_delete_count_calc`(
		IN in_material_id INTEGER(11),
		IN in_project_id INTEGER(11),
		IN in_author_user_id INTEGER(11)
	)
BEGIN
	UPDATE material
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_material_id;

	UPDATE project
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_project_id;

	INSERT IGNORE user_project_link (project_id, user_id, status) 
	VALUES (in_project_id, in_author_user_id, 'deleted');
	UPDATE user_project_link
	SET comment_count_calc = comment_count_calc - 1
	WHERE user_id = in_author_user_id AND project_id = in_project_id;
END//
DELIMITER ;

-- Дамп структуры для таблица material_rate_link
CREATE TABLE IF NOT EXISTS `material_rate_link` (
  `material_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`material_id`,`rate_id`),
  KEY `FK_material_rate_link_rate_id` (`rate_id`),
  KEY `FK_material_rate_link_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_material_rate_link_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_rate_link_material_id` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_rate_link_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица material_user_link
CREATE TABLE IF NOT EXISTS `material_user_link` (
  `user_id` int(10) unsigned NOT NULL,
  `material_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ignored_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `before_status` int(10) unsigned NOT NULL DEFAULT '0',
  `after_status` int(11) NOT NULL DEFAULT '0',
  `ignored` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `before_status_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `after_status_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_status_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`material_id`),
  KEY `FK_material_user_link_material_id` (`material_id`),
  CONSTRAINT `FK_material_user_link_material_id` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица post
CREATE TABLE IF NOT EXISTS `post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author_user_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `block_set_id` int(10) unsigned DEFAULT NULL,
  `community_id` int(10) unsigned NOT NULL,
  `section_id` int(10) unsigned DEFAULT NULL,
  `hide_in_project_feed` tinyint(1) NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `last_comment_id` int(10) unsigned DEFAULT NULL,
  `last_comment_html` text,
  `last_comment_author_user_id` int(10) unsigned DEFAULT NULL,
  `old_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_post_user_id` (`author_user_id`),
  KEY `FK_post_block_set_id` (`block_set_id`),
  KEY `FK_post_section_id` (`section_id`),
  KEY `FK_post_community_id` (`community_id`),
  KEY `FK_post_last_comment_author_user_id` (`last_comment_author_user_id`),
  KEY `FK_post_last_comment_id` (`last_comment_id`),
  CONSTRAINT `FK_post_block_set_id` FOREIGN KEY (`block_set_id`) REFERENCES `block_set` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_post_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_post_last_comment_author_user_id` FOREIGN KEY (`last_comment_author_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_post_last_comment_id` FOREIGN KEY (`last_comment_id`) REFERENCES `comment` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_post_section_id` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_post_user_id` FOREIGN KEY (`author_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для процедура post_add_count_calc
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `post_add_count_calc`(
		IN in_post_id INTEGER(11),
		IN in_section_id INTEGER(11),
		IN in_community_id INTEGER(11),
		IN in_project_id INTEGER(11),
		IN in_author_user_id INTEGER(11),
		IN in_post_comment_count_calc INTEGER(11)
	)
BEGIN
	UPDATE project
	SET 
		post_count_calc = post_count_calc + 1, 
		comment_count_calc = comment_count_calc + in_post_comment_count_calc
	WHERE id = in_project_id;

	UPDATE community
	SET 
		post_count_calc = post_count_calc + 1, 
		comment_count_calc = comment_count_calc + in_post_comment_count_calc
	WHERE id = in_community_id;

	IF in_section_id IS NOT NULL
	THEN
		UPDATE section
		SET post_count_calc = post_count_calc + 1
		WHERE id = in_section_id;
	END IF;

	INSERT IGNORE community_user_link (community_id, user_id, status) 
	VALUES (in_community_id, in_author_user_id, 'deleted');

	UPDATE community_user_link
	SET post_count_calc = post_count_calc + 1
	WHERE community_id = in_community_id AND user_id = in_author_user_id;

	INSERT IGNORE user_project_link (project_id, user_id, status) VALUES (in_project_id, in_author_user_id, 'deleted');

	UPDATE user_project_link
	SET post_count_calc = post_count_calc + 1
	WHERE project_id = in_project_id AND user_id = in_author_user_id;
END//
DELIMITER ;

-- Дамп структуры для таблица post_calc
CREATE TABLE IF NOT EXISTS `post_calc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_user_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `community_id` int(10) unsigned NOT NULL,
  `section_id` int(10) unsigned DEFAULT NULL,
  `hide_in_project_feed` tinyint(1) NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_id` int(10) unsigned DEFAULT NULL,
  `last_comment_html` text,
  `last_comment_author_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_author` (`author_user_id`,`id`),
  KEY `index_community` (`community_id`,`id`),
  KEY `index_section` (`section_id`,`id`),
  KEY `index_project` (`project_id`,`hide_in_project_feed`,`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для процедура post_comment_add_count_calc
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `post_comment_add_count_calc`(
		IN in_post_id INTEGER(11),
		IN in_community_id INTEGER(11),
		IN in_project_id INTEGER(11),
		IN in_author_user_id INTEGER(11)
	)
BEGIN
	UPDATE post
	SET comment_count_calc = comment_count_calc + 1
	WHERE id = in_post_id;

	UPDATE community
	SET comment_count_calc = comment_count_calc + 1
	WHERE id = in_community_id;

	INSERT IGNORE community_user_link (community_id, user_id, status) 
	VALUES (in_community_id, in_author_user_id, 'deleted');
	UPDATE community_user_link
	SET comment_count_calc = comment_count_calc + 1
	WHERE user_id = in_author_user_id AND community_id = in_community_id;

	INSERT IGNORE user_project_link (project_id, user_id, status) 
	VALUES (in_project_id, in_author_user_id, 'deleted');
	UPDATE user_project_link
	SET comment_count_calc = comment_count_calc + 1
	WHERE user_id = in_author_user_id AND project_id = in_project_id;

	UPDATE project
	SET comment_count_calc = comment_count_calc + 1
	WHERE id = in_project_id;
END//
DELIMITER ;

-- Дамп структуры для процедура post_comment_delete_count_calc
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `post_comment_delete_count_calc`(
		IN in_post_id INTEGER(11),
		IN in_community_id INTEGER(11),
		IN in_project_id INTEGER(11),
		IN in_author_user_id INTEGER(11)
	)
BEGIN
	UPDATE post
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_post_id;

	UPDATE community
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_community_id;

	INSERT IGNORE community_user_link (community_id, user_id, status) 
	VALUES (in_community_id, in_author_user_id, 'deleted');
	UPDATE community_user_link
	SET comment_count_calc = comment_count_calc - 1
	WHERE user_id = in_author_user_id AND community_id = in_community_id;

	INSERT IGNORE user_project_link (project_id, user_id, status) 
	VALUES (in_project_id, in_author_user_id, 'deleted');
	UPDATE user_project_link
	SET comment_count_calc = comment_count_calc - 1
	WHERE user_id = in_author_user_id AND project_id = in_project_id;

	UPDATE project
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_project_id;
END//
DELIMITER ;

-- Дамп структуры для процедура post_delete_count_calc
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `post_delete_count_calc`(
		IN in_post_id INTEGER(11),
		IN in_section_id INTEGER(11),
		IN in_community_id INTEGER(11),
		IN in_project_id INTEGER(11),
		IN in_author_user_id INTEGER(11),
		IN in_post_comment_count_calc INTEGER(11)
	)
BEGIN
	UPDATE project
	SET 
		post_count_calc = post_count_calc - 1, 
		comment_count_calc = comment_count_calc - in_post_comment_count_calc
	WHERE id = in_project_id;

	UPDATE community
	SET 
		post_count_calc = post_count_calc - 1, 
		comment_count_calc = comment_count_calc - in_post_comment_count_calc
	WHERE id = in_community_id;

	IF in_section_id IS NOT NULL
	THEN
		UPDATE section
		SET post_count_calc = post_count_calc - 1
		WHERE id = in_section_id;
	END IF;

	INSERT IGNORE community_user_link (community_id, user_id, status) 
	VALUES (in_community_id, in_author_user_id, 'deleted');

	UPDATE community_user_link
	SET post_count_calc = post_count_calc - 1
	WHERE community_id = in_community_id AND user_id = in_author_user_id;

	INSERT IGNORE user_project_link (project_id, user_id, status) 
	VALUES (in_project_id, in_author_user_id, 'deleted');

	UPDATE user_project_link
	SET post_count_calc = post_count_calc - 1
	WHERE project_id = in_project_id AND user_id = in_author_user_id;
END//
DELIMITER ;

-- Дамп структуры для таблица precedent
CREATE TABLE IF NOT EXISTS `precedent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `descr` mediumtext NOT NULL,
  `precedent_group_id` int(10) unsigned DEFAULT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_editor_user_id` int(10) unsigned NOT NULL,
  `adder_user_id` int(10) unsigned NOT NULL,
  `responsible_user_id` int(10) unsigned DEFAULT NULL,
  `access` enum('public','private') NOT NULL DEFAULT 'public',
  `complete_descr` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `complete_members` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `complete_marks` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `ratee_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `default_flash_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_precedent_project_id` (`project_id`),
  KEY `FK_precedent_precedent_group_id` (`precedent_group_id`),
  KEY `FK_precedent_last_editor_user_id` (`last_editor_user_id`),
  KEY `FK_precedent_responsible_user_id` (`responsible_user_id`),
  KEY `FK_precedent_adder_user_id` (`adder_user_id`),
  KEY `FK_precedent_default_flash_group_id` (`default_flash_group_id`),
  CONSTRAINT `FK_precedent_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_default_flash_group_id` FOREIGN KEY (`default_flash_group_id`) REFERENCES `precedent_flash_group` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_last_editor_user_id` FOREIGN KEY (`last_editor_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_precedent_group_id` FOREIGN KEY (`precedent_group_id`) REFERENCES `precedent_group` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_responsible_user_id` FOREIGN KEY (`responsible_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица precedent_comment
CREATE TABLE IF NOT EXISTS `precedent_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `precedent_id` int(10) unsigned NOT NULL,
  `commenter_user_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `html` text,
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned NOT NULL,
  `add_time` datetime DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_precedent_comment_commenter_user_id` (`commenter_user_id`),
  KEY `FK_precedent_comment_parent_id` (`parent_id`),
  KEY `FK_precedent_comment_precedent_id` (`precedent_id`),
  CONSTRAINT `FK_precedent_comment_commenter_user_id` FOREIGN KEY (`commenter_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `precedent_comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_comment_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица precedent_competence_group_link
CREATE TABLE IF NOT EXISTS `precedent_competence_group_link` (
  `precedent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `competence_group_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`precedent_id`,`competence_group_id`),
  KEY `FK_precedent_competence_group_link_competence_set_id` (`competence_set_id`),
  KEY `FK_precedent_competence_group_link_competence_group_id` (`competence_group_id`),
  CONSTRAINT `FK_precedent_competence_group_link_competence_group_id` FOREIGN KEY (`competence_group_id`) REFERENCES `competence_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_competence_group_link_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_competence_group_link_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица precedent_flash_group
CREATE TABLE IF NOT EXISTS `precedent_flash_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `precedent_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_precedent_flash_group_precedent_id` (`precedent_id`),
  CONSTRAINT `FK_precedent_flash_group_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица precedent_flash_group_user_link
CREATE TABLE IF NOT EXISTS `precedent_flash_group_user_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `precedent_flash_group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique` (`user_id`,`precedent_flash_group_id`),
  KEY `FK_precedent_flash_group_user_link_precedent_flash_group_id` (`precedent_flash_group_id`),
  CONSTRAINT `FK_precedent_flash_group_user_link_precedent_flash_group_id` FOREIGN KEY (`precedent_flash_group_id`) REFERENCES `precedent_flash_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_flash_group_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица precedent_group
CREATE TABLE IF NOT EXISTS `precedent_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_precedent_group_project_id` (`project_id`),
  CONSTRAINT `FK_precedent_group_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица precedent_group_link
CREATE TABLE IF NOT EXISTS `precedent_group_link` (
  `precedent_group_id` int(10) unsigned NOT NULL,
  `precedent_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`precedent_group_id`,`precedent_id`),
  KEY `FK_precedent_group_link_precedent_id` (`precedent_id`),
  CONSTRAINT `FK_precedent_group_link_precedent_group_id` FOREIGN KEY (`precedent_group_id`) REFERENCES `precedent_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_group_link_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица precedent_mark_adder
CREATE TABLE IF NOT EXISTS `precedent_mark_adder` (
  `precedent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adder_user_id` int(10) unsigned NOT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`precedent_id`,`adder_user_id`),
  KEY `FK_precedent_mark_adder_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_precedent_mark_adder_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_mark_adder_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица precedent_ratee_calc
CREATE TABLE IF NOT EXISTS `precedent_ratee_calc` (
  `precedent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ratee_user_id` int(10) unsigned NOT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`precedent_id`,`ratee_user_id`),
  KEY `FK_precedent_ratee_calc_ratee_user_id` (`ratee_user_id`),
  CONSTRAINT `FK_precedent_ratee_calc_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_ratee_calc_ratee_user_id` FOREIGN KEY (`ratee_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица precedent_rater_link
CREATE TABLE IF NOT EXISTS `precedent_rater_link` (
  `precedent_id` int(10) unsigned NOT NULL,
  `rater_user_id` int(10) unsigned NOT NULL,
  `comment` text,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`precedent_id`,`rater_user_id`),
  KEY `FK_precedent_rater_link_rater_user_id` (`rater_user_id`),
  CONSTRAINT `FK_precedent_rater_link_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_rater_link_rater_user_id` FOREIGN KEY (`rater_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица professiogram
CREATE TABLE IF NOT EXISTS `professiogram` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rate_id` int(10) unsigned DEFAULT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_professiogram_competence_set_id` (`competence_set_id`),
  KEY `FK_professiogram_rate_id` (`rate_id`),
  KEY `FK_professiogram_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_professiogram_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_professiogram_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_professiogram_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project
CREATE TABLE IF NOT EXISTS `project` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `name` varchar(40) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finish_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `main_url` varchar(255) NOT NULL DEFAULT '',
  `logo_big_height` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_big_width` int(10) unsigned NOT NULL DEFAULT '0',
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `main_url_human_calc` varchar(255) DEFAULT NULL,
  `precedent_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `user_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `vector_competence_set_id` int(10) unsigned DEFAULT NULL,
  `marks_competence_set_id` int(10) unsigned DEFAULT NULL,
  `use_game_name` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vector_self_must_skip` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vector_self_can_skip` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vector_self_can_skip_if_results` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `vector_focus_can_skip` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vector_focus_min_competence_count` int(10) unsigned NOT NULL DEFAULT '4',
  `vector_focus_max_competence_count` int(10) unsigned NOT NULL DEFAULT '10',
  `vector_is_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `role_recommendation_is_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `marks_are_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `events_are_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `endpoint_url` varchar(255) NOT NULL DEFAULT '',
  `endpoint_resend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `endpoint_is_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `descr_short` varchar(2048) DEFAULT NULL,
  `logo_small_width` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_small_height` int(10) unsigned NOT NULL DEFAULT '0',
  `materials_are_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `endpoint_connect_attempts_left` tinyint(1) unsigned NOT NULL DEFAULT '5',
  `endpoint_last_connect_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `endpoint_last_connect_result` varchar(40) NOT NULL DEFAULT '',
  `show_user_numbers` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `communities_are_on` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `community_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `join_premoderation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `access_read` enum('guest','user','member','moderator','admin') NOT NULL DEFAULT 'guest',
  `access_communities_add` enum('user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'user',
  `access_communities_read` enum('guest','user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
  `access_events_read` enum('guest','user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
  `access_events_comment` enum('user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'user',
  `access_materials_read` enum('guest','user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
  `access_materials_comment` enum('user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'user',
  `access_precedents_read` enum('guest','user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
  `access_precedents_comment` enum('user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'user',
  `event_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `material_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_main_url` varchar(255) DEFAULT NULL,
  `bg_body_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_body_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_body_main_url` varchar(255) DEFAULT NULL,
  `logo_version` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_version` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_body_version` int(10) unsigned NOT NULL DEFAULT '0',
  `custom_feed_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `domain_is_on` tinyint(4) NOT NULL DEFAULT '0',
  `domain_name` varchar(32) NOT NULL DEFAULT '',
  `favicon_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `favicon_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `favicon_main_url` varchar(255) DEFAULT NULL,
  `favicon_version` int(10) unsigned NOT NULL DEFAULT '0',
  `color_scheme` enum('green','blue','yellow','red') NOT NULL DEFAULT 'green',
  PRIMARY KEY (`id`),
  KEY `FK_project_vector_competence_set_id` (`vector_competence_set_id`),
  KEY `FK_project_marks_competence_set_id` (`marks_competence_set_id`),
  KEY `FK_project_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_project_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_marks_competence_set_id` FOREIGN KEY (`marks_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_vector_competence_set_id` FOREIGN KEY (`vector_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_admin
CREATE TABLE IF NOT EXISTS `project_admin` (
  `user_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_role_moderator` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_event_moderator` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_material_moderator` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_precedent_moderator` tinyint(1) NOT NULL DEFAULT '0',
  `is_mark_moderator` tinyint(1) NOT NULL DEFAULT '0',
  `is_community_moderator` tinyint(1) NOT NULL DEFAULT '0',
  `is_widget_moderator` tinyint(1) NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adder_admin_user_id` int(10) unsigned DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_admin_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`project_id`),
  KEY `FK_project_admin_project_id` (`project_id`),
  KEY `FK_project_admin_adder_admin_user_id` (`adder_admin_user_id`),
  KEY `FK_project_admin_editor_admin_user_id` (`editor_admin_user_id`),
  CONSTRAINT `FK_project_admin_adder_admin_user_id` FOREIGN KEY (`adder_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_admin_editor_admin_user_id` FOREIGN KEY (`editor_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_admin_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_admin_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_competence_set_link_calc
CREATE TABLE IF NOT EXISTS `project_competence_set_link_calc` (
  `project_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `personal_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`,`competence_set_id`),
  KEY `FK_project_competence_set_link_calc_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_project_competence_set_link_calc_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_competence_set_link_calc_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_custom_feed
CREATE TABLE IF NOT EXISTS `project_custom_feed` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(40) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `descr` text NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_project_custom_feed_project_id` (`project_id`) USING BTREE,
  CONSTRAINT `FK_project_custom_feed_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_custom_feed_source
CREATE TABLE IF NOT EXISTS `project_custom_feed_source` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `custom_feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `section_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique_item` (`custom_feed_id`,`community_id`,`section_id`) USING BTREE,
  KEY `FK_project_custom_feed_source_community_id` (`community_id`) USING BTREE,
  KEY `FK_project_custom_feed_source_section_id` (`section_id`) USING BTREE,
  CONSTRAINT `FK_project_custom_feed_source_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_custom_feed_source_custom_feed_id` FOREIGN KEY (`custom_feed_id`) REFERENCES `project_custom_feed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_custom_feed_source_section_id` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_import
CREATE TABLE IF NOT EXISTS `project_import` (
  `to_project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `from_project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `events_status` int(10) DEFAULT '0',
  `materials_status` int(10) DEFAULT '0',
  PRIMARY KEY (`to_project_id`,`from_project_id`),
  KEY `FK_project_import_from_project_id` (`from_project_id`),
  CONSTRAINT `FK_project_import_from_project_id` FOREIGN KEY (`from_project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_import_to_project_id` FOREIGN KEY (`to_project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_intmenu_item
CREATE TABLE IF NOT EXISTS `project_intmenu_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(60) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_project_intmenu_item_project_id` (`project_id`),
  CONSTRAINT `FK_project_intmenu_item_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_role
CREATE TABLE IF NOT EXISTS `project_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `based_on_professiogram_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_project_role_project_id` (`project_id`),
  KEY `FK_project_role_based_on_professiogram_id` (`based_on_professiogram_id`),
  KEY `FK_project_role_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_project_role_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_role_based_on_professiogram_id` FOREIGN KEY (`based_on_professiogram_id`) REFERENCES `professiogram` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_role_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_role_rate_link
CREATE TABLE IF NOT EXISTS `project_role_rate_link` (
  `project_role_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_role_id`,`rate_id`),
  KEY `FK_project_role_rate_link_rate_id` (`rate_id`),
  KEY `FK_project_role_rate_link_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_project_role_rate_link_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_role_rate_link_project_role_id` FOREIGN KEY (`project_role_id`) REFERENCES `project_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_role_rate_link_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_taxonomy_item
CREATE TABLE IF NOT EXISTS `project_taxonomy_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `position` int(10) unsigned NOT NULL,
  `type` set('module','static_page','link','folder') NOT NULL,
  `title` varchar(255) NOT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `static_page_name` varchar(255) DEFAULT NULL,
  `block_set_id` int(10) unsigned DEFAULT NULL,
  `module_name` varchar(150) DEFAULT NULL,
  `show_in_menu` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL,
  `edit_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_project_taxonomy_item_project_id` (`project_id`),
  KEY `FK_project_taxonomy_item_parent_id` (`parent_id`),
  KEY `FK_project_taxonomy_item_block_set_id` (`block_set_id`),
  CONSTRAINT `FK_project_taxonomy_item_block_set_id` FOREIGN KEY (`block_set_id`) REFERENCES `block_set` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_taxonomy_item_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `project_taxonomy_item` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_taxonomy_item_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_widget
CREATE TABLE IF NOT EXISTS `project_widget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `column` int(2) unsigned NOT NULL DEFAULT '1',
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  `title` varchar(100) NOT NULL DEFAULT '',
  `type_id` char(40) NOT NULL,
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  `old_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_project_widget_project_id` (`project_id`) USING BTREE,
  KEY `FK_project_widget_type_id` (`type_id`) USING BTREE,
  CONSTRAINT `FK_project_widget_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_widget_type_id` FOREIGN KEY (`type_id`) REFERENCES `project_widget_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_widget_communities
CREATE TABLE IF NOT EXISTS `project_widget_communities` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  PRIMARY KEY (`widget_id`,`community_id`),
  KEY `FK_project_widget_communities_2` (`community_id`),
  CONSTRAINT `FK_project_widget_communities_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `project_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_widget_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_widget_custom_feed
CREATE TABLE IF NOT EXISTS `project_widget_custom_feed` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `project_custom_feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`widget_id`),
  KEY `FK_project_widget_custom_feed_project_custom_feed_id` (`project_custom_feed_id`) USING BTREE,
  CONSTRAINT `FK_project_widget_custom_feed_project_feed_id` FOREIGN KEY (`project_custom_feed_id`) REFERENCES `project_custom_feed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_widget_custom_feed_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `project_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_widget_default
CREATE TABLE IF NOT EXISTS `project_widget_default` (
  `column` int(2) unsigned NOT NULL DEFAULT '1',
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  `title` varchar(100) NOT NULL DEFAULT '',
  `type_id` char(40) NOT NULL,
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`column`,`position`),
  KEY `FK_project_widget_default_type_id` (`type_id`) USING BTREE,
  CONSTRAINT `FK_project_widget_default_type_id` FOREIGN KEY (`type_id`) REFERENCES `project_widget_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_widget_text
CREATE TABLE IF NOT EXISTS `project_widget_text` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `html` text NOT NULL,
  PRIMARY KEY (`widget_id`),
  CONSTRAINT `FK_project_widget_text_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `project_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица project_widget_type
CREATE TABLE IF NOT EXISTS `project_widget_type` (
  `type_id` char(40) NOT NULL DEFAULT '',
  `title` char(100) DEFAULT NULL,
  `multiple_instances` tinyint(1) NOT NULL DEFAULT '0',
  `ctrl` char(100) NOT NULL,
  `edit_ctrl` char(100) NOT NULL DEFAULT '',
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица rate
CREATE TABLE IF NOT EXISTS `rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rate_type_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_rate_rate_type_id` (`rate_type_id`),
  CONSTRAINT `FK_rate_rate_type_id` FOREIGN KEY (`rate_type_id`) REFERENCES `rate_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица rate_competence_link
CREATE TABLE IF NOT EXISTS `rate_competence_link` (
  `rate_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `mark` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rate_id`,`competence_id`),
  KEY `FK_rate_competence_link_competence_id` (`competence_id`),
  CONSTRAINT `FK_rate_competence_link_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_rate_competence_link_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица rate_type
CREATE TABLE IF NOT EXISTS `rate_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `main_table` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица section
CREATE TABLE IF NOT EXISTS `section` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `community_id` int(10) unsigned NOT NULL,
  `descr` mediumtext,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `access_add_post` enum('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user',
  `access_comment` enum('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user',
  `access_default` tinyint(1) NOT NULL DEFAULT '1',
  `old_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique` (`name`,`community_id`) USING HASH,
  KEY `FK_section_community_id` (`community_id`),
  CONSTRAINT `FK_section_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sys_params
CREATE TABLE IF NOT EXISTS `sys_params` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(60) NOT NULL DEFAULT '',
  `info_email` varchar(100) NOT NULL,
  `main_page_text` mediumtext,
  `copyright_start_year` char(4) NOT NULL DEFAULT '1900',
  `is_devyourself` tinyint(1) NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ga_tracking_id` varchar(40) NOT NULL DEFAULT '',
  `logo_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_version` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица university
CREATE TABLE IF NOT EXISTS `university` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(40) NOT NULL,
  `title_full` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL DEFAULT '',
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sex` enum('m','f','?') NOT NULL DEFAULT '?',
  `first_name` varchar(60) DEFAULT NULL,
  `mid_name` varchar(60) DEFAULT NULL,
  `last_name` varchar(60) DEFAULT NULL,
  `photo_large_width` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_large_height` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_big_width` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_big_height` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_mid_width` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_mid_height` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_small_width` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_small_height` int(10) unsigned NOT NULL DEFAULT '0',
  `profile_url` varchar(255) NOT NULL DEFAULT '',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `university_id` int(10) unsigned DEFAULT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_large_url` varchar(255) DEFAULT NULL,
  `photo_big_url` varchar(255) DEFAULT NULL,
  `photo_mid_url` varchar(255) DEFAULT NULL,
  `photo_small_url` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `skype` varchar(100) DEFAULT NULL,
  `phone` varchar(40) DEFAULT NULL,
  `icq` varchar(12) DEFAULT NULL,
  `www` varchar(255) DEFAULT NULL,
  `www_human_calc` varchar(255) DEFAULT NULL,
  `about` text,
  `profile_id_bak` int(10) unsigned DEFAULT NULL,
  `register_ip` varchar(15) DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pass_recovery_key` varchar(40) DEFAULT NULL,
  `pass_recovery_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `allow_email_on_reply` tinyint(1) NOT NULL DEFAULT '1',
  `allow_email_on_premoderation` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `photo_version` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `KEY_user_login` (`login`),
  KEY `FK_user_university_id` (`university_id`),
  CONSTRAINT `FK_user_university_id` FOREIGN KEY (`university_id`) REFERENCES `university` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица user_competence_set_link_calc
CREATE TABLE IF NOT EXISTS `user_competence_set_link_calc` (
  `user_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `project_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `personal_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `project_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `default_vector_id_calc` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`competence_set_id`),
  KEY `FK_user_competence_set_link_calc_competence_set_id` (`competence_set_id`),
  KEY `KEY_user_competence_set_link_calc_default_vector_id_calc` (`default_vector_id_calc`),
  CONSTRAINT `FK_user_competence_set_link_calc_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_competence_set_link_calc_default_vector_id_calc` FOREIGN KEY (`default_vector_id_calc`) REFERENCES `vector` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_user_competence_set_link_calc_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица user_competence_set_project_link
CREATE TABLE IF NOT EXISTS `user_competence_set_project_link` (
  `user_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`competence_set_id`,`project_id`),
  KEY `FK_user_competence_set_project_link_competence_set_id` (`competence_set_id`),
  KEY `FK_user_competence_set_project_link_project_id` (`project_id`),
  CONSTRAINT `FK_user_competence_set_project_link_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_competence_set_project_link_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_competence_set_project_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица user_group
CREATE TABLE IF NOT EXISTS `user_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица user_project_link
CREATE TABLE IF NOT EXISTS `user_project_link` (
  `project_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('deleted','pretender','member','moderator','admin') NOT NULL DEFAULT 'deleted',
  `is_analyst` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `game_name` varchar(100) DEFAULT NULL,
  `last_vector_id_calc` int(10) unsigned DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `number` int(10) unsigned NOT NULL DEFAULT '0',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `FK_user_project_link_user_id` (`user_id`),
  KEY `KEY_user_project_link_last_vector_id_calc` (`last_vector_id_calc`),
  CONSTRAINT `FK_user_project_link_last_vector_id_calc` FOREIGN KEY (`last_vector_id_calc`) REFERENCES `vector` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_user_project_link_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_project_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица user_user_group_link
CREATE TABLE IF NOT EXISTS `user_user_group_link` (
  `user_id` int(10) unsigned NOT NULL,
  `user_group_id` int(10) unsigned NOT NULL,
  `rights` enum('member','admin') NOT NULL DEFAULT 'member',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`user_group_id`),
  KEY `FK_user_user_group_link_user_group_id` (`user_group_id`),
  CONSTRAINT `FK_user_user_group_link_user_group_id` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_user_group_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица vector
CREATE TABLE IF NOT EXISTS `vector` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `future_professiogram_count_calc` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `focus_competence_count_calc` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `calculations_base` enum('self','results') NOT NULL DEFAULT 'results',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `future_edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `self_edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `focus_edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_vector_project_id` (`project_id`),
  KEY `FK_vector_competence_set_id` (`competence_set_id`),
  KEY `Index_6` (`user_id`,`competence_set_id`,`project_id`),
  CONSTRAINT `FK_vector_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица vector_focus
CREATE TABLE IF NOT EXISTS `vector_focus` (
  `vector_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`vector_id`,`competence_id`),
  KEY `FK_vector_focus_competence_id` (`competence_id`),
  CONSTRAINT `FK_vector_focus_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_focus_vector_id` FOREIGN KEY (`vector_id`) REFERENCES `vector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица vector_focus_history
CREATE TABLE IF NOT EXISTS `vector_focus_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vector_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_vector_focus_history_vector_id` (`vector_id`),
  CONSTRAINT `FK_vector_focus_history_vector_id` FOREIGN KEY (`vector_id`) REFERENCES `vector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица vector_future
CREATE TABLE IF NOT EXISTS `vector_future` (
  `vector_id` int(10) unsigned NOT NULL,
  `professiogram_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`vector_id`,`professiogram_id`),
  KEY `FK_vector_future_professiogram_id` (`professiogram_id`),
  CONSTRAINT `FK_vector_future_professiogram_id` FOREIGN KEY (`professiogram_id`) REFERENCES `professiogram` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_future_vector_id` FOREIGN KEY (`vector_id`) REFERENCES `vector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица vector_future_competences_calc
CREATE TABLE IF NOT EXISTS `vector_future_competences_calc` (
  `vector_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `mark` int(10) unsigned NOT NULL,
  PRIMARY KEY (`vector_id`,`competence_id`),
  KEY `FK_vector_future_competences_calc_competence_id` (`competence_id`),
  CONSTRAINT `FK_vector_future_competences_calc_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_future_competences_calc_vector_id` FOREIGN KEY (`vector_id`) REFERENCES `vector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица vector_future_history
CREATE TABLE IF NOT EXISTS `vector_future_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vector_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_vector_future_history_vector_id` (`vector_id`),
  CONSTRAINT `FK_vector_future_history_vector_id` FOREIGN KEY (`vector_id`) REFERENCES `vector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица vector_self
CREATE TABLE IF NOT EXISTS `vector_self` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `competence_id` int(10) unsigned NOT NULL,
  `mark` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`competence_id`),
  KEY `FK_vector_self_competence_id` (`competence_id`),
  KEY `FK_vector_self_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_vector_self_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_self_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_self_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица vector_self_history
CREATE TABLE IF NOT EXISTS `vector_self_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_vector_self_history_user_id` (`user_id`),
  KEY `FK_vector_self_history_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_vector_self_history_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_self_history_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица vector_self_stat
CREATE TABLE IF NOT EXISTS `vector_self_stat` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `competence_set_id` int(10) unsigned NOT NULL,
  `self_competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`competence_set_id`),
  KEY `FK_vector_self_stat_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_vector_self_stat_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_self_stat_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для триггер community_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `community_after_insert` AFTER INSERT ON `community` FOR EACH ROW BEGIN
	CALL community_add_count_calc(NEW.id, NEW.project_id, NEW.post_count_calc, NEW.comment_count_calc);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер community_after_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `community_after_update` AFTER UPDATE ON `community` FOR EACH ROW BEGIN
	IF OLD.is_deleted <> NEW.is_deleted
	THEN
		IF NEW.is_deleted
		THEN
			CALL community_delete_count_calc(OLD.id, OLD.project_id, OLD.post_count_calc, OLD.comment_count_calc);
			
			DELETE FROM communities_menu_item WHERE community_id = OLD.id;
			DELETE FROM project_widget_communities WHERE community_id = OLD.id;
			DELETE FROM community_widget_communities WHERE community_id = OLD.id;
			DELETE FROM project_custom_feed_source WHERE community_id = OLD.id;
			DELETE FROM community_custom_feed_source WHERE community_id = OLD.id;

			DELETE FROM post_calc WHERE community_id = OLD.id;
		ELSE
			CALL community_add_count_calc(NEW.id, NEW.project_id, NEW.post_count_calc, NEW.comment_count_calc);
			INSERT INTO post_calc (id, author_user_id, project_id, community_id, section_id, hide_in_project_feed)
			(
				SELECT id, author_user_id, NEW.project_id, community_id, section_id, hide_in_project_feed 
				FROM post 
				WHERE community_id = NEW.id AND is_deleted = 0
			);
		END IF;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер community_custom_feed_after_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `community_custom_feed_after_delete` AFTER DELETE ON `community_custom_feed` FOR EACH ROW BEGIN
	UPDATE community
	SET 
		custom_feed_count_calc = custom_feed_count_calc - 1
	WHERE id = OLD.community_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер community_custom_feed_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `community_custom_feed_after_insert` AFTER INSERT ON `community_custom_feed` FOR EACH ROW BEGIN
	UPDATE community
	SET 
		custom_feed_count_calc = custom_feed_count_calc + 1
	WHERE id = NEW.community_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер event_after_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `event_after_delete` AFTER DELETE ON `event` FOR EACH ROW BEGIN
	UPDATE project
	SET 
		comment_count_calc = comment_count_calc - OLD.comment_count_calc,
		event_count_calc = event_count_calc - 1
	WHERE id = OLD.project_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер event_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `event_after_insert` AFTER INSERT ON `event` FOR EACH ROW BEGIN
	UPDATE project
	SET 
		event_count_calc = event_count_calc + 1
	WHERE id = NEW.project_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер mark_calc_after_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `mark_calc_after_delete` AFTER DELETE ON `mark_calc` FOR EACH ROW BEGIN
	DECLARE personal_mark_inc INT DEFAULT 0;
	DECLARE group_mark_inc INT DEFAULT 0;
	DECLARE group_mark_raw_inc INT DEFAULT 0;
	
	IF OLD.type = 'personal'
	THEN
		SET personal_mark_inc = -1;
	ELSE
		SET group_mark_inc = -1;
		IF (
			SELECT COUNT(*)
			FROM mark_calc
			WHERE
				precedent_flash_group_id = OLD.precedent_flash_group_id
				AND rater_user_id = OLD.rater_user_id
				AND competence_id = OLD.competence_id
				AND type = 'group'
			) = 0
		THEN
			SET group_mark_raw_inc = -1;
		END IF;
	END IF;

	UPDATE competence_set
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.competence_set_id;

	UPDATE competence_full
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.competence_id;

	UPDATE competence_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		competence_id = OLD.competence_id;

	UPDATE precedent
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.precedent_id;

	UPDATE project
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.project_id;

	UPDATE user
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.ratee_user_id;

	UPDATE precedent_rater_link
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		precedent_id = OLD.precedent_id
		AND rater_user_id = OLD.rater_user_id;

	UPDATE precedent_mark_adder
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		precedent_id = OLD.precedent_id
		AND adder_user_id = OLD.adder_user_id;

	UPDATE precedent_ratee_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		precedent_id = OLD.precedent_id
		AND ratee_user_id = OLD.ratee_user_id;

	UPDATE project_competence_set_link_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		project_id = OLD.project_id
		AND competence_set_id = OLD.competence_set_id;

	UPDATE user_competence_set_link_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		user_id = OLD.ratee_user_id
		AND competence_set_id = OLD.competence_set_id;

	UPDATE user_competence_set_project_link
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		user_id = OLD.ratee_user_id
		AND competence_set_id = OLD.competence_set_id
		AND project_id = OLD.project_id;

	UPDATE user_project_link
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		user_id = OLD.ratee_user_id
		AND project_id = OLD.project_id;

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер mark_calc_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `mark_calc_after_insert` AFTER INSERT ON `mark_calc` FOR EACH ROW BEGIN
	DECLARE personal_mark_inc INT DEFAULT 0;
	DECLARE group_mark_inc INT DEFAULT 0;
	DECLARE group_mark_raw_inc INT DEFAULT 0;
	
	IF NEW.type = 'personal'
	THEN
		SET personal_mark_inc = 1;
	ELSE
		SET group_mark_inc = 1;
		IF (
			SELECT COUNT(*)
			FROM mark_calc
			WHERE
				precedent_flash_group_id = NEW.precedent_flash_group_id
				AND rater_user_id = NEW.rater_user_id
				AND competence_id = NEW.competence_id
				AND type = 'group'
			) = 1
		THEN
			SET group_mark_raw_inc = 1;
		END IF;
	END IF;

	UPDATE competence_set
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.competence_set_id;

	UPDATE competence_full
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.competence_id;

	UPDATE competence_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		competence_id = NEW.competence_id;

	UPDATE precedent
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.precedent_id;

	UPDATE project
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.project_id;

	UPDATE user
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.ratee_user_id;

	INSERT INTO precedent_rater_link
	SET
		precedent_id = NEW.precedent_id,
		rater_user_id = NEW.rater_user_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO precedent_mark_adder
	SET
		precedent_id = NEW.precedent_id,
		adder_user_id = NEW.adder_user_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO precedent_ratee_calc
	SET
		precedent_id = NEW.precedent_id,
		ratee_user_id = NEW.ratee_user_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO project_competence_set_link_calc
	SET
		project_id = NEW.project_id,
		competence_set_id = NEW.competence_set_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO user_competence_set_link_calc
	SET
		user_id = NEW.ratee_user_id,
		competence_set_id = NEW.competence_set_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO user_competence_set_project_link
	SET
		user_id = NEW.ratee_user_id,
		competence_set_id = NEW.competence_set_id,
		project_id = NEW.project_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;
	
	INSERT INTO user_project_link
	SET
		user_id = NEW.ratee_user_id,
		project_id = NEW.project_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер mark_calc_after_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `mark_calc_after_update` AFTER UPDATE ON `mark_calc` FOR EACH ROW BEGIN
	DECLARE personal_mark_inc INT DEFAULT 0;
	DECLARE group_mark_inc INT DEFAULT 0;
	DECLARE group_mark_raw_inc INT DEFAULT 0;

	IF NEW.adder_user_id != OLD.adder_user_id
	THEN
		IF NEW.type = 'personal'
		THEN
			SET personal_mark_inc = 1;
		ELSE
			SET group_mark_inc = 1;
			IF (
				SELECT COUNT(*)
				FROM mark_calc
				WHERE
					precedent_flash_group_id = NEW.precedent_flash_group_id
					AND rater_user_id = NEW.rater_user_id
					AND adder_user_id = NEW.adder_user_id
					AND competence_id = NEW.competence_id
					AND type = 'group'
				) = 1
			THEN
				SET group_mark_raw_inc = 1;
			END IF;
		END IF;

		UPDATE precedent_mark_adder
		SET
			personal_mark_count_calc = personal_mark_count_calc - personal_mark_inc,
			group_mark_raw_count_calc = group_mark_raw_count_calc - group_mark_raw_inc,
			group_mark_count_calc = group_mark_count_calc - group_mark_inc,
			total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
		WHERE
			precedent_id = NEW.precedent_id
			AND adder_user_id = OLD.adder_user_id;

		UPDATE precedent_mark_adder
		SET
			personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
			group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
			group_mark_count_calc = group_mark_count_calc + group_mark_inc,
			total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
		WHERE
			precedent_id = NEW.precedent_id
			AND adder_user_id = NEW.adder_user_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер mark_group_after_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `mark_group_after_delete` AFTER DELETE ON `mark_group` FOR EACH ROW BEGIN
	DELETE FROM mark_calc
	WHERE
		precedent_flash_group_id = OLD.precedent_flash_group_id
		AND rater_user_id = OLD.rater_user_id
		AND competence_id = OLD.competence_id
		AND type = 'group';
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер mark_group_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `mark_group_after_insert` AFTER INSERT ON `mark_group` FOR EACH ROW BEGIN
	DECLARE my_precedent_id INT;
	DECLARE my_project_id INT;
	DECLARE my_competence_set_id INT;

	SET my_precedent_id = 
	(
		SELECT precedent_id
		FROM precedent_flash_group
		WHERE id = NEW.precedent_flash_group_id
	);

	SET my_project_id =
	(
		SELECT project_id
		FROM precedent
		WHERE id = my_precedent_id
	);

	SET my_competence_set_id =
	(
		SELECT competence_set_id
		FROM competence_full
		WHERE id = NEW.competence_id
	);

	
	

	INSERT INTO mark_calc
	(
		project_id,
		precedent_flash_group_id,
		precedent_flash_group_user_link_id,
		ratee_user_id,
		rater_user_id,
		adder_user_id,
		competence_id,
		competence_set_id,
		type,
		value,
		comment,
		precedent_id
	)
	SELECT
		my_project_id,
		NEW.precedent_flash_group_id,
		pfgul.id,
		pfgul.user_id,
		NEW.rater_user_id,
		NEW.adder_user_id,
		NEW.competence_id,
		my_competence_set_id,
		'group',
		NEW.value,
		NEW.comment,
		my_precedent_id
	FROM
		precedent_flash_group_user_link AS pfgul
	WHERE
		pfgul.precedent_flash_group_id = NEW.precedent_flash_group_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер mark_group_after_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `mark_group_after_update` AFTER UPDATE ON `mark_group` FOR EACH ROW BEGIN
	UPDATE mark_calc
	SET
		value = NEW.value,
		adder_user_id = NEW.adder_user_id,
		comment = NEW.comment
	WHERE
		precedent_flash_group_id = OLD.precedent_flash_group_id
		AND rater_user_id = OLD.rater_user_id
		AND competence_id = OLD.competence_id
		AND type = 'group';
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер mark_personal_after_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `mark_personal_after_delete` AFTER DELETE ON `mark_personal` FOR EACH ROW BEGIN
	DELETE FROM mark_calc
	WHERE
		precedent_flash_group_user_link_id = OLD.precedent_flash_group_user_link_id
		AND rater_user_id = OLD.rater_user_id
		AND competence_id = OLD.competence_id
		AND type = 'personal';
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер mark_personal_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `mark_personal_after_insert` AFTER INSERT ON `mark_personal` FOR EACH ROW BEGIN
	DECLARE my_ratee_user_id INT;
	DECLARE my_precedent_flash_group_id INT;
	DECLARE my_precedent_id INT;
	DECLARE my_project_id INT;
	DECLARE my_competence_set_id INT;

	SELECT
		user_id,
		precedent_flash_group_id
	INTO
		my_ratee_user_id,
		my_precedent_flash_group_id
	FROM precedent_flash_group_user_link
	WHERE id = NEW.precedent_flash_group_user_link_id;
	
	SET my_precedent_id = 
	(
		SELECT pfg.precedent_id
		FROM
			precedent_flash_group_user_link AS pfgul,
			precedent_flash_group AS pfg
		WHERE
			pfgul.id = NEW.precedent_flash_group_user_link_id
			AND pfg.id = pfgul.precedent_flash_group_id
	);

	SET my_project_id =
	(
		SELECT project_id
		FROM precedent
		WHERE id = my_precedent_id
	);

	SET my_competence_set_id =
	(
		SELECT competence_set_id
		FROM competence_full
		WHERE id = NEW.competence_id
	);

	INSERT INTO mark_calc
	SET
		project_id = my_project_id,
		precedent_flash_group_id = my_precedent_flash_group_id,
		precedent_flash_group_user_link_id = NEW.precedent_flash_group_user_link_id,
		ratee_user_id = my_ratee_user_id,
		rater_user_id = NEW.rater_user_id,
		adder_user_id = NEW.adder_user_id,
		competence_id = NEW.competence_id,
		competence_set_id = my_competence_set_id,
		type = 'personal',
		value = NEW.value,
		comment = NEW.comment,
		precedent_id = my_precedent_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер mark_personal_after_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `mark_personal_after_update` AFTER UPDATE ON `mark_personal` FOR EACH ROW BEGIN
	UPDATE mark_calc
	SET
		value = NEW.value,
		adder_user_id = NEW.adder_user_id,
		comment = NEW.comment
	WHERE
		precedent_flash_group_user_link_id = OLD.precedent_flash_group_user_link_id
		AND rater_user_id = OLD.rater_user_id
		AND competence_id = OLD.competence_id
		AND type = 'personal';
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер material_after_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `material_after_delete` AFTER DELETE ON `material` FOR EACH ROW BEGIN
	UPDATE project
	SET 
		comment_count_calc = comment_count_calc - OLD.comment_count_calc,
		material_count_calc = material_count_calc - 1
	WHERE id = OLD.project_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер material_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `material_after_insert` AFTER INSERT ON `material` FOR EACH ROW BEGIN
	UPDATE project
	SET 
		material_count_calc = material_count_calc + 1
	WHERE id = NEW.project_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер post_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `post_after_insert` AFTER INSERT ON `post` FOR EACH ROW BEGIN
	SET @project_id = (SELECT project_id FROM community WHERE id = NEW.community_id);
	INSERT INTO post_calc
	SET
		id = NEW.id,
		author_user_id = NEW.author_user_id,
		project_id = @project_id,
		community_id = NEW.community_id,
		section_id = NEW.section_id,
		hide_in_project_feed = NEW.hide_in_project_feed,
		comment_count_calc = NEW.comment_count_calc,
		last_comment_id = NEW.last_comment_id,
		last_comment_author_user_id = NEW.last_comment_author_user_id,
		last_comment_html = NEW.last_comment_html;

	CALL post_add_count_calc(NEW.id, NEW.section_id, NEW.community_id, @project_id, NEW.author_user_id, NEW.comment_count_calc);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер post_after_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `post_after_update` AFTER UPDATE ON `post` FOR EACH ROW BEGIN
	DECLARE my_project_id INT;
	SET my_project_id = (SELECT project_id FROM community WHERE id = NEW.community_id);
	IF OLD.is_deleted <> NEW.is_deleted
	THEN
		IF NEW.is_deleted
		THEN
			DELETE FROM post_calc WHERE id = OLD.id;
			CALL post_delete_count_calc(OLD.id, OLD.section_id, OLD.community_id, my_project_id, OLD.author_user_id, OLD.comment_count_calc);
		ELSE
			INSERT INTO post_calc
			SET
				id = NEW.id,
				author_user_id = NEW.author_user_id,
				project_id = my_project_id,
				community_id = NEW.community_id,
				section_id = NEW.section_id,
				hide_in_project_feed = NEW.hide_in_project_feed,
				comment_count_calc = NEW.comment_count_calc,
				last_comment_id = NEW.last_comment_id,
				last_comment_author_user_id = NEW.last_comment_author_user_id,
				last_comment_html = NEW.last_comment_html;
			CALL post_add_count_calc(NEW.id, NEW.section_id, NEW.community_id, @project_id, NEW.author_user_id, NEW.comment_count_calc);
		END IF;
	ELSEIF NEW.is_deleted = 0
	THEN
		UPDATE post_calc 
		SET
				id = NEW.id,
				author_user_id = NEW.author_user_id,
				project_id = my_project_id,
				community_id = NEW.community_id,
				section_id = NEW.section_id,
				hide_in_project_feed = NEW.hide_in_project_feed,
				comment_count_calc = NEW.comment_count_calc,
				last_comment_id = NEW.last_comment_id,
				last_comment_author_user_id = NEW.last_comment_author_user_id,
				last_comment_html = NEW.last_comment_html
		WHERE id = NEW.id;
		IF IFNULL(NEW.section_id <> OLD.section_id, 1)
		THEN
			UPDATE section SET post_count_calc = post_count_calc + 1 WHERE id = NEW.section_id;
			UPDATE section SET post_count_calc = post_count_calc - 1 WHERE id = OLD.section_id;
		END IF;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер precedent_after_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `precedent_after_delete` AFTER DELETE ON `precedent` FOR EACH ROW BEGIN
	UPDATE project
	SET precedent_count_calc = precedent_count_calc - 1
	WHERE id = OLD.project_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер precedent_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `precedent_after_insert` AFTER INSERT ON `precedent` FOR EACH ROW BEGIN
	UPDATE project
	SET precedent_count_calc = precedent_count_calc + 1
	WHERE id = NEW.project_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер precedent_flash_group_user_link_after_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `precedent_flash_group_user_link_after_delete` AFTER DELETE ON `precedent_flash_group_user_link` FOR EACH ROW BEGIN
	DELETE FROM mark_calc
	WHERE
		precedent_flash_group_user_link_id = OLD.id
		AND type='group';
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер precedent_flash_group_user_link_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `precedent_flash_group_user_link_after_insert` AFTER INSERT ON `precedent_flash_group_user_link` FOR EACH ROW BEGIN
	INSERT INTO mark_calc
	(
		project_id,
		precedent_group_id,
		precedent_flash_group_id,
		precedent_flash_group_user_link_id,
		ratee_user_id,
		rater_user_id,
		adder_user_id,
		competence_id,
		competence_set_id,
		type,
		value,
		comment,
		precedent_id
	)
	SELECT
		project_id,
		precedent_group_id,
		precedent_flash_group_id,
		NEW.id AS precedent_flash_group_user_link_id,
		NEW.user_id AS ratee_user_id,
		rater_user_id,
		adder_user_id,
		competence_id,
		competence_set_id,
		type,
		value,
		comment,
		precedent_id
	FROM
		mark_calc
	WHERE
		precedent_flash_group_id = NEW.precedent_flash_group_id
		AND type='group'
	GROUP BY
		competence_id,
		rater_user_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер project_custom_feed_after_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `project_custom_feed_after_delete` AFTER DELETE ON `project_custom_feed` FOR EACH ROW BEGIN
	UPDATE project
	SET 
		custom_feed_count_calc = custom_feed_count_calc - 1
	WHERE id = OLD.project_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Дамп структуры для триггер project_custom_feed_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `project_custom_feed_after_insert` AFTER INSERT ON `project_custom_feed` FOR EACH ROW BEGIN
	UPDATE project
	SET 
		custom_feed_count_calc = custom_feed_count_calc + 1
	WHERE id = NEW.project_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
