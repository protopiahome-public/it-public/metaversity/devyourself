CREATE PROCEDURE `community_add_count_calc`(
		IN community_id INTEGER(11),
		IN project_id INTEGER(11),
		IN community_post_count_calc INTEGER(11),
		IN community_comment_count_calc INTEGER(11)
	)
BEGIN
	UPDATE project
	SET 
		community_count_calc = community_count_calc + 1,
		post_count_calc = post_count_calc + community_post_count_calc,
		comment_count_calc = comment_count_calc + community_comment_count_calc
	WHERE id = project_id;
END;