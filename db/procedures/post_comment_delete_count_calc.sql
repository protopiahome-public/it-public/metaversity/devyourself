CREATE PROCEDURE `post_comment_delete_count_calc`(
		IN in_post_id INTEGER(11),
		IN in_community_id INTEGER(11),
		IN in_project_id INTEGER(11),
		IN in_author_user_id INTEGER(11)
	)
BEGIN
	UPDATE post
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_post_id;

	UPDATE community
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_community_id;

	INSERT IGNORE community_user_link (community_id, user_id, status) 
	VALUES (in_community_id, in_author_user_id, 'deleted');
	UPDATE community_user_link
	SET comment_count_calc = comment_count_calc - 1
	WHERE user_id = in_author_user_id AND community_id = in_community_id;

	INSERT IGNORE user_project_link (project_id, user_id, status) 
	VALUES (in_project_id, in_author_user_id, 'deleted');
	UPDATE user_project_link
	SET comment_count_calc = comment_count_calc - 1
	WHERE user_id = in_author_user_id AND project_id = in_project_id;

	UPDATE project
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_project_id;
END;