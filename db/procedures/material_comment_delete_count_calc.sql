CREATE PROCEDURE `material_comment_delete_count_calc`(
		IN in_material_id INTEGER(11),
		IN in_project_id INTEGER(11),
		IN in_author_user_id INTEGER(11)
	)
BEGIN
	UPDATE material
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_material_id;

	UPDATE project
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_project_id;

	INSERT IGNORE user_project_link (project_id, user_id, status) 
	VALUES (in_project_id, in_author_user_id, 'deleted');
	UPDATE user_project_link
	SET comment_count_calc = comment_count_calc - 1
	WHERE user_id = in_author_user_id AND project_id = in_project_id;
END;