CREATE PROCEDURE `post_delete_count_calc`(
		IN in_post_id INTEGER(11),
		IN in_section_id INTEGER(11),
		IN in_community_id INTEGER(11),
		IN in_project_id INTEGER(11),
		IN in_author_user_id INTEGER(11),
		IN in_post_comment_count_calc INTEGER(11)
	)
BEGIN
	UPDATE project
	SET 
		post_count_calc = post_count_calc - 1, 
		comment_count_calc = comment_count_calc - in_post_comment_count_calc
	WHERE id = in_project_id;

	UPDATE community
	SET 
		post_count_calc = post_count_calc - 1, 
		comment_count_calc = comment_count_calc - in_post_comment_count_calc
	WHERE id = in_community_id;

	IF in_section_id IS NOT NULL
	THEN
		UPDATE section
		SET post_count_calc = post_count_calc - 1
		WHERE id = in_section_id;
	END IF;

	INSERT IGNORE community_user_link (community_id, user_id, status) 
	VALUES (in_community_id, in_author_user_id, 'deleted');

	UPDATE community_user_link
	SET post_count_calc = post_count_calc - 1
	WHERE community_id = in_community_id AND user_id = in_author_user_id;

	INSERT IGNORE user_project_link (project_id, user_id, status) 
	VALUES (in_project_id, in_author_user_id, 'deleted');

	UPDATE user_project_link
	SET post_count_calc = post_count_calc - 1
	WHERE project_id = in_project_id AND user_id = in_author_user_id;
END;