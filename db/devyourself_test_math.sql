-- MySQL dump 10.13  Distrib 5.1.48, for Win32 (ia32)
--
-- Host: localhost    Database: cmp_new_test_math
-- ------------------------------------------------------
-- Server version	5.1.48-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `api_user_update`
--

DROP TABLE IF EXISTS `api_user_update`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_user_update` (
  `project_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `FK_api_user_update_user_id` (`user_id`),
  CONSTRAINT `FK_api_user_update_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_api_user_update_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_user_update`
--

LOCK TABLES `api_user_update` WRITE;
/*!40000 ALTER TABLE `api_user_update` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_user_update` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_file`
--

DROP TABLE IF EXISTS `block_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `block_set_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `ext` varchar(5) NOT NULL DEFAULT '',
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(10) NOT NULL DEFAULT '',
  `file_name` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_fake` tinyint(1) NOT NULL DEFAULT '0',
  `fake_for_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_block_file_block_set_id` (`block_set_id`),
  KEY `FK_block_file_fake_for_id` (`fake_for_id`),
  CONSTRAINT `FK_block_file_block_set_id` FOREIGN KEY (`block_set_id`) REFERENCES `block_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_block_file_fake_for_id` FOREIGN KEY (`fake_for_id`) REFERENCES `block_file` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_file`
--

LOCK TABLES `block_file` WRITE;
/*!40000 ALTER TABLE `block_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `block_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_set`
--

DROP TABLE IF EXISTS `block_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_set` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `xml` longtext NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_set`
--

LOCK TABLES `block_set` WRITE;
/*!40000 ALTER TABLE `block_set` DISABLE KEYS */;
/*!40000 ALTER TABLE `block_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_video`
--

DROP TABLE IF EXISTS `block_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_video` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `block_set_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `html` mediumtext NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `fake_for_id` int(10) unsigned DEFAULT NULL,
  `is_fake` tinyint(1) NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_block_video_block_set_id` (`block_set_id`),
  KEY `FK_block_video_fake_for_id` (`fake_for_id`),
  CONSTRAINT `FK_block_video_block_set_id` FOREIGN KEY (`block_set_id`) REFERENCES `block_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_block_video_fake_for_id` FOREIGN KEY (`fake_for_id`) REFERENCES `block_video` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_video`
--

LOCK TABLES `block_video` WRITE;
/*!40000 ALTER TABLE `block_video` DISABLE KEYS */;
/*!40000 ALTER TABLE `block_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned DEFAULT NULL,
  `author_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `html` text NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `old_id` int(10) unsigned DEFAULT NULL,
  `old_parent_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_comment_parent_id` (`parent_id`),
  KEY `FK_comment_type_id` (`type_id`),
  KEY `FK_comment_author_user_id` (`author_user_id`),
  CONSTRAINT `FK_comment_author_user_id` FOREIGN KEY (`author_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_comment_type_id` FOREIGN KEY (`type_id`) REFERENCES `comment_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment_subscription`
--

DROP TABLE IF EXISTS `comment_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_subscription` (
  `type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`type_id`,`object_id`,`user_id`),
  KEY `FK_comment_subscription_user_id` (`user_id`),
  CONSTRAINT `FK_comment_subscription_type_id` FOREIGN KEY (`type_id`) REFERENCES `comment_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_comment_subscription_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment_subscription`
--

LOCK TABLES `comment_subscription` WRITE;
/*!40000 ALTER TABLE `comment_subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment_subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment_type`
--

DROP TABLE IF EXISTS `comment_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment_type`
--

LOCK TABLES `comment_type` WRITE;
/*!40000 ALTER TABLE `comment_type` DISABLE KEYS */;
INSERT INTO `comment_type` VALUES (1,'event'),(2,'material'),(3,'post');
/*!40000 ALTER TABLE `comment_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communities_menu_dd_item`
--

DROP TABLE IF EXISTS `communities_menu_dd_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communities_menu_dd_item` (
  `community_id` int(10) unsigned DEFAULT '0',
  `project_id` int(10) unsigned DEFAULT '0',
  `position` int(10) unsigned DEFAULT '0',
  `add_time` datetime DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime DEFAULT '0000-00-00 00:00:00',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `FK_communities_menu_dd_project_id` (`project_id`),
  KEY `unique` (`community_id`,`project_id`),
  CONSTRAINT `FK_communities_menu_dd_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_communities_menu_dd_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communities_menu_dd_item`
--

LOCK TABLES `communities_menu_dd_item` WRITE;
/*!40000 ALTER TABLE `communities_menu_dd_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `communities_menu_dd_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communities_menu_item`
--

DROP TABLE IF EXISTS `communities_menu_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communities_menu_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_communities_menu_project_id` (`project_id`),
  KEY `unique` (`community_id`,`project_id`),
  CONSTRAINT `FK_communities_menu_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_communities_menu_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communities_menu_item`
--

LOCK TABLES `communities_menu_item` WRITE;
/*!40000 ALTER TABLE `communities_menu_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `communities_menu_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community`
--

DROP TABLE IF EXISTS `community`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `descr_short` varchar(140) NOT NULL DEFAULT '',
  `logo_big_width` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_big_height` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_small_width` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_small_height` int(10) unsigned NOT NULL DEFAULT '0',
  `project_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `allow_posting_without_section` tinyint(1) NOT NULL DEFAULT '1',
  `member_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `join_premoderation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `bg_head_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_main_url` varchar(255) DEFAULT NULL,
  `bg_body_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_body_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_body_main_url` varchar(255) DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `parent_approved` tinyint(1) NOT NULL DEFAULT '0',
  `allow_child_communities` enum('nobody','user','user_premoderation') NOT NULL DEFAULT 'nobody',
  `access_read` enum('guest','user','project_member','member','moderator','admin') NOT NULL DEFAULT 'guest',
  `access_add_post` enum('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user',
  `access_comment` enum('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user',
  `logo_version` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_version` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_body_version` int(10) unsigned NOT NULL DEFAULT '0',
  `custom_feed_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `child_community_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `old_id` int(10) unsigned DEFAULT NULL,
  `old_parent_id` int(10) unsigned DEFAULT NULL,
  `color_scheme` enum('project','green','blue','yellow','red') NOT NULL DEFAULT 'project',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`,`project_id`,`is_deleted`) USING BTREE,
  KEY `FK_community_project_id` (`project_id`),
  KEY `FK_community_parent_id` (`parent_id`),
  CONSTRAINT `FK_community_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `community` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_community_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community`
--

LOCK TABLES `community` WRITE;
/*!40000 ALTER TABLE `community` DISABLE KEYS */;
/*!40000 ALTER TABLE `community` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `community_after_insert` AFTER INSERT ON `community`
FOR EACH ROW
BEGIN
	CALL community_add_count_calc(NEW.id, NEW.project_id, NEW.post_count_calc, NEW.comment_count_calc);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `community_after_update` AFTER UPDATE ON `community`
FOR EACH ROW
BEGIN
	IF OLD.is_deleted <> NEW.is_deleted
	THEN
		IF NEW.is_deleted
		THEN
			CALL community_delete_count_calc(OLD.id, OLD.project_id, OLD.post_count_calc, OLD.comment_count_calc);
			
			DELETE FROM communities_menu_item WHERE community_id = OLD.id;
			DELETE FROM project_widget_communities WHERE community_id = OLD.id;
			DELETE FROM community_widget_communities WHERE community_id = OLD.id;
			DELETE FROM project_custom_feed_source WHERE community_id = OLD.id;
			DELETE FROM community_custom_feed_source WHERE community_id = OLD.id;

			DELETE FROM post_calc WHERE community_id = OLD.id;
		ELSE
			CALL community_add_count_calc(NEW.id, NEW.project_id, NEW.post_count_calc, NEW.comment_count_calc);
			INSERT INTO post_calc (id, author_user_id, project_id, community_id, section_id, hide_in_project_feed)
			(
				SELECT id, author_user_id, NEW.project_id, community_id, section_id, hide_in_project_feed 
				FROM post 
				WHERE community_id = NEW.id AND is_deleted = 0
			);
		END IF;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `community_admin`
--

DROP TABLE IF EXISTS `community_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_admin` (
  `user_id` int(10) unsigned NOT NULL,
  `community_id` int(10) unsigned NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adder_admin_user_id` int(10) unsigned DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_admin_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`community_id`),
  KEY `FK_community_admin_community_id` (`community_id`),
  KEY `FK_community_admin_adder_admin_user_id` (`adder_admin_user_id`),
  KEY `FK_community_admin_editor_admin_user_id` (`editor_admin_user_id`),
  CONSTRAINT `FK_community_admin_adder_admin_user_id` FOREIGN KEY (`adder_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_community_admin_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_admin_editor_admin_user_id` FOREIGN KEY (`editor_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_community_admin_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_admin`
--

LOCK TABLES `community_admin` WRITE;
/*!40000 ALTER TABLE `community_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `community_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_custom_feed`
--

DROP TABLE IF EXISTS `community_custom_feed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_custom_feed` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `descr` text NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `old_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_community_custom_feed_community_id` (`community_id`) USING BTREE,
  CONSTRAINT `FK_community_custom_feed_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_custom_feed`
--

LOCK TABLES `community_custom_feed` WRITE;
/*!40000 ALTER TABLE `community_custom_feed` DISABLE KEYS */;
/*!40000 ALTER TABLE `community_custom_feed` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `community_custom_feed_after_insert` AFTER INSERT ON `community_custom_feed`
FOR EACH ROW
BEGIN
	UPDATE community
	SET 
		custom_feed_count_calc = custom_feed_count_calc + 1
	WHERE id = NEW.community_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `community_custom_feed_after_delete` AFTER DELETE ON `community_custom_feed`
FOR EACH ROW
BEGIN
	UPDATE community
	SET 
		custom_feed_count_calc = custom_feed_count_calc - 1
	WHERE id = OLD.community_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `community_custom_feed_source`
--

DROP TABLE IF EXISTS `community_custom_feed_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_custom_feed_source` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `custom_feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `section_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique_item` (`custom_feed_id`,`community_id`,`section_id`) USING BTREE,
  KEY `FK_community_custom_feed_source_community_id` (`community_id`) USING BTREE,
  KEY `FK_community_custom_feed_source_section_id` (`section_id`) USING BTREE,
  CONSTRAINT `FK_community_custom_feed_source_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_custom_feed_source_custom_feed_id` FOREIGN KEY (`custom_feed_id`) REFERENCES `community_custom_feed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_custom_feed_source_section_id` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_custom_feed_source`
--

LOCK TABLES `community_custom_feed_source` WRITE;
/*!40000 ALTER TABLE `community_custom_feed_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `community_custom_feed_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_user_link`
--

DROP TABLE IF EXISTS `community_user_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_user_link` (
  `community_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('deleted','pretender','member','admin') NOT NULL DEFAULT 'deleted',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`community_id`,`user_id`),
  KEY `FK_community_user_link_user_id` (`user_id`),
  CONSTRAINT `FK_community_user_link_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_user_link`
--

LOCK TABLES `community_user_link` WRITE;
/*!40000 ALTER TABLE `community_user_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `community_user_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_widget`
--

DROP TABLE IF EXISTS `community_widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_widget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `column` int(2) unsigned NOT NULL DEFAULT '1',
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  `title` varchar(100) NOT NULL DEFAULT '',
  `type_id` char(40) NOT NULL,
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  `old_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_community_widget_community_id` (`community_id`) USING BTREE,
  KEY `FK_community_widget_type_id` (`type_id`) USING BTREE,
  CONSTRAINT `FK_community_widget_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_widget_type_id` FOREIGN KEY (`type_id`) REFERENCES `community_widget_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_widget`
--

LOCK TABLES `community_widget` WRITE;
/*!40000 ALTER TABLE `community_widget` DISABLE KEYS */;
/*!40000 ALTER TABLE `community_widget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_widget_communities`
--

DROP TABLE IF EXISTS `community_widget_communities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_widget_communities` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  PRIMARY KEY (`widget_id`,`community_id`),
  KEY `FK_community_widget_communities_2` (`community_id`),
  CONSTRAINT `FK_community_widget_communities_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_widget_communities_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `community_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_widget_communities`
--

LOCK TABLES `community_widget_communities` WRITE;
/*!40000 ALTER TABLE `community_widget_communities` DISABLE KEYS */;
/*!40000 ALTER TABLE `community_widget_communities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_widget_custom_feed`
--

DROP TABLE IF EXISTS `community_widget_custom_feed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_widget_custom_feed` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_custom_feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`widget_id`),
  KEY `FK_community_widget_custom_feed_community_custom_feed_id` (`community_custom_feed_id`) USING BTREE,
  CONSTRAINT `FK_community_widget_custom_feed_community_feed_id` FOREIGN KEY (`community_custom_feed_id`) REFERENCES `community_custom_feed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_widget_custom_feed_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `community_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_widget_custom_feed`
--

LOCK TABLES `community_widget_custom_feed` WRITE;
/*!40000 ALTER TABLE `community_widget_custom_feed` DISABLE KEYS */;
/*!40000 ALTER TABLE `community_widget_custom_feed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_widget_default`
--

DROP TABLE IF EXISTS `community_widget_default`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_widget_default` (
  `column` int(2) unsigned NOT NULL DEFAULT '1',
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  `title` varchar(100) NOT NULL DEFAULT '',
  `type_id` char(40) NOT NULL,
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`column`,`position`),
  KEY `FK_community_widget_default_type_id` (`type_id`) USING BTREE,
  CONSTRAINT `FK_community_widget_default_type_id` FOREIGN KEY (`type_id`) REFERENCES `community_widget_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_widget_default`
--

LOCK TABLES `community_widget_default` WRITE;
/*!40000 ALTER TABLE `community_widget_default` DISABLE KEYS */;
INSERT INTO `community_widget_default` VALUES (1,2,'Статистика','stat',0),(2,1,'Последние записи','last_posts',5),(2,2,'Последние комментарии','last_comments',5),(3,1,'Новые лица','last_users',10),(3,2,'Активные участники','top_users',10);
/*!40000 ALTER TABLE `community_widget_default` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_widget_text`
--

DROP TABLE IF EXISTS `community_widget_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_widget_text` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `html` text NOT NULL,
  PRIMARY KEY (`widget_id`),
  CONSTRAINT `FK_community_widget_text_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `community_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_widget_text`
--

LOCK TABLES `community_widget_text` WRITE;
/*!40000 ALTER TABLE `community_widget_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `community_widget_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_widget_type`
--

DROP TABLE IF EXISTS `community_widget_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_widget_type` (
  `type_id` char(40) NOT NULL DEFAULT '',
  `title` char(100) DEFAULT NULL,
  `multiple_instances` tinyint(1) NOT NULL DEFAULT '0',
  `ctrl` char(100) NOT NULL,
  `edit_ctrl` char(100) NOT NULL DEFAULT '',
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_widget_type`
--

LOCK TABLES `community_widget_type` WRITE;
/*!40000 ALTER TABLE `community_widget_type` DISABLE KEYS */;
INSERT INTO `community_widget_type` VALUES ('child_communities','Подсообщества',0,'child_communities','',0,6),('communities','Связанные сообщества',1,'communities','communities',0,5),('custom_feed','Лента',1,'custom_feed','custom_feed',5,9),('last_comments','Последние комментарии',0,'last_comments','lister',5,2),('last_posts','Последние записи',0,'last_posts','lister',5,1),('last_users','Новые лица',0,'last_users','lister',10,3),('stat','Статистика',0,'stat','',0,7),('text','Текстовый блок',1,'text','text',0,8),('top_users','Активные участники',0,'top_users','lister',10,4);
/*!40000 ALTER TABLE `community_widget_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competence_calc`
--

DROP TABLE IF EXISTS `competence_calc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competence_calc` (
  `competence_id` int(10) unsigned NOT NULL,
  `title` varchar(1024) NOT NULL,
  `number` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_set_id` int(10) unsigned NOT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`competence_id`),
  KEY `FK_competence_calc_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_competence_calc_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_calc_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competence_calc`
--

LOCK TABLES `competence_calc` WRITE;
/*!40000 ALTER TABLE `competence_calc` DISABLE KEYS */;
INSERT INTO `competence_calc` VALUES (101,'CMP 1',1,1,0,0,0,0),(102,'CMP 2',2,1,0,0,0,0),(103,'CMP 3',3,1,0,0,0,0),(104,'CMP 4',4,1,0,0,0,0),(105,'CMP 5',5,1,0,0,0,0),(201,'CMP 2-1',1,2,0,0,0,0),(202,'CMP 2-2',2,2,0,0,0,0),(203,'CMP 2-3',3,2,0,0,0,0);
/*!40000 ALTER TABLE `competence_calc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competence_full`
--

DROP TABLE IF EXISTS `competence_full`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competence_full` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(1024) NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_competence_full_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_competence_full_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competence_full`
--

LOCK TABLES `competence_full` WRITE;
/*!40000 ALTER TABLE `competence_full` DISABLE KEYS */;
INSERT INTO `competence_full` VALUES (101,1,'CMP 1',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0,0,0),(102,2,'CMP 2',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0,0,0),(103,3,'CMP 3',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0,0,0),(104,4,'CMP 4',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0,0,0),(105,5,'CMP 5',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0,0,0),(106,6,'CMP 6 - deleted',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',1,0,0,0,0),(201,1,'CMP 2-1',2,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0,0,0),(202,2,'CMP 2-2',2,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0,0,0),(203,3,'CMP 2-3',2,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0,0,0);
/*!40000 ALTER TABLE `competence_full` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competence_group`
--

DROP TABLE IF EXISTS `competence_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competence_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_competence_group_parent_id` (`parent_id`),
  KEY `FK_competence_group_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_competence_group_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_group_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `competence_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competence_group`
--

LOCK TABLES `competence_group` WRITE;
/*!40000 ALTER TABLE `competence_group` DISABLE KEYS */;
INSERT INTO `competence_group` VALUES (101,'TEST GROUP 1-1',1,NULL,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(201,'TEST GROUP 2-1',2,NULL,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(202,'TEST GROUP 2-2',2,NULL,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(203,'TEST GROUP 2-3',2,NULL,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `competence_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competence_link`
--

DROP TABLE IF EXISTS `competence_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competence_link` (
  `competence_id` int(10) unsigned NOT NULL,
  `competence_group_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`competence_id`,`competence_group_id`),
  KEY `FK_competence_link_competence_group_id` (`competence_group_id`),
  CONSTRAINT `FK_competence_link_competence_group_id` FOREIGN KEY (`competence_group_id`) REFERENCES `competence_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_link_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competence_link`
--

LOCK TABLES `competence_link` WRITE;
/*!40000 ALTER TABLE `competence_link` DISABLE KEYS */;
INSERT INTO `competence_link` VALUES (101,101,0),(102,101,0),(103,101,0),(104,101,0),(105,101,0),(201,201,0),(202,202,0),(203,202,0);
/*!40000 ALTER TABLE `competence_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competence_set`
--

DROP TABLE IF EXISTS `competence_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competence_set` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `professiogram_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `personal_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `project_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `project_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_competence_set_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_competence_set_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competence_set`
--

LOCK TABLES `competence_set` WRITE;
/*!40000 ALTER TABLE `competence_set` DISABLE KEYS */;
INSERT INTO `competence_set` VALUES (1,'TEST 1','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00',0,0,0,0,NULL,1,0,0,0,0,0,0),(2,'TEST 2','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00',0,0,0,0,NULL,1,0,0,0,0,0,0);
/*!40000 ALTER TABLE `competence_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competence_set_admin`
--

DROP TABLE IF EXISTS `competence_set_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competence_set_admin` (
  `user_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_professiogram_moderator` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adder_admin_user_id` int(10) unsigned DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_admin_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`competence_set_id`),
  KEY `FK_competence_set_admin_competence_set_id` (`competence_set_id`),
  KEY `FK_competence_set_admin_adder_admin_user_id` (`adder_admin_user_id`),
  KEY `FK_competence_set_admin_editor_admin_user_id` (`editor_admin_user_id`),
  CONSTRAINT `FK_competence_set_admin_adder_admin_user_id` FOREIGN KEY (`adder_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_set_admin_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_set_admin_editor_admin_user_id` FOREIGN KEY (`editor_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_set_admin_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competence_set_admin`
--

LOCK TABLES `competence_set_admin` WRITE;
/*!40000 ALTER TABLE `competence_set_admin` DISABLE KEYS */;
INSERT INTO `competence_set_admin` VALUES (1,1,1,0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(1,2,1,0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `competence_set_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competence_translator`
--

DROP TABLE IF EXISTS `competence_translator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competence_translator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_competence_set_id` int(10) unsigned NOT NULL,
  `to_competence_set_id` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_competence_translator_from_competence_set_id` (`from_competence_set_id`),
  KEY `FK_competence_translator_to_competence_set_id` (`to_competence_set_id`),
  CONSTRAINT `FK_competence_translator_from_competence_set_id` FOREIGN KEY (`from_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translator_to_competence_set_id` FOREIGN KEY (`to_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1000000002 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competence_translator`
--

LOCK TABLES `competence_translator` WRITE;
/*!40000 ALTER TABLE `competence_translator` DISABLE KEYS */;
INSERT INTO `competence_translator` VALUES (1000000001,2,1,'TRANS',0);
/*!40000 ALTER TABLE `competence_translator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `to_email` varchar(255) NOT NULL,
  `to_user_id` int(10) unsigned DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `html` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email`
--

LOCK TABLES `email` WRITE;
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
/*!40000 ALTER TABLE `email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_log`
--

DROP TABLE IF EXISTS `email_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `to_email` varchar(255) NOT NULL,
  `to_user_id` int(10) unsigned DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `html` mediumtext,
  `sent_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `success` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_log`
--

LOCK TABLES `email_log` WRITE;
/*!40000 ALTER TABLE `email_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finish_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `place` varchar(255) NOT NULL DEFAULT '',
  `announce` text,
  `reflex_id` int(10) unsigned DEFAULT NULL,
  `reflex_project_id` int(10) unsigned DEFAULT NULL,
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `reflex` (`project_id`,`reflex_id`),
  KEY `FK_event_project_id` (`project_id`),
  KEY `FK_event_adder_user_id` (`adder_user_id`),
  KEY `FK_event_reflex_id` (`reflex_id`),
  KEY `FK_event_reflex_project_id` (`reflex_project_id`),
  CONSTRAINT `FK_event_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_event_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_reflex_id` FOREIGN KEY (`reflex_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_reflex_project_id` FOREIGN KEY (`reflex_project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `event_after_insert` AFTER INSERT ON `event`
FOR EACH ROW
BEGIN
	UPDATE project
	SET 
		event_count_calc = event_count_calc + 1
	WHERE id = NEW.project_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `event_after_delete` AFTER DELETE ON `event`
FOR EACH ROW
BEGIN
	UPDATE project
	SET 
		comment_count_calc = comment_count_calc - OLD.comment_count_calc,
		event_count_calc = event_count_calc - 1
	WHERE id = OLD.project_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `event_category`
--

DROP TABLE IF EXISTS `event_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_event_category_parent_id` (`parent_id`),
  KEY `FK_event_category_project_id` (`project_id`),
  CONSTRAINT `FK_event_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `event_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_category_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_category`
--

LOCK TABLES `event_category` WRITE;
/*!40000 ALTER TABLE `event_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_category_link`
--

DROP TABLE IF EXISTS `event_category_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_category_link` (
  `event_id` int(10) unsigned NOT NULL,
  `event_category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`event_id`,`event_category_id`),
  KEY `FK_event_category_link_event_category_id` (`event_category_id`),
  CONSTRAINT `FK_event_category_link_event_category_id` FOREIGN KEY (`event_category_id`) REFERENCES `event_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_category_link_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_category_link`
--

LOCK TABLES `event_category_link` WRITE;
/*!40000 ALTER TABLE `event_category_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_category_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_rate_link`
--

DROP TABLE IF EXISTS `event_rate_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_rate_link` (
  `event_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`event_id`,`rate_id`),
  KEY `FK_event_rate_link_rate_id` (`rate_id`),
  KEY `FK_event_rate_link_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_event_rate_link_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_rate_link_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_rate_link_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_rate_link`
--

LOCK TABLES `event_rate_link` WRITE;
/*!40000 ALTER TABLE `event_rate_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_rate_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_user_link`
--

DROP TABLE IF EXISTS `event_user_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_user_link` (
  `user_id` int(10) unsigned NOT NULL,
  `event_id` int(10) unsigned NOT NULL,
  `ignored_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `before_status` int(10) unsigned NOT NULL DEFAULT '0',
  `after_status` int(10) NOT NULL DEFAULT '0',
  `ignored` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `before_status_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `after_status_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_status_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`event_id`),
  KEY `FK_event_user_link_event_id` (`event_id`),
  CONSTRAINT `FK_event_user_link_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_event_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_user_link`
--

LOCK TABLES `event_user_link` WRITE;
/*!40000 ALTER TABLE `event_user_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_user_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark_calc`
--

DROP TABLE IF EXISTS `mark_calc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_calc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `precedent_group_id` int(10) unsigned DEFAULT NULL,
  `precedent_flash_group_id` int(10) unsigned NOT NULL,
  `precedent_flash_group_user_link_id` int(10) unsigned NOT NULL,
  `ratee_user_id` int(10) unsigned NOT NULL,
  `rater_user_id` int(10) unsigned NOT NULL,
  `adder_user_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `type` enum('personal','group') NOT NULL,
  `value` int(10) unsigned NOT NULL,
  `comment` text,
  `precedent_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique` (`precedent_flash_group_id`,`ratee_user_id`,`rater_user_id`,`competence_id`,`type`),
  KEY `FK_mark_calc_project_id` (`project_id`),
  KEY `FK_mark_calc_ratee_user_id` (`ratee_user_id`),
  KEY `FK_mark_calc_competence_id` (`competence_id`),
  KEY `FK_mark_calc_competence_set_id` (`competence_set_id`),
  KEY `FK_mark_calc_precedent_flash_group_user_link_id` (`precedent_flash_group_user_link_id`),
  KEY `FK_mark_calc_precedent_group_id` (`precedent_group_id`),
  KEY `FK_mark_calc_precedent_id` (`precedent_id`),
  KEY `FK_mark_calc_rater_user_id` (`rater_user_id`),
  KEY `FK_mark_calc_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_mark_calc_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_precedent_flash_group_id` FOREIGN KEY (`precedent_flash_group_id`) REFERENCES `precedent_flash_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_precedent_flash_group_user_link_id` FOREIGN KEY (`precedent_flash_group_user_link_id`) REFERENCES `precedent_flash_group_user_link` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_precedent_group_id` FOREIGN KEY (`precedent_group_id`) REFERENCES `precedent_group` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_ratee_user_id` FOREIGN KEY (`ratee_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_calc_rater_user_id` FOREIGN KEY (`rater_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_calc`
--

LOCK TABLES `mark_calc` WRITE;
/*!40000 ALTER TABLE `mark_calc` DISABLE KEYS */;
INSERT INTO `mark_calc` VALUES (1,2,201,2010,20101,1,1,1,101,1,'personal',2,'x',201),(2,2,201,2010,20101,1,2,2,102,1,'personal',3,'x',201),(3,2,201,2010,20101,1,2,2,101,1,'personal',1,'x',201),(4,2,201,2010,20102,2,1,1,102,1,'personal',3,'x',201),(5,2,201,2010,20102,2,1,1,103,1,'personal',1,'x',201),(6,2,201,2011,20112,2,1,1,103,1,'personal',2,'x',201),(7,1,101,1010,10101,1,1,1,101,1,'personal',3,'CS1-P1-U1',101),(8,1,101,1030,10301,1,2,2,101,1,'group',2,'CS1-P1-U1',103),(9,1,101,1010,10101,1,3,3,101,1,'group',2,'CS1-P1-U1',101),(10,1,101,1010,10101,1,4,4,101,1,'group',2,'CS1-P1-U1',101),(11,1,101,1011,10111,1,1,1,101,1,'personal',1,'CS1-P1-U1',101),(12,1,101,1010,10101,1,2,2,102,1,'group',3,'CS1-P1-U1',101),(13,1,101,1030,10301,1,3,3,102,1,'personal',1,'CS1-P1-U1',103),(14,1,101,1030,10301,1,4,4,102,1,'personal',1,'CS1-P1-U1',103),(15,1,101,1030,10301,1,1,1,102,1,'personal',1,'CS1-P1-U1',103),(16,1,101,1010,10101,1,2,2,102,1,'personal',3,'CS1-P1-U1',101),(17,1,101,1031,10311,1,3,3,102,1,'personal',2,'CS1-P1-U1',103),(18,1,101,1010,10101,1,4,4,103,1,'personal',3,'CS1-P1-U1',101),(19,1,101,1010,10101,1,1,1,103,1,'personal',2,'CS1-P1-U1',101),(20,1,101,1010,10101,1,2,2,103,1,'personal',2,'CS1-P1-U1',101),(21,1,101,1030,10301,1,3,3,104,1,'personal',2,'CS1-P1-U1',103),(22,1,101,1010,10101,1,4,4,104,1,'personal',1,'CS1-P1-U1',101),(23,1,101,1030,10301,1,4,4,104,1,'personal',1,'CS1-P1-U1',103),(24,1,101,1031,10311,1,4,4,104,1,'personal',1,'CS1-P1-U1',103),(25,1,101,1010,10102,2,4,4,101,1,'personal',2,'CS1-P1-U2',101),(26,1,101,1030,10302,2,4,4,101,1,'personal',2,'CS1-P1-U2',103),(27,1,101,1030,10302,2,3,3,101,1,'personal',2,'CS1-P1-U2',103),(28,1,101,1030,10302,2,2,2,101,1,'group',3,'CS1-P1-U2',103),(29,1,101,1020,10202,2,1,1,101,1,'group',3,'CS1-P1-U2',102),(30,1,101,1021,10212,2,1,1,101,1,'group',3,'CS1-P1-U2',102),(31,1,101,1020,10202,2,1,1,102,1,'group',2,'CS1-P1-U2',102),(32,1,101,1020,10202,2,1,1,102,1,'personal',1,'CS1-P1-U2',102),(33,1,101,1020,10202,2,1,1,103,1,'group',1,'CS1-P1-U2',102),(34,1,101,1030,10302,2,1,1,104,1,'personal',1,'CS1-P1-U2',103),(35,1,101,1020,10202,2,1,1,104,1,'personal',1,'CS1-P1-U2',102),(36,1,101,1030,10302,2,1,1,104,1,'group',1,'CS1-P1-U2',103),(37,1,101,1020,10202,2,1,1,105,1,'personal',3,'CS1-P1-U2',102),(38,1,101,1020,10202,2,1,1,105,1,'group',1,'CS1-P1-U2',102),(39,1,101,1030,10302,2,1,1,105,1,'personal',2,'CS1-P1-U2',103),(40,1,101,1010,10101,1,1,1,201,2,'personal',3,'CS2-P1-U1',101),(41,1,101,1010,10101,1,1,1,202,2,'personal',1,'CS2-P1-U1',101),(42,1,101,1010,10101,1,1,1,202,2,'group',2,'CS2-P1-U1',101),(43,1,101,1010,10102,2,1,1,201,2,'personal',3,'CS2-P1-U1',101),(44,1,101,1010,10103,3,1,1,102,1,'personal',0,'CS1-P1-U2',101);
/*!40000 ALTER TABLE `mark_calc` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `mark_calc_after_insert` AFTER INSERT ON `mark_calc`
FOR EACH ROW















BEGIN
	DECLARE personal_mark_inc INT DEFAULT 0;
	DECLARE group_mark_inc INT DEFAULT 0;
	DECLARE group_mark_raw_inc INT DEFAULT 0;
	
	IF NEW.type = 'personal'
	THEN
		SET personal_mark_inc = 1;
	ELSE
		SET group_mark_inc = 1;
		IF (
			SELECT COUNT(*)
			FROM mark_calc
			WHERE
				precedent_flash_group_id = NEW.precedent_flash_group_id
				AND rater_user_id = NEW.rater_user_id
				AND competence_id = NEW.competence_id
				AND type = 'group'
			) = 1
		THEN
			SET group_mark_raw_inc = 1;
		END IF;
	END IF;

	UPDATE competence_set
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.competence_set_id;

	UPDATE competence_full
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.competence_id;

	UPDATE competence_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		competence_id = NEW.competence_id;

	UPDATE precedent
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.precedent_id;

	UPDATE project
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.project_id;

	UPDATE user
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.ratee_user_id;

	INSERT INTO precedent_rater_link
	SET
		precedent_id = NEW.precedent_id,
		rater_user_id = NEW.rater_user_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO precedent_mark_adder
	SET
		precedent_id = NEW.precedent_id,
		adder_user_id = NEW.adder_user_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO precedent_ratee_calc
	SET
		precedent_id = NEW.precedent_id,
		ratee_user_id = NEW.ratee_user_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO project_competence_set_link_calc
	SET
		project_id = NEW.project_id,
		competence_set_id = NEW.competence_set_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO user_competence_set_link_calc
	SET
		user_id = NEW.ratee_user_id,
		competence_set_id = NEW.competence_set_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO user_competence_set_project_link
	SET
		user_id = NEW.ratee_user_id,
		competence_set_id = NEW.competence_set_id,
		project_id = NEW.project_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;
	
	INSERT INTO user_project_link
	SET
		user_id = NEW.ratee_user_id,
		project_id = NEW.project_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `mark_calc_after_update` AFTER UPDATE ON `mark_calc`
FOR EACH ROW

BEGIN
	DECLARE personal_mark_inc INT DEFAULT 0;
	DECLARE group_mark_inc INT DEFAULT 0;
	DECLARE group_mark_raw_inc INT DEFAULT 0;

	IF NEW.adder_user_id != OLD.adder_user_id
	THEN
		IF NEW.type = 'personal'
		THEN
			SET personal_mark_inc = 1;
		ELSE
			SET group_mark_inc = 1;
			IF (
				SELECT COUNT(*)
				FROM mark_calc
				WHERE
					precedent_flash_group_id = NEW.precedent_flash_group_id
					AND rater_user_id = NEW.rater_user_id
					AND adder_user_id = NEW.adder_user_id
					AND competence_id = NEW.competence_id
					AND type = 'group'
				) = 1
			THEN
				SET group_mark_raw_inc = 1;
			END IF;
		END IF;

		UPDATE precedent_mark_adder
		SET
			personal_mark_count_calc = personal_mark_count_calc - personal_mark_inc,
			group_mark_raw_count_calc = group_mark_raw_count_calc - group_mark_raw_inc,
			group_mark_count_calc = group_mark_count_calc - group_mark_inc,
			total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
		WHERE
			precedent_id = NEW.precedent_id
			AND adder_user_id = OLD.adder_user_id;

		UPDATE precedent_mark_adder
		SET
			personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
			group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
			group_mark_count_calc = group_mark_count_calc + group_mark_inc,
			total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
		WHERE
			precedent_id = NEW.precedent_id
			AND adder_user_id = NEW.adder_user_id;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `mark_calc_after_delete` AFTER DELETE ON `mark_calc`
FOR EACH ROW















BEGIN
	DECLARE personal_mark_inc INT DEFAULT 0;
	DECLARE group_mark_inc INT DEFAULT 0;
	DECLARE group_mark_raw_inc INT DEFAULT 0;
	
	IF OLD.type = 'personal'
	THEN
		SET personal_mark_inc = -1;
	ELSE
		SET group_mark_inc = -1;
		IF (
			SELECT COUNT(*)
			FROM mark_calc
			WHERE
				precedent_flash_group_id = OLD.precedent_flash_group_id
				AND rater_user_id = OLD.rater_user_id
				AND competence_id = OLD.competence_id
				AND type = 'group'
			) = 0
		THEN
			SET group_mark_raw_inc = -1;
		END IF;
	END IF;

	UPDATE competence_set
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.competence_set_id;

	UPDATE competence_full
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.competence_id;

	UPDATE competence_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		competence_id = OLD.competence_id;

	UPDATE precedent
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.precedent_id;

	UPDATE project
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.project_id;

	UPDATE user
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.ratee_user_id;

	UPDATE precedent_rater_link
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		precedent_id = OLD.precedent_id
		AND rater_user_id = OLD.rater_user_id;

	UPDATE precedent_mark_adder
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		precedent_id = OLD.precedent_id
		AND adder_user_id = OLD.adder_user_id;

	UPDATE precedent_ratee_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		precedent_id = OLD.precedent_id
		AND ratee_user_id = OLD.ratee_user_id;

	UPDATE project_competence_set_link_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		project_id = OLD.project_id
		AND competence_set_id = OLD.competence_set_id;

	UPDATE user_competence_set_link_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		user_id = OLD.ratee_user_id
		AND competence_set_id = OLD.competence_set_id;

	UPDATE user_competence_set_project_link
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		user_id = OLD.ratee_user_id
		AND competence_set_id = OLD.competence_set_id
		AND project_id = OLD.project_id;

	UPDATE user_project_link
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		user_id = OLD.ratee_user_id
		AND project_id = OLD.project_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mark_group`
--

DROP TABLE IF EXISTS `mark_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_group` (
  `precedent_flash_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rater_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `adder_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  PRIMARY KEY (`precedent_flash_group_id`,`competence_id`,`rater_user_id`),
  KEY `FK_mark_group_competence_id` (`competence_id`),
  KEY `FK_mark_group_adder_user_id` (`adder_user_id`),
  KEY `FK_mark_group_rater_user_id` (`rater_user_id`),
  CONSTRAINT `FK_mark_group_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_group_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_group_precedent_flash_group_id` FOREIGN KEY (`precedent_flash_group_id`) REFERENCES `precedent_flash_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_group_rater_user_id` FOREIGN KEY (`rater_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=421 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_group`
--

LOCK TABLES `mark_group` WRITE;
/*!40000 ALTER TABLE `mark_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `mark_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `mark_group_after_insert` AFTER INSERT ON `mark_group`
FOR EACH ROW
BEGIN
	DECLARE my_precedent_id INT;
	DECLARE my_project_id INT;
	DECLARE my_competence_set_id INT;

	SET my_precedent_id = 
	(
		SELECT precedent_id
		FROM precedent_flash_group
		WHERE id = NEW.precedent_flash_group_id
	);

	SET my_project_id =
	(
		SELECT project_id
		FROM precedent
		WHERE id = my_precedent_id
	);

	SET my_competence_set_id =
	(
		SELECT competence_set_id
		FROM competence_full
		WHERE id = NEW.competence_id
	);

	
	

	INSERT INTO mark_calc
	(
		project_id,
		precedent_flash_group_id,
		precedent_flash_group_user_link_id,
		ratee_user_id,
		rater_user_id,
		adder_user_id,
		competence_id,
		competence_set_id,
		type,
		value,
		comment,
		precedent_id
	)
	SELECT
		my_project_id,
		NEW.precedent_flash_group_id,
		pfgul.id,
		pfgul.user_id,
		NEW.rater_user_id,
		NEW.adder_user_id,
		NEW.competence_id,
		my_competence_set_id,
		'group',
		NEW.value,
		NEW.comment,
		my_precedent_id
	FROM
		precedent_flash_group_user_link AS pfgul
	WHERE
		pfgul.precedent_flash_group_id = NEW.precedent_flash_group_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `mark_group_after_update` AFTER UPDATE ON `mark_group`
FOR EACH ROW
BEGIN
	UPDATE mark_calc
	SET
		value = NEW.value,
		adder_user_id = NEW.adder_user_id,
		comment = NEW.comment
	WHERE
		precedent_flash_group_id = OLD.precedent_flash_group_id
		AND rater_user_id = OLD.rater_user_id
		AND competence_id = OLD.competence_id
		AND type = 'group';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `mark_group_after_delete` AFTER DELETE ON `mark_group`
FOR EACH ROW
BEGIN
	DELETE FROM mark_calc
	WHERE
		precedent_flash_group_id = OLD.precedent_flash_group_id
		AND rater_user_id = OLD.rater_user_id
		AND competence_id = OLD.competence_id
		AND type = 'group';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mark_personal`
--

DROP TABLE IF EXISTS `mark_personal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_personal` (
  `precedent_flash_group_user_link_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rater_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `adder_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  PRIMARY KEY (`precedent_flash_group_user_link_id`,`competence_id`,`rater_user_id`),
  KEY `FK_mark_personal_competence_id` (`competence_id`),
  KEY `FK_mark_personal_adder_user_id` (`adder_user_id`),
  KEY `FK_mark_personal_rater_user_id` (`rater_user_id`),
  CONSTRAINT `FK_mark_personal_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_personal_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_personal_precedent_flash_group_user_link_id` FOREIGN KEY (`precedent_flash_group_user_link_id`) REFERENCES `precedent_flash_group_user_link` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mark_personal_rater_user_id` FOREIGN KEY (`rater_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=332 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_personal`
--

LOCK TABLES `mark_personal` WRITE;
/*!40000 ALTER TABLE `mark_personal` DISABLE KEYS */;
/*!40000 ALTER TABLE `mark_personal` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `mark_personal_after_insert` AFTER INSERT ON `mark_personal`
FOR EACH ROW
BEGIN
	DECLARE my_ratee_user_id INT;
	DECLARE my_precedent_flash_group_id INT;
	DECLARE my_precedent_id INT;
	DECLARE my_project_id INT;
	DECLARE my_competence_set_id INT;

	SELECT
		user_id,
		precedent_flash_group_id
	INTO
		my_ratee_user_id,
		my_precedent_flash_group_id
	FROM precedent_flash_group_user_link
	WHERE id = NEW.precedent_flash_group_user_link_id;
	
	SET my_precedent_id = 
	(
		SELECT pfg.precedent_id
		FROM
			precedent_flash_group_user_link AS pfgul,
			precedent_flash_group AS pfg
		WHERE
			pfgul.id = NEW.precedent_flash_group_user_link_id
			AND pfg.id = pfgul.precedent_flash_group_id
	);

	SET my_project_id =
	(
		SELECT project_id
		FROM precedent
		WHERE id = my_precedent_id
	);

	SET my_competence_set_id =
	(
		SELECT competence_set_id
		FROM competence_full
		WHERE id = NEW.competence_id
	);

	INSERT INTO mark_calc
	SET
		project_id = my_project_id,
		precedent_flash_group_id = my_precedent_flash_group_id,
		precedent_flash_group_user_link_id = NEW.precedent_flash_group_user_link_id,
		ratee_user_id = my_ratee_user_id,
		rater_user_id = NEW.rater_user_id,
		adder_user_id = NEW.adder_user_id,
		competence_id = NEW.competence_id,
		competence_set_id = my_competence_set_id,
		type = 'personal',
		value = NEW.value,
		comment = NEW.comment,
		precedent_id = my_precedent_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `mark_personal_after_update` AFTER UPDATE ON `mark_personal`
FOR EACH ROW
BEGIN
	UPDATE mark_calc
	SET
		value = NEW.value,
		adder_user_id = NEW.adder_user_id,
		comment = NEW.comment
	WHERE
		precedent_flash_group_user_link_id = OLD.precedent_flash_group_user_link_id
		AND rater_user_id = OLD.rater_user_id
		AND competence_id = OLD.competence_id
		AND type = 'personal';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `mark_personal_after_delete` AFTER DELETE ON `mark_personal`
FOR EACH ROW
BEGIN
	DELETE FROM mark_calc
	WHERE
		precedent_flash_group_user_link_id = OLD.precedent_flash_group_user_link_id
		AND rater_user_id = OLD.rater_user_id
		AND competence_id = OLD.competence_id
		AND type = 'personal';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `material`
--

DROP TABLE IF EXISTS `material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `announce` text,
  `block_set_id` int(10) unsigned DEFAULT NULL,
  `reflex_id` int(10) unsigned DEFAULT NULL,
  `reflex_project_id` int(10) unsigned DEFAULT NULL,
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `reflex` (`project_id`,`reflex_id`),
  KEY `FK_material_project_id` (`project_id`),
  KEY `FK_material_adder_user_id` (`adder_user_id`),
  KEY `FK_material_block_set_id` (`block_set_id`),
  KEY `FK_material_reflex_id` (`reflex_id`),
  KEY `FK_material_reflex_project_id` (`reflex_project_id`),
  CONSTRAINT `FK_material_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_material_block_set_id` FOREIGN KEY (`block_set_id`) REFERENCES `block_set` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_material_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_reflex_id` FOREIGN KEY (`reflex_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_reflex_project_id` FOREIGN KEY (`reflex_project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material`
--

LOCK TABLES `material` WRITE;
/*!40000 ALTER TABLE `material` DISABLE KEYS */;
/*!40000 ALTER TABLE `material` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `material_after_insert` AFTER INSERT ON `material`
FOR EACH ROW
BEGIN
	UPDATE project
	SET 
		material_count_calc = material_count_calc + 1
	WHERE id = NEW.project_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `material_after_delete` AFTER DELETE ON `material`
FOR EACH ROW
BEGIN
	UPDATE project
	SET 
		comment_count_calc = comment_count_calc - OLD.comment_count_calc,
		material_count_calc = material_count_calc - 1
	WHERE id = OLD.project_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `material_category`
--

DROP TABLE IF EXISTS `material_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_material_category_parent_id` (`parent_id`),
  KEY `FK_material_category_project_id` (`project_id`),
  CONSTRAINT `FK_material_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `material_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_category_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material_category`
--

LOCK TABLES `material_category` WRITE;
/*!40000 ALTER TABLE `material_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `material_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material_category_link`
--

DROP TABLE IF EXISTS `material_category_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material_category_link` (
  `material_id` int(10) unsigned NOT NULL DEFAULT '0',
  `material_category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`material_id`,`material_category_id`),
  KEY `FK_material_category_link_material_category_id` (`material_category_id`),
  CONSTRAINT `FK_material_category_link_material_category_id` FOREIGN KEY (`material_category_id`) REFERENCES `material_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_category_link_material_id` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material_category_link`
--

LOCK TABLES `material_category_link` WRITE;
/*!40000 ALTER TABLE `material_category_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `material_category_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material_rate_link`
--

DROP TABLE IF EXISTS `material_rate_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material_rate_link` (
  `material_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`material_id`,`rate_id`),
  KEY `FK_material_rate_link_rate_id` (`rate_id`),
  KEY `FK_material_rate_link_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_material_rate_link_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_rate_link_material_id` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_rate_link_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material_rate_link`
--

LOCK TABLES `material_rate_link` WRITE;
/*!40000 ALTER TABLE `material_rate_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `material_rate_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material_user_link`
--

DROP TABLE IF EXISTS `material_user_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material_user_link` (
  `user_id` int(10) unsigned NOT NULL,
  `material_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ignored_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `before_status` int(10) unsigned NOT NULL DEFAULT '0',
  `after_status` int(11) NOT NULL DEFAULT '0',
  `ignored` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `before_status_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `after_status_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_status_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`material_id`),
  KEY `FK_material_user_link_material_id` (`material_id`),
  CONSTRAINT `FK_material_user_link_material_id` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_material_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material_user_link`
--

LOCK TABLES `material_user_link` WRITE;
/*!40000 ALTER TABLE `material_user_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `material_user_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author_user_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `block_set_id` int(10) unsigned DEFAULT NULL,
  `community_id` int(10) unsigned NOT NULL,
  `section_id` int(10) unsigned DEFAULT NULL,
  `hide_in_project_feed` tinyint(1) NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `last_comment_id` int(10) unsigned DEFAULT NULL,
  `last_comment_html` text,
  `last_comment_author_user_id` int(10) unsigned DEFAULT NULL,
  `old_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_post_user_id` (`author_user_id`),
  KEY `FK_post_block_set_id` (`block_set_id`),
  KEY `FK_post_section_id` (`section_id`),
  KEY `FK_post_community_id` (`community_id`),
  KEY `FK_post_last_comment_author_user_id` (`last_comment_author_user_id`),
  KEY `FK_post_last_comment_id` (`last_comment_id`),
  CONSTRAINT `FK_post_block_set_id` FOREIGN KEY (`block_set_id`) REFERENCES `block_set` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_post_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_post_last_comment_author_user_id` FOREIGN KEY (`last_comment_author_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_post_last_comment_id` FOREIGN KEY (`last_comment_id`) REFERENCES `comment` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_post_section_id` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_post_user_id` FOREIGN KEY (`author_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `post_after_insert` AFTER INSERT ON `post`
FOR EACH ROW
BEGIN
	SET @project_id = (SELECT project_id FROM community WHERE id = NEW.community_id);
	INSERT INTO post_calc
	SET
		id = NEW.id,
		author_user_id = NEW.author_user_id,
		project_id = @project_id,
		community_id = NEW.community_id,
		section_id = NEW.section_id,
		hide_in_project_feed = NEW.hide_in_project_feed,
		comment_count_calc = NEW.comment_count_calc,
		last_comment_id = NEW.last_comment_id,
		last_comment_author_user_id = NEW.last_comment_author_user_id,
		last_comment_html = NEW.last_comment_html;

	CALL post_add_count_calc(NEW.id, NEW.section_id, NEW.community_id, @project_id, NEW.author_user_id, NEW.comment_count_calc);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `post_after_update` AFTER UPDATE ON `post`
FOR EACH ROW
BEGIN
	DECLARE my_project_id INT;
	SET my_project_id = (SELECT project_id FROM community WHERE id = NEW.community_id);
	IF OLD.is_deleted <> NEW.is_deleted
	THEN
		IF NEW.is_deleted
		THEN
			DELETE FROM post_calc WHERE id = OLD.id;
			CALL post_delete_count_calc(OLD.id, OLD.section_id, OLD.community_id, my_project_id, OLD.author_user_id, OLD.comment_count_calc);
		ELSE
			INSERT INTO post_calc
			SET
				id = NEW.id,
				author_user_id = NEW.author_user_id,
				project_id = my_project_id,
				community_id = NEW.community_id,
				section_id = NEW.section_id,
				hide_in_project_feed = NEW.hide_in_project_feed,
				comment_count_calc = NEW.comment_count_calc,
				last_comment_id = NEW.last_comment_id,
				last_comment_author_user_id = NEW.last_comment_author_user_id,
				last_comment_html = NEW.last_comment_html;
			CALL post_add_count_calc(NEW.id, NEW.section_id, NEW.community_id, @project_id, NEW.author_user_id, NEW.comment_count_calc);
		END IF;
	ELSEIF NEW.is_deleted = 0
	THEN
		UPDATE post_calc 
		SET
				id = NEW.id,
				author_user_id = NEW.author_user_id,
				project_id = my_project_id,
				community_id = NEW.community_id,
				section_id = NEW.section_id,
				hide_in_project_feed = NEW.hide_in_project_feed,
				comment_count_calc = NEW.comment_count_calc,
				last_comment_id = NEW.last_comment_id,
				last_comment_author_user_id = NEW.last_comment_author_user_id,
				last_comment_html = NEW.last_comment_html
		WHERE id = NEW.id;
		IF IFNULL(NEW.section_id <> OLD.section_id, 1)
		THEN
			UPDATE section SET post_count_calc = post_count_calc + 1 WHERE id = NEW.section_id;
			UPDATE section SET post_count_calc = post_count_calc - 1 WHERE id = OLD.section_id;
		END IF;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `post_calc`
--

DROP TABLE IF EXISTS `post_calc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_calc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_user_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `community_id` int(10) unsigned NOT NULL,
  `section_id` int(10) unsigned DEFAULT NULL,
  `hide_in_project_feed` tinyint(1) NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_id` int(10) unsigned DEFAULT NULL,
  `last_comment_html` text,
  `last_comment_author_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_author` (`author_user_id`,`id`),
  KEY `index_community` (`community_id`,`id`),
  KEY `index_section` (`section_id`,`id`),
  KEY `index_project` (`project_id`,`hide_in_project_feed`,`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_calc`
--

LOCK TABLES `post_calc` WRITE;
/*!40000 ALTER TABLE `post_calc` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_calc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `precedent`
--

DROP TABLE IF EXISTS `precedent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precedent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `descr` mediumtext NOT NULL,
  `precedent_group_id` int(10) unsigned DEFAULT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_editor_user_id` int(10) unsigned NOT NULL,
  `adder_user_id` int(10) unsigned NOT NULL,
  `responsible_user_id` int(10) unsigned DEFAULT NULL,
  `access` enum('public','private') NOT NULL DEFAULT 'public',
  `complete_descr` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `complete_members` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `complete_marks` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `ratee_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `default_flash_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_precedent_project_id` (`project_id`),
  KEY `FK_precedent_precedent_group_id` (`precedent_group_id`),
  KEY `FK_precedent_last_editor_user_id` (`last_editor_user_id`),
  KEY `FK_precedent_responsible_user_id` (`responsible_user_id`),
  KEY `FK_precedent_adder_user_id` (`adder_user_id`),
  KEY `FK_precedent_default_flash_group_id` (`default_flash_group_id`),
  CONSTRAINT `FK_precedent_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_default_flash_group_id` FOREIGN KEY (`default_flash_group_id`) REFERENCES `precedent_flash_group` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_last_editor_user_id` FOREIGN KEY (`last_editor_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_precedent_group_id` FOREIGN KEY (`precedent_group_id`) REFERENCES `precedent_group` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_responsible_user_id` FOREIGN KEY (`responsible_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precedent`
--

LOCK TABLES `precedent` WRITE;
/*!40000 ALTER TABLE `precedent` DISABLE KEYS */;
INSERT INTO `precedent` VALUES (101,1,'TEST 1','x',101,'0000-00-00 00:00:00','0000-00-00 00:00:00',1,1,NULL,'public',0,0,0,0,0,0,0,0,0,NULL),(102,1,'TEST 2','x',102,'0000-00-00 00:00:00','0000-00-00 00:00:00',1,1,NULL,'public',0,0,0,0,0,0,0,0,0,NULL),(103,1,'TEST 3','x',103,'0000-00-00 00:00:00','0000-00-00 00:00:00',1,1,NULL,'public',0,0,0,0,0,0,0,0,0,NULL),(201,2,'TEST 2-1','x',201,'0000-00-00 00:00:00','0000-00-00 00:00:00',1,1,NULL,'public',0,0,0,0,0,0,0,0,0,NULL);
/*!40000 ALTER TABLE `precedent` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `precedent_after_insert` AFTER INSERT ON `precedent`
FOR EACH ROW
BEGIN
	UPDATE project
	SET precedent_count_calc = precedent_count_calc + 1
	WHERE id = NEW.project_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `precedent_after_delete` AFTER DELETE ON `precedent`
FOR EACH ROW
BEGIN
	UPDATE project
	SET precedent_count_calc = precedent_count_calc - 1
	WHERE id = OLD.project_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `precedent_comment`
--

DROP TABLE IF EXISTS `precedent_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precedent_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `precedent_id` int(10) unsigned NOT NULL,
  `commenter_user_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `html` text,
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned NOT NULL,
  `add_time` datetime DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_precedent_comment_commenter_user_id` (`commenter_user_id`),
  KEY `FK_precedent_comment_parent_id` (`parent_id`),
  KEY `FK_precedent_comment_precedent_id` (`precedent_id`),
  CONSTRAINT `FK_precedent_comment_commenter_user_id` FOREIGN KEY (`commenter_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `precedent_comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_comment_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precedent_comment`
--

LOCK TABLES `precedent_comment` WRITE;
/*!40000 ALTER TABLE `precedent_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `precedent_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `precedent_competence_group_link`
--

DROP TABLE IF EXISTS `precedent_competence_group_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precedent_competence_group_link` (
  `precedent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `competence_group_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`precedent_id`,`competence_group_id`),
  KEY `FK_precedent_competence_group_link_competence_set_id` (`competence_set_id`),
  KEY `FK_precedent_competence_group_link_competence_group_id` (`competence_group_id`),
  CONSTRAINT `FK_precedent_competence_group_link_competence_group_id` FOREIGN KEY (`competence_group_id`) REFERENCES `competence_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_competence_group_link_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_competence_group_link_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=95 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precedent_competence_group_link`
--

LOCK TABLES `precedent_competence_group_link` WRITE;
/*!40000 ALTER TABLE `precedent_competence_group_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `precedent_competence_group_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `precedent_flash_group`
--

DROP TABLE IF EXISTS `precedent_flash_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precedent_flash_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `precedent_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_precedent_flash_group_precedent_id` (`precedent_id`),
  CONSTRAINT `FK_precedent_flash_group_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2012 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=69 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precedent_flash_group`
--

LOCK TABLES `precedent_flash_group` WRITE;
/*!40000 ALTER TABLE `precedent_flash_group` DISABLE KEYS */;
INSERT INTO `precedent_flash_group` VALUES (1010,'FLASH GROUP 101 - basic',101,'0000-00-00 00:00:00'),(1011,'FLASH GROUP 101 - 2',101,'0000-00-00 00:00:00'),(1020,'FLASH GROUP 102 - basic',102,'0000-00-00 00:00:00'),(1021,'FLASH GROUP 102 - 2',102,'0000-00-00 00:00:00'),(1030,'FLASH GROUP 103 - basic',103,'0000-00-00 00:00:00'),(1031,'FLASH GROUP 103 - 2',103,'0000-00-00 00:00:00'),(2010,'FLASH GROUP 201 - basic',201,'0000-00-00 00:00:00'),(2011,'FLASH GROUP 201 - 2',201,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `precedent_flash_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `precedent_flash_group_user_link`
--

DROP TABLE IF EXISTS `precedent_flash_group_user_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precedent_flash_group_user_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `precedent_flash_group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique` (`user_id`,`precedent_flash_group_id`),
  KEY `FK_precedent_flash_group_user_link_precedent_flash_group_id` (`precedent_flash_group_id`),
  CONSTRAINT `FK_precedent_flash_group_user_link_precedent_flash_group_id` FOREIGN KEY (`precedent_flash_group_id`) REFERENCES `precedent_flash_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_flash_group_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20113 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=49 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precedent_flash_group_user_link`
--

LOCK TABLES `precedent_flash_group_user_link` WRITE;
/*!40000 ALTER TABLE `precedent_flash_group_user_link` DISABLE KEYS */;
INSERT INTO `precedent_flash_group_user_link` VALUES (10101,1,1010),(10111,1,1011),(10301,1,1030),(10311,1,1031),(20101,1,2010),(10102,2,1010),(10202,2,1020),(10212,2,1021),(10302,2,1030),(20102,2,2010),(20112,2,2011),(10103,3,1010);
/*!40000 ALTER TABLE `precedent_flash_group_user_link` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `precedent_flash_group_user_link_after_insert` AFTER INSERT ON `precedent_flash_group_user_link`
FOR EACH ROW
BEGIN
	INSERT INTO mark_calc
	(
		project_id,
		precedent_group_id,
		precedent_flash_group_id,
		precedent_flash_group_user_link_id,
		ratee_user_id,
		rater_user_id,
		adder_user_id,
		competence_id,
		competence_set_id,
		type,
		value,
		comment,
		precedent_id
	)
	SELECT
		project_id,
		precedent_group_id,
		precedent_flash_group_id,
		NEW.id AS precedent_flash_group_user_link_id,
		NEW.user_id AS ratee_user_id,
		rater_user_id,
		adder_user_id,
		competence_id,
		competence_set_id,
		type,
		value,
		comment,
		precedent_id
	FROM
		mark_calc
	WHERE
		precedent_flash_group_id = NEW.precedent_flash_group_id
		AND type='group'
	GROUP BY
		competence_id,
		rater_user_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `precedent_flash_group_user_link_after_delete` AFTER DELETE ON `precedent_flash_group_user_link`
FOR EACH ROW
BEGIN
	DELETE FROM mark_calc
	WHERE
		precedent_flash_group_user_link_id = OLD.id
		AND type='group';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `precedent_group`
--

DROP TABLE IF EXISTS `precedent_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precedent_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_precedent_group_project_id` (`project_id`),
  CONSTRAINT `FK_precedent_group_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precedent_group`
--

LOCK TABLES `precedent_group` WRITE;
/*!40000 ALTER TABLE `precedent_group` DISABLE KEYS */;
INSERT INTO `precedent_group` VALUES (101,'TEST 1',1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(102,'TEST 2',1,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(103,'TEST 3',1,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(201,'TEST 2-1',2,1,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `precedent_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `precedent_group_link`
--

DROP TABLE IF EXISTS `precedent_group_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precedent_group_link` (
  `precedent_group_id` int(10) unsigned NOT NULL,
  `precedent_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`precedent_group_id`,`precedent_id`),
  KEY `FK_precedent_group_link_precedent_id` (`precedent_id`),
  CONSTRAINT `FK_precedent_group_link_precedent_group_id` FOREIGN KEY (`precedent_group_id`) REFERENCES `precedent_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_group_link_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precedent_group_link`
--

LOCK TABLES `precedent_group_link` WRITE;
/*!40000 ALTER TABLE `precedent_group_link` DISABLE KEYS */;
INSERT INTO `precedent_group_link` VALUES (101,101),(102,102),(103,103),(201,201);
/*!40000 ALTER TABLE `precedent_group_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `precedent_mark_adder`
--

DROP TABLE IF EXISTS `precedent_mark_adder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precedent_mark_adder` (
  `precedent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adder_user_id` int(10) unsigned NOT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`precedent_id`,`adder_user_id`),
  KEY `FK_precedent_mark_adder_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_precedent_mark_adder_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_mark_adder_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precedent_mark_adder`
--

LOCK TABLES `precedent_mark_adder` WRITE;
/*!40000 ALTER TABLE `precedent_mark_adder` DISABLE KEYS */;
/*!40000 ALTER TABLE `precedent_mark_adder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `precedent_ratee_calc`
--

DROP TABLE IF EXISTS `precedent_ratee_calc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precedent_ratee_calc` (
  `precedent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ratee_user_id` int(10) unsigned NOT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`precedent_id`,`ratee_user_id`),
  KEY `FK_precedent_ratee_calc_ratee_user_id` (`ratee_user_id`),
  CONSTRAINT `FK_precedent_ratee_calc_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_ratee_calc_ratee_user_id` FOREIGN KEY (`ratee_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precedent_ratee_calc`
--

LOCK TABLES `precedent_ratee_calc` WRITE;
/*!40000 ALTER TABLE `precedent_ratee_calc` DISABLE KEYS */;
INSERT INTO `precedent_ratee_calc` VALUES (101,1,0,0,0),(101,2,0,0,0),(101,3,0,0,0),(102,2,0,0,0),(103,1,0,0,0),(103,2,0,0,0),(201,1,0,0,0),(201,2,0,0,0);
/*!40000 ALTER TABLE `precedent_ratee_calc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `precedent_rater_link`
--

DROP TABLE IF EXISTS `precedent_rater_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precedent_rater_link` (
  `precedent_id` int(10) unsigned NOT NULL,
  `rater_user_id` int(10) unsigned NOT NULL,
  `comment` text,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`precedent_id`,`rater_user_id`),
  KEY `FK_precedent_rater_link_rater_user_id` (`rater_user_id`),
  CONSTRAINT `FK_precedent_rater_link_precedent_id` FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_precedent_rater_link_rater_user_id` FOREIGN KEY (`rater_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=38 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precedent_rater_link`
--

LOCK TABLES `precedent_rater_link` WRITE;
/*!40000 ALTER TABLE `precedent_rater_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `precedent_rater_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professiogram`
--

DROP TABLE IF EXISTS `professiogram`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professiogram` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rate_id` int(10) unsigned DEFAULT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_professiogram_competence_set_id` (`competence_set_id`),
  KEY `FK_professiogram_rate_id` (`rate_id`),
  KEY `FK_professiogram_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_professiogram_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_professiogram_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_professiogram_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professiogram`
--

LOCK TABLES `professiogram` WRITE;
/*!40000 ALTER TABLE `professiogram` DISABLE KEYS */;
INSERT INTO `professiogram` VALUES (1,'Prof 1',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',101,1,'',1,0),(2,'Prof 2',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',102,1,'',1,0),(3,'Prof 3',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',103,1,'',1,0),(4,'ABC - Prof 4',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',104,1,'',1,0),(5,'Prof 5',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',201,2,'',1,0),(6,'Prof 6 - deleted',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',106,1,NULL,1,0);
/*!40000 ALTER TABLE `professiogram` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `name` varchar(40) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finish_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `main_url` varchar(255) NOT NULL DEFAULT '',
  `logo_big_height` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_big_width` int(10) unsigned NOT NULL DEFAULT '0',
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `main_url_human_calc` varchar(255) DEFAULT NULL,
  `precedent_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `user_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `vector_competence_set_id` int(10) unsigned DEFAULT NULL,
  `marks_competence_set_id` int(10) unsigned DEFAULT NULL,
  `use_game_name` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vector_self_must_skip` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vector_self_can_skip` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vector_self_can_skip_if_results` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `vector_focus_can_skip` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vector_focus_min_competence_count` int(10) unsigned NOT NULL DEFAULT '4',
  `vector_focus_max_competence_count` int(10) unsigned NOT NULL DEFAULT '10',
  `vector_is_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `role_recommendation_is_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `marks_are_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `events_are_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `endpoint_url` varchar(255) NOT NULL DEFAULT '',
  `endpoint_resend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `endpoint_is_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `descr_short` varchar(2048) DEFAULT NULL,
  `logo_small_width` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_small_height` int(10) unsigned NOT NULL DEFAULT '0',
  `materials_are_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `endpoint_connect_attempts_left` tinyint(1) unsigned NOT NULL DEFAULT '5',
  `endpoint_last_connect_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `endpoint_last_connect_result` varchar(40) NOT NULL DEFAULT '',
  `show_user_numbers` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `communities_are_on` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `community_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `event_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `material_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `join_premoderation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `access_read` enum('guest','user','member','moderator','admin') NOT NULL DEFAULT 'guest',
  `access_communities_add` enum('user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'user',
  `access_communities_read` enum('guest','user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
  `access_events_read` enum('guest','user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
  `access_events_comment` enum('user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'user',
  `access_materials_read` enum('guest','user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
  `access_materials_comment` enum('user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'user',
  `access_precedents_read` enum('guest','user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
  `access_precedents_comment` enum('user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'user',
  `bg_head_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_main_url` varchar(255) DEFAULT NULL,
  `bg_body_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_body_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_body_main_url` varchar(255) DEFAULT NULL,
  `logo_version` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_head_version` int(10) unsigned NOT NULL DEFAULT '0',
  `bg_body_version` int(10) unsigned NOT NULL DEFAULT '0',
  `custom_feed_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `domain_is_on` tinyint(4) NOT NULL DEFAULT '0',
  `domain_name` varchar(32) NOT NULL DEFAULT '',
  `favicon_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `favicon_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `favicon_main_url` varchar(255) DEFAULT NULL,
  `favicon_version` int(10) unsigned NOT NULL DEFAULT '0',
  `color_scheme` enum('green','blue','yellow','red') NOT NULL DEFAULT 'green',
  PRIMARY KEY (`id`),
  KEY `FK_project_vector_competence_set_id` (`vector_competence_set_id`),
  KEY `FK_project_marks_competence_set_id` (`marks_competence_set_id`),
  KEY `FK_project_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_project_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_marks_competence_set_id` FOREIGN KEY (`marks_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_vector_competence_set_id` FOREIGN KEY (`vector_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,'PROJECT 1 (SET 1, 2)','p1','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0,0,'',0,0,NULL,1,NULL,0,0,1,NULL,0,0,0,1,0,1,4,1,0,0,0,'',0,0,NULL,0,0,0,5,'0000-00-00 00:00:00','',0,1,0,0,0,0,0,0,'guest','user','guest','guest','user','guest','user','guest','user',0,0,NULL,0,0,NULL,0,0,0,0,0,'',0,0,'',0,'green'),(2,'PROJECT 2 (SET 1)','p2','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0,0,'',0,0,NULL,1,NULL,0,0,2,NULL,0,0,0,1,0,4,10,1,0,0,0,'',0,0,NULL,0,0,0,5,'0000-00-00 00:00:00','',0,1,0,0,0,0,0,0,'guest','user','guest','guest','user','guest','user','guest','user',0,0,NULL,0,0,NULL,0,0,0,0,0,'',0,0,'',0,'green'),(3,'PROJECT 3 (SET 1)','p3','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0,0,'',0,0,NULL,1,NULL,0,0,NULL,NULL,0,0,0,1,0,4,10,0,0,0,0,'',0,0,NULL,0,0,0,5,'0000-00-00 00:00:00','',0,1,0,0,0,0,0,0,'guest','user','guest','guest','user','guest','user','guest','user',0,0,NULL,0,0,NULL,0,0,0,0,0,'',0,0,'',0,'green'),(4,'PROJECT 4 (SET 2)','p4','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0,0,'',0,0,NULL,1,NULL,0,0,NULL,NULL,0,0,0,1,0,4,10,0,0,0,0,'',0,0,NULL,0,0,0,5,'0000-00-00 00:00:00','',0,1,0,0,0,0,0,0,'guest','user','guest','guest','user','guest','user','guest','user',0,0,NULL,0,0,NULL,0,0,0,0,0,'',0,0,'',0,'green');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_admin`
--

DROP TABLE IF EXISTS `project_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_admin` (
  `user_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_role_moderator` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_event_moderator` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_material_moderator` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_precedent_moderator` tinyint(1) NOT NULL DEFAULT '0',
  `is_mark_moderator` tinyint(1) NOT NULL DEFAULT '0',
  `is_community_moderator` tinyint(1) NOT NULL DEFAULT '0',
  `is_widget_moderator` tinyint(1) NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adder_admin_user_id` int(10) unsigned DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_admin_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`project_id`),
  KEY `FK_project_admin_project_id` (`project_id`),
  KEY `FK_project_admin_adder_admin_user_id` (`adder_admin_user_id`),
  KEY `FK_project_admin_editor_admin_user_id` (`editor_admin_user_id`),
  CONSTRAINT `FK_project_admin_adder_admin_user_id` FOREIGN KEY (`adder_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_admin_editor_admin_user_id` FOREIGN KEY (`editor_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_admin_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_admin_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_admin`
--

LOCK TABLES `project_admin` WRITE;
/*!40000 ALTER TABLE `project_admin` DISABLE KEYS */;
INSERT INTO `project_admin` VALUES (1,1,1,0,0,0,0,0,0,0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(1,2,1,0,0,0,0,0,0,0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(1,3,1,0,0,0,0,0,0,0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(1,4,1,0,0,0,0,0,0,0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `project_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_competence_set_link_calc`
--

DROP TABLE IF EXISTS `project_competence_set_link_calc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_competence_set_link_calc` (
  `project_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `personal_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_raw_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`,`competence_set_id`),
  KEY `FK_project_competence_set_link_calc_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_project_competence_set_link_calc_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_competence_set_link_calc_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_competence_set_link_calc`
--

LOCK TABLES `project_competence_set_link_calc` WRITE;
/*!40000 ALTER TABLE `project_competence_set_link_calc` DISABLE KEYS */;
INSERT INTO `project_competence_set_link_calc` VALUES (1,1,0,0,0,33,0,0,0,0),(1,2,0,0,0,4,0,0,0,0),(2,1,0,0,0,6,0,0,0,0);
/*!40000 ALTER TABLE `project_competence_set_link_calc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_custom_feed`
--

DROP TABLE IF EXISTS `project_custom_feed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_custom_feed` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(40) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `descr` text NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_project_custom_feed_project_id` (`project_id`) USING BTREE,
  CONSTRAINT `FK_project_custom_feed_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_custom_feed`
--

LOCK TABLES `project_custom_feed` WRITE;
/*!40000 ALTER TABLE `project_custom_feed` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_custom_feed` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `project_custom_feed_after_insert` AFTER INSERT ON `project_custom_feed`
FOR EACH ROW
BEGIN
	UPDATE project
	SET 
		custom_feed_count_calc = custom_feed_count_calc + 1
	WHERE id = NEW.project_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=``@`localhost`*/ /*!50003 TRIGGER `project_custom_feed_after_delete` AFTER DELETE ON `project_custom_feed`
FOR EACH ROW
BEGIN
	UPDATE project
	SET 
		custom_feed_count_calc = custom_feed_count_calc - 1
	WHERE id = OLD.project_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_custom_feed_source`
--

DROP TABLE IF EXISTS `project_custom_feed_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_custom_feed_source` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `custom_feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `section_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique_item` (`custom_feed_id`,`community_id`,`section_id`) USING BTREE,
  KEY `FK_project_custom_feed_source_community_id` (`community_id`) USING BTREE,
  KEY `FK_project_custom_feed_source_section_id` (`section_id`) USING BTREE,
  CONSTRAINT `FK_project_custom_feed_source_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_custom_feed_source_custom_feed_id` FOREIGN KEY (`custom_feed_id`) REFERENCES `project_custom_feed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_custom_feed_source_section_id` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_custom_feed_source`
--

LOCK TABLES `project_custom_feed_source` WRITE;
/*!40000 ALTER TABLE `project_custom_feed_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_custom_feed_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_import`
--

DROP TABLE IF EXISTS `project_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_import` (
  `to_project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `from_project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `events_status` int(10) DEFAULT '0',
  `materials_status` int(10) DEFAULT '0',
  PRIMARY KEY (`to_project_id`,`from_project_id`),
  KEY `FK_project_import_from_project_id` (`from_project_id`),
  CONSTRAINT `FK_project_import_from_project_id` FOREIGN KEY (`from_project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_import_to_project_id` FOREIGN KEY (`to_project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_import`
--

LOCK TABLES `project_import` WRITE;
/*!40000 ALTER TABLE `project_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_intmenu_item`
--

DROP TABLE IF EXISTS `project_intmenu_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_intmenu_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(60) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_project_intmenu_item_project_id` (`project_id`),
  CONSTRAINT `FK_project_intmenu_item_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_intmenu_item`
--

LOCK TABLES `project_intmenu_item` WRITE;
/*!40000 ALTER TABLE `project_intmenu_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_intmenu_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_role`
--

DROP TABLE IF EXISTS `project_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `based_on_professiogram_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_project_role_project_id` (`project_id`),
  KEY `FK_project_role_based_on_professiogram_id` (`based_on_professiogram_id`),
  KEY `FK_project_role_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_project_role_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_role_based_on_professiogram_id` FOREIGN KEY (`based_on_professiogram_id`) REFERENCES `professiogram` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_role_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=260 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_role`
--

LOCK TABLES `project_role` WRITE;
/*!40000 ALTER TABLE `project_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_role_rate_link`
--

DROP TABLE IF EXISTS `project_role_rate_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_role_rate_link` (
  `project_role_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_role_id`,`rate_id`),
  KEY `FK_project_role_rate_link_rate_id` (`rate_id`),
  KEY `FK_project_role_rate_link_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_project_role_rate_link_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_role_rate_link_project_role_id` FOREIGN KEY (`project_role_id`) REFERENCES `project_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_role_rate_link_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_role_rate_link`
--

LOCK TABLES `project_role_rate_link` WRITE;
/*!40000 ALTER TABLE `project_role_rate_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_role_rate_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_taxonomy_item`
--

DROP TABLE IF EXISTS `project_taxonomy_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_taxonomy_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `position` int(10) unsigned NOT NULL,
  `type` set('module','static_page','link','folder') NOT NULL,
  `title` varchar(255) NOT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `static_page_name` varchar(255) DEFAULT NULL,
  `block_set_id` int(10) unsigned DEFAULT NULL,
  `module_name` varchar(150) DEFAULT NULL,
  `show_in_menu` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL,
  `edit_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_project_taxonomy_item_project_id` (`project_id`),
  KEY `FK_project_taxonomy_item_parent_id` (`parent_id`),
  KEY `FK_project_taxonomy_item_block_set_id` (`block_set_id`),
  CONSTRAINT `FK_project_taxonomy_item_block_set_id` FOREIGN KEY (`block_set_id`) REFERENCES `block_set` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_taxonomy_item_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `project_taxonomy_item` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_taxonomy_item_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_taxonomy_item`
--

LOCK TABLES `project_taxonomy_item` WRITE;
/*!40000 ALTER TABLE `project_taxonomy_item` DISABLE KEYS */;
INSERT INTO `project_taxonomy_item` VALUES (1,1,NULL,1,'module','Сообщества',NULL,NULL,NULL,'communities',1,'2012-11-16 14:34:29','2012-11-16 14:34:29'),(2,2,NULL,1,'module','Сообщества',NULL,NULL,NULL,'communities',1,'2012-11-16 14:34:29','2012-11-16 14:34:29'),(3,3,NULL,1,'module','Сообщества',NULL,NULL,NULL,'communities',1,'2012-11-16 14:34:29','2012-11-16 14:34:29'),(4,4,NULL,1,'module','Сообщества',NULL,NULL,NULL,'communities',1,'2012-11-16 14:34:29','2012-11-16 14:34:29'),(8,1,NULL,2,'module','Вектор',NULL,NULL,NULL,'vector',1,'2012-11-16 14:34:29','2012-11-16 14:34:29'),(9,2,NULL,2,'module','Вектор',NULL,NULL,NULL,'vector',1,'2012-11-16 14:34:29','2012-11-16 14:34:29'),(11,3,NULL,7,'module','Участники',NULL,NULL,NULL,'members',1,'2012-11-16 14:34:29','2012-11-16 14:34:29'),(12,4,NULL,7,'module','Участники',NULL,NULL,NULL,'members',1,'2012-11-16 14:34:29','2012-11-16 14:34:29'),(13,1,NULL,7,'module','Участники',NULL,NULL,NULL,'members',1,'2012-11-16 14:34:29','2012-11-16 14:34:29'),(14,2,NULL,7,'module','Участники',NULL,NULL,NULL,'members',1,'2012-11-16 14:34:29','2012-11-16 14:34:29');
/*!40000 ALTER TABLE `project_taxonomy_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_widget`
--

DROP TABLE IF EXISTS `project_widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_widget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `column` int(2) unsigned NOT NULL DEFAULT '1',
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  `title` varchar(100) NOT NULL DEFAULT '',
  `type_id` char(40) NOT NULL,
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  `old_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_project_widget_project_id` (`project_id`) USING BTREE,
  KEY `FK_project_widget_type_id` (`type_id`) USING BTREE,
  CONSTRAINT `FK_project_widget_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_widget_type_id` FOREIGN KEY (`type_id`) REFERENCES `project_widget_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_widget`
--

LOCK TABLES `project_widget` WRITE;
/*!40000 ALTER TABLE `project_widget` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_widget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_widget_communities`
--

DROP TABLE IF EXISTS `project_widget_communities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_widget_communities` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  PRIMARY KEY (`widget_id`,`community_id`),
  KEY `FK_project_widget_communities_2` (`community_id`),
  CONSTRAINT `FK_project_widget_communities_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `project_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_widget_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_widget_communities`
--

LOCK TABLES `project_widget_communities` WRITE;
/*!40000 ALTER TABLE `project_widget_communities` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_widget_communities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_widget_custom_feed`
--

DROP TABLE IF EXISTS `project_widget_custom_feed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_widget_custom_feed` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `project_custom_feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`widget_id`),
  KEY `FK_project_widget_custom_feed_project_custom_feed_id` (`project_custom_feed_id`) USING BTREE,
  CONSTRAINT `FK_project_widget_custom_feed_project_feed_id` FOREIGN KEY (`project_custom_feed_id`) REFERENCES `project_custom_feed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_widget_custom_feed_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `project_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_widget_custom_feed`
--

LOCK TABLES `project_widget_custom_feed` WRITE;
/*!40000 ALTER TABLE `project_widget_custom_feed` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_widget_custom_feed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_widget_default`
--

DROP TABLE IF EXISTS `project_widget_default`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_widget_default` (
  `column` int(2) unsigned NOT NULL DEFAULT '1',
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  `title` varchar(100) NOT NULL DEFAULT '',
  `type_id` char(40) NOT NULL,
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`column`,`position`),
  KEY `FK_project_widget_default_type_id` (`type_id`) USING BTREE,
  CONSTRAINT `FK_project_widget_default_type_id` FOREIGN KEY (`type_id`) REFERENCES `project_widget_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_widget_default`
--

LOCK TABLES `project_widget_default` WRITE;
/*!40000 ALTER TABLE `project_widget_default` DISABLE KEYS */;
INSERT INTO `project_widget_default` VALUES (1,2,'Статистика','stat',0),(2,1,'Последние записи','last_posts',5),(2,2,'Последние комментарии','last_comments',5),(3,1,'Новые лица','last_users',10),(3,2,'Активные участники','top_users',10);
/*!40000 ALTER TABLE `project_widget_default` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_widget_text`
--

DROP TABLE IF EXISTS `project_widget_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_widget_text` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `html` text NOT NULL,
  PRIMARY KEY (`widget_id`),
  CONSTRAINT `FK_project_widget_text_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `project_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_widget_text`
--

LOCK TABLES `project_widget_text` WRITE;
/*!40000 ALTER TABLE `project_widget_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_widget_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_widget_type`
--

DROP TABLE IF EXISTS `project_widget_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_widget_type` (
  `type_id` char(40) NOT NULL DEFAULT '',
  `title` char(100) DEFAULT NULL,
  `multiple_instances` tinyint(1) NOT NULL DEFAULT '0',
  `ctrl` char(100) NOT NULL,
  `edit_ctrl` char(100) NOT NULL DEFAULT '',
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_widget_type`
--

LOCK TABLES `project_widget_type` WRITE;
/*!40000 ALTER TABLE `project_widget_type` DISABLE KEYS */;
INSERT INTO `project_widget_type` VALUES ('communities','Сообщества',1,'communities','communities',0,7),('custom_feed','Лента',1,'custom_feed','custom_feed',5,10),('events','Ближайшие события',0,'events','lister',5,5),('last_comments','Последние комментарии',0,'last_comments','lister',5,2),('last_posts','Последние записи',0,'last_posts','lister',5,1),('last_users','Новые лица',0,'last_users','lister',10,3),('materials','Последние материалы',0,'materials','lister',5,6),('stat','Статистика',0,'stat','',0,8),('text','Текстовый блок',1,'text','text',0,9),('top_users','Активные участники',0,'top_users','lister',10,4);
/*!40000 ALTER TABLE `project_widget_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rate`
--

DROP TABLE IF EXISTS `rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rate_type_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_rate_rate_type_id` (`rate_type_id`),
  CONSTRAINT `FK_rate_rate_type_id` FOREIGN KEY (`rate_type_id`) REFERENCES `rate_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rate`
--

LOCK TABLES `rate` WRITE;
/*!40000 ALTER TABLE `rate` DISABLE KEYS */;
INSERT INTO `rate` VALUES (101,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(102,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(103,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(104,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(106,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(201,1,2,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rate_competence_link`
--

DROP TABLE IF EXISTS `rate_competence_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rate_competence_link` (
  `rate_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `mark` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rate_id`,`competence_id`),
  KEY `FK_rate_competence_link_competence_id` (`competence_id`),
  CONSTRAINT `FK_rate_competence_link_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_rate_competence_link_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rate_competence_link`
--

LOCK TABLES `rate_competence_link` WRITE;
/*!40000 ALTER TABLE `rate_competence_link` DISABLE KEYS */;
INSERT INTO `rate_competence_link` VALUES (101,101,3),(101,102,3),(101,106,1),(102,102,3),(102,103,2),(103,104,2),(103,105,2),(104,101,1),(104,102,1),(104,105,1),(201,201,3),(201,202,2);
/*!40000 ALTER TABLE `rate_competence_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rate_type`
--

DROP TABLE IF EXISTS `rate_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rate_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `main_table` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rate_type`
--

LOCK TABLES `rate_type` WRITE;
/*!40000 ALTER TABLE `rate_type` DISABLE KEYS */;
INSERT INTO `rate_type` VALUES (1,'Профессиограмма','professiogram'),(2,'Роль','project_role'),(3,'Событие','event'),(4,'Материал','material');
/*!40000 ALTER TABLE `rate_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `community_id` int(10) unsigned NOT NULL,
  `descr` mediumtext,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `access_add_post` enum('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user',
  `access_comment` enum('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user',
  `access_default` tinyint(1) NOT NULL DEFAULT '1',
  `old_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique` (`name`,`community_id`) USING HASH,
  KEY `FK_section_community_id` (`community_id`),
  CONSTRAINT `FK_section_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section`
--

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_params`
--

DROP TABLE IF EXISTS `sys_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_params` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(60) NOT NULL DEFAULT '',
  `info_email` varchar(100) NOT NULL,
  `main_page_text` mediumtext,
  `copyright_start_year` char(4) NOT NULL DEFAULT '1900',
  `is_devyourself` tinyint(1) NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ga_tracking_id` varchar(40) NOT NULL DEFAULT '',
  `logo_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_version` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_params`
--

LOCK TABLES `sys_params` WRITE;
/*!40000 ALTER TABLE `sys_params` DISABLE KEYS */;
INSERT INTO `sys_params` VALUES (1,'DevYourself','info@devyourself.ru','<p>Text</p>','2007',1,'0000-00-00 00:00:00','0000-00-00 00:00:00','',0,0,0);
/*!40000 ALTER TABLE `sys_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `university`
--

DROP TABLE IF EXISTS `university`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `university` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(40) NOT NULL,
  `title_full` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `university`
--

LOCK TABLES `university` WRITE;
/*!40000 ALTER TABLE `university` DISABLE KEYS */;
INSERT INTO `university` VALUES (1,'ГУ ВШЭ','Государственный университет - Высшая школа экономики'),(2,'МИФИ','Московский инженерно-физический институт'),(3,'МГИМО (У)','Московский государственный институт международных отношений МИД России'),(4,'МГУ','Московский государственный университет им. М. В. Ломоносова'),(5,'КГУ','Костромской государственный университет'),(6,'РГУ им. И. Канта','Российский государственный университет имени Иммануила Канта'),(7,'МФТИ (ГУ)','Московский физико-технический институт (государственный университет)'),(8,'МГТУ им. Баумана','Московский государственный технический университет имени Н. Э. Баумана');
/*!40000 ALTER TABLE `university` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL DEFAULT '',
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sex` enum('m','f','?') NOT NULL DEFAULT '?',
  `first_name` varchar(60) DEFAULT NULL,
  `mid_name` varchar(60) DEFAULT NULL,
  `last_name` varchar(60) DEFAULT NULL,
  `photo_large_width` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_large_height` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_big_width` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_big_height` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_mid_width` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_mid_height` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_small_width` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_small_height` int(10) unsigned NOT NULL DEFAULT '0',
  `profile_url` varchar(255) NOT NULL DEFAULT '',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `university_id` int(10) unsigned DEFAULT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_large_url` varchar(255) DEFAULT NULL,
  `photo_big_url` varchar(255) DEFAULT NULL,
  `photo_mid_url` varchar(255) DEFAULT NULL,
  `photo_small_url` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `skype` varchar(100) DEFAULT NULL,
  `phone` varchar(40) DEFAULT NULL,
  `icq` varchar(12) DEFAULT NULL,
  `www` varchar(255) DEFAULT NULL,
  `www_human_calc` varchar(255) DEFAULT NULL,
  `about` text,
  `profile_id_bak` int(10) unsigned DEFAULT NULL,
  `register_ip` varchar(15) DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pass_recovery_key` varchar(40) DEFAULT NULL,
  `pass_recovery_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `allow_email_on_reply` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `allow_email_on_premoderation` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `photo_version` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `KEY_user_login` (`login`),
  KEY `FK_user_university_id` (`university_id`),
  CONSTRAINT `FK_user_university_id` FOREIGN KEY (`university_id`) REFERENCES `university` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=275 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'TEST 1','535f17dede6c7e9598004c1779b23b9f909f5610',0,'?','','','',0,0,0,0,0,0,0,0,'','0000-00-00 00:00:00',NULL,0,0,0,'','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,1,0),(2,'TEST 2','535f17dede6c7e9598004c1779b23b9f909f5610',0,'?','','','',0,0,0,0,0,0,0,0,'','0000-00-00 00:00:00',NULL,0,0,0,'','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,1,0),(3,'TEST 3','535f17dede6c7e9598004c1779b23b9f909f5610',0,'?','','','',0,0,0,0,0,0,0,0,'','0000-00-00 00:00:00',NULL,0,0,0,'','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,1,0),(4,'TEST 4','535f17dede6c7e9598004c1779b23b9f909f5610',0,'?','','','',0,0,0,0,0,0,0,0,'','0000-00-00 00:00:00',NULL,0,0,0,'','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,1,0),(5,'TEST 5','535f17dede6c7e9598004c1779b23b9f909f5610',0,'?','','','',0,0,0,0,0,0,0,0,'','0000-00-00 00:00:00',NULL,0,0,0,'','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,1,0),(100,'TEST 100 - for vector','535f17dede6c7e9598004c1779b23b9f909f5610',0,'?',NULL,NULL,NULL,0,0,0,0,0,0,0,0,'','0000-00-00 00:00:00',NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,1,0),(200,'TEST 200 - for vector_info','535f17dede6c7e9598004c1779b23b9f909f5610',0,'?',NULL,NULL,NULL,0,0,0,0,0,0,0,0,'','0000-00-00 00:00:00',NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,1,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_competence_set_link_calc`
--

DROP TABLE IF EXISTS `user_competence_set_link_calc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_competence_set_link_calc` (
  `user_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `project_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `personal_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `project_count_with_trans_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `default_vector_id_calc` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`competence_set_id`),
  KEY `FK_user_competence_set_link_calc_competence_set_id` (`competence_set_id`),
  KEY `KEY_user_competence_set_link_calc_default_vector_id_calc` (`default_vector_id_calc`),
  CONSTRAINT `FK_user_competence_set_link_calc_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_competence_set_link_calc_default_vector_id_calc` FOREIGN KEY (`default_vector_id_calc`) REFERENCES `vector` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_user_competence_set_link_calc_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_competence_set_link_calc`
--

LOCK TABLES `user_competence_set_link_calc` WRITE;
/*!40000 ALTER TABLE `user_competence_set_link_calc` DISABLE KEYS */;
INSERT INTO `user_competence_set_link_calc` VALUES (2,2,0,0,0,0,0,0,0,0,3);
/*!40000 ALTER TABLE `user_competence_set_link_calc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_competence_set_project_link`
--

DROP TABLE IF EXISTS `user_competence_set_project_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_competence_set_project_link` (
  `user_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`competence_set_id`,`project_id`),
  KEY `FK_user_competence_set_project_link_competence_set_id` (`competence_set_id`),
  KEY `FK_user_competence_set_project_link_project_id` (`project_id`),
  CONSTRAINT `FK_user_competence_set_project_link_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_competence_set_project_link_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_competence_set_project_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_competence_set_project_link`
--

LOCK TABLES `user_competence_set_project_link` WRITE;
/*!40000 ALTER TABLE `user_competence_set_project_link` DISABLE KEYS */;
INSERT INTO `user_competence_set_project_link` VALUES (1,1,1,0,0,18),(1,1,2,0,0,3),(1,2,1,0,0,2),(2,1,1,0,0,15),(2,1,2,0,0,3),(2,2,1,0,0,1);
/*!40000 ALTER TABLE `user_competence_set_project_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES (1,'G1','0000-00-00 00:00:00','0000-00-00 00:00:00',3),(2,'G2','0000-00-00 00:00:00','0000-00-00 00:00:00',2),(3,'G3','0000-00-00 00:00:00','0000-00-00 00:00:00',2),(4,'G4','0000-00-00 00:00:00','0000-00-00 00:00:00',1);
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_project_link`
--

DROP TABLE IF EXISTS `user_project_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_project_link` (
  `project_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('deleted','pretender','member','moderator','admin') NOT NULL DEFAULT 'deleted',
  `is_analyst` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `personal_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `total_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `game_name` varchar(100) DEFAULT NULL,
  `last_vector_id_calc` int(10) unsigned DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `number` int(10) unsigned NOT NULL DEFAULT '0',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `FK_user_project_link_user_id` (`user_id`),
  KEY `KEY_user_project_link_last_vector_id_calc` (`last_vector_id_calc`),
  CONSTRAINT `FK_user_project_link_last_vector_id_calc` FOREIGN KEY (`last_vector_id_calc`) REFERENCES `vector` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_user_project_link_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_project_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_project_link`
--

LOCK TABLES `user_project_link` WRITE;
/*!40000 ALTER TABLE `user_project_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_project_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_user_group_link`
--

DROP TABLE IF EXISTS `user_user_group_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_user_group_link` (
  `user_id` int(10) unsigned NOT NULL,
  `user_group_id` int(10) unsigned NOT NULL,
  `rights` enum('member','admin') NOT NULL DEFAULT 'member',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`user_group_id`),
  KEY `FK_user_user_group_link_user_group_id` (`user_group_id`),
  CONSTRAINT `FK_user_user_group_link_user_group_id` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_user_group_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_user_group_link`
--

LOCK TABLES `user_user_group_link` WRITE;
/*!40000 ALTER TABLE `user_user_group_link` DISABLE KEYS */;
INSERT INTO `user_user_group_link` VALUES (1,1,'member','0000-00-00 00:00:00'),(1,2,'member','0000-00-00 00:00:00'),(2,1,'member','0000-00-00 00:00:00'),(2,2,'member','0000-00-00 00:00:00'),(2,3,'member','0000-00-00 00:00:00'),(3,1,'member','0000-00-00 00:00:00'),(3,3,'member','0000-00-00 00:00:00'),(3,4,'member','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `user_user_group_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vector`
--

DROP TABLE IF EXISTS `vector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vector` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `future_professiogram_count_calc` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `focus_competence_count_calc` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `calculations_base` enum('self','results') NOT NULL DEFAULT 'results',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `future_edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `self_edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `focus_edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_vector_project_id` (`project_id`),
  KEY `FK_vector_competence_set_id` (`competence_set_id`),
  KEY `Index_6` (`user_id`,`competence_set_id`,`project_id`),
  CONSTRAINT `FK_vector_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=222 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vector`
--

LOCK TABLES `vector` WRITE;
/*!40000 ALTER TABLE `vector` DISABLE KEYS */;
INSERT INTO `vector` VALUES (1,1,1,1,0,0,'results','2010-00-00 00:00:00','2010-00-00 00:00:00','2010-00-00 00:00:01','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,1,1,2,0,0,'results','2012-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,2,2,1,0,0,'results','2011-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,3,1,1,0,0,'results','2013-00-00 00:00:00','2013-00-00 00:00:00','2013-00-00 00:00:01','0000-00-00 00:00:00','0000-00-00 00:00:00'),(100,1,100,1,0,0,'results','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(101,3,100,1,0,0,'results','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(102,4,100,1,0,0,'results','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(200,1,200,1,0,0,'results','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(201,2,200,1,0,0,'results','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(202,3,200,1,0,0,'results','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(203,2,200,2,0,0,'results','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vector` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vector_focus`
--

DROP TABLE IF EXISTS `vector_focus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vector_focus` (
  `vector_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`vector_id`,`competence_id`),
  KEY `FK_vector_focus_competence_id` (`competence_id`),
  CONSTRAINT `FK_vector_focus_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_focus_vector_id` FOREIGN KEY (`vector_id`) REFERENCES `vector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=45 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vector_focus`
--

LOCK TABLES `vector_focus` WRITE;
/*!40000 ALTER TABLE `vector_focus` DISABLE KEYS */;
INSERT INTO `vector_focus` VALUES (1,101,'2011-00-00 00:00:01'),(1,102,'2011-00-00 00:00:02'),(1,105,'2009-00-00 00:00:03'),(1,106,'2010-00-00 00:00:06'),(3,101,'2008-00-00 00:00:04'),(4,105,'2010-00-00 00:00:05'),(100,102,'0000-00-00 00:00:00'),(100,103,'0000-00-00 00:00:00'),(100,106,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vector_focus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vector_focus_history`
--

DROP TABLE IF EXISTS `vector_focus_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vector_focus_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vector_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_vector_focus_history_vector_id` (`vector_id`),
  CONSTRAINT `FK_vector_focus_history_vector_id` FOREIGN KEY (`vector_id`) REFERENCES `vector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1401 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vector_focus_history`
--

LOCK TABLES `vector_focus_history` WRITE;
/*!40000 ALTER TABLE `vector_focus_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `vector_focus_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vector_future`
--

DROP TABLE IF EXISTS `vector_future`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vector_future` (
  `vector_id` int(10) unsigned NOT NULL,
  `professiogram_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`vector_id`,`professiogram_id`),
  KEY `FK_vector_future_professiogram_id` (`professiogram_id`),
  CONSTRAINT `FK_vector_future_professiogram_id` FOREIGN KEY (`professiogram_id`) REFERENCES `professiogram` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_future_vector_id` FOREIGN KEY (`vector_id`) REFERENCES `vector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=61 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vector_future`
--

LOCK TABLES `vector_future` WRITE;
/*!40000 ALTER TABLE `vector_future` DISABLE KEYS */;
INSERT INTO `vector_future` VALUES (1,1,'0000-00-00 00:00:00'),(1,2,'0000-00-00 00:00:00'),(3,3,'0000-00-00 00:00:00'),(4,1,'0000-00-00 00:00:00'),(4,4,'0000-00-00 00:00:00'),(100,1,'0000-00-00 00:00:00'),(100,4,'0000-00-00 00:00:00'),(102,3,'0000-00-00 00:00:00'),(102,4,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vector_future` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vector_future_competences_calc`
--

DROP TABLE IF EXISTS `vector_future_competences_calc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vector_future_competences_calc` (
  `vector_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `mark` int(10) unsigned NOT NULL,
  PRIMARY KEY (`vector_id`,`competence_id`),
  KEY `FK_vector_future_competences_calc_competence_id` (`competence_id`),
  CONSTRAINT `FK_vector_future_competences_calc_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_future_competences_calc_vector_id` FOREIGN KEY (`vector_id`) REFERENCES `vector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=34 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vector_future_competences_calc`
--

LOCK TABLES `vector_future_competences_calc` WRITE;
/*!40000 ALTER TABLE `vector_future_competences_calc` DISABLE KEYS */;
INSERT INTO `vector_future_competences_calc` VALUES (1,101,3),(1,102,3),(1,103,2),(1,106,1),(3,104,2),(3,105,2),(4,101,3),(4,102,3),(4,105,1),(4,106,1),(100,101,3),(100,102,3),(100,105,1),(100,106,1),(102,101,1),(102,102,1),(102,104,2),(102,105,2);
/*!40000 ALTER TABLE `vector_future_competences_calc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vector_future_history`
--

DROP TABLE IF EXISTS `vector_future_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vector_future_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vector_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_vector_future_history_vector_id` (`vector_id`),
  CONSTRAINT `FK_vector_future_history_vector_id` FOREIGN KEY (`vector_id`) REFERENCES `vector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=256 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vector_future_history`
--

LOCK TABLES `vector_future_history` WRITE;
/*!40000 ALTER TABLE `vector_future_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `vector_future_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vector_self`
--

DROP TABLE IF EXISTS `vector_self`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vector_self` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `competence_id` int(10) unsigned NOT NULL,
  `mark` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`competence_id`),
  KEY `FK_vector_self_competence_id` (`competence_id`),
  KEY `FK_vector_self_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_vector_self_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_self_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_self_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=99 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vector_self`
--

LOCK TABLES `vector_self` WRITE;
/*!40000 ALTER TABLE `vector_self` DISABLE KEYS */;
INSERT INTO `vector_self` VALUES (1,101,2,1,'0000-00-00 00:00:00'),(1,102,3,1,'0000-00-00 00:00:00'),(1,104,1,1,'0000-00-00 00:00:00'),(1,106,3,1,'0000-00-00 00:00:00'),(1,201,2,2,'0000-00-00 00:00:00'),(2,101,1,1,'0000-00-00 00:00:00'),(2,201,3,2,'0000-00-00 00:00:00'),(100,101,1,1,'0000-00-00 00:00:00'),(100,104,3,1,'0000-00-00 00:00:00'),(100,105,3,1,'0000-00-00 00:00:00'),(100,106,1,1,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vector_self` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vector_self_history`
--

DROP TABLE IF EXISTS `vector_self_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vector_self_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_vector_self_history_user_id` (`user_id`),
  KEY `FK_vector_self_history_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_vector_self_history_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_self_history_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8917 PACK_KEYS=0 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vector_self_history`
--

LOCK TABLES `vector_self_history` WRITE;
/*!40000 ALTER TABLE `vector_self_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `vector_self_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vector_self_stat`
--

DROP TABLE IF EXISTS `vector_self_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vector_self_stat` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `competence_set_id` int(10) unsigned NOT NULL,
  `self_competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`competence_set_id`),
  KEY `FK_vector_self_stat_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_vector_self_stat_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vector_self_stat_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=49 PACK_KEYS=0 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vector_self_stat`
--

LOCK TABLES `vector_self_stat` WRITE;
/*!40000 ALTER TABLE `vector_self_stat` DISABLE KEYS */;
INSERT INTO `vector_self_stat` VALUES (100,1,18,'2010-01-02 02:00:00');
/*!40000 ALTER TABLE `vector_self_stat` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-12-28 16:28:55
