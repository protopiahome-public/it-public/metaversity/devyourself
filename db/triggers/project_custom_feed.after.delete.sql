BEGIN
	UPDATE project
	SET 
		custom_feed_count_calc = custom_feed_count_calc - 1
	WHERE id = OLD.project_id;
END