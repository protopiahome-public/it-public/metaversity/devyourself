BEGIN
	IF	NEW.total_mark_count_calc = 0
	THEN
		DELETE FROM precedent_mark_adder
		WHERE
			precedent_id = NEW.precedent_id
			AND adder_user_id = NEW.adder_user_id;
END;