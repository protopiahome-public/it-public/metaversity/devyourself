BEGIN
	DECLARE my_ratee_user_id INT;
	DECLARE my_precedent_flash_group_id INT;
	DECLARE my_precedent_id INT;
	DECLARE my_project_id INT;
	DECLARE my_competence_set_id INT;

	SELECT
		user_id,
		precedent_flash_group_id
	INTO
		my_ratee_user_id,
		my_precedent_flash_group_id
	FROM precedent_flash_group_user_link
	WHERE id = NEW.precedent_flash_group_user_link_id;
	
	SET my_precedent_id = 
	(
		SELECT pfg.precedent_id
		FROM
			precedent_flash_group_user_link AS pfgul,
			precedent_flash_group AS pfg
		WHERE
			pfgul.id = NEW.precedent_flash_group_user_link_id
			AND pfg.id = pfgul.precedent_flash_group_id
	);

	SET my_project_id =
	(
		SELECT project_id
		FROM precedent
		WHERE id = my_precedent_id
	);

	SET my_competence_set_id =
	(
		SELECT competence_set_id
		FROM competence_full
		WHERE id = NEW.competence_id
	);

	INSERT INTO mark_calc
	SET
		project_id = my_project_id,
		precedent_flash_group_id = my_precedent_flash_group_id,
		precedent_flash_group_user_link_id = NEW.precedent_flash_group_user_link_id,
		ratee_user_id = my_ratee_user_id,
		rater_user_id = NEW.rater_user_id,
		adder_user_id = NEW.adder_user_id,
		competence_id = NEW.competence_id,
		competence_set_id = my_competence_set_id,
		type = 'personal',
		value = NEW.value,
		comment = NEW.comment,
		precedent_id = my_precedent_id;
END;