BEGIN
	DECLARE my_project_id INT;
	SET my_project_id = (SELECT project_id FROM community WHERE id = NEW.community_id);
	IF OLD.is_deleted <> NEW.is_deleted
	THEN
		IF NEW.is_deleted
		THEN
			DELETE FROM post_calc WHERE id = OLD.id;
			CALL post_delete_count_calc(OLD.id, OLD.section_id, OLD.community_id, my_project_id, OLD.author_user_id, OLD.comment_count_calc);
		ELSE
			INSERT INTO post_calc
			SET
				id = NEW.id,
				author_user_id = NEW.author_user_id,
				project_id = my_project_id,
				community_id = NEW.community_id,
				section_id = NEW.section_id,
				hide_in_project_feed = NEW.hide_in_project_feed,
				comment_count_calc = NEW.comment_count_calc,
				last_comment_id = NEW.last_comment_id,
				last_comment_author_user_id = NEW.last_comment_author_user_id,
				last_comment_html = NEW.last_comment_html;
			CALL post_add_count_calc(NEW.id, NEW.section_id, NEW.community_id, @project_id, NEW.author_user_id, NEW.comment_count_calc);
		END IF;
	ELSEIF NEW.is_deleted = 0
	THEN
		UPDATE post_calc 
		SET
				id = NEW.id,
				author_user_id = NEW.author_user_id,
				project_id = my_project_id,
				community_id = NEW.community_id,
				section_id = NEW.section_id,
				hide_in_project_feed = NEW.hide_in_project_feed,
				comment_count_calc = NEW.comment_count_calc,
				last_comment_id = NEW.last_comment_id,
				last_comment_author_user_id = NEW.last_comment_author_user_id,
				last_comment_html = NEW.last_comment_html
		WHERE id = NEW.id;
		IF IFNULL(NEW.section_id <> OLD.section_id, 1)
		THEN
			UPDATE section SET post_count_calc = post_count_calc + 1 WHERE id = NEW.section_id;
			UPDATE section SET post_count_calc = post_count_calc - 1 WHERE id = OLD.section_id;
		END IF;
	END IF;
END