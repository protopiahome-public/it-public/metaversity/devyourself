BEGIN
	UPDATE mark_calc
	SET
		value = NEW.value,
		adder_user_id = NEW.adder_user_id,
		comment = NEW.comment
	WHERE
		precedent_flash_group_user_link_id = OLD.precedent_flash_group_user_link_id
		AND rater_user_id = OLD.rater_user_id
		AND competence_id = OLD.competence_id
		AND type = 'personal';
END;