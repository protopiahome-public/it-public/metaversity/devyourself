-- recount precedent_mark_adder, if adder_user_id has changed
BEGIN
	DECLARE personal_mark_inc INT DEFAULT 0;
	DECLARE group_mark_inc INT DEFAULT 0;
	DECLARE group_mark_raw_inc INT DEFAULT 0;

	IF NEW.adder_user_id != OLD.adder_user_id
	THEN
		IF NEW.type = 'personal'
		THEN
			SET personal_mark_inc = 1;
		ELSE
			SET group_mark_inc = 1;
			IF (
				SELECT COUNT(*)
				FROM mark_calc
				WHERE
					precedent_flash_group_id = NEW.precedent_flash_group_id
					AND rater_user_id = NEW.rater_user_id
					AND adder_user_id = NEW.adder_user_id
					AND competence_id = NEW.competence_id
					AND type = 'group'
				) = 1
			THEN
				SET group_mark_raw_inc = 1;
			END IF;
		END IF;

		UPDATE precedent_mark_adder
		SET
			personal_mark_count_calc = personal_mark_count_calc - personal_mark_inc,
			group_mark_raw_count_calc = group_mark_raw_count_calc - group_mark_raw_inc,
			group_mark_count_calc = group_mark_count_calc - group_mark_inc,
			total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
		WHERE
			precedent_id = NEW.precedent_id
			AND adder_user_id = OLD.adder_user_id;

		UPDATE precedent_mark_adder
		SET
			personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
			group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
			group_mark_count_calc = group_mark_count_calc + group_mark_inc,
			total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
		WHERE
			precedent_id = NEW.precedent_id
			AND adder_user_id = NEW.adder_user_id;
	END IF;
END