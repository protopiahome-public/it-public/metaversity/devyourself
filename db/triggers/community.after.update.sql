BEGIN
	IF OLD.is_deleted <> NEW.is_deleted
	THEN
		IF NEW.is_deleted
		THEN
			CALL community_delete_count_calc(OLD.id, OLD.project_id, OLD.post_count_calc, OLD.comment_count_calc);
			
			DELETE FROM communities_menu_item WHERE community_id = OLD.id;
			DELETE FROM project_widget_communities WHERE community_id = OLD.id;
			DELETE FROM community_widget_communities WHERE community_id = OLD.id;
			DELETE FROM project_custom_feed_source WHERE community_id = OLD.id;
			DELETE FROM community_custom_feed_source WHERE community_id = OLD.id;

			DELETE FROM post_calc WHERE community_id = OLD.id;
		ELSE
			CALL community_add_count_calc(NEW.id, NEW.project_id, NEW.post_count_calc, NEW.comment_count_calc);
			INSERT INTO post_calc (id, author_user_id, project_id, community_id, section_id, hide_in_project_feed)
			(
				SELECT id, author_user_id, NEW.project_id, community_id, section_id, hide_in_project_feed 
				FROM post 
				WHERE community_id = NEW.id AND is_deleted = 0
			);
		END IF;
	END IF;
END