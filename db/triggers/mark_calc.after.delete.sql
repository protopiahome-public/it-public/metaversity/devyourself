-- tables with *_mark_calc fields
--	competence_set
--	competence_full
--	competence_calc
--	precedent
--	project
--	user
--	precedent_rater_link
--	precedent_mark_adder
--	precedent_ratee_calc
--	project_competence_set_link_calc
--	user_competence_set_link_calc
--	user_competence_set_project_link
--	user_project_link

BEGIN
	DECLARE personal_mark_inc INT DEFAULT 0;
	DECLARE group_mark_inc INT DEFAULT 0;
	DECLARE group_mark_raw_inc INT DEFAULT 0;
	
	IF OLD.type = 'personal'
	THEN
		SET personal_mark_inc = -1;
	ELSE
		SET group_mark_inc = -1;
		IF (
			SELECT COUNT(*)
			FROM mark_calc
			WHERE
				precedent_flash_group_id = OLD.precedent_flash_group_id
				AND rater_user_id = OLD.rater_user_id
				AND competence_id = OLD.competence_id
				AND type = 'group'
			) = 0
		THEN
			SET group_mark_raw_inc = -1;
		END IF;
	END IF;

	UPDATE competence_set
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.competence_set_id;

	UPDATE competence_full
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.competence_id;

	UPDATE competence_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		competence_id = OLD.competence_id;

	UPDATE precedent
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.precedent_id;

	UPDATE project
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.project_id;

	UPDATE user
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = OLD.ratee_user_id;

	UPDATE precedent_rater_link
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		precedent_id = OLD.precedent_id
		AND rater_user_id = OLD.rater_user_id;

	UPDATE precedent_mark_adder
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		precedent_id = OLD.precedent_id
		AND adder_user_id = OLD.adder_user_id;

	UPDATE precedent_ratee_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		precedent_id = OLD.precedent_id
		AND ratee_user_id = OLD.ratee_user_id;

	UPDATE project_competence_set_link_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		project_id = OLD.project_id
		AND competence_set_id = OLD.competence_set_id;

	UPDATE user_competence_set_link_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		user_id = OLD.ratee_user_id
		AND competence_set_id = OLD.competence_set_id;

	UPDATE user_competence_set_project_link
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		user_id = OLD.ratee_user_id
		AND competence_set_id = OLD.competence_set_id
		AND project_id = OLD.project_id;

	UPDATE user_project_link
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		user_id = OLD.ratee_user_id
		AND project_id = OLD.project_id;

END;