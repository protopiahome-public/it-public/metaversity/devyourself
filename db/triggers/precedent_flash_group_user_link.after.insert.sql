BEGIN
	INSERT INTO mark_calc
	(
		project_id,
		precedent_group_id,
		precedent_flash_group_id,
		precedent_flash_group_user_link_id,
		ratee_user_id,
		rater_user_id,
		adder_user_id,
		competence_id,
		competence_set_id,
		type,
		value,
		comment,
		precedent_id
	)
	SELECT
		project_id,
		precedent_group_id,
		precedent_flash_group_id,
		NEW.id AS precedent_flash_group_user_link_id,
		NEW.user_id AS ratee_user_id,
		rater_user_id,
		adder_user_id,
		competence_id,
		competence_set_id,
		type,
		value,
		comment,
		precedent_id
	FROM
		mark_calc
	WHERE
		precedent_flash_group_id = NEW.precedent_flash_group_id
		AND type='group'
	GROUP BY
		competence_id,
		rater_user_id;
END;