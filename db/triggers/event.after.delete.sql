BEGIN
	UPDATE project
	SET 
		comment_count_calc = comment_count_calc - OLD.comment_count_calc,
		event_count_calc = event_count_calc - 1
	WHERE id = OLD.project_id;
END