BEGIN
	DELETE FROM mark_calc
	WHERE
		precedent_flash_group_id = OLD.precedent_flash_group_id
		AND rater_user_id = OLD.rater_user_id
		AND competence_id = OLD.competence_id
		AND type = 'group';
END;