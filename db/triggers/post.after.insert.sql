BEGIN
	SET @project_id = (SELECT project_id FROM community WHERE id = NEW.community_id);
	INSERT INTO post_calc
	SET
		id = NEW.id,
		author_user_id = NEW.author_user_id,
		project_id = @project_id,
		community_id = NEW.community_id,
		section_id = NEW.section_id,
		hide_in_project_feed = NEW.hide_in_project_feed,
		comment_count_calc = NEW.comment_count_calc,
		last_comment_id = NEW.last_comment_id,
		last_comment_author_user_id = NEW.last_comment_author_user_id,
		last_comment_html = NEW.last_comment_html;

	CALL post_add_count_calc(NEW.id, NEW.section_id, NEW.community_id, @project_id, NEW.author_user_id, NEW.comment_count_calc);
END