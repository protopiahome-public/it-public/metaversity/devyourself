BEGIN
	DECLARE my_precedent_id INT;
	DECLARE my_project_id INT;
	DECLARE my_competence_set_id INT;

	SET my_precedent_id = 
	(
		SELECT precedent_id
		FROM precedent_flash_group
		WHERE id = NEW.precedent_flash_group_id
	);

	SET my_project_id =
	(
		SELECT project_id
		FROM precedent
		WHERE id = my_precedent_id
	);

	SET my_competence_set_id =
	(
		SELECT competence_set_id
		FROM competence_full
		WHERE id = NEW.competence_id
	);

	-- @ar test for mark_calc - проверить, что все (!) поля обновляются
	-- проверить все сценарии p/g ins/up

	INSERT INTO mark_calc
	(
		project_id,
		precedent_flash_group_id,
		precedent_flash_group_user_link_id,
		ratee_user_id,
		rater_user_id,
		adder_user_id,
		competence_id,
		competence_set_id,
		type,
		value,
		comment,
		precedent_id
	)
	SELECT
		my_project_id,
		NEW.precedent_flash_group_id,
		pfgul.id,
		pfgul.user_id,
		NEW.rater_user_id,
		NEW.adder_user_id,
		NEW.competence_id,
		my_competence_set_id,
		'group',
		NEW.value,
		NEW.comment,
		my_precedent_id
	FROM
		precedent_flash_group_user_link AS pfgul
	WHERE
		pfgul.precedent_flash_group_id = NEW.precedent_flash_group_id;
END;