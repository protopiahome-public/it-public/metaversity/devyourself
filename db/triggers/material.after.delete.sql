BEGIN
	UPDATE project
	SET 
		comment_count_calc = comment_count_calc - OLD.comment_count_calc,
		material_count_calc = material_count_calc - 1
	WHERE id = OLD.project_id;
END