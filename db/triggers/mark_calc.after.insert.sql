-- tables with *_mark_calc fields
--	competence_set
--	competence_full
--	competence_calc
--	precedent
--	project
--	user
--	precedent_rater_link
--	precedent_mark_adder
--	precedent_ratee_calc
--	project_competence_set_link_calc
--	user_competence_set_link_calc
--	user_competence_set_project_link
--	user_project_link

BEGIN
	DECLARE personal_mark_inc INT DEFAULT 0;
	DECLARE group_mark_inc INT DEFAULT 0;
	DECLARE group_mark_raw_inc INT DEFAULT 0;
	
	IF NEW.type = 'personal'
	THEN
		SET personal_mark_inc = 1;
	ELSE
		SET group_mark_inc = 1;
		IF (
			SELECT COUNT(*)
			FROM mark_calc
			WHERE
				precedent_flash_group_id = NEW.precedent_flash_group_id
				AND rater_user_id = NEW.rater_user_id
				AND competence_id = NEW.competence_id
				AND type = 'group'
			) = 1
		THEN
			SET group_mark_raw_inc = 1;
		END IF;
	END IF;

	UPDATE competence_set
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.competence_set_id;

	UPDATE competence_full
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.competence_id;

	UPDATE competence_calc
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		competence_id = NEW.competence_id;

	UPDATE precedent
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.precedent_id;

	UPDATE project
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.project_id;

	UPDATE user
	SET
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc
	WHERE
		id = NEW.ratee_user_id;

	INSERT INTO precedent_rater_link
	SET
		precedent_id = NEW.precedent_id,
		rater_user_id = NEW.rater_user_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		-- @ar test for this
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO precedent_mark_adder
	SET
		precedent_id = NEW.precedent_id,
		adder_user_id = NEW.adder_user_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO precedent_ratee_calc
	SET
		precedent_id = NEW.precedent_id,
		ratee_user_id = NEW.ratee_user_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO project_competence_set_link_calc
	SET
		project_id = NEW.project_id,
		competence_set_id = NEW.competence_set_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_raw_count_calc = group_mark_raw_count_calc + group_mark_raw_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO user_competence_set_link_calc
	SET
		user_id = NEW.ratee_user_id,
		competence_set_id = NEW.competence_set_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

	INSERT INTO user_competence_set_project_link
	SET
		user_id = NEW.ratee_user_id,
		competence_set_id = NEW.competence_set_id,
		project_id = NEW.project_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;
	
	INSERT INTO user_project_link
	SET
		user_id = NEW.ratee_user_id,
		project_id = NEW.project_id,
		personal_mark_count_calc = personal_mark_inc,
		group_mark_count_calc = group_mark_inc,
		total_mark_count_calc = personal_mark_inc + group_mark_inc
	ON DUPLICATE KEY UPDATE
		personal_mark_count_calc = personal_mark_count_calc + personal_mark_inc,
		group_mark_count_calc = group_mark_count_calc + group_mark_inc,
		total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

END;