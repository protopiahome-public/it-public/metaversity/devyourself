CREATE FUNCTION  `insert_rate`(
	rate_type_id1 int unsigned,
	title1 varchar(255) CHARACTER SET utf8,
	competence_set_id1 int unsigned,
	add_time1 datetime,
	edit_time1 datetime
) RETURNS INT(10) unsigned
BEGIN
	INSERT INTO rate
		(rate_type_id, title, competence_set_id, add_time, edit_time) 
	VALUES
		(rate_type_id1, title1, competence_set_id1, add_time1, edit_time1);

	RETURN LAST_INSERT_ID();
END;