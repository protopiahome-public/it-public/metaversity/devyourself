ALTER TABLE `comment` DROP INDEX `FK_comment_author_id`, DROP FOREIGN KEY `FK_comment_author_id`;
ALTER TABLE `comment` CHANGE COLUMN `author_id` `author_user_id` INTEGER UNSIGNED NOT NULL DEFAULT 0;
ALTER TABLE `comment` ADD CONSTRAINT `FK_comment_author_user_id` FOREIGN KEY `FK_comment_author_user_id` (`author_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;