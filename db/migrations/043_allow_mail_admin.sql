ALTER TABLE `user` ADD COLUMN `allow_email_on_premoderation` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `allow_email_on_reply`;
