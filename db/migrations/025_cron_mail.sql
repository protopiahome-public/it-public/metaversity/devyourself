CREATE TABLE  `email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `to_email` varchar(255) NOT NULL,
  `to_user_id` int(10) unsigned DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `html` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE  `email_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `to_email` varchar(255) NOT NULL,
  `to_user_id` int(10) unsigned DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `html` mediumtext,
  `sent_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `success` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `email` ENGINE = MyISAM
ROW_FORMAT = DYNAMIC;

ALTER TABLE `email_log` ENGINE = MyISAM
ROW_FORMAT = DYNAMIC;
