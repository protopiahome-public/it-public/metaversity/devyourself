ALTER TABLE `community_widget`
 DROP FOREIGN KEY `FK_community_widget_type_id`;
ALTER TABLE `community_widget`
 ADD CONSTRAINT `FK_community_widget_type_id` FOREIGN KEY `FK_community_widget_type_id` (`type_id`)
    REFERENCES `community_widget_type` (`type_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;
UPDATE community_widget_type SET type_id = 'child_communities', ctrl = 'child_communities' WHERE type_id = 'subcommunities';
ALTER TABLE `community` CHANGE COLUMN `subcommunity_add` `allow_child_communities` ENUM('nobody','user','user_premoderation') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'nobody';