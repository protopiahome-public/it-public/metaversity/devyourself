DROP TABLE IF EXISTS `community`;
CREATE TABLE  `community` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `descr_short` varchar(140) NOT NULL DEFAULT '',
  `logo_big_width` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_big_height` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_small_width` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_small_height` int(10) unsigned NOT NULL DEFAULT '0',
  `project_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `allow_posting_without_section` tinyint(1) NOT NULL DEFAULT '1',
  `member_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_community_project_id` (`project_id`),
  CONSTRAINT `FK_community_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `section`;
CREATE TABLE  `section` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `community_id` int(10) unsigned NOT NULL,
  `descr` mediumtext,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_section_community_id` (`community_id`),
  CONSTRAINT `FK_section_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `post`;
CREATE TABLE  `post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `author_user_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `block_set_id` int(10) unsigned DEFAULT NULL,
  `section_id` int(10) unsigned DEFAULT NULL,
  `show_on_main` tinyint(1) NOT NULL DEFAULT '1',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_post_user_id` (`author_user_id`),
  KEY `FK_post_block_set_id` (`block_set_id`),
  KEY `FK_post_section_id` (`section_id`),
  CONSTRAINT `FK_post_section_id` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_post_block_set_id` FOREIGN KEY (`block_set_id`) REFERENCES `block_set` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_post_user_id` FOREIGN KEY (`author_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `community_user_link`;
CREATE TABLE  `community_user_link` (
  `community_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('member','admin') NOT NULL DEFAULT 'member',
  `post_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`community_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `project` MODIFY COLUMN `show_user_numbers` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
 ADD COLUMN `communities_are_on` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `show_user_numbers`,
 ADD COLUMN `community_count_calc` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `communities_are_on`,
 ADD COLUMN `post_count_calc` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `community_count_calc`,
 ADD COLUMN `comment_count_calc` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `post_count_calc`;

ALTER TABLE `user_project_link` ADD COLUMN `post_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `number`,
 ADD COLUMN `comment_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `post_count_calc`;
