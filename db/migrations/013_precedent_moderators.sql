ALTER TABLE  `project_admin`
	ADD  `is_precedent_moderator` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `is_material_moderator` ,
	ADD  `is_mark_moderator` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `is_precedent_moderator` ;
