ALTER TABLE `community` 
 ADD COLUMN `parent_id` INTEGER UNSIGNED AFTER `bg_body_main_url`,
 ADD COLUMN `parent_approved` BOOLEAN NOT NULL DEFAULT 0 AFTER `parent_id`,
 ADD CONSTRAINT `FK_community_parent_id` FOREIGN KEY `FK_community_parent_id` (`parent_id`)
    REFERENCES `community` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;
	
ALTER TABLE `community` 
 ADD COLUMN `subcommunity_add` ENUM('nobody', 'user', 'user_premoderation') NOT NULL DEFAULT 'nobody' AFTER `parent_approved`;