ALTER TABLE `post` CHANGE COLUMN `show_on_main` `is_private` TINYINT(1) NOT NULL DEFAULT 0;
ALTER TABLE `post_calc` CHANGE COLUMN `show_on_main` `is_private` TINYINT(1) NOT NULL DEFAULT 0;
ALTER TABLE `post_calc` DROP INDEX `index_project`,
 ADD INDEX `index_project` USING BTREE(`project_id`, `is_private`, `id`);
