ALTER TABLE `community`
 ADD COLUMN `access_read` ENUM('guest','member','moderator','admin') NOT NULL DEFAULT 'guest' AFTER `subcommunity_add`,
 ADD COLUMN `access_post_add` ENUM('user','member','moderator','admin') NOT NULL DEFAULT 'user' AFTER `access_read`,
 ADD COLUMN `access_comment` ENUM('user','member','moderator','admin') NOT NULL DEFAULT 'user' AFTER `access_post_add`;

ALTER TABLE `community` MODIFY COLUMN `access_read` ENUM('guest','project_member','member','moderator','admin') NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_post_add` ENUM('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user',
 MODIFY COLUMN `access_comment` ENUM('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user';

ALTER TABLE `community` MODIFY COLUMN `access_read` ENUM('guest','user','project_member','member','moderator','admin') NOT NULL DEFAULT 'guest';

ALTER TABLE `project` MODIFY COLUMN `access_read` ENUM('guest','user','member','moderator','admin') NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_communities_read` ENUM('guest','user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_events_read` ENUM('guest','user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_materials_read` ENUM('guest','user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_precedents_read` ENUM('guest','user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest';

ALTER TABLE `section`
 ADD COLUMN `access_post_add` ENUM('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user' AFTER `post_count_calc`,
 ADD COLUMN `access_comment` ENUM('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user' AFTER `access_post_add`;

ALTER TABLE `section` ADD COLUMN `access_default` BOOLEAN NOT NULL DEFAULT 1 AFTER `access_comment`;
