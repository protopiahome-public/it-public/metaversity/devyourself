ALTER TABLE `comment`
 DROP FOREIGN KEY `FK_comment_author_user_id`;

ALTER TABLE `comment`
 DROP FOREIGN KEY `FK_comment_parent_id`;

ALTER TABLE `comment`
 DROP FOREIGN KEY `FK_comment_type_id`;

ALTER TABLE `comment` ADD CONSTRAINT `FK_comment_author_user_id` FOREIGN KEY `FK_comment_author_user_id` (`author_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_comment_parent_id` FOREIGN KEY `FK_comment_parent_id` (`parent_id`)
    REFERENCES `comment` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_comment_type_id` FOREIGN KEY `FK_comment_type_id` (`type_id`)
    REFERENCES `comment_type` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
