ALTER TABLE `post` DROP FOREIGN KEY `FK_post_last_comment_author_user_id`,
 DROP FOREIGN KEY `FK_post_last_comment_id`;

ALTER TABLE `post` DROP INDEX `FK_post_last_comment_author_user_id`
, DROP INDEX `FK_post_last_comment_id`;

ALTER TABLE `post` ADD CONSTRAINT `FK_post_last_comment_id` FOREIGN KEY `FK_post_last_comment_id` (`last_comment_id`)
    REFERENCES `comment` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

ALTER TABLE `post` ADD CONSTRAINT `FK_post_last_comment_author_user_id` FOREIGN KEY `FK_post_last_comment_author_user_id` (`last_comment_author_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;
