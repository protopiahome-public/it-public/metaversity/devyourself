ALTER TABLE `community` ADD COLUMN `old_id` INTEGER UNSIGNED NULL AFTER `child_community_count_calc`;
ALTER TABLE `community` ADD COLUMN `old_parent_id` INTEGER UNSIGNED NULL AFTER `old_id`;

ALTER TABLE `community` DROP INDEX `unique_name`,
 ADD UNIQUE INDEX `unique_name` USING BTREE(`name`, `project_id`, `is_deleted`);
 
ALTER TABLE `section` ADD COLUMN `old_id` INTEGER UNSIGNED NULL AFTER `access_default`;

CREATE TABLE  `communities_menu_dd_item` (
  `community_id` int(10) unsigned NULL DEFAULT '0',
  `project_id` int(10) unsigned NULL DEFAULT '0',
  `position` int(10) unsigned NULL DEFAULT '0',
  `add_time` datetime NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NULL DEFAULT '0000-00-00 00:00:00',
  `id` int(10) unsigned NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `FK_communities_menu_dd_project_id` (`project_id`),
  KEY `unique` (`community_id`,`project_id`),
  CONSTRAINT `FK_communities_menu_dd_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_communities_menu_dd_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `post` ADD COLUMN `old_id` INTEGER UNSIGNED NULL AFTER `last_comment_author_user_id`;

ALTER TABLE `comment` ADD COLUMN `old_id` INTEGER UNSIGNED NULL AFTER `edit_time`;
ALTER TABLE `comment` ADD COLUMN `old_parent_id` INTEGER UNSIGNED NULL AFTER `old_id`;

ALTER TABLE `community_custom_feed` ADD COLUMN `old_id` INTEGER UNSIGNED NULL AFTER `edit_time`;

ALTER TABLE `project_widget` ADD COLUMN `old_id` INTEGER UNSIGNED NULL AFTER `lister_item_count`;
ALTER TABLE `community_widget` ADD COLUMN `old_id` INTEGER UNSIGNED NULL AFTER `lister_item_count`;