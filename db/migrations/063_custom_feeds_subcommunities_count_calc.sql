ALTER TABLE `community`
 ADD COLUMN `custom_feed_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0,
 ADD COLUMN `child_community_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE `project`
 ADD COLUMN `custom_feed_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0;
