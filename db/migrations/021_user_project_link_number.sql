ALTER TABLE `user_project_link` ADD COLUMN `number` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `edit_time`;
UPDATE user_project_link SET number = user_id;
ALTER TABLE `project` ADD COLUMN `show_user_numbers` BOOLEAN NOT NULL DEFAULT 0 AFTER `endpoint_last_connect_result`;