CREATE TABLE  `comment_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `comment_type` VALUES (1,'event'),(2,'material');

CREATE TABLE  `comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned DEFAULT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `html` text NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_comment_parent_id` (`parent_id`),
  KEY `FK_comment_author_id` (`author_id`),
  KEY `FK_comment_type_id` (`type_id`),
  CONSTRAINT `FK_comment_type_id` FOREIGN KEY (`type_id`) REFERENCES `comment_type` (`id`),
  CONSTRAINT `FK_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `comment` (`id`),
  CONSTRAINT `FK_comment_author_id` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `event` ADD COLUMN `comment_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0;
ALTER TABLE `material` ADD COLUMN `comment_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0;