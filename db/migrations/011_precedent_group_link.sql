CREATE TABLE precedent_group_link (
	precedent_group_id	int unsigned NOT NULL,
	precedent_id		int unsigned NOT NULL,
	PRIMARY KEY (precedent_group_id,precedent_id),
	FOREIGN KEY (precedent_group_id) REFERENCES precedent_group (id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (precedent_id) REFERENCES precedent (id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO precedent_group_link (precedent_id, precedent_group_id)
	SELECT id, precedent_group_id FROM precedent;