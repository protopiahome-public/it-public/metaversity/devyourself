ALTER TABLE `comment`
 DROP FOREIGN KEY `FK_comment_author_user_id`;

ALTER TABLE `comment`
 DROP FOREIGN KEY `FK_comment_type_id`;

ALTER TABLE `comment` ADD CONSTRAINT `FK_comment_author_user_id` FOREIGN KEY `FK_comment_author_user_id` (`author_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_comment_type_id` FOREIGN KEY `FK_comment_type_id` (`type_id`)
    REFERENCES `comment_type` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `community_widget_default`
 DROP FOREIGN KEY `FK_community_widget_default_type_id`;

ALTER TABLE `community_widget_default` ADD CONSTRAINT `FK_community_widget_default_type_id` FOREIGN KEY `FK_community_widget_default_type_id` (`type_id`)
    REFERENCES `community_widget_type` (`type_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `post`
 DROP FOREIGN KEY `FK_post_last_comment_author_user_id`;

ALTER TABLE `post`
 DROP FOREIGN KEY `FK_post_last_comment_id`;

ALTER TABLE `post` ADD CONSTRAINT `FK_post_last_comment_author_user_id` FOREIGN KEY `FK_post_last_comment_author_user_id` (`last_comment_author_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_post_last_comment_id` FOREIGN KEY `FK_post_last_comment_id` (`last_comment_id`)
    REFERENCES `comment` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

ALTER TABLE `precedent_comment`
 DROP FOREIGN KEY `FK_precedent_comment_commenter_user_id`;

ALTER TABLE `precedent_comment` ADD CONSTRAINT `FK_precedent_comment_commenter_user_id` FOREIGN KEY `FK_precedent_comment_commenter_user_id` (`commenter_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `project_widget`
 DROP FOREIGN KEY `FK_project_widget_project_id`;

ALTER TABLE `project_widget`
 DROP FOREIGN KEY `FK_project_widget_type_id`;

ALTER TABLE `project_widget` ADD CONSTRAINT `FK_project_widget_project_id` FOREIGN KEY `FK_project_widget_project_id` (`project_id`)
    REFERENCES `project` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_project_widget_type_id` FOREIGN KEY `FK_project_widget_type_id` (`type_id`)
    REFERENCES `project_widget_type` (`type_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `project_widget_default`
 DROP FOREIGN KEY `FK_project_widget_default_type_id`;

ALTER TABLE `project_widget_default` ADD CONSTRAINT `FK_project_widget_default_type_id` FOREIGN KEY `FK_project_widget_default_type_id` (`type_id`)
    REFERENCES `project_widget_type` (`type_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
