CREATE TABLE  `community_admin` (
  `user_id` int(10) unsigned NOT NULL,
  `community_id` int(10) unsigned NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adder_admin_user_id` int(10) unsigned DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_admin_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`community_id`),
  KEY `FK_community_admin_community_id` (`community_id`),
  KEY `FK_community_admin_adder_admin_user_id` (`adder_admin_user_id`),
  KEY `FK_community_admin_editor_admin_user_id` (`editor_admin_user_id`),
  CONSTRAINT `FK_community_admin_adder_admin_user_id` FOREIGN KEY (`adder_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_community_admin_editor_admin_user_id` FOREIGN KEY (`editor_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_community_admin_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_admin_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;