ALTER TABLE  `project`
  ADD COLUMN `favicon_main_width` int(10) unsigned NOT NULL,
  ADD COLUMN `favicon_main_height` int(10) unsigned NOT NULL,
  ADD COLUMN `favicon_main_url` varchar(255) NOT NULL,
  ADD COLUMN `favicon_version` int(10) unsigned NOT NULL
;