ALTER TABLE   `event` 
  ADD `reflex_id` int(10) unsigned DEFAULT NULL,
  ADD `reflex_project_id` int(10) unsigned DEFAULT NULL,
  ADD KEY `FK_event_reflex_id` (`reflex_id`),
  ADD KEY `FK_event_reflex_project_id` (`reflex_project_id`),
  ADD CONSTRAINT `FK_event_reflex_id` FOREIGN KEY (`reflex_project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_event_reflex_project_id` FOREIGN KEY (`reflex_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE   `material` 
  ADD `reflex_id` int(10) unsigned DEFAULT NULL,
  ADD `reflex_project_id` int(10) unsigned DEFAULT NULL,
  ADD KEY `FK_material_reflex_id` (`reflex_id`),
  ADD KEY `FK_material_reflex_project_id` (`reflex_project_id`),
  ADD CONSTRAINT `FK_material_reflex_id` FOREIGN KEY (`reflex_project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_material_reflex_project_id` FOREIGN KEY (`reflex_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE  `project_import` (
  `to_project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `from_project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `events_status` int(10) NOT NULL DEFAULT '0',
  `materials_status` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`to_project_id`,`from_project_id`),
  KEY `FK_project_import_2` (`from_project_id`),
  CONSTRAINT `FK_project_import_from` FOREIGN KEY (`from_project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_import_to` FOREIGN KEY (`to_project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;