CREATE TABLE `project_feed` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `descr` text NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_project_feed_project_id` (`project_id`) USING BTREE,
  CONSTRAINT `FK_project_feed_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `project_feed_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `section_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique_item` (`feed_id`,`community_id`,`section_id`) USING BTREE,
  KEY `FK_project_feed_item_community_id` (`community_id`),
  KEY `FK_project_feed_item_section_id` (`section_id`),
  CONSTRAINT `FK_project_feed_item_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`),
  CONSTRAINT `FK_project_feed_item_section_id` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `project_admin`
 ADD COLUMN `is_feed_moderator` BOOLEAN NOT NULL DEFAULT 0 AFTER `is_widget_moderator`;

INSERT INTO `project_widget_type` (type_id, title, multiple_instances, ctrl, edit_ctrl, lister_item_count)
  VALUES ('feed', 'Лента', 1, 'project_widget_feed', 'feed', 5);
  
CREATE TABLE  `project_widget_feed` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `project_feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`widget_id`),
  CONSTRAINT `FK_project_widget_feed_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `project_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_widget_feed_project_feed_id` FOREIGN KEY (`project_feed_id`) REFERENCES `project_feed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;