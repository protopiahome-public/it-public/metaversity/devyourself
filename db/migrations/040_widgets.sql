ALTER TABLE `project_admin`
 ADD COLUMN `is_widget_moderator` BOOLEAN NOT NULL DEFAULT 0 AFTER `is_community_moderator`;
 
ALTER TABLE `post_calc` ADD COLUMN `comment_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `is_private`;
UPDATE post_calc pc SET comment_count_calc = (SELECT comment_count_calc FROM post WHERE id = pc.id);

ALTER TABLE `post` ADD COLUMN `last_comment_id` INTEGER UNSIGNED NULL DEFAULT NULL AFTER `is_deleted`,
 ADD COLUMN `last_comment_text` TEXT NULL AFTER `last_comment_id`,
 ADD COLUMN `last_comment_author_user_id` INTEGER UNSIGNED NULL DEFAULT NULL AFTER `last_comment_text`;

ALTER TABLE `post`
 ADD CONSTRAINT `FK_post_last_comment_author_user_id` FOREIGN KEY `FK_post_last_comment_author_user_id` (`last_comment_author_user_id`)
    REFERENCES `user` (`id`)
	ON UPDATE RESTRICT
    ON DELETE RESTRICT,
 ADD CONSTRAINT `FK_post_last_comment_id` FOREIGN KEY `FK_post_last_comment_id` (`last_comment_id`)
    REFERENCES `comment` (`id`)
	ON UPDATE RESTRICT
    ON DELETE RESTRICT;
	
ALTER TABLE `post_calc` ADD COLUMN `last_comment_id` INTEGER UNSIGNED NULL DEFAULT NULL,
 ADD COLUMN `last_comment_text` TEXT NULL AFTER `last_comment_id`,
 ADD COLUMN `last_comment_author_user_id` INTEGER UNSIGNED NULL DEFAULT NULL AFTER `last_comment_text`;

ALTER TABLE `project` ADD COLUMN `event_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `comment_count_calc`,
 ADD COLUMN `material_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `event_count_calc`;
 
UPDATE `project` p SET event_count_calc =
	(SELECT count(*) FROM event WHERE project_id = p.id)
;

UPDATE `project` p SET material_count_calc =
	(SELECT count(*) FROM material WHERE project_id = p.id)
;