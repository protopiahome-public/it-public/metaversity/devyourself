ALTER TABLE  `project` ADD  `endpoint_is_on` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `endpoint_url;
ALTER TABLE  `project` ADD  `endpoint_resend` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `endpoint_url;
CREATE TABLE api_project_user_update(
project_id INT UNSIGNED NOT NULL ,
user_id INT UNSIGNED NOT NULL ,
PRIMARY KEY ( project_id, user_id ) ,
FOREIGN KEY (  `project_id` ) REFERENCES  `project` (  `id` ) ON DELETE CASCADE ON UPDATE CASCADE ,
FOREIGN KEY (  `user_id` ) REFERENCES  `user` (  `id` ) ON DELETE CASCADE ON UPDATE CASCADE
);