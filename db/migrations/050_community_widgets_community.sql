CREATE TABLE `community_widget_communities` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  PRIMARY KEY (`widget_id`,`community_id`),
  KEY `FK_community_widget_communities_2` (`community_id`),
  CONSTRAINT `FK_community_widget_communities_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `community_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_widget_communities_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

INSERT INTO `community_widget_type` (`type_id`, `title`, `multiple_instances`, `ctrl`, `edit_ctrl`, `lister_item_count`) VALUES
  ('communities', 'Сообщества', 1, 'community_widget_communities', 'communities', 0);