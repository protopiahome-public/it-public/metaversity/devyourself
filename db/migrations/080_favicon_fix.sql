ALTER TABLE `project` MODIFY COLUMN `favicon_main_width` INT(10) UNSIGNED NOT NULL DEFAULT 0,
 MODIFY COLUMN `favicon_main_height` INT(10) UNSIGNED NOT NULL DEFAULT 0,
 MODIFY COLUMN `favicon_main_url` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
 MODIFY COLUMN `favicon_version` INT(10) UNSIGNED NOT NULL DEFAULT 0;
