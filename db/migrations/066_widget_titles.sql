DELETE FROM `project_widget_default`;
INSERT INTO `project_widget_default` VALUES (1,2,'Статистика','stat',0),(2,1,'Последние записи','last_posts',5),(2,2,'Последние комментарии','last_comments',5),(3,1,'Новые лица','last_users',10),(3,2,'Активные участники','top_users',10);

DELETE FROM `community_widget_default`;
INSERT INTO `community_widget_default` VALUES (1,2,'Статистика','stat',0),(2,1,'Последние записи','last_posts',5),(2,2,'Последние комментарии','last_comments',5),(3,1,'Новые лица','last_users',10),(3,2,'Активные участники','top_users',10);

UPDATE project_widget_type SET title = 'Новые лица' WHERE type_id = 'last_users';
UPDATE project_widget_type SET title = 'Ближайшие события' WHERE type_id = 'events';
UPDATE community_widget_type SET title = 'Новые лица' WHERE type_id = 'last_users';
UPDATE community_widget_type SET title = 'Связанные сообщества' WHERE type_id = 'communities';