ALTER TABLE `project` ADD COLUMN `color_scheme` ENUM('green','blue','yellow','red') NOT NULL DEFAULT 'green' AFTER `favicon_version`;

ALTER TABLE `community` ADD COLUMN `color_scheme` ENUM('project','green','blue','yellow','red') NOT NULL DEFAULT 'project' AFTER `old_parent_id`;
