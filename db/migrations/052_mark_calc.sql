ALTER TABLE  `mark_calc`
	ADD  `adder_user_id` INT UNSIGNED NOT NULL AFTER  `rater_user_id`;
UPDATE mark_calc
	SET adder_user_id = rater_user_id;
ALTER TABLE  `mark_calc`
	ADD CONSTRAINT `FK_mark_calc_adder_user_id`
	FOREIGN KEY (`adder_user_id`)
	REFERENCES `user` (`id`)
	ON DELETE CASCADE ON UPDATE CASCADE;