RENAME TABLE project_feed TO project_custom_feed;
RENAME TABLE project_feed_item TO project_custom_feed_item;

UPDATE project_widget_type SET ctrl='custom_feed', edit_ctrl='custom_feed', type_id='custom_feed' WHERE type_id='feed';

DROP TABLE `project_widget_feed`;
CREATE TABLE  `project_widget_custom_feed` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `project_custom_feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`widget_id`),
  KEY `FK_project_widget_custom_feed_project_custom_feed_id` (`project_custom_feed_id`) USING BTREE,
  CONSTRAINT `FK_project_widget_custom_feed_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `project_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_widget_custom_feed_project_feed_id` FOREIGN KEY (`project_custom_feed_id`) REFERENCES `project_custom_feed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `project_custom_feed` DROP INDEX `FK_project_feed_project_id`,
 ADD INDEX `FK_project_custom_feed_project_id` USING BTREE(`project_id`),
 DROP FOREIGN KEY `FK_project_feed_project_id`,
 ADD CONSTRAINT `FK_project_custom_feed_project_id` FOREIGN KEY `FK_project_custom_feed_project_id` (`project_id`)
    REFERENCES `project` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;
	
ALTER TABLE `project_custom_feed_item` DROP INDEX `FK_project_feed_item_community_id`
, DROP INDEX `FK_project_feed_item_section_id`,
 ADD INDEX `FK_project_custom_feed_item_community_id` USING BTREE(`community_id`),
 ADD INDEX `FK_project_custom_feed_item_section_id` USING BTREE(`section_id`),
 DROP FOREIGN KEY `FK_project_feed_item_community_id`,
 DROP FOREIGN KEY `FK_project_feed_item_section_id`,
 ADD CONSTRAINT `FK_project_custom_feed_item_community_id` FOREIGN KEY `FK_project_custom_feed_item_community_id` (`community_id`)
    REFERENCES `community` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
 ADD CONSTRAINT `FK_project_custom_feed_item_section_id` FOREIGN KEY `FK_project_custom_feed_item_section_id` (`section_id`)
    REFERENCES `section` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `project_custom_feed_item` CHANGE COLUMN `feed_id` `custom_feed_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
 DROP INDEX `Unique_item`,
 ADD UNIQUE `Unique_item` USING BTREE(`custom_feed_id`, `community_id`, `section_id`);

ALTER TABLE `project_custom_feed_item` ADD CONSTRAINT `FK_project_custom_feed_item_custom_feed_id` FOREIGN KEY `FK_project_custom_feed_item_custom_feed_id` (`custom_feed_id`)
    REFERENCES `project_custom_feed` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
