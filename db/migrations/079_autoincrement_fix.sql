ALTER TABLE `precedent_rater_link` MODIFY COLUMN `precedent_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `project_competence_set_link_calc` MODIFY COLUMN `project_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `rate_competence_link` MODIFY COLUMN `rate_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `user_competence_set_link_calc` MODIFY COLUMN `user_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `user_competence_set_project_link` MODIFY COLUMN `user_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `user_user_group_link` MODIFY COLUMN `user_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `user_project_link` MODIFY COLUMN `project_id` INT(10) UNSIGNED NOT NULL;
