ALTER TABLE `event_user_link` ADD COLUMN `last_status_time` DATETIME NOT NULL DEFAULT 0 AFTER `after_status_time`;
ALTER TABLE `material_user_link` ADD COLUMN `last_status_time` DATETIME NOT NULL DEFAULT 0 AFTER `after_status_time`;
UPDATE `event_user_link` SET `last_status_time` = GREATEST(before_status_time, after_status_time);
UPDATE `material_user_link` SET `last_status_time` = GREATEST(before_status_time, after_status_time);