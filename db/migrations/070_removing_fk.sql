ALTER TABLE `community` DROP FOREIGN KEY `FK_community_parent_id` ;
ALTER TABLE `community` 
  ADD CONSTRAINT `FK_community_parent_id`
  FOREIGN KEY (`parent_id` )
  REFERENCES `community` (`id` )
  ON DELETE SET NULL
  ON UPDATE CASCADE;
