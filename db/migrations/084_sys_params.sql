CREATE TABLE  `sys_params` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(60) NOT NULL DEFAULT '',
  `info_email` varchar(100) NOT NULL,
  `main_page_text` mediumtext,
  `copyright_start_year` char(4) NOT NULL DEFAULT '1900',
  `is_devyourself` tinyint(1) NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ga_tracking_id` varchar(40) NOT NULL DEFAULT '',
  `logo_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_version` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO sys_params (id, title, info_email, main_page_text, copyright_start_year, is_devyourself)
VALUES (1, 'DevYourself', 'info@devyourself.ru', '<p>Text</p>', '2007', 1);