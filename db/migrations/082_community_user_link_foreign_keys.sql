ALTER TABLE `community_user_link` ADD CONSTRAINT `FK_community_user_link_user_id` FOREIGN KEY `FK_community_user_link_user_id` (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_community_user_link_community_id` FOREIGN KEY `FK_community_user_link_community_id` (`community_id`)
    REFERENCES `community` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
