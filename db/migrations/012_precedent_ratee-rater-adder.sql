ALTER TABLE precedent_rater_link
	ADD personal_mark_count_calc int(10) unsigned NOT NULL DEFAULT '0',
	ADD group_mark_count_calc int(10) unsigned NOT NULL DEFAULT '0',
	ADD group_mark_raw_count_calc int(10) unsigned NOT NULL DEFAULT '0',
	ADD total_mark_count_calc int(10) unsigned NOT NULL DEFAULT '0';
	
CREATE TABLE precedent_ratee_calc (
	precedent_id int(10) unsigned NOT NULL AUTO_INCREMENT,
	ratee_user_id int(10) unsigned NOT NULL,
	personal_mark_count_calc int(10) unsigned NOT NULL DEFAULT '0',
	group_mark_count_calc int(10) unsigned NOT NULL DEFAULT '0',
	total_mark_count_calc int(10) unsigned NOT NULL DEFAULT '0',
	PRIMARY KEY (`precedent_id`,`ratee_user_id`),
	FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`ratee_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE precedent_mark_adder (
	precedent_id int(10) unsigned NOT NULL AUTO_INCREMENT,
	adder_user_id int(10) unsigned NOT NULL,
	personal_mark_count_calc int(10) unsigned NOT NULL DEFAULT '0',
	group_mark_count_calc int(10) unsigned NOT NULL DEFAULT '0',
	group_mark_raw_count_calc int(10) unsigned NOT NULL DEFAULT '0',
	total_mark_count_calc int(10) unsigned NOT NULL DEFAULT '0',
	PRIMARY KEY (`precedent_id`,`adder_user_id`),
	FOREIGN KEY (`precedent_id`) REFERENCES `precedent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- precedent_rater_link : group_mark_raw_count_calc
INSERT INTO precedent_rater_link (precedent_id, rater_user_id, group_mark_raw_count_calc)
	SELECT
		p.id AS precedent_id,
		mg.rater_user_id AS rater_user_id,
		count(*) AS group_mark_raw_count_calc
	FROM
		precedent AS p,
		precedent_flash_group AS pfg,
		mark_group AS mg
	WHERE
		pfg.precedent_id = p.id
		AND mg.precedent_flash_group_id = pfg.id
	GROUP BY
		p.id, mg.rater_user_id
ON DUPLICATE KEY UPDATE
	group_mark_raw_count_calc = VALUES(group_mark_raw_count_calc);

-- precedent_rater_link : personal_mark_count_calc
INSERT INTO precedent_rater_link (precedent_id, rater_user_id, personal_mark_count_calc)
	SELECT
		p.id AS precedent_id,
		mp.rater_user_id AS rater_user_id,
		count(*) AS personal_mark_count_calc
	FROM
		precedent AS p,
		precedent_flash_group AS pfg,
		precedent_flash_group_user_link AS pfgul,
		mark_personal AS mp
	WHERE
		pfg.precedent_id = p.id
		AND pfgul.precedent_flash_group_id = pfg.id
		AND mp.precedent_flash_group_user_link_id = pfgul.id
	GROUP BY
		p.id, mp.rater_user_id
ON DUPLICATE KEY UPDATE
	personal_mark_count_calc = VALUES(personal_mark_count_calc);

-- precedent_rater_link : group_mark_count_calc
INSERT INTO precedent_rater_link (precedent_id, rater_user_id, group_mark_count_calc)
	SELECT
		p.id AS precedent_id,
		mg.rater_user_id AS rater_user_id,
		count(*) AS personal_mark_count_calc
	FROM
		precedent AS p,
		precedent_flash_group AS pfg,
		precedent_flash_group_user_link AS pfgul,
		mark_group AS mg
	WHERE
		pfg.precedent_id = p.id
		AND pfgul.precedent_flash_group_id = pfg.id
		AND mg.precedent_flash_group_id = pfg.id
	GROUP BY
		p.id, mg.rater_user_id
ON DUPLICATE KEY UPDATE
	group_mark_count_calc = VALUES(group_mark_count_calc);

-- precedent_rater_link : total_mark_count_calc
UPDATE
	precedent_rater_link
SET
	total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;
	
-- -----------------------------------------------------------------------

-- precedent_mark_adder : group_mark_raw_count_calc
INSERT INTO precedent_mark_adder (precedent_id, adder_user_id, group_mark_raw_count_calc)
	SELECT
		p.id AS precedent_id,
		mg.adder_user_id AS adder_user_id,
		count(*) AS group_mark_raw_count_calc
	FROM
		precedent AS p,
		precedent_flash_group AS pfg,
		mark_group AS mg
	WHERE
		pfg.precedent_id = p.id
		AND mg.precedent_flash_group_id = pfg.id
		AND mg.rater_user_id != mg.adder_user_id
	GROUP BY
		p.id, mg.adder_user_id
ON DUPLICATE KEY UPDATE
	group_mark_raw_count_calc = VALUES(group_mark_raw_count_calc);

-- precedent_mark_adder : personal_mark_count_calc
INSERT INTO precedent_mark_adder (precedent_id, adder_user_id, personal_mark_count_calc)
	SELECT
		p.id AS precedent_id,
		mp.adder_user_id AS adder_user_id,
		count(*) AS personal_mark_count_calc
	FROM
		precedent AS p,
		precedent_flash_group AS pfg,
		precedent_flash_group_user_link AS pfgul,
		mark_personal AS mp
	WHERE
		pfg.precedent_id = p.id
		AND pfgul.precedent_flash_group_id = pfg.id
		AND mp.precedent_flash_group_user_link_id = pfgul.id
		AND mp.rater_user_id != mp.adder_user_id
	GROUP BY
		p.id, mp.adder_user_id
ON DUPLICATE KEY UPDATE
	personal_mark_count_calc = VALUES(personal_mark_count_calc);

-- precedent_mark_adder : group_mark_count_calc
INSERT INTO precedent_mark_adder (precedent_id, adder_user_id, group_mark_count_calc)
	SELECT
		p.id AS precedent_id,
		mg.adder_user_id AS adder_user_id,
		count(*) AS personal_mark_count_calc
	FROM
		precedent AS p,
		precedent_flash_group AS pfg,
		precedent_flash_group_user_link AS pfgul,
		mark_group AS mg
	WHERE
		pfg.precedent_id = p.id
		AND pfgul.precedent_flash_group_id = pfg.id
		AND mg.precedent_flash_group_id = pfg.id
		AND mg.rater_user_id != mg.adder_user_id
	GROUP BY
		p.id, mg.adder_user_id
ON DUPLICATE KEY UPDATE
	group_mark_count_calc = VALUES(group_mark_count_calc);

-- precedent_mark_adder : total_mark_count_calc
UPDATE
	precedent_mark_adder
SET
	total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;

-- -----------------------------------------------------------------------
-- precedent_ratee_calc : personal_mark_count_calc
INSERT INTO precedent_ratee_calc (precedent_id, ratee_user_id, personal_mark_count_calc)
	SELECT
		p.id AS precedent_id,
		pfgul.user_id AS ratee_user_id,
		count(*) AS personal_mark_count_calc
	FROM
		precedent AS p,
		precedent_flash_group AS pfg,
		precedent_flash_group_user_link AS pfgul,
		mark_personal AS mp
	WHERE
		pfg.precedent_id = p.id
		AND pfgul.precedent_flash_group_id = pfg.id
		AND mp.precedent_flash_group_user_link_id = pfgul.id
	GROUP BY
		p.id, pfgul.user_id
ON DUPLICATE KEY UPDATE
	personal_mark_count_calc = VALUES(personal_mark_count_calc);

-- precedent_ratee_calc : group_mark_count_calc
INSERT INTO precedent_ratee_calc (precedent_id, ratee_user_id, group_mark_count_calc)
	SELECT
		p.id AS precedent_id,
		pfgul.user_id AS ratee_user_id,
		count(*) AS personal_mark_count_calc
	FROM
		precedent AS p,
		precedent_flash_group AS pfg,
		precedent_flash_group_user_link AS pfgul,
		mark_group AS mg
	WHERE
		pfg.precedent_id = p.id
		AND pfgul.precedent_flash_group_id = pfg.id
		AND mg.precedent_flash_group_id = pfg.id
	GROUP BY
		p.id, pfgul.user_id
ON DUPLICATE KEY UPDATE
	group_mark_count_calc = VALUES(group_mark_count_calc);

-- precedent_ratee_calc : total_mark_count_calc
UPDATE
	precedent_ratee_calc
SET
	total_mark_count_calc = personal_mark_count_calc + group_mark_count_calc;
