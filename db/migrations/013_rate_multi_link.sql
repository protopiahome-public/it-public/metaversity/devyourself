CREATE TABLE `event_rate_link` (
`event_id` int(10) unsigned NOT NULL DEFAULT '0',
`rate_id` int(10) unsigned NOT NULL DEFAULT '0',
`competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
`competence_count_calc` int(10) UNSIGNED NOT NULL DEFAULT '0',
PRIMARY KEY (`event_id`,`rate_id`),
KEY `FK_event_rate_link_2` (`rate_id`),
CONSTRAINT `FK_event_rate_link_1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `FK_event_rate_link_2` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `material_rate_link` (
`material_id` int(10) unsigned NOT NULL DEFAULT '0',
`rate_id` int(10) unsigned NOT NULL DEFAULT '0',
`competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
`competence_count_calc` int(10) UNSIGNED NOT NULL DEFAULT '0',
PRIMARY KEY (`material_id`,`rate_id`),
KEY `FK_material_rate_link_2` (`rate_id`),
CONSTRAINT `FK_material_rate_link_1` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `FK_material_rate_link_2` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `project_role_rate_link` (
`project_role_id` int(10) unsigned NOT NULL DEFAULT '0',
`rate_id` int(10) unsigned NOT NULL DEFAULT '0',
`competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
`competence_count_calc` int(10) UNSIGNED NOT NULL DEFAULT '0',
PRIMARY KEY (`project_role_id`,`rate_id`),
KEY `FK_project_role_rate_link_2` (`rate_id`),
CONSTRAINT `FK_project_role_rate_link_1` FOREIGN KEY (`project_role_id`) REFERENCES `project_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `FK_project_role_rate_link_2` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO material_rate_link (material_id, rate_id, competence_set_id) (
  SELECT e.id, r.id, r.competence_set_id FROM material e LEFT JOIN rate r ON (e.rate_id = r.id) WHERE NOT(r.id IS NULL)
);
INSERT INTO event_rate_link (event_id, rate_id, competence_set_id) (
  SELECT e.id, r.id, r.competence_set_id FROM event e LEFT JOIN rate r ON (e.rate_id = r.id) WHERE NOT(r.id IS NULL)
);
INSERT INTO project_role_rate_link (project_role_id, rate_id, competence_set_id) (
  SELECT e.id, r.id, r.competence_set_id FROM project_role e LEFT JOIN rate r ON (e.rate_id = r.id) WHERE NOT(r.id IS NULL)
);


UPDATE `project_role_rate_link` rl SET `competence_count_calc` = (SELECT count(*) FROM rate_competence_link rcl WHERE rcl.rate_id = rl.rate_id);

UPDATE `event_rate_link` rl SET `competence_count_calc` = (SELECT count(*) FROM rate_competence_link rcl WHERE rcl.rate_id = rl.rate_id);

UPDATE `material_rate_link` rl SET `competence_count_calc` = (SELECT count(*) FROM rate_competence_link rcl WHERE rcl.rate_id = rl.rate_id);