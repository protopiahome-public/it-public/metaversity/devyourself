ALTER TABLE `project` ADD COLUMN `access_read` ENUM('guest','member','moderator','admin') NOT NULL DEFAULT 'guest' AFTER `join_premoderation`;

ALTER TABLE `project` MODIFY COLUMN `access_read` ENUM('guest','member','moderator','admin') NOT NULL DEFAULT 'guest',
 ADD COLUMN `access_communities_add` ENUM('user','member','moderator','community_moderator','admin') NOT NULL AFTER `access_read`,
 ADD COLUMN `access_communities_read` ENUM('guest','project_member','member','moderator','admin') NOT NULL AFTER `access_communities_add`,
 ADD COLUMN `access_communities_comment` ENUM('user','project_member','member','moderator','admin') NOT NULL AFTER `access_communities_read`,
 ADD COLUMN `access_communities_post_add` ENUM('user','project_member','member','moderator','admin') NOT NULL AFTER `access_communities_comment`;

ALTER TABLE `project` MODIFY COLUMN `access_communities_add` ENUM('user','member','moderator','community_moderator','admin') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'user',
 MODIFY COLUMN `access_communities_read` ENUM('guest','project_member','member','moderator','admin') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_communities_comment` ENUM('user','project_member','member','moderator','admin') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'user',
 MODIFY COLUMN `access_communities_post_add` ENUM('user','project_member','member','moderator','admin') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'user';

ALTER TABLE `project` MODIFY COLUMN `access_communities_add` ENUM('user','member','moderator','communities_moderator','admin') NOT NULL DEFAULT 'user';

ALTER TABLE `project` MODIFY COLUMN `access_communities_add` ENUM('user','member','moderator','admin','communities_moderator') NOT NULL DEFAULT 'user';

ALTER TABLE `project` MODIFY COLUMN `access_communities_add` ENUM('user','member','moderator','communities_moderator','admin') NOT NULL DEFAULT 'user',
 MODIFY COLUMN `access_communities_read` ENUM('guest','project_member','member','moderator','admin') NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_communities_comment` ENUM('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user',
 MODIFY COLUMN `access_communities_post_add` ENUM('user','project_member','member','moderator','admin') NOT NULL DEFAULT 'user';

ALTER TABLE `project` MODIFY COLUMN `access_communities_read` ENUM('guest','project_member') NOT NULL DEFAULT 'guest';

ALTER TABLE `project` MODIFY COLUMN `access_communities_comment` ENUM('user','project_member','member','admin') NOT NULL DEFAULT 'user',
 MODIFY COLUMN `access_communities_post_add` ENUM('user','project_member','member','admin') NOT NULL DEFAULT 'user';

ALTER TABLE `project` MODIFY COLUMN `access_read` ENUM('guest','member','moderator','admin') NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_communities_add` ENUM('user','member','moderator','communities_moderator','admin') NOT NULL DEFAULT 'user',
 ADD COLUMN `access_events_read` ENUM('guest','member','moderator','events_moderator','admin') NOT NULL DEFAULT 'guest' AFTER `access_communities_post_add`,
 ADD COLUMN `access_events_comment` ENUM('user','member','moderator','events_moderator','admin') NOT NULL DEFAULT 'user' AFTER `access_events_read`;

ALTER TABLE `project` MODIFY COLUMN `access_events_read` ENUM('guest','member','moderator','events_moderator','admin') NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_events_comment` ENUM('user','member','moderator','events_moderator','admin') NOT NULL DEFAULT 'user',
 ADD COLUMN `access_materials_read` ENUM('guest','member','moderator','materials_moderator','admin') NOT NULL DEFAULT 'guest' AFTER `access_events_comment`,
 ADD COLUMN `access_materials_comment` ENUM('user','member','moderator','materials_moderator','admin') NOT NULL DEFAULT 'user' AFTER `access_materials_read`,
 ADD COLUMN `access_precedents_read` ENUM('guest','member','moderator','precedents_moderator','admin') NOT NULL DEFAULT 'guest' AFTER `access_materials_comment`,
 ADD COLUMN `access_precedents_comment` ENUM('user','member','moderator','precedents_moderator','admin') NOT NULL DEFAULT 'user' AFTER `access_precedents_read`;

ALTER TABLE `project` MODIFY COLUMN `access_communities_add` ENUM('user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'user',
 MODIFY COLUMN `access_events_read` ENUM('guest','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_events_comment` ENUM('user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'user',
 MODIFY COLUMN `access_materials_read` ENUM('guest','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_materials_comment` ENUM('user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'user',
 MODIFY COLUMN `access_precedents_read` ENUM('guest','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_precedents_comment` ENUM('user','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'user';
