CREATE TABLE  `community_widget_type` (
  `type_id` char(40) NOT NULL DEFAULT '',
  `title` char(100) DEFAULT NULL,
  `multiple_instances` tinyint(1) NOT NULL DEFAULT '0',
  `ctrl` char(100) NOT NULL,
  `edit_ctrl` char(100) NOT NULL DEFAULT '',
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

CREATE TABLE  `community_widget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `column` int(2) unsigned NOT NULL DEFAULT '1',
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  `title` varchar(100) NOT NULL DEFAULT '',
  `type_id` char(40) NOT NULL,
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_community_widget_community_id` (`community_id`) USING BTREE,
  KEY `FK_community_widget_type_id` (`type_id`) USING BTREE,
  CONSTRAINT `FK_community_widget_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`),
  CONSTRAINT `FK_community_widget_type_id` FOREIGN KEY (`type_id`) REFERENCES `community_widget_type` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

CREATE TABLE  `community_widget_text` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `html` text NOT NULL,
  PRIMARY KEY (`widget_id`),
  CONSTRAINT `FK_community_widget_text_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `community_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE  `community_widget_default` (
  `column` int(2) unsigned NOT NULL DEFAULT '1',
  `position` int(10) unsigned NOT NULL DEFAULT '999999',
  `title` varchar(100) NOT NULL DEFAULT '',
  `type_id` char(40) NOT NULL,
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`column`,`position`),
  KEY `FK_community_widget_default_type_id` (`type_id`) USING BTREE,
  CONSTRAINT `FK_community_widget_default_type_id` FOREIGN KEY (`type_id`) REFERENCES `community_widget_type` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

INSERT INTO `community_widget_type` (`type_id`, `title`, `multiple_instances`, `ctrl`, `edit_ctrl`, `lister_item_count`) VALUES   
  ('last_comments', 'Последние комментарии', 0, 'community_widget_last_comments', 'lister', 5),
  ('last_posts', 'Последние записи', 0, 'community_widget_last_posts', 'lister', 5),
  ('stat', 'Статистика', 0, 'community_widget_stat', '', 0),
  ('text', 'Текстовый блок', 1, 'community_widget_text', 'text', 0),
  ('top_users', 'Активные участники', 0, 'community_widget_top_users', 'lister', 10);
  
INSERT INTO `community_widget_default` (`column`, `position`, `title`, `type_id`, `lister_item_count`) VALUES
  (2, 1, 'Последние записи', 'last_posts', 5),
  (2, 2, 'Последние комментарии', 'last_comments', 5),
  (3, 1, 'Активные участники', 'top_users', 10),
  (3, 3, 'Статистика', 'stat', 0);