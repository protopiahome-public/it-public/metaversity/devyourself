ALTER TABLE  mark_calc
	CHANGE witness_user_id rater_user_id int(10) unsigned NOT NULL,
	DROP FOREIGN KEY FK_mark_calc_witness_user_id,
	DROP KEY FK_mark_calc_witness_user_id;

ALTER TABLE mark_calc
	ADD FOREIGN KEY (rater_user_id) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE;

--

ALTER TABLE mark_group
	CHANGE witness_user_id rater_user_id int(10) unsigned NOT NULL DEFAULT 0,
	DROP FOREIGN KEY FK_mark_group_witness_user_id,
	DROP KEY FK_mark_group_witness_user_id;
	
ALTER TABLE mark_group
	ADD FOREIGN KEY (rater_user_id) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE;

--

ALTER TABLE mark_personal
	CHANGE witness_user_id rater_user_id int(10) unsigned NOT NULL DEFAULT 0,
	DROP FOREIGN KEY FK_mark_personal_witness_user_id,
	DROP KEY FK_mark_personal_witness_user_id;
	
ALTER TABLE mark_personal
	ADD FOREIGN KEY (rater_user_id) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE;

--

RENAME TABLE precedent_witness_link TO precedent_rater_link;

ALTER TABLE precedent_rater_link
	CHANGE witness_user_id rater_user_id int(10) unsigned NOT NULL,
	DROP FOREIGN KEY FK_precedent_witness_link_witness_user_id,
	DROP FOREIGN KEY FK_precedent_witness_link_precedent_id,
	DROP KEY FK_precedent_witness_link_witness_user_id;
	
ALTER TABLE precedent_rater_link
	ADD FOREIGN KEY (rater_user_id) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD FOREIGN KEY (precedent_id) REFERENCES precedent (id) ON DELETE CASCADE ON UPDATE CASCADE;
