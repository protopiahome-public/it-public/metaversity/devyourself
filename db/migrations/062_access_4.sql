ALTER TABLE `community` CHANGE COLUMN `access_post_add` `access_add_post` ENUM('user','project_member','member','moderator','admin') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'user';
ALTER TABLE `section` CHANGE COLUMN `access_post_add` `access_add_post` ENUM('user','project_member','member','moderator','admin') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'user';
