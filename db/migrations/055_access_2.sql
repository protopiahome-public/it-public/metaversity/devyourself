ALTER TABLE `project` DROP COLUMN `access_communities_comment`,
 DROP COLUMN `access_communities_post_add`;

ALTER TABLE `project` MODIFY COLUMN `access_communities_read` ENUM('guest','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest',
 MODIFY COLUMN `access_events_read` ENUM('guest','member','moderator','special_moderator','admin') NOT NULL DEFAULT 'guest';
