ALTER TABLE `event` DROP COLUMN `rate_id`
, DROP INDEX `FK_event_rate_id`,
 DROP FOREIGN KEY `FK_event_rate_id`;
 
 ALTER TABLE `material` DROP COLUMN `rate_id`
, DROP INDEX `FK_material_rate_id`,
 DROP FOREIGN KEY `FK_material_rate_id`;
 
 ALTER TABLE `project_role` DROP COLUMN `rate_id`
, DROP INDEX `FK_project_role_rate_id`,
 DROP FOREIGN KEY `FK_project_role_rate_id`;