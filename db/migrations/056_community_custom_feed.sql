CREATE TABLE `community_custom_feed` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `descr` text NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_community_custom_feed_community_id` (`community_id`) USING BTREE,
  CONSTRAINT `FK_community_custom_feed_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `community_custom_feed_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `custom_feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `section_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique_item` (`custom_feed_id`,`community_id`,`section_id`) USING BTREE,
  KEY `FK_community_custom_feed_item_community_id` (`community_id`) USING BTREE,
  KEY `FK_community_custom_feed_item_section_id` (`section_id`) USING BTREE,
  CONSTRAINT `FK_community_custom_feed_item_custom_feed_id` FOREIGN KEY (`custom_feed_id`) REFERENCES `community_custom_feed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_custom_feed_item_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`),
  CONSTRAINT `FK_community_custom_feed_item_section_id` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `community_widget_custom_feed` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_custom_feed_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`widget_id`),
  KEY `FK_community_widget_custom_feed_community_custom_feed_id` (`community_custom_feed_id`) USING BTREE,
  CONSTRAINT `FK_community_widget_custom_feed_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `community_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_community_widget_custom_feed_community_feed_id` FOREIGN KEY (`community_custom_feed_id`) REFERENCES `community_custom_feed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `community_widget_type` (type_id, title, multiple_instances, ctrl, edit_ctrl, lister_item_count)
  VALUES ('custom_feed', 'Лента', 1, 'custom_feed', 'custom_feed', 5);
  
ALTER TABLE `project_admin`
 DROP COLUMN `is_feed_moderator`;