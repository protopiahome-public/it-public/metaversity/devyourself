DELETE FROM project_taxonomy_item;

INSERT INTO project_taxonomy_item (project_id, type, module_name, title, position, add_time, edit_time, show_in_menu)
(
	SELECT id, 'module', 'communities', 'Сообщества', 1, NOW(), NOW(), 1
	FROM project
	WHERE communities_are_on = 1
);

INSERT INTO project_taxonomy_item (project_id, type, module_name, title, position, add_time, edit_time, show_in_menu)
(
	SELECT id, 'module', 'vector', 'Вектор', 2, NOW(), NOW(), 1
	FROM project
	WHERE vector_is_on = 1
);

INSERT INTO project_taxonomy_item (project_id, type, module_name, title, position, add_time, edit_time, show_in_menu)
(
	SELECT id, 'module', 'roles', 'Рекомендация ролей', 3, NOW(), NOW(), 1
	FROM project
	WHERE role_recommendation_is_on = 1
);

INSERT INTO project_taxonomy_item (project_id, type, module_name, title, position, add_time, edit_time, show_in_menu)
(
	SELECT id, 'module', 'events', 'События', 4, NOW(), NOW(), 1
	FROM project
	WHERE events_are_on = 1
);

INSERT INTO project_taxonomy_item (project_id, type, module_name, title, position, add_time, edit_time, show_in_menu)
(
	SELECT id, 'module', 'materials', 'Материалы', 5, NOW(), NOW(), 1
	FROM project
	WHERE materials_are_on = 1
);

INSERT INTO project_taxonomy_item (project_id, type, module_name, title, position, add_time, edit_time, show_in_menu)
(
	SELECT id, 'module', 'marks', 'Прецеденты', 6, NOW(), NOW(), 1
	FROM project
	WHERE marks_are_on = 1
);

INSERT INTO project_taxonomy_item (project_id, type, module_name, title, position, add_time, edit_time, show_in_menu)
(
	SELECT id, 'module', 'members', 'Участники', 7, NOW(), NOW(), 1
	FROM project
);