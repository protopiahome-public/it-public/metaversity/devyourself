ALTER TABLE `project_widget_default` CHANGE COLUMN `sort` `position` INTEGER UNSIGNED NOT NULL DEFAULT 999999;
ALTER TABLE `project_widget` CHANGE COLUMN `sort` `position` INTEGER UNSIGNED NOT NULL DEFAULT 999999;
ALTER TABLE `project_widget_communities` CHANGE COLUMN `sort` `position` INTEGER UNSIGNED NOT NULL DEFAULT 999999;