CREATE TABLE  `project_widget_type` (
  `type_id` char(40) NOT NULL DEFAULT '',
  `title` char(100) DEFAULT NULL,
  `multiple_instances` tinyint(1) NOT NULL DEFAULT '0',
  `ctrl` char(100) NOT NULL,
  `edit_ctrl` char(100) NOT NULL DEFAULT '',
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

CREATE TABLE  `project_widget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `column` int(2) unsigned NOT NULL DEFAULT '1',
  `sort` int(10) unsigned NOT NULL DEFAULT '999999',
  `title` varchar(100) NOT NULL DEFAULT '',
  `type_id` char(40) NOT NULL,
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_project_widget_project_id` (`project_id`) USING BTREE,
  KEY `FK_project_widget_type_id` (`type_id`) USING BTREE,
  CONSTRAINT `FK_project_widget_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `FK_project_widget_type_id` FOREIGN KEY (`type_id`) REFERENCES `project_widget_type` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

CREATE TABLE  `project_widget_text` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `html` text NOT NULL,
  PRIMARY KEY (`widget_id`),
  CONSTRAINT `FK_project_widget_text_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `project_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE  `project_widget_communities` (
  `widget_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL,
  `sort` int(10) unsigned NOT NULL DEFAULT '999999',
  PRIMARY KEY (`widget_id`,`community_id`),
  KEY `FK_project_widget_communities_2` (`community_id`),
  CONSTRAINT `FK_project_widget_community_id` FOREIGN KEY (`community_id`) REFERENCES `community` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_widget_communities_widget_id` FOREIGN KEY (`widget_id`) REFERENCES `project_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

CREATE TABLE  `project_widget_default` (
  `column` int(2) unsigned NOT NULL DEFAULT '1',
  `sort` int(10) unsigned NOT NULL DEFAULT '999999',
  `title` varchar(100) NOT NULL DEFAULT '',
  `type_id` char(40) NOT NULL,
  `lister_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `FK_project_widget_default_type_id` (`type_id`) USING BTREE,
  CONSTRAINT `FK_project_widget_default_type_id` FOREIGN KEY (`type_id`) REFERENCES `project_widget_type` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

INSERT INTO `project_widget_type` (`type_id`, `title`, `multiple_instances`, `ctrl`, `edit_ctrl`, `lister_item_count`) VALUES   
  ('communities', 'Сообщества', 1, 'project_widget_communities', 'communities', 0),
  ('events', 'Новые события', 0, 'project_widget_events', 'lister', 5),
  ('last_comments', 'Последние комментарии', 0, 'project_widget_last_comments', 'lister', 5),
  ('last_posts', 'Последние записи', 0, 'project_widget_last_posts', 'lister', 5),
  ('materials', 'Последние материалы', 0, 'project_widget_materials', 'lister', 5),
  ('stat', 'Статистика', 0, 'project_widget_stat', '', 0),
  ('text', 'Текстовый блок', 1, 'project_widget_text', 'text', 0),
  ('top_users', 'Активные участники', 0, 'project_widget_top_users', 'lister', 10);