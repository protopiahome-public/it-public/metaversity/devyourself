ALTER TABLE `community_custom_feed_item` RENAME TO `community_custom_feed_source`;
ALTER TABLE `project_custom_feed_item` RENAME TO `project_custom_feed_source`;

ALTER TABLE `project_custom_feed_source` DROP INDEX `FK_project_custom_feed_item_community_id`
, DROP INDEX `FK_project_custom_feed_item_section_id`,
 ADD INDEX `FK_project_custom_feed_source_community_id` USING BTREE(`community_id`),
 ADD INDEX `FK_project_custom_feed_source_section_id` USING BTREE(`section_id`),
 DROP FOREIGN KEY `FK_project_custom_feed_item_custom_feed_id`,
 DROP FOREIGN KEY `FK_project_custom_feed_item_community_id`,
 DROP FOREIGN KEY `FK_project_custom_feed_item_section_id`,
 ADD CONSTRAINT `FK_project_custom_feed_source_custom_feed_id` FOREIGN KEY `FK_project_custom_feed_source_custom_feed_id` (`custom_feed_id`)
    REFERENCES `project_custom_feed` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_project_custom_feed_source_community_id` FOREIGN KEY `FK_project_custom_feed_source_community_id` (`community_id`)
    REFERENCES `community` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
 ADD CONSTRAINT `FK_project_custom_feed_source_section_id` FOREIGN KEY `FK_project_custom_feed_source_section_id` (`section_id`)
    REFERENCES `section` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
ALTER TABLE `community_custom_feed_source` DROP INDEX `FK_community_custom_feed_item_community_id`
, DROP INDEX `FK_community_custom_feed_item_section_id`,
 ADD INDEX `FK_community_custom_feed_source_community_id` USING BTREE(`community_id`),
 ADD INDEX `FK_community_custom_feed_source_section_id` USING BTREE(`section_id`),
 DROP FOREIGN KEY `FK_community_custom_feed_item_custom_feed_id`,
 DROP FOREIGN KEY `FK_community_custom_feed_item_community_id`,
 DROP FOREIGN KEY `FK_community_custom_feed_item_section_id`,
 ADD CONSTRAINT `FK_community_custom_feed_source_custom_feed_id` FOREIGN KEY `FK_community_custom_feed_source_custom_feed_id` (`custom_feed_id`)
    REFERENCES `community_custom_feed` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_community_custom_feed_source_community_id` FOREIGN KEY `FK_community_custom_feed_source_community_id` (`community_id`)
    REFERENCES `community` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
 ADD CONSTRAINT `FK_community_custom_feed_source_section_id` FOREIGN KEY `FK_community_custom_feed_source_section_id` (`section_id`)
    REFERENCES `section` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
