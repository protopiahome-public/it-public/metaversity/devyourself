CREATE TABLE  `project_taxonomy_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `position` int(10) unsigned NOT NULL,
  `type` set('module','static_page','link','folder') NOT NULL,
  `title` varchar(255) NOT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `static_page_name` varchar(255) DEFAULT NULL,
  `block_set_id` int(10) unsigned DEFAULT NULL,
  `module_name` varchar(150) DEFAULT NULL,
  `show_in_menu` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL,
  `edit_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_project_taxonomy_item_project_id` (`project_id`),
  KEY `FK_project_taxonomy_item_parent_id` (`parent_id`),
  KEY `FK_project_taxonomy_item_block_set_id` (`block_set_id`),
  CONSTRAINT `FK_project_taxonomy_item_block_set_id` FOREIGN KEY (`block_set_id`) REFERENCES `block_set` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_taxonomy_item_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `project_taxonomy_item` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_project_taxonomy_item_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;