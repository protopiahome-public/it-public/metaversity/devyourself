ALTER TABLE project
  ADD COLUMN `bg_head_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  ADD COLUMN `bg_head_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  ADD COLUMN `bg_head_main_url` varchar(255) DEFAULT NULL;
  
ALTER TABLE project
  ADD COLUMN `bg_body_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  ADD COLUMN `bg_body_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  ADD COLUMN `bg_body_main_url` varchar(255) DEFAULT NULL;
  
ALTER TABLE community
  ADD COLUMN `bg_head_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  ADD COLUMN `bg_head_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  ADD COLUMN `bg_head_main_url` varchar(255) DEFAULT NULL;
  
ALTER TABLE community
  ADD COLUMN `bg_body_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  ADD COLUMN `bg_body_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  ADD COLUMN `bg_body_main_url` varchar(255) DEFAULT NULL;