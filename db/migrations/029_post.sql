ALTER TABLE `post` ADD COLUMN `community_id` INTEGER UNSIGNED NOT NULL AFTER `block_set_id`;
ALTER TABLE `post` ADD CONSTRAINT `FK_post_community_id` FOREIGN KEY `FK_post_community_id` (`community_id`)
	REFERENCES `community` (`id`)
	ON DELETE CASCADE
	ON UPDATE CASCADE;

ALTER TABLE `post` ADD COLUMN `is_deleted` BOOLEAN NOT NULL DEFAULT 0 AFTER `comment_count_calc`;

CREATE TABLE `post_calc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_user_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `community_id` int(10) unsigned NOT NULL,
  `section_id` int(10) unsigned DEFAULT NULL,
  `show_on_main` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_author` (`author_user_id`,`id`),
  KEY `index_project` (`project_id`,`id`),
  KEY `index_community` (`community_id`,`id`),
  KEY `index_section` (`section_id`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
