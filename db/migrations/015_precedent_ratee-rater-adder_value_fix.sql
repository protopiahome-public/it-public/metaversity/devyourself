-- -----------------------------------------------------------------------
-- precedent_ratee_calc : initial values, take in account ratee without marks 
INSERT INTO precedent_ratee_calc (precedent_id, ratee_user_id)
	SELECT
		p.id AS precedent_id,
		pfgul.user_id AS ratee_user_id
	FROM
		precedent AS p,
		precedent_flash_group AS pfg,
		precedent_flash_group_user_link AS pfgul
	WHERE
		pfg.precedent_id = p.id
		AND pfgul.precedent_flash_group_id = pfg.id
	GROUP BY
		p.id, pfgul.user_id
ON DUPLICATE KEY UPDATE
	personal_mark_count_calc = personal_mark_count_calc;

