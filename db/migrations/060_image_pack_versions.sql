ALTER TABLE project
  ADD COLUMN `logo_version` int(10) unsigned NOT NULL DEFAULT '0';

 ALTER TABLE project
  ADD COLUMN `bg_head_version` int(10) unsigned NOT NULL DEFAULT '0';
  
ALTER TABLE project
  ADD COLUMN `bg_body_version` int(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE community
  ADD COLUMN `logo_version` int(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE community
  ADD COLUMN `bg_head_version` int(10) unsigned NOT NULL DEFAULT '0';
  
ALTER TABLE community
  ADD COLUMN `bg_body_version` int(10) unsigned NOT NULL DEFAULT '0';
  
ALTER TABLE user
  ADD COLUMN `photo_version` int(10) unsigned NOT NULL DEFAULT '0';