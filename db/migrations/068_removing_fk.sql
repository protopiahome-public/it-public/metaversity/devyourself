ALTER TABLE `community_custom_feed`
 DROP FOREIGN KEY `FK_community_custom_feed_community_id`;

ALTER TABLE `community_custom_feed` ADD CONSTRAINT `FK_community_custom_feed_community_id` FOREIGN KEY `FK_community_custom_feed_community_id` (`community_id`)
    REFERENCES `community` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `community_custom_feed_source`
 DROP FOREIGN KEY `FK_community_custom_feed_source_community_id`;

ALTER TABLE `community_custom_feed_source` ADD CONSTRAINT `FK_community_custom_feed_source_community_id` FOREIGN KEY `FK_community_custom_feed_source_community_id` (`community_id`)
    REFERENCES `community` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `project_custom_feed`
 DROP FOREIGN KEY `FK_project_custom_feed_project_id`;

ALTER TABLE `project_custom_feed` ADD CONSTRAINT `FK_project_custom_feed_project_id` FOREIGN KEY `FK_project_custom_feed_project_id` (`project_id`)
    REFERENCES `project` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `project_custom_feed_source`
 DROP FOREIGN KEY `FK_project_custom_feed_source_community_id`;

ALTER TABLE `project_custom_feed_source` ADD CONSTRAINT `FK_project_custom_feed_source_community_id` FOREIGN KEY `FK_project_custom_feed_source_community_id` (`community_id`)
    REFERENCES `community` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `community_widget`
 DROP FOREIGN KEY `FK_community_widget_community_id`;

ALTER TABLE `community_widget`
 DROP FOREIGN KEY `FK_community_widget_type_id`;

ALTER TABLE `community_widget` ADD CONSTRAINT `FK_community_widget_community_id` FOREIGN KEY `FK_community_widget_community_id` (`community_id`)
    REFERENCES `community` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_community_widget_type_id` FOREIGN KEY `FK_community_widget_type_id` (`type_id`)
    REFERENCES `community_widget_type` (`type_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
