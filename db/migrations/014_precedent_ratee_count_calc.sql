ALTER TABLE  precedent ADD COLUMN ratee_count_calc INT( 10 ) UNSIGNED NOT NULL DEFAULT  '0';

UPDATE
	precedent,
	(
		SELECT precedent_id, COUNT( ratee_user_id ) AS ratee_count_calc
		FROM precedent_ratee_calc
		GROUP BY (
		precedent_id
		)
	) AS tmp
SET
	precedent.ratee_count_calc = tmp.ratee_count_calc
WHERE
	precedent.id = tmp.precedent_id
