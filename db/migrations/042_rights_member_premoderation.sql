ALTER TABLE `project` ADD COLUMN `join_premoderation` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `comment_count_calc`;
ALTER TABLE `community` ADD COLUMN `join_premoderation` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `is_deleted`;

ALTER TABLE `user_project_link` MODIFY COLUMN `status` ENUM('deleted','pretender','member','moderator','admin') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'deleted';
ALTER TABLE `community_user_link` MODIFY COLUMN `status` ENUM('deleted','pretender','member','admin') NOT NULL DEFAULT 'deleted';
